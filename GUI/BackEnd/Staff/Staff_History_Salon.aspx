﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Staff_History_Salon.aspx.cs" Inherits="_30shine.GUI.BackEnd.UIStaff.Staff_History_Salon" %>


<asp:Content ID="pageHead" ContentPlaceHolderID="head" runat="server">
    <title>Lịch sử chuyển Salon</title>

    <style>
        .table-listing .uv-avatar {
            width: 120px;
        }

        .table-row-detail {
            display: inline-block;
            min-width: 500px;
            float: none !important;
        }

        .event-team-add {
            width: 600px;
            float: left;
            padding-bottom: 10px;
            display: none;
        }

            .event-team-add .container {
                min-width: 600px;
            }

            .event-team-add .team-add-head {
                padding: 10px;
                font-weight: normal;
                text-transform: uppercase;
                font-family: Roboto Condensed Bold;
                float: left;
                width: 100%;
                margin-bottom: 10px;
                border-bottom: 1px solid #e7e7e7;
            }

            .event-team-add label {
                line-height: 34px;
                font-weight: normal;
            }

            .event-team-add .form-group {
                float: left;
                width: 100%;
            }

        .ms-sel-ctn input[type='text'] {
            height: auto !important;
            line-height: 24px !important;
            border: none !important;
        }

        .ms-ctn .ms-sel-item {
            line-height: 22px;
            color: #000000;
            font-size: 13px;
        }

        .ms-helper {
            display: none !important;
        }

        .ms-ctn .dropdown-menu {
            height: 220px;
        }
    </style>

</asp:Content>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Lịch sử &nbsp;&#187; </li>
                        <li class="be-report-li"><a href="javascript:void(0);"><i class="fa fa-th-large"></i>Danh sách</a></li>
                        <li class="li-add"><a href="javascript:void(0);" onclick="fnTransferSalon($(this))">Chuyển Salon</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing be-report be-report-timekeeping">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <asp:ScriptManager ID="SM01" runat="server"></asp:ScriptManager>
                <div class="row">
                    <div class="filter-item">
                        <strong class="st-head"><i class="fa fa-filter"></i>Điều kiện lọc</strong>
                        <%--  <div class="time-wp">
                            <asp:TextBox CssClass="txtDateTime st-head" ID="TxtDateTimeFrom" placeholder="Từ ngày" Style="margin-left: 10px!important"
                                ClientIDMode="Static" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="ValidateTime" ControlToValidate="TxtDateTimeFrom" runat="server" CssClass="fb-cover-error" Text="Vui lòng chọn thời gian!"></asp:RequiredFieldValidator>
                        </div>
                        <strong class="st-head" style="margin-left: 10px;">
                            <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                        </strong>
                        <asp:TextBox CssClass="txtDateTime st-head" ID="TxtDateTimeTo" placeholder="Đến ngày" Style="margin-left: 10px!important"
                            ClientIDMode="Static" runat="server"></asp:TextBox>--%>
                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="ddlSalon" CssClass="form-control select" runat="server" ClientIDMode="Static" Style="width: 190px;"></asp:DropDownList>
                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="ddlBoPhan" runat="server" ClientIDMode="Static" Style="width: 190px;"></asp:DropDownList>
                    </div>

                    <asp:Panel ID="ViewData" CssClass="st-head btn-viewdata" ClientIDMode="Static"
                        onclick="excPaging(1)" runat="server">
                        Xem dữ liệu
                    </asp:Panel>
                    <a href="javascript:void(0);" onclick="location.href=location.href" class="st-head btn-viewdata">Reset Filter</a>

                </div>
                <div class="row">
                    <strong class="st-head"><i class="fa fa-file-text"></i>Lịch sử chuyển salon của nhân viên</strong>
                </div>
                <!-- Row Table Filter -->
                <div class="table-func-panel" style="margin-top: -33px;">
                    <div class="table-func-elm">
                        <span>Số hàng / Page : </span>
                        <div class="table-func-input-wp">
                            <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                            <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                            <ul class="ul-opt-segment">
                                <li data-value="10">10</li>
                                <li data-value="20">20</li>
                                <li data-value="30">30</li>
                                <li data-value="40">40</li>
                                <li data-value="50">50</li>
                                <li data-value="1000000">Tất cả</li>
                            </ul>
                            <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                        </div>
                    </div>
                </div>
                <!-- End Row Table Filter -->
                <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                    <ContentTemplate>
                        <div class="row table-wp">
                            <table class="table-add table-listing" id="tblHistorySalon">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Mã nhân viên</th>
                                        <th>Nhân viên</th>
                                        <th>Cấp bậc</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="rptHistorySalon" runat="server">
                                        <ItemTemplate>
                                            <tr onclick="addTrToTable($(this));" class="parent" data-id="<%# Eval("Id") %>">
                                                <td><%# Container.ItemIndex + 1 + PAGING._Offset %></td>
                                                <td><%# Eval("Id") %></td>
                                                <td><a href="javascript://" title="Chuyển Salon" onclick="fnTransferSalon($(this))" data-salonid="<%# Eval("SalonId") %>" data-staffid="<%# Eval("Id") %>"><%# Eval("Fullname") %></a></td>
                                                <td><%# Eval("SkillLevelName") %></td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>
                        <!-- Paging -->
                        <div class="site-paging-wp">
                            <% if (PAGING.TotalPage > 1)
                                { %>
                            <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                                <% if (PAGING._Paging.Prev != 0)
                                    { %>
                                <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                                <% } %>
                                <asp:Repeater ID="RptPaging" runat="server">
                                    <ItemTemplate>
                                        <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                            <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>>
                                            <%# Eval("PageNum") %>
                                        </a>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                                    { %>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                                <% } %>
                            </asp:Panel>
                            <% } %>
                        </div>
                        <!-- End Paging -->
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />

            </div>
            <%-- END Listing --%>
        </div>

        <!-- Pop up-->
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog" id="modalDialog">
                <!-- Modal content-->
                <div class="modal-content" id="modalContent">
                    <div class="modal-header" id="modalHeader">
                        <button type="button" class="close closepopup" data-dismiss="modal">&times;</button>
                        <h3 class="modal-title"><b>Chuyển salon</b></h3>
                    </div>
                    <div class="modal-body">

                        <div class="form-group form-group-lg row">
                            <div class="col-xs-3">Từ Salon</div>
                            <div class="col-xs-9">
                                <select class="form-control" name="ddlSalonFrom" id="ddlSalonFrom" onchange="getStafftBySalon()">
                                </select>
                            </div>
                        </div>
                        <div class="form-group form-group-lg row div-department">
                            <div class="col-xs-3">Bộ phận</div>
                            <div class="col-xs-9">
                                <select class="form-control" name="ddlDepartment" id="ddlDepartment" onchange="getStafftBySalon()">
                                </select>
                            </div>
                        </div>

                        <div class="form-group form-group-lg row">
                            <div class="col-xs-3">Nhân Viên</div>
                            <div class="col-xs-9">
                                <select class="form-control" id="ddlStaff">
                                </select>
                            </div>
                        </div>

                        <div class="form-group form-group-lg row">
                            <div class="col-xs-3">Chuyển sang Salon</div>
                            <div class="col-xs-9">
                                <select class="form-control" id="ddlSalonTo">
                                </select>
                            </div>
                        </div>

                        <div class="form-group form-group-lg row">
                            <div class="col-xs-10"></div>
                            <div class="col-xs-2">
                                <a class="btn btn-success" onclick="fnTransferSalonConfirm();" data-dismiss="modal">Hoàn tất</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="abc"></div>
        <!-- Page loading -->
        <div class="page-loading">
            <p>Vui lòng đợi trong giây lát...</p>
        </div>
        <!--/ Page loading -->

        <link href="/Assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" />
        <script src="/Assets/js/jquery.mCustomScrollbar.js"></script>
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 49% !important;
                margin-top: 5px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }
        </style>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                // Add active menu
                $("#glbAdminSales").addClass("active");
                $("#glbAdminReportService").addClass("active");
                $("li.be-report-li").addClass("active");

                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) { }
                });

                $(".closepopup").click(function () {
                    closepopup();
                });

            });



            function closepopup() {
                $(document.body).removeClass("modal-open");
                $(".modal-backdrop").removeClass('modal-backdrop fade in');
                $("#myModal").removeClass("in");
                $("#myModal").prop('aria-hidden', 'true');
                $("#myModal").css('display', 'none');
            }

            function loadSalonCombo(This) {
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/Staff/Staff_History_Salon.aspx/ReturnAllSalon",
                    data: '',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        var selectSalonFrom = $('#ddlSalonFrom');
                        var selectSalonTo = $('#ddlSalonTo');

                        selectSalonFrom.empty();
                        selectSalonTo.empty();

                        var optionFrom = "";
                        var optionTo = "";

                        $.each(response.d, function (key, value) {
                            optionFrom += '<option value="' + value.Id + '">' + value.Name + '</option>';
                            optionTo += '<option value="' + value.Id + '">' + value.Name + '</option>';
                        });

                        selectSalonFrom.append('<option selected="selected" value="0">Chọn Salon</option>' + optionFrom);
                        selectSalonTo.append('<option selected="selected" value="0">Chọn Salon</option>' + optionTo);

                        if ($(This).attr("data-salonId") != null) {
                            $('#ddlSalonFrom option').removeAttr('selected').filter('[value=' + $(This).attr("data-salonId") + ']').attr('selected', 'selected');
                        }

                    },
                    failure: function (response) { console.log(response.d); }
                });
            }

            function loadDepartmentCombo() {
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/Staff/Staff_History_Salon.aspx/ReturnAllStaff_Type",
                    data: '',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        var select = $('#ddlDepartment');
                        select.empty();

                        select.append('<option selected="selected" value="0">Chọn bộ phận</option>');
                        $.each(response.d, function (key, value) {
                            select.append('<option value="' + value.Id + '">' + value.Name + '</option>');
                        });
                        removeLoading();
                    },
                    failure: function (response) { console.log(response.d); }
                });
            }

            function getStafftBySalon() {
                var salonId = $("#ddlSalonFrom").val();
                var departmentId = $('#ddlDepartment').val();
                var select = $('#ddlStaff');
                select.empty();

                if (salonId == null) {
                    salonId = "0";
                }
                if (departmentId == null) {
                    departmentId = "0";
                }
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/Staff/Staff_History_Salon.aspx/ReturnAllStaffBySalon",
                    data: '{_SalonId: ' + salonId + ', _DepartmentId:' + departmentId + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        select.html('');
                        select.append('<option selected="selected" value="0">Chọn nhân viên</option>');
                        $.each(response.d, function (key, value) {
                            select.append('<option value="' + value.Id + '">' + value.Fullname + '</option>');
                        });
                        removeLoading();
                    },
                    failure: function (response) { console.log(response.d); }
                });
            }

            function addTrToTable(This) {
                //addLoading();
                var rowIndex = $('#tblHistorySalon tbody tr.parent').index(This);
                var newRow = $('<tr class="expand"> <td colspan="4">'
                  + '  <table class="table-row-detail">                       '
                  + '      <tbody>                                                                                                  '
                  + '         <tr><td colspan="6" style="border:none; text-align: left">Lịch sử chuyển salon</td></tr>                '
                  + '          <tr class="tr-field-ahalf tr-product">                                                                '
                  + '              <td style="border: none;" colspan="4">                                    '
                  + '                  <div class="listing-product item-product" id="ListingProductWp">                              '
                  + '                      <table class="table table-add table-listing" id="table-history-salon">    '
                  + '                          <thead>                                                                               '
                  + '                              <tr>                                                                              '
                  + '                                  <th>STT</th>                                                                 '
                  + '                                  <th>Ngày chuyển</th>                                                                  '
                  + '                                  <th>Từ salon </th>                                     '
                  + '                                  <th>Sang salon</th>                                                        '
                  + '                              </tr>                                                                             '
                  + '                          </thead>                                                                              '
                  + '                          <tbody>                                                                               '
                  + '                          </tbody>                                                                              '
                  + '                      </table>                                                                                  '
                  + '                  </div>                                                                                        '
                  + '              </td>                                                                                             '
                  + '         </tr>                                                                                                  '

                  + '      </tbody>                                                                                                   '
                  + '  </table>                                                                                                       '
                  + '    </td></tr>');

                var nextIndex = rowIndex + 1;
                if ($('#tblHistorySalon tbody tr:nth(' + nextIndex + ')').hasClass('expand')) {
                    $('#tblHistorySalon tbody').find('.expand').remove();
                    $('#tblHistorySalon tbody tr').removeClass('test');
                }
                else {
                    for (var i = 0; i < $('#tblHistorySalon tbody tr').length; i++) {
                        $('#tblHistorySalon tbody tr').removeClass('test');
                        $('#tblHistorySalon tbody').find('.expand').remove();
                    }
                    newRow.insertAfter($('#tblHistorySalon tbody tr:nth(' + rowIndex + ')'));
                    $(This).addClass('test');
                }
                LoadHistorySalonById(This);
            }

            function LoadHistorySalonById(This) {
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/Staff/Staff_History_Salon.aspx/LoadHistorySalonById",
                    data: '{_StaffId : ' + $(This).attr("data-Id") + ' }',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                       
                        if (response.d.length > 0) {
                            var trs = "";
                            console.log(response.d);
                            $.each(response.d, function (i, v) {
                                trs += '<tr>' +
                                        '<td class="td-STT">' + (i + 1) + '</td>' +
                                        '<td class="td-TransferDate">' + ConvertJsonDateToStringDateTime(v.TransferDate) + '</td>' +
                                        '<td class="td-OlderSalonName">' + v.OlderSalonName + '</td>' +
                                        '<td class="td-CurrentSalonName">' + v.CurrentSalonName + '</td>'
                                '</tr>';
                                
                            });
                            $('#table-history-salon tbody').append(trs);
                        }
                        else {
                            $('.table-row-detail').addClass('hidden');
                            $('#tblHistorySalon tbody tr.expand td').append('Không có lịch sử chuyển salon');
                        }
                        //removeLoading();
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            }

            function fnTransferSalon(This) {
                addLoading();
                $(document.body).addClass("modal-open");
                $(document.body).append('<div class="modal-backdrop fade in"></div>');
                $("#myModal").addClass("in");
                $("#myModal").prop('aria-hidden', 'false');
                $("#myModal").css('display', 'block');

                loadSalonCombo(This);

                if ($(This).attr("data-staffId") != null) {
                    $(".div-department").addClass("hidden");
                    $("#ddlStaff").prop("disabled", "true");
                    $("#ddlSalonFrom").prop("disabled", "true");

                    var select = $('#ddlStaff');
                    $.ajax({
                        type: "POST",
                        url: "/GUI/BackEnd/Staff/Staff_History_Salon.aspx/getStaffById",
                        data: '{_staffId: ' + $(This).attr("data-staffId") + '}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json", success: function (response) {
                            select.html('');
                            $.each(response.d, function (key, value) {
                                select.append('<option value="' + value.Id + '">' + value.Fullname + '</option>');
                            });
                            removeLoading();
                        },
                        failure: function (response) { console.log(response.d); }
                    });
                }
                else {
                    $(".div-department").removeClass("hidden");
                    $("#ddlStaff").removeAttr("disabled");
                    $("#ddlSalonFrom").removeAttr("disabled");
                    $("#ddlSalonFrom").remove("disabled");
                    getStafftBySalon();
                    loadDepartmentCombo();
                }
                if (!e) var e = window.event;
                e.cancelBubble = true;
                if (e.stopPropagation) e.stopPropagation();
            }

            function fnTransferSalonConfirm() {
                var salonFromId = $("#ddlSalonFrom").val();
                var salonToId = $("#ddlSalonTo").val();
                var staffId = $("#ddlStaff").val();
                if (salonFromId != "0" && salonToId != "0" && staffId != "0") {
                    $.ajax({
                        type: "POST",
                        url: "/GUI/BackEnd/Staff/Staff_History_Salon.aspx/TransferSalonConfirm",
                        data: '{salonFromId: ' + salonFromId + ', salonToId:' + salonToId + ', staffId:' + staffId + ' }',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json", success: function (response) {
                            alert("Chuyển salon thành công!");
                            closepopup();
                            setTimeout(function () {
                                $("#ViewData").click();
                            }, 1000);

                        },
                        failure: function (response) { console.log(response.d); }
                    });
                }

            }
        </script>

    </asp:Panel>
</asp:Content>
