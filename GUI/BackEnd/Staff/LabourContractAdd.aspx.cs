﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using DocumentFormat.OpenXml;
using _30shine.MODEL.ENTITY.EDMX;
using System.Web.Services;
using System.IO;

namespace _30shine.GUI.BackEnd.UIStaff
{
    public partial class LabourContractAdd : System.Web.UI.Page
    {
        private bool Perm_Access;
        private bool Perm_AllPermission;
        private bool Perm_TypeStaff;
        IContract ContractModel = new ContractModel();
        public static LabourContractAdd instance;
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                ////Perm_HidePermission = permissionModel.GetActionByActionNameAndPageId("Perm_HidePermission", PageID, permission);
                ////Perm_Add = permissionModel.GetActionByActionNameAndPageId("Perm_Add", PageID, permission);
                //Perm_AllPermission = permissionModel.GetActionByActionNameAndPageId("Perm_AllPermission", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_AllPermission = permissionModel.CheckPermisionByAction("Perm_AllPermission", pageId, staffId);
                Perm_TypeStaff = permissionModel.CheckPermisionByAction("Perm_TypeStaff", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        /// <summary>
        /// Get WorkDay_Listing instance
        /// </summary>
        /// <returns></returns>
        public static LabourContractAdd getInstance()
        {
            if (!(LabourContractAdd.instance is LabourContractAdd))
            {
                return new LabourContractAdd();
            }
            else
            {
                return LabourContractAdd.instance;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            txtPathTempContract.Enabled = false;
            if (!IsPostBack)
            {
                BindContractType();
                SetPermission();
                Bind_TypeStaff();
            }
        }
        protected void AddContract(object sender, EventArgs e)
        {
            int interger = 0;
            try
            {
                string contractName = txtNameContract.Text;
                int departmentId = int.TryParse(ddlTypeStaff.SelectedValue, out interger) ? interger : 0;
                string description = txtDescripton.Text;
                string pathContract = txtPathTempContract.Text;
                Contract objContract = new Contract();
                objContract.IsDelete = false;
                objContract.CreatedTime = DateTime.Now;
                objContract.DescriptionContract = description;
                objContract.DepartmentId = departmentId;
                objContract.Name = contractName;
                objContract.TypeContract = int.TryParse(ddlContractType.SelectedValue, out interger) ? interger : 0;
                objContract.PathContract = pathContract;
                //Kiểm tra hợp đồng có bị trùng không.
                var resultCheck = ContractModel.checkDuplicateContract(departmentId, objContract.TypeContract ?? 0);
                if (resultCheck.success)
                {
                    WaringClient(resultCheck.msg);
                    return;
                }
                var result = ContractModel.Insert(objContract);
                if (result.success)
                {
                    WaringClient("Thêm mới thành công");
                }
                else
                {
                    WaringClient(result.msg);
                }
            }
            catch(Exception ex)
            {
                WaringClient("Đã có lỗi xảy ra!");
            }

        }
        /// <summary>
        /// Gửi thông tin cảnh báo về client
        /// </summary>
        /// <param name="message"></param>
        public void WaringClient(string message)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Message System", "showMsgSystem('" + message + "','" + "msg-system success" + "'," + 15000 + ");", true);
        }
        /// <summary>
        /// Bind loại hợp đồng
        /// </summary>
        public void BindContractType()
        {
            using (var db = new Solution_30shineEntities())
            {
                var LST = db.Tbl_Config.Where(r=>r.Key == "hop_dong" && r.IsDelete == 0).ToList();
                var Key = 0;
                ddlContractType.DataTextField = "TypeContract";
                ddlContractType.DataValueField = "Id";
                System.Web.UI.WebControls.ListItem item = new System.Web.UI.WebControls.ListItem("Chọn loại hợp đồng", "0");
                ddlContractType.Items.Insert(0, item);
                if (LST.Count > 0)
                {
                    foreach (var v in LST)
                    {
                        Key++;
                        item = new System.Web.UI.WebControls.ListItem(v.Label, v.Value);
                        ddlContractType.Items.Insert(Key, item);
                    }
                    ddlContractType.SelectedIndex = 0;
                }
            }
        }
        /// <summary>
        /// Bind department
        /// </summary>
        public void Bind_TypeStaff()
        {
            using (var db = new Solution_30shineEntities())
            {
                var LST = db.Staff_Type.Where(w => w.IsDelete != 1 && w.Publish == true).OrderBy(o => o.Order).ToList();
                var Key = 0;
                ddlTypeStaff.DataTextField = "TypeStaff";
                ddlTypeStaff.DataValueField = "Id";

                System.Web.UI.WebControls.ListItem item = new System.Web.UI.WebControls.ListItem("Chọn bộ phận", "0");
                ddlTypeStaff.Items.Insert(0, item);

                if (LST.Count > 0)
                {
                    foreach (var v in LST)
                    {
                        Key++;
                        item = new System.Web.UI.WebControls.ListItem(v.Name, v.Id.ToString());
                        ddlTypeStaff.Items.Insert(Key, item);
                    }
                    ddlTypeStaff.SelectedIndex = 0;
                }
            }
        }
        [WebMethod]
        public static object SaveContractToFoldel(string base64, string department, string contractType)
        {
            var result = new _30shine.Helpers.Msg();
            if (base64 != "" && base64 != null )
            {
                byte[] bytes = System.Convert.FromBase64String(base64.Replace(@"data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64,", ""));
                string domainPhysical = getInstance().Server.MapPath("~");
                string pathFoldelPhysical = @"\Public\Word\";
                string domainUrl = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host;
                string pathFoldelUrl = @"/Public/Word/";
                string fileName = "temp-"+ department + "-" + contractType + "-" + Guid.NewGuid() + ".docx";
                string pathFileUrl = domainUrl + ':' + HttpContext.Current.Request.Url.Port  + pathFoldelUrl + fileName;
                string pathFilePhysical = domainPhysical  + pathFoldelPhysical + fileName;
                //Kiem tra neu khong ton tai thu muc thi tao thu muc moi
                if (!Directory.Exists(domainPhysical + pathFoldelPhysical))
                {
                    Directory.CreateDirectory(domainPhysical + pathFoldelPhysical);
                }
                try
                {
                    using (MemoryStream ms = new MemoryStream(bytes, true))
                    {
                        ms.Write(bytes, 0, bytes.Length);
                        using (WordprocessingDocument doc = WordprocessingDocument.Open(ms, true))
                        {
                            doc.SaveAs(pathFilePhysical).Dispose();
                        }
                    }
                    result.success = true;
                    result.msg = "Tải file thành công";
                    result.data = pathFileUrl;
                }
                catch (Exception ex)
                {
                    result.success = false;
                    result.msg = "Không tải được file!";
                }
            }
            else
            {
                result.success = false;
                result.msg = "File Word không hợp lệ";
            }
            return result;
        }
    }
}