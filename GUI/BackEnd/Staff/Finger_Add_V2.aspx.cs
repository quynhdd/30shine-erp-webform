﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Linq.Expressions;
using LinqKit;
using System.Web.Services;
using System.Data.Entity.Migrations;
using System.Web.Script.Serialization;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.UIStaff
{
    public partial class Finger_Add_V2 : System.Web.UI.Page
    {
        private string PageID = "NV_VT";
        protected Paging PAGING = new Paging();
        private Expression<Func<_30shine.MODEL.ENTITY.EDMX.Staff, bool>> Where = PredicateBuilder.True<_30shine.MODEL.ENTITY.EDMX.Staff>();
        private bool Perm_Access = false;
        private bool Perm_ViewAllData = false;
        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        private bool Perm_ShowSalon = false;
        protected int SalonId;
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                //Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }



        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
            
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            GenWhere();
            if (!IsPostBack)
            {
                Library.Function.bindSalon(new List<DropDownList> { Salon }, Perm_ViewAllData);
                Bind_StaffType();
                Bind_Paging();
                Bind_RptStaff();
                Get_FingerPrint();
            }
        }

        private void GenWhere()
        {
            Where = Where.And(w => w.IsDelete != 1 && (w.Permission != "admin" || w.Permission == null));
            int integer;
            int SalonId = int.TryParse(Salon.SelectedValue, out integer) ? integer : 0;
            int StaffTypeId = int.TryParse(StaffType.SelectedValue, out integer) ? integer : 0;
            if (SalonId > 0)
            {
                Where = Where.And(w => w.SalonId == SalonId);
            }
            if (StaffTypeId > 0)
            {
                Where = Where.And(w => w.Type == StaffTypeId);
            }
        }

        private void Bind_RptStaff()
        {
            using (var db = new Solution_30shineEntities())
            {
                var _Staffs = db.Staffs.AsExpandable().Where(Where).OrderByDescending(o => o.Id).Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                RptStaff.DataSource = _Staffs;
                RptStaff.DataBind();
            }
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            Bind_Paging();
            Bind_RptStaff();
        }        

        protected void Bind_Paging()
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage() : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage()
        {
            using (var db = new Solution_30shineEntities())
            {
                var Count = db.Staffs.AsExpandable().Count(Where);
                int TotalRow = Count - PAGING._TopNewsNum;
                int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
                return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
            }
        }

        private void Bind_StaffType()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Staff_Type.Where(w => w.IsDelete != 1).OrderBy(o => o.Id).ToList();
                var Key = 0;

                StaffType.DataTextField = "StaffType";
                StaffType.DataValueField = "Id";

                ListItem item = new ListItem("Chọn kiểu nhân viên", "0");
                StaffType.Items.Insert(0, item);

                foreach (var v in lst)
                {
                    Key++;
                    item = new ListItem(v.Name, v.Id.ToString());
                    StaffType.Items.Insert(Key, item);
                }
                StaffType.SelectedIndex = 0;
            }
        }

        private void Get_FingerPrint()
        {
                var FP_template = Request.QueryString["fgTemplate"];
                if (FP_template != null && FP_template != "")
                {
                    Session["FP_Template"] = FP_template;
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "Open Add Customer", "$('.addnew-customer').click();", true);
                }
        }

        [WebMethod]
        public static string Add_Finger(int id)
        {
            using (var db = new Solution_30shineEntities())
            {
                var Msg = new Msg();
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                //var FP_token = HttpContext.Current.Session["FP_Token"] != null ? HttpContext.Current.Session["FP_Token"].ToString() : "";
                var FP_template = HttpContext.Current.Session["FP_Template"] != null ? HttpContext.Current.Session["FP_Template"].ToString() : "";
                if (id > 0)
                {
                    var obj = db.Staffs.FirstOrDefault(w => w.Id == id);
                    if (obj != null)
                    {
                        //obj.FingerToken = FP_token;
                        obj.FingerTemplate = FP_template;
                        db.Staffs.AddOrUpdate(obj);
                        var exc = db.SaveChanges();

                        if (exc > 0)
                        {
                            var obj2 = new Staff_Roll();
                            obj2.StaffId = obj.Id;
                            obj2.CreatedDate = DateTime.Now;
                            db.Staff_Roll.AddOrUpdate(obj2);
                            db.SaveChanges();
                            HttpContext.Current.Session["FP_Template"] = null;
                            Msg.success = true;
                        }
                        else
                        {
                            Msg.success = false;
                        }
                    }
                }
                return serializer.Serialize(Msg);
            }
        }

        

        public void Repeater_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
              || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (Perm_Edit == true)
                    ((Panel)(e.Item.FindControl("EAedit"))).Visible = true;
                if (Perm_Delete == true)
                    ((Panel)(e.Item.FindControl("EAdelete"))).Visible = true;
            }
        }
    }

    struct Msg
    {
        public bool success { get; set; }
        public string msg { get; set; }
    }
}