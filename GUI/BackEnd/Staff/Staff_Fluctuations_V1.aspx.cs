﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Globalization;
using LinqKit;
using System.Linq.Expressions;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;
using System.Data.Entity;
using ExportToExcel;

namespace _30shine.GUI.BackEnd.UIStaff
{
    public partial class Staff_Fluctuations_V1 : System.Web.UI.Page
    {
        protected string PageID = "BK_DS";
        protected Paging PAGING = new Paging();
        private bool Perm_Access = false;
        public TimeSpan timeSpan = new TimeSpan();
        private bool Perm_Delete = false;
        protected bool Perm_ViewAllData = false;
        protected bool Perm_ViewAllDataByDate = false;
        protected bool Perm_ViewAllDataBySalon = false;
        protected string style_css = "";
        protected string style1_css = "";
        protected DateTime timeFrom;
        protected DateTime timeTo;
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                txtStartDate.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(DateTime.Now));
                txtEndDate.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(DateTime.Now));
                Library.Function.bindSalon_NotHoiQuan(new List<DropDownList> { ddlSalon }, Perm_ViewAllData);
                BindStaff();
                BindStatus();
                Bind_StaffType();
            }
        }
        CultureInfo culture = new CultureInfo("vi-VN");
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_ViewAllDataByDate = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllDataByDate", PageID, permission);
                //Perm_ViewAllDataBySalon = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllDataBySalon", PageID, permission);
                //Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllDataByDate = permissionModel.CheckPermisionByAction("Perm_ViewAllDataByDate", pageId, staffId);
                Perm_ViewAllDataBySalon = permissionModel.CheckPermisionByAction("Perm_ViewAllDataBySalon", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        protected string checkper()
        {
            string str = "";
            int _UserId = Convert.ToInt32(Session["User_Id"]);
            string a = Session["User_Permission"].ToString();
            if (a == "admin" || a == "root")
            {
                str = "";
            }
            if (_UserId == 119 || _UserId == 79)
            {
                str = "";
            }
            else
            {
                str = "display_permission";
            }

            return str;
        }

        private void Bind_StaffType()
        {
            using (var db = new Solution_30shineEntities())
            {
                ListItem item = new ListItem("Chọn bộ phận", "0");
                ddlStaffType.Items.Insert(0, item);

                var lst = db.Staff_Type.Where(w => w.IsDelete != 1).OrderBy(o => o.Id).ToList();
                if (lst.Count > 0)
                {
                    for (int i = 0; i < lst.Count; i++)
                    {
                        ddlStaffType.Items.Add(new ListItem(lst[i].Name, lst[i].Id.ToString()));
                    }
                    ddlStaffType.DataBind();
                }
            }
        }
        //private void Bind_Salon()
        //{
        //    using (var db = new Solution_30shineEntities())
        //    {
        //        var _Salons = db.Tbl_Salon.Where(w => w.IsDelete != 1 && w.Publish == true && w.IsSalonHoiQuan == false).OrderBy(o => o.Order).ToList();
        //        var Key = 0;
        //        var SalonId = Convert.ToInt32(Session["SalonId"]);

        //        ddlSalon.DataTextField = "Salon";
        //        ddlSalon.DataValueField = "Id";
        //        if (Perm_ViewAllData || Perm_ViewAllDataBySalon)
        //        {
        //            ListItem item = new ListItem("Chọn tất cả Salon", "0");
        //            ddlSalon.Items.Insert(0, item);

        //            foreach (var v in _Salons)
        //            {
        //                Key++;
        //                item = new ListItem(v.Name, v.Id.ToString());
        //                ddlSalon.Items.Insert(Key, item);
        //            }
        //            ddlSalon.SelectedIndex = 0;
        //        }
        //        else
        //        {
        //            foreach (var v in _Salons)
        //            {
        //                if (v.Id == SalonId)
        //                {
        //                    ListItem item = new ListItem(v.Name, v.Id.ToString());
        //                    ddlSalon.Items.Insert(Key, item);
        //                    ddlSalon.SelectedIndex = SalonId;
        //                    ddlSalon.Enabled = false;
        //                }

        //            }
        //        }
        //    }
        //}

        private void BindStatus()
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var sql = "";
                    sql = @"select * from Tbl_Status where Id in (7,8,11,12,13,14,16,17,18) order by Id asc";
                    var lstStatusName = db.Tbl_Status.SqlQuery(sql).ToList();
                    var Key = 0;
                    ddlStatus.DataTextField = "Name";
                    ddlStatus.DataValueField = "Id";
                    ListItem item = new ListItem("Chọn hình thức", "0");
                    ddlStatus.Items.Insert(0, item);
                    foreach (var v in lstStatusName)
                    {
                        Key++;
                        item = new ListItem(v.Name, v.Id.ToString());
                        ddlStatus.Items.Insert(Key, item);
                    }
                    ddlStatus.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        private void BindStaff()
        {
            using (var db = new Solution_30shineEntities())
            {
                int integer;
                var salon = int.TryParse(ddlSalon.SelectedValue, out integer) ? integer : 0;
                var staff = new List<Staff>();
                var whereSalon = "";
                var sql = "";
                if (salon > 0)
                {
                    whereSalon = " and SalonId = " + salon;
                }
                sql = @"select * from Staff 
                        where IsDelete = 0 
                        and (isAccountLogin = 0 or isAccountLogin is null)" + whereSalon;
                staff = db.Staffs.SqlQuery(sql).ToList();

                var first = new Staff();
                first.Id = 0;
                first.Fullname = "Chọn nhân viên";
                staff.Insert(0, first);

                ddlStaff.DataTextField = "Fullname";
                ddlStaff.DataValueField = "Id";
                ddlStaff.DataSource = staff;
                ddlStaff.DataBind();
            }
        }
        protected void bindStaffBySalon(object sender, EventArgs e)
        {
            BindStaff();
        }
        private void Permission()
        {
            int _UserId = Convert.ToInt32(Session["User_Id"]);
            if (_UserId == 119 || _UserId == 79 || _UserId == 0)
            {
                style1_css = "display_permission";
            }
            else
            {
                style_css = "display_permission";
            }
        }

        public List<cls_Staff_Fluctuation> bindData()
        {
            List<cls_Staff_Fluctuation> listRecord = new List<cls_Staff_Fluctuation>();
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    int integer;
                    int staffId = int.TryParse(ddlStaff.SelectedValue, out integer) ? integer : 0;
                    int salonId = int.TryParse(ddlSalon.SelectedValue, out integer) ? integer : 0;
                    int statusId = int.TryParse(ddlStatus.SelectedValue, out integer) ? integer : 0;
                    int staffType = int.TryParse(ddlStaffType.SelectedValue, out integer) ? integer : 0;
                    this.timeFrom = new DateTime();
                    this.timeTo = new DateTime();
                    if (txtStartDate.Text != "")
                    {
                        timeFrom = Convert.ToDateTime(txtStartDate.Text, culture);
                        if (txtEndDate.Text != "")
                        {
                            if (txtStartDate.Text == txtEndDate.Text)
                            {
                                timeTo = Convert.ToDateTime(txtStartDate.Text, culture).AddDays(1);
                            }
                            else
                            {
                                timeTo = Convert.ToDateTime(txtEndDate.Text, culture).AddDays(1);
                            }
                        }
                        else
                        {
                            timeTo = timeFrom.AddDays(1);
                        }
                    }

                    listRecord = (
                                       from a in db.Staff_Fluctuations
                                       join b in db.Tbl_Salon on a.SalonCurrentId equals b.Id into temp
                                       from b in temp.DefaultIfEmpty()
                                       join c in db.Tbl_Salon on a.SalonChangeId equals c.Id into temp1
                                       from c in temp1.DefaultIfEmpty()
                                       join status in db.Tbl_Status on a.StatusId.Value equals status.Id
                                       join type in db.Staff_Type on a.StaffType.Value equals type.Id
                                       join staff in db.Staffs on a.StaffId equals staff.Id
                                       join skill in db.Tbl_SkillLevel on staff.SkillLevel equals skill.Id
                                       where
                                       a.IsDetele == false
                                       && ((a.CreatedDate >= timeFrom) && (a.CreatedDate <= timeTo))
                                       && ((a.StaffId == staffId) || (staffId == 0))
                                       && ((a.SalonCurrentId == salonId) || (salonId == 0))
                                       && ((a.StatusId == statusId) || (statusId == 0))
                                       && ((a.StaffType == staffType) || (staffType == 0))
                                       select new cls_Staff_Fluctuation
                                       {
                                           Id = a.Id,
                                           Level= skill.Name,
                                           CreateDate = a.CreatedDate.Value,
                                           StatusName = status.Name,
                                           StatusId = a.StatusId,
                                           StaffId = a.StaffId.Value,
                                           StaffName = staff.Fullname,
                                           SalonCurrent = b.Name,
                                           SalonChange = c.Name,
                                           staffDescription = a.Description,
                                           TypeName = type.Name,
                                           NgayTinhThamNien = staff.NgayTinhThamNien
                                       }
                                     ).OrderByDescending(w => w.Id).ToList();
                    Bind_Paging(listRecord.Count);
                    RptFluctuation.DataSource = listRecord.Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                    RptFluctuation.DataBind();
                }

            }
            catch (Exception ex) { }
            return listRecord;
        }

        [WebMethod]
        public static object updateNote(int Id, string Value)
        {
            try
            {
                //Hàm update đánh giá của TSL
                using (var db = new Solution_30shineEntities())
                {
                    _30shine.MODEL.ENTITY.EDMX.Staff_Fluctuations updateNote = (from c in db.Staff_Fluctuations
                                                                                where c.Id == Id
                                                                                select c).FirstOrDefault();
                    if (updateNote != null)
                    {
                        updateNote.Description = Value;
                        updateNote.IsDetele = false;
                        updateNote.ModifiedTime = DateTime.Now;
                        db.SaveChanges();
                    }
                    return updateNote;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        protected void Bind_Paging(int TotalRow)
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(TotalRow) : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();
            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage(int TotalRow)
        {
            TotalRow -= PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        }
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }
        protected void _BtnClick(object sender, EventArgs e)
        {
            bindData();
            RemoveLoading();
        }

        /// <summary>
        /// Export to excel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Exc_ExportExcel(object sender, EventArgs e)
        {
            var data = bindData();
            var _ExcelHeadRow = new List<string>();
            var StaffRowLST = new List<List<string>>();
            var StaffRow = new List<string>();
            _ExcelHeadRow.Add("ID");
            _ExcelHeadRow.Add("Tên nhân viên");
            _ExcelHeadRow.Add("Bộ phận");
            _ExcelHeadRow.Add("Bậc");
            _ExcelHeadRow.Add("Salon hiện tại");
            _ExcelHeadRow.Add("Salon thay đổi");
            _ExcelHeadRow.Add("Hình thức");
            _ExcelHeadRow.Add("Ngày biến động");
            _ExcelHeadRow.Add("Ngày tính thâm niên");
            _ExcelHeadRow.Add("Ghi chú");
            foreach (var item in data)
            {
                StaffRow = new List<string>();
                StaffRow.Add(item.StaffId.ToString());
                StaffRow.Add(item.StaffName);
                StaffRow.Add(item.TypeName);
                StaffRow.Add(item.Level);
                StaffRow.Add(item.SalonCurrent);
                StaffRow.Add(item.SalonChange);
                StaffRow.Add(item.StatusName);
                StaffRow.Add(string.Format("{0:yyyy/MM/dd}", item.CreateDate));
                StaffRow.Add(ConvertDateToInt(item.NgayTinhThamNien, item.CreateDate, item.StatusId));
                StaffRow.Add(item.staffDescription);
                StaffRowLST.Add(StaffRow);
            }
            // export
            var ExcelStorePath = Server.MapPath("~") + "Public/Excel/Nhan.Vien/";
            var FileName = "Bien_dong_nhan_su_" + string.Format("{0:dd_MM_yyyy_hh_mm_ss}", DateTime.Now) + ".xlsx";
            var UrlFileName = "http://" + Request.Url.Host + "/Public/Excel/Nhan.Vien/" + FileName;
            ScriptManager.RegisterStartupScript(BtnFakeExcel, this.GetType(), "Download Excel", "CallBackExportExcel('" + UrlFileName + "');UP_FormatPrice('.be-report-price'); SubTableFixHeight();SetActiveTableListing();", true);
            ExportXcel(_ExcelHeadRow, StaffRowLST, ExcelStorePath + FileName);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_ExcelHeadRow"></param>
        /// <param name="List"></param>
        /// <param name="Path"></param>
        private static void ExportXcel(List<string> _ExcelHeadRow, List<List<string>> List, string Path)
        {
            try
            {
                CreateExcelFile.ExcelHeadRow = _ExcelHeadRow;
                CreateExcelFile.CreateExcel(List, Path);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi. Không tạo được file excel.\r\nException: " + ex.Message);
            }
        }

        public void Repeater_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
              || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (Perm_Delete == true)
                    ((Panel)(e.Item.FindControl("EAdelete"))).Visible = true;
            }
        }

        /// <summary>
        /// Convert Datetime to int
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public string ConvertDateToInt(object NgayThamNien, object NgayBienDong, object statusId)
        {
            string days = "";
            int day = 0;
            if (Convert.ToInt32(statusId) == 11 || Convert.ToInt32(statusId) == 12)
            {
                if (NgayThamNien != null && NgayThamNien != "")
                {
                    DateTime ngaythamnien = Convert.ToDateTime(NgayThamNien, new CultureInfo("vi-VN"));
                    DateTime ngaybiendong = Convert.ToDateTime(NgayBienDong, new CultureInfo("vi-VN"));
                    day = ngaybiendong.Subtract(ngaythamnien).Days;
                    days = day.ToString();
                }
                else
                {
                    days = "";
                }
            }
            return days;
        }
    }
    public partial class cls_Status
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public partial class cls_Staff_Fluctuation
    {
        public int Id { get; set; }
        public string Level { get; set; }
        public DateTime CreateDate { get; set; }
        public string StatusName { get; set; }
        public byte? StatusId { get; set; }
        public int StaffId { get; set; }
        public string StaffName { get; set; }
        public string SalonCurrent { get; set; }
        public string SalonChange { get; set; }
        public string staffDescription { get; set; }
        public string TypeName { get; set; }
        public DateTime? NgayTinhThamNien { get; set; }
        public double? DateNumber { get; set; }
    }


}