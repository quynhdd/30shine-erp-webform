﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.CustomClass;
using System.Web.Services;
using System.Data.Entity.Migrations;

namespace _30shine.GUI.BackEnd.UIStaff
{
    public partial class LabourContractListing : System.Web.UI.Page
    {
        private bool Perm_Access;
        private bool Perm_AllPermission;
        protected Paging PAGING = new Paging();
        IContract ContractModel = new ContractModel();

        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                ////Perm_HidePermission = permissionModel.GetActionByActionNameAndPageId("Perm_HidePermission", PageID, permission);
                ////Perm_Add = permissionModel.GetActionByActionNameAndPageId("Perm_Add", PageID, permission);
                //Perm_AllPermission = permissionModel.GetActionByActionNameAndPageId("Perm_AllPermission", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_AllPermission = permissionModel.CheckPermisionByAction("Perm_AllPermission", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindContractType();
                SetPermission();
                Bind_TypeStaff();
            }
        }
        /// <summary>
        /// Bind loại hợp đồng
        /// </summary>
        public void BindContractType()
        {
            using (var db = new Solution_30shineEntities())
            {
                var LST = db.Tbl_Config.Where(r => r.Key == "hop_dong" && r.IsDelete == 0).ToList();
                var Key = 0;
                ddlContractType.DataTextField = "TypeContract";
                ddlContractType.DataValueField = "Id";
                System.Web.UI.WebControls.ListItem item = new System.Web.UI.WebControls.ListItem("Chọn loại hợp đồng", "0");
                ddlContractType.Items.Insert(0, item);
                if (LST.Count > 0)
                {
                    foreach (var v in LST)
                    {
                        Key++;
                        item = new System.Web.UI.WebControls.ListItem(v.Label, v.Value);
                        ddlContractType.Items.Insert(Key, item);
                    }
                    ddlContractType.SelectedIndex = 0;
                }
            }
        }
        public void BindContractData()
        {
            int interger;
            var list = new List<ContractClass>();
            var deparmentId = int.TryParse(ddlTypeStaff.SelectedValue, out interger) ? interger : 0;
            var contractType = int.TryParse(ddlContractType.SelectedValue, out interger) ? interger : 0;

            var result = ContractModel.Getlist(contractType, deparmentId);
            if (result.success)
            {
                list = (List<ContractClass>)result.data;
                Bind_Paging(list.Count());
                Rpt.DataSource = list.Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                Rpt.DataBind();
            }
            else
            {
                WaringClient(result.msg);
            }
        }
        /// <summary>
        /// Paging
        /// </summary>
        /// <param name="TotalRow"></param>
        protected void Bind_Paging(int TotalRow)
        {
            //init Paging value
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(TotalRow) : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        /// <summary>
        /// Get_TotalPage
        /// </summary>
        /// <param name="TotalRow"></param>
        /// <returns></returns>
        protected int Get_TotalPage(int TotalRow)
        {
            TotalRow -= PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        }
        /// <summary>
        /// Gửi thông tin cảnh báo về client
        /// </summary>
        /// <param name="message"></param>
        public void WaringClient(string message)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Message System", "showMsgSystem('" + message + "','" + "msg-system success" + "'," + 15000 + ");", true);
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            BindContractData();
            RemoveLoading();
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }
        /// <summary>
        /// Bind department
        /// </summary>
        public void Bind_TypeStaff()
        {
            using (var db = new Solution_30shineEntities())
            {
                var LST = db.Staff_Type.Where(w => w.IsDelete != 1 && w.Publish == true).OrderBy(o => o.Order).ToList();
                var Key = 0;
                ddlTypeStaff.DataTextField = "TypeStaff";
                ddlTypeStaff.DataValueField = "Id";

                System.Web.UI.WebControls.ListItem item = new System.Web.UI.WebControls.ListItem("Chọn bộ phận", "0");
                ddlTypeStaff.Items.Insert(0, item);

                if (LST.Count > 0)
                {
                    foreach (var v in LST)
                    {
                        Key++;
                        item = new System.Web.UI.WebControls.ListItem(v.Name, v.Id.ToString());
                        ddlTypeStaff.Items.Insert(Key, item);
                    }
                    ddlTypeStaff.SelectedIndex = 0;
                }
            }
        }
        public string GetContractType(string contractType)
        {
            string result = "";
            switch (contractType)
            {
                case "3_thang": result = "3 tháng"; break;
                case "6_thang": result = "6 tháng"; break;
                case "lao_dong": result = "lao_dong"; break;
                case "thu_viec": result = "thử việc"; break;
            }
            return result;
        }
        [WebMethod]
        public static object DeleteContract(int Id)
        {
            _30shine.Helpers.Msg result = new _30shine.Helpers.Msg();
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var record = db.Contracts.Where(r => r.Id == Id && r.IsDelete == false).FirstOrDefault();
                    if (record != null)
                    {
                        record.IsDelete = true;
                        db.Contracts.AddOrUpdate(record);
                        var exc = db.SaveChanges();
                        if (exc >0 )
                        {
                            result.success = true;
                            result.msg = "Thành công!";
                        }
                        else
                        {
                            result.success = false;
                            result.msg = "Không lưu được data";
                        }
                    }
                    else
                    {
                        result.success = false;
                        result.msg = "Không tìm thấy dữ liệu";
                    }
                }
            }
            catch (Exception ex)
            {
                result.success = false;
                result.msg = "Đã có lỗi xảy ra!";
            }
            return result;
        }
    }
}