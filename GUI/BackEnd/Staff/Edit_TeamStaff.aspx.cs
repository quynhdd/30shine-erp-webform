﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using LinqKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.UIStaff
{
    public partial class Edit_TeamStaff : System.Web.UI.Page
    {
        private string PageID = "QL_NS_CH_GROUP";
        protected Paging PAGING = new Paging();
        private static Solution_30shineEntities db;
        private Expression<Func<Staff, bool>> Where = PredicateBuilder.True<Staff>();
        private bool Perm_Access = false;
        private bool Perm_ViewAllData = false;
        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        private bool Perm_ShowSalon = false;
        protected bool Perm_Add = false;
        private static Edit_TeamStaff instance;

        public Edit_TeamStaff()
        {
            db = new Solution_30shineEntities();
        }

        /// <summary>
        /// Get Edit_TeamStaff instance
        /// </summary>
        /// <returns></returns>
        public static Edit_TeamStaff getInstance()
        {
            if (!(Edit_TeamStaff.instance is Edit_TeamStaff))
            {
                return new Edit_TeamStaff();
            }
            else
            {
                return Edit_TeamStaff.instance;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                Library.Function.bindSalon(new List<DropDownList> { Salon }, Perm_ViewAllData);
                BindStaffType();
                BindDataStaff();
            }
        }
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Add = permissionModel.CheckPermisionByAction("Perm_Add", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }



        private void ExecuteByPermission()
        {

            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        private void BindStaffType()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Staff_Type.Where(w => w.IsDelete == 0 && (w.Id == 1 || w.Id == 2)).OrderBy(o => o.Id).ToList();
                if (lst.Count > 0)
                {
                    for (int i = 0; i < lst.Count; i++)
                    {
                        StaffType.Items.Add(new ListItem(lst[i].Name, lst[i].Id.ToString()));
                    }
                    StaffType.DataBind();
                }
            }
        }

        public List<Temp_Staff> GetData(int salonId, int departmentId)
        {
            try
            {
                var DateTimeNow = DateTime.Now.Date;
                var listStaff = new List<Staff>();
                var lstMain = new List<Temp_Staff>();
                var lstIdOther = new List<int>();
                var staff = db.Staffs.Where(w => ((w.SalonId == salonId) || (salonId == 0)) && w.Type == departmentId && w.Active == 1 && w.IsDelete == 0).ToList();
                var list = db.FlowTimeKeepings.Where(w => ((w.SalonId == salonId) || (salonId == 0)) && w.WorkDate == DateTimeNow && w.IsEnroll == true && w.IsDelete == 0).ToList();
                if (salonId > 0)
                {
                    var lstId = staff.Select(s => s.Id);
                    var lstSalonId = staff.Select(s => s.SalonId);
                    var listT = db.FlowTimeKeepings.Where(w => lstId.Contains(w.StaffId) && !lstSalonId.Contains(w.SalonId) && w.WorkDate == DateTimeNow && w.IsEnroll == true && w.IsDelete == 0).ToList();
                    lstIdOther = listT.Select(s => s.StaffId).ToList();
                }
                if (list.Count > 0)
                {
                    var lstIds = list.Select(s => s.StaffId);
                    listStaff = db.Staffs.Where(w => lstIds.Contains(w.Id) && w.SalonId != salonId && w.Type == departmentId).ToList();
                }
                if (lstIdOther.Count > 0)
                {
                    lstMain = staff.Where(w => !lstIdOther.Contains(w.Id)).Union(listStaff).Distinct().Select(s => new Temp_Staff
                    {
                        Id = s.Id,
                        Name = s.Fullname,
                        GroupLevel = s.GroupLevelId,
                        TeamId = s.TeamId
                    }).ToList();
                }
                else
                {
                    lstMain = staff.Union(listStaff).Distinct().Select(s => new Temp_Staff
                    {
                        Id = s.Id,
                        Name = s.Fullname,
                        GroupLevel = s.GroupLevelId,
                        TeamId = s.TeamId
                    }).ToList();
                }
                return lstMain;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private void BindDataStaff()
        {
            int salonId = int.TryParse(Salon.SelectedValue, out int salon) ? salon : 0;
            int departmentId = int.TryParse(StaffType.SelectedValue, out int department) ? department : 0;
            var data = GetData(salonId, departmentId);
            Bind_Paging(data.Count);
            RptStaff.DataSource = data.Skip(PAGING._Offset).Take(PAGING._Segment).OrderBy(o => o.TeamId).ThenBy(o => o.GroupLevel).ToList();
            RptStaff.DataBind();
        }
        protected void Bind_Paging(int TotalRecord)
        {
            // init Paging value            
            PAGING._Segment = 30;
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(TotalRecord) : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }
        protected int Get_TotalPage(int TotalRecord)
        {
            int TotalRow = TotalRecord - PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;

        }
        protected void _BtnClick(object sender, EventArgs e)
        {
            BindDataStaff();
            RemoveLoading();

        }
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        /// <summary>
        /// Cập nhật GroupLevelId, TeamId
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="group"></param>
        /// <param name="team"></param>
        /// <returns></returns>
        [WebMethod]
        public static int DeleteTeamStaff(int Id, int group, int team)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    int result = 0; //thất bại
                    if (Id > 0)
                    {
                        var staff = db.Staffs.FirstOrDefault(a => a.Id == Id);
                        if (staff != null)
                        {
                            staff.GroupLevelId = group;
                            staff.TeamId = team;
                            staff.ModifiedDate = DateTime.Now;
                            db.SaveChanges();
                            result = 1; //thành công
                        }
                    }
                    return result;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Temp_Staff> GetDataUpdate(int staffId, int salonId, int departmentId, int team)
        {
            try
            {
                var DateTimeNow = DateTime.Now.Date;
                var listStaff = new List<Staff>();
                var lstMain = new List<Temp_Staff>();
                var lstIdOther = new List<int>();
                var staff = db.Staffs.Where(w => ((w.SalonId == salonId) || (salonId == 0)) && w.Type == departmentId && w.Active == 1 && w.IsDelete == 0 && w.TeamId == team).ToList();
                var list = db.FlowTimeKeepings.Where(w => ((w.SalonId == salonId) || (salonId == 0)) && w.WorkDate == DateTimeNow && w.IsEnroll == true && w.IsDelete == 0 && w.StaffId != staffId).ToList();
                if (salonId > 0)
                {
                    var lstId = staff.Select(s => s.Id);
                    var lstSalonId = staff.Select(s => s.SalonId);
                    var listT = db.FlowTimeKeepings.Where(w => lstId.Contains(w.StaffId) && !lstSalonId.Contains(w.SalonId) && w.WorkDate == DateTimeNow && w.IsEnroll == true && w.IsDelete == 0 && w.StaffId != staffId).ToList();
                    lstIdOther = listT.Select(s => s.StaffId).ToList();
                }
                if (list.Count > 0)
                {
                    var lstIds = list.Select(s => s.StaffId);
                    listStaff = db.Staffs.Where(w => lstIds.Contains(w.Id) && w.SalonId != salonId && w.Type == departmentId && w.TeamId == team).ToList();
                }
                if (lstIdOther.Count > 0)
                {
                    lstMain = staff.Where(w => !lstIdOther.Contains(w.Id)).Union(listStaff).Distinct().Select(s => new Temp_Staff
                    {
                        Id = s.Id,
                        Name = s.Fullname,
                        GroupLevel = s.GroupLevelId,
                        TeamId = s.TeamId
                    }).ToList();
                }
                else
                {
                    lstMain = staff.Union(listStaff).Distinct().Select(s => new Temp_Staff
                    {
                        Id = s.Id,
                        Name = s.Fullname,
                        GroupLevel = s.GroupLevelId,
                        TeamId = s.TeamId
                    }).ToList();
                }
                return lstMain;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Cập nhật GroupLevelId, TeamId
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="group"></param>
        /// <param name="team"></param>
        /// <returns></returns>
        [WebMethod]
        public static int UpdateTeamStaff(int Id, int salonId, int departmentId, int group, int team)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    int result = 0; //thất bại
                    if (Id > 0)
                    {
                        var staff = db.Staffs.FirstOrDefault(a => a.Id == Id);
                        if (staff != null)
                        {
                            if (departmentId == 1)
                            {
                                if (getInstance().GetDataUpdate(staff.Id, salonId, departmentId, team).Count >= 2)
                                {
                                    return result = 1; //đủ người
                                }
                            }
                            staff.GroupLevelId = group;
                            staff.TeamId = team;
                            staff.ModifiedDate = DateTime.Now;
                            db.SaveChanges();
                            result = 2; //thành công
                        }
                    }
                    return result;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public class Temp_Staff
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public int? GroupLevel { get; set; }
            public int? TeamId { get; set; }
        }
    }
}