﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Linq.Expressions;
using LinqKit;
using System.Web.Services;
using Library;
using System.Data.Entity.Migrations;
using System.Web.Script.Serialization;
using System.Globalization;
using _30shine.MODEL.BO;
using _30shine.MODEL.IO;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;
using ExportToExcel;

namespace _30shine.GUI.BackEnd.UIStaff
{
    public partial class Staff_Listing : System.Web.UI.Page
    {
        protected Paging PAGING = new Paging();
        private Expression<Func<Staff, bool>> Where = PredicateBuilder.True<Staff>();
        private bool Perm_Access = false;
        private bool Perm_ViewAllData = false;
        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        protected bool Perm_Add = false;
        protected int SalonId;
        IStaffModel staffModel = new StaffModel();
        public int IsActive, IsTempLeave, IsLeave, TotalStaff;

        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                pageId = permissionModel.GetRegexPageIdSpecial().Replace(pageId, ".html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_Add = permissionModel.CheckPermisionByAction("Perm_Add", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }

        }
        public static Library.SendEmail sendMail = new Library.SendEmail();

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                Library.Function.bindSalon(new List<DropDownList> { Salon }, Perm_ViewAllData);
                Bind_StaffType();
                Bind_Skilllevel();
                Bind_StaffbyDay();
                Bind_ListPermission();
                Bind_StaffbyMonth();
                BindContracts();
                Bind_ListPermission();
                BindStaffProfile();
                BindRegion();
                RemoveLoading();
            }
            else
            {
                //
            }
        }
        /// <summary>
        /// Bind data to Rpt gọi dbModel từ class StaffModel trong Folder BO
        /// </summary>
        private void BindData()
        {
            int integer;
            DateTime TimeFrom = DateTime.MinValue;
            DateTime TimeTo = String.IsNullOrEmpty(txtTimeTo.Text) ? DateTime.MaxValue : Convert.ToDateTime(txtTimeTo.Text, new CultureInfo("vi-VN"));
            int salonId = int.TryParse(Salon.SelectedValue, out integer) ? integer : 0;
            int staffTypeId = int.TryParse(StaffType.SelectedValue, out integer) ? integer : 0;
            int skillLevelId = int.TryParse(SkillLevel.SelectedValue, out integer) ? integer : 0;
            int active = int.TryParse(Active.SelectedValue, out integer) ? integer : 0;
            int snDay = int.TryParse(SnDay.SelectedValue, out integer) ? integer : 0;
            int snMonth = int.TryParse(SnMonth.SelectedValue, out integer) ? integer : 0;
            int contract = int.TryParse(ddlContract.SelectedValue, out integer) ? integer : 0;
            int profile = int.TryParse(ddlStaffProfile.SelectedValue, out integer) ? integer : 0;
            int regionId = int.TryParse(ddlRegion.SelectedValue, out integer) ? integer : 0;
            string filterEmail = FilterEmail.Text;
            int permission = int.TryParse(ddlPermission.SelectedValue, out integer) ? integer : 0;
            //Lay list salonId theo dieu kien truyen vao
            var listSalonId = new List<int>();
            using (var db = new Solution_30shineEntities())
            {
                if (salonId == 0 && regionId == 0)
                {
                    listSalonId = db.Tbl_Salon.Where(r => r.IsDelete == 0 && r.Publish == true).Select(r => r.Id).ToList();
                    listSalonId.Add(0);
                }
                else if (regionId > 0)
                {
                    listSalonId = db.PermissionSalonAreas.Where(r => r.IsDelete == false && r.IsActive == true && r.StaffId == regionId).Select(r => r.SalonId ?? 0).ToList();
                }
                else if (salonId > 0 && regionId == 0)
                {
                    listSalonId = new List<int>() { salonId };
                }
            }
            if (TimeTo.Date != DateTime.MaxValue.Date)
            {
                TimeTo = TimeTo.AddDays(1);
            }
            GetStaffStatisticIsActive(TimeFrom, TimeTo, listSalonId, staffTypeId);
            var list = staffModel.GetListStaff(TimeFrom, TimeTo, listSalonId, staffTypeId, skillLevelId, active, snDay, snMonth, filterEmail, permission, contract, profile).ToList();
            Bind_Paging(list.Count);
            RptStaff.DataSource = list.Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
            RptStaff.DataBind();
        }

        public void GetStaffStatisticIsActive(DateTime from, DateTime to, List<int> listSalonId, int departmentId)
        {
            using (var db = new Solution_30shineEntities())
            {
                DateTime minDate = DateTime.MinValue;
                var list = db.Staffs.Where(w => w.NgayTinhThamNien >= from && w.NgayTinhThamNien < to &&
                                                 w.NgayTinhThamNien != null && w.NgayTinhThamNien != minDate &&
                                                 listSalonId.Contains(w.SalonId.Value) && w.isAccountLogin == 0 &&
                                                 ((w.Type == departmentId) || (departmentId == 0)) && w.IsDelete == 0
                                           ).ToList();
                if (list.Count > 0)
                {
                    TotalStaff = list.Count();
                    IsActive = list.Where(s => s.Active == 1).Select(s => s.Id).Count();
                    IsTempLeave = list.Where(s => s.Active == 2).Select(s => s.Id).Count();
                    IsLeave = list.Where(s => s.Active == 0 || s.Active == null).Select(s => s.Id).Count();
                }
                else
                {
                    IsActive = 0;
                    IsTempLeave = 0;
                    IsLeave = 0;
                }
            }
        }

        /// <summary>
        ///Get Name Nguồn 
        /// </summary>
        public string GetValue(int Id)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var list = db.TuyenDung_Nguon.Where(w => w.Id == Id).ToList();
                    foreach (var v in list)
                    {
                        return v.Name.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return "";
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            BindData();
            RemoveLoading();

        }
        /// <summary>
        /// Phân trang
        /// </summary>
        /// <param name="TotalRow"></param>
        protected void Bind_Paging(int TotalRow)
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(TotalRow) : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage(int TotalRow)
        {
            TotalRow -= PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        }
        private void Bind_StaffType()
        {
            using (var db = new Solution_30shineEntities())
            {
                ListItem item = new ListItem("--Chọn bộ phận--", "0");
                StaffType.Items.Insert(0, item);

                var lst = db.Staff_Type.Where(w => w.IsDelete != 1).OrderBy(o => o.Id).ToList();
                if (lst.Count > 0)
                {
                    for (int i = 0; i < lst.Count; i++)
                    {
                        StaffType.Items.Add(new ListItem(lst[i].Name, lst[i].Id.ToString()));
                    }
                    StaffType.DataBind();
                }
            }
        }
        private void BindRegion()
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var listStaffRegion = db.Staffs.Where(r => r.Type == 17 && r.IsDelete == 0).ToList();
                    if (listStaffRegion.Any())
                    {
                        var record = new Staff();
                        record.Fullname = "Chọn vùng ASM";
                        record.Id = 0;
                        listStaffRegion.Insert(0, record);

                        ddlRegion.DataTextField = "FullName";
                        ddlRegion.DataValueField = "Id";
                        ddlRegion.DataSource = listStaffRegion;
                        ddlRegion.DataBind();
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Bind toàn bộ hợp đồng của nhân viên
        /// </summary>
        private void BindContracts()
        {
            using (var db = new Solution_30shineEntities())
            {
                ddlContract.Items.Insert(0, new ListItem("--Chọn hợp đồng--", "0"));
                ddlContract.Items.Insert(1, new ListItem("Chưa có hợp đồng", "-1"));
                List<Tbl_Config> lst = db.Tbl_Config.Where(w => w.IsDelete == 0 && w.Key == "hop_dong").ToList();
                if (lst.Count > 0)
                {
                    for (int i = 0; i < lst.Count; i++)
                    {
                        ddlContract.Items.Add(new ListItem(lst[i].Label, lst[i].Value));
                    }
                    ddlContract.DataBind();
                }
            }
        }

        /// <summary>
        /// Bind toàn bộ hồ sơ nhân viên
        /// </summary>
        private void BindStaffProfile()
        {
            using (var db = new Solution_30shineEntities())
            {
                ListItem item = new ListItem("--Chọn hồ sơ--", "0");
                ddlStaffProfile.Items.Insert(0, item);

                var lst = db.Tbl_Status.Where(w => w.IsDelete == false && w.ParentId == 19).OrderBy(o => o.Id).ToList();
                if (lst.Count > 0)
                {
                    for (int i = 0; i < lst.Count; i++)
                    {
                        ddlStaffProfile.Items.Add(new ListItem(lst[i].Name, lst[i].Value.ToString()));
                    }
                    ddlStaffProfile.DataBind();
                }
            }
        }

        /// <summary>
        /// bind staff by filter day of birth 
        /// </summary>
        private void Bind_StaffbyDay()
        {
            using (var db = new Solution_30shineEntities())
            {
                ListItem item = new ListItem("--Chọn ngày sinh--", "0");
                SnDay.Items.Insert(0, item);

                for (int i = 1; i <= 31; i++)
                {
                    SnDay.Items.Add(new ListItem(Convert.ToString(i), i.ToString()));
                }
            }
        }

        /// <summary>
        /// by staff by filter month of birth
        /// </summary>
        private void Bind_StaffbyMonth()
        {
            using (var db = new Solution_30shineEntities())
            {
                ListItem item = new ListItem("--Chọn tháng sinh--", "0");
                SnMonth.Items.Insert(0, item);

                for (int i = 1; i <= 12; i++)
                {
                    SnMonth.Items.Add(new ListItem(Convert.ToString(i), i.ToString()));
                }
            }
        }

        /// <summary>
        /// Bind SkillLevel
        /// </summary>
        private void Bind_Skilllevel()
        {
            using (var db = new Solution_30shineEntities())
            {
                ListItem item = new ListItem("--Chọn bậc kỹ năng--", "0");
                SkillLevel.Items.Insert(0, item);
                var lst = db.Tbl_SkillLevel.Where(w => w.IsDelete != 1).OrderBy(o => o.Id).ToList();
                if (lst.Count > 0)
                {
                    for (int i = 0; i < lst.Count; i++)
                    {
                        SkillLevel.Items.Add(new ListItem(lst[i].Name, lst[i].Id.ToString()));
                    }
                    SkillLevel.DataBind();
                }
            }
        }

        /// <summary>
        /// Bind Permission
        /// </summary>
        private void Bind_ListPermission()
        {
            using (var db = new Solution_30shineEntities())
            {
                ListItem item = new ListItem("--Chọn nhóm quyền--", "0");
                ddlPermission.Items.Insert(0, item);
                var lst = db.PermissionErps.Where(w => w.IsDelete == false).OrderBy(o => o.Id).ToList();
                if (lst.Count > 0)
                {
                    for (int i = 0; i < lst.Count; i++)
                    {
                        ddlPermission.Items.Add(new ListItem(lst[i].Name, lst[i].Id.ToString()));
                    }
                    ddlPermission.DataBind();
                }
            }
        }

        /// <summary>
        /// Lấy danh sách skill
        /// </summary>
        /// <param name="staffId"></param>
        /// <returns></returns>
        [WebMethod]
        public static object getSkillList(int staffId)
        {
            var message = new Library.Class.cls_message();
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var data = new List<cls_skill_level>();
                    var item = new cls_skill_level();
                    var levelList = new List<cls_skill_level_item>();
                    var levelItem = new cls_skill_level_item();
                    var index = -1;
                    var list = db.Store_Staff_HairMode_LevelListing(staffId).ToList();
                    if (list.Count > 0)
                    {
                        foreach (var v in list)
                        {
                            index = data.FindIndex(w => w.SkillId == v.hairModeId);
                            if (index == -1)
                            {
                                item = new cls_skill_level();
                                item.SkillId = v.hairModeId;
                                item.SkillName = v.hairModeTitle;
                                item.SkillLevels = new List<cls_skill_level_item>();
                                item.Order = v.hairModeOrder.Value;
                                levelItem = new cls_skill_level_item();
                                levelItem.LevelId = v.hairModeLevelId.Value;
                                levelItem.LevelName = v.hairModeLevelTitle;
                                levelItem.Value = v.Value.Value;
                                levelItem.Point = v.Point.Value;
                                try
                                {
                                    levelItem.IsChecked = Convert.ToBoolean(v.IsChecked);
                                }
                                catch
                                {
                                    levelItem.IsChecked = false;
                                }

                                item.SkillLevels.Add(levelItem);
                                data.Add(item);
                            }
                            else
                            {
                                item = data[index];
                                levelItem = new cls_skill_level_item();
                                levelItem.LevelId = v.hairModeLevelId.Value;
                                levelItem.LevelName = v.hairModeLevelTitle;
                                levelItem.Value = v.Value.Value;
                                levelItem.Point = v.Point.Value;
                                try
                                {
                                    levelItem.IsChecked = Convert.ToBoolean(v.IsChecked);
                                }
                                catch
                                {
                                    levelItem.IsChecked = false;
                                }
                                item.SkillLevels.Add(levelItem);
                                data[index] = item;
                            }
                        }
                    }

                    message.success = true;
                    message.message = "succeeded";
                    message.data = data.OrderBy(w => w.Order);
                    return message;
                }
            }
            catch (Exception ex)
            {
                //// Get stack trace for the exception with source file information
                //var st = new StackTrace(ex, true);
                //// Get the top stack frame
                //var frame = st.GetFrame(0);
                //// Get the line number from the stack frame
                //var line = frame.GetFileLineNumber();
                string messageBody = @"<p>Url : ql.30shine.com/admin/nhan-vien.html<p></br>
                                       <p>Path : /GUI/BackEnd/Staff/Staff_Listing.aspx/getSkillList</p></br>
                                       <p>Exception : " + ex.Message + "</p>";

                sendMail.SendMail("vong.lv@hkphone.com.vn,nhat.nv@hkphone.com.vn", "Exception | ql.30shine.com/admin/nhan-vien.html", messageBody);

                message.success = false;
                message.message = "Đã có lỗi xảy ra! Vui lòng liên hệ nhóm phát triển!";
                return message;
            }
        }

        /// <summary>
        /// Set điểm kỹ năng cho nhân viên
        /// </summary>
        /// <param name="staffId"></param>
        /// <param name="skillId"></param>
        /// <param name="levelId"></param>
        /// <returns></returns>
        [WebMethod]
        public static object setSkillLevel(int staffId, int skillId, int levelId, bool isChecked)
        {
            var message = new Library.Class.cls_message();
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var record = db.Api_HairMode_Staff.FirstOrDefault(w => w.StaffId == staffId && w.HairModeId == skillId && w.HairModeLevelId == levelId);
                    if (record != null)
                    {
                        record.IsChecked = isChecked;
                        record.ModifiedTime = DateTime.Now;
                    }
                    else
                    {
                        record = new Api_HairMode_Staff();
                        record.StaffId = staffId;
                        record.HairModeId = skillId;
                        record.HairModeLevelId = levelId;
                        record.IsChecked = isChecked;
                        record.IsDelete = false;
                        record.Publish = true;
                        record.CreatedTime = DateTime.Now;
                    }
                    db.Api_HairMode_Staff.AddOrUpdate(record);
                    db.SaveChanges();

                    // Update level point
                    var staff = db.Staffs.FirstOrDefault(w => w.Id == staffId);
                    if (staff != null)
                    {
                        var list = db.Store_Staff_HairMode_LevelListing(staffId).ToList();
                        try
                        {
                            staff.LevelPoint = list.Sum(s => s.Point);
                        }
                        catch (Exception ex)
                        {
                            string messageBody = @"<p>Url : ql.30shine.com/admin/nhan-vien.html<p></br>
                                       <p>Path : /GUI/BackEnd/Staff/Staff_Listing.aspx/setSkillLevel</p></br>
                                       <p>Exception : " + ex.Message + "</p>";

                            sendMail.SendMail("vong.lv@hkphone.com.vn,nhat.nv@hkphone.com.vn", "Exception | ql.30shine.com/admin/nhan-vien.html", messageBody);

                            message.success = false;
                            message.message = "Đã có lỗi xảy ra! Vui lòng liên hệ nhóm phát triển!";
                            return message;
                        }
                        db.Staffs.AddOrUpdate(staff);
                        db.SaveChanges();
                    }

                    message.success = true;
                    message.message = "succeeded";
                    return message;
                }
            }
            catch (Exception ex)
            {
                //// Get stack trace for the exception with source file information
                //var st = new StackTrace(ex, true);
                //// Get the top stack frame
                //var frame = st.GetFrame(0);
                //// Get the line number from the stack frame
                //var line = frame.GetFileLineNumber();
                string messageBody = @"<p>Url : ql.30shine.com/admin/nhan-vien.html<p></br>
                                       <p>Path : /GUI/BackEnd/Staff/Staff_Listing.aspx/setSkillLevel</p></br>
                                       <p>Exception : " + ex.Message + "</p>";

                sendMail.SendMail("vong.lv@hkphone.com.vn,nhat.nv@hkphone.com.vn", "Exception | ql.30shine.com/admin/nhan-vien.html", messageBody);

                message.success = false;
                message.message = "Đã có lỗi xảy ra! Vui lòng liên hệ nhóm phát triển!";
                return message;
            }
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }
        /// <summary>
        /// Thông số tổng quát
        /// </summary>
        /// <param name="staffID"></param>
        /// <param name="timeForm"></param>
        /// <param name="timeTo"></param>
        /// <returns></returns>
        [WebMethod]
        public static string GetTotalResult(string timeFrom, string timeTo, int staffID)
        {
            using (var db = new Solution_30shineEntities())
            {
                var list = db.Store_Report_By_Time_V1(timeFrom, timeTo, staffID).ToList();
                return new JavaScriptSerializer().Serialize(list);
            }
        }
        /// <summary>
        /// GetTotalService
        /// </summary>
        /// <param name="staffID"></param>
        /// <param name="timeForm"></param>
        /// <param name="timeTo"></param>
        /// <returns></returns>
        [WebMethod]
        public static string GetTotalService(string timeFrom, string timeTo, int staffID)
        {
            using (var db = new Solution_30shineEntities())
            {
                var list = db.Store_Staff_TotalMoney_BillService_V1(timeFrom, timeTo, staffID).ToList();
                return new JavaScriptSerializer().Serialize(list);
            }
        }
        /// <summary>
        /// GetTotalMissTake
        /// </summary>
        /// <param name="staffID"></param>
        /// <param name="timeForm"></param>
        /// <param name="timeTo"></param>
        /// <returns></returns>
        [WebMethod]
        public static string GetTotalMisstake(string timeFrom, string timeTo, int staffID)
        {
            using (var db = new Solution_30shineEntities())
            {
                var list = db.Store_Total_Misstake_V1(timeFrom, timeTo, staffID).ToList();
                return new JavaScriptSerializer().Serialize(list);
            }
        }
        /// <summary>
        /// GetTotalHairStyle - ĐỘ đa dạng kieur tóc
        /// </summary>
        /// <param name="timeFrom"></param>
        /// <param name="timeTo"></param>
        /// <param name="staffID"></param>
        /// <returns></returns>
        [WebMethod]
        public static string GetTotalHairStyle(string timeFrom, string timeTo, int staffID)
        {
            using (var db = new Solution_30shineEntities())
            {
                var list = db.Store_Staff_TotalHairStyle_V1(timeFrom, timeTo, staffID).ToList();
                return new JavaScriptSerializer().Serialize(list);
            }
        }

        public void Repeater_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
              || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (Perm_Edit == true)
                    ((Panel)(e.Item.FindControl("EAedit"))).Visible = true;
                if (Perm_Delete == true)
                    ((Panel)(e.Item.FindControl("EAdelete"))).Visible = true;
            }
        }


        /// <summary>
        /// Export to excel
        /// Author: QuynhDD
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Exc_ExportExcel(object sender, EventArgs e)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    int integer;
                    DateTime TimeFrom = DateTime.MinValue;
                    DateTime TimeTo = String.IsNullOrEmpty(txtTimeTo.Text) ? DateTime.MaxValue : Convert.ToDateTime(txtTimeTo.Text, new CultureInfo("vi-VN"));
                    int salonId = int.TryParse(Salon.SelectedValue, out integer) ? integer : 0;
                    int staffTypeId = int.TryParse(StaffType.SelectedValue, out integer) ? integer : 0;
                    int skillLevelId = int.TryParse(SkillLevel.SelectedValue, out integer) ? integer : 0;
                    int active = int.TryParse(Active.SelectedValue, out integer) ? integer : 0;
                    int snDay = int.TryParse(SnDay.SelectedValue, out integer) ? integer : 0;
                    int snMonth = int.TryParse(SnMonth.SelectedValue, out integer) ? integer : 0;
                    int contract = int.TryParse(ddlContract.SelectedValue, out integer) ? integer : 0;
                    int profile = int.TryParse(ddlStaffProfile.SelectedValue, out integer) ? integer : 0;
                    string filterEmail = FilterEmail.Text;
                    int permission = int.TryParse(ddlPermission.SelectedValue, out integer) ? integer : 0;
                    string sql = $@"DECLARE
                                    @salonId int,
                                    @department int,
                                    @skillLevel int,
                                    @active INT,
                                    @day INT,
                                    @month INT,
                                    @email NVARCHAR(50),
						            @permission INT,
							        @contract INT,
							        @profile INT,
                                    @timefrom NVARCHAR(50),
                                    @timeto NVARCHAR(50)
                                    SET @salonId ='{salonId}';
                                    SET @department = '{staffTypeId}';
                                    SET @skillLevel = '{skillLevelId}';
                                    SET @active = '{active}';
                                    SET @day = '{snDay}';
                                    SET @month = '{snMonth}';
                                    SET @email = '{filterEmail}';
                                    SET @permission = '{permission}';
                                    SET @contract ='{contract}';
							        SET @profile = '{profile}';
                                    SET @timefrom = '{string.Format("{0:yyyy/MM/dd}", TimeFrom)}';
                                    SET @timeto = '{string.Format("{0:yyyy/MM/dd}", TimeTo)}';
                                    WITH tempStaff AS 
                                    (
	                                    SELECT staff.Id AS StaffId, staff.Fullname AS FullName,
			                                    ISNULL(staff.NameCMT,'') AS NameCMT,
			                                    ISNULL(staff.StaffID,'') AS NumberCMT,
			                             		ISNULL(CONVERT(varchar(20),staff.IDProvidedDate,103),'') as ProviderDate,
			                                    ISNULL(staff.IDProviderLocale,'') AS ProviderLocale,
			                                    ISNULL(salon.ShortName,'') as SalonName, 
			                                    ISNULL(skillLevel.Name,'') as LevelName,
			                                    ISNULL(department.Name,'') as DepartmentName,
												ISNULL(staff.[Address],'') as [Address],
												ISNULL(CONVERT(varchar(20),staff.DateJoin,103),'') as DateJoin,
			                                    ISNULL(staff.NumberInsurrance,'') AS NumberInsurrance,
			                                    ISNULL(staff.MST,'') AS TaxCode,
			                                    ISNULL(staff.NganHang_Ten,'') AS BankName,
			                                    ISNULL(staff.NganHang_SoTK,0) AS BankNumber,
			                                    ISNULL(CONCAT(staff.SN_day,'-',staff.SN_month,'-',staff.SN_year),'') as BirthDay,
			                                    CASE WHEN staff.Gender = 1 THEN N'Nam' WHEN staff.Gender = 2 THEN N'Nữ' WHEN staff.Gender = 3 THEN N'Khác' ELSE '' END AS Gender,
			                                    ISNULL(staff.Phone,'') AS StaffPhone,
			                                    ISNULL(staff.Email,'') AS StaffEmail,
			                                    CASE WHEN staff.Active = 0 THEN N'Đã nghỉ' WHEN staff.Active = 1 THEN N'Đang hoạt động' WHEN staff.Active = 2 THEN N'Nghỉ tạm thời' ELSE '' END AS [Status],
			                                    ISNULL([source].Name,'') AS RecruitmentSource,
			                                    CASE WHEN staff.isAccountLogin = 0 THEN N'Tài khoản nhân viên' WHEN staff.isAccountLogin = 1 THEN N'Tài khoản đăng nhập' ELSE '' END AS TypeAccount,
			                                    CASE WHEN staff.SalaryByPerson = 0 THEN N'Theo nhóm(Bộ phận)' WHEN staff.SalaryByPerson = 1 THEN N'Cá nhân' ELSE '' END AS SalaryByPerson,
			                                    CASE WHEN staff.RequireEnroll = 0 THEN N'Không' WHEN staff.RequireEnroll = 1 THEN N'Có' ELSE '' END AS RequireEnroll,
			                                    ISNULL(CONVERT(varchar(20),staff.NgayTinhThamNien,103),'') as SeniorityDate,
												ISNULL(CASE WHEN f.Id IS NOT NULL AND f.CreatedDate > staff.NgayTinhThamNien AND staff.Active = 0 THEN DATEDIFF(dd, staff.NgayTinhThamNien, f.CreatedDate) ELSE DATEDIFF(dd, staff.NgayTinhThamNien, GETDATE()) END,'') AS NumberSeniority,
			                                    ISNULL(config.Label, '') AS ContractsName,
								                mapcontracts.Active AS ContractsActive,
								                (SELECT Id FROM dbo.StaffProfileMap WHERE StatusProfileId = @profile AND StaffId = staff.Id AND IsDelete = 0) AS StaffProfileId,
		                                    -- set thông tin hồ sơ lý lịch đã nộp hay chưa
		                                    CASE WHEN st.ParentId = 19 AND st.Value = 1 AND pro.IsDelete=0 THEN 1 ELSE 0 END AS SoYeuLyLichtemp,
		                                    CASE WHEN st.ParentId = 19 AND st.Value = 2 AND pro.IsDelete=0  THEN 1 ELSE 0 END AS CMTCongChungtemp,
		                                    CASE WHEN st.ParentId = 19 AND st.Value = 3 AND pro.IsDelete=0  THEN 1 ELSE 0 END AS SoHoKhautemp,
		                                    CASE WHEN st.ParentId = 19 AND st.Value = 4 AND pro.IsDelete=0  THEN 1 ELSE 0 END AS GiayKhaiSinhtemp,
		                                    CASE WHEN st.ParentId = 19 AND st.Value = 5 AND pro.IsDelete=0  THEN 1 ELSE 0 END AS GiayKhamSucKhoetemp
		                                    from Staff as staff
		                                    LEFT JOIN Staff_Type as department on department.Id = staff.[Type]
		                                    LEFT JOIN dbo.TinhThanh AS city ON  city.ID = staff.CityId
		                                    LEFT JOIN Tbl_Salon as salon on salon.Id = staff.SalonId
		                                    LEFT JOIN Tbl_SkillLevel as skillLevel on skillLevel.Id = staff.SkillLevel
		                                    LEFT JOIN dbo.TuyenDung_Nguon AS [source] ON [source].id= staff.SourceId
		                                    LEFT JOIN dbo.StaffProfileMap AS pro ON pro.StaffId = staff.Id
		                                    LEFT JOIN dbo.Tbl_Status AS st ON st.Value = pro.StatusProfileId
		                                    LEFT JOIN dbo.StaffContractMap AS mapcontracts ON mapcontracts.StaffId = staff.Id 
							                LEFT JOIN dbo.Tbl_Config AS config ON mapcontracts.ContractId = TRY_PARSE(config.Value AS INT) AND config.[Key] = 'hop_dong'
							                LEFT JOIN dbo.PermissionStaff AS per ON per.StaffId = staff.Id
											LEFT JOIN dbo.Staff_Fluctuations AS f ON f.StaffId = staff.Id AND f.StatusId = 12 AND f.IsDetele = 0
		                                    WHERE staff.IsDelete = 0 AND staff.NgayTinhThamNien >= @timefrom AND staff.NgayTinhThamNien < @timeto 			
			                                    AND ((staff.SalonId = @salonId) or (@salonId =0 and staff.SalonId>0))
			                                    AND ((staff.[Type] = @department) or (@department = 0 and staff.[Type] > 0))
			                                    AND ((staff.SkillLevel= @skillLevel) or (@skillLevel = 0 and staff.SkillLevel > 0))
			                                    AND ((staff.SN_day = @day) or (@day = 0))
			                                    AND ((staff.SN_month = @month) or (@month = 0))
			                                    AND staff.Active = @active
								                AND ((per.PermissionId = @permission) or (@permission = 0))
							                    AND ((mapcontracts.ContractId = @contract) or (@contract = 0))
                                                AND ((staff.Email = @email) OR (@email = ''))
	                                        GROUP BY st.ParentId,st.Value,staff.Id,staff.Fullname,salon.ShortName,skillLevel.Name,department.Name,staff.[Address],staff.OrderCode,staff.Phone,staff.Email,
		                                    [source].Name,staff.NgayTinhThamNien,skillLevel.[Order],config.Value,config.Label,staff.NameCMT,staff.StaffID,staff.IDProvidedDate,
		                                    staff.IDProviderLocale,staff.DateJoin,staff.NumberInsurrance,staff.MST,staff.NganHang_Ten,staff.NganHang_SoTK,
		                                    staff.SN_day,staff.SN_month,staff.SN_year,staff.Gender,staff.Active,staff.isAccountLogin,staff.RequireEnroll,staff.SalaryByPerson,pro.IsDelete,mapcontracts.Active,f.Id, f.CreatedDate
		                                    )
                                   SELECT tempStaff.StaffId,tempStaff.FullName,tempStaff.NameCMT,tempStaff.NumberCMT,tempStaff.ProviderDate,tempStaff.ProviderLocale,
		                                    tempStaff.SalonName,tempStaff.LevelName,tempStaff.DepartmentName,tempStaff.[Address],tempStaff.DateJoin,tempStaff.NumberInsurrance,tempStaff.TaxCode,tempStaff.BankName,tempStaff.BankNumber,tempStaff.BirthDay,
		                                    tempStaff.Gender,tempStaff.StaffPhone,tempStaff.StaffEmail,tempStaff.RecruitmentSource,tempStaff.TypeAccount,
		                                    tempStaff.SalaryByPerson,tempStaff.RequireEnroll,tempStaff.SeniorityDate,tempStaff.NumberSeniority,tempStaff.ContractsName, tempStaff.[Status],
											
		                                    CASE WHEN SUM(tempStaff.SoYeuLyLichtemp) >= 1 THEN N'Có' ELSE 'Không' END AS SoYeuLL,
		                                    CASE WHEN SUM(tempStaff.CMTCongChungtemp) >= 1 THEN N'Có' ELSE 'Không' END AS CMTCongChung,
		                                    CASE WHEN SUM(tempStaff.SoHoKhautemp) >= 1 THEN N'Có' ELSE 'Không' END AS SoHoKhau,
		                                    CASE WHEN SUM(tempStaff.GiayKhaiSinhtemp) >= 1 THEN N'Có' ELSE 'Không' END AS GiayKhaiSinh,
		                                    CASE WHEN SUM(tempStaff.GiayKhamSucKhoetemp) >= 1 THEN N'Có' ELSE 'Không' END AS GiayKhamSK
		                            FROM tempStaff
							        WHERE ((tempStaff.ContractsActive = 1) OR (tempStaff.ContractsActive IS NULL)) AND tempStaff.StaffProfileId IS NULL
	                                GROUP BY tempStaff.StaffId,tempStaff.FullName,tempStaff.NameCMT,tempStaff.NumberCMT,tempStaff.ProviderDate,tempStaff.ProviderLocale,tempStaff.SalonName,
	                                    tempStaff.LevelName,tempStaff.DepartmentName,tempStaff.[Address],tempStaff.DateJoin,tempStaff.NumberInsurrance,tempStaff.TaxCode,tempStaff.BankName,tempStaff.BankNumber,tempStaff.BirthDay,
	                                    tempStaff.Gender,tempStaff.StaffPhone,tempStaff.StaffEmail,tempStaff.RecruitmentSource,tempStaff.TypeAccount,tempStaff.SalaryByPerson,tempStaff.RequireEnroll,tempStaff.SeniorityDate,tempStaff.NumberSeniority,tempStaff.ContractsName, tempStaff.[Status],tempStaff.StaffProfileId
                                    ORDER BY tempStaff.StaffId DESC";
                    var list = db.Database.SqlQuery<Output_ExportExcel>(sql).ToList();
                    var _ExcelHeadRow = new List<string>();
                    var RowLST = new List<List<string>>();
                    var Row = new List<string>();
                    _ExcelHeadRow.Add("ID NV");
                    _ExcelHeadRow.Add("Họ tên");
                    _ExcelHeadRow.Add("Tên theo CMT");
                    _ExcelHeadRow.Add("Số CMT");
                    _ExcelHeadRow.Add("Ngày cấp");
                    _ExcelHeadRow.Add("Nơi cấp");
                    _ExcelHeadRow.Add("Salon");
                    _ExcelHeadRow.Add("Level");
                    _ExcelHeadRow.Add("Bộ phận");
                    //_ExcelHeadRow.Add("Địa chỉ");
                    _ExcelHeadRow.Add("Ngày vào");
                    _ExcelHeadRow.Add("Số sổ bảo hiểm");
                    _ExcelHeadRow.Add("MST");
                    _ExcelHeadRow.Add("Tên ngân hàng");
                    _ExcelHeadRow.Add("Số TK");
                    _ExcelHeadRow.Add("Ngày sinh");
                    _ExcelHeadRow.Add("Giới tính");
                    _ExcelHeadRow.Add("Số ĐT");
                    _ExcelHeadRow.Add("Tài khoản ĐN");
                    _ExcelHeadRow.Add("Nguồn tuyển dụng");
                    _ExcelHeadRow.Add("Loại tài khoản");
                    _ExcelHeadRow.Add("Tính lương");
                    _ExcelHeadRow.Add("Điểm danh");
                    _ExcelHeadRow.Add("Ngày tính thâm niên");
                    _ExcelHeadRow.Add("Số ngày thâm niên");
                    _ExcelHeadRow.Add("Loại hợp đồng");
                    _ExcelHeadRow.Add("Tài khoản");
                    _ExcelHeadRow.Add("Sơ yếu lý lịch");
                    _ExcelHeadRow.Add("CMT công chứng");
                    _ExcelHeadRow.Add("Sổ hộ khẩu");
                    _ExcelHeadRow.Add("Giấy khai sinh");
                    _ExcelHeadRow.Add("Giấy khám SK");
                    if (list.Count > 0)
                    {
                        foreach (var item in list)
                        {
                            Row = new List<string>();
                            Row.Add(item.StaffId.ToString());
                            Row.Add(item.FullName);
                            Row.Add(item.NameCMT);
                            Row.Add(item.NumberCMT.ToString());
                            Row.Add(item.ProviderDate.ToString());
                            Row.Add(item.ProviderLocale);
                            Row.Add(item.SalonName);
                            Row.Add(item.LevelName);
                            Row.Add(item.DepartmentName);
                            //Row.Add(item.Address.ToString());
                            Row.Add(item.DateJoin.ToString());
                            Row.Add(item.NumberInsurrance);
                            Row.Add(item.TaxCode);
                            Row.Add(item.BankName);
                            Row.Add(item.BankNumber.ToString());
                            Row.Add(item.BirthDay);
                            Row.Add(item.Gender);
                            Row.Add(item.StaffPhone);
                            Row.Add(item.StaffEmail);
                            Row.Add(item.RecruitmentSource);
                            Row.Add(item.TypeAccount);
                            Row.Add(item.SalaryByPerson);
                            Row.Add(item.RequireEnroll);
                            Row.Add(item.SeniorityDate.ToString());
                            Row.Add(item.NumberSeniority.ToString());
                            Row.Add(item.ContractsName);
                            Row.Add(item.Status);
                            Row.Add(item.SoYeuLL);
                            Row.Add(item.CMTCongChung);
                            Row.Add(item.SoHoKhau);
                            Row.Add(item.GiayKhaiSinh);
                            Row.Add(item.GiayKhamSK);
                            RowLST.Add(Row);
                        }
                        // export
                        var ExcelStorePath = Server.MapPath("~") + @"Public\Excel\Nhan.Vien\";
                        var FileName = "Nhan_vien_" + string.Format("{0:dd_MM_yyyy_hh_mm_ss}", DateTime.Now) + ".xlsx";
                        var UrlFileName = "http://" + Request.Url.Host + "/Public/Excel/Nhan.Vien/" + FileName;
                        ExportXcel(_ExcelHeadRow, RowLST, ExcelStorePath + FileName);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Download Excel", "CallBackExportExcel('" + UrlFileName + "');UP_FormatPrice('.be-report-price'); SubTableFixHeight();SetActiveTableListing(); alert(\"Xuất excel thành công.\")", true);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi. Không tạo được file excel.\r\nException: " + ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_ExcelHeadRow"></param>
        /// <param name="List"></param>
        /// <param name="Path"></param>
        private static void ExportXcel(List<string> _ExcelHeadRow, List<List<string>> List, string Path)
        {
            try
            {
                CreateExcelFile.ExcelHeadRow = _ExcelHeadRow;
                CreateExcelFile.CreateExcel(List, Path);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi. Không tạo được file excel.\r\nException: " + ex.Message);
            }
        }
        public class cls_staffL : Staff
        {
            public string salonName { get; set; }
            public string departmentName { get; set; }
            public string skillLevelName { get; set; }
        }

        /// <summary>
        /// Class phục vụ set điểm kỹ năng cho nhân viên
        /// </summary>
        public class cls_skill_level
        {
            public int SkillId { get; set; }
            public string SkillName { get; set; }
            public List<cls_skill_level_item> SkillLevels { get; set; }
            public int Order { get; set; }
        }

        /// <summary>
        /// Class phục vụ set điểm kỹ năng cho nhân viên
        /// </summary>
        public class cls_skill_level_item
        {
            public int LevelId { get; set; }
            public string LevelName { get; set; }
            public int Value { get; set; }
            public int Point { get; set; }
            public bool IsChecked { get; set; }
        }

        /// <summary>
        /// Output export excel
        /// </summary>
        public class Output_ExportExcel
        {
            public int StaffId { get; set; }
            public string FullName { get; set; }
            public string NameCMT { get; set; }
            public string NumberCMT { get; set; }
            public string ProviderDate { get; set; }
            public string ProviderLocale { get; set; }
            public string SalonName { get; set; }
            public string LevelName { get; set; }
            public string DepartmentName { get; set; }
            public string Address { get; set; }
            public string DateJoin { get; set; }
            public string NumberInsurrance { get; set; }
            public string TaxCode { get; set; }
            public string BankName { get; set; }
            public string BankNumber { get; set; }
            public string BirthDay { get; set; }
            public string Gender { get; set; }
            public string StaffPhone { get; set; }
            public string StaffEmail { get; set; }
            public string RecruitmentSource { get; set; }
            public string TypeAccount { get; set; }
            public string SalaryByPerson { get; set; }
            public string RequireEnroll { get; set; }
            public string SeniorityDate { get; set; }
            public int NumberSeniority { get; set; }
            public string ContractsName { get; set; }
            public string Status { get; set; }
            public string SoYeuLL { get; set; }
            public string CMTCongChung { get; set; }
            public string SoHoKhau { get; set; }
            public string GiayKhaiSinh { get; set; }
            public string GiayKhamSK { get; set; }
        }
    }
}