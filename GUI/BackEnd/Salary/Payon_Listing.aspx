﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Payon_Listing.aspx.cs" Inherits="_30shine.GUI.BackEnd.Salary.Payon_Listing"  MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="ServiceListing" ContentPlaceHolderID="CtMain" runat="server">
<asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
<asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

<div class="wp sub-menu">
    <div class="wp960">
        <div class="wp content-wp">
            <ul class="ul-sub-menu" id="subMenu">
                <li>Quản lý cấu hình tiền công &nbsp;&#187; </li>
                <li class="li-listing"><a href="/admin/cau-hinh-luong.html">Danh sách</a></li>
                <%--<li class="li-add"><a href="/admin/payon/them-moi.html">Thêm mới</a></li>--%>
            </ul>
        </div>
    </div>
</div>

<div class="wp customer-add customer-listing be-report config-salary">
    <%-- Listing --%>                
    <div class="wp960 content-wp">
        <div class="row" style="display:none;">
            <strong class="st-head"><i class="fa fa-filter"></i>Lọc kết quả</strong>
            <div class="filter-item">
                <select id="KindView" name="KindView" runat="server" ClientIDMode="Static" style="margin: 12px 0; width: 190px;" >
                    <option value="1" selected="selected">Danh mục : Dịch vụ</option>
                    <option value="2">Danh mục : Sản phẩm</option>
                </select>
            </div>
            <div class="filter-item">
                <asp:DropDownList ID="StaffType" runat="server" ClientIDMode="Static" style="margin: 12px 0; width: 190px;"></asp:DropDownList>
            </div>
            <asp:Panel ID="ViewDataFilter" CssClass="st-head btn-viewdata" ClientIDMode="Static" 
                onclick="excPaging(1)" runat="server">Xem dữ liệu</asp:Panel>
            <a href="/admin/bao-cao/salary.html" class="st-head btn-viewdata">Reset Filter</a>
            <%--<div class="export-wp drop-down-list-wp">
                <ul class="ul-drop-down-list" style="display:none;">
                    <li onclick="EntryExportExcel('all')">Tất cả dữ liệu</li>
                    <li onclick="EntryExportExcel('current page')">Trang hiện tại</li>
                </ul>                
            </div>--%>
        </div>
        <div class="row">
            <strong class="st-head" style="margin-bottom: -20px
;"><i class="fa fa-file-text"></i>Danh sách cấu hình lương</strong>
        </div>
        <!-- Row Table Filter -->
        <div class="table-func-panel">
            <div class="table-func-elm">
                <span>Số hàng / Page : </span>
                <div class="table-func-input-wp">
                    <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                    <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                    <ul class="ul-opt-segment">
                        <li data-value="10">10</li>
                        <li data-value="20">20</li>
                        <li data-value="30">30</li>
                        <li data-value="40">40</li>
                        <li data-value="50">50</li>
                        <li data-value="1000000">Tất cả</li>
                    </ul>
                    <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                </div>                
            </div>            
        </div>
        <!-- End Row Table Filter -->
        <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
        <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
            <ContentTemplate>
                <div class="table-wp">
                    <table class="table-add table-listing table-config-salary">
                        <thead>
                            <tr>
                                <th>STT</th>
                                <th>ID</th>
                                <th>Item</th>
                                <th colspan="6">Stylist</th>
                                <th colspan="3">Skinner</th>
                                <th colspan="4">Lễ tân</th>
                                <th>Bảo vệ</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>Học việc bậc 1</td>
                                <td>Học việc bậc 2</td>
                                <td>Thử việc</td>
                                <td>Bậc 1</td>
                                <td>Bậc 2</td>
                                <td>Bậc 3</td>
                                <td>Thử việc</td>
                                <td>Bậc 1</td>
                                <td>Bậc 2</td>
                                <td>Thử việc</td>
                                <td>Bậc 1</td>
                                <td>Bậc 2</td>
                                <td>Bậc 3</td>
                                <td></td>
                            </tr>
                            <tr style="background:yellow!important;">
                                <td></td>
                                <td></td>
                                <td>Lương cứng</td>
                                <td>
                                    <div class="input-inline-wp">
                                        <input type="text" value="<%=fixSalary.Stylist.Level1.Value %>" onchange="updatePayon($(this), <%=fixSalary.Stylist.Level1.Id%>)" />
                                    </div>
                                </td>
                                <td>
                                    <div class="input-inline-wp">
                                        <input type="text" value="<%=fixSalary.Stylist.Level2.Value %>" onchange="updatePayon($(this), <%=fixSalary.Stylist.Level2.Id%>)" />
                                    </div>
                                </td>
                                <td>
                                    <div class="input-inline-wp">
                                        <input type="text" value="<%=fixSalary.Stylist.Level3.Value %>" onchange="updatePayon($(this), <%=fixSalary.Stylist.Level3.Id%>)" />
                                    </div>
                                </td>
                                <td>
                                    <div class="input-inline-wp">
                                        <input type="text" value="<%=fixSalary.Stylist.Level4.Value %>" onchange="updatePayon($(this), <%=fixSalary.Stylist.Level4.Id%>)" />
                                    </div>
                                </td>
                                <td>
                                    <div class="input-inline-wp">
                                        <input type="text" value="<%=fixSalary.Stylist.Level5.Value %>" onchange="updatePayon($(this), <%=fixSalary.Stylist.Level5.Id%>)" />
                                    </div>
                                </td>
                                <td>
                                    <div class="input-inline-wp">
                                        <input type="text" value="<%=fixSalary.Stylist.Level6.Value %>" onchange="updatePayon($(this), <%=fixSalary.Stylist.Level6.Id%>)" />
                                    </div>
                                </td>
                                <td>
                                    <div class="input-inline-wp">
                                        <input type="text" value="<%=fixSalary.Skinner.Level3.Value %>" onchange="updatePayon($(this), <%=fixSalary.Skinner.Level3.Id%>)" />
                                    </div>
                                </td>
                                <td>
                                    <div class="input-inline-wp">
                                        <input type="text" value="<%=fixSalary.Skinner.Level4.Value %>" onchange="updatePayon($(this), <%=fixSalary.Skinner.Level4.Id%>)" />
                                    </div>
                                </td>
                                <td>
                                    <div class="input-inline-wp">
                                        <input type="text" value="<%=fixSalary.Skinner.Level5.Value %>" onchange="updatePayon($(this), <%=fixSalary.Skinner.Level5.Id%>)" />
                                    </div>
                                </td>
                                <td>
                                    <div class="input-inline-wp">
                                        <input type="text" value="<%=fixSalary.Reception.Level3.Value %>" onchange="updatePayon($(this), <%=fixSalary.Reception.Level3.Id%>)" />
                                    </div>
                                </td>
                                <td>
                                    <div class="input-inline-wp">
                                        <input type="text" value="<%=fixSalary.Reception.Level4.Value %>" onchange="updatePayon($(this), <%=fixSalary.Reception.Level4.Id%>)" />
                                    </div>
                                </td>
                                <td>
                                    <div class="input-inline-wp">
                                        <input type="text" value="<%=fixSalary.Reception.Level5.Value %>" onchange="updatePayon($(this), <%=fixSalary.Reception.Level5.Id%>)" />
                                    </div>
                                </td>
                                <td>
                                    <div class="input-inline-wp">
                                        <input type="text" value="<%=fixSalary.Reception.Level6.Value %>" onchange="updatePayon($(this), <%=fixSalary.Reception.Level6.Id%>)" />
                                    </div>
                                </td>
                                <td>
                                    <div class="input-inline-wp">
                                        <input type="text" value="<%=fixSalary.Peaceofficer.Level0.Value %>" onchange="updatePayon($(this), <%=fixSalary.Peaceofficer.Level0.Id%>)" />
                                    </div>
                                </td>
                            </tr>
                            <tr style="background:yellow!important;">
                                <td></td>
                                <td></td>
                                <td>Part-time</td>
                                <td>
                                    <div class="input-inline-wp">
                                        <input type="text" value="<%=partTimeSalary.Stylist.Level1.Value %>" onchange="updatePayon($(this), <%=partTimeSalary.Stylist.Level1.Id%>)" />
                                    </div>
                                </td>
                                <td>
                                    <div class="input-inline-wp">
                                        <input type="text" value="<%=partTimeSalary.Stylist.Level2.Value %>" onchange="updatePayon($(this), <%=partTimeSalary.Stylist.Level2.Id%>)" />
                                    </div>
                                </td>
                                <td>
                                    <div class="input-inline-wp">
                                        <input type="text" value="<%=partTimeSalary.Stylist.Level3.Value %>" onchange="updatePayon($(this), <%=partTimeSalary.Stylist.Level3.Id%>)" />
                                    </div>
                                </td>
                                <td>
                                    <div class="input-inline-wp">
                                        <input type="text" value="<%=partTimeSalary.Stylist.Level4.Value %>" onchange="updatePayon($(this), <%=partTimeSalary.Stylist.Level4.Id%>)" />
                                    </div>
                                </td>
                                <td>
                                    <div class="input-inline-wp">
                                        <input type="text" value="<%=partTimeSalary.Stylist.Level5.Value %>" onchange="updatePayon($(this), <%=partTimeSalary.Stylist.Level5.Id%>)" />
                                    </div>
                                </td>
                                <td>
                                    <div class="input-inline-wp">
                                        <input type="text" value="<%=partTimeSalary.Stylist.Level6.Value %>" onchange="updatePayon($(this), <%=partTimeSalary.Stylist.Level6.Id%>)" />
                                    </div>
                                </td>
                                <td>
                                    <div class="input-inline-wp">
                                        <input type="text" value="<%=partTimeSalary.Skinner.Level3.Value %>" onchange="updatePayon($(this), <%=partTimeSalary.Skinner.Level3.Id%>)" />
                                    </div>
                                </td>
                                <td>
                                    <div class="input-inline-wp">
                                        <input type="text" value="<%=partTimeSalary.Skinner.Level4.Value %>" onchange="updatePayon($(this), <%=partTimeSalary.Skinner.Level4.Id%>)" />
                                    </div>
                                </td>
                                <td>
                                    <div class="input-inline-wp">
                                        <input type="text" value="<%=partTimeSalary.Skinner.Level5.Value %>" onchange="updatePayon($(this), <%=partTimeSalary.Skinner.Level5.Id%>)" />
                                    </div>
                                </td>
                                <td>
                                    <div class="input-inline-wp">
                                        <input type="text" value="<%=partTimeSalary.Reception.Level3.Value %>" onchange="updatePayon($(this), <%=partTimeSalary.Reception.Level3.Id%>)" />
                                    </div>
                                </td>
                                <td>
                                    <div class="input-inline-wp">
                                        <input type="text" value="<%=partTimeSalary.Reception.Level4.Value %>" onchange="updatePayon($(this), <%=partTimeSalary.Reception.Level4.Id%>)" />
                                    </div>
                                </td>
                                <td>
                                    <div class="input-inline-wp">
                                        <input type="text" value="<%=partTimeSalary.Reception.Level5.Value %>" onchange="updatePayon($(this), <%=partTimeSalary.Reception.Level5.Id%>)" />
                                    </div>
                                </td>
                                <td>
                                    <div class="input-inline-wp">
                                        <input type="text" value="<%=partTimeSalary.Reception.Level6.Value %>" onchange="updatePayon($(this), <%=partTimeSalary.Reception.Level6.Id%>)" />
                                    </div>
                                </td>
                                <td>
                                    <div class="input-inline-wp">
                                        <input type="text" value="<%=partTimeSalary.Peaceofficer.Level0.Value %>" onchange="updatePayon($(this), <%=partTimeSalary.Peaceofficer.Level0.Id%>)" />
                                    </div>
                                </td>
                            </tr>
                            <tr style="background:yellow!important;">
                                <td></td>
                                <td></td>
                                <td>Phụ cấp</td>
                                <td>
                                    <div class="input-inline-wp">
                                        <input type="text" value="<%=allowanceSalary.Stylist.Level1.Value %>" onchange="updatePayon($(this), <%=allowanceSalary.Stylist.Level1.Id%>)" />
                                    </div>
                                </td>
                                <td>
                                    <div class="input-inline-wp">
                                        <input type="text" value="<%=allowanceSalary.Stylist.Level2.Value %>" onchange="updatePayon($(this), <%=allowanceSalary.Stylist.Level2.Id%>)" />
                                    </div>
                                </td>
                                <td>
                                    <div class="input-inline-wp">
                                        <input type="text" value="<%=allowanceSalary.Stylist.Level3.Value %>" onchange="updatePayon($(this), <%=allowanceSalary.Stylist.Level3.Id%>)" />
                                    </div>
                                </td>
                                <td>
                                    <div class="input-inline-wp">
                                        <input type="text" value="<%=allowanceSalary.Stylist.Level4.Value %>" onchange="updatePayon($(this), <%=allowanceSalary.Stylist.Level4.Id%>)" />
                                    </div>
                                </td>
                                <td>
                                    <div class="input-inline-wp">
                                        <input type="text" value="<%=allowanceSalary.Stylist.Level5.Value %>" onchange="updatePayon($(this), <%=allowanceSalary.Stylist.Level5.Id%>)" />
                                    </div>
                                </td>
                                <td>
                                    <div class="input-inline-wp">
                                        <input type="text" value="<%=allowanceSalary.Stylist.Level6.Value %>" onchange="updatePayon($(this), <%=allowanceSalary.Stylist.Level6.Id%>)" />
                                    </div>
                                </td>
                                <td>
                                    <div class="input-inline-wp">
                                        <input type="text" value="<%=allowanceSalary.Skinner.Level3.Value %>" onchange="updatePayon($(this), <%=allowanceSalary.Skinner.Level3.Id%>)" />
                                    </div>
                                </td>
                                <td>
                                    <div class="input-inline-wp">
                                        <input type="text" value="<%=allowanceSalary.Skinner.Level4.Value %>" onchange="updatePayon($(this), <%=allowanceSalary.Skinner.Level4.Id%>)" />
                                    </div>
                                </td>
                                <td>
                                    <div class="input-inline-wp">
                                        <input type="text" value="<%=allowanceSalary.Skinner.Level5.Value %>" onchange="updatePayon($(this), <%=allowanceSalary.Skinner.Level5.Id%>)" />
                                    </div>
                                </td>
                                <td>
                                    <div class="input-inline-wp">
                                        <input type="text" value="<%=allowanceSalary.Reception.Level3.Value %>" onchange="updatePayon($(this), <%=allowanceSalary.Reception.Level3.Id%>)" />
                                    </div>
                                </td>
                                <td>
                                    <div class="input-inline-wp">
                                        <input type="text" value="<%=allowanceSalary.Reception.Level4.Value %>" onchange="updatePayon($(this), <%=allowanceSalary.Reception.Level4.Id%>)" />
                                    </div>
                                </td>
                                <td>
                                    <div class="input-inline-wp">
                                        <input type="text" value="<%=allowanceSalary.Reception.Level5.Value %>" onchange="updatePayon($(this), <%=allowanceSalary.Reception.Level5.Id%>)" />
                                    </div>
                                </td>
                                <td>
                                    <div class="input-inline-wp">
                                        <input type="text" value="<%=allowanceSalary.Reception.Level6.Value %>" onchange="updatePayon($(this), <%=allowanceSalary.Reception.Level6.Id%>)" />
                                    </div>
                                </td>
                                <td>
                                    <div class="input-inline-wp">
                                        <input type="text" value="<%=allowanceSalary.Peaceofficer.Level0.Value %>" onchange="updatePayon($(this), <%=allowanceSalary.Peaceofficer.Level0.Id%>)" />
                                    </div>
                                </td>
                            </tr>
                            <asp:Repeater ID="RptService" runat="server">
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Container.ItemIndex + 1 + PAGING._Offset %></td>
                                        <td><%# Eval("KeyId") %></td>
                                        <td><a href="/admin/dich-vu/<%# Eval("KeyId") %>.html"><%# Eval("ItemName") %></a></td>
                                        <td>
                                            <div class="input-inline-wp">
                                                <input type="text" value="<%# Eval("Stylist.Level1.Value") %>" onchange="updatePayon($(this), <%# Eval("Stylist.Level1.Id") %>)" />
                                            </div>
                                        </td>
                                        <td>
                                            <div class="input-inline-wp">
                                                <input type="text" value="<%# Eval("Stylist.Level2.Value") %>" onchange="updatePayon($(this), <%# Eval("Stylist.Level2.Id") %>)" />
                                            </div>
                                        </td>
                                        <td>
                                            <div class="input-inline-wp">
                                                <input type="text" value="<%# Eval("Stylist.Level3.Value") %>" onchange="updatePayon($(this), <%# Eval("Stylist.Level3.Id") %>)" />
                                            </div>
                                        </td>
                                        <td>
                                            <div class="input-inline-wp">
                                                <input type="text" value="<%# Eval("Stylist.Level4.Value") %>" onchange="updatePayon($(this), <%# Eval("Stylist.Level4.Id") %>)" />
                                            </div>
                                        </td>
                                        <td>
                                            <div class="input-inline-wp">
                                                <input type="text" value="<%# Eval("Stylist.Level5.Value") %>" onchange="updatePayon($(this), <%# Eval("Stylist.Level5.Id") %>)" />
                                            </div>
                                        </td>
                                        <td>
                                            <div class="input-inline-wp">
                                                <input type="text" value="<%# Eval("Stylist.Level6.Value") %>" onchange="updatePayon($(this), <%# Eval("Stylist.Level6.Id") %>)" />
                                            </div>
                                        </td>
                                        <td>
                                            <div class="input-inline-wp">
                                                <input type="text" value="<%# Eval("Skinner.Level3.Value") %>" onchange="updatePayon($(this), <%# Eval("Skinner.Level3.Id") %>)" />
                                            </div>
                                        </td>
                                        <td>
                                            <div class="input-inline-wp">
                                                <input type="text" value="<%# Eval("Skinner.Level4.Value") %>" onchange="updatePayon($(this), <%# Eval("Skinner.Level4.Id") %>)" />
                                            </div>
                                        </td>
                                        <td>
                                            <div class="input-inline-wp">
                                                <input type="text" value="<%# Eval("Skinner.Level5.Value") %>" onchange="updatePayon($(this), <%# Eval("Skinner.Level5.Id") %>)" />
                                            </div>
                                        </td>
                                        <td>
                                            <%--<div class="input-inline-wp">
                                                <input type="text" value="<%# Eval("Reception.Level0.Value") %>" onchange="updatePayon($(this), <%# Eval("Reception.Level0.Id") %>)" />
                                            </div>--%>
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td class="map-edit">
                                            <div class="edit-wp">
                                                <a class="elm edit-btn" href="/admin/dich-vu/<%# Eval("KeyId") %>.html" title="Sửa"></a>
                                                <a class="elm del-btn" onchange="del(this.parentNode.parentNode.parentNode,'<%# Eval("KeyId") %>', '<%# Eval("ItemName") %>')" href="javascript://" title="Xóa"></a>
                                            </div>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>                    
                        </tbody>
                    </table>
                </div>

                <!-- Paging -->
                <div class="site-paging-wp">
                <% if(PAGING.TotalPage > 1){ %>
                    <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                        <% if (PAGING._Paging.Prev != 0)
                           { %>
                        <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                        <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                        <% } %>
                        <asp:Repeater ID="RptPaging" runat="server">
                            <ItemTemplate>
                                <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                    <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>
                                >
                                    <%# Eval("PageNum") %>
                                </a>
                            </ItemTemplate>                    
                        </asp:Repeater>
                        <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                           { %>                
                        <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                        <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                        <% } %>
                    </asp:Panel>
                <% } %>
                </div>
		        <!-- End Paging -->                
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
        <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" style="display:none;" />
        <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
    </div>    
    <%-- END Listing --%>
</div>

<!-- Notification box -->
<div class="notification-box">Thành công</div>
<!--// Notification box -->

<script>
    jQuery(document).ready(function () {
        $("#glbAdminContent").addClass("active");
        $("#glbAdminService").addClass("active");
        $("#subMenu .li-listing").addClass("active");
    });

    //============================
    // Event delete
    //============================
    function del(This, code, name) {
        var code = code || null,
            name = name || null,
            Row = This;
        if (!code) return false;

        // show EBPopup
        $(".confirm-yn").openEBPopup();
        $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn xóa [ " + name + " ] ?");

        $("#EBPopup .yn-yes").bind("click", function () {
            $.ajax({
                type: "POST",
                url: "/GUI/SystemService/Ajax/Del.aspx/Delele_Service",
                data: '{Code : "' + code + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json", success: function (response) {
                    var mission = JSON.parse(response.d);
                    if (mission.success) {
                        delSuccess();
                        Row.remove();
                    } else {
                        delFailed();
                    }
                },
                failure: function (response) { alert(response.d); }
            });
        });
        $("#EBPopup .yn-no").bind("click", function () {
            autoCloseEBPopup(0);
        });
    }

    //============================
    // Event publish, featured, is free
    //============================
    function checkFeatured(This, table) {
        var Id = This.attr("data-id");
        var isChecked = This.is(":checked");
        $.ajax({
            type: "POST",
            url: "/GUI/SystemService/Ajax/Publish_Featured.aspx/Featured",
            data: '{Id : "' + Id + '", Table : "' + table + '", isChecked : "' + isChecked + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json", success: function (response) {
                var mission = JSON.parse(response.d);
                if (mission.success) {
                    alert("Chuyển trạng thái thành công.");
                } else {
                }
            },
            failure: function (response) { alert(response.d); }
        });
    }

    function checkPublish(This, table) {
        var Id = This.attr("data-id");
        var isChecked = This.is(":checked");
        $.ajax({
            type: "POST",
            url: "/GUI/SystemService/Ajax/Publish_Featured.aspx/Publish",
            data: '{Id : "' + Id + '", Table : "' + table + '", isChecked : "' + isChecked + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json", success: function (response) {
                var mission = JSON.parse(response.d);
                if (mission.success) {
                    alert("Chuyển trạng thái thành công.");
                } else {
                }
            },
            failure: function (response) { alert(response.d); }
        });
    }

    function checkIsFree(This, table) {
        var Id = This.attr("data-id");
        var isChecked = This.is(":checked");
        $.ajax({
            type: "POST",
            url: "/GUI/SystemService/Ajax/Publish_Featured.aspx/IsFree",
            data: '{Id : "' + Id + '", Table : "' + table + '", isChecked : "' + isChecked + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json", success: function (response) {
                var mission = JSON.parse(response.d);
                if (mission.success) {
                    alert("Chuyển trạng thái thành công.");
                } else {
                }
            },
            failure: function (response) { alert(response.d); }
        });
    }

    /// Cập nhật giá trị payon
    function updatePayon(This, Id) {
        $.ajax({
            type: "POST",
            url: "/GUI/BackEnd/Salary/Payon_Listing.aspx/updatePayon",
            data: '{Id : ' + Id + ', Value : ' + This.val() + '}',
            contentType: "application/json; charset=utf-8",
            dataType: "json", success: function (response) {
                var mission = JSON.parse(response.d);
                if (mission.success) {
                    showNotification("Dữ liệu đã được cập nhật.");
                    $("#BtnFakeUP").click();
                } else {
                    alert("Có lỗi xảy ra. Vui lòng liên hệ nhóm phát triển.");
                }
            },
            failure: function (response) { alert(response.d); }
        });
    }

    function showNotification(content) {
        $(".notification-box").text(content).fadeIn(function () {
            setTimeout(function () {
                $(".notification-box").fadeOut();
            }, 2000);
        });
    }

</script>

</asp:Panel>
</asp:Content>

