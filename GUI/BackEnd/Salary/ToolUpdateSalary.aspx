﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="ToolUpdateSalary.aspx.cs" Inherits="_30shine.GUI.BackEnd.Salary.ToolUpdateSalary" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <style>
            #success-alert {
                top: 100px;
                right: 10px;
                position: fixed;
                width: 20% !important;
                z-index: 2000;
            }
        </style>
        <div class="alert alert-danger" id="success-alert">
            <span id="msg-alert"></span>
        </div>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Cập nhật lương theo Salon/Bộ phận/Nhân viên &nbsp;&#187;</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add">
            <%-- Add --%>
            <div class="wp960 content-wp">
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <div id="" style="height: 250px;">
                    <div class="filter-item" style="margin-left: 0;">
                        <strong class="st-head" style="margin-right: 10px; font-size: 18px;"><i class="fa fa-clock-o"></i>Thời gian </strong>
                        <hr />
                        <div class="datepicker-wp">
                            <input id="TxtDateTimeFrom" style="width: 20%; display: inline-block;" class="txtDateTime st-head form-control" placeholder="Từ ngày" />
                            <strong class="st-head" style="margin-left: 10px; margin-right: 10px;">
                                <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                            </strong>
                            <input id="TxtDateTimeTo" style="width: 20%; display: inline-block;" class="txtDateTime st-head form-control" placeholder="Đến ngày" />
                        </div>
                    </div>
                    <hr />
                    <table class="table-add">
                        <tbody>
                            <tr class="title-head">
                                <td><strong>Cập nhật theo bộ phận</strong></td>
                                <td></td>
                            </tr>
                            <tr class="tr-margin" style="height: 20px;"></tr>
                            <tr>
                                <td class="col-xs-2 left"><span>Salon<strong style="color: red"> &nbsp*</strong></span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <%--OnSelectedIndexChanged="bindStaffBySalon"--%>
                                        <asp:DropDownList ID="ddlSalonByStaffType" CssClass="form-control select" AutoPostBack="true" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                    </span>
                                </td>
                            </tr>
                            <tr style="margin-top: 5px;">
                                <td class="col-xs-2 left"><span>Bộ phận<strong style="color: red"> &nbsp*</strong></span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:DropDownList ID="ddlStaffType" CssClass="form-control select" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-2 left"></td>
                                <td class="col-xs-9 right">
                                    <span style="margin-top: 7px;" class="field-wp">
                                        <input type="button" name="UpdateByStaffType" onclick="UpdateSalaryByType()" id="UpdateByStaffType" style="background-color: black; color: white; padding: 7px;" class="btn" value="Cập nhật theo bộ phận" />
                                    </span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div id="" style="height: 200px;">
                    <table class="table-add">
                        <tbody>
                            <tr class="title-head">
                                <td><strong>Cập nhật theo salon</strong></td>
                                <td></td>
                            </tr>
                            <tr class="tr-margin" style="height: 20px;"></tr>
                            <tr>
                                <td class="col-xs-2 left"><span>Salon<strong style="color: red"> &nbsp*</strong></span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <%--OnSelectedIndexChanged="bindStaffBySalon"--%>
                                        <asp:DropDownList ID="ddlSalonBySalon" CssClass="form-control select" AutoPostBack="true" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-2 left"></td>
                                <td class="col-xs-9 right">
                                    <span style="margin-top: 7px;" class="field-wp">
                                        <input type="button" name="UpdateBySalon" id="UpdateBySalon" onclick="UpdateSalaryBySalon()" style="background-color: black; color: white; padding: 7px;" class="btn" value="Cập nhật theo salon" />
                                    </span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div id="" style="height: 250px;">
                    <table class="table-add">
                        <tbody>
                            <tr class="title-head">
                                <td><strong>Cập nhật theo nhân viên</strong></td>
                                <td></td>
                            </tr>
                            <tr class="tr-margin" style="height: 20px;"></tr>
                            <tr>
                                <td class="col-xs-2 left"><span>Salon<strong style="color: red"> &nbsp*</strong></span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:DropDownList ID="ddlSalonByStaff" CssClass="form-control select" OnSelectedIndexChanged="bindStaffBySalon" AutoPostBack="true" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                    </span>
                                </td>
                            </tr>
                            <tr style="margin-top: 5px;">
                                <td class="col-xs-2 left"><span>Nhân viên<strong style="color: red"> &nbsp*</strong></span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:DropDownList ID="ddlStaff" CssClass="form-control select" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-2 left"></td>
                                <td class="col-xs-9 right">
                                    <span style="margin-top: 7px;" class="field-wp">
                                        <input name="UpdateByStaff" onclick="UpdateSalaryByStaff()" id="UpdateByStaff" type="button" style="background-color: black; color: white; padding: 7px;" class="btn" value="Cập nhật theo nhân viên" />
                                    </span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <%-- end Add --%>
        </div>
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 190px !important;
                margin-top: 5px !important;
            }
            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }
        </style>
        <style>
            .be-report table.table-listing.table-listing-salary td {
                padding: 7px 5px !important;
            }
        </style>
        <script>
            //============================
            // Datepicker
            //============================
            $('.txtDateTime').datetimepicker({
                dayOfWeekStart: 1,
                lang: 'vi',
                startDate: '2014/10/10',
                format: 'd/m/Y',
                dateonly: true,
                showHour: false,
                showMinute: false,
                timepicker: false,
                onChangeDateTime: function (dp, $input) {
                    //$input.val($input.val().split(' ')[0]);
                }
            });
            $("#success-alert").hide();

            // Cập nhật lương cho salon
            function UpdateSalaryByStaff() {

                var kt = true;
                var error = 'Vui lòng nhập đầy đủ thông tin!';
                var salon = $('#ddlSalonByStaff :selected').val();
                var staff = $('#ddlStaff :selected').val();
                var TimeFrom = $("#TxtDateTimeFrom").val();
                var TimeTo = $("#TxtDateTimeTo").val();
                if (TimeFrom == "" || TimeFrom == null) {
                    kt = false;
                    error += "[Chọn thời gian!], ";
                    $("#TxtDateTimeFrom").css("border-color", "red");
                }
                if (TimeTo == "" || TimeTo == null) {
                    kt = false;
                    error += "[Chọn thời gian tới!], ";
                    $("#TxtDateTimeTo").css("border-color", "red");
                }
                if (salon == "0" || salon == null) {
                    kt = false;
                    error += "[Chọn Salon!], ";
                    $("#ddlSalonByStaff").css("border-color", "red");
                }
                if (staff == "0" || staff == null) {
                    kt = false;
                    error += "[Chọn nhân viên!], ";
                    $("#ddlStaff").css("border-color", "red");
                }
                if (!kt) {
                    $("#success-alert").fadeTo(2000, 2000).slideUp(2000, function () {
                        $("#success-alert").slideUp(2000);
                    });
                    $("#msg-alert").text(error);
                }
                else {
                    addLoading();
                    $.ajax({
                        type: 'POST',
                        url: '/GUI/BackEnd/Salary/ToolUpdateSalary.aspx/UpdateSalaryByStaff',
                        data: '{ salonid : ' + salon + ', staffid : ' + staff + ', timeFrom : "' + TimeFrom + '", timeTo : "' + TimeTo + '" }',
                        contentType: 'application/json',
                    }).success(function (response) {
                        if (response.d != false) {
                            alert("Cập nhật thành công!");
                        }
                        else {
                            alert("Cập nhật thất bại!");
                        }
                        removeLoading();
                    });
                }
                return kt;
            }
            //cập nhật lương cho nhân viên
            function UpdateSalaryBySalon() {
                var kt = true;
                var error = 'Vui lòng nhập đầy đủ thông tin!';
                var salon = $('#ddlSalonBySalon :selected').val();
                var TimeFrom = $("#TxtDateTimeFrom").val();
                var TimeTo = $("#TxtDateTimeTo").val();
                if (salon == "0" || salon == null) {
                    kt = false;
                    error += "[Chọn Salon!], ";
                    $("#ddlSalonBySalon").css("border-color", "red");
                }
                if (TimeFrom == "" || TimeFrom == null) {
                    kt = false;
                    error += "[Chọn thời gian!], ";
                    $("#TxtDateTimeFrom").css("border-color", "red");
                }
                if (TimeTo == "" || TimeTo == null) {
                    kt = false;
                    error += "[Chọn thời gian tới!], ";
                    $("#TxtDateTimeTo").css("border-color", "red");
                }
                if (!kt) {
                    $("#success-alert").fadeTo(2000, 2000).slideUp(2000, function () {
                        $("#success-alert").slideUp(2000);
                    });
                    $("#msg-alert").text(error);
                }
                else {
                    addLoading();
                    $.ajax({
                        type: 'POST',
                        url: '/GUI/BackEnd/Salary/ToolUpdateSalary.aspx/UpdateSalaryBySalon',
                        data: '{ salonid : ' + salon + ', timeFrom : "' + TimeFrom + '", timeTo : "' + TimeTo + '" }',
                        contentType: 'application/json',
                    }).success(function (response) {
                        if (response.d != false) {
                            alert("Cập nhật thành công!");
                        }
                        else {
                            alert("Cập nhật thất bại!");
                        }
                        removeLoading();
                    });
                }
                return kt;
            }
            // Cập nhật lương cho bộ phận
            function UpdateSalaryByType() {
                var kt = true;
                var error = 'Vui lòng nhập đầy đủ thông tin!';
                var salon = $('#ddlSalonByStaffType :selected').val();
                var type = $('#ddlStaffType :selected').val();
                var TimeFrom = $("#TxtDateTimeFrom").val();
                var TimeTo = $("#TxtDateTimeTo").val();
                if (salon == "0" || salon == null) {
                    kt = false;
                    error += "[Chọn Salon!], ";
                    $("#ddlSalonByStaffType").css("border-color", "red");
                }
                if (type == "0" || type == null) {
                    kt = false;
                    error += "[Chọn bộ phận], ";
                    $("#ddlStaffType").css("border-color", "red");
                }
                if (TimeFrom == "" || TimeFrom == null) {
                    kt = false;
                    error += "[Chọn thời gian!], ";
                    $("#TxtDateTimeFrom").css("border-color", "red");
                }
                if (TimeTo == "" || TimeTo == null) {
                    kt = false;
                    error += "[Chọn thời gian tới!], ";
                    $("#TxtDateTimeTo").css("border-color", "red");
                }
                if (!kt) {
                    $("#success-alert").fadeTo(2000, 2000).slideUp(2000, function () {
                        $("#success-alert").slideUp(2000);
                    });
                    $("#msg-alert").text(error);
                }
                else {
                    addLoading();
                    $.ajax({
                        type: 'POST',
                        url: '/GUI/BackEnd/Salary/ToolUpdateSalary.aspx/UpdateSalaryByType',
                        data: '{ salonid : ' + salon + ', type : ' + type + ', timeFrom : "' + TimeFrom + '", timeTo : "' + TimeTo + '" }',
                        contentType: 'application/json',
                    }).success(function (response) {
                        if (response.d != false) {
                            alert("Cập nhật thành công!");
                        }
                        else {
                            alert("Cập nhật thất bại!");
                        }
                        removeLoading();
                    });
                }
                return kt;
            }
        </script>
    </asp:Panel>
</asp:Content>
