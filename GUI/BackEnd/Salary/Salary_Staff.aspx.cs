﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Globalization;
using LinqKit;
using System.Linq.Expressions;
using ExportToExcel;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.Salary
{
    public partial class Salary_Staff : System.Web.UI.Page
    {
        protected Paging PAGING = new Paging();
        private string PageID = "";
        private int _TypeStaff;
        private int _StaffId;

        protected bool Staff_All = false;
        protected bool Staff_Hairdresser = false;
        protected bool Staff_HairMassage = false;
        protected bool Staff_Singer = false;
        protected string THead = "";
        public CultureInfo culture = new CultureInfo("vi-VN");
        private DateTime d1 = new DateTime();
        private DateTime d2 = new DateTime();
        public string timeFrom;
        public string timeTo;
        private Expression<Func<Staff, bool>> WhereStaff = PredicateBuilder.True<Staff>();
        private List<Service> _Service = new List<Service>();
        private List<Product> _Product = new List<Product>();
        protected string FirstDayOfMonth = string.Format("{0:dd/MM/yyyy}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1));
        protected string Today = String.Format("{0:dd/MM/yyyy}", DateTime.Now);
        protected int ThisMon;// = DateTime.Now.Month;
        protected int DayOfMon;// = DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month);
        protected int ConfigWorkDay = 0;

        private bool Perm_ViewAllData = false;
        private bool Perm_Access = false;
        protected void checkPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //string perName = Session["User_Permission"].ToString();
                //string url = Request.Url.AbsolutePath.ToString();
                //Solution_30shineEntities db = new Solution_30shineEntities();
                //var pem = (from m in db.Tbl_Permission_Map
                //           join mn in db.Tbl_Permission_Menu on m.mID equals mn.mID
                //           where m.pName == perName && mn.Url_Main.EndsWith(url) || mn.Url_Rewrite.EndsWith(url)
                //           select new { m.aID }
                //           ).FirstOrDefault();
                //if (pem != null)
                //{
                //    var pemArray = pem.aID.Split(',');
                //    if (Array.IndexOf(pemArray, "1") > -1)
                //    {
                //        Perm_Access = true;
                //        if (Array.IndexOf(pemArray, "2") > -1)
                //        {
                //            Perm_ViewAllData = true;
                //        }

                //    }
                //    else
                //    {
                //        ContentWrap.Visible = false;
                //        NotAllowAccess.Visible = true;
                //    }

                //}
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();

            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            checkPermission();

            if (!IsPostBack)
            {
                TxtDateTimeFrom.Text = FirstDayOfMonth;
                TxtDateTimeTo.Text = Today;
                Library.Function.bindSalon(new List<DropDownList> { Salon }, Perm_ViewAllData);
                Bind_TypeStaff();
                GenWhere();
                Bind_All_ByDay();
                //bindTotalByDepartment();
            }
            else
            {
                GenWhere();
                //Exc_Filter();
                Calculate_Salary();
            }
            RemoveLoading();
        }

        private void GenWhere()
        {
            _TypeStaff = HDF_TypeStaff.Value != "" ? Convert.ToInt32(HDF_TypeStaff.Value) : 0;
            _StaffId = HDF_Staff.Value != "" ? Convert.ToInt32(HDF_Staff.Value) : 0;

            if (_StaffId > 0)
            {
                WhereStaff = WhereStaff.And(w => w.Id == _StaffId);
            }
            if (_TypeStaff > 0)
            {
                WhereStaff = WhereStaff.And(w => w.Type == _TypeStaff);
            }
            WhereStaff = WhereStaff.And(w => w.IsDelete != 1);

            int SalonId = Convert.ToInt32(Salon.SelectedValue);
            if (!Perm_ViewAllData)
            {
                WhereStaff = WhereStaff.And(p => p.SalonId == SalonId);
            }
            else
            {
                if (SalonId > 0)
                {
                    WhereStaff = WhereStaff.And(p => p.SalonId == SalonId);
                }
                else
                {
                    WhereStaff = WhereStaff.And(p => p.SalonId > 0);
                }
            }

            WhereStaff = WhereStaff.And(w => w.Permission != "accountant" && w.Permission != "admin" && (w.isAccountLogin != 1 || w.isAccountLogin == null));

            d1 = Convert.ToDateTime(TxtDateTimeFrom.Text.Trim(), culture);
            if (TxtDateTimeTo.Text.Trim() != "")
            {
                d2 = Convert.ToDateTime(TxtDateTimeTo.Text.Trim(), culture).AddDays(1);
            }
            else
            {
                d2 = d1.AddDays(1);
            }
            timeFrom = String.Format("{0:MM/dd/yyyy}", d1);
            timeTo = String.Format("{0:MM/dd/yyyy}", d2);
            ThisMon = d1.Month;
            DayOfMon = DateTime.DaysInMonth(d1.Year, d1.Month);
        }
        

        public void Calculate_Salary()
        {
            var lst = Get_DataBy_Filter(true);
            RptTimeKeeping.DataSource = lst;
            RptTimeKeeping.DataBind();
        }

        private List<Staff_TimeKeeping> Get_DataBy_Filter(bool paging)
        {
            using (var db = new Solution_30shineEntities())
            {
                _TypeStaff = HDF_TypeStaff.Value != "" ? Convert.ToInt32(HDF_TypeStaff.Value) : 0;
                _StaffId = HDF_Staff.Value != "" ? Convert.ToInt32(HDF_Staff.Value) : 0;

                CultureInfo culture = new CultureInfo("vi-VN");
                //var WhereStaff = PredicateBuilder.True<Staff>();
                var TK_Serivce = new List<Staff_TimeKeeping>();
                var TK_Product = new List<Staff_TimeKeeping>();
                var LST_Staff = new List<Staff_TimeKeeping>();
                var OBJ_Staff_TimeKeeping = new Staff_TimeKeeping();
                var LST0 = new List<Staff>();
                int index = -1;
                int index2 = -1;
                var lstStatistic = new List<ItemStatistic>();
                var item = new ItemStatistic();
                var itemTemp = new ItemStatistic();
                var strStatistic = "";
                int totalMoney = 0;
                int money;
                int moneySales = 0;
                int money_m = 0;
                int integer;
                int workDay = 0;
                int workHour = 0;
                string sql;
                string whereStaff = "";
                int SkinnerIdInGroup;
                var payTable = new List<Tbl_Payon>();
                int skn_FixSalary = 0;
                int skn_baseSalary = 0;
                string skn_sql;
                int skn_workDay;
                var skn_payon = new Tbl_Payon();
                var skn = new Staff();
                int PayPercent = 10;
                var _ProductTmp = new ItemStatistic();
                var department = db.Database.SqlQuery<cls_department>("select * from Staff_Type where IsDelete != 1 and Publish = 1").ToList();

                // Get staff
                if (paging)
                {
                    LST0 = db.Staffs.AsExpandable().Where(WhereStaff).OrderBy(o => o.Id).Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                }
                else
                {
                    LST0 = db.Staffs.AsExpandable().Where(WhereStaff).OrderBy(o => o.Id).ToList();
                }

                // Time condition
                if (TxtDateTimeFrom.Text != "")
                {
                    int year = Convert.ToDateTime(TxtDateTimeFrom.Text, culture).Year;
                    int mon = Convert.ToDateTime(TxtDateTimeFrom.Text, culture).Month;
                    //var wds = db.Tbl_ConfigWorkDays.FirstOrDefault(w => w.Year == year && w.Month == mon);
                    //if (wds != null)
                    //{
                    //    ConfigWorkDay = int.TryParse(wds.WorkDays.ToString(), out integer) ? integer : DateTime.DaysInMonth(year, mon) - 2;
                    //}
                    //else
                    //{
                        ConfigWorkDay = DateTime.DaysInMonth(year, mon) - 2;
                    //}

                    // Map data to staff
                    if (LST0.Count > 0)
                    {
                        foreach (var v in LST0)
                        {
                            /// 1. Services
                            whereStaff = "";                            
                            switch (v.Type)
                            {
                                case 1: whereStaff += "and Staff_Hairdresser_Id = " + v.Id;
                                    break;
                                case 2: whereStaff += "and Staff_HairMassage_Id = " + v.Id;
                                    break;
                                //default: whereStaff += "and Staff_Hairdresser_Id = " + v.Id;
                                //    break;
                            }

                            if (whereStaff != "")
                            {
                                sql = @"select a.*, b.SkinnerId, 
                                        Coalesce((select Value from Tbl_Payon where TypeStaffId = 2 and ForeignId = b.SkinnerLevel and KeyId = a.ServiceId and Hint = 'service'), 0) as SkinnerPayon
                                    from FlowService as a
                                    inner join
                                    (
	                                    select Id, ServiceIds, TotalMoney, SalonId, CreatedDate, Staff_HairMassage_Id as SkinnerId,
                                        (	case 
			                                    when (Staff_HairMassage_Id > 0) then (select SkillLevel from Staff where Id = Staff_HairMassage_Id)
			                                    else 0
		                                    end
	                                    ) as SkinnerLevel
	                                    from BillService
	                                    where IsDelete != 1 and Pending != 1" + whereStaff + @"
	                                    and CreatedDate between '" + timeFrom + "' and '" + timeTo + @"' 
	                                    and (ServiceIds != '' and ServiceIds is not null)
                                    ) as b
                                    on a.BillId = b.Id
                                    where a.IsDelete != 1";
                                var bills = db.Database.SqlQuery<cls_FlowService>(sql).ToList();

                                sql = @"select COUNT(*)
                                    from
                                    (
                                    select COUNT(*) as times from BillService
                                    where IsDelete != 1 and Pending != 1" + whereStaff + @"
                                    and CreatedDate between '" + timeFrom + "' and '" + timeTo + @"'
                                    and (ServiceIds != '' and ServiceIds is not null)
                                    group by DATEPART(YEAR, CreatedDate), DATEPART(MONTH, CreatedDate), DATEPART(DAY, CreatedDate)
                                    ) as a
                                    where a.times >= 3";
                                workDay = db.Database.SqlQuery<int>(sql).FirstOrDefault();

                                if (bills.Count > 0)
                                {
                                    totalMoney = 0;
                                    moneySales = 0;
                                    money_m = 0;
                                    foreach (var v2 in bills)
                                    {
                                        item = new ItemStatistic();
                                        index = lstStatistic.FindIndex(fi => fi.ItemId == v2.ServiceId);
                                        var payTemp = db.Tbl_Payon.FirstOrDefault(w => w.TypeStaffId == v.Type && w.KeyId == v2.ServiceId && w.ForeignId == v.SkillLevel);
                                        money = payTemp != null ? (int.TryParse(payTemp.Value.ToString(), out integer) ? integer : 0) : 0;

                                        if (index == -1)
                                        {
                                            item.ItemId = Convert.ToInt32(v2.ServiceId);
                                            item.ItemTimes = int.TryParse(v2.Quantity.ToString(), out integer) ? integer : 0;
                                            item.ItemMoney = item.ItemTimes * money;
                                            lstStatistic.Add(item);
                                        }
                                        else
                                        {
                                            itemTemp = new ItemStatistic();
                                            item = lstStatistic.Find(f => f.ItemId == v2.ServiceId);
                                            itemTemp.ItemId = item.ItemId;
                                            itemTemp.ItemTimes = item.ItemTimes + (int.TryParse(v2.Quantity.ToString(), out integer) ? integer : 0);
                                            itemTemp.ItemMoney = item.ItemMoney + item.ItemTimes * money;
                                            lstStatistic[index] = itemTemp;
                                        }
                                        totalMoney += money;
                                        moneySales += int.TryParse((v2.Price * v2.Quantity * (float)(100 - v2.VoucherPercent) / 100).ToString(), out integer) ? integer : 0;

                                        ///# Chỉ số m : lương dịch vụ của Skinner lọc theo các bill Stylist đó làm
                                        /// - Chek theo từng bill có mặt Stylist
                                        /// - Tính lương dịch vụ của Skinner trong bill đó
                                        /// - Map lương dịch vụ vừa tính được vào tổng m
                                        if (v.Type == 1)
                                        {
                                                money_m += (int.TryParse(v2.SkinnerPayon.ToString(), out integer) ? integer : 0) * (int.TryParse(v2.Quantity.ToString(), out integer) ? integer : 0);
                                        }
                                    }

                                    ///### Tính thêm chỉ số l và m
                                    ///# Chỉ số l : 0.5 * (lương cứng Skinner trong đội đó)
                                    /// - Check đối với Stylist
                                    ///     - Kiểm tra lấy Skinner A theo nhóm
                                    ///     - Tính lương cứng của Skinner A này
                                    /// * Trường hợp đặc biệt :
                                    ///     + Thuộc đội 9 - 82 Trần Đại Nghĩa (3 stylist, 2 skinner)
                                    ///     - 2 skinner : Nguyễn Ngọc Diệp (Id = 57), Hà Thị Huế (Id = 29)
                                    ///     - Stylist Đỗ Văn An (Id = 19) : l = 0.5 * ( ( lương cứng skinner 1 + lương cứng skinner 2 ) / 2 )
                                    ///     - Stylist Hoàng Văn Nhất (Id = 127), Nguyễn Hải Nam (Id = 128) : l = 0.25 * ( ( lương cứng skinner 1 + lương cứng skinner 2 ) / 2 )
                                    ///# Chỉ số m : lương dịch vụ của Skinner lọc theo các bill Stylist đó làm,
                                    /// - Chek theo từng bill có mặt Stylist
                                    /// - Tính lương dịch vụ của Skinner trong bill đó
                                    /// - Map lương dịch vụ vừa tính được vào tổng m
                                    skn_FixSalary = 0;
                                    skn_baseSalary = 0;
                                    skn_sql = "";
                                    skn_workDay = 0;
                                    skn_payon = new Tbl_Payon();
                                    skn = new Staff();

                                    if (v.Type == 1)
                                    {
                                        /// Trường hợp đặc biệt : đội 9 - Salon 82 Trần Đại Nghĩa
                                        var stl_d9 = new int[] { 19, 127, 128 };
                                        var skn_d9 = new int[] {57, 29};
                                        if (Array.IndexOf(stl_d9, v.Id) != -1)
                                        {
                                            // Tính lương cứng của skinner Nguyễn Ngọc Diệp
                                            foreach (var skinnerId in skn_d9)
                                            {
                                                skn = db.Staffs.FirstOrDefault(w => w.Id == skinnerId);
                                                if (skn != null)
                                                {
                                                    skn_sql = @"select COUNT(*)
                                                    from
                                                    (
                                                        select COUNT(*) as times from BillService
                                                        where IsDelete != 1 and Pending != 1 and Staff_HairMassage_Id = " + skn.Id +
                                                                    @"and CreatedDate between '" + timeFrom + "' and '" + timeTo + @"'
                                                        and (ServiceIds != '' and ServiceIds is not null)
                                                        group by DATEPART(YEAR, CreatedDate), DATEPART(MONTH, CreatedDate), DATEPART(DAY, CreatedDate)
                                                    ) as a
                                                    where a.times >= 3";
                                                    skn_workDay = db.Database.SqlQuery<int>(skn_sql).FirstOrDefault();
                                                    // Tính lương cứng Skinner                                    
                                                    skn_payon = db.Tbl_Payon.FirstOrDefault(w => w.IsDelete != 1 && w.TypeStaffId == skn.Type && w.ForeignId == skn.SkillLevel && w.Hint == "base_salary");
                                                    if (skn_payon != null)
                                                    {
                                                        skn_baseSalary = int.TryParse(skn_payon.Value.ToString(), out integer) ? integer : 0;
                                                        if (skn_payon.PayByTime == 1) // lương theo tháng
                                                        {
                                                            skn_FixSalary += Convert.ToInt32(((float)skn_workDay / ConfigWorkDay) * skn_payon.Value);
                                                        }
                                                        else if (skn_payon.PayByTime == 2) // lương theo ngày
                                                        {
                                                            skn_FixSalary += Convert.ToInt32(skn_workDay * skn_payon.Value);
                                                        }                                                        
                                                    }
                                                }                                                
                                            }
                                            skn_FixSalary = Convert.ToInt32(skn_FixSalary / 2);

                                            if (v.Id == 19)
                                            {
                                                skn_FixSalary = Convert.ToInt32(skn_FixSalary * 0.5);
                                            }
                                            else
                                            {
                                                skn_FixSalary = Convert.ToInt32(skn_FixSalary * 0.25);
                                            }
                                        }
                                        else
                                        {
                                            SkinnerIdInGroup = int.TryParse(v.SkinnerIdInGroup.ToString(), out integer) ? integer : 0;
                                            skn = db.Staffs.FirstOrDefault(w => w.Id == SkinnerIdInGroup);

                                            if (skn != null)
                                            {
                                                skn_sql = @"select COUNT(*)
                                                    from
                                                    (
                                                        select COUNT(*) as times from BillService
                                                        where IsDelete != 1 and Pending != 1 and Staff_HairMassage_Id = " + SkinnerIdInGroup +
                                                                @"and CreatedDate between '" + timeFrom + "' and '" + timeTo + @"'
                                                        and (ServiceIds != '' and ServiceIds is not null)
                                                        group by DATEPART(YEAR, CreatedDate), DATEPART(MONTH, CreatedDate), DATEPART(DAY, CreatedDate)
                                                    ) as a
                                                    where a.times >= 3";
                                                skn_workDay = db.Database.SqlQuery<int>(skn_sql).FirstOrDefault();
                                                // Tính lương cứng Skinner A                                        
                                                skn_payon = db.Tbl_Payon.FirstOrDefault(w => w.IsDelete != 1 && w.TypeStaffId == skn.Type && w.ForeignId == skn.SkillLevel && w.Hint == "base_salary");
                                                if (skn_payon != null)
                                                {
                                                    skn_baseSalary = int.TryParse(skn_payon.Value.ToString(), out integer) ? integer : 0;
                                                    if (skn_payon.PayByTime == 1) // lương theo tháng
                                                    {
                                                        skn_FixSalary = Convert.ToInt32(((float)skn_workDay / ConfigWorkDay) * skn_payon.Value);
                                                    }
                                                    else if (skn_payon.PayByTime == 2) // lương theo ngày
                                                    {
                                                        skn_FixSalary = Convert.ToInt32(skn_workDay * skn_payon.Value);
                                                    }
                                                    skn_FixSalary = Convert.ToInt32(skn_FixSalary * 0.5);
                                                }
                                            }
                                        }                                        
                                    }
                                    /// Map data cho nhân viên
                                    OBJ_Staff_TimeKeeping = new Staff_TimeKeeping();
                                    OBJ_Staff_TimeKeeping.Id = v.Id;
                                    OBJ_Staff_TimeKeeping.Fullname = v.Fullname;
                                    OBJ_Staff_TimeKeeping.TypeName = v.StaffTypeName;
                                    OBJ_Staff_TimeKeeping.SkillLevelName = v.SkillLevelName;
                                    OBJ_Staff_TimeKeeping._ServiceStatistic = lstStatistic;
                                    OBJ_Staff_TimeKeeping.StrDataService = strStatistic;
                                    OBJ_Staff_TimeKeeping.TotalMoney = totalMoney;
                                    OBJ_Staff_TimeKeeping.Salary_Service = totalMoney;
                                    OBJ_Staff_TimeKeeping.WorkDay = workDay;
                                    OBJ_Staff_TimeKeeping.WorkHour = workHour;
                                    OBJ_Staff_TimeKeeping.SalesService = moneySales;
                                    OBJ_Staff_TimeKeeping.IndexLBySkinner = skn_FixSalary;
                                    OBJ_Staff_TimeKeeping.IndexMBySkinner = money_m;
                                    TK_Serivce.Add(OBJ_Staff_TimeKeeping);
                                }
                            }

                            /// 2.Products
                            _ProductTmp = new ItemStatistic();
                            if (v.SkillLevel == 2 && v.Type == 2)
                            {
                                PayPercent = 12;
                            }
                            else
                            {
                                PayPercent = 10;
                            }
                            sql = @"select a.*, Cast(Coalesce(c.ForSalary, 0) as float) as ForSalary
                                    from FlowProduct as a
                                    inner join
                                    (
	                                    select Id, ProductIds, TotalMoney, SalonId, CreatedDate
	                                    from BillService
	                                    where SellerId = " + v.Id + @"
	                                    and IsDelete != 1 and Pending != 1
	                                    and CreatedDate between '" + timeFrom + "' and '" + timeTo + @"' 
	                                    and (ProductIds != '' and ProductIds is not null)
                                    ) as b
                                    on a.BillId = b.Id
                                    left join Product as c
                                    on a.ProductId = c.Id
                                    where a.IsDelete != 1
                                    and (a.ProductId not in (74,82))";
                            var bills_product = db.Database.SqlQuery<cls_flowproduct>(sql).ToList();

                            sql = @"select a.BillId
                                    from FlowProduct as a
                                    inner join
                                    (
	                                    select Id, ProductIds, TotalMoney, SalonId, CreatedDate
	                                    from BillService
	                                    where SellerId = " + v.Id + @"
	                                    and IsDelete != 1 and Pending != 1
	                                    and CreatedDate between '" + timeFrom + "' and '" + timeTo + @"' 
	                                    and (ProductIds != '' and ProductIds is not null)
                                    ) as b
                                    on a.BillId = b.Id
                                    where a.IsDelete != 1
                                    and (a.ProductId in (74,82))
                                    group by BillId";
                            var bills_promotion = db.Database.SqlQuery<int>(sql).ToList();

                            if (bills_product.Count > 0)
                            {
                                lstStatistic = new List<ItemStatistic>();
                                var strproduct = "";
                                totalMoney = 0;
                                moneySales = 0;
                                int totalPromotion = 0;
                                foreach (var v2 in bills_product)
                                {
                                    item = new ItemStatistic();
                                    index = lstStatistic.FindIndex(fi => fi.ItemId == v2.ProductId);
                                    index2 = bills_promotion.FindIndex(fi => fi == v2.BillId);
                                    if (index2 != -1)
                                    {
                                        money = 10000;
                                        totalPromotion++;
                                    }
                                    else
                                    {
                                        money = Convert.ToInt32(v2.Price * v2.Quantity * (float)(100 - v2.VoucherPercent) / 100 * (float)v2.ForSalary / 100);
                                    }

                                    if (index == -1)
                                    {
                                        item.ItemId = Convert.ToInt32(v2.ProductId);
                                        item.ItemTimes = int.TryParse(v2.Quantity.ToString(), out integer) ? integer : 0;
                                        item.ItemMoney = item.ItemTimes * money;
                                        lstStatistic.Add(item);
                                    }
                                    else
                                    {
                                        _ProductTmp = new ItemStatistic();
                                        item = lstStatistic.Find(f => f.ItemId == v2.ProductId);
                                        _ProductTmp.ItemId = item.ItemId;
                                        _ProductTmp.ItemTimes = item.ItemTimes + (int.TryParse(v2.Quantity.ToString(), out integer) ? integer : 0);
                                        _ProductTmp.ItemMoney = item.ItemMoney + _ProductTmp.ItemTimes * money;
                                        lstStatistic.Add(item);
                                        lstStatistic[index] = _ProductTmp;
                                    }
                                    totalMoney += money;
                                    moneySales += int.TryParse((v2.Price * v2.Quantity * (float)(100 - v2.VoucherPercent) / 100).ToString(), out integer) ? integer : 0;
                                    if (v2.PromotionMoney > 0)
                                    {
                                        moneySales -= int.TryParse((v2.PromotionMoney * v2.Quantity).ToString(), out integer) ? integer : 0;
                                    }
                                }

                                OBJ_Staff_TimeKeeping = new Staff_TimeKeeping();
                                OBJ_Staff_TimeKeeping.Id = v.Id;
                                OBJ_Staff_TimeKeeping.Fullname = v.Fullname;
                                OBJ_Staff_TimeKeeping.TypeName = v.StaffTypeName;
                                OBJ_Staff_TimeKeeping.SkillLevelName = v.SkillLevelName;
                                OBJ_Staff_TimeKeeping._ProductStatistic = lstStatistic;
                                OBJ_Staff_TimeKeeping.StrDataService = strproduct;
                                OBJ_Staff_TimeKeeping.TotalPromotion = totalPromotion;
                                OBJ_Staff_TimeKeeping.TotalMoney = totalMoney;
                                OBJ_Staff_TimeKeeping.Salary_Product = totalMoney;
                                OBJ_Staff_TimeKeeping.SalesProduct = moneySales;
                                TK_Product.Add(OBJ_Staff_TimeKeeping);
                            }
                        }
                    }

                    // Merge staff
                    if (LST0.Count > 0)
                    {
                        var loop = 0;
                        int fixSalary = 0;
                        int baseSalary = 0;
                        int partTimeSalary = 0;
                        int allowanceSalary = 0;
                        foreach (var v in LST0)
                        {
                            fixSalary = 0;
                            baseSalary = 0;                                                    

                            /// Service 
                            index = TK_Serivce.FindIndex(fi => fi.Id == v.Id);
                            if (index == -1)
                            {
                                OBJ_Staff_TimeKeeping = new Staff_TimeKeeping();
                                OBJ_Staff_TimeKeeping.Id = v.Id;
                                OBJ_Staff_TimeKeeping.Fullname = v.Fullname;
                                OBJ_Staff_TimeKeeping.TypeName = v.StaffTypeName;
                                OBJ_Staff_TimeKeeping.SkillLevelName = v.SkillLevelName;
                                OBJ_Staff_TimeKeeping.IdShowroom = int.TryParse(v.IdShowroom.ToString(), out integer) ? integer : 0;
                                OBJ_Staff_TimeKeeping.WorkDay = 0;
                                OBJ_Staff_TimeKeeping.Salary_Service = 0;
                                OBJ_Staff_TimeKeeping.Salary_Product = 0;
                            }
                            else
                            {
                                OBJ_Staff_TimeKeeping = TK_Serivce[index];                                
                                //loop++;
                            }

                            // Tính workDay cho nhân viên tính công theo điểm danh ngày (Lễ tân, bảo vệ)
                            if (v.Type != 1 && v.Type != 2)
                            {
                                sql = @"select Count(*)
                                            from FlowTimeKeeping
                                            where IsDelete != 1  and StaffId = " + v.Id + @"  
                                            and WorkDate >= '" + timeFrom + "' and WorkDate < '" + timeTo + "'";
                                OBJ_Staff_TimeKeeping.WorkDay = db.Database.SqlQuery<int>(sql).FirstOrDefault();
                            }
                            /// Tính công part-time cho nhân viên
                            sql = @"select Coalesce(SUM(WorkHour),0) as workHour from FlowTimeKeeping 
                                    where IsDelete != 1 and WorkDate >= '" + timeFrom + "' and WorkDate < '" + timeTo + @"'
                                    and StaffId = " + v.Id;
                            workHour = db.Database.SqlQuery<int>(sql).FirstOrDefault();
                            // Calculate part-time salary
                            var partTimePayon = db.Tbl_Payon.FirstOrDefault(w => w.IsDelete != 1 && w.TypeStaffId == v.Type && w.ForeignId == v.SkillLevel && w.Hint == "base_salary" && w.PayByTime == 3);
                            if (partTimePayon != null)
                            {
                                partTimeSalary = (int.TryParse(partTimePayon.Value.ToString(), out integer) ? integer : 0) * workHour;
                            }
                            // Calculate base salary
                            var payon = db.Tbl_Payon.FirstOrDefault(w => w.IsDelete != 1 && w.TypeStaffId == v.Type && w.ForeignId == v.SkillLevel && w.Hint == "base_salary");
                            if (payon != null)
                            {
                                baseSalary = int.TryParse(payon.Value.ToString(), out integer) ? integer : 0;
                                if (payon.PayByTime == 1)
                                {
                                    fixSalary = Convert.ToInt32(((float)OBJ_Staff_TimeKeeping.WorkDay / ConfigWorkDay) * payon.Value);
                                }
                                else if (payon.PayByTime == 2)
                                {
                                    fixSalary = Convert.ToInt32(OBJ_Staff_TimeKeeping.WorkDay * payon.Value);
                                }
                            }
                            // Calculate phụ cấp
                            var allowance = db.Tbl_Payon.FirstOrDefault(w => w.IsDelete != 1 && w.Publish == true && w.TypeStaffId == v.Type && w.ForeignId == v.SkillLevel && w.PayByTime == 4);
                            if (allowance != null)
                            {
                                allowanceSalary = Convert.ToInt32(((float)(d2 - d1).Days / ConfigWorkDay) * allowance.Value);
                            }

                            OBJ_Staff_TimeKeeping.Salary_Base = baseSalary;
                            OBJ_Staff_TimeKeeping.Salary_Fix = fixSalary;
                            OBJ_Staff_TimeKeeping.Salary_PartTime = partTimeSalary;
                            OBJ_Staff_TimeKeeping.Salary_Allowance = allowanceSalary;
                            OBJ_Staff_TimeKeeping.Salary_Total = fixSalary + OBJ_Staff_TimeKeeping.Salary_Service + partTimeSalary + allowanceSalary;                            
                            LST_Staff.Add(OBJ_Staff_TimeKeeping);                            

                            /// Product
                            index = TK_Product.FindIndex(fi => fi.Id == v.Id);
                            if (index != -1)
                            {
                                var temp = LST_Staff.Last();
                                temp.Salary_Product = TK_Product[index].Salary_Product;
                                temp.Salary_Total = temp.Salary_Total + temp.Salary_Product;
                                temp.SalesProduct = TK_Product[index].SalesProduct;
                                LST_Staff[loop] = temp;
                            }

                            loop++;
                        }
                    }
                }

                return LST_Staff;
            }
        }        

        //        public int CalculateSalaryByDate(string timeFrom, string timeTo, int payType, int baseSalary)
        //        {
        //            using(var db = new Solution_30shineEntities())
        //            {
        //                int salary;            
        //                DateTime dtFrom = Convert.ToDateTime(timeFrom, culture);
        //                DateTime dtTo = Convert.ToDateTime(timeTo, culture);
        //                int mon = dtFrom.Month;
        //                int year = dtFrom.Year;
        //                int workDays = 0;
        //                int integer;
        //                if (dtFrom < dtTo)
        //                {
        //                    if (dtFrom.Year == dtTo.Year && dtFrom.Month == dtTo.Month)
        //                    {
        //                        string sql = @"select COUNT(*)
        //                                    from
        //                                    (
        //                                    select COUNT(*) as times from BillService
        //                                    where IsDelete != 1 and Pending != 1" + whereStaff + @"
        //                                    and CreatedDate between '" + timeFrom + "' and '" + timeTo + @"'
        //                                    and (ServiceIds != '' and ServiceIds is not null)
        //                                    group by DATEPART(YEAR, CreatedDate), DATEPART(MONTH, CreatedDate), DATEPART(DAY, CreatedDate)
        //                                    ) as a
        //                                    where a.times >= 3";
        //                        workDay = db.Database.SqlQuery<int>(sql).FirstOrDefault();
        //                        var wds = db.Tbl_ConfigWorkDays.FirstOrDefault( w=>w.Year == year && w.Month == mon );
        //                        if (wds != null)
        //                        {
        //                            workDays = int.TryParse(wds.WorkDays.ToString(), out integer) ? integer : DateTime.DaysInMonth(year, mon) - 2;
        //                            if (payType == 1)
        //                            {
        //                                salary = Convert.ToInt32(((float)workDays / dayinmonth) * payon.Value);
        //                            }
        //                            else if (payType == 2)
        //                            {
        //                                salary = Convert.ToInt32(OBJ_Staff_TimeKeeping.WorkDay * payon.Value);
        //                            }
        //                        }
        //                    }
        //                }   
        //            }            
        //        }

        private void Bind_All_ByDay()
        {
            using (var db = new Solution_30shineEntities())
            {
                var LST = db.Staffs.AsExpandable().Where(WhereStaff).OrderBy(o => o.Id).ToList();
                Rpt_Staff.DataSource = LST;
                Rpt_Staff.DataBind();
            }
        }

        public string Get_Services()
        {
            using (var db = new Solution_30shineEntities())
            {
                var str = "";
                DateTime d1 = Convert.ToDateTime(TxtDateTimeFrom.Text.Trim(), culture);
                DateTime d2;
                if (TxtDateTimeTo.Text.Trim() != "")
                {
                    d2 = Convert.ToDateTime(TxtDateTimeTo.Text.Trim(), culture).AddDays(1);
                }
                else
                {
                    d2 = d1.AddDays(1);
                }
                string timeFrom = String.Format("{0:MM/dd/yyyy}", d1);
                string timeTo = String.Format("{0:MM/dd/yyyy}", d2);

                string sql_service = @"select b.* 
                                    from
                                    (
                                    select ServiceId from FlowService
                                    where IsDelete != 1
                                    and (CreatedDate between '" + timeFrom + "' and '" + timeTo + "')" +
                                        @"group by ServiceId
                                    ) as a
                                    join Service as b
                                    on a.ServiceId = b.Id";
                _Service = db.Services.SqlQuery(sql_service).ToList();
                if (_Service.Count > 0)
                {
                    foreach (var v in _Service)
                    {
                        str += "<th>" + v.Name + "</th>";
                    }
                }
                return str;
            }
        }

        public string Get_Products()
        {
            using (var db = new Solution_30shineEntities())
            {
                var str = "";
                DateTime d1 = Convert.ToDateTime(TxtDateTimeFrom.Text.Trim(), culture);
                DateTime d2;
                if (TxtDateTimeTo.Text.Trim() != "")
                {
                    d2 = Convert.ToDateTime(TxtDateTimeTo.Text.Trim(), culture).AddDays(1);
                }
                else
                {
                    d2 = d1.AddDays(1);
                }
                string timeFrom = String.Format("{0:MM/dd/yyyy}", d1);
                string timeTo = String.Format("{0:MM/dd/yyyy}", d2);

                string sql_product = @"select b.* 
                                    from
                                    (
                                    select ProductId from FlowProduct
                                    where IsDelete != 1
                                    and (CreatedDate between '" + timeFrom + "' and '" + timeTo + "')" +
                                        @"group by ProductId
                                    ) as a
                                    join Product as b
                                    on a.ProductId = b.Id";
                _Product = db.Products.SqlQuery(sql_product).ToList();
                if (_Product.Count > 0)
                {
                    foreach (var v in _Product)
                    {
                        str += "<th>" + v.Name + "</th>";
                    }
                }
                return str;
            }
        }

        public void Bind_TypeStaff()
        {
            using (var db = new Solution_30shineEntities())
            {
                var LST = db.Staff_Type.Where(w => w.IsDelete != 1 && w.Publish == true).OrderBy(o => o.Order).ToList();
                Rpt_StaffTye.DataSource = LST;
                Rpt_StaffTye.DataBind();
            }
        }

        [WebMethod]
        public static string Load_Staff_ByType(string type)
        {
            {
                var _Msg = new Msg();
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                var Type = Convert.ToInt32(type);
                using (var db = new Solution_30shineEntities())
                {
                    var WhereLoadStaff = PredicateBuilder.True<Staff>();
                    WhereLoadStaff = WhereLoadStaff.And(w => w.Type == Type && w.Permission != "admin" && w.Permission != "accountant");
                    if (HttpContext.Current.Session["SalonId"] != null)
                    {
                        int integer;
                        int Salonid = int.TryParse(HttpContext.Current.Session["SalonId"].ToString(), out integer) ? integer : 0;
                        if (Salonid > 0)
                        {
                            WhereLoadStaff = WhereLoadStaff.And(w => w.SalonId == Salonid);
                        }
                    }
                    var LST = db.Staffs.AsExpandable().Where(WhereLoadStaff).Select(s => new { s.Id, s.Fullname, s.Code }).ToList();

                    if (LST.Count > 0)
                    {
                        _Msg.success = true;
                        _Msg.msg = serializer.Serialize(LST);
                    }
                    else
                    {
                        _Msg.success = false;
                    }
                }
                return serializer.Serialize(_Msg);
            }
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            Bind_Paging();
            Calculate_Salary();
            //bindTotalByDepartment();
            ScriptManager.RegisterStartupScript(this, GetType(), "Script", "SetActiveTableListing();ReplaceZezoValue();", true);
            RemoveLoading();
        }

        protected void Bind_Paging()
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? (!HDF_Page.Value.Equals("") ? Convert.ToInt32(HDF_Page.Value) : 1) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage() : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage()
        {
            using (var db = new Solution_30shineEntities())
            {
                _TypeStaff = HDF_TypeStaff.Value != "" ? Convert.ToInt32(HDF_TypeStaff.Value) : 0;
                _StaffId = HDF_Staff.Value != "" ? Convert.ToInt32(HDF_Staff.Value) : 0;

                CultureInfo culture = new CultureInfo("vi-VN");
                //var WhereStaff = PredicateBuilder.True<Staff>();
                int Count = 0;

                //if (_StaffId > 0)
                //{
                //    WhereStaff = WhereStaff.And(w => w.Id == _StaffId);
                //}
                //if (_TypeStaff > 0)
                //{
                //    WhereStaff = WhereStaff.And(w => w.Type == _TypeStaff);
                //}
                //WhereStaff = WhereStaff.And(w => w.IsDelete != 1);

                // Get staff
                if (TxtDateTimeFrom.Text != "")
                {
                    Count = db.Staffs.AsExpandable().Count(WhereStaff);
                }

                int TotalRow = Count - PAGING._TopNewsNum;
                int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
                return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
            }
        }

        protected void Exc_ExportExcel(object sender, EventArgs e)
        {
            using (var db = new Solution_30shineEntities())
            {
                var _ExcelHeadRow = new List<string>();
                var serializer = new JavaScriptSerializer();
                var BillRowLST = new List<List<string>>();
                var BillRow = new List<string>();
                int t;
                _ExcelHeadRow.Add("Tên nhân viên");
                _ExcelHeadRow.Add("Bộ phận");
                _ExcelHeadRow.Add("Bậc kỹ năng");
                _ExcelHeadRow.Add("Tổng công tháng " + ThisMon.ToString());
                _ExcelHeadRow.Add("Chấm công");
                _ExcelHeadRow.Add("Lương cơ bản");
                _ExcelHeadRow.Add("Lương cứng");
                _ExcelHeadRow.Add("Lương dịch vụ");
                _ExcelHeadRow.Add("Lương sản phẩm");
                _ExcelHeadRow.Add("Tổng lương");
                _ExcelHeadRow.Add("Doanh thu DV");
                _ExcelHeadRow.Add("Doanh thu SP");
                _ExcelHeadRow.Add("Hệ số DV");
                //_ExcelHeadRow.Add("Hệ số SP");
                _ExcelHeadRow.Add("l");
                _ExcelHeadRow.Add("m");

                var LSTStaff = Get_DataBy_Filter(false);
                if (LSTStaff.Count > 0)
                {
                    foreach (var v in LSTStaff)
                    {
                        BillRow = new List<string>();
                        BillRow.Add(v.Fullname);
                        BillRow.Add(v.TypeName);
                        BillRow.Add(v.SkillLevelName);
                        BillRow.Add((DayOfMon - 2).ToString());
                        BillRow.Add(v.WorkDay.ToString());
                        BillRow.Add(v.Salary_Base.ToString());
                        BillRow.Add(v.Salary_Fix.ToString());
                        BillRow.Add(v.Salary_Service.ToString());
                        BillRow.Add(v.Salary_Product.ToString());
                        BillRow.Add(v.Salary_Total.ToString());
                        BillRow.Add(v.SalesService.ToString());
                        BillRow.Add(v.SalesProduct.ToString());
                        t = v.Salary_Fix + v.Salary_Service + v.IndexLBySkinner + v.IndexMBySkinner;
                        if (t > 0)
                        {
                            BillRow.Add(((float)v.SalesService / t).ToString());
                        }
                        else
                        {
                            BillRow.Add("0");
                        }

                        //if ((v.Salary_Fix + v.Salary_Product) > 0)
                        //{
                        //    BillRow.Add(((float)v.SalesProduct / (v.Salary_Fix + v.Salary_Product)).ToString());
                        //}
                        //else
                        //{
                        //    BillRow.Add("0");
                        //}

                        BillRow.Add(v.IndexLBySkinner.ToString());
                        BillRow.Add(v.IndexMBySkinner.ToString());

                        BillRowLST.Add(BillRow);
                    }
                }

                // export
                var ExcelStorePath = Server.MapPath("~") + "Public/Excel/Nhan.Vien/";
                var FileName = "Hoa_Don_" + string.Format("{0:dd_MM_yyyy_hh_mm_ss}", DateTime.Now) + ".xlsx";
                var UrlFileName = "http://" + Request.Url.Host + "/Public/Excel/Nhan.Vien/" + FileName;

                ExportXcel(_ExcelHeadRow, BillRowLST, ExcelStorePath + FileName);
                Bind_Paging();
                ScriptManager.RegisterStartupScript(BtnFakeExcel, this.GetType(), "Download Excel", "CallBackExportExcel('" + UrlFileName + "');UP_FormatPrice('.be-report-price'); SubTableFixHeight();SetActiveTableListing();", true);
            }
        }

        private static void ExportXcel(List<string> _ExcelHeadRow, List<List<string>> List, string Path)
        {
            try
            {
                CreateExcelFile.ExcelHeadRow = _ExcelHeadRow;
                CreateExcelFile.CreateExcel(List, Path);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi. Không tạo được file excel.\r\nException: " + ex.Message);
            }
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        /// <summary>
        /// Check permission
        /// </summary>
        private void CheckPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                string[] Allow = new string[] { "root", "admin", "accountant", "salonmanager" };
                string[] AllowViewAllDate = new string[] { "root", "admin", "accountant"};
                var Permission = Session["User_Permission"].ToString().Trim();
                if (Array.IndexOf(Allow, Permission) != -1)
                {
                    Perm_Access = true;
                }

                if (Array.IndexOf(AllowViewAllDate, Permission) != -1)
                {
                    Perm_ViewAllData = true;
                }
            }

            // Call execute function
            ExecuteByPermission();
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
    }

    public struct Staff_TimeKeeping
    {
        public int Id { get; set; }
        public string Fullname { get; set; }
        public string TypeName { get; set; }
        public string SkillLevelName { get; set; }
        public int IdShowroom { get; set; }
        public List<int> ServiceIds { get; set; }
        public List<ItemStatistic> _ServiceStatistic { get; set; }
        public List<ItemStatistic> _ProductStatistic { get; set; }
        public int TotalService { get; set; }
        public string StrDataService { get; set; }
        public string StrTotalService { get; set; }
        public int TotalPromotion { get; set; }
        /// <summary>
        /// Tổng doanh thu
        /// </summary>
        public int TotalMoney { get; set; }
        /// <summary>
        /// Lương dịch vụ
        /// </summary>
        public int Salary_Service { get; set; }
        /// <summary>
        /// Lương sản phẩm
        /// </summary>
        public int Salary_Product { get; set; }
        /// <summary>
        /// Lương part-time
        /// </summary>
        public int Salary_PartTime { get; set; }
        /// <summary>
        /// Lương phụ cấp
        /// </summary>
        public int Salary_Allowance { get; set; }        
        /// <summary>
        /// Lương cơ bản (lương tháng, lương ngày)
        /// </summary>
        public int Salary_Base { get; set; }
        /// <summary>
        /// Lương cứng (lương tính theo ngày công)
        /// </summary>
        public int Salary_Fix { get; set; }
        /// <summary>
        /// Tổng lương
        /// </summary>
        public int Salary_Total { get; set; }
        /// <summary>
        /// Số ngày công
        /// </summary>
        public int WorkDay { get; set; }
        /// <summary>
        /// Số giờ làm việc part-time, làm tăng ca
        /// </summary>
        public int WorkHour { get; set; }
        /// <summary>
        /// Doanh thu dịch vụ
        /// </summary>
        public int SalesService { get; set; }
        /// <summary>
        /// Doanh thu sản phẩm
        /// </summary>
        public int SalesProduct { get; set; }
        /// <summary>
        /// Chỉ số l theo skinner
        /// </summary>
        public int IndexLBySkinner { get; set; }
        /// <summary>
        /// Chỉ số m theo skinner
        /// </summary>
        public int IndexMBySkinner { get; set; }
    }

    public struct Salary
    { 
        
    }

    public struct ServiceStatistic
    {
        public int ServiceId { get; set; }
        public int ServiceName { get; set; }
        public int ServiceTimes { get; set; }
        public int ServiceMoney { get; set; }
    }

    public struct ProductStatistic
    {
        public int ProductId { get; set; }
        public int ProductName { get; set; }
        public int ProductTimes { get; set; }
        public int ProductMoney { get; set; }
    }

    public struct ItemStatistic
    {
        public int ItemId { get; set; }
        public string ItemName { get; set; }
        public int ItemTimes { get; set; }
        public int ItemMoney { get; set; }
    }

    public struct BillServiceJoin
    {
        public int Id { get; set; }
        public string Fullname { get; set; }
        public int Type { get; set; }
        public int IdShowroom { get; set; }
        public int BillIsDelete { get; set; }
        public int BillId { get; set; }
        public DateTime BillCreatedDate { get; set; }
    }

    public class cls_FlowService : FlowService
    {
        public int SkinnerId { get; set; }
        public int SkinnerPayon { get; set; }
    }

    public class cls_flowproduct : FlowProduct
    {
        public double ForSalary { get; set; }
    }

    public class cls_department:Staff_Type
    {
        /// <summary>
        /// Tổng lương theo từng bộ phận (không tính lương mỹ phẩm)
        /// </summary>
        public long totalSalaryNoProduct { get; set; }
    }
}