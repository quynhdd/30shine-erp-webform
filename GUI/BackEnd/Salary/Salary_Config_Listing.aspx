﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Salary_Config_Listing.aspx.cs" Inherits="_30shine.GUI.BackEnd.Salary.Salary_Config_Listing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý cấu hình lương chung</li>
                        <li class="li-listing active"><a href="/admin/cau-hinh-luong/bo-phan/danh-sach.html">Danh sách</a></li>
                        <li class="li-add"><a href="/admin/cau-hinh-luong/bo-phan/them-moi.html">Thêm mới</a></li>
                    </ul>
                </div>
            </div>
        </div>


        <div class="wp customer-add customer-listing be-report">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <span style="display: none"></span>
                <!-- Filter -->
                <div class="row row-filter">
                    <div class="filter-item">
                        <asp:DropDownList ID="ddlDepartment" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 150px;"></asp:DropDownList>
                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="ddlLevel" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 150px;"></asp:DropDownList>
                    </div>
                    <asp:Panel ID="ViewDataFilter" CssClass="st-head btn-viewdata" ClientIDMode="Static"
                        onclick="excPaging(1)" runat="server">
                        Xem dữ liệu
                    </asp:Panel>
                </div>
                <!-- End Filter -->

                <!-- Row Table Filter -->
                <div class="table-func-panel">
                    <div class="table-func-elm">
                        <span>Số hàng / Page : </span>
                        <div class="table-func-input-wp">
                            <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                            <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                            <ul class="ul-opt-segment">
                                <li data-value="10">10</li>
                                <li data-value="20">20</li>
                                <li data-value="30">30</li>
                                <li data-value="40">40</li>
                                <li data-value="50">50</li>
                                <li data-value="1000000">Tất cả</li>
                            </ul>
                            <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                        </div>
                    </div>
                </div>
                <!-- End Row Table Filter -->
                <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
                <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                    <ContentTemplate>
                        <div class="table-wp">
                            <table class="table-add table-listing">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Hình thức</th>
                                        <th>Bộ phận</th>
                                        <th>Bậc</th>
                                        <th>Lương cứng</th>
                                        <th>Tiền trợ cấp</th>
                                        <th>Tiền tăng ca</th>
                                        <th>Hệ số Ratting</th>
                                        <th>Tiền % sản phẩm</th>
                                        <th>Lương 5C</th>
                                        <th>Chức năng</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="Rpt" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# Container.ItemIndex + 1 + PAGING._Offset %></td>
                                                <td><%# Eval("NameTypeWork") %></a></td>
                                                <td><%# Eval("DepartmentName") %></td>
                                                <td><%# Eval("LevelName") %></td>
                                                <td><%#  String.Format("{0:#,###}", Eval("FixedSalary")).Replace(',', '.') %></td>
                                                <td><%#  String.Format("{0:#,###}",Eval("AllowanceSalary")).Replace(',', '.')  %></td>
                                                <td><%#  String.Format("{0:#,###}",Eval("OvertimeSalary")).Replace(',', '.')  %></td>
                                                <td><%#  String.Format("{0:#,###}",Eval("RattingSalary")).Replace(',', '.')  %></td>
                                                <td><%#  String.Format("{0:#,###}",Eval("ProductBonus")).Replace(',', '.')  %></td>
                                                <td><%#  String.Format("{0:#,###}",Eval("BehaveSalary")).Replace(',', '.')  %></td>
                                                <td class="map-edit" style="width: 100px">
                                                    <div class="edit-wp">

                                                        <a class="elm edit-btn" href="/admin/cau-hinh-luong/bo-phan/<%# Eval("Id") %>.html" title="Sửa"></a>

                                                        <%--  <a class="elm del-btn" onclick="del(this.parentNode.parentNode.parentNode,'<%# Eval("Id") %>', '<%# Eval("Fullname") %>')" href="javascript://" title="Xóa"></a>--%>
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>

                        <!-- Paging -->
                        <div class="site-paging-wp">
                            <% if (PAGING.TotalPage > 1)
                                { %>
                            <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                                <% if (PAGING._Paging.Prev != 0)
                                    { %>
                                <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                                <% } %>
                                <asp:Repeater ID="RptPaging" runat="server">
                                    <ItemTemplate>
                                        <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                            <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>>
                                            <%# Eval("PageNum") %>
                                        </a>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                                    { %>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                                <% } %>
                            </asp:Panel>
                            <% } %>
                        </div>
                        <!-- End Paging -->
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
            </div>
        </div>
    </asp:Panel>
</asp:Content>
