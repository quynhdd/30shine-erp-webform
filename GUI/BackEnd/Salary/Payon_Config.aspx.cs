﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Linq.Expressions;
using LinqKit;
using System.Web.Services;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.Salary
{
    public partial class Payon_Config : System.Web.UI.Page
    {
        private string PageID = "QL_NS_CHL";
        protected Paging PAGING = new Paging();
        private Expression<Func<Staff, bool>> Where = PredicateBuilder.True<Staff>();
        private bool Perm_Access = false;
        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        protected bool Perm_Add = false;
        protected int SalonId;
        protected List<KeyValuePair<int, string>> payMethods = new List<KeyValuePair<int, string>>();
        protected List<cls_departmentPayon> listPayon = new List<cls_departmentPayon>();
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //if (Request.QueryString["Code"] != null)
                //{
                //    PageID = "PAYON_EDIT";
                //}
                //else
                //{
                //    PageID = "PAYON_ADD";
                //}
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                //Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                //Perm_Add = permissionModel.GetActionByActionNameAndPageId("Perm_Add", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                Perm_Add = permissionModel.CheckPermisionByAction("Perm_Add", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        private void ExecuteByPermission()
        {

            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (!IsPostBack)
            {
                Bind_StaffType();
                Bind_Staff();
                //listPayon = getDataPayon();
                //rpt_payMethod.DataSource = payMethods;
                //rpt_payMethod.DataBind();
            }
        }

        private void Bind_StaffType()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Staff_Type.Where(w => w.IsDelete != 1).OrderBy(o => o.Id).ToList();
                StaffType.DataTextField = "Name";
                StaffType.DataValueField = "Id";
                StaffType.DataSource = lst;
                StaffType.DataBind();
                var item = new ListItem("Theo bộ phận", "0");
                StaffType.Items.Insert(0, item);
            }
        }

        private void Bind_Staff()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Staffs.Where(w => w.IsDelete != 1 && w.Active == 1 && w.SalaryByPerson == true).OrderBy(w => w.Fullname).ToList();
                ddlStaff.DataTextField = "Fullname";
                ddlStaff.DataValueField = "Id";
                ddlStaff.DataSource = lst;
                ddlStaff.DataBind();
                var item = new ListItem("Theo nhân sự", "0");
                ddlStaff.Items.Insert(0, item);
            }
        }

        /// <summary>
        /// Load cấu hình theo nhân sự
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void loadDataByPerson(object sender, EventArgs e)
        {
            Bind_StaffType();
            listPayon = getDataPayonByPerson();
            rpt_payMethod.DataSource = payMethods;
            rpt_payMethod.DataBind();
        }

        /// <summary>
        /// Load cấu hình theo bộ phận
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void loadDataByDepartment(object sender, EventArgs e)
        {
            Bind_Staff();
            listPayon = getDataPayonByDeparment();
            rpt_payMethod.DataSource = payMethods;
            rpt_payMethod.DataBind();
        }

        /// <summary>
        /// Xử lý load dữ liệu cấu hình
        /// </summary>
        /// <returns></returns>
        private List<cls_departmentPayon> getDataPayonByDeparment()
        {
            using (var db = new Solution_30shineEntities())
            {
                var list = new List<cls_departmentPayon>();
                var item = new cls_departmentPayon();
                var temp = new List<Tbl_Payon>();
                var payonSet = new List<KeyValuePair<int, int>>();
                int loop = 0;
                bool flag = true;

                // Lấy lương cứng (theo tháng), lương part-time, lương phụ cấp
                int department = StaffType.SelectedValue != null ? Convert.ToInt32(StaffType.SelectedValue) : 0;
                var payon = db.Tbl_Payon.Where(w => w.TypeStaffId == department && w.Hint == "base_salary" && w.StaffId == null).OrderBy(o => o.ForeignId).ToList();
                var skillLevels = getSkillLevel(department, db);
                if (skillLevels.Count > 0)
                {
                    foreach (var v in skillLevels)
                    {
                        flag = loop++ > 0 ? false : true;
                        payonSet = new List<KeyValuePair<int, int>>();
                        item = new cls_departmentPayon();
                        item.departmentName = v.Name;
                        temp = payon.Where(w => w.ForeignId == v.Id).OrderBy(o => o.PayByTime).ToList();
                        if (temp.Count > 0)
                        {
                            foreach (var v2 in temp)
                            {
                                payonSet.Add(new KeyValuePair<int, int>(v2.Id, v2.Value.Value));
                                if (flag)
                                {
                                    payMethods.Add(new KeyValuePair<int, string>(Convert.ToInt32(v2.PayByTime), v2.Description));
                                }
                            }
                            if (flag)
                            {
                                payMethods.Insert(0, new KeyValuePair<int, string>(Convert.ToInt32(0), "Bậc kỹ năng"));
                            }
                        }
                        item.payonSet = payonSet;
                        item.payMethod = payMethods;
                        list.Add(item);
                    }
                }
                else
                {
                    // Giá trị mặc định ForeignId = 0
                    payonSet = new List<KeyValuePair<int, int>>();
                    item = new cls_departmentPayon();
                    item.departmentName = "Mặc định";
                    if (payon.Count > 0)
                    {
                        foreach (var v2 in payon)
                        {
                            payonSet.Add(new KeyValuePair<int, int>(v2.Id, v2.Value.Value));
                            payMethods.Add(new KeyValuePair<int, string>(Convert.ToInt32(v2.PayByTime), v2.Description));
                        }
                        payMethods.Insert(0, new KeyValuePair<int, string>(Convert.ToInt32(0), "Bậc kỹ năng"));
                    }
                    item.payonSet = payonSet;
                    item.payMethod = payMethods;
                    list.Add(item);
                }
                return list;
            }
        }

        private List<cls_departmentPayon> getDataPayonByPerson()
        {
            using (var db = new Solution_30shineEntities())
            {
                var list = new List<cls_departmentPayon>();
                var item = new cls_departmentPayon();
                var temp = new List<Tbl_Payon>();
                var payonSet = new List<KeyValuePair<int, int>>();

                // Lấy lương cứng (theo tháng), lương part-time, lương phụ cấp
                int staffId = ddlStaff.SelectedValue != null ? Convert.ToInt32(ddlStaff.SelectedValue) : 0;
                var payon = db.Tbl_Payon.Where(w => w.StaffId == staffId && w.Hint == "base_salary").OrderBy(o => o.PayByTime).ToList();

                if (payon.Count == 0)
                {
                    var lst = db.Staffs.Where(w => w.IsDelete != 1 && w.Active == 1 && w.SalaryByPerson == true && w.Id == staffId).SingleOrDefault();
                    if (lst != null)
                    {
                        //khởi tạo lương cứng
                        var obj = new Tbl_Payon();
                        obj.TypeStaffId = lst.Type;
                        obj.ForeignId = 0;
                        obj.Value = 0;
                        obj.Hint = "base_salary";
                        obj.IsDelete = 0;
                        obj.Publish = true;
                        obj.CreatedDate = DateTime.Now;
                        obj.PayByTime = 1;
                        obj.Description = "Lương cứng";
                        obj.StaffId = staffId;
                        db.Tbl_Payon.Add(obj);
                        db.SaveChanges();

                        // khởi tạo lương part-time
                        obj = new Tbl_Payon();
                        obj.TypeStaffId = lst.Type;
                        obj.ForeignId = 0;
                        obj.Value = 0;
                        obj.Hint = "base_salary";
                        obj.IsDelete = 0;
                        obj.Publish = true;
                        obj.CreatedDate = DateTime.Now;
                        obj.PayByTime = 3;
                        obj.Description = "Lương part-time";
                        obj.StaffId = staffId;
                        db.Tbl_Payon.Add(obj);
                        db.SaveChanges();

                        //khởi tạo lương phụ cấp
                        obj = new Tbl_Payon();
                        obj.TypeStaffId = lst.Type;
                        obj.ForeignId = 0;
                        obj.Value = 0;
                        obj.Hint = "base_salary";
                        obj.IsDelete = 0;
                        obj.Publish = true;
                        obj.CreatedDate = DateTime.Now;
                        obj.PayByTime = 4;
                        obj.Description = "Lương phụ cấp";
                        obj.StaffId = staffId;
                        db.Tbl_Payon.Add(obj);
                        db.SaveChanges();

                        //khởi tạo Hệ số điểm hài lòng
                        obj = new Tbl_Payon();
                        obj.TypeStaffId = lst.Type;
                        obj.ForeignId = 0;
                        obj.Value = 0;
                        obj.Hint = "base_salary";
                        obj.IsDelete = 0;
                        obj.Publish = true;
                        obj.CreatedDate = DateTime.Now;
                        obj.PayByTime = 5;
                        obj.Description = "Hệ số điểm hài lòng";
                        obj.StaffId = staffId;
                        db.Tbl_Payon.Add(obj);
                        db.SaveChanges();
                    }
                    payon = db.Tbl_Payon.Where(w => w.StaffId == staffId && w.Hint == "base_salary").OrderBy(o => o.PayByTime).ToList();
                }


                // Giá trị mặc định ForeignId = 0
                payonSet = new List<KeyValuePair<int, int>>();
                item = new cls_departmentPayon();
                item.departmentName = "Mặc định";
                if (payon.Count > 0)
                {
                    foreach (var v2 in payon)
                    {
                        payonSet.Add(new KeyValuePair<int, int>(v2.Id, v2.Value.Value));
                        payMethods.Add(new KeyValuePair<int, string>(Convert.ToInt32(v2.PayByTime), v2.Description));
                    }
                    payMethods.Insert(0, new KeyValuePair<int, string>(Convert.ToInt32(0), "Bậc kỹ năng"));
                }
                item.payonSet = payonSet;
                item.payMethod = payMethods;
                list.Add(item);
                return list;
            }
        }

        private List<Tbl_SkillLevel> getSkillLevel(int department, Solution_30shineEntities db)
        {
            var sql = @"select skillLevel.*
                        from
                        (
	                        select ForeignId from Tbl_Payon 
	                        where TypeStaffId = " + department + @" and Hint = 'base_salary' 
	                        group by ForeignId
                        ) as a
                        inner join Tbl_SkillLevel as skillLevel
                        on skillLevel.Id = a.ForeignId
                        order by skillLevel.[Order]";
            return db.Tbl_SkillLevel.SqlQuery(sql).ToList();
        }
        
        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        

        public void Repeater_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
              || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (Perm_Edit == true)
                    ((Panel)(e.Item.FindControl("EAedit"))).Visible = true;
                if (Perm_Delete == true)
                    ((Panel)(e.Item.FindControl("EAdelete"))).Visible = true;
            }
        }

        public class cls_departmentPayon
        {
            public string departmentName { get; set; }
            public List<KeyValuePair<int, int>> payonSet { get; set; }
            public List<KeyValuePair<int, string>> payMethod { get; set; }
        }
    }
}