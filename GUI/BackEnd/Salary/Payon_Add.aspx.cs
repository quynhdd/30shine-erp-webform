﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.Salary
{
    public partial class Payon_Add : System.Web.UI.Page
    {
        protected Tbl_Payon OBJ;
        protected string _Code;
        protected bool _IsUpdate = false;
        private string PageID = "";
        private bool Perm_Access = false;

        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //if (Request.QueryString["Code"]!=null)
                //{
                //    PageID = "PAYON_EDIT";
                //}
                //else
                //{
                //    PageID = "PAYON_ADD";
                //}
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                ////Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                ////Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                ////Perm_ViewAllDate = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllDate", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        private void ExecuteByPermission()
        {

            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (!IsPostBack)
            {
                Bind_Service_Product();
                Bind_SkillLevel();
                Bind_TypeStaff();

                if (IsUpdate())
                {
                    if (Bind_OBJ())
                    {
                        //
                    }
                }
                else
                {
                     // 
                }   
                
            }            
        }

        protected void ExcAddOrUpdate(object sender, EventArgs e)
        {
            if (IsUpdate())
            {
                Update();
            }
            else
            {
                Add();
            }
        }

        private void Add()
        {
            using (var db = new Solution_30shineEntities())
            {
                var obj = new Tbl_Payon();
                int integer;
                obj.TypeStaffId = Convert.ToInt32(TypeStaff.SelectedValue);
                obj.KeyId = Convert.ToInt32(ItemKey.SelectedValue);
                obj.ForeignId = Convert.ToInt32(ItemForeign.SelectedValue);
                obj.Value = int.TryParse(Value.Text, out integer) ? integer : 0;
                obj.Hint = "service";
                obj.Publish = Publish.Checked;
                obj.CreatedDate = DateTime.Now;
                obj.IsDelete = 0;

                // Validate
                var Error = false;

                if (!Error)
                {
                    db.Tbl_Payon.Add(obj);
                    var exc = db.SaveChanges();

                    if (exc > 0)
                    {
                        var MsgParam = new List<KeyValuePair<string, string>>();
                        MsgParam.Add(new KeyValuePair<string, string>("msg_add_status", "success"));
                        MsgParam.Add(new KeyValuePair<string, string>("msg_add_message", "Cập nhật thành công!"));
                        //UIHelpers.Redirect("/admin/salon.html", MsgParam);
                    }
                    else
                    {
                        var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển.";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);  
                    }
                }
            }
        }

        private void Update()
        {
            using (var db = new Solution_30shineEntities())
            {
                int Id;
                int integer;
                if (int.TryParse(_Code, out Id))
                {
                    OBJ = db.Tbl_Payon.FirstOrDefault(w => w.Id == Id);

                    if (!OBJ.Equals(null))
                    {
                        OBJ.TypeStaffId = Convert.ToInt32(TypeStaff.SelectedValue);
                        OBJ.KeyId = Convert.ToInt32(ItemKey.SelectedValue);
                        OBJ.ForeignId = Convert.ToInt32(ItemForeign.SelectedValue);
                        OBJ.Value = int.TryParse(Value.Text, out integer) ? integer : 0;
                        OBJ.Publish = Publish.Checked;
                        OBJ.ModifiedDate = DateTime.Now;
                        OBJ.IsDelete = 0;

                        db.Tbl_Payon.AddOrUpdate(OBJ);
                        var exc = db.SaveChanges();
                        if (exc > 0)
                        {
                            var msg = "Cập nhật thành công!";
                            var status = "success";
                            UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                        }
                        else
                        {
                            var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển!";
                            var status = "warning";
                            UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                        }
                    }
                    else
                    {
                        var msg = "Lỗi! Không tìm thấy danh mục.";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);

                    }
                }
                else
                {
                    MsgSystem.Text = "Lỗi! Không tìm thấy danh mục.";
                    MsgSystem.CssClass = "msg-system warning";
                }

                Context.RewritePath("+", "", "");
            }
        }

        public void Bind_TypeStaff()
        {
            using (var db = new Solution_30shineEntities())
            {
                var LST = db.Staff_Type.Where(w => w.IsDelete != 1 && w.Publish == true).OrderBy(o => o.Order).ToList();
                var Key = 0;
                var Count = LST.Count;
                TypeStaff.DataTextField = "TypeStaff";
                TypeStaff.DataValueField = "Id";
                ListItem item = new ListItem("Chọn bộ phận", "0");
                TypeStaff.Items.Insert(Key, item);
                Key++;

                if (Count > 0)
                {
                    foreach (var v in LST)
                    {
                        item = new ListItem(v.Name, v.Id.ToString());
                        TypeStaff.Items.Insert(Key, item);
                        Key++;
                    }
                }
            }
        }

        public void Bind_SkillLevel()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Tbl_SkillLevel.Where(w=>w.IsDelete != 1 && w.Publish == true).ToList();                
                var Key = 0;
                ItemForeign.DataTextField = "ItemForeign";
                ItemForeign.DataValueField = "Value";
                ListItem item = new ListItem("Chọn bậc kỹ năng", "0");
                ItemForeign.Items.Insert(Key, item);
                Key++;

                if (lst.Count > 0)
                {
                    foreach (var v in lst)
                    {
                        item = new ListItem(v.Name, v.Id.ToString());
                        ItemForeign.Items.Insert(Key, item);
                        Key++;
                    }
                }
            }
        }

        public void Bind_Service_Product()
        {
            using (var db = new Solution_30shineEntities())
            {
                var Key = 0;
                int Count;
                ItemKey.DataTextField = "ItemKey";
                ItemKey.DataValueField = "Id";
                //ListItem item = new ListItem("Chọn dịch vụ | sản phẩm", "0");
                ListItem item = new ListItem("Chọn dịch vụ", "0");
                ItemKey.Items.Insert(Key, item);
                Key++;

                // Bind Dịch vụ
                item = new ListItem("---Dịch vụ", "0");
                ItemKey.Items.Insert(Key, item);
                Key++;
                var LST = db.Services.Where(w => /*w.IsDelete != 1 && w.Publish == 1 &&*/ (w.IsFreeService != 1 || w.IsFreeService == null)).OrderByDescending(o => o.Id).ToList();
                Count = LST.Count;
                if (Count > 0)
                {
                    foreach (var v in LST)
                    {
                        item = new ListItem(v.Name, v.Id.ToString());
                        ItemKey.Items.Insert(Key, item);
                        Key++;
                    }
                }

                // Bind Sản phẩm
                //item = new ListItem("---Sản phẩm", "0");
                //ItemKey.Items.Insert(Key, item);
                //Key++;
                //var LST2 = db.Products.Where(w => w.IsDelete != 1 && w.Publish == 1).OrderByDescending(o => o.Id).ToList();
                //Count = LST2.Count;
                //if (Count > 0)
                //{
                //    foreach (var v in LST2)
                //    {
                //        item = new ListItem(v.Name, v.Id.ToString());
                //        ItemKey.Items.Insert(Key, item);
                //        Key++;
                //    }
                //}
            }
        }

        private bool Bind_OBJ()
        {
            using (var db = new Solution_30shineEntities())
            {
                var ExistOBJ = true;
                int Id;
                if (int.TryParse(_Code, out Id))
                {
                    OBJ = db.Tbl_Payon.FirstOrDefault(w => w.Id == Id);

                    if (!OBJ.Equals(null))
                    {
                        if (OBJ.Publish == true)
                        {
                            Publish.Checked = true;
                        }
                        else
                        {
                            Publish.Checked = false;
                        }

                        var ItemSelectedTypeStaff = TypeStaff.Items.FindByValue(OBJ.TypeStaffId.ToString());
                        if (ItemSelectedTypeStaff != null)
                            ItemSelectedTypeStaff.Selected = true;

                        var ItemSelectedKey = ItemKey.Items.FindByValue(OBJ.KeyId.ToString());
                        if (ItemSelectedKey != null)
                            ItemSelectedKey.Selected = true;

                        var ItemSelectedForeign = ItemForeign.Items.FindByValue(OBJ.ForeignId.ToString());
                        if (ItemSelectedForeign != null)
                            ItemSelectedForeign.Selected = true;

                    }
                    else
                    {
                        ExistOBJ = false;
                        var msg = "Lỗi!";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                    }
                }
                else
                {
                    ExistOBJ = false;
                    var msg = "Lỗi!";
                    var status = "warning";
                    UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                }

                return ExistOBJ;
            }
        }

        public void ReloadPage()
        {
            HttpRequest Request = HttpContext.Current.Request;
            string host = "http://" + Request.Url.Host.ToString();
            string rawUrl = Request.RawUrl.ToString();
            string Url = host + rawUrl;
            Response.Redirect(Url);
        }

        private bool IsUpdate()
        {
            _Code = Request.QueryString["Code"];
            if (_Code != null)
            {
                _IsUpdate = true;
                return true;
            }
            else
            {
                _IsUpdate = false;
                return false;
            }
        }

        
    }
}