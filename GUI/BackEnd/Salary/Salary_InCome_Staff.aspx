﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Salary_InCome_Staff.aspx.cs" Inherits="_30shine.GUI.BackEnd.Salary.Salary_InCome_Staff" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <style>
            table.table-report-department {
            }

            table.table-report-department {
                border-collapse: collapse;
            }

            table.table-report-department, .table-report-department th, .table-report-department td {
                border: 1px solid #bababa;
            }

            .table-report-department th {
                font-weight: normal;
                font-family: Roboto Condensed Bold;
                padding: 5px 10px;
            }

            .table-report-department td {
                padding: 5px 10px;
            }
        </style>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Báo cáo &nbsp;&#187; </li>
                        <li class="be-report-li" id="LuongNhanVien"><a href="/admin/bao-cao/salary-2018-staff.html.html"><i class="fa fa-th-large"></i>Tính lương nhân viên</a></li>
                        <li class="be-report-li" id="LuongSalon"><a href="/admin/bao-cao/salary-2018-salon.html"><i class="fa fa-th-large"></i>Tính lương cho salon</a></li>
                        <li class="be-report-li" id="Import"><a href="/admin/import-excel/salary-income-change.html"><i class="fa fa-th-large"></i>Import data</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="wp customer-add customer-listing be-report be-report-timekeeping">
            <%-- Listing --%>
            <asp:ScriptManager ID="SM01" runat="server"></asp:ScriptManager>
            <div class="wp960 content-wp">
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <div class="row">
                    <strong class="st-head"><i class="fa fa-clock-o"></i>Thời gian</strong>
                    <div class="time-wp">
                        <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeFrom" placeholder="Từ ngày"
                            ClientIDMode="Static" runat="server"></asp:TextBox>
                    </div>
                    <strong class="st-head" style="margin-left: 10px;">
                        <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                    </strong>
                    <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeTo" placeholder="Đến ngày"
                        ClientIDMode="Static" runat="server"></asp:TextBox>
                    <asp:UpdatePanel runat="server" ID="UPsalon">
                        <ContentTemplate>
                            <div class="filter-item">
                                <asp:DropDownList ID="Salon" runat="server" CssClass="form-control select" ClientIDMode="Static" Style="width: 190px;" OnSelectedIndexChanged="bindStaffByDepartment" AutoPostBack="true"></asp:DropDownList>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="row">
                    <strong class="st-head"><i class="fa fa-filter"></i>Lọc kết quả</strong>
                    <asp:UpdatePanel runat="server" ID="UPStaff">
                        <ContentTemplate>
                            <div class="filter-item">
                                <asp:DropDownList ID="ddlDepartment" CssClass="form-control select" runat="server" ClientIDMode="Static" Style="width: 190px;" OnSelectedIndexChanged="bindStaffByDepartment" AutoPostBack="true"></asp:DropDownList>
                            </div>
                            <div class="filter-item">
                                <asp:DropDownList ID="ddlStaff" CssClass="form-control select" runat="server" ClientIDMode="Static" Style="width: 190px;"></asp:DropDownList>
                            </div>
                            <div class="filter-item">
                                <asp:DropDownList ID="Active" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 149px;">
                                    <asp:ListItem Text="Chọn tất cả" Value="411"></asp:ListItem>
                                    <asp:ListItem Text="Đang làm việc" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Nghỉ tạm thời" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="Đã nghỉ" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </ContentTemplate>
                        <Triggers></Triggers>
                    </asp:UpdatePanel>
                    <asp:Panel ID="ViewDataFilter" CssClass="st-head btn-viewdata" ClientIDMode="Static"
                        onclick="excPaging(1)" runat="server">
                        Xem dữ liệu
                    </asp:Panel>
                    <%--<a href="/admin/bao-cao/salary.html" class="st-head btn-viewdata">Reset Filter</a>--%>
                    <asp:Panel ID="resetFillter" CssClass="st-head btn-viewdata" ClientIDMode="Static" runat="server">
                        Reset Filter
                    </asp:Panel>
                    <div class="export-wp drop-down-list-wp">
                        <asp:Panel ID="fff" CssClass="st-head btn-viewdata"
                            ClientIDMode="Static" runat="server" onclick="EntryExportExcel('all');">
                            Xuất File Excel
                        </asp:Panel>
                    </div>
                    <div class="export-wp drop-down-list-wp">
                        <asp:Panel ID="ExcelMypham" CssClass="st-head btn-viewdata" ClientIDMode="Static" runat="server" onclick="$('#BtnExcelProduct').click()">
                            Xuất File Excel Mỹ phẩm
                        </asp:Panel>
                    </div>
                    <div class="export-wp drop-down-list-wp">
                        <asp:Panel ID="ExportExcel" CssClass="st-head btn-viewdata"
                            ClientIDMode="Static" runat="server" onclick="EntryExportExcelForMisa();">
                            Xuất File Excel Dịch Vụ
                        </asp:Panel>
                    </div>
                </div>
                <div class="row">
                    <strong class="st-head"><i class="fa fa-file-text"></i>Chấm công nhân viên</strong>
                </div>
                <asp:UpdatePanel runat="server" ID="UPtotalByDepartment">
                    <ContentTemplate>
                        <table class="table-report-department">
                            <thead>
                                <tr>
                                    <th>Bộ phận</th>
                                    <th>Tổng lương (Không tính mỹ phẩm): <%= string.Format("{0:#,###}", this.salaryTotal).Replace(',','.') %></th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater runat="server" ID="rptSalaryTotal">
                                    <ItemTemplate>
                                        <tr>
                                            <td><%# String.Format("{0:#,###}", Eval("departmentName")).Replace(',', '.') %></td>
                                            <td><%# String.Format("{0:#,###}", Eval("salaryNoProduct")).Replace(',', '.') %></td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <!-- Row Table Filter -->
                <!-- Row Table Filter -->
                <div class="table-func-panel" style="margin-top: -33px;">
                    <div class="table-func-elm">
                        <span>Số hàng / Page : </span>
                        <div class="table-func-input-wp">
                            <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                            <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                            <ul class="ul-opt-segment">
                                <li data-value="10">10</li>
                                <li data-value="20">20</li>
                                <li data-value="30">30</li>
                                <li data-value="40">40</li>
                                <li data-value="50">50</li>
                                <li data-value="1000000">Tất cả</li>
                            </ul>
                            <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                        </div>
                    </div>
                </div>
                <!-- End Row Table Filter -->
                <!-- End Row Table Filter -->
                <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                    <ContentTemplate>
                        <div class="row table-wp">
                            <table class="table-add table-listing table-listing-salary">
                                <thead>
                                    <tr>
                                        <th class="auto-style1">STT</th>
                                        <th class="auto-style1">ID</th>
                                        <th class="col-name">Tên nhân viên</th>
                                        <th class="auto-style1">Bộ phận</th>
                                        <th class="auto-style1">Bậc kỹ năng</th>
                                        <th class="auto-style1">Chấm công
                                            <br />
                                            ( b )</th>
                                        <th class="auto-style1">Lương cứng (theo ngày công)
                                            <br />
                                            ( d )</th>
                                        <th class="auto-style1">Lương VHPV
                                            <br />
                                            ( e )</th>
                                        <th class="auto-style1">Lương phụ cấp
                                            <br />
                                            ( f )</th>
                                        <th class="auto-style1">Thưởng dịch vụ
                                            <br />
                                            ( g )</th>
                                        <th class="auto-style1" style="width: 60px">TIP
                                            ( h )</th>
                                        <th class="auto-style1">Lương part-time
                                            <br />
                                            ( i )</th>
                                        <th class="auto-style1">Bù lương
                                            <br />
                                            ( j )</th>
                                        <th class="auto-style1">Bảo hiểm
                                            <br />
                                            ( k )
                                            <br />
                                        </th>
                                        <th class="auto-style1">Thuế thu nhập cá nhân
                                            <br />
                                            ( l )</th>
                                        <th class="auto-style1">Ứng lương
                                            <br />
                                            ( m )</th>
                                        <th class="auto-style1">Trừ khác
                                            <br />
                                            ( n )</th>
                                        <th class="auto-style1">Thưởng/Trừ 5C
                                            <b />
                                            ( o )</th>
                                        <th class="auto-style1">30shine Care
                                            <br />
                                            ( p )</th>
                                        <th class="auto-style1">Thưởng tích lũy tháng
                                            <br />
                                            ( q )</th>
                                        <th class="auto-style1">Thưởng tích lũy cả năm
                                            <br /></th>
                                        <th class="auto-style1">Tổng lương
                                            <br />
                                            s = (d + e + f + g + h + i + j - k - l - m + (o)- p - q)</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <asp:Repeater ID="rptSalary" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# Container.ItemIndex + 1 + PAGING._Offset %></td>
                                                <td><a href="/admin/nhan-vien/<%# Eval("Id") %>.html"><%# Eval("Id") %></a></td>
                                                <td><a href="/admin/nhan-vien/<%# Eval("Id") %>.html"><%# Eval("FullName") %></a></td>
                                                <td><%# Eval("NameType") %></td>
                                                <td><%# Eval("SkillName") %></td>
                                                <td><%# Eval("Workday") %></td>
                                                <td><%# String.Format("{0:#,####}", Eval("FixedSalary")).Replace(',', '.') %></td>
                                                <td><%# String.Format("{0:#,####}", Eval("BehaveSalary")).Replace(',', '.') %></td>
                                                <td><%# String.Format("{0:#,####}", Eval("FixSalaryOscillation")).Replace(',', '.') %></td>
                                                <td><%# String.Format("{0:#,####}", Convert.ToInt64(Eval("ServiceSalary"))).Replace(',', '.') %></td>
                                                <td><%# String.Format("{0:#,####}", Eval("ProductSalary")).Replace(',', '.') %></td>
                                                <td><%# String.Format("{0:#,####}", Eval("OvertimeSalary")).Replace(',', '.') %></td>
                                                <td><%# String.Format("{0:#,####}", Eval("Buluong")).Replace(',', '.') %></td>
                                                <td><%# String.Format("{0:#,####}", Eval("BaoHiem")).Replace(',', '.') %></td>
                                                <td><%# String.Format("{0:#,####}", Eval("thue_thu_nhap_ca_nhan")).Replace(',', '.') %></td>
                                                <td><%# String.Format("{0:#,####}", Eval("Ungluong")).Replace(',', '.') %></td>
                                                <td><%# String.Format("{0:#,####}", Eval("Trukhac")).Replace(',', '.') %></td>
                                                <td><%# String.Format("{0:#,####}", Eval("luong_5c")).Replace(',', '.') %></td>
                                                <td><%# String.Format("{0:#,####}", Eval("MoneyVoucherWaitTime")).Replace(',', '.') %></td>
                                                <td><%# String.Format("{0:#,####}", Eval("dhl")).Replace(',', '.') %></td>
                                                <td><%# String.Format("{0:#,####}", Eval("tong_dhl")).Replace(',', '.') %></td>
                                                <td>
                                                    <%# String.Format("{0:#,####}",
                                                          Convert.ToInt64(Eval("FixedSalary"))
                                                        + Convert.ToInt64(Eval("BehaveSalary"))
                                                        + Convert.ToInt64(Eval("FixSalaryOscillation"))
                                                        + Convert.ToInt64(Eval("ServiceSalary"))
                                                        + Convert.ToInt32(Eval("ProductSalary"))
                                                        + Convert.ToInt32(Eval("OvertimeSalary"))
                                                        + Convert.ToInt32(Eval("Buluong"))
                                                        - Convert.ToInt32(Eval("BaoHiem"))
                                                        - Convert.ToInt32(Eval("thue_thu_nhap_ca_nhan"))
                                                        - Convert.ToInt32(Eval("Ungluong"))
                                                        - Convert.ToInt32(Eval("Trukhac"))
                                                        + (Convert.ToInt32(Eval("luong_5c")))
                                                        - Convert.ToInt32(Eval("MoneyVoucherWaitTime"))
                                                        - Convert.ToInt32(Eval("dhl"))
                                                        ) %>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>

                        <!-- Paging -->
                        <div class="site-paging-wp">
                            <% if (PAGING.TotalPage > 1)
                                { %>
                            <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                                <% if (PAGING._Paging.Prev != 0)
                                    { %>
                                <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                                <% } %>
                                <asp:Repeater ID="RptPaging" runat="server">
                                    <ItemTemplate>
                                        <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                            <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>>
                                            <%# Eval("PageNum") %>
                                        </a>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                                    { %>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                                <% } %>
                            </asp:Panel>
                            <% } %>
                        </div>
                        <!-- End Paging -->
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>

                <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                <asp:Button ID="BtnFakeExcel" ClientIDMode="Static" runat="server" OnClick="Exc_ExportExcel" Style="display: none;" />
                <asp:Button ID="BtnExcelProduct" ClientIDMode="Static" runat="server" OnClick="BtnExcelProduct_Click" Style="display: none;" />
                <asp:Button ID="BtnFakeExcelForMisa" ClientIDMode="Static" runat="server" OnClick="Exc_ExportExcelService" Style="display: none;" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
                <asp:HiddenField ID="HDF_SalonId" ClientIDMode="Static" runat="server" />
                <!-- Hidden Field-->
                <asp:HiddenField ID="HDF_TypeStaff" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="HDF_Staff" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="HDF_ExcelType" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="HDF_ExportType" ClientIDMode="Static" runat="server" />
                <!-- END Hidden Field-->
            </div>
            <%-- END Listing --%>
        </div>
        <style>
            .be-report table.table-listing.table-listing-salary td {
                padding: 7px 5px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                margin-top: 4px !important;
            }

                .select2-container--default .select2-selection--single .select2-selection__arrow {
                    margin-top: 5px;
                }
        </style>
        <link href="/Assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" />
        <script src="/Assets/js/jquery.mCustomScrollbar.js"></script>
        <%--<script src="../../../Assets/js/dropdownlist.js"></script>--%>
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            function SelectFunc() {
                $('.select').select2();
            }
        </script>
        <style>
            .select2-container {
                width: 190px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }
        </style>
        <script type="text/ecmascript">
            jQuery(document).ready(function () {
                SelectFunc();
                // Add active menu
                $("#glbAdminSales").addClass("active");
                $("#glbAdminReportSalary").addClass("active");
                $("li#LuongNhanVien").addClass("active");

                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) { }
                });

                //============================
                // Bind Staff
                //============================
                BindStaffFilter();

            });

            function BindStaffFilter() {
                $(".eb-select").bind("focus", function () {
                    EBSelect_ShowBox($(this).parent().find(".eb-select-data"));
                });
                $(window).bind("click", function (e) {
                    console.log(e);
                    if ((!e.target.className.match("eb-select") && e.target.parentElement.className.match("ul-listing-staff")) ||
                        (!e.target.className.match("eb-select") && !e.target.parentElement.className.match("ul-listing-staff"))
                        && !e.target.className.match("mCSB_dragger_bar")) {
                        EBSelect_HideBox();
                    }
                });
                //============================
                // Scroll Staff Filter
                //============================
                $('.eb-select-data').each(function () {
                    if ($(this).height() > 230) {
                        $(this).mCustomScrollbar({
                            theme: "dark-2",
                            scrollInertia: 100
                        });
                    }
                });
            }

            function BindIdToHDF(THIS, id, HDF_DomId, Input_DomId) {
                var text = THIS.innerText.trim(),
                    HDF_Dom = document.getElementById(HDF_DomId),
                    InputText = document.getElementById(Input_DomId);
                HDF_Dom.value = id;
                InputText.value = text;
                InputText.setAttribute("data-value", id);
                ajaxGetStaffsByType(id);
                if (HDF_DomId == "HDF_TypeStaff") {
                    $("#StaffName").val("");
                    $("#HDF_Staff").val("");
                }
            }

            function ajaxGetStaffsByType(type) {
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/Report/TimeKeeping.aspx/Load_Staff_ByType",
                    data: '{type : "' + type + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        var mission = JSON.parse(response.d);
                        if (mission.success) {
                            var staffs = JSON.parse(mission.msg);
                            if (staffs.length > 0) {
                                var tmp = "";
                                $.each(staffs, function (i, v) {
                                    tmp += '<li data-code="' + v.Code + '" data-id="' + v.Id + '"' +
                                        'onclick="BindIdToHDF(this,' + v.Id + ',\'HDF_Staff\',\'StaffName\')">' +
                                        v.Fullname + '</li>';
                                });
                                $("#UlListStaff").empty().append(tmp);
                            }

                        } else {
                            var msg = "Không tìm thấy mã khách hàng trong hệ thống.";
                            showMsgSystem(msg, "warning");
                        }
                    },
                    failure: function (response) { alert(response.d); }
                });
            }

            function EBSelect_ShowBox(Dom) {
                EBSelect_HideBox();
                Dom.show();
            }

            function EBSelect_HideBox() {
                $(".eb-select-data").hide();
            }

            function EBSelect_BindData() {

            }

            function ReplaceZezoValue() {
                $("table.table-listing td").each(function () {
                    if ($(this).text().trim() == "0") {
                        $(this).text("-");
                    }
                });
            }

            // Get list salonId
            $("#ViewDataFilter").bind("click", function () {
                var kt = true;
                var error = '';
                var salonId = ~~$('#Salon').val();
                var timeFrom = $('#TxtDateTimeFrom').val();
                if (timeFrom === "" || timeFrom === null) {
                    kt = false;
                    error = "Vui lòng chọn thời gian!";
                    $("TxtDateTimeFrom").css("border-color", "red");
                }
                else if (salonId === 0) {
                    kt = false;
                    error = "Bạn phải chọn Salon!";
                    $("Salon").css("border-color", "red");
                }
                if (!kt) {
                    ShowMessage('', error, 3);
                    removeLoading();
                }
                else {
                    addLoading();
                    //var Ids = [];
                    //var prd = {};
                    //if (salonId === 0) {
                    //    var findIndex = $('#Salon').find('option');
                    //    $(findIndex).each(function () {
                    //        prd = {};
                    //        var result = ~~$(this).val();
                    //        if (result > 0) {
                    //            prd = result;
                    //            Ids.push(prd);
                    //        }
                    //    });
                    //}
                    //else {
                    //Ids = salonId;
                    //}
                    $("#HDF_SalonId").val(salonId);
                    $("#BtnFakeUP").click();
                }
            });
        </script>
    </asp:Panel>
</asp:Content>
