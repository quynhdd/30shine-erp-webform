﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.Salary
{
    public partial class ToolUpdateSalaryV2 : System.Web.UI.Page
    {
        protected string _Code;
        protected bool _IsUpdate = false;
        private bool Perm_Access = false;
        protected bool Perm_ViewAllData = false;
        protected bool Perm_Delete = false;
        protected bool Perm_Edit = false;
        private CultureInfo culture = new CultureInfo("vi-VN");
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                Library.Function.bindSalon(new List<DropDownList> { ddlSalonByStaff }, Perm_ViewAllData);
                Library.Function.bindSalon(new List<DropDownList> { ddlSalonBySalon }, Perm_ViewAllData);
                Library.Function.bindSalon(new List<DropDownList> { ddlSalonByStaffType }, Perm_ViewAllData);
                BindStaff();
                Bind_TypeStaff();
            }
        }


        public void Bind_TypeStaff()
        {
            using (var db = new Solution_30shineEntities())
            {
                var LST = db.Staff_Type.Where(w => w.IsDelete != 1 && w.Publish == true).OrderBy(o => o.Order).ToList();
                var Key = 0;
                var Count = LST.Count;
                ddlStaffType.DataTextField = "TypeStaff";
                ddlStaffType.DataValueField = "Id";
                ListItem item = new ListItem("Chọn bộ phận", "0");
                ddlStaffType.Items.Insert(Key, item);
                Key++;

                if (Count > 0)
                {
                    foreach (var v in LST)
                    {
                        item = new ListItem(v.Name, v.Id.ToString());
                        ddlStaffType.Items.Insert(Key, item);
                        Key++;
                    }
                }
            }
        }
        private void BindStaff()
        {
            using (var db = new Solution_30shineEntities())
            {
                int integer;
                var salon = int.TryParse(ddlSalonByStaff.SelectedValue, out integer) ? integer : 0;
                var staff = new List<_30shine.MODEL.StaffView>();
                var whereSalon = "";
                var sql = "";
                if (salon > 0)
                {
                    whereSalon = " and s.SalonId = " + salon;
                }
                sql = @"select s.Id AS id, CAST((s.Id) AS NVARCHAR(100)) + ' - ' + s.Fullname AS fullName FROM Staff s
                        where s.IsDelete != 1 and s.Active = 1 
                        and (s.isAccountLogin != 1 or s.isAccountLogin is null)" + whereSalon;
                staff = db.Database.SqlQuery<_30shine.MODEL.StaffView>(sql).ToList();
                var first = new _30shine.MODEL.StaffView();
                first.id = 0;
                first.fullName = "Chọn Stylist";
                staff.Insert(0, first);
                ddlStaff.DataTextField = "fullName";
                ddlStaff.DataValueField = "id";
                ddlStaff.DataSource = staff;
                ddlStaff.DataBind();
            }
        }
        protected void bindStaffBySalon(object sender, EventArgs e)
        {
            BindStaff();
        }

        /// <summary>
        /// Update lương theo nhân viên
        /// </summary>
        /// <param name="salonid"></param>
        /// <param name="staffid"></param>
        /// <returns></returns>
        [WebMethod]
        public static bool UpdateSalaryByStaff(int salonid, int staffid, string timeFrom, string timeTo)
        {
            bool? salary = false;
            try
            {
                salary = ToolSalaryLib_V2.updateFlowSalaryByStaffOrBySalon(staffid, salonid, 0, timeFrom, timeTo);
                if (salary == null)
                {
                    throw new Exception("Lỗi. Cập nhật thất bại.");
                }
                else if (salary == false)
                {
                    throw new Exception("Lỗi. Cập nhật thất bại.");
                }

            }
            catch (Exception ex)
            {
                // Push thông báo cho mọi người(by slack) khi có lỗi xảy ra
                new Helpers.PushNotiErrorToSlack().Push("GA1LZN9AM", ex.Message + ", MAIN: " + ex.StackTrace + ", " + "UpdateSalaryByStaff",
                                                        "UpdateSalaryByStaff" + ".updateFlowSalaryByStaffOrBySalon", "dungnm",
                                                        HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host);
            }
            return salary.Value;
        }


        /// <summary>
        /// Update lương theo salon
        /// </summary>
        /// <param name="salonid"></param>
        /// <param name="staffid"></param>
        /// <returns></returns>
        [WebMethod]
        public static bool UpdateSalaryBySalon(int salonid, string timeFrom, string timeTo)
        {
            bool? salary = false;
            try
            {
                salary = ToolSalaryLib_V2.updateFlowSalaryByStaffOrBySalon(0, salonid, 0,timeFrom, timeTo);
                if (salary == null)
                {
                    throw new Exception("Lỗi. Cập nhật thất bại.");
                }
                else if (salary == false)
                {
                    throw new Exception("Lỗi. Cập nhật thất bại.");
                }

            }
            catch (Exception ex)
            { // Push thông báo cho mọi người(by slack) khi có lỗi xảy ra
                new Helpers.PushNotiErrorToSlack().Push("GA1LZN9AM", ex.Message + ", MAIN: " + ex.StackTrace + ", " + "UpdateSalaryBySalon",
                                                        "UpdateSalaryBySalon" + ".updateFlowSalaryByStaffOrBySalon", "dungnm",
                                                        HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host);
            }

            return salary.Value;
        }

        /// <summary>
        /// Update lương theo salon
        /// </summary>
        /// <param name="salonid"></param>
        /// <param name="staffid"></param>
        /// <returns></returns>
        [WebMethod]
        public static bool UpdateSalaryByType(int salonid, int type, string timeFrom, string timeTo)
        {
            bool? salary = false;
            try
            {
                salary = ToolSalaryLib_V2.updateFlowSalaryByStaffOrBySalon(0, salonid, type, timeFrom, timeTo);
                if (salary == null)
                {
                    throw new Exception("Lỗi. Cập nhật thất bại.");
                }
                else if (salary == false)
                {
                    throw new Exception("Lỗi. Cập nhật thất bại.");
                }

            }
            catch (Exception ex)
            { // Push thông báo cho mọi người(by slack) khi có lỗi xảy ra
                new Helpers.PushNotiErrorToSlack().Push("GA1LZN9AM", ex.Message + ", MAIN: " + ex.StackTrace + ", " + "UpdateSalaryByType",
                                                        "UpdateSalaryByType" + ".updateFlowSalaryByType", "dungnm",
                                                        HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host);
            }
            return salary.Value;
        }
    }
}