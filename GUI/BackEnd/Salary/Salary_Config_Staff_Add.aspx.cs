﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Globalization;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Threading.Tasks;
using System.Net.Http;
using System.Text;
using System.Net.Http.Headers;
using _30shine.GUI.BackEnd.Quanlity;
using System.Net;

namespace _30shine.GUI.BackEnd.Salary
{
    public partial class Salary_Config_Staff_Add : System.Web.UI.Page
    {
        private string PageID = "";
        protected string _Code;
        protected bool _IsUpdate = false;
        private bool Perm_Access = false;
        protected bool Perm_ViewAllData = false;
        protected bool Perm_Delete = false;
        protected bool Perm_Edit = false;
        public SalaryConfigStaff OBJ;
        private CultureInfo culture = new CultureInfo("vi-VN");
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //if (Request.QueryString["Code"] != null)
                //{
                //    PageID = "SALARY_CONFIG_STAFF_EDIT";
                //}
                //else
                //{
                //    PageID = "SALARY_CONFIG_STAFF_ADD";
                //}
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //if (Array.IndexOf(new string[] { "ADMIN", "root" }, permission) != -1)
                //{
                //    Perm_Access = true;
                //    Perm_Delete = true;
                //    Perm_ViewAllData = true;
                //    Perm_Edit = true;
                //}
                ////Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                ////Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                ////Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //////Perm_Add = permissionModel.GetActionByActionNameAndPageId("Perm_Add", PageID, permission);
                ////Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                Library.Function.bindSalon(new List<DropDownList> { ddlSalon }, Perm_ViewAllData);
                BindStaff();
                if (IsUpdate())
                {
                    if (Bind_OBJ())
                    {
                        ddlSalon.Enabled = false;
                        ddlStaff.Enabled = false;
                    }
                }
                else
                {
                }

            }
        }

        protected void ExcAddOrUpdate(object sender, EventArgs e)
        {
            if (IsUpdate())
            {
                Update();
            }
            else
            {
                Add();
            }
        }
        private void BindStaff()
        {
            using (var db = new Solution_30shineEntities())
            {
                int integer;
                var salon = int.TryParse(ddlSalon.SelectedValue, out integer) ? integer : 0;
                var staff = new List<Staff>();
                var whereSalon = "";
                var sql = "";
                if (salon > 0)
                {
                    whereSalon = " and SalonId = " + salon;
                }
                sql = @"select * from Staff 
                        where IsDelete = 0 and Active = 1
                        and (isAccountLogin = 0 or isAccountLogin is null)" + whereSalon;
                staff = db.Staffs.SqlQuery(sql).ToList();

                var first = new Staff();
                first.Id = 0;
                first.Fullname = "Chọn nhân viên";
                staff.Insert(0, first);
                ddlStaff.DataTextField = "Fullname";
                ddlStaff.DataValueField = "Id";
                ddlStaff.DataSource = staff;
                ddlStaff.DataBind();
            }
        }
        protected void bindStaffBySalon(object sender, EventArgs e)
        {
            BindStaff();
        }
        private void Add()
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    int integer;
                    var msg = "";
                    var status = "";
                    var obj = new cls_Salary_Config();
                    obj.staffId = int.TryParse(ddlStaff.SelectedValue, out integer) ? integer : 0;
                    obj.salonId = int.TryParse(ddlSalon.SelectedValue, out integer) ? integer : 0;
                    obj.fixSalaryOscillation = Convert.ToDouble(txtFixSalary.Text);
                    if (obj.staffId > 0)
                    {
                        var getStaff = (from s in db.Staffs
                                        where s.Id == obj.staffId && s.IsDelete == 0 && s.Active == 1
                                        select new
                                        {
                                            Type = s.Type,
                                            Level = s.SkillLevel
                                        }).FirstOrDefault();
                        obj.salaryConfigId = db.SalaryConfigs.FirstOrDefault(w => w.DepartmentId == getStaff.Type && w.LevelId == getStaff.Level).Id;
                    }
                    if (obj != null)
                    {
                        var Error = 0;
                        if (obj.staffId != null)
                        {
                            var checkStaff = db.SalaryConfigStaffs.FirstOrDefault(f => f.StaffId == obj.staffId && f.IsDeleted == false);
                            if (checkStaff != null)
                            {
                                var MsgParam = new List<KeyValuePair<string, string>>();
                                msg = "Nhân viên đã thêm! Vui lòng chọn nhân viên khác!!!";
                                status = "msg-system warning";
                                UIHelpers.TriggerJsMsgSystem(Page, msg, status, 1000);
                                Error = 1;
                            }
                        }
                        if (Error == 0)
                        {

                            var callApi = PushNotificationInsert(obj);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void Update()
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    int Id = 0;
                    int integer;
                    if (int.TryParse(_Code, out Id))
                    {
                        var Error = 0;
                        var obj = new cls_Salary_Config();
                        obj.id = Id;
                        obj.staffId = int.TryParse(ddlStaff.SelectedValue, out integer) ? integer : 0;
                        obj.salonId = int.TryParse(ddlSalon.SelectedValue, out integer) ? integer : 0;
                        obj.fixSalaryOscillation = Convert.ToDouble(txtFixSalary.Text);
                        if (obj.staffId > 0)
                        {
                            var getStaff = (from s in db.Staffs
                                            where s.Id == obj.staffId && s.IsDelete == 0 && s.Active == 1
                                            select new
                                            {
                                                Type = s.Type,
                                                Level = s.SkillLevel
                                            }).FirstOrDefault();
                            obj.salaryConfigId = db.SalaryConfigs.FirstOrDefault(w => w.DepartmentId == getStaff.Type && w.LevelId == getStaff.Level).Id;
                        }
                        if (Error == 0)
                        {
                            var callApi = PushNotificationUpdate(obj);
                        }
                        else
                        {
                            var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển!";
                            var status = "warning";
                            UIHelpers.TriggerJsMsgSystem(Page, msg, status, 1000);
                        }
                    }
                    else
                    {
                        MsgSystem.Text = "Lỗi! Bản ghi không tồn tại.";
                        MsgSystem.CssClass = "msg-system warning";
                    }
                    Context.RewritePath("+", "", "");
                }
            }
            catch
            {
                throw new Exception();
            }
        }

        private bool Bind_OBJ()
        {
            using (var db = new Solution_30shineEntities())
            {
                var ExistOBJ = true;
                int Id;
                if (int.TryParse(_Code, out Id))
                {
                    //OBJ = db.Devices.Where(w => w.Id == Id).FirstOrDefault();
                    using (var client = new HttpClient())
                    {
                        ServicePointManager.Expect100Continue = true;
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                        client.BaseAddress = new Uri(Libraries.AppConstants.URL_API_FINANCIAL);
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        var response = client.GetAsync("/api/salary-config-staff/view?id=" + Id).Result;
                        if (response.IsSuccessStatusCode)
                        {
                            var result = response.Content.ReadAsStringAsync().Result;
                            var serializer = new JavaScriptSerializer();
                            var product = serializer.Deserialize<cls_SalaryConfig>(result);
                            ddlStaff.SelectedValue = product.data.staff_id.ToString();
                            ddlSalon.SelectedValue = product.data.salon_id.ToString();
                            txtFixSalary.Text = product.data.fix_salary_oscillation.ToString();
                        }
                    }
                }
                else
                {
                    ExistOBJ = false;
                    var msg = "Lỗi! Bản ghi không tồn tại";
                    var status = "warning";
                    UIHelpers.TriggerJsMsgSystem(Page, msg, status, 1000);
                }

                return ExistOBJ;
            }
        }
        private bool IsUpdate()
        {
            _Code = Request.QueryString["Code"];
            if (_Code != null)
            {
                _IsUpdate = true;
                return true;
            }
            else
            {
                _IsUpdate = false;
                return false;
            }
        }

        public async Task PushNotificationInsert(cls_Salary_Config obj)
        {
            var msg = "";
            var status = "";
            var MsgParam = new List<KeyValuePair<string, string>>();
            try
            {
                using (var client = new HttpClient())
                {
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    client.BaseAddress = new Uri(Libraries.AppConstants.URL_API_FINANCIAL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var Result = client.PostAsJsonAsync("/api/salary-config-staff/insert", obj).Result;
                    if (Result.IsSuccessStatusCode)
                    {
                        var result = Result.Content.ReadAsStringAsync().Result;
                        JavaScriptSerializer serializer = new JavaScriptSerializer();
                        var responseData = serializer.Deserialize<ResponseData>(result);
                        if (responseData.status == 1)
                        {
                            msg = "Thêm mới thành công!!";
                            status = "msg-system warning";
                            UIHelpers.TriggerJsMsgSystem(Page, msg, status, 1000);
                            MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                            MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Thêm mới thành công"));
                            UIHelpers.Redirect("/admin/cau-hinh-luong/nhan-vien/danh-sach.html", MsgParam);

                        }
                        else
                        {
                            msg = "Thêm mới thất bại!!";
                            status = "msg-system warning";
                            UIHelpers.TriggerJsMsgSystem(Page, msg, status, 1000);
                            MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                            MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Thêm mới thất bại!"));
                        }
                    }
                    else
                    {
                        msg = "Thêm mới thất bại!!";
                        status = "msg-system warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 1000);
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Thêm mới thất bại!"));
                    }
                }
            }
            catch
            {
                msg = "Thêm mới thất bại!!";
                status = "msg-system warning";
                UIHelpers.TriggerJsMsgSystem(Page, msg, status, 1000);
                MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Thêm mới thất bại!"));
            }
        }

        public async Task PushNotificationUpdate(cls_Salary_Config obj)
        {
            var msg = "";
            var status = "";
            var MsgParam = new List<KeyValuePair<string, string>>();
            try
            {
                using (var client = new HttpClient())
                {
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    client.BaseAddress = new Uri(Libraries.AppConstants.URL_API_FINANCIAL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var Result = client.PutAsJsonAsync("/api/salary-config-staff/update", obj).Result;
                    if (Result.IsSuccessStatusCode)
                    {
                        var result = Result.Content.ReadAsStringAsync().Result;
                        JavaScriptSerializer serializer = new JavaScriptSerializer();
                        var responseData = serializer.Deserialize<ResponseData>(result);
                        if (responseData.status == 1)
                        {
                            msg = "Cập nhật thành công!!";
                            status = "msg-system warning";
                            UIHelpers.TriggerJsMsgSystem(Page, msg, status, 2000);
                            MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                            MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thành công"));
                            UIHelpers.Redirect("/admin/cau-hinh-luong/nhan-vien/danh-sach.html", MsgParam);
                        }
                        else
                        {
                            msg = "Cập nhật thất bại!!";
                            status = "msg-system warning";
                            UIHelpers.TriggerJsMsgSystem(Page, msg, status, 2000);
                            MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                            MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thất bại!"));
                        }
                    }
                    else
                    {
                        msg = "Cập nhật thất bại!!";
                        status = "msg-system warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 2000);
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thất bại!"));
                    }
                }
            }
            catch
            {
                msg = "Cập nhật thất bại!!";
                status = "msg-system warning";
                UIHelpers.TriggerJsMsgSystem(Page, msg, status, 2000);
                MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thất bại!"));
            }
        }
    }

    public class cls_Salary_Config
    {
        public int id { get; set; }
        public int staffId { get; set; }
        public int salonId { get; set; }
        public double fixSalaryOscillation { get; set; }
        public int? salaryConfigId { get; set; }
    }
    public class cls_SalaryConfig
    {
        public data data { get; set; }
    }

    public class data
    {
        public int id { get; set; }
        public int staff_id { get; set; }
        public int salon_id { get; set; }
        public double fix_salary_oscillation { get; set; }
    }
    /// <summary>
    /// author: dungnm
    /// </summary>
    public class ResponseData
    {
        public int status { get; set; }
        public string message { get; set; }
        public dynamic data { get; set; }
        public ResponseData()
        {
            data = null;
            status = 0;
            message = "";
        }
    }
}