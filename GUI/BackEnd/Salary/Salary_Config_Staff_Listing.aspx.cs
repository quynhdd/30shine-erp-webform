﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Net;

namespace _30shine.GUI.BackEnd.Salary
{
    public partial class Salary_Config_Staff_Listing : System.Web.UI.Page
    {
        private string PageID = "";
        protected Paging PAGING = new Paging();
        private bool Perm_Access = false;
        protected bool Perm_Edit = false;
        protected bool Perm_Delete = false;
        protected bool Perm_ViewAllData = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                Library.Function.bindSalon(new List<DropDownList> { ddlSalon }, Perm_ViewAllData);
                BindStaff();
                GetListData();
                RemoveLoading();
            }
        }

        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //PageID = "SALARY_CONFIG_STAFF_LISTING";
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //if (Array.IndexOf(new string[] { "ADMIN", "root" }, permission) != -1)
                //{
                //    Perm_Access = true;
                //    Perm_Delete = true;
                //    Perm_ViewAllData = true;
                //    Perm_Edit = true;
                //}
                ////Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                ////Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                ////Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                ////Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //////Perm_ViewAllDate = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllDate", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        private void BindStaff()
        {
            using (var db = new Solution_30shineEntities())
            {
                int integer;
                var salon = int.TryParse(ddlSalon.SelectedValue, out integer) ? integer : 0;
                var staff = new List<Staff>();
                var whereSalon = "";
                var sql = "";
                if (salon > 0)
                {
                    whereSalon = " and SalonId = " + salon;
                }
                sql = @"select * from Staff 
                        where IsDelete = 0 and Active = 1
                        and (isAccountLogin = 0 or isAccountLogin is null)" + whereSalon;
                staff = db.Staffs.SqlQuery(sql).ToList();

                var first = new Staff();
                first.Id = 0;
                first.Fullname = "Chọn nhân viên";
                staff.Insert(0, first);
                ddlStaff.DataTextField = "Fullname";
                ddlStaff.DataValueField = "Id";
                ddlStaff.DataSource = staff;
                ddlStaff.DataBind();
            }
        }
        protected void bindStaffBySalon(object sender, EventArgs e)
        {
            BindStaff();
        }

        /// <summary>
        /// author: dungnm
        /// bind data by method call api
        /// </summary>
        private List<Origin> GetListData()
        {
            var lstOrigin = new List<Origin>();
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    int integer;
                    int a = Convert.ToInt32(ddlStaff.SelectedValue);
                    int staff = int.TryParse(ddlStaff.SelectedValue, out integer) ? integer : 0;
                    int salon = int.TryParse(ddlSalon.SelectedValue, out integer) ? integer : 0;
                    using (var client = new HttpClient())
                    {
                        ServicePointManager.Expect100Continue = true;
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                        client.BaseAddress = new Uri(Libraries.AppConstants.URL_API_FINANCIAL);
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        var response = client.GetAsync("/api/salary-config-staff/get-list?staff_id=" + staff + "&salon_id=" + salon + "").Result;
                        var serializer = new JavaScriptSerializer();
                        var data = response.Content.ReadAsAsync<clsSalaryConfig>().Result.data.ToList();
                        if (data != null)
                        {
                            Bind_Paging(data.Count());
                            RptSalaryConfig.DataSource = data.Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                            RptSalaryConfig.DataBind();
                        }
                    }
                }
            }
            catch
            {
                return lstOrigin;
            }
            return lstOrigin;
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            GetListData();
            RemoveLoading();
        }

        /// <summary>
        /// Phân trang
        /// </summary>
        /// <param name="TotalRow"></param>
        protected void Bind_Paging(int TotalRow)
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(TotalRow) : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();
            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage(int TotalRow)
        {
            TotalRow -= PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }
        [WebMethod]
        public static string Delele(int Id)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();
            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                SalaryConfigStaff salaryConfig = (from c in db.SalaryConfigStaffs
                                    where c.Id == Id && c.IsDeleted == false
                                    select c).FirstOrDefault();
                salaryConfig.IsDeleted = true;
                var exc = db.SaveChanges();
                if (exc > 0)
                {
                    Msg.success = true;
                }
                else
                {
                    Msg.success = false;
                }
            }
            return serializer.Serialize(Msg);
        }
        public void Repeater_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
              || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (Perm_Edit == true)
                    ((Panel)(e.Item.FindControl("EAedit"))).Visible = true;
                if (Perm_Delete == true)
                    ((Panel)(e.Item.FindControl("EAdelete"))).Visible = true;
            }
        }
    }
    public class clsSalaryConfig
    {
        public int status { get; set; }
        public string message { get; set; }
        public List<Origin> data { get; set; }
    }
    public class Origin
    {
        public int id { get; set; }
        public string staff_name { get; set; }
        public string salon_name { get; set; }
        public double fix_salary_oscillation { get; set; }
    }
}