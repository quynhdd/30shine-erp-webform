﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Payon_Add.aspx.cs" Inherits="_30shine.GUI.BackEnd.Salary.Payon_Add" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="ProductAdd" ContentPlaceHolderID="CtMain" runat="server">
<asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
<asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

<div class="wp sub-menu">
    <div class="wp960">
        <div class="wp content-wp">
            <ul class="ul-sub-menu" id="subMenu">        
                <li>Quản lý cấu hình tiền công &nbsp;&#187; </li>      
                <li class="li-listing"><a href="/admin/payon.html">Danh sách</a></li>
                <% if(_IsUpdate){ %>
                    <li class="li-add"><a href="/admin/payon/them-moi.html">Thêm mới</a></li>
                    <li class="li-edit active"><a href="/admin/payon/<%= _Code %>.html">Cập nhật</a></li>
                <% }else{ %>
                    <li class="li-add active"><a href="/admin/payon/them-moi.html">Thêm mới</a></li>
                <% } %>
            </ul>
        </div>
    </div>
</div>

<div class="wp customer-add">
    <%-- Add --%>                
    <div class="wp960 content-wp">
        <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
            <div class="table-wp">
                <table class="table-add admin-product-table-add">
                    <tbody>
                        <tr class="title-head">
                            <td><strong>Thông tin Payon</strong></td>
                            <td></td>
                        </tr>
                        <tr class="tr-margin" style="height: 20px;"></tr>
                        
                        <tr class="tr-field-ahalf">
                            <td class="col-xs-2 left"><span>Bộ phận</span></td>
                            <td class="col-xs-9 right">
                                <span class="field-wp">
                                    <asp:DropDownList ID="TypeStaff"  runat="server" ClientIDMode="Static"></asp:DropDownList>
                                    <asp:DropDownList ID="ItemForeign" runat="server" ClientIDMode="Static" style="margin-left:10px;"></asp:DropDownList>
                                    <asp:RequiredFieldValidator InitialValue="0" 
                                        ID="ValidateItemForeign" Display="Dynamic" 
                                        ControlToValidate="ItemForeign"
                                        runat="server"  Text="Vui lòng nhập giá trị." 
                                        ErrorMessage="Vui lòng nhập giá trị."
                                        ForeColor="Red"
                                        CssClass="fb-cover-error">
                                    </asp:RequiredFieldValidator>
                                </span>
                            </td>
                        </tr>

                        <tr class="tr-field-ahalf" runat="server" id="TrSalon">
                            <td class="col-xs-3 left"><span>Items</span></td>
                            <td class="col-xs-9 right">
                                <span class="field-wp">
                                    <asp:DropDownList ID="ItemKey" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                    <asp:RequiredFieldValidator InitialValue="0" 
                                        ID="ValidateItemKey" Display="Dynamic" 
                                        ControlToValidate="ItemKey"
                                        runat="server"  Text="Vui lòng nhập giá trị" 
                                        ErrorMessage="Vui lòng nhập giá trị."
                                        ForeColor="Red"
                                        CssClass="fb-cover-error">
                                    </asp:RequiredFieldValidator>

                                </span>
                            </td>
                        </tr>

                        <tr class="tr-field-ahalf">
                            <td class="col-xs-2 left"><span>Giá trị</span></td>
                            <td class="col-xs-9 right">
                                <span class="field-wp">
                                    <asp:TextBox ID="Value" runat="server" ClientIDMode="Static"
                                        placeholder=""></asp:TextBox>
                                </span>
                            </td>
                        </tr>

                        <%--<tr class="tr-description">
                            <td class="col-xs-2 left"><span>Mô tả</span></td>
                            <td class="col-xs-9 right">
                                <span class="field-wp">
                                    <asp:TextBox TextMode="MultiLine" Rows="5" ID="Description" runat="server" ClientIDMode="Static"></asp:TextBox>
                                </span>
                            </td>
                        </tr>--%>
                        
                        <tr class="tr-product-category">
                            <td class="col-xs-2 left"><span>Publish</span></td>
                            <td class="col-xs-9 right">
                                <span class="field-wp">
                                    <asp:CheckBox ID="Publish" Checked="true" runat="server" />
                                </span>
                            </td>
                        </tr>
                    
                        <tr class="tr-send">
                            <td class="col-xs-2 left"></td>
                            <td class="col-xs-9 right no-border">
                                <span class="field-wp">
                                    <asp:Button ID="Send" CssClass="btn-send" runat="server" Text="Hoàn tất" ClientIDMode="Static" OnClick="ExcAddOrUpdate"></asp:Button>
                                </span>
                            </td>
                        </tr>
                        <asp:HiddenField runat="server" ID="UrlKeyField"  ClientIDMode="Static"/>
                        <asp:HiddenField runat="server" ID="ImgCoverField"  ClientIDMode="Static"/>
                    </tbody>
                </table>
            </div>
        </div>
    <%-- end Add --%>
</div>

<script>
    jQuery(document).ready(function () {
        $("#glbAdminSalon").addClass("active");
        //============================
        // Show System Message
        //============================ 
        var qs = getQueryStrings();
        showMsgSystem(qs["msg_update_message"], qs["msg_update_status"]);

    });
</script>

</asp:Panel>
</asp:Content>

