﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Salary_Staff_V4.aspx.cs" Inherits="_30shine.GUI.BackEnd.Salary.Salary_Staff_V4" %>

<%--<%@ Register Src="~/GUI/BackEnd/Report/ReportMenu.ascx" TagName="ReportMenu" TagPrefix="uc1" %>--%>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
<asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
<asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

<%--<uc1:ReportMenu runat="server" id="ReportMenu" />--%>

<style>
    table.table-report-department { }
    table.table-report-department {
        border-collapse: collapse;
    }

    table.table-report-department, .table-report-department th, .table-report-department td {
        border: 1px solid #bababa;
    }
    .table-report-department th { font-weight:normal; font-family: Roboto Condensed Bold; padding: 5px 10px;}
    .table-report-department td { padding: 5px 10px;}
</style>
<div class="wp sub-menu">
    <div class="wp960">
        <div class="wp content-wp">
            <ul class="ul-sub-menu" id="subMenu">
                <li>Báo cáo &nbsp;&#187; </li>
                <li class="be-report-li" id="LuongNhanVien"><a href="/admin/bao-cao/salary-v3.html"><i class="fa fa-th-large"></i>Tính lương nhân viên</a></li>
                <li class="be-report-li" id="LuongSalon"><a href="/admin/bao-cao/salary-follow-time-keepingv4.html"><i class="fa fa-th-large"></i>Tính lương cho salon</a></li>          
            </ul>
        </div>
    </div>
</div>

<div class="wp customer-add customer-listing be-report be-report-timekeeping">
    <%-- Listing --%>             
    <asp:ScriptManager ID="SM01" runat="server"></asp:ScriptManager>         
    <div class="wp960 content-wp">
        <div class="row">            
            <strong class="st-head"><i class="fa fa-clock-o"></i>Thời gian</strong>
            <div class="time-wp">
                <asp:TextBox CssClass="txtDateTime st-head" ID="TxtDateTimeFrom" placeholder="Từ ngày"
                    ClientIDMode="Static" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="ValidateTime" ControlToValidate="TxtDateTimeFrom" runat="server" CssClass="fb-cover-error" Text="Vui lòng chọn thời gian!"></asp:RequiredFieldValidator>
            </div>
            <strong class="st-head" style="margin-left: 10px;">
                <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
            </strong>
            <asp:TextBox CssClass="txtDateTime st-head" ID="TxtDateTimeTo" placeholder="Đến ngày"
                ClientIDMode="Static" runat="server"></asp:TextBox>
            <asp:UpdatePanel runat="server" ID="UPsalon">
                <ContentTemplate>
                    <div class="filter-item">
                        <asp:DropDownList ID="Salon" runat="server" ClientIDMode="Static" style="width: 190px;" OnSelectedIndexChanged="bindStaffByDepartment" AutoPostBack="true"></asp:DropDownList>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>            
        </div>
        <div class="row">
            <strong class="st-head"><i class="fa fa-filter"></i>Lọc kết quả</strong>                    
            <asp:UpdatePanel runat="server" ID="UPStaff">
                <ContentTemplate>
                    <div class="filter-item">
                        <asp:DropDownList ID="ddlDepartment" runat="server" ClientIDMode="Static" style="width: 190px;" OnSelectedIndexChanged="bindStaffByDepartment" AutoPostBack="true"></asp:DropDownList>
                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="ddlStaff" runat="server" ClientIDMode="Static" style="width: 190px;"></asp:DropDownList>
                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="Active" runat="server" ClientIDMode="Static" style="margin: 12px 0; width: 149px;">
                            <asp:ListItem Text="Tất cả" Value="3" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Đang làm việc" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Đã nghỉ" Value="0"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </ContentTemplate>
                <Triggers></Triggers>
            </asp:UpdatePanel>            
            <asp:Panel ID="ViewDataFilter" CssClass="st-head btn-viewdata" ClientIDMode="Static" CtMain_UPtotalByDepartment
                onclick="excPaging(1)" runat="server">Xem dữ liệu</asp:Panel>
            <%--<a href="/admin/bao-cao/salary.html" class="st-head btn-viewdata">Reset Filter</a>--%>
            <div class="export-wp drop-down-list-wp">
                <asp:Panel ID="fff" CssClass="st-head btn-viewdata"
                    ClientIDMode="Static" runat="server" onclick="EntryExportExcel('Export')">Xuất File Excel</asp:Panel>
              
            </div>    
        </div>
        <div class="row">
            <strong class="st-head"><i class="fa fa-file-text"></i>Chấm công nhân viên</strong>
        </div>
        <asp:UpdatePanel runat="server" ID="UPtotalByDepartment">
            <ContentTemplate>
                <table class="table-report-department">
                    <thead>
                        <tr>
                            <th>Bộ phận</th>
                            <th>Tổng lương (Không tính lương mỹ phẩm)</th>
                            <% if (isRoot)
                                { %>
                            <th>Tổng lương</th>
                            <% } %>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater runat="server" ID="rptSalaryTotal">
                            <ItemTemplate>
                                <tr>
                                    <td><%#Eval("NameDepartment") %></td>
                                    <td><%# String.Format("{0:#,###}", Eval("TongLuongKhongMyPham")).Replace(',', '.') %></td>
                                    <% if (isRoot)
                                        { %>
                                    <td><%# String.Format("{0:#,###}", Eval("TongLuong")).Replace(',', '.') %></td>
                                    <% } %>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>                
            </ContentTemplate>
        </asp:UpdatePanel>
        <!-- Row Table Filter -->
        <div class="table-func-panel" style="margin-top: -33px;">
            <div class="table-func-elm">
                <span>Số hàng / Page : </span>
                <div class="table-func-input-wp">
                    <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                    <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                    <ul class="ul-opt-segment">
                        <li data-value="10">10</li>
                        <li data-value="20">20</li>
                        <li data-value="30">30</li>
                        <li data-value="40">40</li>
                        <li data-value="50">50</li>
                        <li data-value="1000000">Tất cả</li>
                    </ul>
                    <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                </div>                
            </div>            
        </div>
        <!-- End Row Table Filter -->
        <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
            <ContentTemplate>
                <div class="row table-wp">
                    <table class="table-add table-listing table-listing-salary">
                        <thead>
                            <tr>
                                <th>STT</th>
                                <th>ID</th>
                                <th class="col-name">Tên nhân viên</th>
                                <th>Bộ phận</th>
                                <th>Bậc kỹ năng</th>
                                <%--<th>Tổng công tháng <%= ThisMon %> <br /> ( = Số ngày - 2 )<br /> ( a )</th>--%>
                                <th>Chấm công <br /> ( b )</th>
                                <th>Lương cơ bản <br /> ( c )</th>
                                <th>Lương cứng (theo ngày công) <br /> d = ( b / a ) * c <br /> Học việc : d = b * c</th>
                                <th>Lương dịch vụ <br /> ( e )</th>
                                <th>Lương sản phẩm <br /> ( f )</th>
                                <th>Lương part-time <br /> ( o )</th>
                                <th>Bù lương <br /> ( h )</th>
                                <th>Bảo hiểm <br /> ( i )</th>
                                <th>Ứng lương <br /> ( k )</th>
                                <th>Trừ khác <br /> ( l )</th>
                                <th>Phạt <br /> ( t )</th>
                                <th>Tổng lương <br /> g = ( d + e + f + o + h - i - k - l - t )</th>
                                <%--<th>Doanh thu DV <br /> ( h ) </th>
                                <th>Doanh thu SP <br /> ( i )</th>
                                <th>Hệ số DV <br /> j = h / ( d + e + l + m )</th>
                                <th><br />l</th>
                                <th><br />m</th>--%>
                                <%--<th>Hệ số SP <br /> k = i / ( d + f )</th>--%>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:Repeater ID="rptSalary" runat="server">
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Container.ItemIndex + 1 + PAGING._Offset %></td>
                                        <td><%# Eval("NhanVienID") %></td>
                                        <td><a href="/admin/nhan-vien/<%# Eval("NhanVienID") %>.html"><%# Eval("Fullname") %></a></td>
                                        <td><%# Eval("TypeName") %></td>
                                        <td><%# Eval("SkillLerver") %></td>
                                        <%--<td><%= DayOfMon - 2 %></td>--%>
                                        <td><%# Eval("ChamCong") %></td>
                                        <td><%# String.Format("{0:#,####}", Eval("LuongCoBan")).Replace(',', '.') %></td>
                                        <td><%# String.Format("{0:#,####}", Eval("TongLuongCungtheongay")).Replace(',', '.') %></td>
                                        <td><%# String.Format("{0:#,####}", Eval("TongLuongDichVu")).Replace(',', '.') %></td>
                                        <td><%# String.Format("{0:#,####}", Eval("TongLuongMyPham")).Replace(',', '.') %></td>
                                        <td><%# String.Format("{0:#,####}", Eval("TongLuongParTime")).Replace(',', '.') %></td>
                                        <td><%# String.Format("{0:#,####}", Eval("BuLuong")).Replace(',', '.') %></td>
                                        <td><%# String.Format("{0:#,####}", Eval("BaoHiem")).Replace(',', '.') %></td>
                                        <td><%# String.Format("{0:#,####}", Eval("UngLuong")).Replace(',', '.') %></td>
                                        <td><%# String.Format("{0:#,####}", Eval("TruKhac")).Replace(',', '.') %></td>
                                        <td><%# String.Format("{0:#,####}", Eval("XuPhat")).Replace(',', '.') %></td>
                                        <td><%# String.Format("{0:#,####}", Convert.ToInt32( Eval("TongLuong"))) %></td>
                                        
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>                    
                        </tbody>
                    </table>
                </div>
                <!-- Paging -->
                <div class="site-paging-wp">
                <% if(PAGING.TotalPage > 1){ %>
                    <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                        <% if (PAGING._Paging.Prev != 0)
                           { %>
                        <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                        <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                        <% } %>
                        <asp:Repeater ID="RptPaging" runat="server">
                            <ItemTemplate>
                                <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                    <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>
                                >
                                    <%# Eval("PageNum") %>
                                </a>
                            </ItemTemplate>                    
                        </asp:Repeater>
                        <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                           { %>                
                        <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                        <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                        <% } %>
                    </asp:Panel>
                <% } %>
                </div>
		        <!-- End Paging -->                
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
        <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" style="display:none;" />
        <asp:Button ID="BtnFakeExcel" ClientIDMode="Static" runat="server" OnClick="Exc_ExportExcel" style="display:none;" />
        <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />

        <!-- Hidden Field-->
        <input id="Button1" type="button" style="display:none;" value="button" />
        <asp:HiddenField ID="HDF_TypeStaff" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="HDF_Staff" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="HDF_ExcelType" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="HDF_ExportType" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="HiddenField1" runat="server" />
        <!-- END Hidden Field-->
        <asp:HiddenField ID="hiddentest" runat="server" />
    </div>
    <%-- END Listing --%>
</div>

<style>
    .be-report table.table-listing.table-listing-salary td { padding: 7px 5px!important;}
</style>

<link href="/Assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" />
<script src="/Assets/js/jquery.mCustomScrollbar.js"></script>
<script type="text/ecmascript">
    jQuery(document).ready(function () {
        // Add active menu
        $("#glbAdminSales").addClass("active");
        $("#glbAdminReportSalary").addClass("active");
        $( "li#LuongSalon" ).addClass( "active" );

        //============================
        // Datepicker
        //============================
        $('.txtDateTime').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'vi',
            startDate: '2014/10/10',
            format: 'd/m/Y',
            dateonly: true,
            showHour: false,
            showMinute: false,
            timepicker: false,
            onChangeDateTime: function (dp, $input) { }
        });

        //============================
        // Bind Staff
        //============================
        BindStaffFilter();

    });

    function BindStaffFilter() {
        $(".eb-select").bind("focus", function () {
            EBSelect_ShowBox($(this).parent().find(".eb-select-data"));
        });
        $(window).bind("click", function (e) {
            console.log(e);
            if ((!e.target.className.match("eb-select") && e.target.parentElement.className.match("ul-listing-staff")) ||
                (!e.target.className.match("eb-select") && !e.target.parentElement.className.match("ul-listing-staff"))
            && !e.target.className.match("mCSB_dragger_bar")) {
                EBSelect_HideBox();
            }
        });
        //============================
        // Scroll Staff Filter
        //============================
        $('.eb-select-data').each(function () {
            if ($(this).height() > 230) {
                $(this).mCustomScrollbar({
                    theme: "dark-2",
                    scrollInertia: 100
                });
            }
        });
    }

    function BindIdToHDF(THIS, id, HDF_DomId, Input_DomId) {
        var text = THIS.innerText.trim(),
            HDF_Dom = document.getElementById(HDF_DomId),
            InputText = document.getElementById(Input_DomId);
        HDF_Dom.value = id;
        InputText.value = text;
        InputText.setAttribute("data-value", id);
        ajaxGetStaffsByType(id);
        if (HDF_DomId == "HDF_TypeStaff") {
            $("#StaffName").val("");
            $("#HDF_Staff").val("");
        }
    }

    function ajaxGetStaffsByType(type) {
        $.ajax({
            type: "POST",
            url: "/GUI/BackEnd/Report/TimeKeeping.aspx/Load_Staff_ByType",
            data: '{type : "' + type + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json", success: function (response) {
                var mission = JSON.parse(response.d);
                if (mission.success) {
                    var staffs = JSON.parse(mission.msg);
                    if (staffs.length > 0) {
                        var tmp = "";
                        $.each(staffs, function (i, v) {
                            tmp += '<li data-code="' + v.Code + '" data-id="' + v.Id + '"' +
                                    'onclick="BindIdToHDF(this,' + v.Id + ',\'HDF_Staff\',\'StaffName\')">' +
                                    v.Fullname + '</li>';
                        });
                        $("#UlListStaff").empty().append(tmp);
                    }

                } else {
                    var msg = "Không tìm thấy mã khách hàng trong hệ thống.";
                    showMsgSystem(msg, "warning");
                }
            },
            failure: function (response) { alert(response.d); }
        });
    }

    function EBSelect_ShowBox(Dom) {
        EBSelect_HideBox();
        Dom.show();
    }

    function EBSelect_HideBox() {
        $(".eb-select-data").hide();
    }

    function EBSelect_BindData() {

    }

    function ReplaceZezoValue() {
        $("table.table-listing td").each(function () {
            if ($(this).text().trim() == "0") {
                $(this).text("-");
            }
        });
    }


</script>

</asp:Panel>
</asp:Content>


