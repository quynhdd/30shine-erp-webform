﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Salary_Config_Staff_Add.aspx.cs" Inherits="_30shine.GUI.BackEnd.Salary.Salary_Config_Staff_Add" %>

<asp:Content ID="staffConfigAdd" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

        <style>
            #success-alert {
                top: 100px;
                right: 10px;
                position: fixed;
                width: 20% !important;
                z-index: 2000;
            }
        </style>
        <div class="alert alert-danger" id="success-alert">
            <span id="msg-alert"></span>
        </div>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý &nbsp;&#187; Cấu hình lương nhân viên &nbsp;&#187;</li>
                        <li class="li-listing"><a href="/admin/cau-hinh-luong/nhan-vien/danh-sach.html">Danh sách</a></li>
                        <li class="li-add active"><a href="/admin/cau-hinh-luong/nhan-vien/them-moi.html">Thêm mới</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add">
            <%-- Add --%>
            <div class="wp960 content-wp">
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <div class="table-wp">
                    <table class="table-add admin-product-table-add">
                        <tbody>
                            <tr class="title-head">
                                <td><strong>Thông tin</strong></td>
                                <td></td>
                            </tr>
                            <tr class="tr-margin" style="height: 20px;"></tr>
                            <tr id="DropdownSalon">
                                <td class="col-xs-3 left"><span>Salon<strong style="color: red"> &nbsp*</strong></span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:DropDownList ID="ddlSalon" onkeyup="checkColor()" CssClass="form-control select" OnSelectedIndexChanged="bindStaffBySalon" AutoPostBack="true" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                    </span>
                                </td>
                            </tr>
                            <tr id="DropdownStaff">
                                <td class="col-xs-3 left"><span>Nhân viên<strong style="color: red"> &nbsp*</strong></span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:DropDownList ID="ddlStaff" Width="59%" CssClass="form-control" onkeyup="checkColor()" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-3 left"><span>Lương nhân viên<strong style="color: red"> &nbsp*</strong></span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="txtFixSalary" Width="59%" onkeyup="checkColor(), reformatText(this)" runat="server" onkeypress="return ValidateKeypress(/\d/,event);" ClientIDMode="Static"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>
                            <tr class="tr-send">
                                <td class="col-xs-3 left"></td>
                                <td class="col-xs-9 right no-border">
                                    <span class="field-wp">
                                        <%--  <asp:Panel ID="btnSend" CssClass="btn-send" runat="server" ClientIDMode="Static" OnClick="ExcAddOrUpdate();">Hoàn Tất</asp:Panel>--%>
                                        <asp:Button ID="btnSend" CssClass="btn-send" runat="server" Text="Hoàn tất" ClientIDMode="Static" OnClick="ExcAddOrUpdate" OnClientClick="if(!ExcAddOrUpdate()) return false;"></asp:Button>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="map-edit">
                                    <div class="edit-wp">
                                        <asp:Panel CssClass="edit-action-wp action-edit" runat="server" ID="EAedit" Visible="false">
                                            <a class="elm edit-btn" href="/admin/thiet-bi/<%# Eval("Id") %>.html" title="Sửa"></a>
                                        </asp:Panel>
                                        <asp:Panel CssClass="edit-action-wp action-delete" runat="server" ID="EAdelete" Visible="false">
                                            <a class="elm del-btn" onclick="del(this.parentNode.parentNode.parentNode.parentNode,'<%# Eval("Id") %>', '<%# Eval("DeviceName") %>')" href="javascript://" title="Xóa"></a>
                                        </asp:Panel>
                                    </div>
                                </td>
                            </tr>
                            <asp:HiddenField runat="server" ID="UrlKeyField" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="ImgCoverField" ClientIDMode="Static" />
                        </tbody>
                    </table>
                </div>
            </div>
            <%-- end Add --%>
        </div>
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 59% !important;
                margin-top: 5px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }
        </style>
        <script>
            //định dạng text box học phí 000.000
            String.prototype.reverse = function () {
                return this.split("").reverse().join("");
            }
            function reformatText(input) {
                var x = input.value;
                x = x.replace(/,/g, "");
                x = x.reverse();
                x = x.replace(/.../g, function (e) {
                    return e + ",";
                });
                x = x.reverse();
                x = x.replace(/^,/, "");
                input.value = x;
            }
            function ValidateKeypress(numcheck, e) {
                var keynum, keychar, numcheck;
                if (window.event) {
                    keynum = e.keyCode;
                }
                else if (e.which) {
                    keynum = e.which;
                }
                if (keynum == 8 || keynum == 127 || keynum == null || keynum == 9 || keynum == 0 || keynum == 13) return true;
                keychar = String.fromCharCode(keynum);
                var result = numcheck.test(keychar);
                return result;
            }
            $("#success-alert").hide();
            jQuery(document).ready(function () {
                $("#glbS4M").addClass("active");
                $("#glbS4MClass").addClass("active");
                //============================
                // Show System Message
                //============================ 
                var qs = getQueryStrings();
                showMsgSystem(qs["msg_update_message"], qs["msg_update_status"]);
            });
            function checkColor(This) {
                $("#txtFixSalary").css("border-color", "#ddd");
                $("#ddlSalon").css("border-color", "#ddd");
                $("#ddlStaff").css("border-color", "#ddd");
            }
            function ExcAddOrUpdate(This) {
                var kt = true;
                var error = 'Vui lòng nhập đầy đủ thông tin!';
                var salary = $('#txtFixSalary').val();
                var salon = $('#ddlSalon :selected').val();
                var staff = $('#ddlStaff :selected').val();
                if (salary == "") {
                    kt = false;
                    error += " [Lương nhân viên] ";
                    $("#txtFixSalary").css("border-color", "red");
                    $("#success-alert").show();
                }
                if (salon == "" || salon == 0) {
                    kt = false;
                    error += "[Salon] ";
                    $("#ddlSalon").css("border-color", "red");
                    $("#success-alert").show();
                }
                if (staff == "" || staff == 0) {
                    kt = false;
                    error += "[Nhân viên!], ";
                    $("#ddlStaff").css("border-color", "red");
                    $("#success-alert").show();
                }
                if (!kt) {
                    $("#success-alert").fadeTo(2000, 2000).slideUp(2000, function () {
                        $("#success-alert").slideUp(2000);
                    });
                    $("#msg-alert").text(error);
                }
                return kt;
            }
        </script>
    </asp:Panel>
</asp:Content>

