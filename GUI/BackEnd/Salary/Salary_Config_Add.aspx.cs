﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY;
using _30shine.Helpers;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;

namespace _30shine.GUI.BackEnd.Salary
{
    public partial class Salary_Config_Add : System.Web.UI.Page
    {
        private string PageID = "";
        protected string _Code;
        protected bool _IsUpdate = false;
        private bool Perm_Access = false;
        protected bool Perm_ViewAllData = false;
        protected bool Perm_Delete = false;
        protected bool Perm_Edit = false;

        /// <summary>
        /// Cấu hình lương chung hệ thống
        /// Author:Quynh DD
        /// </summary>

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                Library.Function.Bind_SkillLevel(new List<DropDownList>() { ddlLevel });
                //LoadConfigValue();
                LoadDeparment();
            }
        }

        /// <summary>
        /// Load hình thức làm việc ( FullTime, PartTime)
        /// </summary>
        //private void LoadConfigValue()
        //{
        //    try
        //    {
        //        using (var db = new Solution_30shineEntities())
        //        {
        //            var list = db.Tbl_Config.Where(w => w.IsDelete == 0 && w.Key == "form_work").ToList();
        //            if (list.Count > 0)
        //            {
        //                for (int i = 0; i < list.Count; i++)
        //                {
        //                    ddlHinhThuc.Items.Add(new ListItem(list[i].Label, list[i].Value.ToString()));
        //                }

        //                ddlHinhThuc.DataBind();
        //            }
        //        }
        //    }
        //    catch { throw new Exception(); }
        //}

        /// <summary>
        /// Load bộ phận
        /// </summary>
        private void LoadDeparment()
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    ddlDepartment.Items.Add(new ListItem("Chọn bộ phận", "0"));
                    var list = db.Staff_Type.Where(w => w.IsDelete == 0 && w.Publish == true).ToList();
                    if (list.Count > 0)
                    {
                        for (int i = 0; i < list.Count; i++)
                        {
                            ddlDepartment.Items.Add(new ListItem(list[i].Name, list[i].Id.ToString()));
                        }

                        ddlDepartment.DataBind();
                    }
                }
            }
            catch (Exception)
            {

                throw new Exception();
            }
        }

        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //if (Request.QueryString["Code"] != null)
                //{
                //    PageID = "SALARY_CONFIG_EDIT";
                //}
                //else
                //{
                //    PageID = "SALARY_CONFIG_ADD";
                //}
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //if (Array.IndexOf(new string[] { "ADMIN", "root" }, permission) != -1)
                //{
                //    Perm_Access = true;
                //    Perm_Delete = true;
                //    Perm_ViewAllData = true;
                //    Perm_Edit = true;
                //}

                ////Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                ////Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                ////Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                ////Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }

        }
    }
}