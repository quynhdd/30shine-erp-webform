﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Globalization;
using LinqKit;
using System.Linq.Expressions;
using ExportToExcel;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.SalaryV2
{
    public partial class Salary_Staff_V2 : System.Web.UI.Page
    {
        protected Paging PAGING = new Paging();

        private int _TypeStaff;
        private int _StaffId;

        protected bool Staff_All = false;
        protected bool Staff_Hairdresser = false;
        protected bool Staff_HairMassage = false;
        protected bool Staff_Singer = false;
        protected string THead = "";
        public CultureInfo culture = new CultureInfo("vi-VN");
        private DateTime d1 = new DateTime();
        private DateTime d2 = new DateTime();
        public string timeFrom;
        public string timeTo;
        private Expression<Func<Staff, bool>> WhereStaff = PredicateBuilder.True<Staff>();
        private List<Service> _Service = new List<Service>();
        private List<Product> _Product = new List<Product>();
        protected string FirstDayOfMonth = string.Format("{0:dd/MM/yyyy}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1));
        protected string Today = String.Format("{0:dd/MM/yyyy}", DateTime.Now);
        protected int ThisMon;// = DateTime.Now.Month;
        protected int DayOfMon;// = DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month);
        protected int ConfigWorkDay = 0;

        private bool Perm_ViewAllData = false;
        private bool Perm_Access = false;
        protected bool isRoot = false;
        protected void checkPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //string perName = Session["User_Permission"].ToString();
                //string url = Request.Url.AbsolutePath.ToString();
                //Solution_30shineEntities db = new Solution_30shineEntities();
                //var pem = (from m in db.Tbl_Permission_Map
                //           join mn in db.Tbl_Permission_Menu on m.mID equals mn.mID
                //           where m.pName == perName && mn.Url_Main.EndsWith(url) || mn.Url_Rewrite.EndsWith(url)
                //           select new { m.aID }
                //           ).FirstOrDefault();
                //if (pem != null)
                //{
                //    var pemArray = pem.aID.Split(',');
                //    if (Array.IndexOf(pemArray, "1") > -1)
                //    {
                //        Perm_Access = true;
                //        if (Array.IndexOf(pemArray, "2") > -1)
                //        {
                //            Perm_ViewAllData = true;
                //        }

                //    }
                //    else
                //    {
                //        ContentWrap.Visible = false;
                //        NotAllowAccess.Visible = true;
                //    }

                //}
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();


            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            checkPermission();

            if (!IsPostBack)
            {
                TxtDateTimeFrom.Text = FirstDayOfMonth;
                TxtDateTimeTo.Text = Today;
                Library.Function.bindSalon(new List<DropDownList> { Salon }, Perm_ViewAllData);
                Bind_TypeStaff();
            }
            else
            {
                //Exc_Filter();
                //bindSalary();
            }
            RemoveLoading();
        }

        private string genSql()
        {
            string whereStaff = "";
            string whereSalon = "";           

            int SalonId = Convert.ToInt32(Salon.SelectedValue);
            if (!Perm_ViewAllData)
            {
                whereSalon = " and staff.SalonId = " + SalonId;
            }
            else
            {
                if (SalonId > 0)
                {
                    whereSalon = " and staff.SalonId = " + SalonId;
                }
                else
                {
                    whereSalon = " and staff.SalonId > 0";
                }
            }
            
            d1 = Convert.ToDateTime(TxtDateTimeFrom.Text.Trim(), culture);
            if (TxtDateTimeTo.Text.Trim() != "")
            {
                d2 = Convert.ToDateTime(TxtDateTimeTo.Text.Trim(), culture).AddDays(1);
            }
            else
            {
                d2 = d1.AddDays(1);
            }
            // Số ngày công trong tháng (= số ngày trong tháng - 2)
            DayOfMon = DateTime.DaysInMonth(d1.Year, d1.Month) - 2;
            
            string sql = @"declare @dayMonth int;
                            declare @timeFrom nvarchar(50);
                            declare @timeTo nvarchar(50);
                            set @dayMonth = " + DayOfMon + @";
                            set @timeFrom = '" + String.Format("{0:yyyy/MM/dd}", d1)+ @"';
                            set @timeTo = '" + String.Format("{0:yyyy/MM/dd}", d2) + @"';

                            select staff.*, staffType.Name as TypeName, skillLevel.Name as SkillLevelName,
	                            Coalesce(wh.workHour * tblPayon2.Value, 0) as partTime, 
	                            Coalesce(tblPayon.Value, 0) as allowance, 
	                            Cast(Coalesce(productSL.productSalary, 0) as bigint) as productSalary,
	                            Cast(Coalesce(serviceSL.serviceSalary, 0) as bigint) as serviceSalary,
	                            Coalesce(workDay.workDay, 0) as workDay,
                                Coalesce(tblPayon3.Value, 0) as baseSalary,
	                            Cast(Coalesce((case 
		                            -- lương theo tháng
		                            when tblPayon3.PayByTime = 1 then workDay.workDay * tblPayon3.Value / @dayMonth
		                            -- lương theo ngày
		                            when tblPayon3.PayByTime = 2 then workDay.workDay * tblPayon3.Value
	                            end), 0) as bigint) as fixSalary
                            from Staff as staff
                            left join Staff_Type as staffType
                            on staff.[Type] = staffType.Id
                            left join Tbl_SkillLevel as skillLevel
                            on staff.SkillLevel = skillLevel.Id
                            -- giờ làm thêm (parttime)
                            left join
                            (
	                            select StaffId, SUM(WorkHour) as workHour
	                            from FlowTimeKeeping
	                            where IsDelete != 1 and WorkDate >= @timeFrom and WorkDate < @timeTo	
	                            group by StaffId
                            ) as wh
                            on wh.StaffId = staff.Id
                            -- phụ cấp
                            left join Tbl_Payon as tblPayon
                            on tblPayon.TypeStaffId = staff.[Type] 
	                            and tblPayon.ForeignId = staff.SkillLevel 
	                            and tblPayon.PayByTime = 4
                            -- đơn vị lương 1 giờ (partime)
                            left join Tbl_Payon as tblPayon2
                            on tblPayon2.TypeStaffId = staff.[Type]
	                            and tblPayon2.ForeignId = staff.SkillLevel
	                            and tblPayon2.PayByTime = 3
                            -- đơn vị lương cứng
                            left join Tbl_Payon as tblPayon3
                            on tblPayon3.TypeStaffId = staff.[Type]
	                            and tblPayon3.ForeignId = staff.SkillLevel
	                            and tblPayon3.Hint = 'base_salary'
	                            and tblPayon3.PayByTime in (1,2)
                            -- lương theo sản phẩm
                            left join 
                            (
	                            select bill.SellerId, 
		                            --flowProduct.*, 
		                            --prd.ForSalary, 
		                            Coalesce(SUM(case 
			                            when (flowProduct.PromotionMoney > 0 or flowProduct.ProductId in (74,82) ) then 10000
			                            else flowProduct.Price * flowProduct.Quantity * (Cast(100 - flowProduct.VoucherPercent as float) / Cast(100 as float)) * (Cast(prd.ForSalary as float)/Cast(100 as float))
		                            end), 0) as productSalary
		
	                            from BillService as bill
	                            inner join FlowProduct as flowProduct
	                            on bill.Id = flowProduct.BillId 
		                            and flowProduct.IsDelete != 1 
		                            and flowProduct.CreatedDate between @timeFrom and @timeTo
	                            left join Product as prd
	                            on flowProduct.ProductId = prd.Id
	                            where bill.IsDelete != 1 and bill.Pending != 1
	                            and bill.CreatedDate between @timeFrom and @timeTo
	                            group by bill.SellerId
                            ) as productSL
                            on staff.Id = productSL.SellerId
                            -- lương theo dịch vụ (stylist và skinner)
                            left join 
                            (
	                            ------ stylist
	                            select 
		                            --flowService.*
		                            bill.Staff_Hairdresser_Id as staffId, Coalesce(SUM(flowService.Quantity * tblPayon.Value), 0) as serviceSalary
	                            from BillService as bill
	                            inner join FlowService as flowService
	                            on bill.Id = flowService.BillId
		                            and flowService.IsDelete != 1 
		                            and flowService.CreatedDate between @timeFrom and @timeTo
	                            left join Tbl_Payon as tblPayon
	                            on (select [Type] from Staff where Id = bill.Staff_Hairdresser_Id) = tblPayon.TypeStaffId 
		                            and (select SkillLevel from Staff where Id = bill.Staff_Hairdresser_Id) = tblPayon.ForeignId
		                            and flowService.ServiceId = tblPayon.KeyId 
	                            where bill.IsDelete != 1 and bill.Pending != 1
	                            and bill.CreatedDate between @timeFrom and @timeTo
	                            and bill.Staff_Hairdresser_Id > 0
	                            group by bill.Staff_Hairdresser_Id
	                            union
	                            ------ skinner
	                            select 
		                            --flowService.*
		                            bill.Staff_HairMassage_Id as staffId, Coalesce(SUM(flowService.Quantity * tblPayon.Value), 0) as serviceSalary
	                            from BillService as bill
	                            inner join FlowService as flowService
	                            on bill.Id = flowService.BillId
		                            and flowService.IsDelete != 1 
		                            and flowService.CreatedDate between @timeFrom and @timeTo
	                            left join Tbl_Payon as tblPayon
	                            on (select [Type] from Staff where Id = bill.Staff_HairMassage_Id) = tblPayon.TypeStaffId 
		                            and (select SkillLevel from Staff where Id = bill.Staff_HairMassage_Id) = tblPayon.ForeignId
		                            and flowService.ServiceId = tblPayon.KeyId 
	                            where bill.IsDelete != 1 and bill.Pending != 1
	                            and bill.CreatedDate between @timeFrom and @timeTo
	                            and bill.Staff_HairMassage_Id > 0
	                            group by bill.Staff_HairMassage_Id
                            ) as serviceSL
                            on staff.Id = serviceSL.staffId
                            -- lương cứng
                            left join
                            (
	                            select svtimes.staffId, Count(*) as workDay
	                            from
	                            (
		                            select bill.Staff_Hairdresser_Id as staffId, Count(*) as times
		                            from BillService as bill
		                            where bill.IsDelete != 1 and bill.Pending != 1
		                            and bill.CreatedDate between @timeFrom and @timeTo
		                            and bill.Staff_Hairdresser_Id > 0
		                            group by bill.Staff_Hairdresser_Id, 
			                            DATEPART(YEAR, bill.CreatedDate), 
			                            DATEPART(MONTH, bill.CreatedDate), 
			                            DATEPART(DAY, bill.CreatedDate)
	                            ) as svtimes
	                            where svtimes.times >= 3
	                            group by svtimes.staffId
	                            ----
	                            union 
	                            ----
	                            select svtimes.staffId, Count(*) as workDay
	                            from
	                            (
		                            select bill.Staff_HairMassage_Id as staffId, Count(*) as times
		                            from BillService as bill
		                            where bill.IsDelete != 1 and bill.Pending != 1
		                            and bill.CreatedDate between @timeFrom and @timeTo
		                            and bill.Staff_HairMassage_Id > 0
		                            group by bill.Staff_HairMassage_Id, 
			                            DATEPART(YEAR, bill.CreatedDate), 
			                            DATEPART(MONTH, bill.CreatedDate), 
			                            DATEPART(DAY, bill.CreatedDate)
	                            ) as svtimes
	                            where svtimes.times >= 3
	                            group by svtimes.staffId
                            ) as workDay
                            on staff.Id = workDay.staffId
                            where staff.IsDelete != 1 and staff.Active = 1 and 
                            (staff.isAccountLogin != 1 or staff.isAccountLogin is null) " +
                            whereSalon;
            return sql;
        }

        private List<cls_staff> getData()
        {
            var sql = genSql();
            var list = new List<cls_staff>();
            if (sql != "")
            {
                using (var db = new Solution_30shineEntities())
                {
                    list = db.Database.SqlQuery<cls_staff>(sql).ToList();                    
                }
            }
            return list;
        }

        private List<cls_department> getDataByDepartment(List<cls_staff> list)
        {
            var department = new List<cls_department>();
            if (list.Count > 0)
            {
                using (var db = new Solution_30shineEntities())
                {
                    var listDepartment = db.Database.SqlQuery<cls_department>("select * from Staff_Type where IsDelete != 1 and Publish = 1").ToList();
                    var index = -1;
                    var index2 = -1;
                    var temp = new cls_department();
                    var total = new cls_department();
                    foreach (var v in list)
                    {
                        index = listDepartment.FindIndex(w=>w.Id == v.Type);
                        if (index != -1)
                        {
                            index2 = department.FindIndex(w=>w.Id == listDepartment[index].Id);
                            if (index2 == -1)
                            {
                                temp = listDepartment[index];
                                temp.totalSalaryNoProduct = v.partTime + v.allowance + v.serviceSalary + v.fixSalary;
                                total.totalSalaryNoProduct += v.partTime + v.allowance + v.serviceSalary + v.fixSalary;
                                if (isRoot)
                                {
                                    temp.totalSalary = v.partTime + v.allowance + v.serviceSalary + v.productSalary + v.fixSalary;
                                    total.totalSalary += v.partTime + v.allowance + v.serviceSalary + v.productSalary + v.fixSalary;
                                }
                                department.Add(temp);
                            }
                            else
                            {
                                temp = department[index2];
                                temp.totalSalaryNoProduct += v.partTime + v.allowance + v.serviceSalary + v.fixSalary;
                                total.totalSalaryNoProduct += v.partTime + v.allowance + v.serviceSalary + v.fixSalary;
                                if (isRoot)
                                {
                                    temp.totalSalary += v.partTime + v.allowance + v.serviceSalary + v.productSalary + v.fixSalary;
                                    total.totalSalary += v.partTime + v.allowance + v.serviceSalary + v.productSalary + v.fixSalary;
                                }
                                department[index2] = temp;
                            }
                        }
                    }
                    //total.Name = "Tổng";
                    department.Add(total);
                }
            }
            return department;
        }

        public void bindSalary()
        {
            var list = getData();
            var listByDepartment = getDataByDepartment(list);
            _TypeStaff = HDF_TypeStaff.Value != "" ? Convert.ToInt32(HDF_TypeStaff.Value) : 0;
            _StaffId = HDF_Staff.Value != "" ? Convert.ToInt32(HDF_Staff.Value) : 0;
            if (_StaffId > 0)
            {
                list = list.Where(w => w.Id == _StaffId).ToList();
            }
            else if (_TypeStaff > 0)
            {
                list = list.Where(w => w.Type == _TypeStaff).ToList();
            }
            RptTimeKeeping.DataSource = list.OrderByDescending(o=>o.baseSalary).Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
            RptTimeKeeping.DataBind();
            rptTotalByDepartment.DataSource = listByDepartment;
            rptTotalByDepartment.DataBind();
        }

        private void Bind_All_ByDay()
        {
            using (var db = new Solution_30shineEntities())
            {
                var LST = db.Staffs.AsExpandable().Where(WhereStaff).OrderBy(o => o.Id).ToList();
                Rpt_Staff.DataSource = LST;
                Rpt_Staff.DataBind();
            }
        }


        public string Get_Services()
        {
            using (var db = new Solution_30shineEntities())
            {
                var str = "";
                DateTime d1 = Convert.ToDateTime(TxtDateTimeFrom.Text.Trim(), culture);
                DateTime d2;
                if (TxtDateTimeTo.Text.Trim() != "")
                {
                    d2 = Convert.ToDateTime(TxtDateTimeTo.Text.Trim(), culture).AddDays(1);
                }
                else
                {
                    d2 = d1.AddDays(1);
                }
                string timeFrom = String.Format("{0:MM/dd/yyyy}", d1);
                string timeTo = String.Format("{0:MM/dd/yyyy}", d2);

                string sql_service = @"select b.* 
                                    from
                                    (
                                    select ServiceId from FlowService
                                    where IsDelete != 1
                                    and (CreatedDate between '" + timeFrom + "' and '" + timeTo + "')" +
                                        @"group by ServiceId
                                    ) as a
                                    join Service as b
                                    on a.ServiceId = b.Id";
                _Service = db.Services.SqlQuery(sql_service).ToList();
                if (_Service.Count > 0)
                {
                    foreach (var v in _Service)
                    {
                        str += "<th>" + v.Name + "</th>";
                    }
                }
                return str;
            }
        }

        public string Get_Products()
        {
            using (var db = new Solution_30shineEntities())
            {
                var str = "";
                DateTime d1 = Convert.ToDateTime(TxtDateTimeFrom.Text.Trim(), culture);
                DateTime d2;
                if (TxtDateTimeTo.Text.Trim() != "")
                {
                    d2 = Convert.ToDateTime(TxtDateTimeTo.Text.Trim(), culture).AddDays(1);
                }
                else
                {
                    d2 = d1.AddDays(1);
                }
                string timeFrom = String.Format("{0:MM/dd/yyyy}", d1);
                string timeTo = String.Format("{0:MM/dd/yyyy}", d2);

                string sql_product = @"select b.* 
                                    from
                                    (
                                    select ProductId from FlowProduct
                                    where IsDelete != 1
                                    and (CreatedDate between '" + timeFrom + "' and '" + timeTo + "')" +
                                        @"group by ProductId
                                    ) as a
                                    join Product as b
                                    on a.ProductId = b.Id";
                _Product = db.Products.SqlQuery(sql_product).ToList();
                if (_Product.Count > 0)
                {
                    foreach (var v in _Product)
                    {
                        str += "<th>" + v.Name + "</th>";
                    }
                }
                return str;
            }
        }

        public void Bind_TypeStaff()
        {
            using (var db = new Solution_30shineEntities())
            {
                var LST = db.Staff_Type.Where(w => w.IsDelete != 1 && w.Publish == true).OrderBy(o => o.Order).ToList();
                Rpt_StaffTye.DataSource = LST;
                Rpt_StaffTye.DataBind();
            }
        }

        [WebMethod]
        public static string Load_Staff_ByType(string type)
        {
            {
                var _Msg = new Msg();
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                var Type = Convert.ToInt32(type);
                using (var db = new Solution_30shineEntities())
                {
                    var WhereLoadStaff = PredicateBuilder.True<Staff>();
                    WhereLoadStaff = WhereLoadStaff.And(w => w.Type == Type && w.Permission != "admin" && w.Permission != "accountant");
                    if (HttpContext.Current.Session["SalonId"] != null)
                    {
                        int integer;
                        int Salonid = int.TryParse(HttpContext.Current.Session["SalonId"].ToString(), out integer) ? integer : 0;
                        if (Salonid > 0)
                        {
                            WhereLoadStaff = WhereLoadStaff.And(w => w.SalonId == Salonid);
                        }
                    }
                    var LST = db.Staffs.AsExpandable().Where(WhereLoadStaff).Select(s => new { s.Id, s.Fullname, s.Code }).ToList();

                    if (LST.Count > 0)
                    {
                        _Msg.success = true;
                        _Msg.msg = serializer.Serialize(LST);
                    }
                    else
                    {
                        _Msg.success = false;
                    }
                }
                return serializer.Serialize(_Msg);
            }
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            Bind_Paging();
            bindSalary();
            ScriptManager.RegisterStartupScript(this, GetType(), "Script", "SetActiveTableListing();ReplaceZezoValue();", true);
            RemoveLoading();
        }

        protected void Bind_Paging()
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? (!HDF_Page.Value.Equals("") ? Convert.ToInt32(HDF_Page.Value) : 1) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage() : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage()
        {
            using (var db = new Solution_30shineEntities())
            {
                int Count = 0;
                _TypeStaff = HDF_TypeStaff.Value != "" ? Convert.ToInt32(HDF_TypeStaff.Value) : 0;
                _StaffId = HDF_Staff.Value != "" ? Convert.ToInt32(HDF_Staff.Value) : 0;
                if (_StaffId > 0)
                {
                    Count = db.Staffs.Where(w => w.Id == _StaffId).Count();
                }
                else if (_TypeStaff > 0)
                {
                    Count = db.Staffs.Where(w => w.Type == _TypeStaff && w.IsDelete != 1 && w.Active == 1 && (w.isAccountLogin != 1 || w.isAccountLogin == null) ).Count();
                }
                else
                {
                    Count = db.Staffs.Where(w=>w.IsDelete != 1 && w.Active == 1 && (w.isAccountLogin != 1 || w.isAccountLogin == null)).Count();
                }

                int TotalRow = Count - PAGING._TopNewsNum;
                int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
                return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
            }
        }

        protected void Exc_ExportExcel(object sender, EventArgs e)
        {
            //using (var db = new Solution_30shineEntities())
            //{
            //    var _ExcelHeadRow = new List<string>();
            //    var serializer = new JavaScriptSerializer();
            //    var BillRowLST = new List<List<string>>();
            //    var BillRow = new List<string>();
            //    int t;
            //    _ExcelHeadRow.Add("Tên nhân viên");
            //    _ExcelHeadRow.Add("Bộ phận");
            //    _ExcelHeadRow.Add("Bậc kỹ năng");
            //    _ExcelHeadRow.Add("Tổng công tháng " + ThisMon.ToString());
            //    _ExcelHeadRow.Add("Chấm công");
            //    _ExcelHeadRow.Add("Lương cơ bản");
            //    _ExcelHeadRow.Add("Lương cứng");
            //    _ExcelHeadRow.Add("Lương dịch vụ");
            //    _ExcelHeadRow.Add("Lương sản phẩm");
            //    _ExcelHeadRow.Add("Tổng lương");
            //    _ExcelHeadRow.Add("Doanh thu DV");
            //    _ExcelHeadRow.Add("Doanh thu SP");
            //    _ExcelHeadRow.Add("Hệ số DV");
            //    //_ExcelHeadRow.Add("Hệ số SP");
            //    _ExcelHeadRow.Add("l");
            //    _ExcelHeadRow.Add("m");

            //    var LSTStaff = Get_DataBy_Filter(false);
            //    if (LSTStaff.Count > 0)
            //    {
            //        foreach (var v in LSTStaff)
            //        {
            //            BillRow = new List<string>();
            //            BillRow.Add(v.Fullname);
            //            BillRow.Add(v.TypeName);
            //            BillRow.Add(v.SkillLevelName);
            //            BillRow.Add((DayOfMon - 2).ToString());
            //            BillRow.Add(v.WorkDay.ToString());
            //            BillRow.Add(v.Salary_Base.ToString());
            //            BillRow.Add(v.Salary_Fix.ToString());
            //            BillRow.Add(v.Salary_Service.ToString());
            //            BillRow.Add(v.Salary_Product.ToString());
            //            BillRow.Add(v.Salary_Total.ToString());
            //            BillRow.Add(v.SalesService.ToString());
            //            BillRow.Add(v.SalesProduct.ToString());
            //            t = v.Salary_Fix + v.Salary_Service + v.IndexLBySkinner + v.IndexMBySkinner;
            //            if (t > 0)
            //            {
            //                BillRow.Add(((float)v.SalesService / t).ToString());
            //            }
            //            else
            //            {
            //                BillRow.Add("0");
            //            }

            //            //if ((v.Salary_Fix + v.Salary_Product) > 0)
            //            //{
            //            //    BillRow.Add(((float)v.SalesProduct / (v.Salary_Fix + v.Salary_Product)).ToString());
            //            //}
            //            //else
            //            //{
            //            //    BillRow.Add("0");
            //            //}

            //            BillRow.Add(v.IndexLBySkinner.ToString());
            //            BillRow.Add(v.IndexMBySkinner.ToString());

            //            BillRowLST.Add(BillRow);
            //        }
            //    }

            //    // export
            //    var ExcelStorePath = Server.MapPath("~") + "Public/Excel/Nhan.Vien/";
            //    var FileName = "Hoa_Don_" + string.Format("{0:dd_MM_yyyy_hh_mm_ss}", DateTime.Now) + ".xlsx";
            //    var UrlFileName = "http://" + Request.Url.Host + "/Public/Excel/Nhan.Vien/" + FileName;

            //    ExportXcel(_ExcelHeadRow, BillRowLST, ExcelStorePath + FileName);
            //    Bind_Paging();
            //    ScriptManager.RegisterStartupScript(BtnFakeExcel, this.GetType(), "Download Excel", "CallBackExportExcel('" + UrlFileName + "');UP_FormatPrice('.be-report-price'); SubTableFixHeight();SetActiveTableListing();", true);
            //}
        }

        private static void ExportXcel(List<string> _ExcelHeadRow, List<List<string>> List, string Path)
        {
            try
            {
                CreateExcelFile.ExcelHeadRow = _ExcelHeadRow;
                CreateExcelFile.CreateExcel(List, Path);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi. Không tạo được file excel.\r\nException: " + ex.Message);
            }
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        /// <summary>
        /// Check permission
        /// </summary>
        private void CheckPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                string[] Allow = new string[] { "root", "admin", "accountant", "salonmanager" };
                string[] AllowViewAllDate = new string[] { "root", "admin", "accountant" };
                string[] Root = new string[] { "root" };
                var Permission = Session["User_Permission"].ToString().Trim();
                if (Array.IndexOf(Allow, Permission) != -1)
                {
                    Perm_Access = true;
                }

                if (Array.IndexOf(AllowViewAllDate, Permission) != -1)
                {
                    Perm_ViewAllData = true;
                }
                if (Array.IndexOf(Root, Permission) != -1)
                {
                    isRoot = true;
                }
            }

            // Call execute function
            ExecuteByPermission();
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
    }

    public struct Staff_TimeKeeping
    {
        public int Id { get; set; }
        public string Fullname { get; set; }
        public string TypeName { get; set; }
        public string SkillLevelName { get; set; }
        public int IdShowroom { get; set; }
        public List<int> ServiceIds { get; set; }
        public List<ItemStatistic> _ServiceStatistic { get; set; }
        public List<ItemStatistic> _ProductStatistic { get; set; }
        public int TotalService { get; set; }
        public string StrDataService { get; set; }
        public string StrTotalService { get; set; }
        public int TotalPromotion { get; set; }
        /// <summary>
        /// Tổng doanh thu
        /// </summary>
        public int TotalMoney { get; set; }
        /// <summary>
        /// Lương dịch vụ
        /// </summary>
        public int Salary_Service { get; set; }
        /// <summary>
        /// Lương sản phẩm
        /// </summary>
        public int Salary_Product { get; set; }
        /// <summary>
        /// Lương part-time
        /// </summary>
        public int Salary_PartTime { get; set; }
        /// <summary>
        /// Lương phụ cấp
        /// </summary>
        public int Salary_Allowance { get; set; }
        /// <summary>
        /// Lương cơ bản (lương tháng, lương ngày)
        /// </summary>
        public int Salary_Base { get; set; }
        /// <summary>
        /// Lương cứng (lương tính theo ngày công)
        /// </summary>
        public int Salary_Fix { get; set; }
        /// <summary>
        /// Tổng lương
        /// </summary>
        public int Salary_Total { get; set; }
        /// <summary>
        /// Số ngày công
        /// </summary>
        public int WorkDay { get; set; }
        /// <summary>
        /// Số giờ làm việc part-time, làm tăng ca
        /// </summary>
        public int WorkHour { get; set; }
        /// <summary>
        /// Doanh thu dịch vụ
        /// </summary>
        public int SalesService { get; set; }
        /// <summary>
        /// Doanh thu sản phẩm
        /// </summary>
        public int SalesProduct { get; set; }
        /// <summary>
        /// Chỉ số l theo skinner
        /// </summary>
        public int IndexLBySkinner { get; set; }
        /// <summary>
        /// Chỉ số m theo skinner
        /// </summary>
        public int IndexMBySkinner { get; set; }
    }

    public struct Salary
    {

    }

    public struct ServiceStatistic
    {
        public int ServiceId { get; set; }
        public int ServiceName { get; set; }
        public int ServiceTimes { get; set; }
        public int ServiceMoney { get; set; }
    }

    public struct ProductStatistic
    {
        public int ProductId { get; set; }
        public int ProductName { get; set; }
        public int ProductTimes { get; set; }
        public int ProductMoney { get; set; }
    }

    public struct ItemStatistic
    {
        public int ItemId { get; set; }
        public string ItemName { get; set; }
        public int ItemTimes { get; set; }
        public int ItemMoney { get; set; }
    }

    public struct BillServiceJoin
    {
        public int Id { get; set; }
        public string Fullname { get; set; }
        public int Type { get; set; }
        public int IdShowroom { get; set; }
        public int BillIsDelete { get; set; }
        public int BillId { get; set; }
        public DateTime BillCreatedDate { get; set; }
    }

    public class cls_FlowService : FlowService
    {
        public int SkinnerId { get; set; }
        public int SkinnerPayon { get; set; }
    }

    public class cls_flowproduct : FlowProduct
    {
        public double ForSalary { get; set; }
    }

    public class cls_department : Staff_Type
    {
        /// <summary>
        /// Tổng lương theo từng bộ phận (không tính lương mỹ phẩm)
        /// </summary>
        public double totalSalaryNoProduct { get; set; }
        public double totalSalary { get; set; }
    }

    public class cls_staff : Staff
    {
        /// <summary>
        /// Tên bộ phận
        /// </summary>
        public string DepartmentName { get; set; }
        /// <summary>
        /// Tên bậc kỹ năng
        /// </summary>
        public string SkillLevelName { get; set; }
        /// <summary>
        /// Chấm công
        /// </summary>
        public int workDay { get; set; }
        /// <summary>
        /// Lương làm thêm, part-time
        /// </summary>
        public int partTime { get; set; }
        /// <summary>
        /// Lương phụ cấp
        /// </summary>
        public int allowance { get; set; }
        /// <summary>
        /// Lương mỹ phẩm
        /// </summary>
        public long productSalary { get; set; }
        /// <summary>
        /// Lương dịch vụ
        /// </summary>
        public long serviceSalary { get; set; }
        /// <summary>
        /// Lương cứng
        /// </summary>
        public long fixSalary { get; set; }
        public int baseSalary { get; set; }
    }
}