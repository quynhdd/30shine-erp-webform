﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using LinqKit;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Data.Entity.Migrations;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.Salary
{
    public partial class Payon_Listing : System.Web.UI.Page
    {
        private string PageID = "PAYON";
        protected Paging PAGING = new Paging();
        private bool Perm_Access = false;

        /// <summary>
        /// Lương cứng
        /// </summary>
        protected cls_payonExc fixSalary = new cls_payonExc();
        /// <summary>
        /// Lương part-time
        /// </summary>
        protected cls_payonExc partTimeSalary = new cls_payonExc();
        /// <summary>
        /// Lương phụ cấp
        /// </summary>
        protected cls_payonExc allowanceSalary = new cls_payonExc();
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
          
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                ////Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                ////Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                ////Perm_Add = permissionModel.GetActionByActionNameAndPageId("Perm_Add", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        private void ExecuteByPermission()
        {

            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (!IsPostBack)
            {
                //Bind_StaffType();
                //Bind_Paging();
                getPayon();
                RemoveLoading();
            }
        }

        private void Bind_RptPayon()
        {
            using (var db = new Solution_30shineEntities())
            {
                int integer;
                int index = 0;
                string category;
                string table;
                int TypeStaffId = int.TryParse(StaffType.SelectedValue, out integer) ? integer : 1;
                int cate = int.TryParse(KindView.Value, out integer) ? integer : 0;
                switch (cate)
                {
                    case 1: category = "service";
                        table = "Service";
                        break;
                    case 2: category = "product";
                        table = "Product";
                        break;
                    default: category = "service";
                        table = "Service";
                        break;

                }
                var Levels = db.Tbl_SkillLevel.Where(w => w.IsDelete != 1 && w.Publish == true).OrderByDescending(o => o.Id).ToList();
                var _Services = db.Services.Where(w => w.IsDelete != 1).OrderByDescending(o => o.Id).Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                var sql = @"select a.Id, a.KeyId, a.ForeignId, a.Value, c.Name as ItemName, b.Name as SkillLevelName
                            from Tbl_Payon as a
                            inner join Tbl_SkillLevel as b
                            on a.ForeignId = b.Id
                            inner join [" + table + @"] as c
                            on a.KeyId = c.Id
                            where TypeStaffId = " + TypeStaffId + " and Hint = '" + category + @"'
                            and b.Publish = 1 and b.IsDelete != 1
                            order by KeyId desc, ForeignId desc";
                var lst = db.Database.SqlQuery<Cls_Payon>(sql).ToList();
                if (lst.Count > 0 && Levels.Count > 0)
                {
                    foreach (var v in lst)
                    {

                    }
                }

                RptService.DataSource = _Services;
                RptService.DataBind();
            }
        }

        private void Bind_StaffType()
        {
            using (var db = new Solution_30shineEntities())
            {
                var types = db.Staff_Type.Where(w => w.IsDelete != 1).OrderBy(o => o.Id).ToList();
                var Key = 0;
                ListItem item = new ListItem("Chọn Salon", "0");

                StaffType.DataTextField = "StaffType";
                StaffType.DataValueField = "Id";

                if (types.Count > 0)
                {
                    foreach (var v in types)
                    {
                        item = new ListItem("Bộ phận : " + v.Name, v.Id.ToString());
                        StaffType.Items.Insert(Key++, item);
                    }
                }
                else
                {
                    StaffType.Items.Insert(Key, item);
                }
            }
        }

        private void getPayon()
        {
            using (var db = new Solution_30shineEntities())
            {
                var listPayonServices = new List<cls_payonExc>();                
                var sql = @"select a.*, c.Name as ItemName, b.Name as SkillLevelName
                            from Tbl_Payon as a
                            left join Tbl_SkillLevel as b
                            on a.ForeignId = b.Id
                            left join Service as c
                            on a.KeyId = c.Id
                            where a.Publish = 1 and a.IsDelete != 1
                            order by KeyId desc, ForeignId desc";
                List<Cls_Payon> payon = db.Database.SqlQuery<Cls_Payon>(sql).ToList();
                if (payon.Count > 0)
                {
                    /// Danh sách lương cứng
                    var payonFix = payon.Where(w => w.Hint == "base_salary" && (w.PayByTime == 1 || w.PayByTime == 2)).ToList();
                    fixSalary.Stylist = getPayonLevels(0, 1, payonFix);
                    fixSalary.Skinner = getPayonLevels(0, 2, payonFix);
                    fixSalary.Reception = getPayonLevels(0, 3, payonFix);
                    fixSalary.Peaceofficer = getPayonLevels(0, 4, payonFix);
                    /// Danh sách part-time
                    var payonPartTime = payon.Where(w => w.Hint == "base_salary" && w.PayByTime == 3).ToList();
                    partTimeSalary.Stylist = getPayonLevels(0, 1, payonPartTime);
                    partTimeSalary.Skinner = getPayonLevels(0, 2, payonPartTime);
                    partTimeSalary.Reception = getPayonLevels(0, 3, payonPartTime);
                    partTimeSalary.Peaceofficer = getPayonLevels(0, 4, payonPartTime);
                    /// Danh sách phụ cấp
                    var payonAllowance = payon.Where(w => w.Hint == "base_salary" && w.PayByTime == 4 ).ToList();
                    allowanceSalary.Stylist = getPayonLevels(0, 1, payonAllowance);
                    allowanceSalary.Skinner = getPayonLevels(0, 2, payonAllowance);
                    allowanceSalary.Reception = getPayonLevels(0, 3, payonAllowance);
                    allowanceSalary.Peaceofficer = getPayonLevels(0, 4, payonAllowance);

                    /// Danh sách theo dịch vụ
                    listPayonServices = getPayonServices(payon.Where(w => w.Hint == "service").ToList()).OrderBy(o=>o.KeyId).ToList();//.Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                }
                RptService.DataSource = listPayonServices;
                RptService.DataBind();
            }
        }

        /// <summary>
        /// Lấy lương cứng, part-time, phụ cấp
        /// </summary>
        public cls_payon_level getPayonLevels(int keyId, int staffType, List<Cls_Payon> data)
        {
            /// Id level
            /// Id = 1 : Bậc 1
            /// Id = 2 : Bậc 2
            /// Id = 3 : Thử việc
            /// Id = 4 : Học việc bậc 1
            /// Id = 5 : Học việc bậc 2
            /// Id = 6 : Bậc 3
            var ret = new cls_payon_level();
            var temp = new Cls_Payon();
            // Không có bậc
            ret.Level0 = new cls_payon_unit();
            // Học việc bậc 1
            ret.Level1 = new cls_payon_unit();
            // Học việc bậc 2
            ret.Level2 = new cls_payon_unit();
            // Thử việc
            ret.Level3 = new cls_payon_unit();
            // Bậc 1
            ret.Level4 = new cls_payon_unit();
            // Bậc 2
            ret.Level5 = new cls_payon_unit();
            // Bậc 3
            ret.Level6 = new cls_payon_unit();

            temp = data.FirstOrDefault(w => w.TypeStaffId == staffType && w.KeyId == keyId && w.ForeignId == 0);
            if (temp != null)
            {
                ret.Level0.Id = temp.Id;
                ret.Level0.Value = Convert.ToInt32(temp.Value);
            }
            temp = data.FirstOrDefault(w => w.TypeStaffId == staffType && w.KeyId == keyId && w.ForeignId == 4);
            if (temp != null)
            {
                ret.Level1.Id = temp.Id;
                ret.Level1.Value = Convert.ToInt32(temp.Value);
            }
            temp = data.FirstOrDefault(w => w.TypeStaffId == staffType && w.KeyId == keyId && w.ForeignId == 5);
            if (temp != null)
            {
                ret.Level2.Id = temp.Id;
                ret.Level2.Value = Convert.ToInt32(temp.Value);
            }
            temp = data.FirstOrDefault(w => w.TypeStaffId == staffType && w.KeyId == keyId && w.ForeignId == 3);
            if (temp != null)
            {
                ret.Level3.Id = temp.Id;
                ret.Level3.Value = Convert.ToInt32(temp.Value);
            }
            temp = data.FirstOrDefault(w => w.TypeStaffId == staffType && w.KeyId == keyId && w.ForeignId == 1);
            if (temp != null)
            {
                ret.Level4.Id = temp.Id;
                ret.Level4.Value = Convert.ToInt32(temp.Value);
            }
            temp = data.FirstOrDefault(w => w.TypeStaffId == staffType && w.KeyId == keyId && w.ForeignId == 2);
            if (temp != null)
            {
                ret.Level5.Id = temp.Id;
                ret.Level5.Value = Convert.ToInt32(temp.Value);
            }
            temp = data.FirstOrDefault(w => w.TypeStaffId == staffType && w.KeyId == keyId && w.ForeignId == 6);
            if (temp != null)
            {
                ret.Level6.Id = temp.Id;
                ret.Level6.Value = Convert.ToInt32(temp.Value);
            }
            return ret;
        }

        public List<cls_payonExc> getPayonServices(List<Cls_Payon> data)
        {
            var payonExc = new cls_payonExc();
            var listPayonExc = new List<cls_payonExc>();
            var index = -1;
            var services = data.GroupBy(g => g.KeyId).Select(
                    s=>new {
                        s.First().KeyId,
                        s.First().ItemName
                    }
                ).ToList();
            if (services.Count > 0)
            {
                foreach (var v in services)
                {
                    index = listPayonExc.FindIndex(w => w.Id == v.KeyId);
                    if (index == -1)
                    {
                        payonExc = new cls_payonExc();
                        payonExc.KeyId = Convert.ToInt32(v.KeyId);
                        payonExc.ItemName = v.ItemName;
                    }
                    else
                    {
                        payonExc = listPayonExc[index];
                    }
                    // stylist
                    payonExc.Stylist = getPayonLevels(Convert.ToInt32(v.KeyId), 1, data);
                    payonExc.Skinner = getPayonLevels(Convert.ToInt32(v.KeyId), 2, data);
                    payonExc.Reception = getPayonLevels(Convert.ToInt32(v.KeyId), 3, data);
                    payonExc.Peaceofficer = getPayonLevels(Convert.ToInt32(v.KeyId), 4, data);

                    listPayonExc.Add(payonExc);
                }
            }

            return listPayonExc;
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            //Bind_Paging();
            getPayon();
            RemoveLoading();
        }

        protected void Bind_Paging()
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage() : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage()
        {
            using (var db = new Solution_30shineEntities())
            {
                var Count = db.Services.Count(w => w.IsDelete != 1);
                int TotalRow = Count - PAGING._TopNewsNum;
                int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
                return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
            }
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        [WebMethod]
        public static string updatePayon(int Id, int Value)
        {
            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                var obj = db.Tbl_Payon.FirstOrDefault(w=>w.Id == Id);
                if (obj != null)
                {
                    obj.Value = Value;
                    obj.ModifiedDate = DateTime.Now;
                    db.Tbl_Payon.AddOrUpdate();
                    var exc = db.SaveChanges();
                    if (exc > 0)
                    {
                        Msg.success = true;
                    }
                    else
                    {
                        Msg.success = false;
                    }
                }
                else
                {
                    Msg.success = false;
                }
            }
            return serializer.Serialize(Msg);
        }

        
    }

    public class Cls_Payon : Tbl_Payon
    {
        public string ItemName { get; set; }
        public string SkillLevelName { get; set; }
    }

    public class cls_payonExc
    {
        /// <summary>
        /// PayonId
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// ProductId...
        /// </summary>
        public int KeyId { get; set; }
        /// <summary>
        /// SkillLevel...
        /// </summary>
        public int ForeignId { get; set; }
        public int Value { get; set; }
        /// <summary>
        /// ProductName, ServiceName...
        /// </summary>
        public string ItemName { get; set; }
        /// <summary>
        /// Tên bậc
        /// </summary>
        public string SkillLevelName { get; set; }
        /// <summary>
        /// Thợ cắt tóc
        /// </summary>
        public cls_payon_level Stylist { get; set; }
        /// <summary>
        /// Thợ gội
        /// </summary>
        public cls_payon_level Skinner { get; set; }
        /// <summary>
        /// Lễ tân
        /// </summary>
        public cls_payon_level Reception { get; set; }
        /// <summary>
        /// Nhân viên bảo vệ
        /// </summary>
        public cls_payon_level Peaceofficer { get; set; }
    }

    public class cls_payon_level
    {
        /// <summary>
        /// Ứng với vị trí không có bậc (Bảo vệ, Lễ tân...)
        /// Id level
        /// Id = 1 : Bậc 1
        /// Id = 2 : Bậc 2
        /// Id = 3 : Thử việc
        /// Id = 4 : Học việc bậc 1
        /// Id = 5 : Học việc bậc 2
        /// Id = 6 : Bậc 3
        /// </summary>
        public cls_payon_unit Level0 { get; set; }
        /// <summary>
        /// Học việc bậc 1
        /// </summary>
        public cls_payon_unit Level1 { get; set; }
        /// <summary>
        /// Học việc bậc 2
        /// </summary>
        public cls_payon_unit Level2 { get; set; }
        /// <summary>
        /// Thử việc
        /// </summary>
        public cls_payon_unit Level3 { get; set; }
        /// <summary>
        /// Bậc 1
        /// </summary>
        public cls_payon_unit Level4 { get; set; }
        /// <summary>
        /// Bậc 2
        /// </summary>
        public cls_payon_unit Level5 { get; set; }
        /// <summary>
        /// Bậc 3
        /// </summary>
        public cls_payon_unit Level6 { get; set; }
    }

    public class cls_payon_unit
    {
        /// <summary>
        /// PayonId
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// PayonValue
        /// </summary>
        public int Value { get; set; }
    }
}