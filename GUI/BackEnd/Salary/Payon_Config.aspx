﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Payon_Config.aspx.cs" Inherits="_30shine.GUI.BackEnd.Salary.Payon_Config" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
<asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
<asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
<asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>

<style>
    table.table-config-salary thead tr th { min-width: 70px; }
    table.table-config-salary thead tr th:first-child { min-width: 100px; }    
</style>

<div class="wp sub-menu">
    <div class="wp960">
        <div class="wp content-wp">
            <ul class="ul-sub-menu" id="subMenu">
                <li>Quản lý lương &nbsp;&#187; </li>
                <li class="li-listing li-listing1"><a href="javascript://">Cấu hình</a></li>
            </ul>
        </div>
    </div>
</div>
    
<div class="wp customer-add customer-listing be-report">
    <%-- Listing --%>                
    <div class="wp960 content-wp">
        <!-- Filter -->        
        <div class="row row-filter">
            <strong class="st-head"><i class="fa fa-filter"></i>Lọc kết quả</strong>
            <div class="filter-item">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" ClientIDMode="Static">
                    <ContentTemplate>
                        <asp:DropDownList ID="StaffType" runat="server" ClientIDMode="Static" style="margin: 12px 0; width: 190px;" OnSelectedIndexChanged="loadDataByDepartment" AutoPostBack="true"></asp:DropDownList>
                    </ContentTemplate>                
                </asp:UpdatePanel>
            </div>
            <div class="filter-item">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server" ClientIDMode="Static">
                    <ContentTemplate>
                        <asp:DropDownList ID="ddlStaff" runat="server" ClientIDMode="Static" style="margin: 12px 0; width: 190px;" OnSelectedIndexChanged="loadDataByPerson" AutoPostBack="true"></asp:DropDownList>
                    </ContentTemplate>                
                </asp:UpdatePanel>
            </div>
        </div>
        <!-- End Filter -->
        <!-- End Row Table Filter -->
        
        <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
            <ContentTemplate>
                <div class="table-wp" style="margin-top: 15px;">
                    <table class="table-add table-listing table-config-salary" style="float: left; width: auto;">
                        <thead>
                            <tr>
                                <asp:Repeater runat="server" ID="rpt_payMethod">
                                    <ItemTemplate>
                                        <th><%# Eval("Value") %></th>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <%--<th style="min-width: 100px;">Bậc kỹ năng</th>
                                <th>Lương cứng</th>
                                <th>Lương part-time</th>
                                <th>Lương phụ cấp</th>--%>
                            </tr>
                        </thead>
                        <tbody>
                            <% if (listPayon.Count > 0)
                                {
                                    foreach (var v in listPayon)
                                    {
                                        %>
                                        <tr>
                                            <td><%= v.departmentName %></td>          
                                        <% if (v.payonSet.Count > 0)
                                            {
                                                foreach (var v2 in v.payonSet)
                                                {
                                                    %>
                                                    <td>
                                                        <div class="input-inline-wp">
                                                            <input type="text" value="<%=v2.Value %>" onchange="updatePayon($(this), <%=v2.Key %>)" style="margin-left: 0!important;" />
                                                        </div>
                                                    </td>
                            <%
                                                }
                                            } %>
                                        </tr>
                            <%
                                    }
                                }
                                         %>
                        </tbody>
                    </table>                                
                </div>
            </ContentTemplate>
            <Triggers>
                <%--<asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />--%>
            </Triggers>
        </asp:UpdatePanel>
        <%--<asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" Text="Click" style="display:none;" />--%>
        <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
        <!-- Hidden Field-->
        <asp:HiddenField ID="HDF_Suggestion_Code" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="HDF_Suggestion_Field" ClientIDMode="Static" runat="server" />
        <!-- END Hidden Field-->
    </div>    
    <%-- END Listing --%>
</div>

<!-- Notification box -->
<div class="notification-box">Thành công</div>
<!--// Notification box -->

<script>
    jQuery(document).ready(function () {
        // Add active menu
        $("#glbAdminContent").addClass("active");
        $("#glbAdminStaff").addClass("active");
        $("#subMenu .li-listing1").addClass("active");

    });

    //============================
    // Event delete
    //============================
    function del(This, code, name) {
        var code = code || null,
            name = name || null,
            Row = This;
        if (!code) return false;

        // show EBPopup
        $(".confirm-yn").openEBPopup();
        $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn xóa [ " + name + " ] ?");

        $("#EBPopup .yn-yes").bind("click", function () {
            $.ajax({
                type: "POST",
                url: "/GUI/SystemService/Ajax/Del.aspx/Delele_Staff",
                data: '{Code : "' + code + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json", success: function (response) {
                    var mission = JSON.parse(response.d);
                    if (mission.success) {
                        delSuccess();
                        Row.remove();
                    } else {
                        delFailed();
                    }
                },
                failure: function (response) { alert(response.d); }
            });
        });
        $("#EBPopup .yn-no").bind("click", function () {
            autoCloseEBPopup(0);
        });
    }

    /// Cập nhật giá trị payon
    function updatePayon(This, Id) {
        $.ajax({
            type: "POST",
            url: "/GUI/BackEnd/Salary/Payon_Listing.aspx/updatePayon",
            data: '{Id : ' + Id + ', Value : ' + This.val() + '}',
            contentType: "application/json; charset=utf-8",
            dataType: "json", success: function (response) {
                var mission = JSON.parse(response.d);
                if (mission.success) {
                    showNotification("Dữ liệu đã được cập nhật.");
                    //$("#BtnFakeUP").click();
                } else {
                    alert("Có lỗi xảy ra. Vui lòng liên hệ nhóm phát triển.");
                }
            },
            failure: function (response) { alert(response.d); }
        });
    }

    function showNotification(content) {
        $(".notification-box").text(content).fadeIn(function () {
            setTimeout(function () {
                $(".notification-box").fadeOut();
            }, 2000);
        });
    }

</script>

</asp:Panel>
</asp:Content>