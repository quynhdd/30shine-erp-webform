﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.Helpers;
using _30shine.MODEL.ENTITY.EDMX;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Script.Serialization;
using System.Collections.Specialized;
using System.Net;
using System.Text;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.Interface;

namespace _30shine.GUI.BackEnd.Salary
{
    public partial class Salary_Config_Listing : System.Web.UI.Page
    {
        private string PageID = "";
        private bool Perm_Access = false;
        protected bool Perm_ViewAllData = false;
        protected bool Perm_Delete = false;
        protected bool Perm_Edit = false;
        protected Paging PAGING = new Paging();
        public string data = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                Library.Function.Bind_SkillLevel(new List<DropDownList>() { ddlLevel });
                LoadDeparment();
                GetData();
            }
        }

        /// <summary>
        /// Load bộ phận
        /// </summary>
        private void LoadDeparment()
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    ddlDepartment.Items.Add(new ListItem("Chọn bộ phận", "0"));
                    var list = db.Staff_Type.Where(w => w.IsDelete == 0 && w.Publish == true).ToList();
                    if (list.Count > 0)
                    {
                        for (int i = 0; i < list.Count; i++)
                        {
                            ddlDepartment.Items.Add(new ListItem(list[i].Name, list[i].Id.ToString()));
                        }

                        ddlDepartment.DataBind();
                    }
                }
            }
            catch (Exception)
            {
                throw new Exception();
            }
        }

        /// <summary>
        /// Get data bind to rpt
        /// </summary>
        private void GetData()
        {
            try
            {
                int integer;
                int departmentId = int.TryParse(ddlDepartment.SelectedValue, out integer) ? integer : 0;
                int levelId = int.TryParse(ddlLevel.SelectedValue, out integer) ? integer : 0;
                using (var client = new HttpClient())
                {
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    client.BaseAddress = new Uri(Libraries.AppConstants.URL_API_FINANCIAL);
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var response = client.GetAsync("/api/salary-config/get-list?departmentId=" + departmentId + "&levelId=" + levelId + "").Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var result = response.Content.ReadAsStringAsync().Result;
                        var data = response.Content.ReadAsAsync<ResponseData>().Result.data.ToList();
                        if (data != null)
                        {
                            Bind_Paging(data.Count());
                            Rpt.DataSource = data.Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                            Rpt.DataBind();
                        }
                    }
                }
            }
            catch (Exception)
            {
                Rpt.DataSource = "";
                Rpt.DataBind();
                //throw new Exception();
            }
        }

        /// <summary>
        /// button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void _BtnClick(object sender, EventArgs e)
        {
            GetData();
            RemoveLoading();
        }

        protected void Bind_Paging(int TotalRow)
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(TotalRow) : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage(int TotalRow)
        {
            TotalRow -= PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        }

        /// <summary>
        /// check permission
        /// </summary>
        /// 
        protected void SetPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //PageID = "SALARY_CONFIG_LISTING";
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //if (Array.IndexOf(new string[] { "ADMIN", "root" }, permission) != -1)
                //{
                //    Perm_Access = true;
                //    Perm_Delete = true;
                //    Perm_ViewAllData = true;
                //    Perm_Edit = true;
                //}
                ////Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                ////Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                ////Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                ////Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }

        }
        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }


        /// <summary>
        /// author: dungnm
        /// </summary>
        public class ResponseData
        {
            public int status { get; set; }
            public string message { get; set; }
            public List<Origin> data { get; set; }
            public ResponseData()
            {
                data = null;
                status = 0;
                message = "";
            }
        }
        /// <summary>
        /// Author: QuynhDD
        /// Create:15/12/2017
        /// </summary>
        public class Origin
        {
            public int Id { get; set; }
            public int? TypeWork { get; set; }
            public int? StaffType { get; set; }
            public int? LevelId { get; set; }
            public double? FixedSalary { get; set; }
            public double? AllowanceSalary { get; set; }
            public double? OvertimeSalary { get; set; }
            public double? RattingSalary { get; set; }
            public double? ProductBonus { get; set; }
            public double? BehaveSalary { get; set; }
            public string CreatedTime { get; set; }
            public bool? IsDeleted { get; set; }
            public string LevelName { get; set; }
            public string DepartmentName { get; set; }
            public string NameTypeWork { get; set; }
        }
    }
}