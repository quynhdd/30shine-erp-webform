﻿using _30shine.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;
using System.Globalization;
using LinqKit;
using System.Linq.Expressions;
using ExportToExcel;
using System.Data.SqlClient;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using System.Net.Http;
using System.Data;
using _30shine.Helpers.Http;

namespace _30shine.GUI.BackEnd.Salary
{
    public partial class Salary_InCome_Staff : System.Web.UI.Page
    {
        /// <summary>
        /// Report tính lương hệ thống cho nhân viên
        /// Author: QuynhDD
        /// Create: 25/12/2017
        /// Sử dụng các api để get data https://apifinancial.30shine.com/api/salary-income/get-list-salary-of-staff
        /// </summary>

        protected Paging PAGING = new Paging();
        public CultureInfo culture = new CultureInfo("vi-VN");
        public bool Perm_ViewAllData = false;
        private bool Perm_Access = false;
        protected bool isRoot = false;
        protected int departmentId;
        public double? salaryTotal = 0;
        protected string FirstDayOfMonth = string.Format("{0:dd/MM/yyyy}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1));
        protected string Today = String.Format("{0:dd/MM/yyyy}", DateTime.Now);

        /// <summary>
        /// set permission
        /// </summary>
        public void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        /// <summary>
        /// ExecuteByPermission
        /// </summary>
        private void ExecuteByPermission()
        {

            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                TxtDateTimeFrom.Text = FirstDayOfMonth;
                TxtDateTimeTo.Text = Today;
                BindSalon(new List<DropDownList> { Salon }, Perm_ViewAllData);
                bindDepartment();
                bindStaff();
            }
            RemoveLoading();
        }

        /// <summary>
        /// Bind Salon
        /// </summary>
        /// <param name="permViewAll"></param>
        private void BindSalon(List<DropDownList> ddls, bool permViewAll)
        {
            using (var db = new Solution_30shineEntities())
            {
                var listSalon = new List<Tbl_Salon>();
                var SalonId = Convert.ToInt32(HttpContext.Current.Session["SalonId"]);
                var UserId = Convert.ToInt32(HttpContext.Current.Session["User_Id"]);
                var item = new Tbl_Salon();
                if (permViewAll)
                {
                    listSalon = db.Tbl_Salon.Where(w => w.IsDelete == 0 && w.Publish == true).OrderBy(o => o.Order).ToList();
                    item.ShortName = "Chọn tất cả salon";
                    item.Id = 0;
                    listSalon.Insert(0, item);
                }
                else
                {
                    listSalon = (
                                        from p in db.PermissionSalonAreas.AsNoTracking()
                                        join s in db.Tbl_Salon.AsNoTracking() on p.SalonId equals s.Id
                                        where
                                        p.IsDelete == false && p.StaffId == UserId
                                        && s.Publish == true && s.IsDelete == 0
                                        select s
                                   ).OrderBy(o => o.Order).ToList();
                    if (listSalon.Count <= 0)
                    {
                        listSalon = db.Tbl_Salon.Where(w => w.Id == SalonId).ToList();
                    }
                    item.ShortName = "Chọn salon trong vùng";
                    item.Id = 0;
                    listSalon.Insert(0, item);
                }
                for (var i = 0; i < ddls.Count; i++)
                {
                    ddls[i].DataTextField = "ShortName";
                    ddls[i].DataValueField = "Id";
                    ddls[i].DataSource = listSalon;
                    ddls[i].DataBind();
                }
            }
        }

        /// <summary>
        /// Load department
        /// </summary>
        private void bindDepartment()
        {
            using (var db = new Solution_30shineEntities())
            {
                var department = db.Staff_Type.Where(w => w.IsDelete != 1 && w.Publish == true).ToList();
                var first = new Staff_Type();
                first.Id = 0;
                first.Name = "Chọn bộ phận";
                department.Insert(0, first);
                ddlDepartment.DataTextField = "Name";
                ddlDepartment.DataValueField = "Id";
                ddlDepartment.DataSource = department;
                ddlDepartment.DataBind();
                ddlDepartment.SelectedIndex = 0;
            }
        }

        /// <summary>
        /// Load staff
        /// </summary>
        private void bindStaff()
        {
            using (var db = new Solution_30shineEntities())
            {
                var department = Convert.ToInt32(ddlDepartment.SelectedValue);
                var salon = Convert.ToInt32(Salon.SelectedValue);
                var staff = new List<_30shine.MODEL.StaffView>();
                var whereSalon = "";
                var whereDepartment = "";
                var sql = "";
                if (department > 0)
                {
                    whereDepartment = " and s.[Type] = " + department;
                }
                if (salon > 0)
                {
                    whereSalon = " and s.SalonId = " + salon;
                }
                sql = @"select s.Id AS id, CAST((s.Id) AS NVARCHAR(100)) + ' - ' + s.Fullname AS fullName FROM Staff s
                        where s.IsDelete != 1 and s.Active = 1 
                        and (s.isAccountLogin != 1 or s.isAccountLogin is null)" + whereSalon + whereDepartment;
                staff = db.Database.SqlQuery<_30shine.MODEL.StaffView>(sql).ToList();
                var first = new _30shine.MODEL.StaffView();
                first.id = 0;
                first.fullName = "Chọn Stylist";
                staff.Insert(0, first);
                ddlStaff.DataTextField = "fullName";
                ddlStaff.DataValueField = "id";
                ddlStaff.DataSource = staff;
                ddlStaff.DataBind();
            }
        }

        /// <summary>
        /// select change ddl 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void bindStaffByDepartment(object sender, EventArgs e)
        {
            bindStaff();
            ScriptManager.RegisterStartupScript(this, GetType(), "Script", "SelectFunc();", true);
        }

        public List<Salary_InCome_Staff.OutputDataServiceForExcel> GetDataServiceStaffForExcel()
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    int integer;
                    string timeFrom = "";
                    string timeTo = "";
                    var lstJson = new List<ProductBasic>();
                    var list = new List<OutputDataServiceForExcel>();
                    var record = new OutputDataServiceForExcel();
                    var serializer = new JavaScriptSerializer();
                    int salonId = int.TryParse(Salon.SelectedValue, out integer) ? integer : 0;
                    int departmentId = int.TryParse(ddlDepartment.SelectedValue, out integer) ? integer : 0;
                    int staffId = int.TryParse(ddlStaff.SelectedValue, out integer) ? integer : 0;
                    int active = int.TryParse(Active.SelectedValue, out integer) ? integer : 411;
                    if (!String.IsNullOrEmpty(TxtDateTimeFrom.Text.Trim()))
                    {
                        timeFrom = String.Format("{0:yyyy/MM/dd}", Convert.ToDateTime(TxtDateTimeFrom.Text, culture));
                        if (!String.IsNullOrEmpty(TxtDateTimeTo.Text.Trim()))
                        {
                            timeTo = String.Format("{0:yyyy/MM/dd}", Convert.ToDateTime(TxtDateTimeTo.Text, culture));
                        }
                        else
                        {
                            timeTo = String.Format("{0:yyyy/MM/dd}", Convert.ToDateTime(TxtDateTimeFrom.Text, culture));
                        }
                        string sql = $@"BEGIN
                                        DECLARE
                                        @TimeFrom DATETIME,
                                        @TimeTo DATETIME,
                                        @SalonId INT,
                                        @StaffId INT,
                                        @DepartmentId INT,
                                        @IsActive INT
                                        SET @TimeFrom = '{timeFrom}';
                                        SET @TimeTo = '{timeTo}';
                                        SET @SalonId = {salonId};
                                        SET @StaffId = {staffId};
                                        SET @DepartmentId = {departmentId};
                                        SET @IsActive = {active};
                                        SELECT 
                                            s.Id AS StaffId, s.Fullname AS StaffName, t.Id AS DepartmentId,
                                            t.Name AS DepartmentName, l.Name AS LevelName, salon.Id AS SalonId,
                                            salon.ShortName AS SalonName, SUM(ISNULL(income.ServiceSalary, 0)) AS TotalMoneyService, 
                                            SUM(ISNULL(income.RatingPoint,0)) AS TotalPoint 
                                        FROM dbo.Staff AS s
                                        INNER JOIN dbo.SalaryIncome AS income ON income.StaffId = s.Id
                                        INNER JOIN dbo.Staff_Type AS t ON t.Id = s.[Type]
                                        INNER JOIN dbo.Tbl_SkillLevel AS l ON l.Id = income.LevelId
                                        INNER JOIN dbo.Tbl_Salon AS salon ON salon.Id = s.SalonId
                                        WHERE
                                            s.IsDelete = 0 AND income.IsDeleted = 0
                                            AND t.IsDelete = 0 AND l.IsDelete = 0 AND salon.IsDelete = 0
                                            AND income.WorkDate >= @TimeFrom AND income.WorkDate <= @TimeTo
                                            AND ((s.SalonId = @SalonId) OR (@SalonId = 0))
                                            AND ((s.Id = @StaffId) OR (@StaffId = 0))
                                            AND ((s.[Type] = @DepartmentId) OR (@DepartmentId = 0))
                                            AND ((s.Active = @IsActive) OR (@IsActive = 411))
                                            AND s.[Type] IN (1,2) -- only stylist and skinner
                                        GROUP BY s.Id, s.Fullname, salon.Id, salon.ShortName, t.Id, t.Name, l.Name,s.[Type]
                                        ORDER BY s.[Type] ASC, s.Id ASC
                                        END";
                        list = db.Database.SqlQuery<OutputDataServiceForExcel>(sql).ToList();
                        if (list.Any())
                        {
                            DateTime tf = Convert.ToDateTime(timeFrom, culture);
                            DateTime tt = Convert.ToDateTime(timeTo, culture).AddDays(1);
                            var lstConfigService = db.ServiceSalonConfigs.Where(w => w.ServiceId == 90 || w.ServiceId == 91).ToList();
                            var lstBillService = (
                                            from a in db.BillServices
                                            join b in db.FlowServices on a.Id equals b.BillId
                                            where a.CreatedDate >= tf && a.CreatedDate < tt &&
                                                  ((a.SalonId == salonId) || (salonId == 0)) &&
                                                  a.Pending == 0 && a.IsDelete == 0 &&
                                                  b.IsDelete == 0 && a.ServiceIds != "" &&
                                                  (b.ServiceId == 90 || b.ServiceId == 91)

                                            select new
                                            {
                                                SalonId = a.SalonId,
                                                ServiceId = b.ServiceId,
                                                StylistId = a.Staff_Hairdresser_Id ?? 0,
                                                SkinnerId = a.Staff_HairMassage_Id ?? 0,
                                                Quantity = b.Quantity ?? 0
                                            }
                                         ).ToList();


                            foreach (var v in list)
                            {
                                var itemDmn = lstConfigService.FirstOrDefault(w => w.SalonId == v.SalonId && w.DepartmentId == v.DepartmentId && w.ServiceId == 90);
                                if (itemDmn != null)
                                {
                                    //90 Đắp mặt nạ 15k
                                    if (v.DepartmentId == 1)
                                    {
                                        v.TotalMoneyDmn = lstBillService.Where(w => w.ServiceId == 90 && w.StylistId == v.StaffId).Sum(w => w.Quantity) * (itemDmn.ServiceBonus ?? 0);
                                    }
                                    else if (v.DepartmentId == 2)
                                    {
                                        v.TotalMoneyDmn = lstBillService.Where(w => w.ServiceId == 90 && w.SkinnerId == v.StaffId).Sum(w => w.Quantity) * (itemDmn.ServiceBonus ?? 0);
                                    }
                                }
                                var itemTdc = lstConfigService.FirstOrDefault(w => w.SalonId == v.SalonId && w.DepartmentId == v.DepartmentId && w.ServiceId == 91);
                                if (itemTdc != null)
                                {
                                    //91 Tẩy da chết 15k
                                    if (v.DepartmentId == 2)
                                    {
                                        v.TotalMoneyTdc = lstBillService.Where(w => w.ServiceId == 91 && w.SkinnerId == v.StaffId).Sum(w => w.Quantity) * (itemDmn.ServiceBonus ?? 0);
                                    }
                                }
                                v.TotalMoneyFinal = v.TotalMoneyService - v.TotalMoneyTdc - v.TotalMoneyDmn;
                            }
                        }
                    }
                    return list;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Get data
        /// 1. Xử lý đầu vào ( datime, salon, department, staff, active)
        /// 2. Xử lý list data từ api 
        /// 
        /// 3. Sử dụng vòng lặp lấy ra data thưởng phạt nhân viên,  voucherwaitTime(30shinecare), lương phụ cấp
        /// </summary>
        private List<cls_OutputData> getData()
        {
            var listOrigin = new List<cls_OutputData>();
            try
            {
                var test = HDF_SalonId.Value;
                var clsSearch = new cls_Search();
                int integer;
                clsSearch.timeFrom = new DateTime();
                clsSearch.timeTo = new DateTime();
                if (HDF_SalonId.Value != "[]" && !string.IsNullOrEmpty(HDF_SalonId.Value) && HDF_SalonId.Value != "")
                {
                    clsSearch.salonId = HDF_SalonId.Value;
                }
                //clsSearch.salonId = int.TryParse(Salon.SelectedValue, out integer) ? integer : 0;
                clsSearch.departmentId = int.TryParse(ddlDepartment.SelectedValue, out integer) ? integer : 0;
                clsSearch.staffId = int.TryParse(ddlStaff.SelectedValue, out integer) ? integer : 0;
                clsSearch.active = Convert.ToInt32(Active.SelectedValue);
                if (TxtDateTimeFrom.Text != "")
                {
                    clsSearch.timeFrom = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                    if (TxtDateTimeTo.Text != "")
                    {
                        if (TxtDateTimeFrom.Text == TxtDateTimeTo.Text)
                        {
                            clsSearch.timeTo = clsSearch.timeFrom;
                        }
                        else
                        {
                            clsSearch.timeTo = Convert.ToDateTime(TxtDateTimeTo.Text, culture);
                        }
                    }
                    else
                    {
                        clsSearch.timeTo = clsSearch.timeFrom;
                    }
                }
                var request = new Request();
                var response = request.RunPostAsync(Libraries.AppConstants.URL_API_FINANCIAL + "/api/salary-income/get-list-by-staff", clsSearch).Result;
                var result = response.Content.ReadAsStringAsync().Result;
                var serializer = new JavaScriptSerializer();
                if (response.IsSuccessStatusCode)
                {
                    var list = serializer.Deserialize<cls_Salary>(result);
                    if (list != null)
                    {
                        foreach (var v in list.data)
                        {
                            var item = new cls_OutputData();
                            item.Id = v.Id;
                            item.SalonId = v.SalonId;
                            item.FullName = v.FullName;
                            item.NameCMT = v.NameCMT;
                            item.NumberCMT = v.NumberCMT;
                            item.NameType = v.NameType;
                            item.SkillName = v.SkillName;
                            item.NganHang_TenTK = v.NganHang_TenTK;
                            item.NganHang_SoTK = v.NganHang_SoTK;
                            item.NumberInsurrance = v.NumberInsurrance;
                            item.MST = v.MST;
                            item.DepartmentId = v.DepartmentId;
                            item.Workday = v.Workday;
                            item.FixSalaryOscillation = v.FixSalaryOscillation ?? 0;
                            item.FixedSalaryConfig = v.FixedSalaryConfig ?? 0;
                            item.FixedSalary = (v.FixSalaryMainShift ?? 0) + (v.FixSalarySubShift ?? 0);
                            item.FixSalaryMainShift = v.FixSalaryMainShift ?? 0;
                            item.FixSalarySubShift = v.FixSalarySubShift ?? 0;
                            item.BehaveSalary = (v.BehaveSalaryMainShift ?? 0) + (v.BehaveSalarySubShift ?? 0);
                            item.BehaveSalaryMainShift = v.BehaveSalaryMainShift ?? 0;
                            item.BehaveSalarySubShift = v.BehaveSalarySubShift ?? 0;
                            item.ServiceSalary = v.ServiceSalary ?? 0;
                            item.ProductSalary = v.ProductSalary ?? 0;
                            item.OvertimeSalary = v.OvertimeSalary ?? 0;
                            //get list thưởng phạt và voucher waitiTime
                            item.listvoucherWaitTime = new List<cls_VoucherWaitTime>();
                            item.listSalaryIncomeChange = new List<cls_SalaryIncomeChage>();
                            // xử lý bind data voucher waitTime
                            if (item.DepartmentId == 1)
                            {
                                item.MoneyVoucherWaitTime = v.voucherWaitTime.Sum(a => a.stylistmoney);
                            }
                            else if (item.DepartmentId == 2)
                            {
                                item.MoneyVoucherWaitTime = v.voucherWaitTime.Sum(a => a.skinnermoney);
                            }
                            else if (item.DepartmentId == 5)
                            {
                                item.MoneyVoucherWaitTime = v.voucherWaitTime.Sum(a => a.checkinmoney);
                            }
                            // xử lý bind data thưởng phạt, bảo hiểm (sử dụng changetype để phân biệt)
                            item.Buluong = v.salaryIncomeChange.Where(a => a.changedType.Equals("bu_luong")).Sum(s => s.point ?? 0);
                            item.BaoHiem = v.salaryIncomeChange.Where(a => a.changedType.Equals("bao_hiem")).Sum(s => s.point ?? 0);
                            item.Ungluong = v.salaryIncomeChange.Where(a => a.changedType.Equals("ung_luong")).Sum(s => s.point ?? 0); ;
                            item.Trukhac = v.salaryIncomeChange.Where(a => a.changedType.Equals("khac") || a.changedType.Equals("xu_phat")).Sum(s => s.point ?? 0);
                            item.luong_5c = v.salaryIncomeChange.Where(a => a.changedType.Equals("luong_5c")).Sum(s => s.point ?? 0);
                            item.thue_thu_nhap_ca_nhan = v.salaryIncomeChange.Where(a => a.changedType.Equals("thue_thu_nhap_ca_nhan")).Sum(s => s.point ?? 0);
                            item.dhl = v.salaryIncomeChange.Where(a => a.changedType.Equals("dhl")).Sum(s => s.point ?? 0);
                            item.tong_dhl = v.salaryIncomeChange.Where(a => a.changedType.Equals("tong_dhl")).Sum(s => s.point ?? 0);
                            item.listWorkTime = new List<cls_ListWorkTime>();
                            item.Ca1 = v.listWorkTime.FirstOrDefault(a => a.workTimeId == 1) != null ? v.listWorkTime.FirstOrDefault(a => a.workTimeId == 1).workDay : 0;
                            item.Ca2 = v.listWorkTime.FirstOrDefault(a => a.workTimeId == 2) != null ? v.listWorkTime.FirstOrDefault(a => a.workTimeId == 2).workDay : 0;
                            item.Ca3 = v.listWorkTime.FirstOrDefault(a => a.workTimeId == 3) != null ? v.listWorkTime.FirstOrDefault(a => a.workTimeId == 3).workDay : 0;
                            item.Ca11 = v.listWorkTime.FirstOrDefault(a => a.workTimeId == 5) != null ? v.listWorkTime.FirstOrDefault(a => a.workTimeId == 5).workDay : 0;
                            item.Ca31 = v.listWorkTime.FirstOrDefault(a => a.workTimeId == 6) != null ? v.listWorkTime.FirstOrDefault(a => a.workTimeId == 6).workDay : 0;
                            listOrigin.Add(item);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return listOrigin;
        }

        /// <summary>
        /// Get data total
        /// Bind data to rpttotal (tổng lương chung hệ thống)
        /// </summary>
        private List<cls_reportTotal> getDataTotal(List<cls_OutputData> data)
        {
            var list = new List<cls_reportTotal>();
            try
            {
                var item = new cls_reportTotal();
                var index = -1;
                if (data.Count > 0)
                {
                    foreach (var i in data)
                    {
                        if (i.DepartmentId > 0)
                        {
                            item = new cls_reportTotal();
                            index = list.FindIndex(w => w.departmentId == i.DepartmentId);
                            if (index == -1)
                            {
                                item.departmentId = i.DepartmentId;
                                item.departmentName = i.NameType;
                                item.salaryNoProduct = i.FixedSalary + i.BehaveSalary + i.FixSalaryOscillation + i.OvertimeSalary + i.ServiceSalary + i.Buluong - i.MoneyVoucherWaitTime - i.BaoHiem - i.Trukhac - i.Ungluong + (i.luong_5c) - i.thue_thu_nhap_ca_nhan - i.dhl;
                                item.salary = item.salaryNoProduct + i.ProductSalary;
                                list.Add(item);
                            }
                            else
                            {
                                item = list[index];
                                item.salaryNoProduct += i.FixedSalary + i.BehaveSalary + i.FixSalaryOscillation + i.OvertimeSalary + i.ServiceSalary + i.Buluong - i.MoneyVoucherWaitTime - i.BaoHiem - i.Trukhac - i.Ungluong + (i.luong_5c) - i.thue_thu_nhap_ca_nhan - i.dhl;
                                item.salary += i.FixedSalary + i.BehaveSalary + i.FixSalaryOscillation + i.OvertimeSalary + i.ServiceSalary + i.ProductSalary + i.Buluong - i.MoneyVoucherWaitTime - i.BaoHiem - i.Trukhac - i.Ungluong + (i.luong_5c) - i.thue_thu_nhap_ca_nhan - i.dhl;
                                list[index] = item;
                            }
                        }
                    }
                    salaryTotal = list.Sum(r => r.salaryNoProduct);
                }
            }

            catch { }

            return list;

        }

        /// <summary>
        /// Bind data to rptTotal and rptDetail
        /// </summary>
        private void bindData()
        {
            try
            {
                var data = getData();
                var dataTotal = getDataTotal(data);
                Bind_Paging(data.Count());
                rptSalary.DataSource = data.Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                rptSalaryTotal.DataSource = dataTotal;
                rptSalary.DataBind();
                rptSalaryTotal.DataBind();
            }
            catch (Exception ex)
            {
                new Helpers.PushNotiErrorToSlack().Push("GA1LZN9AM", ex.Message + ", MAIN: " + ex.StackTrace + ", " + "SalaryIncomeStaff",
                                          "SalaryIncomeStaff" + ".SalaryIncomeStaff", "QuynhDD",
                                          HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host);
            }

        }

        /// <summary>
        /// event click to load data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void _BtnClick(object sender, EventArgs e)
        {
            bindData();
            //ScriptManager.RegisterStartupScript(this, GetType(), "Script", "SetActiveTableListing();ReplaceZezoValue();SelectFunc();", true);
            RemoveLoading();
        }

        /// <summary>
        /// Export to excel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Exc_ExportExcelService(object sender, EventArgs e)
        {
            try
            {
                var _ExcelHeadRow = new List<string>();
                var serializer = new JavaScriptSerializer();
                var BillRowLST = new List<List<string>>();
                var BillRow = new List<string>();
                _ExcelHeadRow.Add("Id");
                _ExcelHeadRow.Add("Tên nhân viên");
                _ExcelHeadRow.Add("Bộ phận");
                _ExcelHeadRow.Add("Bậc kỹ năng");
                _ExcelHeadRow.Add("Tên Salon");
                _ExcelHeadRow.Add("Tổng tiền dịch vụ (a)");
                _ExcelHeadRow.Add("Tổng tiền tẩy da chết (b)");
                _ExcelHeadRow.Add("Tổng tiền đắp mặt nạ (c)");
                _ExcelHeadRow.Add("Tổng tiền ( a - (b + c) )");
                _ExcelHeadRow.Add("Tổng thưởng tích lũy tháng");
                var data = GetDataServiceStaffForExcel();
                foreach (var v in data)
                {
                    BillRow = new List<string>();
                    BillRow.Add(v.StaffId.ToString());
                    BillRow.Add(v.StaffName);
                    BillRow.Add(v.DepartmentName.ToString());
                    BillRow.Add(v.LevelName.ToString());
                    BillRow.Add(v.SalonName);
                    BillRow.Add(v.TotalMoneyService.ToString());
                    BillRow.Add(v.TotalMoneyTdc.ToString());
                    BillRow.Add(v.TotalMoneyDmn.ToString());
                    BillRow.Add(v.TotalMoneyFinal.ToString());
                    BillRow.Add(v.TotalPoint.ToString());
                    BillRowLST.Add(BillRow);
                }
                // export
                var ExcelStorePath = Server.MapPath("~") + "Public/Excel/Nhan.Vien/";
                var FileName = "Tong_Tien_Dich_Vu_Theo_Staff_" + string.Format("{0:dd_MM_yyyy_hh_mm_ss}", DateTime.Now) + ".xlsx";
                var UrlFileName = "http://" + Request.Url.Host + "/Public/Excel/Nhan.Vien/" + FileName;
                ExportExcelService(_ExcelHeadRow, BillRowLST, ExcelStorePath + FileName);
                ScriptManager.RegisterStartupScript(BtnFakeExcel, this.GetType(), "Download Excel", "CallBackExportExcel('" + UrlFileName + "');", true);
            }
            catch (Exception ex)
            {
                TriggerJsMsgSystem_temp("Lỗi: " + ex.Message, "msg-system warning", 15000);
            }
        }

        private static void ExportExcelService(List<string> _ExcelHeadRow, List<List<string>> List, string Path)
        {
            try
            {
                CreateExcelFile.ExcelHeadRow = _ExcelHeadRow;
                CreateExcelFile.CreateExcel(List, Path);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Export to excel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Exc_ExportExcel(object sender, EventArgs e)
        {
            var _ExcelHeadRow = new List<string>();
            var BillRowLST = new List<List<string>>();
            var BillRow = new List<string>();


            _ExcelHeadRow.Add("ID");
            _ExcelHeadRow.Add("Tên nhân viên");
            _ExcelHeadRow.Add("Tên CMT");
            _ExcelHeadRow.Add("Số CMT");
            _ExcelHeadRow.Add("Bộ phận");
            _ExcelHeadRow.Add("Bậc kỹ năng");
            _ExcelHeadRow.Add("Tổng công");
            _ExcelHeadRow.Add("Ca1");
            _ExcelHeadRow.Add("Ca2");
            _ExcelHeadRow.Add("Ca3");
            _ExcelHeadRow.Add("Ca1.1");
            _ExcelHeadRow.Add("Ca3.1");
            _ExcelHeadRow.Add("Lương cơ bản");
            _ExcelHeadRow.Add("Lương CB theo ngày công (ca thường)");
            _ExcelHeadRow.Add("Lương CB theo ngày công (ca gãy) ");
            _ExcelHeadRow.Add("Lương VHPV (ca thường)");
            _ExcelHeadRow.Add("Lương VHPV (ca gãy)");
            _ExcelHeadRow.Add("Thưởng dịch vụ");
            _ExcelHeadRow.Add("TIP");
            _ExcelHeadRow.Add("Lương part-time");
            _ExcelHeadRow.Add("Bù lương");
            _ExcelHeadRow.Add("Bảo hiểm");
            _ExcelHeadRow.Add("Thuế thu nhập cá nhân");
            _ExcelHeadRow.Add("Trừ khác");
            _ExcelHeadRow.Add("Trừ/Thưởng 5C");
            _ExcelHeadRow.Add("Tạm ứng");
            _ExcelHeadRow.Add("Thưởng tích lũy tháng");
            _ExcelHeadRow.Add("30shine Care");
            _ExcelHeadRow.Add("Tổng lương");
            _ExcelHeadRow.Add("Tên TK Ngân hàng");
            _ExcelHeadRow.Add("Số TK Ngân hàng");
            _ExcelHeadRow.Add("Số BHXH");
            _ExcelHeadRow.Add("Mã số thuế TNCN");
            var data = getData();
            foreach (var v in data)
            {
                BillRow = new List<string>();
                BillRow.Add(v.Id.ToString());
                BillRow.Add(v.FullName);
                BillRow.Add(v.NameCMT);
                BillRow.Add(v.NumberCMT);
                BillRow.Add(v.NameType);
                BillRow.Add(v.SkillName);
                BillRow.Add(v.Workday.ToString());
                BillRow.Add(v.Ca1.ToString());
                BillRow.Add(v.Ca2.ToString());
                BillRow.Add(v.Ca3.ToString());
                BillRow.Add(v.Ca11.ToString());
                BillRow.Add(v.Ca31.ToString());
                BillRow.Add(v.FixedSalaryConfig.ToString());
                BillRow.Add(v.FixSalaryMainShift.ToString());
                BillRow.Add(v.FixSalarySubShift.ToString());
                BillRow.Add(v.BehaveSalaryMainShift.ToString());
                BillRow.Add(v.BehaveSalarySubShift.ToString());
                //BillRow.Add(v.BehaveSalary.ToString());
                BillRow.Add(v.ServiceSalary.ToString());
                BillRow.Add(v.ProductSalary.ToString());
                BillRow.Add(v.OvertimeSalary.ToString());
                BillRow.Add(v.Buluong.ToString());
                BillRow.Add(v.BaoHiem.ToString());
                BillRow.Add(v.thue_thu_nhap_ca_nhan.ToString());
                BillRow.Add(v.Trukhac.ToString());
                BillRow.Add(v.luong_5c.ToString());
                BillRow.Add(v.Ungluong.ToString());
                BillRow.Add(v.dhl.ToString());
                BillRow.Add(v.MoneyVoucherWaitTime.ToString());
                BillRow.Add(Convert.ToString(v.FixedSalary + v.BehaveSalary + v.ServiceSalary + v.ProductSalary + v.Buluong + v.FixSalaryOscillation + v.OvertimeSalary - v.Xuphat - v.BaoHiem - v.Ungluong + (v.luong_5c) - v.MoneyVoucherWaitTime - v.thue_thu_nhap_ca_nhan - v.dhl));
                BillRow.Add(v.NganHang_TenTK);
                BillRow.Add(v.NganHang_SoTK);
                BillRow.Add(v.NumberInsurrance);
                BillRow.Add(v.MST);
                BillRowLST.Add(BillRow);
            }
            // export
            var ExcelStorePath = Server.MapPath("~") + @"Public\\Excel\\Nhan.Vien\\";
            var FileName = "Hoa_Don_" + string.Format("{0:dd_MM_yyyy_hh_mm_ss}", DateTime.Now) + ".xlsx";
            var UrlFileName = "http://" + Request.Url.Host + "/Public/Excel/Nhan.Vien/" + FileName;
            ExportXcel(_ExcelHeadRow, BillRowLST, ExcelStorePath + FileName);
            ScriptManager.RegisterStartupScript(BtnFakeExcel, this.GetType(), "Download Excel", "CallBackExportExcel('" + UrlFileName + "');UP_FormatPrice('.be-report-price');", true);
        }

        protected void BtnExcelProduct_Click(object sender, EventArgs e)
        {

            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    int integer = 0;
                    var departmentId = int.TryParse(ddlDepartment.SelectedValue, out integer) ? integer : 0;
                    int salonId = int.TryParse(Salon.SelectedValue, out integer) ? integer : 0;
                    int staffId = int.TryParse(ddlStaff.SelectedValue, out integer) ? integer : 0;
                    int active = int.TryParse(Active.SelectedValue, out integer) ? integer : 0;
                    string timeFrom = "";
                    string timeTo = "";
                    if (TxtDateTimeFrom.Text != "")
                    {
                        var dateFrom = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                        timeFrom = string.Format("{0:yyyy-MM-dd}", dateFrom);
                        if (TxtDateTimeTo.Text != "")
                        {
                            var dateTo = Convert.ToDateTime(TxtDateTimeTo.Text, culture);
                            if (TxtDateTimeFrom.Text == TxtDateTimeTo.Text)
                            {
                                timeTo = string.Format("{0:yyyy-MM-dd}", dateFrom.AddDays(1));
                            }
                            else
                            {
                                timeTo = string.Format("{0:yyyy-MM-dd}", dateTo.AddDays(1));
                            }
                        }
                        else
                        {
                            timeTo = string.Format("{0:yyyy-MM-dd}", dateFrom.AddDays(1));
                        }
                    }
                    string sql = @"	
                                DECLARE
		                            @timeFrom DATE, @timeTo DATE, @salonId INT, @department INT, @active INT, @staffId INT
		                            SET @timeFrom = '" + timeFrom + @"'
		                            SET @timeTo = '" + timeTo + @"'
		                            SET @salonId = " + salonId + @"
		                            SET @department = " + departmentId + @"
		                            SET @staffId = " + staffId + @"
		                            SET @active = " + active + @"
	                            BEGIN
		                            WITH bills AS (

			                            SELECT bill.SellerId AS staffId, SUM(flproduct.Price * flproduct.Quantity * (100 - CAST(flproduct.VoucherPercent AS FLOAT))/100)  AS tongtien
			                            FROM dbo.BillService AS bill 
				                            INNER JOIN dbo.FlowProduct AS flproduct ON flproduct.BillId = bill.Id AND bill.Pending = 0 AND bill.IsDelete = 0
			                            WHERE bill.CreatedDate BETWEEN @timeFrom AND @timeTo AND ((bill.SellerId = @staffId AND @staffId > 0) OR @staffId = 0) AND
                                              bill.ProductIds != '' AND  flproduct.ComboId IS NULL AND bill.ProductIds IS NOT NULL AND flproduct.IsDelete = 0
			                            GROUP BY bill.SellerId
		                            )
			                            SELECT income.StaffId, staff.Fullname, leve.Name AS Skinnleve, salon.ShortName AS SalonShortName, SUM(income.ProductSalary) AS Totaltip, ISNULL(bills.tongtien, 0) AS TotalProduct
			                            FROM dbo.SalaryIncome AS income
	 			                            INNER JOIN staff AS staff ON staff.Id = income.StaffId
				                            LEFT JOIN dbo.Tbl_SkillLevel AS leve ON staff.SkillLevel = leve.Id
				                            INNER JOIN dbo.Tbl_Salon AS salon ON salon.Id = staff.SalonId
				                            LEFT JOIN bills ON bills.staffId = income.StaffId
			                            WHERE income.IsDeleted = 0 AND income.WorkDate >= @timeFrom AND  income.WorkDate < @timeTo AND 
				                            ((staff.SalonId = @salonId AND @salonId > 0 ) OR @salonId = 0 ) AND 
				                            staff.IsDelete = 0 AND  ((staff.Active = @active AND @active != 411) OR @active = 411 ) AND
				                            ((staff.[Type] = @department AND @department > 0) OR @department = 0) AND 
				                            ((staff.Id = @staffId AND @staffId > 0) OR @staffId = 0) AND 
				                            leve.IsDelete = 0 AND leve.Publish = 1 AND salon.IsDelete = 0 AND salon.Publish = 1
			                            GROUP BY income.StaffId, staff.Fullname, leve.Name, salon.ShortName,bills.tongtien
			                            ORDER BY salon.ShortName  DESC
	                            END ";
                    var data = db.Database.SqlQuery<cls_ExcelProduct>(sql);
                    var excelHeadRow = new List<string>() { "StaffId", "Tên NV", "Bậc", "Tên Salon", "Tổng MP Bán", "Tổng Tip" };
                    var lstSalaryRow = new List<List<string>>();
                    foreach (var v in data)
                    {
                        var salaryRow = new List<string>() { v.StaffId.ToString(), v.FullName, v.Skinnleve, v.SalonShortName, v.TotalProduct.ToString(), v.TotalTip.ToString() };
                        lstSalaryRow.Add(salaryRow);
                    }
                    // export
                    var ExcelStorePath = Server.MapPath("~") + "Public/Excel/Nhan.Vien/";
                    var FileName = "Bang_luong_" + string.Format("{0:dd_MM_yyyy_hh_mm_ss}", DateTime.Now) + ".xlsx";
                    var UrlFileName = "http://" + Request.Url.Host + "/Public/Excel/Nhan.Vien/" + FileName;
                    ExportXcel(excelHeadRow, lstSalaryRow, ExcelStorePath + FileName);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Download Excel", "CallBackExportExcel('" + UrlFileName + "')", true);
                }
            }
            catch (Exception ex)
            {
                TriggerJsMsgSystem_temp("Lỗi: " + ex.Message, "msg-system warning", 15000);
            }
        }

        private void ExportXcel(List<string> _ExcelHeadRow, List<List<string>> List, string Path)
        {
            try
            {
                CreateExcelFile.ExcelHeadRow = _ExcelHeadRow;
                CreateExcelFile.CreateExcel(List, Path);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Paging
        /// </summary>
        /// <param name="TotalRow"></param>
        protected void Bind_Paging(int TotalRow)
        {
            try
            {
                // init Paging value
                int integer = 1;
                PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
                PAGING._PageNumber = !IsPostBack ? 1 : (int.TryParse(HDF_Page.Value, out integer) ? integer : 1);
                PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
                PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(TotalRow) : PAGING.TotalPage;
                PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
                PAGING._Paging = PAGING.Make_Paging();

                RptPaging.DataSource = PAGING._Paging.ListPage;
                RptPaging.DataBind();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        protected int Get_TotalPage(int TotalRow)
        {
            TotalRow -= PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        public void TriggerJsMsgSystem_temp(string msg, string status, int duration)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Message System", "showMsgSystem('" + msg.Replace("'", @"\'") + "','" + status + "'," + duration + ");", true);
        }

        /// <summary>
        /// author: QuynhDD
        /// </summary>
        public class cls_Salary
        {
            public List<Origin> data { get; set; }
        }

        /// <summary>
        /// List data ban đầu
        /// </summary> 
        public class Origin
        {
            public int Id { get; set; }
            public int? SalonId { get; set; }
            public string FullName { get; set; }
            public string NameCMT { get; set; }
            public string NumberCMT { get; set; }
            public int? DepartmentId { get; set; }
            public string NameType { get; set; }
            public string SkillName { get; set; }
            public string NganHang_TenTK { get; set; }
            public string NganHang_SoTK { get; set; }
            public string NumberInsurrance { get; set; }
            public string MST { get; set; }
            /// <summary>
            /// workday
            /// </summary>
            public int Workday { get; set; }
            public List<cls_ListWorkTime> listWorkTime { get; set; }
            /// <summary>
            /// lương hỗ trợ
            /// </summary>
            public double? FixSalaryOscillation { get; set; }
            /// <summary>
            /// get lương cơ bản
            /// </summary>
            public double? FixedSalaryConfig { get; set; }
            /// <summary>
            /// Lấy bản ghi lương của nhân viên
            /// </summary>
            //public Models.Entity.SalaryIncome SalaryIncome { get; set; }
            public double? FixSalaryMainShift { get; set; }
            public double? FixSalarySubShift { get; set; }
            public double? AllowanceSalary { get; set; }
            public double? OvertimeSalary { get; set; }
            public double? ProductSalary { get; set; }
            public double? ServiceSalary { get; set; }
            public double? TotalIncome { get; set; }
            public double? GrandTotalIncome { get; set; }
            public double? BehaveSalaryMainShift { get; set; }
            public double? BehaveSalarySubShift { get; set; }

            public List<cls_SalaryIncomeChage> salaryIncomeChange { get; set; }
            public List<cls_VoucherWaitTime> voucherWaitTime { get; set; }

            public Origin()
            {
                SalonId = 0;
                NameCMT = "";
                NumberCMT = "";
                NganHang_SoTK = "";
                NganHang_TenTK = "";
                NumberInsurrance = "";
                MST = "";
            }
        }

        /// <summary>
        ///  Output Data bind rpt
        /// </summary>
        public class cls_OutputData
        {
            public int Id { get; set; }
            public int? StaffId { get; set; }
            public int? SalonId { get; set; }
            public double FixedSalary { get; set; }
            public double? FixSalaryMainShift { get; set; }
            public double? FixSalarySubShift { get; set; }
            public double BehaveSalary { get; set; }
            public double BehaveSalaryMainShift { get; set; }
            public double BehaveSalarySubShift { get; set; }
            public double AllowanceSalary { get; set; }
            public double OvertimeSalary { get; set; }
            public double ServiceSalary { get; set; }
            public double ProductSalary { get; set; }
            public double TotalIncome { get; set; }
            public double GrandTotalIncome { get; set; }
            public DateTime? CreatedTime { get; set; }
            public DateTime? ModifiedTime { get; set; }
            public bool? IsDeleted { get; set; }
            //public int? ChangeTypeId { get; set; }
            public string description { get; set; }
            public string changedType { get; set; }
            public double? Point { get; set; }
            public string FullName { get; set; }
            public string NameCMT { get; set; }
            public string NumberCMT { get; set; }
            public string NameType { get; set; }
            public int? DepartmentId { get; set; }
            public string SkillName { get; set; }
            public string NganHang_TenTK { get; set; }
            public string NganHang_SoTK { get; set; }
            public string NumberInsurrance { get; set; }
            public string MST { get; set; }
            public int? Workday { get; set; }
            public double FixSalaryOscillation { get; set; }
            public double FixedSalaryConfig { get; set; }
            public double BaoHiem { get; set; }
            public double Xuphat { get; set; }
            public double Ungluong { get; set; }
            public double Buluong { get; set; }
            public double Trukhac { get; set; }
            public double luong_5c { get; set; }
            public double thue_thu_nhap_ca_nhan { get; set; }
            public double dhl { get; set; }
            public double tong_dhl { get; set; }
            public double MoneyVoucherWaitTime { get; set; }
            public List<cls_SalaryIncomeChage> listSalaryIncomeChange { get; set; }
            public List<cls_VoucherWaitTime> listvoucherWaitTime { get; set; }
            public List<cls_ListWorkTime> listWorkTime { get; set; }
            public int Ca1 { get; set; }
            public int Ca2 { get; set; }
            public int Ca3 { get; set; }
            public int Ca11 { get; set; }
            public int Ca31 { get; set; }
        }

        /// <summary>
        /// Salary_Income_Change
        /// </summary>
        public class cls_SalaryIncomeChage
        {
            public string description { get; set; }
            public string changedType { get; set; }
            public int? point { get; set; }
        }

        public class cls_ListWorkTime
        {
            public int workTimeId { get; set; }
            public string workName { get; set; }
            public int workDay { get; set; }
        }
        public class cls_reportTotal
        {
            /// <summary>
            /// Bộ phận ID
            /// </summary>
            public int? departmentId { get; set; }
            /// <summary>
            /// Tên bộ phận
            /// </summary>
            public string departmentName { get; set; }
            /// <summary>
            /// Tổng lương (ko tính lương sản phẩm)
            /// </summary>
            public double? salaryNoProduct { get; set; }
            /// <summary>
            /// Tổng lương
            /// </summary>
            public double? salary { get; set; }
        }

        /// <summary>
        /// Custom class đầu vào tìm kiếm
        /// </summary>
        public class cls_Search
        {
            public DateTime timeFrom { get; set; }
            public DateTime timeTo { get; set; }
            public string salonId { get; set; }
            public int departmentId { get; set; }
            public int staffId { get; set; }
            public int active { get; set; }
            //public int pagez { get; set; }
            //public int pageSize { get; set; }
        }

        /// <summary>
        /// 30shine care 
        /// </summary>
        public class cls_VoucherWaitTime
        {
            public double checkinmoney { get; set; }
            public double skinnermoney { get; set; }
            public double stylistmoney { get; set; }
        }

        public class cls_ExcelProduct
        {
            public int StaffId { get; set; }
            public string FullName { get; set; }
            public string Skinnleve { get; set; }
            public string SalonShortName { get; set; }
            public double TotalTip { get; set; }
            public double TotalProduct { get; set; }
        }

        public class OutputDataServiceForExcel
        {
            public int StaffId { get; set; }
            public string StaffName { get; set; }
            public int DepartmentId { get; set; }
            public string DepartmentName { get; set; }
            public string LevelName { get; set; }
            public int SalonId { get; set; }
            public string SalonName { get; set; }
            public double TotalMoneyService { get; set; }
            public double TotalMoneyTdc { get; set; }
            public double TotalMoneyDmn { get; set; }
            public double TotalMoneyFinal { get; set; }
            public double TotalPoint { get; set; }
        }
    }
}