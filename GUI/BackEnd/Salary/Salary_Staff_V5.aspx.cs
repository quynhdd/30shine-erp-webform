﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Globalization;
using LinqKit;
using System.Linq.Expressions;
using ExportToExcel;
using System.Data.SqlClient;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.SalaryV5
{
    public partial class Salary_Staff_V5 : System.Web.UI.Page
    {
        protected Paging PAGING = new Paging();

        private int _TypeStaff;
        private int _StaffId;

        protected bool Staff_All = false;
        protected bool Staff_Hairdresser = false;
        protected bool Staff_HairMassage = false;
        protected bool Staff_Singer = false;
        protected string THead = "";
        public CultureInfo culture = new CultureInfo("vi-VN");
        private DateTime d1 = new DateTime();
        private DateTime d2 = new DateTime();
        public DateTime timeFrom;
        public DateTime timeTo;
        private Expression<Func<Staff, bool>> WhereStaff = PredicateBuilder.True<Staff>();
        private List<Service> _Service = new List<Service>();
        private List<Product> _Product = new List<Product>();
        protected string FirstDayOfMonth = string.Format("{0:dd/MM/yyyy}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1));
        protected string Today = String.Format("{0:dd/MM/yyyy}", DateTime.Now);
        protected int ThisMon;// = DateTime.Now.Month;
        protected int DayOfMon;// = DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month);
        protected int ConfigWorkDay = 0;

        private string PageID = "BC_LUONG";
        private bool Perm_ViewAllData = false;
        private bool Perm_Access = false;
        protected bool isRoot = false;
        protected int departmentId;
        public void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                ////Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                ////Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        private void ExecuteByPermission()
        {

            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (!IsPostBack)
            {
                TxtDateTimeFrom.Text = FirstDayOfMonth;
                TxtDateTimeTo.Text = Today;
                Library.Function.bindSalon(new List<DropDownList> { Salon }, Perm_ViewAllData);
                bindDepartment();
                bindStaff();
            }
            else
            {
                //
            }
            RemoveLoading();
        }

        private string genSql()
        {
            string whereStaff = "";
            string whereSalon = "";
            string whereActive = "";
            string sql = "";
            this.departmentId = Convert.ToInt32(ddlDepartment.SelectedValue);
            int staffId = Convert.ToInt32(ddlStaff.SelectedValue);
            int SalonId = Convert.ToInt32(Salon.SelectedValue);
            int active = Convert.ToInt32(Active.SelectedValue);
            var timeFrom = new DateTime();
            var timeTo = new DateTime();

            if (!Perm_ViewAllData)
            {
                whereSalon = " and staff.SalonId = " + SalonId;
            }
            else
            {
                if (SalonId > 0)
                {
                    whereSalon = " and staff.SalonId = " + SalonId;
                }
                else
                {
                    whereSalon = " and staff.SalonId >= 0";
                }
            }

            if (staffId > 0)
            {
                whereStaff = " and staff.Id = " + staffId;
            }
            else if (departmentId > 0)
            {
                whereStaff = " and staff.[Type] = " + departmentId;
            }

            if (active > 0)
            {
                whereActive = " and staff.[Active] = 1";
            }
            else if (active == 0)
            {
                whereActive = " and staff.[Active] = 0";
            }

            if (TxtDateTimeFrom.Text != "")
            {
                timeFrom = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                if (TxtDateTimeTo.Text != "")
                {
                    if (TxtDateTimeFrom.Text == TxtDateTimeTo.Text)
                    {
                        timeTo = timeFrom;
                    }
                    else
                    {
                        timeTo = Convert.ToDateTime(TxtDateTimeTo.Text, culture);
                    }
                }
                else
                {
                    timeTo = timeFrom;
                }
            }
            // Số ngày công trong tháng (= số ngày trong tháng - 2)
            DayOfMon = DateTime.DaysInMonth(d1.Year, d1.Month) - 2;
            sql = @"declare @workDayInMonth int;
                    set @workDayInMonth = " + DayOfMon + @";

                    select staff.*, Coalesce(skillLevel.[Order], 0) as skillLevelOrder, Coalesce(@workDayInMonth, 0) as workDayInMonth,
	                    department.Name as departmentName,
	                    skillLevel.Name as skillLevelName,
	                    Coalesce(payon.Value, 0) as baseSalary,
	                    fs.workDay, fs.fixSalary, fs.allowanceSalary, fs.partTimeSalary, fs.serviceSalary, fs.productSalary, fs.mistake, fs.careSalary, fs.salesSalary
	
                    from Staff as staff
                    left join Staff_Type as department
                    on staff.[Type] = department.Id
                    left join Tbl_SkillLevel as skillLevel
                    on staff.SkillLevel = skillLevel.Id
                    left join Tbl_Payon as payon
                    on payon.TypeStaffId = staff.[Type] and payon.ForeignId = staff.SkillLevel and payon.Publish = 1
	                    and payon.Hint = 'base_salary' and (payon.PayByTime = 1 or payon.PayByTime = 2)
                    left join
                    (
	                    select staff.Id as staffId, 
		                    Coalesce(SUM(Coalesce(flowSalary.fixSalary, 0)), 0) as fixSalary,
		                    Coalesce(SUM(Coalesce(flowSalary.allowanceSalary, 0)), 0) as allowanceSalary,
		                    Coalesce(SUM(Coalesce(flowSalary.partTimeSalary, 0)), 0) as partTimeSalary,
		                    Coalesce(SUM(Coalesce(flowSalary.serviceSalary, 0)), 0) as serviceSalary,
		                    Coalesce(SUM(Coalesce(flowSalary.productSalary, 0)), 0) as productSalary,
                            Coalesce(SUM(Coalesce(flowSalary.Mistake_Point, 0)), 0) as mistake,
	                        Coalesce(SUM(Coalesce(flowSalary.SalaryCare, 0)), 0) as careSalary,
							Coalesce(SUM(Coalesce(flowSalary.SalesSalary, 0)), 0) as salesSalary,
		                    Coalesce((
			                    select COUNT(*) from FlowSalary where IsDelete != 1
			                    and sDate between '" + string.Format("{0:yyyy/MM/dd}", timeFrom) + "' and '" + string.Format("{0:yyyy/MM/dd}", timeTo) + @"'
			                    and fixSalary > 0 and staffId = staff.Id
		                    ), 0) as workDay
	                    from Staff as staff
	                    left join FlowSalary as flowSalary
	                    on staff.Id = flowSalary.staffId and flowSalary.IsDelete != 1
	                        and flowSalary.sDate between '" + string.Format("{0:yyyy/MM/dd}", timeFrom) + "' and '" + string.Format("{0:yyyy/MM/dd}", timeTo) + @"'
	                    group by staff.Id
                    ) as fs
                    on staff.Id = fs.staffId
                    where staff.IsDelete != 1 
                    and (staff.isAccountLogin != 1 or staff.isAccountLogin is null)" +
                    whereSalon + whereStaff + whereActive +
                    " order by staff.[Type] asc, skillLevel.[Order] asc";
            return sql;
        }

        private List<cls_staff> getData()
        {
            this.departmentId = Convert.ToInt32(ddlDepartment.SelectedValue);
            int staffId = Convert.ToInt32(ddlStaff.SelectedValue);
            int SalonId = Convert.ToInt32(Salon.SelectedValue);
            int active = Convert.ToInt32(Active.SelectedValue);
            this.timeFrom = new DateTime();
            this.timeTo = new DateTime();
            if (TxtDateTimeFrom.Text != "")
            {
                timeFrom = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                if (TxtDateTimeTo.Text != "")
                {
                    if (TxtDateTimeFrom.Text == TxtDateTimeTo.Text)
                    {
                        timeTo = timeFrom;
                    }
                    else
                    {
                        timeTo = Convert.ToDateTime(TxtDateTimeTo.Text, culture);
                    }
                }
                else
                {
                    timeTo = timeFrom;
                }
            }
            object[] paras =
            {
                new SqlParameter("@timeFrom", string.Format("{0:yyyy/MM/dd}", this.timeFrom)),
                new SqlParameter("@timeTo", string.Format("{0:yyyy/MM/dd}", this.timeTo)),
                new SqlParameter("@salonId", SalonId),
                new SqlParameter("@staffType", this.departmentId),
                new SqlParameter("@staffId", staffId),
                new SqlParameter("@active", active)
            };
            var list = new List<cls_staff>();
            using (var db = new Solution_30shineEntities())
            {
                list = db.Database.SqlQuery<cls_staff>("sp_BC_SalaryReport_V3 @timeFrom , @timeTo ,  @salonId , @staffType , @staffId , @active ", paras).ToList();
                foreach (var item in list)
                {
                    //Dũng EDIT Staffautolevel
                    var autoLog = db.StaffAutoLevelLogs.Where(f => f.Staff_Id == staffId).OrderByDescending(f => f.Id).FirstOrDefault();
                    if (autoLog != null)
                    {
                        if (timeFrom > autoLog.sDate && autoLog.StatusSkillLevel == true && autoLog.TSL_DanhGia != null && autoLog.Approval == 2)
                        {
                            item.SkillLevel = autoLog.SkillLevelup_Id;
                            var objSkillName = db.Tbl_SkillLevel.FirstOrDefault(f => f.Id == autoLog.SkillLevelup_Id);
                            item.skillLevelName = objSkillName.Name;
                            var objSalary = db.Tbl_Payon.FirstOrDefault(f => f.TypeStaffId == autoLog.StaffType_Id && f.PayByTime == 1 && f.ForeignId == autoLog.SkillLevelup_Id);
                            item.baseSalary = (int)objSalary.Value;
                        }
                        else if (autoLog.sDate > timeFrom && timeTo >= autoLog.sDate && autoLog.StatusSkillLevel == true && autoLog.TSL_DanhGia != null && autoLog.Approval == 2)
                        {
                            item.SkillLevel = autoLog.SkillLevelup_Id;
                            var objSkillName = db.Tbl_SkillLevel.FirstOrDefault(f => f.Id == autoLog.SkillLevelup_Id);
                            item.skillLevelName = objSkillName.Name;
                            var objSalary = db.Tbl_Payon.FirstOrDefault(f => f.TypeStaffId == autoLog.StaffType_Id && f.PayByTime == 1 && f.ForeignId == autoLog.SkillLevelup_Id);
                            item.baseSalary = (int)objSalary.Value;
                        }
                        else if (autoLog.sDate >= timeFrom && autoLog.sDate >= timeTo)
                        {
                            item.SkillLevel = autoLog.SkillLevel_Id;
                            var objSkillName = db.Tbl_SkillLevel.FirstOrDefault(f => f.Id == autoLog.SkillLevel_Id);
                            item.skillLevelName = objSkillName.Name;
                            var objSalary = db.Tbl_Payon.FirstOrDefault(f => f.TypeStaffId == autoLog.StaffType_Id && f.PayByTime == 1 && f.ForeignId == autoLog.SkillLevel_Id);
                            item.baseSalary = (int)objSalary.Value;
                        }
                    }
                    //END edit staffautolevel
                }
                return list;
            }
        }
        private List<cls_reportTotal> getDataTotal(List<cls_staff> data)
        {
            var list = new List<cls_reportTotal>();
            var item = new cls_reportTotal();
            var index = -1;
            Solution_30shineEntities db = new Solution_30shineEntities();
            if (data.Count > 0)
            {
                foreach (var v in data)
                {
                    if (v.Type > 0)
                    {
                        var checkinMoneyCare = from o in db.CRM_VoucherWaitTime
                                          where o.CheckinID == v.Id && o.CreatedTime > this.timeFrom && o.CreatedTime < this.timeTo
                                          select new { o.CheckinMoney };
                        v.careSalary = (double)checkinMoneyCare.AsEnumerable().Sum(o => o.CheckinMoney);
                        item = new cls_reportTotal();
                        index = list.FindIndex(w => w.departmentId == v.Type);
                        if (index == -1)
                        {
                            // Thêm mới bộ phận vào list
                            item.departmentId = v.Type.Value;
                            item.departmentName = v.departmentName;
                            //item.salaryNoProduct = v.fixSalary + v.allowanceSalary + v.partTimeSalary + v.serviceSalary - v.careSalary - v.mistake;
                            item.salaryNoProduct = v.fixSalary + v.MistakeExtra + v.partTimeSalary + v.serviceSalary - v.careSalary - v.mistake - v.MistakeInsurrance - v.MistakeOrther - v.MistakeTemporary;
                            //item.salary = item.salaryNoProduct + v.productSalary - v.mistake;
                            item.salary = item.salaryNoProduct + v.productSalary;
                            list.Add(item);
                        }
                        else
                        {
                                // Cập nhật giá trị các thuộc tính của bộ phận
                                item = list[index];
                            //item.salaryNoProduct += v.fixSalary + v.allowanceSalary + v.partTimeSalary + v.serviceSalary - v.careSalary - v.mistake;
                            //item.salary += v.fixSalary + v.allowanceSalary + v.partTimeSalary + v.serviceSalary + v.productSalary - v.mistake ;
                            item.salaryNoProduct += v.fixSalary + v.MistakeExtra + v.partTimeSalary + v.serviceSalary - v.careSalary - v.mistake - v.MistakeInsurrance - v.MistakeOrther - v.MistakeTemporary;
                            item.salary += v.fixSalary + v.MistakeExtra + v.partTimeSalary + v.serviceSalary + v.productSalary - v.mistake - v.MistakeInsurrance - v.MistakeOrther - v.MistakeTemporary- v.careSalary;
                            list[index] = item;
                        }
                    }
                }
            }
            return list;
        }

        private void bindSalary()
        {
            var data = getData();
            var dataTotal = getDataTotal(data);
            rptSalary.DataSource = data.Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
            rptSalaryTotal.DataSource = dataTotal;
            rptSalary.DataBind();
            rptSalaryTotal.DataBind();
        }
        

        private void bindDepartment()
        {
            using (var db = new Solution_30shineEntities())
            {
                var department = db.Staff_Type.Where(w => w.IsDelete != 1 && w.Publish == true).ToList();
                //var first = new Staff_Type();
                //first.Id = 0;
                //first.Name = "Chọn bộ phận";
                //department.Insert(0, first);

                ddlDepartment.DataTextField = "Name";
                ddlDepartment.DataValueField = "Id";
                ddlDepartment.DataSource = department;
                ddlDepartment.DataBind();
                ddlDepartment.SelectedIndex = 0;
            }
        }

        private void bindStaff()
        {
            using (var db = new Solution_30shineEntities())
            {
                var department = Convert.ToInt32(ddlDepartment.SelectedValue);
                var salon = Convert.ToInt32(Salon.SelectedValue);
                var staff = new List<Staff>();
                var whereSalon = "";
                var whereDepartment = "";
                var sql = "";
                if (department > 0)
                {
                    whereDepartment = " and [Type] = " + department;
                }
                if (salon > 0)
                {
                    whereSalon = " and SalonId = " + salon;
                }
                sql = @"select * from Staff 
                        where IsDelete != 1 and Active = 1 
                        and (isAccountLogin != 1 or isAccountLogin is null)" + whereSalon + whereDepartment;
                staff = db.Staffs.SqlQuery(sql).ToList();

                var first = new Staff();
                first.Id = 0;
                first.Fullname = "Chọn nhân viên";
                staff.Insert(0, first);

                ddlStaff.DataTextField = "Fullname";
                ddlStaff.DataValueField = "Id";
                ddlStaff.DataSource = staff;
                ddlStaff.DataBind();
                //ddlStaff.SelectedIndex = 0;
            }
        }

        protected void bindStaffByDepartment(object sender, EventArgs e)
        {
            bindStaff();
        }

        /// <summary>
        /// event click to load data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void _BtnClick(object sender, EventArgs e)
        {
            Bind_Paging();
            bindSalary();
            ScriptManager.RegisterStartupScript(this, GetType(), "Script", "SetActiveTableListing();ReplaceZezoValue();", true);
            RemoveLoading();
        }

        /// <summary>
        /// bind paging
        /// </summary>
        protected void Bind_Paging()
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? (!HDF_Page.Value.Equals("") ? Convert.ToInt32(HDF_Page.Value) : 1) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage() : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        /// <summary>
        /// get total page (paging)
        /// </summary>
        /// <returns></returns>
        protected int Get_TotalPage()
        {
            this.departmentId = Convert.ToInt32(ddlDepartment.SelectedValue);
            int staffId = Convert.ToInt32(ddlStaff.SelectedValue);
            int SalonId = Convert.ToInt32(Salon.SelectedValue);
            int active = Convert.ToInt32(Active.SelectedValue);
            var timeFrom = new DateTime();
            var timeTo = new DateTime();
            if (TxtDateTimeFrom.Text != "")
            {
                timeFrom = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                if (TxtDateTimeTo.Text != "")
                {
                    if (TxtDateTimeFrom.Text == TxtDateTimeTo.Text)
                    {
                        timeTo = timeFrom;
                    }
                    else
                    {
                        timeTo = Convert.ToDateTime(TxtDateTimeTo.Text, culture);
                    }
                }
                else
                {
                    timeTo = timeFrom;
                }
            }
            object[] paras =
            {
                new SqlParameter("@timeFrom", string.Format("{0:yyyy/MM/dd}", timeFrom)),
                new SqlParameter("@timeTo", string.Format("{0:yyyy/MM/dd}", timeTo)),
                new SqlParameter("@salonId", SalonId),
                new SqlParameter("@staffType", this.departmentId),
                new SqlParameter("@staffId", staffId),
                new SqlParameter("@active", active)
            };
            using (var db = new Solution_30shineEntities())
            {
                int Count = 0;

                Count = db.Database.SqlQuery<cls_staff>("sp_BC_SalaryReport_V3 @timeFrom , @timeTo ,  @salonId , @staffType , @staffId , @active ", paras).Count();

                int TotalRow = Count - PAGING._TopNewsNum;
                int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
                return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
            }
        }

        /// <summary>
        /// export excel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Exc_ExportExcel(object sender, EventArgs e)
        {
            using (var db = new Solution_30shineEntities())
            {
                var _ExcelHeadRow = new List<string>();
                var serializer = new JavaScriptSerializer();
                var BillRowLST = new List<List<string>>();
                var BillRow = new List<string>();

                _ExcelHeadRow.Add("ID");
                _ExcelHeadRow.Add("Tên nhân viên");
                _ExcelHeadRow.Add("Tên CMT");
                _ExcelHeadRow.Add("Số CMT");
                _ExcelHeadRow.Add("Bộ phận");
                _ExcelHeadRow.Add("Bậc kỹ năng");
                //_ExcelHeadRow.Add("Tổng công tháng " + ThisMon.ToString());
                _ExcelHeadRow.Add("Chấm công");
                _ExcelHeadRow.Add("Lương cơ bản");
                _ExcelHeadRow.Add("Lương cứng");
                _ExcelHeadRow.Add("Lương dịch vụ");
                _ExcelHeadRow.Add("Lương sản phẩm");
                _ExcelHeadRow.Add("Lương part-time");
                _ExcelHeadRow.Add("Xử phạt");
                _ExcelHeadRow.Add("Bù lương");
                _ExcelHeadRow.Add("Bảo hiểm");
                _ExcelHeadRow.Add("Trừ khác");
                _ExcelHeadRow.Add("Tạm ứng");
                _ExcelHeadRow.Add("Tổng lương");
                _ExcelHeadRow.Add("Tên TK Ngân hàng");
                _ExcelHeadRow.Add("Số TK Ngân hàng");
                _ExcelHeadRow.Add("Số BHXH");
                _ExcelHeadRow.Add("Mã số thuế TNCN");


                var data = getData();
                if (data.Count > 0)
                {
                    foreach (var v in data)
                    {
                        BillRow = new List<string>();
                        BillRow.Add(v.Id.ToString());
                        BillRow.Add(v.Fullname);
                        BillRow.Add(v.NameCMT);
                        BillRow.Add(v.StaffID);
                        BillRow.Add(v.departmentName);
                        BillRow.Add(v.skillLevelName);
                        BillRow.Add(v.workDay.ToString());
                        BillRow.Add(v.baseSalary.ToString());
                        BillRow.Add(Math.Floor(v.fixSalary).ToString());
                        BillRow.Add(Math.Floor(v.serviceSalary).ToString());
                        BillRow.Add(Math.Floor(v.productSalary).ToString());
                        BillRow.Add(Math.Floor(v.partTimeSalary).ToString());
                        BillRow.Add(v.mistake.ToString());
                        BillRow.Add(v.MistakeExtra.ToString());
                        BillRow.Add(v.MistakeInsurrance.ToString());
                        BillRow.Add(v.MistakeOrther.ToString());
                        BillRow.Add(v.MistakeTemporary.ToString());
                        BillRow.Add((Math.Floor(v.fixSalary) + Math.Floor(v.serviceSalary) + Math.Floor(v.productSalary) + Math.Floor(v.partTimeSalary) + v.MistakeExtra - v.MistakeInsurrance - v.MistakeOrther - v.MistakeTemporary - v.mistake).ToString());
                        BillRow.Add(v.NganHang_TenTK);
                        BillRow.Add(v.NganHang_SoTK);
                        BillRow.Add(v.NumberInsurrance);
                        BillRow.Add(v.MST);
                        BillRowLST.Add(BillRow);
                    }
                }

                // export
                var ExcelStorePath = Server.MapPath("~") + "Public/Excel/Nhan.Vien/";
                var FileName = "Hoa_Don_" + string.Format("{0:dd_MM_yyyy_hh_mm_ss}", DateTime.Now) + ".xlsx";
                var UrlFileName = "http://" + Request.Url.Host + "/Public/Excel/Nhan.Vien/" + FileName;

                ExportXcel(_ExcelHeadRow, BillRowLST, ExcelStorePath + FileName);
                Bind_Paging();
                ScriptManager.RegisterStartupScript(BtnFakeExcel, this.GetType(), "Download Excel", "CallBackExportExcel('" + UrlFileName + "');UP_FormatPrice('.be-report-price'); SubTableFixHeight();SetActiveTableListing();", true);
            }
        }

        private static void ExportXcel(List<string> _ExcelHeadRow, List<List<string>> List, string Path)
        {
            try
            {
                CreateExcelFile.ExcelHeadRow = _ExcelHeadRow;
                CreateExcelFile.CreateExcel(List, Path);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi. Không tạo được file excel.\r\nException: " + ex.Message);
            }
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        
    }

    public struct Staff_TimeKeeping
    {
        public int Id { get; set; }
        public string Fullname { get; set; }
        public string TypeName { get; set; }
        public string SkillLevelName { get; set; }
        public int IdShowroom { get; set; }
        public List<int> ServiceIds { get; set; }
        public List<ItemStatistic> _ServiceStatistic { get; set; }
        public List<ItemStatistic> _ProductStatistic { get; set; }
        public int TotalService { get; set; }
        public string StrDataService { get; set; }
        public string StrTotalService { get; set; }
        public int TotalPromotion { get; set; }
        /// <summary>
        /// Tổng doanh thu
        /// </summary>
        public int TotalMoney { get; set; }
        /// <summary>
        /// Lương dịch vụ
        /// </summary>
        public int Salary_Service { get; set; }
        /// <summary>
        /// Lương sản phẩm
        /// </summary>
        public int Salary_Product { get; set; }
        /// <summary>
        /// Lương part-time
        /// </summary>
        public int Salary_PartTime { get; set; }
        /// <summary>
        /// Lương phụ cấp
        /// </summary>
        public int Salary_Allowance { get; set; }
        /// <summary>
        /// Lương cơ bản (lương tháng, lương ngày)
        /// </summary>
        public int Salary_Base { get; set; }
        /// <summary>
        /// Lương cứng (lương tính theo ngày công)
        /// </summary>
        public int Salary_Fix { get; set; }
        /// <summary>
        /// Tổng lương
        /// </summary>
        public int Salary_Total { get; set; }
        /// <summary>
        /// Số ngày công
        /// </summary>
        public int WorkDay { get; set; }
        /// <summary>
        /// Số giờ làm việc part-time, làm tăng ca
        /// </summary>
        public int WorkHour { get; set; }
        /// <summary>
        /// Doanh thu dịch vụ
        /// </summary>
        public int SalesService { get; set; }
        /// <summary>
        /// Doanh thu sản phẩm
        /// </summary>
        public int SalesProduct { get; set; }
        /// <summary>
        /// Chỉ số l theo skinner
        /// </summary>
        public int IndexLBySkinner { get; set; }
        /// <summary>
        /// Chỉ số m theo skinner
        /// </summary>
        public int IndexMBySkinner { get; set; }
    }

    public struct ServiceStatistic
    {
        public int ServiceId { get; set; }
        public int ServiceName { get; set; }
        public int ServiceTimes { get; set; }
        public int ServiceMoney { get; set; }
    }

    public struct ProductStatistic
    {
        public int ProductId { get; set; }
        public int ProductName { get; set; }
        public int ProductTimes { get; set; }
        public int ProductMoney { get; set; }
    }

    public struct ItemStatistic
    {
        public int ItemId { get; set; }
        public string ItemName { get; set; }
        public int ItemTimes { get; set; }
        public int ItemMoney { get; set; }
    }

    public struct BillServiceJoin
    {
        public int Id { get; set; }
        public string Fullname { get; set; }
        public int Type { get; set; }
        public int IdShowroom { get; set; }
        public int BillIsDelete { get; set; }
        public int BillId { get; set; }
        public DateTime BillCreatedDate { get; set; }
    }

    public class cls_FlowService : FlowService
    {
        public int SkinnerId { get; set; }
        public int SkinnerPayon { get; set; }
    }

    public class cls_flowproduct : FlowProduct
    {
        public double ForSalary { get; set; }
    }

    public class cls_department : Staff_Type
    {
        /// <summary>
        /// Tổng lương theo từng bộ phận (không tính lương mỹ phẩm)
        /// </summary>
        public double totalSalaryNoProduct { get; set; }
        public double totalSalary { get; set; }
    }

    public class cls_staff : Staff
    {
        /// <summary>
        /// Tên bộ phận
        /// </summary>
        public string departmentName { get; set; }
        /// <summary>
        /// Tên bậc kỹ năng
        /// </summary>
        public string skillLevelName { get; set; }
        /// <summary>
        /// Chấm công
        /// </summary>
        public int workDay { get; set; }
        /// <summary>
        /// Lương làm thêm, part-time
        /// </summary>
        public double partTimeSalary { get; set; }
        /// <summary>
        /// Lương phụ cấp
        /// </summary>
        public double allowanceSalary { get; set; }
        /// <summary>
        /// Lương mỹ phẩm
        /// </summary>
        public double productSalary { get; set; }
        /// <summary>
        /// Lương dịch vụ
        /// </summary>
        public double serviceSalary { get; set; }
        /// <summary>
        /// Lương cứng (tính theo ngày công)
        /// </summary>
        public double fixSalary { get; set; }
        /// <summary>
        /// Lương doanh số 
        /// </summary>
        public double salesSalary { get; set; }
        /// <summary>
        /// Lương 30shine care
        /// </summary>
        public double careSalary { get; set; }
        /// <summary>
        /// Lương cứng cơ bản theo tháng
        /// </summary>
        public int baseSalary { get; set; }
        /// <summary>
        /// Ngày công trong tháng (= ngày của tháng - 2)
        /// </summary>
        public int workDayInMonth { get; set; }
        /// <summary>
        /// Phạt lỗi xử phạt
        /// </summary>
        public int mistake { get; set; }
        /// <summary>
        /// Bù lương
        /// </summary>
        public int MistakeExtra { get; set; }
        /// <summary>
        /// Ứng lương
        /// </summary>
        public int MistakeTemporary { get; set; }
        /// <summary>
        /// Phạt lỗi khác
        /// </summary>
        public int MistakeOrther { get; set; }
        /// <summary>
        /// Bảo hiểm
        /// </summary>
        public int MistakeInsurrance { get; set; }

    }

    public class cls_reportTotal
    {
        /// <summary>
        /// Bộ phận ID
        /// </summary>
        public int departmentId { get; set; }
        /// <summary>
        /// Tên bộ phận
        /// </summary>
        public string departmentName { get; set; }
        /// <summary>
        /// Tổng lương (ko tính lương sản phẩm)
        /// </summary>
        public double salaryNoProduct { get; set; }
        /// <summary>
        /// Tổng lương
        /// </summary>
        public double salary { get; set; }
    }
}