﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Globalization;
using LinqKit;
using System.Linq.Expressions;
using ExportToExcel;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.Salary
{
    public partial class ThuNghiemLuongCheckIn : System.Web.UI.Page
    {
        protected Paging PAGING = new Paging();
        private string PageID = "";
        protected bool Staff_All = false;
        protected bool Staff_Hairdresser = false;
        protected bool Staff_HairMassage = false;
        protected bool Staff_Singer = false;
        protected string THead = "";
        public CultureInfo culture = new CultureInfo("vi-VN");
        public string timeFrom;
        public string timeTo;
        private Expression<Func<Staff, bool>> WhereStaff = PredicateBuilder.True<Staff>();
        private List<Service> _Service = new List<Service>();
        private List<Product> _Product = new List<Product>();
        protected string FirstDayOfMonth = string.Format("{0:dd/MM/yyyy}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1));
        protected string Today = String.Format("{0:dd/MM/yyyy}", DateTime.Now);
        protected int ThisMon;// = DateTime.Now.Month;
        protected int DayOfMon;// = DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month);
        protected int ConfigWorkDay = 0;

        private bool Perm_ViewAllData = false;
        private bool Perm_Access = false;
        protected bool isRoot = false;

        protected int totalPoint = 0;
        protected int totalMoney = 0;
        protected int unitMoneyBookGood = 3; // Đơn vị tiền cho điểm khách book chờ ngắn
        protected int unitMoneyBookBad = 1; // Đơn vị tiền cho điểm khách book chờ dài
        protected int unitMoneyNormalGood = 0; // Đơn vị tiền cho điểm khách thường chờ ngắn
        protected int unitMoneyNormalBad = 1; // Đơn vị tiền cho điểm khách thường chờ dài
      
        protected Store_Temp_CheckinSalary_Result dataCheckin;
        protected void checkPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //string perName = Session["User_Permission"].ToString();
                //string url = Request.Url.AbsolutePath.ToString();
                //Solution_30shineEntities db = new Solution_30shineEntities();
                //var pem = (from m in db.Tbl_Permission_Map
                //           join mn in db.Tbl_Permission_Menu on m.mID equals mn.mID
                //           where m.pName == perName && mn.Url_Main.EndsWith(url) || mn.Url_Rewrite.EndsWith(url)
                //           select new { m.aID }
                //           ).FirstOrDefault();
                //if (pem != null)
                //{
                //    var pemArray = pem.aID.Split(',');
                //    if (Array.IndexOf(pemArray, "1") > -1)
                //    {
                //        Perm_Access = true;
                //        if (Array.IndexOf(pemArray, "2") > -1)
                //        {
                //            Perm_ViewAllData = true;
                //        }

                //    }
                //    else
                //    {
                //        ContentWrap.Visible = false;
                //        NotAllowAccess.Visible = true;
                //    }

                //}
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();

            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            checkPermission();

            if (!IsPostBack)
            {
                TxtDateTimeFrom.Text = FirstDayOfMonth;
                TxtDateTimeTo.Text = Today;
                Library.Function.bindSalon(new List<DropDownList> { Salon }, Perm_ViewAllData);
                bindDepartment();
                bindStaff();
                bindData();
            }
            else
            {
                //
            }
            RemoveLoading();
        }

        private Store_Temp_CheckinSalary_Result getData()
        {
            var data = new Store_Temp_CheckinSalary_Result();
            using (var db = new Solution_30shineEntities())
            {
                int staffId = Convert.ToInt32(ddlStaff.SelectedValue);
                var timeFrom = new DateTime();
                var timeTo = new DateTime();

                if (TxtDateTimeTo.Text != "")
                {
                    timeFrom = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                    if (TxtDateTimeTo.Text != "")
                    {
                        timeTo = Convert.ToDateTime(TxtDateTimeTo.Text, culture).AddDays(1);
                    }
                    else
                    {
                        timeTo = timeFrom.AddDays(1);
                    }

                    data = db.Store_Temp_CheckinSalary(String.Format("{0:yyyy/MM/dd}", timeFrom), String.Format("{0:yyyy/MM/dd}", timeTo), staffId, 30, 15, 30).FirstOrDefault();                    
                }
                return data;
            }
        }

        public void bindData()
        {
            dataCheckin = getData();
            if (dataCheckin != null)
            {
                dataCheckin.totalMoney = dataCheckin.totalBookPointGood.Value * unitMoneyBookGood + dataCheckin.totalBookPointBad.Value * unitMoneyBookBad +
                                 dataCheckin.totalNormalPointGood.Value * unitMoneyNormalGood + dataCheckin.totalNormalPointBad.Value * unitMoneyBookBad;
                tableWrap.Visible = true;
            }            
        }

        private void bindDepartment()
        {
            using (var db = new Solution_30shineEntities())
            {
                var department = db.Staff_Type.Where(w => w.IsDelete != 1 && w.Publish == true).ToList();
                var first = new Staff_Type();
                first.Id = 0;
                first.Name = "Chọn bộ phận";
                department.Insert(0, first);

                ddlDepartment.DataTextField = "Name";
                ddlDepartment.DataValueField = "Id";
                ddlDepartment.DataSource = department;
                ddlDepartment.DataBind();
                ddlDepartment.SelectedIndex = 0;
            }
        }

        private void bindStaff()
        {
            using (var db = new Solution_30shineEntities())
            {
                var department = 5;
                var salon = Convert.ToInt32(Salon.SelectedValue);
                var staff = new List<Staff>();
                var whereSalon = "";
                var whereDepartment = "";
                var sql = "";
                if (department > 0)
                {
                    whereDepartment = " and [Type] = " + department;
                }
                if (salon > 0)
                {
                    whereSalon = " and SalonId = " + salon;
                }
                sql = @"select * from Staff 
                        where IsDelete != 1 and Active = 1 
                        and (isAccountLogin != 1 or isAccountLogin is null)" + whereSalon + whereDepartment;
                staff = db.Staffs.SqlQuery(sql).ToList();

                var first = new Staff();
                first.Id = 0;
                first.Fullname = "Chọn nhân viên";
                staff.Insert(0, first);

                ddlStaff.DataTextField = "Fullname";
                ddlStaff.DataValueField = "Id";
                ddlStaff.DataSource = staff;
                ddlStaff.DataBind();
                //ddlStaff.SelectedIndex = 0;
            }
        }

        protected void bindStaffByDepartment(object sender, EventArgs e)
        {
            bindStaff();
        }

        /// <summary>
        /// event click to load data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void _BtnClick(object sender, EventArgs e)
        {
            bindData();
            ScriptManager.RegisterStartupScript(this, GetType(), "Script", "SetActiveTableListing();", true);
            RemoveLoading();
        }        

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        /// <summary>
        /// Check permission
        /// </summary>
        private void CheckPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                string[] Allow = new string[] { "root", "admin", "accountant", "salonmanager", "reception", "checkin" };
                string[] AllowViewAllDate = new string[] { "root", "admin", "accountant"};
                string[] Root = new string[] { "root" };
                var Permission = Session["User_Permission"].ToString().Trim();
                if (Array.IndexOf(Allow, Permission) != -1)
                {
                    Perm_Access = true;
                }

                if (Array.IndexOf(AllowViewAllDate, Permission) != -1)
                {
                    Perm_ViewAllData = true;
                }
                if (Array.IndexOf(Root, Permission) != -1)
                {
                    isRoot = true;
                }
            }

            // Call execute function
            ExecuteByPermission();
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
    }
    
    public class cls_staff : Staff
    {
        /// <summary>
        /// Tên bộ phận
        /// </summary>
        public string departmentName { get; set; }
        /// <summary>
        /// Tên bậc kỹ năng
        /// </summary>
        public string skillLevelName { get; set; }
        /// <summary>
        /// Chấm công
        /// </summary>
        public int workDay { get; set; }
        /// <summary>
        /// Lương làm thêm, part-time
        /// </summary>
        public double partTimeSalary { get; set; }
        /// <summary>
        /// Lương phụ cấp
        /// </summary>
        public double allowanceSalary { get; set; }
        /// <summary>
        /// Lương mỹ phẩm
        /// </summary>
        public double productSalary { get; set; }
        /// <summary>
        /// Lương dịch vụ
        /// </summary>
        public double serviceSalary { get; set; }
        /// <summary>
        /// Lương cứng (tính theo ngày công)
        /// </summary>
        public double fixSalary { get; set; }
        /// <summary>
        /// Lương cứng cơ bản theo tháng
        /// </summary>
        public int baseSalary { get; set; }
        /// <summary>
        /// Ngày công trong tháng (= ngày của tháng - 2)
        /// </summary>
        public int workDayInMonth { get; set; }
        /// <summary>
        /// Thưởng / Phạt
        /// </summary>
        public int mistake { get; set; }
    }

    public class cls_reportTotal
    {
        /// <summary>
        /// Bộ phận ID
        /// </summary>
        public int departmentId { get; set; }
        /// <summary>
        /// Tên bộ phận
        /// </summary>
        public string departmentName { get; set; }
        /// <summary>
        /// Tổng lương (ko tính lương sản phẩm)
        /// </summary>
        public double salaryNoProduct { get; set; }
        /// <summary>
        /// Tổng lương
        /// </summary>
        public double salary { get; set; }
    }
}