﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Salary_Config_Add.aspx.cs" Inherits="_30shine.GUI.BackEnd.Salary.Salary_Config_Add" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <style>
            #success-alert { top: 100px; right: 10px; position: fixed; width: 20% !important; z-index: 2000; }
        </style>
        <div class="alert alert-danger" id="success-alert">
            <span id="msg-alert"></span>
        </div>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý cấu hình lương chung</li>
                        <li class="li-listing"><a href="/admin/cau-hinh-luong/bo-phan/danh-sach.html">Danh sách</a></li>
                        <li class="li-add active"><a href="/admin/cau-hinh-luong/bo-phan/them-moi.html">Thêm mới</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="wp customer-add">
            <%-- Add --%>
            <div class="wp960 content-wp">
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <div class="table-wp" style="">
                    <table class="table-add admin-product-table-add">
                        <tbody>

                            <tr class="title-head">
                                <td><strong>Thông tin cấu hình lương chung cho hệ thống</strong></td>
                                <td></td>
                            </tr>

                            <tr class="tr-upload">
                                <td class="col-xs-3 left">Bộ phận/ Level<strong style="color: red"> &nbsp*</strong></td>
                                <td class="col-xs-9 right">
                                    <div class="field-wp">
                                        <div class="div_col">
                                            <asp:DropDownList ID="ddlDepartment" runat="server" ClientIDMode="Static" Width="49%"></asp:DropDownList>
                                        </div>
                                        <div class="div_col">
                                            <asp:DropDownList ID="ddlLevel" runat="server" ClientIDMode="Static" Width="49%" Style="margin-left: 10px"></asp:DropDownList>

                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr class="tr-upload">
                                <td class="col-xs-3 left">Hình thức<strong style="color: red"> &nbsp*</strong></td>
                                <td class="col-xs-9 right">
                                    <div class="field-wp">
                                      <div class="div_col">
                                            <asp:DropDownList ID="ddlHinhThuc" runat="server" ClientIDMode="Static" Width="99.5%">
                                                <asp:ListItem Text="Fulltime" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="Parttime" Value="2"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr class="tr-upload">
                                <td class="col-xs-3 left">Lương cứng/ Lương 5C<strong style="color: red"> &nbsp*</strong></td>
                                <td class="col-xs-9 right">
                                    <div class="field-wp">
                                        <div class="div_col">
                                            <input type="text" id="txtFixedSalary" placeholder="Nhập hệ số lương cứng" style="width: 49%" onkeypress="return ValidateKeypress(/\d/,event);" onkeyup="reformatText(this)" />
                                        </div>
                                         <div class="div_col">
                                            <input type="text" id="txtBehaveSalary" placeholder="Nhập hệ số lương 5c" style="margin-left: 10px; width: 49%" onkeypress="return ValidateKeypress(/\d/,event);" onkeyup="reformatText(this)" />
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr class="tr-upload">
                                <td class="col-xs-3 left">Tiền trợ cấp/ Tăng ca<strong style="color: red"> &nbsp*</strong></td>
                                <td class="col-xs-9 right">
                                    <div class="field-wp">
                                        <div class="div_col">
                                            <input type="text" id="txtAllowance" placeholder="Nhập tiền trợ cấp (Vd: 0)" onkeypress="return ValidateKeypress(/\d/,event);" onkeyup="reformatText(this)" style="width: 49%" />
                                        </div>
                                        <div class="div_col">
                                            <input type="text" id="txtOverTime" placeholder="Nhập tiền tăng ca (Vd: 0)" style="margin-left: 10px; width: 49%" onkeypress="return ValidateKeypress(/\d/,event);" onkeyup="reformatText(this)" />
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr class="tr-upload">
                                <td class="col-xs-3 left">Hệ số Ratting/Tiền % bán SP<strong style="color: red"> &nbsp*</strong></td>
                                <td class="col-xs-9 right">
                                    <div class="field-wp">
                                        <div class="div_col">
                                            <input type="text" id="txtRatting" placeholder="Hệ số đánh giá (Vd: 4.500)" onkeypress="return ValidateKeypress(/\d/,event);" onkeyup="reformatText(this)" style="width: 49%" />
                                        </div>
                                        <div class="div_col">
                                            <input type="text" id="txtProductBonus" placeholder="Tiền % hoa hồng bán sản phẩm (Vd: 0)" style="margin-left: 10px; width: 49%" onkeypress="return ValidateKeypress(/\d/,event);" onkeyup="reformatText(this)" />
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            <tr class="tr-send">
                                <td class="col-xs-2 left"></td>
                                <td class="col-xs-9 right no-border">
                                    <span id="Hoantat" class="field-wp">
                                        <input type="button" id="btnAdd" class="btn-send" onclick="AddOrUpdate();" value="Hoàn tất" />
                                    </span>
                                    <span id="Huy" class="field-wp" style="margin-left: 10px">
                                        <input type="button" id="btnCancel" class="btn-send" onclick="AddCancel();" value="Hủy" />
                                    </span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <%-- end Add --%>
        </div>
        <script>
            //insert or update config_salary 
            //author: QuynhDD

            $("#success-alert").hide();
            jQuery(document).ready(function () {
                $("#glbS4M").addClass("active");
                $("#glbS4MClass").addClass("active");
                if (Id > 0) {
                    BindOBJ();
                }
            });

            var Id = (<%= Convert.ToInt32(Request.QueryString["Code"]) >0 ? Convert.ToInt32(Request.QueryString["Code"]) : 0%>);
            var kt, error, department, level, hinhthuc, fixedSalary, allowace, overtime, ratting, productBonus;
            var URL_API_FINANCIAL = "<%= Libraries.AppConstants.URL_API_FINANCIAL %>";
            //Bind OBJ
            function BindOBJ() {
                //debugger;
                $.ajax({
                    type: "GET",
                    contentType: "application/json;charset:UTF-8",
                    dataType: "JSON",
                    url: URL_API_FINANCIAL + "/api/salary-config/get/" + Id + "",
                    success: function (objData) {
                        var data = new Object();
                        data = objData.data;
                        console.log(data);
                        $("#ddlLevel").val(data.levelId);
                        $("#ddlHinhThuc").val(data.typeWork);
                        $("#ddlDepartment").val(data.staffType);
                        $("#txtFixedSalary").val(data.fixedSalary);
                        $("#txtAllowance").val(data.allowanceSalary);
                        $("#txtOverTime").val(data.overtimeSalary);
                        $("#txtRatting").val(data.rattingSalary);
                        $("#txtProductBonus").val(data.productBonus);
                        $("#txtBehaveSalary").val(data.behaveSalary);
                    }
                });
            }
            function AddOrUpdate(This) {
                //debugger;
                kt = true;
                error = "Vui lòng kiểm tra lại thông tin!";
                department = $("#ddlDepartment :selected").val();
                level = $("#ddlLevel :selected").val();
                hinhthuc = $("#ddlHinhThuc :selected").val();
                fixedSalary = $("#txtFixedSalary").val().replace(",", "");
                allowace = $("#txtAllowance").val().replace(",", "");
                overtime = $("#txtOverTime").val().replace(",", "");
                ratting = $("#txtRatting").val().replace(",", "");
                productBonus = $("#txtProductBonus").val().replace(",", "");
                behaveSalary = $("#txtBehaveSalary").val().replace(",", "");
                if (department == "" || department == 0) {
                    kt = false;
                    error += "[Chọn bộ phận] ";
                    $("#ddlDepartment").css("border-color", "red");
                    $("#success-alert").show();
                }
                if (level == "" || level == 0) {
                    kt = false;
                    error += "[Chọn level] ";
                    $("#ddlLevel").css("border-color", "red");
                    $("#success-alert").show();
                }
                if (fixedSalary == "") {
                    kt = false;
                    error += "[Nhập hệ số lương cứng] ";
                    $("#txtFixedSalary").css("border-color", "red");
                    $("#success-alert").show();
                }
                if (behaveSalary == "") {
                    kt = false;
                    error += "[Nhập hệ số lương 5C] ";
                    $("#txtBehaveSalary").css("border-color", "red");
                    $("#success-alert").show();
                }
                if (allowace == "") {
                    kt = false;
                    error += "[Nhập tiền trợ cấp] ";
                    $("#txtAllowance").css("border-color", "red");
                    $("#success-alert").show();
                }
                if (overtime == "") {
                    kt = false;
                    error += "[Nhập tiền trợ cấp] ";
                    $("#txtOverTime").css("border-color", "red");
                    $("#success-alert").show();
                }
                if (ratting == "") {
                    kt = false;
                    error += "[Nhập tiền trợ cấp] ";
                    $("#txtRatting").css("border-color", "red");
                    $("#success-alert").show();
                }
                if (productBonus == "") {
                    kt = false;
                    error += "[Nhập tiền trợ cấp] ";
                    $("#txtProductBonus").css("border-color", "red");
                    $("#success-alert").show();
                }
                if (!kt) {
                    $("#success-alert").fadeTo(2000, 2000).slideUp(2000, function () {
                        $("#success-alert").slideUp(2000);
                    });
                    $("#msg-alert").text(error);
                }

                else {
                    if (Id == 0 || Id == null) {
                        Add();
                    }
                    else {
                        Update(Id);
                    }
                }

                return kt;
            }

            //Add
            function Add() {
                addLoading();
                var datajson = {
                    TypeWork: hinhthuc,
                    DepartmentId: department,
                    LevelId: level,
                    FixedSalary: fixedSalary,
                    AllowanceSalary: allowace,
                    OvertimeSalary: overtime,
                    RattingSalary: ratting,
                    ProductBonus: productBonus,
                    BehaveSalary: behaveSalary
                };
                $.ajax({
                    url: URL_API_FINANCIAL + "/api/salary-config/insert",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(datajson),
                    dataType: "json",
                    success: function (response) {
                        console.log(response.status);
                        if (response.status == 1) {
                            $("#success-alert").fadeTo(2000, 2000).slideUp(2000, function () {
                                $("#success-alert").slideUp(2000);
                            });
                            $("#msg-alert").text("Cấu hình lương thành công!");
                            setTimeout(function () {
                                window.location.href = "/admin/cau-hinh-luong/bo-phan/danh-sach.html";
                            }, 1500);
                        }

                        else {
                            $("#success-alert").fadeTo(2000, 2000).slideUp(2000, function () {
                                $("#success-alert").slideUp(2000);
                            });
                            $("#msg-alert").text("Có lỗi xảy ra!Vui lòng liên hệ với nhóm phát triển!");
                        }
                    },
                });
            }

            //update
            function Update(salaryConfigId) {
                addLoading();
                var datajson = {
                    Id: salaryConfigId,
                    TypeWork: hinhthuc,
                    DepartmentId: department,
                    LevelId: level,
                    FixedSalary: fixedSalary,
                    AllowanceSalary: allowace,
                    OvertimeSalary: overtime,
                    RattingSalary: ratting,
                    ProductBonus: productBonus,
                    BehaveSalary: behaveSalary
                };
                $.ajax({
                    url: URL_API_FINANCIAL + "/api/salary-config/update",
                    type: "PUT",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(datajson),
                    dataType: "json",
                    success: function (response) {
                        console.log(response.status);
                        if (response.status == 1) {
                            $("#success-alert").fadeTo(2000, 2000).slideUp(2000, function () {
                                $("#success-alert").slideUp(2000);
                            });
                            $("#msg-alert").text("Cập nhật cấu hình lương thành công!");
                            setTimeout(function () {
                                window.location.href = "/admin/cau-hinh-luong/bo-phan/danh-sach.html";
                            }, 1500);
                        }

                        else {
                            $("#success-alert").fadeTo(2000, 2000).slideUp(2000, function () {
                                $("#success-alert").slideUp(2000);
                            });
                            $("#msg-alert").text("Có lỗi xảy ra!Vui lòng liên hệ với nhóm phát triển!");
                        }
                    },
                });
            }

            ///Input is only number
            function ValidateKeypress(numcheck, e) {
                var keynum, keychar, numcheck;
                if (window.event) {
                    keynum = e.keyCode;
                }
                else if (e.which) {
                    keynum = e.which;
                }
                if (keynum == 8 || keynum == 127 || keynum == null || keynum == 9 || keynum == 0 || keynum == 13) return true;
                keychar = String.fromCharCode(keynum);
                var result = numcheck.test(keychar);
                return result;
            }

            //Format number 000.000
            String.prototype.reverse = function () {
                return this.split("").reverse().join("");
            }
            function reformatText(input) {
                var x = input.value;
                x = x.replace(/,/g, "");
                x = x.reverse();
                x = x.replace(/.../g, function (e) {
                    return e + ",";
                });
                x = x.reverse();
                x = x.replace(/^,/, "");
                input.value = x;
            }

            //format datetime in javascript (2017-12-01 15:20:12)
            function js_yyyy_mm_dd_hh_mm_ss() {
                now = new Date();
                year = "" + now.getFullYear();
                month = "" + (now.getMonth() + 1); if (month.length == 1) { month = "0" + month; }
                day = "" + now.getDate(); if (day.length == 1) { day = "0" + day; }
                hour = "" + now.getHours(); if (hour.length == 1) { hour = "0" + hour; }
                minute = "" + now.getMinutes(); if (minute.length == 1) { minute = "0" + minute; }
                second = "" + now.getSeconds(); if (second.length == 1) { second = "0" + second; }
                return year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;
            }
        </script>
    </asp:Panel>
</asp:Content>