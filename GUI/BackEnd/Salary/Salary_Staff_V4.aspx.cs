﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;

using System.Web.Services;
using System.Web.Script.Serialization;
using System.Globalization;
using LinqKit;
using System.Linq.Expressions;
using ExportToExcel;
using System.Data.SqlClient;
using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.Interface;

namespace _30shine.GUI.BackEnd.Salary
{
    public partial class Salary_Staff_V4 : System.Web.UI.Page
    {
        protected Paging PAGING = new Paging();



        private int _TypeStaff;
        private int _StaffId;

        protected bool Staff_All = false;
        protected bool Staff_Hairdresser = false;
        protected bool Staff_HairMassage = false;
        protected bool Staff_Singer = false;
        protected string THead = "";
        public CultureInfo culture = new CultureInfo("vi-VN");
        private DateTime d1 = new DateTime();
        private DateTime d2 = new DateTime();
        public string timeFrom;
        public string timeTo;
        private Expression<Func<Staff, bool>> WhereStaff = PredicateBuilder.True<Staff>();
        private List<Service> _Service = new List<Service>();
        private List<Product> _Product = new List<Product>();
        protected string FirstDayOfMonth = string.Format("{0:dd/MM/yyyy}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1));
        protected string Today = String.Format("{0:dd/MM/yyyy}", DateTime.Now);
        protected int ThisMon;// = DateTime.Now.Month;
        protected int DayOfMon;// = DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month);
        protected int ConfigWorkDay = 0;
        protected string sql = "";
        private string PageID = "BC_LUONG";
        private bool Perm_ViewAllData = false;
        private bool Perm_Access = false;
        protected bool isRoot = false;
        protected int departmentId;
        public void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                ////Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                ////Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        //private void ExecuteByPermission()
        //{

        //    if (!Perm_Access)
        //    {
        //        ContentWrap.Visible = false;
        //        NotAllowAccess.Visible = true;
        //    }
        //}
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (!IsPostBack)
            {
                TxtDateTimeFrom.Text = FirstDayOfMonth;
                TxtDateTimeTo.Text = Today;
                Library.Function.bindSalon(new List<DropDownList> { Salon }, Perm_ViewAllData);
                //Bind_Salon();
                bindDepartment();
                bindStaff();


            }
            else
            {
                //
            }
            RemoveLoading();
        }

        private void bindDepartment()
        {
            using (var db = new Solution_30shineEntities())
            {
                var department = db.Staff_Type.Where(w => w.IsDelete != 1 && w.Publish == true).ToList();
                var first = new Staff_Type();
                first.Id = 0;
                first.Name = "Chọn bộ phận";
                department.Insert(0, first);

                ddlDepartment.DataTextField = "Name";
                ddlDepartment.DataValueField = "Id";
                ddlDepartment.DataSource = department;
                ddlDepartment.DataBind();
                ddlDepartment.SelectedIndex = 0;
            }
        }

        private void bindStaff()
        {
            using (var db = new Solution_30shineEntities())
            {
                var department = Convert.ToInt32(ddlDepartment.SelectedValue);
                var salon = Convert.ToInt32(Salon.SelectedValue);
                var staff = new List<Staff>();
                var whereSalon = "";
                var whereDepartment = "";
                var sql = "";
                if (department > 0)
                {
                    whereDepartment = " and [Type] = " + department;
                }
                if (salon > 0)
                {
                    whereSalon = " and SalonId = " + salon;
                }
                sql = @"select * from Staff 
                        where IsDelete != 1 and Active = 1 
                        and (isAccountLogin != 1 or isAccountLogin is null)" + whereSalon + whereDepartment;
                staff = db.Staffs.SqlQuery(sql).ToList();

                var first = new Staff();
                first.Id = 0;
                first.Fullname = "Chọn nhân viên";
                staff.Insert(0, first);

                ddlStaff.DataTextField = "Fullname";
                ddlStaff.DataValueField = "Id";
                ddlStaff.DataSource = staff;
                ddlStaff.DataBind();
                //ddlStaff.SelectedIndex = 0;
            }
        }

        protected void bindStaffByDepartment(object sender, EventArgs e)
        {
            bindStaff();
        }

        /// <summary>
        /// event click to load data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void _BtnClick(object sender, EventArgs e)
        {
            int departmentId = Convert.ToInt32(ddlDepartment.SelectedValue);
            int staffId = Convert.ToInt32(ddlStaff.SelectedValue);
            int SalonId = Convert.ToInt32(Salon.SelectedValue);
            int active = Convert.ToInt32(Active.SelectedValue);
            DateTime timeFrom = new DateTime();
            DateTime timeTo = new DateTime();
            if (TxtDateTimeFrom.Text != "")
            {
                timeFrom = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                if (TxtDateTimeTo.Text != "")
                {
                    if (TxtDateTimeFrom.Text == "")
                    {
                        timeTo = timeFrom;
                    }
                    else
                    {
                        timeTo = Convert.ToDateTime(TxtDateTimeTo.Text, culture);
                    }
                }
                else
                {
                    timeTo = timeFrom;
                }
            }

            using (var db = new Solution_30shineEntities())
            {
                var LstTongLuongBoPhan = new List<TempTongBoPhan>();
                var item = new TempTongBoPhan();
                string where = "";
                if (active == 3)
                {
                    where = "";
                }
                else
                {
                    where = "  and s.Active = " + active + "";
                }
                sql = @"-- Tis lương theo chấm công
                declare
                @FromDate datetime,
                @ToDate datetime,
                @DepartmentId int,
                @SalonId int,
                @StaffId int
                    set @SalonId = " + SalonId + @"
                    set @FromDate ='" + timeFrom + @"'
                    set @ToDate ='" + timeTo + @"'
                    set @DepartmentId = " + departmentId + @"
                    set @SalonId = " + SalonId + @"
                    set @StaffId = " + staffId + @"
                begin
                with  tempFlowsalary as
                ( 
                select a.staffId ,
                Round ( cast(SUM(a.fixSalary) as float),0) as TongLuongCungtheongay,
                Round ( cast(SUM(a.serviceSalary) as float),0) as TongLuongDichVu, 
                Round ( cast(SUM(a.productSalary) as float),0) as TongLuongMyPham, 
                Round ( cast(SUM(a.partTimeSalary) as float),0) as TongLuongParTime ,
                Round ( cast(SUM( a.Mistake_Point) as float),0) as XuPhat ,
				Round ( cast(SUM( a.MistakeExtra) as float),0) as BuLuong ,
				Round ( cast(SUM( a.MistakeInsurrance) as float),0) as BaoHiem ,
				Round ( cast(SUM( a.MistakeTemporary) as float),0) as UngLuong ,
				Round ( cast(SUM( a.MistakeOrther) as float),0) as TruKhac ,
                  a.SalonId as salonChancong 

                  from 
                FlowSalary as a
                left join Staff as b on a.staffId = b.Id
                where 
                a.sDate between @FromDate and @ToDate
                and ((a.SalonId  = @SalonId) or (@SalonId = 0) ) and a.IsDelete = 0 
                and ((b.Type = @DepartmentId) or @DepartmentId =0 )
                and ((b.Id = @StaffId ) or (@StaffId = 0))
                group by a.staffId , a.SalonId
                ),

                --/Tinhs chaams coong/ 
                 tempChamCong as (
	                select ftk.StaffId as NhanVienID, COUNT(ftk.StaffId) as ChamCong  ,s.Type,s.SkillLevel,s.Fullname
	                 from SM_EnrollTemp as  ftk 
	                 left join Staff as s on ftk.StaffId = s.Id
	                where ftk .IsEnroll = 1 and ((ftk.SalonId = @SaLonId) or (@SaLonId = 0)) and ftk.WorkDate  BETWEEN @FromDate AND @ToDate 
	                and ((s.Type = @DepartmentId) or (@DepartmentId = 0)) and ((s.Id = @StaffId) or (@StaffId = 0) ) and s.SkillLevel > 0 " + where + @"

	                group by ftk.StaffId  ,s.Type,s.SkillLevel,s.Fullname
	                ),
	                tempdata   as (
	                select * from 
	                tempChamCong as a 
	                left join  tempFlowsalary    as b on a.NhanVienID = b.staffId
	                )
	                select 
	                t.NhanVienID, 
	                Isnull(t.ChamCong,'0') as ChamCong,
	                t.FullName ,
	                st.Name as TypeName,
	                skill.Name as SkillLerver,
		                --Luong co ban
		                ISNULL((select payon.Value from Tbl_Payon as payon where payon.TypeStaffId =t.Type and payon.PayByTime = 1 and payon.ForeignId = t.SkillLevel),0) as LuongCoBan,
		                ISNULL(t.TongLuongCungtheongay,0) as TongLuongCungtheongay  ,
		                ISNULL(t.TongLuongDichVu,0) as TongLuongDichVu ,
		                ISNULL(t.TongLuongMyPham,0) as TongLuongMyPham,
		                ISNULL (t.TongLuongParTime,0) as TongLuongParTime,
				        ISNULL (t.XuPhat,0) as XuPhat,
						ISNULL (t.BuLuong,0) as BuLuong,
						ISNULL (t.BaoHiem,0) as BaoHiem,
						ISNULL (t.UngLuong,0) as UngLuong,
						ISNULL (t.TruKhac,0) as TruKhac,
						--Tổng lương
						( ISNULL(t.TongLuongCungtheongay,0)  + ISNULL(t.TongLuongDichVu,0) + ISNULL(t.TongLuongMyPham,0) + ISNULL(t.TongLuongParTime,0) + ISNULL(t.BuLuong,0)  
							- ISNULL(t.XuPhat,0) - ISNULL(t.BaoHiem,0) - ISNULL(t.UngLuong,0)- ISNULL(t.TruKhac,0)
						) as TongLuong,
                        st.Id as StaffTypeId

	                 from tempdata as t
	                 left join Staff_Type as st on t.Type = st.Id
	                 left join Tbl_SkillLevel as  skill  on  t.SkillLevel = skill.Id
                end
                    ";
                var lst = db.Database.SqlQuery<cls_Report_Salary_Follow_Timekeeping>(sql).ToList();
                //var data = db.Report_Salary_Follow_Timekeeping(timeFrom, timeTo, SalonId, departmentId, staffId, active).ToList();
                int _Count = lst.Count;
                Bind_Paging(_Count);

                // Tinh Tong Luong Bo Phan
                var LstDepartment = db.Store_Department(departmentId).ToList();
                if (LstDepartment.Count > 0)
                {
                    for (int i = 0; i < LstDepartment.Count; i++)
                    {
                        item = new TempTongBoPhan();
                        item.Id = LstDepartment[i].Id;
                        item.NameDepartment = LstDepartment[i].Name;
                        item.TongLuongKhongMyPham = lst.Where(a => a.StaffTypeId == Convert.ToInt32(LstDepartment[i].Id)).Sum(a => a.TongLuong - a.TongLuongMyPham);
                        item.TongLuong = lst.Where(a => a.StaffTypeId == Convert.ToInt32(LstDepartment[i].Id)).Sum(a => a.TongLuong);

                        LstTongLuongBoPhan.Add(item);
                    }

                }
                rptSalary.DataSource = lst.Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                rptSalaryTotal.DataSource = LstTongLuongBoPhan;
                rptSalary.DataBind();
                rptSalaryTotal.DataBind();
            }
            ScriptManager.RegisterStartupScript(this, GetType(), "Script", "SetActiveTableListing();ReplaceZezoValue();", true);
            RemoveLoading();
        }

        /// <summary>
        /// get total page (paging)
        /// </summary>
        /// <returns></returns>
        protected int Get_TotalPage(int TotalRow)
        {
            using (var db = new Solution_30shineEntities())
            {

                int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
                return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
            }
        }
        /// <summary>
        /// bind paging
        /// </summary>
        protected void Bind_Paging(int TotalRow)
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._Segment = 10000;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(TotalRow) : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        /// <summary>
        /// Check permission
        /// </summary>
        private void CheckPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                string[] Allow = new string[] { "root", "admin", "accountant", "salonmanager" };
                string[] AllowViewAllDate = new string[] { "root", "admin", "accountant" };
                string[] Root = new string[] { "root" };
                var Permission = Session["User_Permission"].ToString().Trim();
                if (Array.IndexOf(Allow, Permission) != -1)
                {
                    Perm_Access = true;
                }

                if (Array.IndexOf(AllowViewAllDate, Permission) != -1)
                {
                    Perm_ViewAllData = true;
                }
                if (Array.IndexOf(Root, Permission) != -1)
                {
                    isRoot = true;
                }
            }

            // Call execute function
            ExecuteByPermission();
        }
        protected void Exc_ExportExcel(object sender, EventArgs e)
        {
            using (var db = new Solution_30shineEntities())
            {
                var _ExcelHeadRow = new List<string>();
                var serializer = new JavaScriptSerializer();
                var BillRowLST = new List<List<string>>();
                var BillRow = new List<string>();

                int departmentId = Convert.ToInt32(ddlDepartment.SelectedValue);
                int staffId = Convert.ToInt32(ddlStaff.SelectedValue);
                int SalonId = Convert.ToInt32(Salon.SelectedValue);
                int active = Convert.ToInt32(Active.SelectedValue);
                DateTime timeFrom = new DateTime();
                DateTime timeTo = new DateTime();
                if (TxtDateTimeFrom.Text != "")
                {
                    timeFrom = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                    if (TxtDateTimeTo.Text != "")
                    {
                        if (TxtDateTimeFrom.Text == "")
                        {
                            timeTo = timeFrom;
                        }
                        else
                        {
                            timeTo = Convert.ToDateTime(TxtDateTimeTo.Text, culture);
                        }
                    }
                    else
                    {
                        timeTo = timeFrom;
                    }
                }


                var LstTongLuongBoPhan = new List<TempTongBoPhan>();
                var item = new TempTongBoPhan();
                string where = "";
                if (active == 3)
                {
                    where = "";
                }
                else
                {
                    where = "  and s.Active = " + active + "";
                }
                sql = @"-- Tis lương theo chấm công
                declare
                @FromDate datetime,
                @ToDate datetime,
                @DepartmentId int,
                @SalonId int,
                @StaffId int
                    set @SalonId = " + SalonId + @"
                    set @FromDate ='" + timeFrom + @"'
                    set @ToDate ='" + timeTo + @"'
                    set @DepartmentId = " + departmentId + @"
                    set @SalonId = " + SalonId + @"
                    set @StaffId = " + staffId + @"
                begin
                    with  tempFlowsalary as
                    ( 
                    select a.staffId ,
                    b.NameCMT,
                    b.StaffId as NumberCMT,
				    b.NganHang_ChiNhanh,
				    b.NganHang_SoTK,
				    b.NumberInsurrance,
				    b.MST,
                    Round ( cast(SUM(a.fixSalary) as float),0) as TongLuongCungtheongay,
                    Round ( cast(SUM(a.serviceSalary) as float),0) as TongLuongDichVu, 
                    Round ( cast(SUM(a.productSalary) as float),0) as TongLuongMyPham, 
                    Round ( cast(SUM(a.partTimeSalary) as float),0) as TongLuongParTime ,
                    Round ( cast(SUM( a.Mistake_Point) as float),0) as XuPhat ,
					Round ( cast(SUM( a.MistakeExtra) as float),0) as BuLuong ,
					Round ( cast(SUM( a.MistakeInsurrance) as float),0) as BaoHiem ,
					Round ( cast(SUM( a.MistakeTemporary) as float),0) as UngLuong ,
					Round ( cast(SUM( a.MistakeOrther) as float),0) as TruKhac ,
                      a.SalonId as salonChancong 

                      from 
                    FlowSalary as a
                    left join Staff as b on a.staffId = b.Id
                    where 
                    a.sDate between @FromDate and @ToDate
                    and ((a.SalonId  = @SalonId) or (@SalonId = 0) ) and a.IsDelete = 0
                    and ((b.Type = @DepartmentId) or @DepartmentId =0 )
                    and ((b.Id = @StaffId ) or (@StaffId = 0))
                    group by a.staffId , a.SalonId,
                    b.StaffId,
                    b.NameCMT,b.NganHang_ChiNhanh,
				    b.NganHang_SoTK,
				    b.NumberInsurrance,
				    b.MST
                    ),

                    --/Tinhs chaams coong/ 
                     tempChamCong as (
	                    select ftk.StaffId as NhanVienID, COUNT(ftk.StaffId) as ChamCong  ,s.Type,s.SkillLevel,s.Fullname
	                     from SM_EnrollTemp as  ftk 
	                     left join Staff as s on ftk.StaffId = s.Id
	                    where ftk .IsEnroll = 1 and ((ftk.SalonId = @SaLonId) or (@SaLonId = 0)) and ftk.WorkDate  BETWEEN @FromDate AND @ToDate 
	                    and ((s.Type = @DepartmentId) or (@DepartmentId = 0)) and ((s.Id = @StaffId) or (@StaffId = 0) ) and s.SkillLevel > 0 " + where + @"

	                    group by ftk.StaffId  ,s.Type,s.SkillLevel,s.Fullname
	                    ),
	                    tempdata   as (
	                    select * from 
	                    tempChamCong as a 
	                    left join  tempFlowsalary    as b on a.NhanVienID = b.staffId
	                    )
	                    select 
                        t.NameCMT,
                        t.NumberCMT,
					    t.NganHang_ChiNhanh,
					    t.NganHang_SoTK,
					    t.NumberInsurrance,
					    t.MST,
	                    t.NhanVienID, 
	                    Isnull(t.ChamCong,'0') as ChamCong,
	                    t.FullName ,
	                    st.Name as TypeName,
	                    skill.Name as SkillLerver,
		                    --Luong co ban
		                    ISNULL((select payon.Value from Tbl_Payon as payon where payon.TypeStaffId =t.Type and payon.PayByTime = 1 and payon.ForeignId = t.SkillLevel),0) as LuongCoBan,
		                    ISNULL(t.TongLuongCungtheongay,0) as TongLuongCungtheongay  ,
		                    ISNULL(t.TongLuongDichVu,0) as TongLuongDichVu ,
		                    ISNULL(t.TongLuongMyPham,0) as TongLuongMyPham,
		                    ISNULL (t.TongLuongParTime,0) as TongLuongParTime,
				                    ISNULL (t.XuPhat,0) as XuPhat,
							ISNULL (t.BuLuong,0) as BuLuong,
							ISNULL (t.BaoHiem,0) as BaoHiem,
							ISNULL (t.UngLuong,0) as UngLuong,
							ISNULL (t.TruKhac,0) as TruKhac,
							--Tổng lương
							( ISNULL(t.TongLuongCungtheongay,0)  + ISNULL(t.TongLuongDichVu,0) + ISNULL(t.TongLuongMyPham,0) + ISNULL(t.TongLuongParTime,0) + ISNULL(t.BuLuong,0)  
								- ISNULL(t.XuPhat,0) - ISNULL(t.BaoHiem,0) - ISNULL(t.UngLuong,0)- ISNULL(t.TruKhac,0)
							) as TongLuong,
                            st.Id as StaffTypeId

	                     from tempdata as t
	                     left join Staff_Type as st on t.Type = st.Id
	                     left join Tbl_SkillLevel as  skill  on  t.SkillLevel = skill.Id
                end
                    ";
                var lst = db.Database.SqlQuery<cls_Report_Salary_Follow_Timekeeping>(sql).ToList();
                //var data = db.Report_Salary_Follow_Timekeeping(timeFrom, timeTo, SalonId, departmentId, staffId, active).ToList();


                _ExcelHeadRow.Add("ID");
                _ExcelHeadRow.Add("Tên nhân viên");
                _ExcelHeadRow.Add("Tên CMT");
                _ExcelHeadRow.Add("Số CMT");
                _ExcelHeadRow.Add("Bộ phận");
                _ExcelHeadRow.Add("Bậc kỹ năng");
                //_ExcelHeadRow.Add("Tổng công tháng " + ThisMon.ToString());
                _ExcelHeadRow.Add("Chấm công");
                _ExcelHeadRow.Add("Lương cơ bản");
                _ExcelHeadRow.Add("Lương cứng");
                _ExcelHeadRow.Add("Lương dịch vụ");
                _ExcelHeadRow.Add("Lương sản phẩm");
                _ExcelHeadRow.Add("Lương part-time");
                _ExcelHeadRow.Add("Bù lương");
                _ExcelHeadRow.Add("Ứng lương");
                _ExcelHeadRow.Add("Bảo hiểm");
                _ExcelHeadRow.Add("Trừ khác");
                _ExcelHeadRow.Add("Xử phạt");
                _ExcelHeadRow.Add("Tổng lương");
                _ExcelHeadRow.Add("Tên TK Ngân hàng");
                _ExcelHeadRow.Add("Số TK Ngân hàng");
                _ExcelHeadRow.Add("Số BHXH");
                _ExcelHeadRow.Add("Mã số thuế TNCN");


                if (lst.Count > 0)
                {
                    foreach (var v in lst)
                    {
                        BillRow = new List<string>();
                        BillRow.Add(v.NhanVienID.ToString());
                        BillRow.Add(v.FullName);
                        BillRow.Add(v.NameCMT);
                        BillRow.Add(v.NumberCMT);
                        BillRow.Add(v.TypeName);
                        BillRow.Add(v.SkillLerver);
                        BillRow.Add(v.ChamCong.ToString());
                        BillRow.Add(v.LuongCoBan.ToString());
                        BillRow.Add(Math.Floor(v.TongLuongCungtheongay).ToString());
                        BillRow.Add(Math.Floor(v.TongLuongDichVu).ToString());
                        BillRow.Add(Math.Floor(v.TongLuongMyPham).ToString());
                        BillRow.Add(Math.Floor(v.TongLuongParTime).ToString());
                        BillRow.Add(Convert.ToString(v.BuLuong).ToString());
                        BillRow.Add(Convert.ToString(v.UngLuong).ToString());
                        BillRow.Add(Convert.ToString(v.BaoHiem).ToString());
                        BillRow.Add(Convert.ToString(v.TruKhac).ToString());
                        BillRow.Add(Convert.ToString(v.XuPhat).ToString());
                        BillRow.Add((Math.Floor(v.TongLuongCungtheongay) + Math.Floor(v.TongLuongDichVu) + Math.Floor(v.TongLuongMyPham) + Math.Floor(v.TongLuongParTime) + Convert.ToInt32(v.LuongPhuCap)).ToString());
                        BillRow.Add(v.NganHang_TenTK);
                        BillRow.Add(v.NganHang_SoTK);
                        BillRow.Add(v.NumberInsurrance);
                        BillRow.Add(v.MST);
                        BillRowLST.Add(BillRow);
                    }


                    // export
                    var ExcelStorePath = Server.MapPath("~") + "Public/Excel/Nhan.Vien/";
                    var FileName = "Hoa_Don_" + string.Format("{0:dd_MM_yyyy_hh_mm_ss}", DateTime.Now) + ".xlsx";
                    var UrlFileName = "http://" + Request.Url.Host + "/Public/Excel/Nhan.Vien/" + FileName;

                    ExportXcel(_ExcelHeadRow, BillRowLST, ExcelStorePath + FileName);
                    ScriptManager.RegisterStartupScript(BtnFakeExcel, this.GetType(), "Download Excel", "CallBackExportExcel('" + UrlFileName + "');UP_FormatPrice('.be-report-price'); SubTableFixHeight();SetActiveTableListing();", true);
                }
            }
        }

        private static void ExportXcel(List<string> _ExcelHeadRow, List<List<string>> List, string Path)
        {
            try
            {
                CreateExcelFile.ExcelHeadRow = _ExcelHeadRow;
                CreateExcelFile.CreateExcel(List, Path);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi. Không tạo được file excel.\r\nException: " + ex.Message);
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
    }

    public class TempTongBoPhan
    {
        public int Id { get; set; }
        public string NameDepartment { get; set; }
        public double TongLuongKhongMyPham { get; set; }
        public double TongLuong { get; set; }
    }

    public class cls_Report_Salary_Follow_Timekeeping
    {
        public Nullable<int> NhanVienID { get; set; }
        public string NumberCMT { get; set; }
        public string NameCMT { get; set; }
        public int ChamCong { get; set; }
        public string FullName { get; set; }
        public string TypeName { get; set; }
        public string SkillLerver { get; set; }
        public int LuongCoBan { get; set; }
        public double TongLuongCungtheongay { get; set; }
        public double TongLuongDichVu { get; set; }
        public double TongLuongMyPham { get; set; }
        public double TongLuongParTime { get; set; }
        public int LuongPhuCap { get; set; }
        public double TongPhatLuong { get; set; }
        public double XuPhat { get; set; }
        public double UngLuong { get; set; }
        public double BuLuong { get; set; }
        public double TruKhac { get; set; }
        public double BaoHiem { get; set; }

        public double TongLuong { get; set; }
        public Nullable<int> StaffTypeId { get; set; }
        public string NganHang_TenTK { get; set; }
        public string NganHang_SoTK { get; set; }
        public string NumberInsurrance { get; set; }
        public string MST { get; set; }
    }
}