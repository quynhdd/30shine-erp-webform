﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;
using Library;
using System.Web.Services;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.MODEL.BO;
using _30shine.MODEL.IO;
using System.Text.RegularExpressions;
using System.Reflection;
using _30shine.Helpers;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;
using System.Threading.Tasks;
using _30shine.Helpers.Http;
using _30shine.MODEL.CustomClass;

namespace _30shine.GUI.BackEnd.StaffController
{
    public partial class UpdateSalaryByBillFromExcel : System.Web.UI.Page
    {
        public JavaScriptSerializer Serializer;
        public Class.AjaxResponse AjaxResponse;
        public Solution_30shineEntities db;
        IStaffTypeModel staffTypeModel = new StaffTypeModel();
        ISalonModel salonModel = new SalonModel();
        private string PageID = "UPDATE_EXCELL";
        private bool Perm_Access = false;

        public string domainERPLiveSite = "";
        public string PATH_ASYNC_CHECKOUT_BILL_LOG = @"30ShineSystemLog\erp.30shine.com\" + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + "_tool_async_checkout_bill_statistic.txt";
        public string PATH_ASYNC_CHECKOUT_BILL_LOG_EXCEPTION = @"30ShineSystemLog\erp.30shine.com\" + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + "_tool_async_checkout_bill_statistic_exception.txt";

        public UpdateSalaryByBillFromExcel()
        {
            Serializer = new JavaScriptSerializer();
            AjaxResponse = new Class.AjaxResponse();
            db = new Solution_30shineEntities();
        }

        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }


        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            domainERPLiveSite = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host;
        }

        [WebMethod]
        public static string UploadFile(List<DataStaffFromExcel> DataFromExcel)
        {
            using (var db = new Solution_30shineEntities())
            {
                var instance = Instance;
                instance.AjaxResponse.Reset();
                var domainERPLiveSite = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host;

                try
                {
                    DataFromExcel.ForEach(delegate (DataStaffFromExcel Data)
                    {
                        var bill = db.BillServices.FirstOrDefault(w => w.Id == Data.BillID);
                        if (UpdateSalaryByBillFromExcel.Instance.isWaitLongTime(bill))
                        {
                            /// Cập nhật store lương hôm nay
                            //SalaryLib.updateFlowSalaryByBill(OBJ, db, true);
                            var data = new Data_StatisticSalary
                            {
                                Bill = bill,
                                IsVoucher = true
                            };
                            try
                            {
                                var request = new Request();
                                Task taskStatisSalary = Task.Run(async () =>
                                {
                                    await request.RunPostAsync(
                                        domainERPLiveSite + "/GUI/SystemService/Webservice/wsBillService.asmx/StatisticSalary",
                                        new
                                        {
                                            data = data
                                        }
                                    );
                                });
                                taskStatisSalary.Wait(100);
                            }
                            catch (Exception ex)
                            {
                                Library.Function.WriteToFile("StatisticSalary Exception First " + DateTime.Now + ": " + Library.Function.JavaScript.Serialize(bill) + " ex " + ex.StackTrace, UpdateSalaryByBillFromExcel.Instance.PATH_ASYNC_CHECKOUT_BILL_LOG_EXCEPTION);
                            }

                            // Kiểm tra tạo vourcher khách hàng trường hợp thời gian chờ > 20 phút
                            UpdateSalaryByBillFromExcel.Instance.createVoucher(bill);
                        }
                        else
                        {
                            /// Cập nhật store lương hôm nay
                            //SalaryLib.updateFlowSalaryByBill(OBJ, db);
                            var data = new Data_StatisticSalary
                            {
                                Bill = bill,
                                IsVoucher = false
                            };
                            try
                            {
                                var request = new Request();
                                Task taskStatisSalary = Task.Run(async () =>
                                {
                                    await request.RunPostAsync(
                                        domainERPLiveSite + "/GUI/SystemService/Webservice/wsBillService.asmx/StatisticSalary",
                                        new
                                        {
                                            data = data
                                        }
                                    );
                                });
                                taskStatisSalary.Wait(100);
                            }
                            catch (Exception ex)
                            {
                                Library.Function.WriteToFile("StatisticSalary Exception First " + DateTime.Now + ": " + Library.Function.JavaScript.Serialize(bill) + " ex " + ex.StackTrace, UpdateSalaryByBillFromExcel.Instance.PATH_ASYNC_CHECKOUT_BILL_LOG_EXCEPTION);
                            }
                        }
                    });

                    instance.AjaxResponse.success = true;
                }
                catch (Exception Ex)
                {
                    Library.Function.WriteToFile("StatisticSalary Exception Upload " + DateTime.Now + ": " + " ex " + Ex.StackTrace, UpdateSalaryByBillFromExcel.Instance.PATH_ASYNC_CHECKOUT_BILL_LOG_EXCEPTION);
                    instance.AjaxResponse.success = false;
                    instance.AjaxResponse.msg = "Có lỗi xảy ra, vui lòng thử lại! {" + Ex.Message + "}";
                }

                return instance.Serializer.Serialize(instance.AjaxResponse);
            }
        }

        public bool isWaitLongTime(BillService bill)
        {
            try
            {
                var isWait = false;
                int integer;
                var configModel = new ConfigModel();
                var configWaitTime = int.TryParse(configModel.GetValueByKey("bill_voucher_waittime_minute"), out integer) ? integer : 0;

                if (bill != null && (!UpdateSalaryByBillFromExcel.Instance.isCustomerSpecial(bill.CustomerId.Value) && !UpdateSalaryByBillFromExcel.Instance.isCustomerVIP(bill.CustomerId.Value)))
                {
                    int? time = GetWaitTime(bill);
                    if (time != null && time >= configWaitTime)
                    {
                        isWait = true;
                    }
                }
                return isWait;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Tính thời gian chờ
        /// </summary>
        /// <param name="bs"></param>
        /// <returns></returns>
        public static int? GetWaitTime(BillService bs)
        {
            Solution_30shineEntities db = new Solution_30shineEntities();
            var WaitTime = 0;
            try
            {
                var book = db.Bookings.FirstOrDefault(f => f.Id == bs.BookingId);
                if (book != null && book.IsBookOnline == true)
                {
                    DateTime? pvgTime = bs.InProcedureTime;
                    DateTime? checkinTime = bs.CreatedDate;
                    DateTime bookHour = new DateTime();
                    var hour = db.BookHours.FirstOrDefault(w => w.Id == book.HourId);
                    bookHour = bs.CreatedDate.Value.Date.Add(hour.HourFrame.Value);

                    if (checkinTime >= bookHour)
                    {
                        WaitTime = (int)(pvgTime - checkinTime).Value.TotalMinutes;
                    }
                    else
                    {
                        WaitTime = (int)(pvgTime - bookHour).Value.TotalMinutes;
                    }
                }

            }
            catch (Exception ex)
            {
                //throw new Exception("Error");
                return null;
            }

            return WaitTime;
        }


        /// <summary>
        /// Check khách có phải đặc biệt hay không
        /// </summary>
        /// <param name="customerID"></param>
        /// <returns></returns>
        public bool isCustomerSpecial(int customerID)
        {
            using (var db = new Solution_30shineEntities())
            {
                try
                {
                    var ret = false;
                    var specialCustomer = new SpecialCustomer();
                    var detailSpecialCustomer = new SpecialCusDetail();
                    // Kiểm tra khách hàng đặc biệt
                    specialCustomer = db.SpecialCustomers.Where(a => a.IsDelete == false && a.CustomerTypeId == 1 && a.CustomerId == customerID).OrderBy(a => a.Id).FirstOrDefault();

                    if (specialCustomer != null)
                    {
                        detailSpecialCustomer = db.SpecialCusDetails.Where(a => a.IsDelete == false && a.QuantityInvited > 0 && a.SpecialCusId == specialCustomer.Id).OrderByDescending(a => a.Id).FirstOrDefault();
                        if (detailSpecialCustomer != null)
                        {
                            ret = true;
                        }
                    }
                    return ret;
                }
                catch (Exception ex)
                {
                    //throw new Exception(ex.Message);
                    return false;
                }
            }
        }

        /// <summary>
        /// Tạo voucher khách hàng trường hợp khách đợi quá 20 phút
        /// </summary>
        /// <param name="billID"></param>
        //[WebMethod]
        public void createVoucher(BillService bill)
        {
            try
            {
                if (bill != null)
                {
                    var configModel = new ConfigModel();
                    var payonModel = new PayonConfigModel();
                    float _float;
                    var itemVoucher = new cls_voucher_waitime_custom();
                    var today = DateTime.Now.Date;
                    Staff staff;

                    var staffModel = new StaffModel();
                    // Update cho Stylist
                    if (bill.Staff_Hairdresser_Id != null)
                    {
                        staff = staffModel.GetStaffById(bill.Staff_Hairdresser_Id.Value);
                        itemVoucher.StylistConfigSalary = payonModel.GetValueByKey(staff, "salary_coefficient_rating");
                        itemVoucher.StylistPoint = float.TryParse(configModel.GetValueByKey("bill_voucher_waittime_20p_stylist_point"), out _float) ? _float : 0;
                        this.UpdateSalaryByStaff(staff, today, itemVoucher.StylistPoint.Value, itemVoucher.StylistConfigSalary);
                    }
                    // Update cho Skinner
                    if (bill.Staff_HairMassage_Id != null)
                    {
                        staff = staffModel.GetStaffById(bill.Staff_HairMassage_Id.Value);
                        itemVoucher.SkinnerConfigSalary = payonModel.GetValueByKey(staff, "salary_coefficient_rating");
                        itemVoucher.SkinnerPoint = float.TryParse(configModel.GetValueByKey("bill_voucher_waittime_20p_skinner_point"), out _float) ? _float : 0;
                        this.UpdateSalaryByStaff(staff, today, itemVoucher.SkinnerPoint.Value, itemVoucher.SkinnerConfigSalary);
                    }
                    // Update cho Checkin
                    if (bill.ReceptionId != null)
                    {
                        staff = staffModel.GetStaffById(bill.ReceptionId.Value);
                        itemVoucher.CheckinConfigSalary = payonModel.GetValueByKey(staff, "salary_coefficient_rating");
                        itemVoucher.CheckinPoint = float.TryParse(configModel.GetValueByKey("bill_voucher_waittime_20p_checkin_point"), out _float) ? _float : 0;
                        this.UpdateSalaryByStaff(staff, today, itemVoucher.CheckinPoint.Value, itemVoucher.CheckinConfigSalary);
                    }

                    // insert bản ghi voucher
                    UpdateSalaryByBillFromExcel.Instance.vourcherWaitTimeInsert(bill, itemVoucher);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Cập nhật lại lương cho nhân viên (cập nhật điểm trừ trong trường hợp thời gian chờ lâu)
        /// </summary>
        /// <param name="staff"></param>
        /// <param name="date"></param>
        /// <param name="configPoint"></param>
        /// <param name="configSalary"></param>
        public void UpdateSalaryByStaff(Staff staff, DateTime date, double configPoint, int configSalary)
        {
            try
            {
                if (staff != null && date != null)
                {
                    var salaryModel = new SalaryModel();
                    var flowSalary = salaryModel.GetFlowSalaryByStaffDate(staff, date.Date);
                    if (flowSalary != null)
                    {
                        if (flowSalary.waitTimeMistake == null || flowSalary.waitTimeMistake == 0)
                        {
                            flowSalary.waitTimeMistake = configPoint;
                        }
                        else
                        {
                            flowSalary.waitTimeMistake = configPoint + flowSalary.waitTimeMistake;
                        }

                        flowSalary.ratingPoint = flowSalary.ratingPoint != null ? flowSalary.ratingPoint : 0;
                        flowSalary.serviceSalary = (float)configSalary * ((float)flowSalary.ratingPoint - flowSalary.waitTimeMistake);

                        salaryModel.Update(flowSalary);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        /// <summary>
        /// Insert voucher wait-time
        /// </summary>
        /// <param name="bill"></param>
        public void vourcherWaitTimeInsert(BillService bill, cls_voucher_waitime_custom item)
        {
            try
            {
                //add data to CRM_VoucherWaitTime
                CRM_VoucherWaitTime crm_voucher = new CRM_VoucherWaitTime();
                crm_voucher.BillID = bill.Id;
                crm_voucher.CheckinID = bill.ReceptionId;
                crm_voucher.CreatedTime = DateTime.Now;
                crm_voucher.CustomerID = bill.CustomerId;

                crm_voucher.SalonID = bill.SalonId;
                crm_voucher.SkinnerID = bill.Staff_HairMassage_Id;
                crm_voucher.StylistID = bill.Staff_Hairdresser_Id;

                crm_voucher.StylistPoint = item.StylistPoint;
                crm_voucher.SkinnerPoint = item.SkinnerPoint;
                crm_voucher.CheckinPoint = item.CheckinPoint;

                crm_voucher.StylistMoney = item.StylistPoint * item.StylistConfigSalary;
                crm_voucher.SkinnerMoney = item.SkinnerPoint * item.SkinnerConfigSalary;
                crm_voucher.CheckinMoney = item.CheckinPoint * item.CheckinConfigSalary;

                crm_voucher.VoucherPercent = 20;
                crm_voucher.IsUsed = false;
                crm_voucher.IsDelete = false;

                this.db.CRM_VoucherWaitTime.Add(crm_voucher);
                this.db.SaveChanges();
            }
            catch (Exception ex)
            {
                //throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Check khách có phải khách VIP hay không
        /// </summary>
        /// <param name="customerID"></param>
        /// <returns></returns>
        public bool isCustomerVIP(int customerID)
        {
            try
            {
                var ret = false;
                var specialCustomer = new SpecialCustomer();
                var detailSpecialCustomer = new SpecialCusDetail();
                // Kiểm tra khách hàng đặc biệt
                specialCustomer = this.db.SpecialCustomers.Where(a => a.IsDelete == false && a.CustomerTypeId == 2 && a.CustomerId == customerID).OrderBy(a => a.Id).FirstOrDefault();

                if (specialCustomer != null)
                {
                    ret = true;
                }
                return ret;
            }
            catch (Exception ex)
            {
                //throw new Exception(ex.Message);
                return false;
            }
        }

        /**
         * Get ServiceRatingModel Instance
         * @return ServiceRatingModel
         **/
        public static UpdateSalaryByBillFromExcel Instance
        {
            get
            {
                return new UpdateSalaryByBillFromExcel();
            }
        }

        public int IndexToInt(DataStaffFromExcel data, string index)
        {
            int c = 0;
            int indexInt = 0;

            foreach (PropertyInfo i in data.GetType().GetProperties())
            {
                if (index == i.Name)
                {
                    indexInt = c;
                    break;
                }
                c++;
            }

            return indexInt;
        }

        public class DataStaffFromExcel
        {
            public int BillID { get; set; }
        }

        /// <summary>
        /// Class sử dụng cho phần update voucher giảm giá khi thời gian chờ >= 20 phút
        /// </summary>
        public class cls_voucher_waitime
        {
            public int ID { get; set; }
            public bool isVoucher { get; set; }
            public float VoucherPercent { get; set; }
            public List<int> ServiceIDs { get; set; }
        }

        public class cls_voucher_waitime_custom : CRM_VoucherWaitTime
        {
            public int StylistConfigSalary { get; set; }
            public int SkinnerConfigSalary { get; set; }
            public int CheckinConfigSalary { get; set; }
        }
    }
}