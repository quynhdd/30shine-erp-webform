﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OffenItem_Add.aspx.cs" Inherits="_30shine.GUI.BackEnd.Fund.OffenItem_Add" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="ProductAdd" ContentPlaceHolderID="CtMain" runat="server">
<asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
<asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

<div class="wp sub-menu">
    <div class="wp960">
        <div class="wp content-wp">
            <ul class="ul-sub-menu" id="subMenu">      
                <li>Quản lý ngân sách &nbsp;&#187; Khoản thu/chi thường xuyên &nbsp;&#187;</li>
                <li class="li-listing"><a href="/admin/ngan-sach/khoan-thuong-xuyen/danh-sach.html">Danh sách</a></li>
                <% if(_IsUpdate){ %>
                    <li class="li-add"><a href="/admin/ngan-sach/khoan-thuong-xuyen/them-moi.html">Thêm mới</a></li>
                    <li class="li-edit active"><a href="/admin/ngan-sach/khoan-thuong-xuyen/<%= _Code %>.html">Cập nhật</a></li>
                <% }else{ %>
                    <li class="li-add active"><a href="/admin/ngan-sach/khoan-thuong-xuyen/them-moi.html">Thêm mới</a></li>
                <% } %>
            </ul>
        </div>
    </div>
</div>

<div class="wp customer-add">
    <%-- Add --%>                
    <div class="wp960 content-wp">
        <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
            <div class="table-wp">
                <table class="table-add admin-product-table-add">
                    <tbody>
                        <tr class="title-head">
                            <td><strong>Thông tin khoản thu chi thường xuyên</strong></td>
                            <td></td>
                        </tr>
                        <tr class="tr-margin" style="height: 20px;"></tr>
                        <tr>
                            <td class="col-xs-3 left"><span>Tiêu đề</span></td>
                            <td class="col-xs-9 right">
                                <span class="field-wp">
                                    <asp:TextBox ID="Title" runat="server" ClientIDMode="Static"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="TitleValidate" ControlToValidate="Title" runat="server" CssClass="fb-cover-error" Text="Bạn chưa nhập tiêu đề!"></asp:RequiredFieldValidator>
                                </span>
                                            
                            </td>
                        </tr>                   

                        <tr class="tr-description">
                            <td class="col-xs-3 left"><span>Mô tả</span></td>
                            <td class="col-xs-9 right">
                                <span class="field-wp">
                                    <asp:TextBox TextMode="MultiLine" Rows="5" ID="Description" runat="server" ClientIDMode="Static" style="padding: 10px;"></asp:TextBox>
                                </span>
                            </td>
                        </tr>

                        <tr class="tr-field-ahalf">
                            <td class="col-xs-3 left"><span>Nguồn</span></td>
                            <td class="col-xs-9 right">
                                <span class="field-wp">
                                    <asp:DropDownList ID="ddlSource" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                        <asp:RequiredFieldValidator InitialValue="0" 
                                            ID="ValidateSource" Display="Dynamic" 
                                            ControlToValidate="ddlSource"
                                            runat="server"  Text="Bạn chưa chọn nguồn!" 
                                            ErrorMessage="Vui lòng chọn nguồn!"
                                            ForeColor="Red"
                                        CssClass="fb-cover-error">
                                        </asp:RequiredFieldValidator>
                                </span>
                            </td>
                        </tr>

                        <tr class="tr-field-ahalf">
                            <td class="col-xs-3 left"><span>Thu / Chi</span></td>
                            <td class="col-xs-9 right">
                                <span class="field-wp">
                                    <asp:DropDownList ID="ddlIsReceipt" runat="server" ClientIDMode="Static">
                                        <asp:ListItem Text="Thu" Value="True" ></asp:ListItem>
                                        <asp:ListItem Text="Chi" Value="False"></asp:ListItem>
                                    </asp:DropDownList>
                                </span>
                            </td>
                        </tr>

                        <%--<tr class="tr-field-ahalf">
                            <td class="col-xs-3 left"><span>Chi phí (Số tiền - VNĐ)</span></td>
                            <td class="col-xs-9 right">
                                <span class="field-wp">
                                    <asp:TextBox ID="Money" TextMode="Number" runat="server" ClientIDMode="Static" Style="height: 36px; line-height: 36px; border: 1px solid #ddd; padding: 0 10px;"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="MoneyValidate" ControlToValidate="Money" runat="server" CssClass="fb-cover-error" Text="Bạn chưa nhập chi phí!"></asp:RequiredFieldValidator>
                                </span>
                                            
                            </td>
                        </tr>--%>


                        <tr class="tr-field-ahalf">
                            <td class="col-xs-3 left"><span>Thứ tự sắp xếp</span></td>
                            <td class="col-xs-9 right">
                                <span class="field-wp">
                                    <asp:TextBox ID="_Order" runat="server" ClientIDMode="Static"
                                        placeholder="Ví dụ : 1"></asp:TextBox>
                                </span>
                            </td>
                        </tr>

                        <tr class="tr-product-category">
                            <td class="col-xs-3 left"><span>Publish</span></td>
                            <td class="col-xs-9 right">
                                <span class="field-wp">
                                    <asp:CheckBox ID="Publish" Checked="true" runat="server" />
                                </span>
                            </td>
                        </tr>
                    
                        <tr class="tr-send">
                            <td class="col-xs-3 left"></td>
                            <td class="col-xs-9 right no-border">
                                <span class="field-wp">
                                    <asp:Button ID="Send" CssClass="btn-send" runat="server" Text="Hoàn tất" ClientIDMode="Static" OnClick="ExcAddOrUpdate" OnClientClick="addLoading();"></asp:Button>
                                    <%--<span class="btn-send btn-cancel" id="CalcelAddFbcv" style="margin-left: 12px;" >Hủy</span>--%>
                                </span>
                            </td>
                        </tr>
                        <asp:HiddenField runat="server" ID="UrlKeyField"  ClientIDMode="Static"/>
                        <asp:HiddenField runat="server" ID="ImgCoverField"  ClientIDMode="Static"/>
                    </tbody>
                </table>
            </div>
        </div>
    <%-- end Add --%>
</div>

<script>
    jQuery(document).ready(function () {
        $("#glbAdminContent").addClass("active");
        $("#glbAdminFundOffenItem").addClass("active");
        //============================
        // Show System Message
        //============================ 
        var qs = getQueryStrings();
        showMsgSystem(qs["msg_update_message"], qs["msg_update_status"]);

    });
</script>

</asp:Panel>
</asp:Content>


