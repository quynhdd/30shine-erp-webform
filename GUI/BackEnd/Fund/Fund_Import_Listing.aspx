﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Fund_Import_Listing.aspx.cs" Inherits="_30shine.GUI.BackEnd.Fund.Fund_Import_Listing" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
<asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

<style>
    .customer-add .table-add td.map-edit { text-align:right; }
</style>

<div class="wp sub-menu">
    <div class="wp960">
        <div class="wp content-wp">
            <ul class="ul-sub-menu" id="subMenu">
                <li>Quản lý ngân sách &nbsp;&#187; Thu/chi &nbsp;&#187; </li>
                <li class="li-listing"><a href="/admin/ngan-sach/thong-ke.html">Báo cáo dòng tiền</a></li>
                <li class="li-listing active"><a href="/admin/ngan-sach/thu-chi/danh-sach.html">Nhật ký thu / chi</a></li>
                <li class="li-add"><a href="/admin/ngan-sach/thu-chi/them-moi.html">Thêm mới</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="wp customer-add customer-listing be-report">    
    <%-- Listing --%>                
    <div class="wp960 content-wp">
        <!-- Filter -->
        <div class="row">
            <div class="filter-item" style="margin-left: 0;">
                <strong class="st-head" style="margin-right: 10px;"><i class="fa fa-clock-o"></i>Thời gian</strong>                
                <div class="datepicker-wp">
                    <asp:TextBox CssClass="txtDateTime st-head" ID="TxtDateTimeFrom" placeholder="Từ ngày"
                        ClientIDMode="Static" runat="server"></asp:TextBox>

                    <strong class="st-head" style="margin-left: 10px; margin-right: 10px;">
                        <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                    </strong>
                    <asp:TextBox CssClass="txtDateTime st-head" ID="TxtDateTimeTo" placeholder="Đến ngày"
                        ClientIDMode="Static" runat="server"></asp:TextBox>
                </div>
                <br />
                <div class="tag-wp">
                    <p class="tag tag-date" onclick="viewDataByDate($(this), '<%=Day1 %>')">Hôm kia <span class="txt-date"><%=Day1 %></span></p>
                    <p class="tag tag-date" onclick="viewDataByDate($(this), '<%=Day2 %>')">Hôm qua<span class="txt-date"><%=Day2 %></span></p>
                    <p class="tag tag-date tag-date-today" onclick="viewDataByDate($(this), '<%=Day3 %>')">Hôm nay<span class="txt-date"><%=Day3 %></span></p>
                </div>
            </div>
            
            <div class="filter-item">
                <asp:DropDownList ID="ddlAccountType" runat="server" ClientIDMode="Static" style="margin: 12px 0; width: 190px;"></asp:DropDownList>
            </div>                     

            <div class="filter-item">
                <asp:DropDownList ID="ddlSource" runat="server" ClientIDMode="Static" style="margin: 12px 0; width: 190px;"></asp:DropDownList>
            </div>    

            <asp:Panel ID="ViewData" CssClass="st-head btn-viewdata" Text="Xem dữ liệu" 
                ClientIDMode="Static" onclick="excPaging(1)" runat="server">Xem dữ liệu</asp:Panel>
            <a href="javascript://" onclick="location.href=location.href" class="st-head btn-viewdata">Reset Filter</a>
            <div class="export-wp drop-down-list-wp">
                <asp:Panel ID="fff" CssClass="st-head btn-viewdata"
                    ClientIDMode="Static" runat="server" onclick="EntryExportExcel('all')">Xuất File Excel</asp:Panel>
                <ul class="ul-drop-down-list" style="display:none;">
                    <li onclick="EntryExportExcel('all')">Tất cả dữ liệu</li>
                    <li onclick="EntryExportExcel('current page')">Trang hiện tại</li>
                </ul>
            </div>          
        </div> 
        <!-- End Filter -->
        <div class="row mgt" style="margin-top: 10px;">
            <strong class="st-head"><i class="fa fa-file-text"></i>Danh sách thu/chi</strong>
        </div>
        <!-- Row Table Filter -->
        <div class="table-func-panel" style="margin-top: -33px;">
            <div class="table-func-elm">
                <span>Số hàng / Page : </span>
                <div class="table-func-input-wp">
                    <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                    <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                    <ul class="ul-opt-segment">
                        <li data-value="10">10</li>
                        <li data-value="20">20</li>
                        <li data-value="30">30</li>
                        <li data-value="40">40</li>
                        <li data-value="50">50</li>
                        <li data-value="1000000">Tất cả</li>
                    </ul>
                    <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                </div>                
            </div>            
        </div>
        <!-- End Row Table Filter -->
        <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>        
        <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
            <ContentTemplate>
                <div class="table-wp">
                    <table class="table-add table-listing">
                        <thead>
                            <tr>
                                <th>STT</th>
                                <th>ID</th>
                                <th>Ngày</th>
                                <th>Diễn giải nội dung</th>
                                <th>Nguồn</th>
                                <th>Thu / Chi</th>
                                <th>Tổng chi phí</th>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:Repeater ID="Rpt" runat="server">
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Container.ItemIndex + 1 + PAGING._Offset %></td>
                                        <td><a href="/admin/ngan-sach/thu-chi/<%# Eval("Id") %>.html"><%# Eval("Id") %></a></td>
                                        <td>
                                            <a href="/admin/ngan-sach/thu-chi/<%# Eval("Id") %>.html"><%# String.Format("{0:dd/MM/yyyy}", Eval("ImportDate")) %></a>
                                        </td>                                        
                                        <td><%# Eval("Note") %></td>
                                        <td><%# Eval("sourceName") %></td>
                                        <td><%# Convert.ToBoolean(Eval("IsReceipt")) ? "Thu" : "Chi" %></td>
                                        <td class="map-edit">
                                            <%# String.Format("{0:#,###}",Eval("TotalMoney")).Replace(',','.') %>
                                            <div class="edit-wp">
                                                <a class="elm edit-btn" href="/admin/ngan-sach/thu-chi/<%# Eval("Id") %>.html" title="Sửa"></a>
                                                <a class="elm del-btn" onclick="del(this.parentNode.parentNode.parentNode,'<%# Eval("Id") %>', '<%# Eval("Id") %>')" href="javascript://" title="Xóa"></a>
                                            </div>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>                    
                        </tbody>
                    </table>
                </div>

                <!-- Paging -->
                <div class="site-paging-wp">
                <% if(PAGING.TotalPage > 1){ %>
                    <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                        <% if (PAGING._Paging.Prev != 0)
                           { %>
                        <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                        <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                        <% } %>
                        <asp:Repeater ID="RptPaging" runat="server">
                            <ItemTemplate>
                                <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                    <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>
                                >
                                    <%# Eval("PageNum") %>
                                </a>
                            </ItemTemplate>                    
                        </asp:Repeater>
                        <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                           { %>                
                        <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                        <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                        <% } %>
                    </asp:Panel>
                <% } %>
                </div>
		        <!-- End Paging -->                
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
        <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" style="display:none;" />
        <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
        <%-- Excel --%>
        <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_ExcelType" />
        <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_EXCELPage" />
        <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_EXCELSegment" />
        <asp:Button ID="BtnFakeExcel" ClientIDMode="Static" runat="server" OnClick="Exc_ExportExcel" style="display:none;" />  
        <%--// Excel --%>
    </div> 
    <%-- END Listing --%>
</div>

<script>
    jQuery(document).ready(function () {
        $("#glbAdminContent").addClass("active");
        $("#glbAdminFund").addClass("active");

        //============================
        // Datepicker
        //============================
        $('.txtDateTime').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'vi',
            startDate: '2014/10/10',
            format: 'd/m/Y',
            dateonly: true,
            showHour: false,
            showMinute: false,
            timepicker: false,
            onChangeDateTime: function (dp, $input) { },
            scrollMonth: false,
            scrollTime: false,
            scrollInput: false
        });

        // View data today
        $(".tag-date-today").click();

    });

    function viewDataByDate(This, time) {
        $(".tag-date.active").removeClass("active");
        This.addClass("active");
        $("#TxtDateTimeTo").val("");
        $("#TxtDateTimeFrom").val(time);
        $("#ViewData").click();
    }

    //============================
    // Event delete
    //============================
    function del(This, code, name) {
        var code = code || null,
            name = name || null,
            Row = This;
        if (!code) return false;

        // show EBPopup
        $(".confirm-yn").openEBPopup();
        $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn xóa [ " + name + " ] ?");

        $("#EBPopup .yn-yes").bind("click", function () {
            $.ajax({
                type: "POST",
                url: "/GUI/SystemService/Ajax/Del.aspx/Delele_Fund_Import",
                data: '{Code : "' + code + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json", success: function (response) {
                    var mission = JSON.parse(response.d);
                    if (mission.success) {
                        delSuccess();
                        Row.remove();
                    } else {
                        delFailed();
                    }
                },
                failure: function (response) { alert(response.d); }
            });
        });
        $("#EBPopup .yn-no").bind("click", function () {
            autoCloseEBPopup(0);
        });
    }
</script>

</asp:Panel>
</asp:Content>

