﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Globalization;
using ExportToExcel;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.Interface;

namespace _30shine.GUI.BackEnd.Fund
{
    public partial class Fund_Import_Listing : System.Web.UI.Page
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public Fund_Import_Listing()
        {
            db = new Solution_30shineEntities();
        }
        private Solution_30shineEntities db;
        private string PageID = "QL_DT_DT";
        private bool Perm_Access = false;
        private bool Perm_Edit = false;
        private bool Perm_Delete = false;

        protected string Day1 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-2));
        protected string Day2 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-1));
        protected string Day3 = string.Format("{0:dd/MM/yyyy}", DateTime.Now);

        protected Paging PAGING = new Paging();

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (!IsPostBack)
            {
                bindAccountType();
                bindSource();
                Bind_Paging();
                Bind_Rpt();
            }
        }
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);               
                //Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);  
                //ExecuteByPermission();

                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        /// <summary>
        /// Tạo câu truy vấn sql
        /// </summary>
        /// <returns></returns>
        private string genSql()
        {
            int integer;
            DateTime timeFrom;
            DateTime timeTo;
            CultureInfo culture = new CultureInfo("vi-VN");
            string whereTime = "";
            string whereSource = "";
            string whereAccount = "";
            int sourceId = int.TryParse(ddlSource.SelectedValue, out integer) ? integer : 0;
            int accountTypeId = int.TryParse(ddlAccountType.SelectedValue, out integer) ? integer : 0;

            if (TxtDateTimeFrom.Text != "")
            {
                timeFrom = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                if (TxtDateTimeTo.Text != "")
                {
                    timeTo = Convert.ToDateTime(TxtDateTimeTo.Text, culture);
                }
                else
                {
                    timeTo = timeFrom;
                }
                whereTime = " and fund_import.ImportDate between '" + String.Format("{0:yyyy/MM/dd}", timeFrom) + "' and '" + String.Format("{0:yyyy/MM/dd}", timeTo) + "'";
            }
            
            if (sourceId > 0)
            {
                whereSource = " and fund_import.SourceId = "+ sourceId;
            }

            if (accountTypeId > 0)
            {
                whereAccount = " and fund_import.AccountTypeId = " + accountTypeId;
            }

            string sql = @"select fund_import.*, 
                            fund_accountType.Name as accountTypeName, fund_source.Name as sourceName
                        from Fund_Import as fund_import
                        left join Fund_AccountType as fund_accountType
                        on fund_import.AccountTypeId = fund_accountType.Id
                        left join Fund_Source as fund_source
                        on fund_import.SourceId = fund_source.Id
                        where fund_import.IsDelete != 1" + whereAccount + whereSource + whereTime;
            return sql;
        }

        /// <summary>
        /// Lấy dữ liệu import
        /// </summary>
        /// <param name="isPaging"></param>
        /// <returns></returns>
        private List<Library.Class.cls_fund_import_item> getData(bool isPaging) {
            var list = new List<Library.Class.cls_fund_import_item>();
            string sql = genSql();
            if (sql != "")
            {
                if(isPaging)
                    list = db.Database.SqlQuery<Library.Class.cls_fund_import_item>(sql).OrderByDescending(o => o.ImportDate).Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                else
                    list = db.Database.SqlQuery<Library.Class.cls_fund_import_item>(sql).ToList();
            }
            return list;
        }

        /// <summary>
        /// Bind data
        /// </summary>
        private void Bind_Rpt()
        {
            Rpt.DataSource = getData(true);
            Rpt.DataBind();
        }

        /// <summary>
        /// Bind tài khoản tiền
        /// </summary>
        public void bindAccountType()
        {
            var accountType = db.Fund_AccountType.Where(w => w.IsDelete != true && w.Publish == true).ToList();
            var item = new Fund_AccountType();
            item.Id = 0;
            item.Name = "Tất cả tài khoản";
            accountType.Insert(0, item);
            ddlAccountType.DataTextField = "Name";
            ddlAccountType.DataValueField = "Id";
            ddlAccountType.DataSource = accountType;
            ddlAccountType.DataBind();
        }

        /// <summary>
        /// Bind đối tượng (salon, bộ phận chung...)
        /// </summary>
        public void bindSource()
        {
            var source = db.Fund_Source.Where(w => w.IsDelete != true && w.Publish == true).ToList();
            var item = new Fund_Source();
            item.Id = 0;
            item.Name = "Tất cả 30Shine";
            source.Insert(0, item);
            ddlSource.DataTextField = "Name";
            ddlSource.DataValueField = "Id";
            ddlSource.DataSource = source;
            ddlSource.DataBind();
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            Bind_Paging();
            Bind_Rpt();
            RemoveLoading();
        }

        protected void Bind_Paging()
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? (HDF_Page.Value != "" ? Convert.ToInt32(HDF_Page.Value) : 1) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage() : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage()
        {
            int Count = 0;
            string sql = genSql();
            if (sql != "")
            {
                Count = db.Database.SqlQuery<Library.Class.cls_fund_import_item>(sql).Count();
            }
            int TotalRow = Count - PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        /// <summary>
        /// Tạo dữ liệu excel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Exc_ExportExcel(object sender, EventArgs e)
        {
            var data = getData(false);

            var _ExcelHeadRow = new List<string>();
            var BillRowLST = new List<List<string>>();
            var BillRow = new List<string>();

            _ExcelHeadRow.Add("STT");
            _ExcelHeadRow.Add("Ngày");
            _ExcelHeadRow.Add("Thu/chi");
            _ExcelHeadRow.Add("Nội dung diễn giải");
            _ExcelHeadRow.Add("Số tiền");
            _ExcelHeadRow.Add("Tài khoản");
            _ExcelHeadRow.Add("Đối tượng");            

            if (data.Count > 0)
            {
                int loop = 0;
                foreach (var v in data)
                {
                    BillRow = new List<string>();
                    BillRow.Add((++loop).ToString());
                    BillRow.Add(String.Format("{0:dd/MM/yyyy}",v.ImportDate));
                    BillRow.Add(v.IsReceipt.Value ? "Thu" : "Chi");
                    BillRow.Add(v.Note);
                    BillRow.Add(v.TotalMoney.ToString());
                    BillRow.Add(v.accountTypeName.ToString());
                    BillRow.Add(v.sourceName.ToString());                    
                    BillRowLST.Add(BillRow);
                }
            }

            // export
            var ExcelStorePath = Server.MapPath("~") + "Public/Excel/Thu.Chi/";
            var FileName = "Thu_Chi_" + string.Format("{0:dd_MM_yyyy_hh_mm_ss}", DateTime.Now) + ".xlsx";
            var UrlFileName = "http://" + Request.Url.Host + "/Public/Excel/Thu.Chi/" + FileName;

            ExportXcel(_ExcelHeadRow, BillRowLST, ExcelStorePath + FileName);
            Bind_Paging();
            ScriptManager.RegisterStartupScript(BtnFakeExcel, this.GetType(), "Download Excel", "CallBackExportExcel('" + UrlFileName + "');UP_FormatPrice('.be-report-price'); SubTableFixHeight();SetActiveTableListing();", true);
        }

        /// <summary>
        /// Export Excel
        /// </summary>
        /// <param name="_ExcelHeadRow"></param>
        /// <param name="List"></param>
        /// <param name="Path"></param>
        private static void ExportXcel(List<string> _ExcelHeadRow, List<List<string>> List, string Path)
        {
            try
            {
                CreateExcelFile.ExcelHeadRow = _ExcelHeadRow;
                CreateExcelFile.CreateExcel(List, Path);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi. Không tạo được file excel.\r\nException: " + ex.Message);
            }
        }


        public void Repeater_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
              || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (Perm_Edit == true)
                    ((Panel)(e.Item.FindControl("EAedit"))).Visible = true;
                if (Perm_Delete == true)
                    ((Panel)(e.Item.FindControl("EAdelete"))).Visible = true;
            }
        }
    }
}