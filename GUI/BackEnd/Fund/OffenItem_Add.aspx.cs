﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.Fund
{
    public partial class OffenItem_Add : System.Web.UI.Page
    {
        private string PageID = "";
        protected Fund_OffenItem OBJ;
        protected string _Code;
        protected bool _IsUpdate = false;
        private bool Perm_Access = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (!IsPostBack)
            {
                if (IsUpdate())
                {
                    if (Bind_OBJ())
                    {
                        //
                        bindSource();
                    }
                }
                else
                {
                    // 
                    bindSource();
                }

            }
        }
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //if (Request.QueryString["Code"] != null)
                //{
                //    PageID = "NS_KTX_ADD";
                //}
                //else
                //{
                //    PageID = "NS_KTX_EDIT";
                //}
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);               
                //ExecuteByPermission();

                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        protected void ExcAddOrUpdate(object sender, EventArgs e)
        {
            if (IsUpdate())
            {
                Update();
            }
            else
            {
                Add();
            }
        }

        private void Add()
        {
            using (var db = new Solution_30shineEntities())
            {
                int integer;
                var obj = new Fund_OffenItem();
                obj.Title = Title.Text;
                obj.Description = Description.Text;
                //obj.Money = int.TryParse(Money.Text, out integer) ? integer : 0;
                obj.Order = _Order.Text != "" ? Convert.ToInt32(_Order.Text) : 0;
                obj.SourceId = int.TryParse(ddlSource.SelectedValue, out integer) ? integer : 0;
                obj.IsReceipt = Convert.ToBoolean(ddlIsReceipt.SelectedValue);
                obj.Publish = Publish.Checked;
                obj.CreatedTime = DateTime.Now;
                obj.IsDelete = false;

                // Validate
                var Error = false;

                if (!Error)
                {
                    db.Fund_OffenItem.Add(obj);
                    var exc = db.SaveChanges();

                    if (exc > 0)
                    {
                        var MsgParam = new List<KeyValuePair<string, string>>();
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thành công!"));
                        UIHelpers.Redirect("/admin/ngan-sach/khoan-thuong-xuyen/" + obj.Id + ".html", MsgParam);
                    }
                    else
                    {
                        var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển.";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                    }
                }
            }
        }

        private void Update()
        {
            using (var db = new Solution_30shineEntities())
            {
                int Id = 0;
                int integer;
                if (int.TryParse(_Code, out Id))
                {
                    OBJ = db.Fund_OffenItem.FirstOrDefault(w => w.Id == Id);

                    if (!OBJ.Equals(null))
                    {
                        OBJ.Title = Title.Text;
                        OBJ.Description = Description.Text;
                        //OBJ.Money = int.TryParse(Money.Text, out integer) ? integer : 0;
                        OBJ.SourceId = int.TryParse(ddlSource.SelectedValue, out integer) ? integer : 0;
                        OBJ.IsReceipt = Convert.ToBoolean(ddlIsReceipt.SelectedValue);
                        OBJ.Order = _Order.Text != "" ? Convert.ToInt32(_Order.Text) : 0;
                        OBJ.Publish = Publish.Checked;
                        OBJ.ModifiedTime = DateTime.Now;

                        db.Fund_OffenItem.AddOrUpdate(OBJ);
                        var exc = db.SaveChanges();
                        if (exc > 0)
                        {
                            var MsgParam = new List<KeyValuePair<string, string>>();
                            MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                            MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thành công!"));
                            UIHelpers.Redirect("/admin/ngan-sach/khoan-thuong-xuyen/" + OBJ.Id + ".html", MsgParam);
                        }
                        else
                        {
                            var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển!";
                            var status = "warning";
                            UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                        }
                    }
                    else
                    {
                        var msg = "Lỗi! Bản ghi không tồn tại.";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);

                    }
                }
                else
                {
                    MsgSystem.Text = "Lỗi! Bản ghi không tồn tại.";
                    MsgSystem.CssClass = "msg-system warning";
                }

                Context.RewritePath("+", "", "");
            }
        }

        private bool Bind_OBJ()
        {
            using (var db = new Solution_30shineEntities())
            {
                var ExistOBJ = true;
                int Id;
                if (int.TryParse(_Code, out Id))
                {
                    OBJ = db.Fund_OffenItem.Where(w => w.Id == Id).FirstOrDefault();

                    if (!OBJ.Equals(null))
                    {
                        Title.Text = OBJ.Title;
                        Description.Text = OBJ.Description;
                        //Money.Text = OBJ.Money.ToString();
                        _Order.Text = Convert.ToString(OBJ.Order);

                        // Bind Thu/Chi option
                        var itemSelected = ddlIsReceipt.Items.FindByValue(OBJ.IsReceipt.ToString());
                        if (itemSelected != null)
                        {
                            itemSelected.Selected = true;
                        }

                        if (OBJ.Publish == true)
                        {
                            Publish.Checked = true;
                        }
                        else
                        {
                            Publish.Checked = false;
                        }

                    }
                    else
                    {
                        ExistOBJ = false;
                        var msg = "Lỗi! Bản ghi không tồn tại";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                    }
                }
                else
                {
                    ExistOBJ = false;
                    var msg = "Lỗi! Bản ghi không tồn tại";
                    var status = "warning";
                    UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                }

                return ExistOBJ;
            }
        }

        public void bindSource()
        {
            using (var db = new Solution_30shineEntities())
            {
                var source = db.Fund_Source.Where(w => w.IsDelete != true && w.Publish == true).ToList();
                ddlSource.DataTextField = "Name";
                ddlSource.DataValueField = "Id";
                ddlSource.DataSource = source;
                ddlSource.DataBind();
                if (OBJ != null)
                {
                    var itemSelected = ddlSource.Items.FindByValue(OBJ.SourceId.ToString());
                    if (itemSelected != null)
                    {
                        itemSelected.Selected = true;
                    }
                }
            }
        }

        public void ReloadPage()
        {
            HttpRequest Request = HttpContext.Current.Request;
            string host = "http://" + Request.Url.Host.ToString();
            string rawUrl = Request.RawUrl.ToString();
            string Url = host + rawUrl;
            Response.Redirect(Url);
        }

        private bool IsUpdate()
        {
            _Code = Request.QueryString["Code"];
            if (_Code != null)
            {
                _IsUpdate = true;
                return true;
            }
            else
            {
                _IsUpdate = false;
                return false;
            }
        }

      
    }
}