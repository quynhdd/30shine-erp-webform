﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Web.Script.Serialization;
using System.Globalization;
using System.Web.Services;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.Fund
{
    public partial class Fund_Import_Add_V3 : System.Web.UI.Page
    {
        private string PageID = "";
        private bool Perm_Access = false;

        protected Fund_Import OBJ;
        protected string _Code;
        protected bool _IsUpdate = false;
        protected List<Fund_OffenItem> offenItems = new List<Fund_OffenItem>();
        private CultureInfo culture = new CultureInfo("vi-VN");
        protected List<Fund_OffenItem> listOffenItem = new List<Fund_OffenItem>();
        protected List<Fund_Source> listSource = new List<Fund_Source>();
        protected List<ThuChi_DanhSachNguon_Result> listSourceFlow = new List<ThuChi_DanhSachNguon_Result>();
        protected List<int> listSourceChecked = new List<int>();
        protected Fund_OffenItem offenItem;

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (!IsPostBack)
            {
                txtDate.Text = String.Format("{0:dd/MM/yyyy}", DateTime.Now);
                using (var db = new Solution_30shineEntities())
                {
                    listOffenItem = db.Fund_OffenItem.Where(w => w.IsReceipt == true && w.IsDelete != true).OrderBy(o => o.Order).ToList();
                    listSource = db.Fund_Source.Where(w => w.IsDelete != true && w.Publish == true).ToList();
                }

                if (IsUpdate())
                {
                    if (Bind_OBJ())
                    {
                        //
                        bindAccountType();
                        bindSource();
                        //bindOffenItem();
                    }
                }
                else
                {
                    // 
                    bindAccountType();
                    bindSource();
                    //bindOffenItem();
                }

            }
        }
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //if (Request.QueryString["Code"] !=null)
                //{
                //    PageID = "NS_TC_EDIT";
                //}
                //else
                //{
                //    PageID = "NS_TC_ADD";
                //}
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //ExecuteByPermission();

                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        protected void ExcAddOrUpdate(object sender, EventArgs e)
        {
            if (IsUpdate())
            {
                Update();
            }
            else
            {
                Add();
            }
        }

        private void Add()
        {
            using (var db = new Solution_30shineEntities())
            {
                if (HDF_CostId.Value != "" && txtDate.Text != "")
                {
                    int integer; 
                    int costId;
                    int error = 0;
                    String[] sourceIds;
                    DateTime date = Convert.ToDateTime(txtDate.Text, culture);
                    int accountTypeId = int.TryParse(ddlAccountType.SelectedValue, out integer) ? integer : 0;
                    bool isReceipt = Convert.ToBoolean(ddlIsReceipt.SelectedValue);
                    string note = Note.Text;
                    sourceIds = HDF_Source.Value.Split(',');
                    costId = int.TryParse(HDF_CostId.Value, out integer) ? integer : 0;
                    Fund_OffenItem offenItem = db.Fund_OffenItem.FirstOrDefault(w => w.Id == costId);

                    var obj = new Fund_Import();
                    obj.ImportDate = date;
                    obj.OffenItemId = costId;
                    obj.AccountTypeId = accountTypeId;
                    obj.IsReceipt = isReceipt;
                    obj.Note = note;
                    obj.CreatedTime = DateTime.Now;
                    obj.IsDelete = false;

                    db.Fund_Import.Add(obj);
                    error += db.SaveChanges() > 0 ? 0 : 1;
                    
                    if (offenItem != null)
                    {
                        // Cập nhật item thu chi vào bảng Fund_Item_Flow
                        var serialize = new JavaScriptSerializer();
                        var jsonArrayItems = HDF_ReceiptItems.Value;
                        if (jsonArrayItems != "")
                        {
                            var postItem = serialize.Deserialize<List<Library.Class.cls_fund_receipt_item>>(jsonArrayItems);
                            if (postItem.Count > 0)
                            {
                                int totalMoney = 0;
                                foreach (var v in postItem)
                                {
                                    var item = new Fund_Item_Flow();
                                    item.ImportId = obj.Id;
                                    item.SourceId = v.Id;
                                    item.ItemId = offenItem.Id;
                                    item.ItemTitle = offenItem.Title;
                                    item.ItemMoney = v.Money;
                                    item.CreatedTime = DateTime.Now;
                                    item.IsDelete = false;
                                    db.Fund_Item_Flow.AddOrUpdate(item);
                                    db.SaveChanges();
                                    totalMoney += v.Money;
                                }
                                obj.TotalMoney = totalMoney;
                                db.Fund_Import.AddOrUpdate(obj);
                                db.SaveChanges();
                            }
                        }
                    }

                    if (error == 0)
                    {
                        var MsgParam = new List<KeyValuePair<string, string>>();
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thành công!"));
                        UIHelpers.Redirect("/admin/ngan-sach/thu-chi/" + obj.Id + ".html", MsgParam);
                    }
                    else
                    {
                        var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển.";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                    }
                }
            }
        }

        private void Update()
        {
            using (var db = new Solution_30shineEntities())
            {
                int Id = 0;
                int integer;
                if (int.TryParse(_Code, out Id))
                {
                    OBJ = db.Fund_Import.FirstOrDefault(w => w.Id == Id);
                    var costId = int.TryParse(HDF_CostId.Value, out integer) ? integer : 0;
                    var offenItem = db.Fund_OffenItem.FirstOrDefault(w=>w.Id == costId);
                    if (OBJ != null && offenItem != null)
                    {
                        OBJ.OffenItemId = costId;
                        OBJ.ImportDate = Convert.ToDateTime(txtDate.Text, culture);
                        OBJ.AccountTypeId = int.TryParse(ddlAccountType.SelectedValue, out integer) ? integer : 0;
                        OBJ.IsReceipt = Convert.ToBoolean(ddlIsReceipt.SelectedValue);
                        OBJ.Note = Note.Text;
                        OBJ.ModifiedTime = DateTime.Now;

                        db.Fund_Import.AddOrUpdate(OBJ);
                        var exc = db.SaveChanges();
                        if (exc > 0)
                        {
                            // Cập nhật item thu chi vào bảng Fund_Item_Flow
                            // 1. Đối chiếu danh sách thu/chi post lên với danh sách từ db, nếu có item nào mới (ko nằm trong danh sách db) thì thêm mới
                            // 2. Đối chiếu danh sách thu/chi từ db với danh sách post lên, nếu có item nào ko nằm trong danh sách post (item này bị xóa) thì xóa item
                            // Quy ước  A là danh sách item Thu/Chi được post lên từ client
                            //          B là danh sách item Thu/Chi có trong db
                            int sourceId;
                            String[] sourceIds;
                            var serialize = new JavaScriptSerializer();
                            var item = new Fund_Item_Flow();
                            var index = -1;
                            int totalMoney = 0;
                            var jsonArrayItems = HDF_ReceiptItems.Value;
                            var postItems = new List<Library.Class.cls_fund_receipt_item>();
                            var postSource = HDF_Source.Value.Split(',');
                            var dbItems = db.Fund_Item_Flow.Where(w => w.ImportId == OBJ.Id && w.IsDelete != true).ToList();

                            if (jsonArrayItems != "")
                            {
                                postItems = serialize.Deserialize<List<Library.Class.cls_fund_receipt_item>>(jsonArrayItems);
                            }

                            // 1. Kiểm tra danh sách postItems
                            if (postItems.Count > 0)
                            {
                                foreach (var v in postItems)
                                {
                                    index = dbItems.FindIndex(w => w.SourceId == v.Id);
                                    if (index != -1)
                                    {
                                        item = dbItems[index];
                                        item.ItemId = offenItem.Id;
                                        item.ItemTitle = offenItem.Title;
                                        item.ItemMoney = v.Money;
                                        item.SourceId = v.Id;
                                        item.ModifiedTime = DateTime.Now;
                                        db.Fund_Item_Flow.AddOrUpdate(item);
                                        db.SaveChanges();
                                    }
                                    else
                                    {
                                        item = new Fund_Item_Flow();
                                        item.ImportId = OBJ.Id;
                                        item.SourceId = v.Id;
                                        item.ItemId = offenItem.Id;
                                        item.ItemTitle = offenItem.Title;
                                        item.ItemMoney = v.Money;
                                        item.IsDelete = false;
                                        item.CreatedTime = DateTime.Now;
                                        db.Fund_Item_Flow.Add(item);
                                        db.SaveChanges();
                                    }
                                    totalMoney += v.Money;
                                }
                            }

                            // 2. Kiểm tra danh sách dbItems
                            if (dbItems.Count > 0)
                            {
                                foreach (var v in dbItems)
                                {
                                    index = postItems.FindIndex(w=>w.Id == v.SourceId);
                                    if (index != -1)
                                    {
                                        //  return
                                    }
                                    else
                                    {
                                        item = v;
                                        item.IsDelete = true;
                                        item.ModifiedTime = DateTime.Now;
                                        db.Fund_Item_Flow.AddOrUpdate(item);
                                        db.SaveChanges();
                                    }
                                }
                            }

                            OBJ.TotalMoney = totalMoney;
                            db.Fund_Import.AddOrUpdate(OBJ);
                            db.SaveChanges();

                            var MsgParam = new List<KeyValuePair<string, string>>();
                            MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                            MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thành công!"));
                            UIHelpers.Redirect("/admin/ngan-sach/thu-chi/" + OBJ.Id + ".html", MsgParam);
                        }
                        else
                        {
                            var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển!";
                            var status = "warning";
                            UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                        }
                    }
                    else
                    {
                        var msg = "Lỗi! Bản ghi không tồn tại.";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);

                    }
                }
                else
                {
                    MsgSystem.Text = "Lỗi! Bản ghi không tồn tại.";
                    MsgSystem.CssClass = "msg-system warning";
                }

                Context.RewritePath("+", "", "");
            }
        }

        /// <summary>
        /// Bind dữ liệu record
        /// 1. Hiển thị field cơ bản
        /// 2. Hiển thị list salon item
        /// </summary>
        /// <returns></returns>
        private bool Bind_OBJ()
        {
            using (var db = new Solution_30shineEntities())
            {
                var ExistOBJ = true;
                int Id;
                if (int.TryParse(_Code, out Id))
                {
                    OBJ = db.Fund_Import.Where(w => w.Id == Id).FirstOrDefault();

                    if (!OBJ.Equals(null))
                    {
                        offenItem = db.Fund_OffenItem.FirstOrDefault(w=>w.Id == OBJ.OffenItemId);
                        HDF_CostId.Value = OBJ.OffenItemId.ToString();
                        // Bind ngày nhập liệu
                        txtDate.Text = String.Format("{0:dd/MM/yyyy}", OBJ.ImportDate);

                        // Bind Thu/Chi option
                        listSourceFlow = db.ThuChi_DanhSachNguon(OBJ.Id).ToList();
                        rptItemFlow.DataSource = listSourceFlow;
                        rptItemFlow.DataBind();

                        // Bind Note
                        Note.Text = OBJ.Note;
                    }
                    else
                    {
                        ExistOBJ = false;
                        var msg = "Lỗi! Bản ghi không tồn tại";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                    }
                }
                else
                {
                    ExistOBJ = false;
                    var msg = "Lỗi! Bản ghi không tồn tại";
                    var status = "warning";
                    UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                }

                return ExistOBJ;
            }
        }

        public void bindAccountType()
        {
            using (var db = new Solution_30shineEntities())
            {
                var accountType = db.Fund_AccountType.Where(w => w.IsDelete != true && w.Publish == true).ToList();
                ddlAccountType.DataTextField = "Name";
                ddlAccountType.DataValueField = "Id";
                ddlAccountType.DataSource = accountType;
                ddlAccountType.DataBind();
                if (OBJ != null)
                {
                    var itemSelected = ddlAccountType.Items.FindByValue(OBJ.AccountTypeId.ToString());
                    if (itemSelected != null)
                    {
                        itemSelected.Selected = true;
                    }
                }
            }
        }

        public void bindSource()
        {
            using (var db = new Solution_30shineEntities())
            {
                var source = db.Fund_Source.Where(w => w.IsDelete != true && w.Publish == true).ToList();
                ddlSource.DataTextField = "Name";
                ddlSource.DataValueField = "Id";
                ddlSource.DataSource = source;
                ddlSource.DataBind();
                if (OBJ != null)
                {
                    var itemSelected = ddlSource.Items.FindByValue(OBJ.SourceId.ToString());
                    if (itemSelected != null)
                    {
                        itemSelected.Selected = true;
                    }
                }
            }
        }

        //public void bindOffenItem()
        //{
        //    using (var db = new Solution_30shineEntities())
        //    {
        //        var items = db.Fund_OffenItem.Where(w => w.IsDelete != true && w.Publish == true).OrderBy(o => o.Order).ToList();
        //        rptOffenItem.DataSource = items;
        //        rptOffenItem.DataBind();
        //    }
        //}

        [WebMethod]
        public static string loadOffenReceipt(bool isReceipt)
        {
            using (var db = new Solution_30shineEntities())
            {
                var serializer = new JavaScriptSerializer();
                var Msg = new Msg();
                var list = db.Fund_OffenItem.Where(w => w.IsReceipt == isReceipt && w.IsDelete != true).OrderBy(o => o.Order).ToList();
                Msg.msg = serializer.Serialize(list);
                Msg.success = true;

                return serializer.Serialize(Msg);
            }
        }

        public void ReloadPage()
        {
            HttpRequest Request = HttpContext.Current.Request;
            string host = "http://" + Request.Url.Host.ToString();
            string rawUrl = Request.RawUrl.ToString();
            string Url = host + rawUrl;
            Response.Redirect(Url);
        }

        private bool IsUpdate()
        {
            _Code = Request.QueryString["Code"];
            if (_Code != null)
            {
                _IsUpdate = true;
                return true;
            }
            else
            {
                _IsUpdate = false;
                return false;
            }
        }

       
    }
}