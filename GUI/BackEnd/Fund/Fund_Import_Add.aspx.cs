﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Web.Script.Serialization;
using System.Globalization;
using System.Web.Services;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.Fund
{
    public partial class Fund_Import_Add : System.Web.UI.Page
    {
        private string PageID = "";
        private bool Perm_Access = false;

        protected Fund_Import OBJ;
        protected string _Code;
        protected bool _IsUpdate = false;
        protected List<Fund_OffenItem> offenItems = new List<Fund_OffenItem>();
        private CultureInfo culture = new CultureInfo("vi-VN");
        protected List<Fund_OffenItem> listOffenItem = new List<Fund_OffenItem>();

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (!IsPostBack)
            {
                txtDate.Text = String.Format("{0:dd/MM/yyyy}", DateTime.Now);
                using (var db = new Solution_30shineEntities())
                {
                    listOffenItem = db.Fund_OffenItem.Where(w=>w.IsReceipt == true && w.IsDelete != true).OrderBy(o=>o.Order).ToList();
                }                

                if (IsUpdate())
                {
                    if (Bind_OBJ())
                    {
                        //
                        bindAccountType();
                        bindSource();
                        //bindOffenItem();
                    }
                }
                else
                {
                    // 
                    bindAccountType();
                    bindSource();
                    //bindOffenItem();
                }

            }
        }

        protected void ExcAddOrUpdate(object sender, EventArgs e)
        {
            if (IsUpdate())
            {
                Update();
            }
            else
            {
                Add();
            }
        }

        private void Add()
        {
            using (var db = new Solution_30shineEntities())
            {
                int integer;
                var obj = new Fund_Import();
                obj.ImportDate = Convert.ToDateTime(txtDate.Text, culture);
                obj.AccountTypeId = int.TryParse(ddlAccountType.SelectedValue, out integer) ? integer : 0;
                obj.SourceId = int.TryParse(ddlSource.SelectedValue, out integer) ? integer : 0;
                obj.IsReceipt = Convert.ToBoolean(ddlIsReceipt.SelectedValue);
                obj.Note = Note.Text;
                obj.CreatedTime = DateTime.Now;
                obj.IsDelete = false;

                // Validate
                var Error = false;

                if (!Error)
                {
                    db.Fund_Import.Add(obj);
                    var exc = db.SaveChanges();

                    // Cập nhật item thu chi vào bảng Fund_Item_Flow
                    var serialize = new JavaScriptSerializer();
                    var jsonArrayItems = HDF_ReceiptItems.Value;
                    if (jsonArrayItems != "")
                    {
                        var postItem = serialize.Deserialize<List<Library.Class.cls_fund_receipt_item>>(jsonArrayItems);
                        if (postItem.Count > 0)
                        {
                            int totalMoney = 0;
                            foreach (var v in postItem)
                            {
                                var item = new Fund_Item_Flow();
                                item.ImportId = obj.Id;
                                item.ItemId = v.Id;
                                item.ItemTitle = v.Title;
                                item.ItemMoney = v.Money;
                                item.CreatedTime = DateTime.Now;
                                item.IsDelete = false;
                                db.Fund_Item_Flow.AddOrUpdate(item);
                                db.SaveChanges();
                                totalMoney += v.Money;
                            }
                            obj.TotalMoney = totalMoney;
                            db.Fund_Import.AddOrUpdate(obj);
                            db.SaveChanges();
                        }
                    }

                    if (exc > 0)
                    {
                        var MsgParam = new List<KeyValuePair<string, string>>();
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thành công!"));
                        UIHelpers.Redirect("/admin/ngan-sach/thu-chi/" + obj.Id + ".html", MsgParam);
                    }
                    else
                    {
                        var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển.";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                    }
                }
            }
        }

        private void Update()
        {
            using (var db = new Solution_30shineEntities())
            {
                int Id = 0;
                int integer;
                if (int.TryParse(_Code, out Id))
                {
                    OBJ = db.Fund_Import.FirstOrDefault(w => w.Id == Id);

                    if (!OBJ.Equals(null))
                    {
                        OBJ.ImportDate = Convert.ToDateTime(txtDate.Text, culture);
                        OBJ.AccountTypeId = int.TryParse(ddlAccountType.SelectedValue, out integer) ? integer : 0;
                        OBJ.SourceId = int.TryParse(ddlSource.SelectedValue, out integer) ? integer : 0;
                        OBJ.IsReceipt = Convert.ToBoolean(ddlIsReceipt.SelectedValue);
                        OBJ.Note = Note.Text;
                        OBJ.ModifiedTime = DateTime.Now;

                        db.Fund_Import.AddOrUpdate(OBJ);
                        var exc = db.SaveChanges();
                        if (exc > 0)
                        {
                            // Cập nhật item thu chi vào bảng Fund_Item_Flow
                            // 1. Đối chiếu danh sách thu/chi post lên với danh sách từ db, nếu có item nào mới (ko nằm trong danh sách db) thì thêm mới
                            // 2. Đối chiếu danh sách thu/chi từ db với danh sách post lên, nếu có item nào ko nằm trong danh sách post (item này bị xóa) thì xóa item
                            var serialize = new JavaScriptSerializer();                            
                            var item = new Fund_Item_Flow();
                            var index = -1;
                            var jsonArrayItems = HDF_ReceiptItems.Value;
                            var dbItems = db.Fund_Item_Flow.Where(w=>w.ImportId == OBJ.Id && w.IsDelete != true).ToList();
                            var postItem = new List<Library.Class.cls_fund_receipt_item>();

                            // 1. Check thêm mới item                            
                            if (jsonArrayItems != "")
                            {
                                postItem = serialize.Deserialize<List<Library.Class.cls_fund_receipt_item>>(jsonArrayItems);
                            }

                            if (postItem.Count > 0)
                            {
                                int totalMoney = 0;
                                foreach (var v in postItem)
                                {
                                    index = dbItems.FindIndex(w=>w.ItemId == v.Id);
                                    if (index == -1)
                                    {
                                        item = new Fund_Item_Flow();
                                        item.ImportId = OBJ.Id;
                                        item.ItemId = v.Id;
                                        item.ItemTitle = v.Title;
                                        item.ItemMoney = v.Money;
                                        item.CreatedTime = DateTime.Now;
                                        item.IsDelete = false;
                                        db.Fund_Item_Flow.AddOrUpdate(item);
                                        db.SaveChanges();
                                    }                                        
                                    totalMoney += v.Money;
                                }
                                OBJ.TotalMoney = totalMoney;
                                db.Fund_Import.AddOrUpdate(OBJ);
                                db.SaveChanges();
                            }
                            // 2. Check xóa item
                            if (dbItems.Count > 0)
                            {
                                foreach (var v in dbItems)
                                {
                                    index = postItem.FindIndex(w=>w.Id == v.ItemId);
                                    if (index == -1)
                                    {
                                        v.IsDelete = true;
                                        db.Fund_Item_Flow.AddOrUpdate(v);
                                        db.SaveChanges();
                                    }
                                }
                            }

                            var MsgParam = new List<KeyValuePair<string, string>>();
                            MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                            MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thành công!"));
                            UIHelpers.Redirect("/admin/ngan-sach/thu-chi/" + OBJ.Id + ".html", MsgParam);
                        }
                        else
                        {
                            var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển!";
                            var status = "warning";
                            UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                        }
                    }
                    else
                    {
                        var msg = "Lỗi! Bản ghi không tồn tại.";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);

                    }
                }
                else
                {
                    MsgSystem.Text = "Lỗi! Bản ghi không tồn tại.";
                    MsgSystem.CssClass = "msg-system warning";
                }

                Context.RewritePath("+", "", "");
            }
        }

        private bool Bind_OBJ()
        {
            using (var db = new Solution_30shineEntities())
            {
                var ExistOBJ = true;
                int Id;
                if (int.TryParse(_Code, out Id))
                {
                    OBJ = db.Fund_Import.Where(w => w.Id == Id).FirstOrDefault();

                    if (!OBJ.Equals(null))
                    {
                        // Bind ngày nhập liệu
                        txtDate.Text = String.Format("{0:dd/MM/yyyy}", OBJ.ImportDate);

                        // Bind danh sách nội dung thu/chi
                        var listItem = db.Fund_Item_Flow.Where(w => w.ImportId == OBJ.Id && w.IsDelete != true).ToList();
                        rptItemFlow.DataSource = listItem;
                        rptItemFlow.DataBind();
                        if (listItem.Count > 0)
                        {
                            itemReceiptList.Style.Add("display", "block");
                        }                        

                        // Bind Thu/Chi option
                        var itemSelected = ddlIsReceipt.Items.FindByValue(OBJ.IsReceipt.ToString());
                        if (itemSelected != null)
                        {
                            itemSelected.Selected = true;
                        }

                        // Bind Note
                        Note.Text = OBJ.Note;
                    }
                    else
                    {
                        ExistOBJ = false;
                        var msg = "Lỗi! Bản ghi không tồn tại";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                    }
                }
                else
                {
                    ExistOBJ = false;
                    var msg = "Lỗi! Bản ghi không tồn tại";
                    var status = "warning";
                    UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                }

                return ExistOBJ;
            }
        }

        public void bindAccountType()
        {
            using (var db = new Solution_30shineEntities())
            {
                var accountType = db.Fund_AccountType.Where(w => w.IsDelete != true && w.Publish == true).ToList();
                ddlAccountType.DataTextField = "Name";
                ddlAccountType.DataValueField = "Id";
                ddlAccountType.DataSource = accountType;
                ddlAccountType.DataBind();
                if (OBJ != null)
                {
                    var itemSelected = ddlAccountType.Items.FindByValue(OBJ.AccountTypeId.ToString());
                    if (itemSelected != null)
                    {
                        itemSelected.Selected = true;
                    }
                }
            }
        }

        public void bindSource()
        {
            using (var db = new Solution_30shineEntities())
            {
                var source = db.Fund_Source.Where(w => w.IsDelete != true && w.Publish == true).ToList();
                ddlSource.DataTextField = "Name";
                ddlSource.DataValueField = "Id";
                ddlSource.DataSource = source;
                ddlSource.DataBind();
                if (OBJ != null)
                {
                    var itemSelected = ddlSource.Items.FindByValue(OBJ.SourceId.ToString());
                    if (itemSelected != null)
                    {
                        itemSelected.Selected = true;
                    }
                }
            }
        }


        [WebMethod]
        public static string loadOffenReceipt(bool isReceipt)
        {
            using (var db = new Solution_30shineEntities())
            {
                var serializer = new JavaScriptSerializer();
                var Msg = new Msg();
                var list = db.Fund_OffenItem.Where(w => w.IsReceipt == isReceipt && w.IsDelete != true).OrderBy(o=>o.Order).ToList();
                Msg.msg = serializer.Serialize(list);
                Msg.success = true;

                return serializer.Serialize(Msg);
            }
        }

        public void ReloadPage()
        {
            HttpRequest Request = HttpContext.Current.Request;
            string host = "http://" + Request.Url.Host.ToString();
            string rawUrl = Request.RawUrl.ToString();
            string Url = host + rawUrl;
            Response.Redirect(Url);
        }

        private bool IsUpdate()
        {
            _Code = Request.QueryString["Code"];
            if (_Code != null)
            {
                _IsUpdate = true;
                return true;
            }
            else
            {
                _IsUpdate = false;
                return false;
            }
        }

        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //IPermission_V2 permissionModel = new PermissionModel_V2();
                //var staffId = Convert.ToInt32(Session["User_Id"].ToString());
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, staffId);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
    }
}