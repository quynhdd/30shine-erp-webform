﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Fund_Report.aspx.cs" Inherits="_30shine.GUI.BackEnd.Fund.Fund_Report" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
<asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

<style>
    .filter-item.filter-checkbox-list { width: 100%; float: left;}
    .filter-item.filter-checkbox-list .checkbox { width: auto; float: left; margin-top: 10px; padding-left: 13px; }
    .filter-item.filter-checkbox-list label { padding-left: 5px; }
    .filter-item.filter-checkbox-list input[type='checkbox'] { position: relative; top: -1px; }
    table.table-total-report-1, 
    table.table-total-report-1 th, 
    table.table-total-report-1 td {
        border: 1px solid black; padding: 5px;
    }
    .customer-add .table-add td.map-edit { text-align:right; }
</style>

<div class="wp sub-menu">
    <div class="wp960">
        <div class="wp content-wp">
            <ul class="ul-sub-menu" id="subMenu">
                <li>Quản lý ngân sách &nbsp;&#187; Thu/chi &nbsp;&#187; </li>
                <li class="li-listing active"><a href="/admin/ngan-sach/thong-ke.html">Báo cáo dòng tiền</a></li>
                <li class="li-listing"><a href="/admin/ngan-sach/thu-chi/danh-sach.html">Nhật ký thu / chi</a></li>                
                <li class="li-add"><a href="/admin/ngan-sach/thu-chi/them-moi.html">Thêm mới</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="wp customer-add customer-listing be-report">
    <%-- Listing --%>                
    <div class="wp960 content-wp">
        <!-- Filter -->
        <div class="row">
            <div class="filter-item" style="margin-left: 0;">
                <strong class="st-head" style="margin-right: 10px;"><i class="fa fa-clock-o"></i>Thời gian</strong>                
                <div class="datepicker-wp">
                    <asp:TextBox CssClass="txtDateTime st-head" ID="TxtDateTimeFrom" placeholder="Từ ngày"
                        ClientIDMode="Static" runat="server"></asp:TextBox>

                    <strong class="st-head" style="margin-left: 10px; margin-right: 10px;">
                        <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                    </strong>
                    <asp:TextBox CssClass="txtDateTime st-head" ID="TxtDateTimeTo" placeholder="Đến ngày"
                        ClientIDMode="Static" runat="server"></asp:TextBox>
                </div>
            </div>                        

            <asp:Panel ID="ViewData" CssClass="st-head btn-viewdata" Text="Xem dữ liệu" 
                ClientIDMode="Static" onclick="excPaging(1)" runat="server">Xem dữ liệu</asp:Panel>
            <a href="javascript://" onclick="location.href=location.href" class="st-head btn-viewdata">Reset Filter</a>
            <div class="export-wp drop-down-list-wp" style="display:none;">
                <asp:Panel ID="fff" CssClass="st-head btn-viewdata"
                    ClientIDMode="Static" runat="server" onclick="EntryExportExcel('all')">Xuất File Excel</asp:Panel><%--OnClick="Exc_ExportExcel" --%>
                <ul class="ul-drop-down-list" style="display:none;">
                    <li onclick="EntryExportExcel('all')">Tất cả dữ liệu</li>
                    <li onclick="EntryExportExcel('current page')">Trang hiện tại</li>
                </ul>
            </div>

            <div class="filter-item filter-checkbox-list" style="width: 100%; display:none;">
                <asp:Repeater runat="server" ID="rptSource">
                    <ItemTemplate>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" data-id="<%# Eval("Id") %>" onclick="filterSource($(this))"/> <%# Eval("Name") %>
                            </label>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>            
        </div> 
        <!-- End Filter -->  
        <div class="row">
            <strong class="st-head"><i class="fa fa-file-text"></i>Thống kê quỹ thu/chi</strong>
        </div>
        <!-- Row Table Filter -->
        <%--<div class="table-func-panel" style="margin-top: -33px;">
            <div class="table-func-elm">
                <span>Số hàng / Page : </span>
                <div class="table-func-input-wp">
                    <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                    <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                    <ul class="ul-opt-segment">
                        <li data-value="10">10</li>
                        <li data-value="20">20</li>
                        <li data-value="30">30</li>
                        <li data-value="40">40</li>
                        <li data-value="50">50</li>
                        <li data-value="1000000">Tất cả</li>
                    </ul>
                    <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                </div>                
            </div>            
        </div>--%>
        <!-- End Row Table Filter -->
        <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>        
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" ClientIDMode="Static">
            <ContentTemplate>
                <div class="table-wp">
                    <table class="table-add table-listing">
                        <thead>
                            <tr>
                                <th></th>
                                <th></th>
                                <th>Số dư tồn quỹ đầu ngày (kỳ)</th>
                                <th>Tiền thu</th>
                                <th>Tiền chi</th>
                                <th>Số dư tồn quỹ cuối ngày (kỳ)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:Repeater ID="Rpt" runat="server">
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Eval("Name") %></td>
                                        <td><%# Eval("AccountNumber") %></td>
                                        <td><%# String.Format("{0:#,###}",Eval("accountStart")).Replace(',','.') %></td>                                        
                                        <td><%# String.Format("{0:#,###}",Eval("totalReceipt")).Replace(',','.') %></td>
                                        <td><%# String.Format("{0:#,###}",Eval("totalPayment")).Replace(',','.') %></td>
                                        <td><%# String.Format("{0:#,###}",Eval("accountEnd")).Replace(',','.') %></td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>                    
                        </tbody>
                    </table>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
        <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" style="display:none;" />
        <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
        <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_SourceIds" />
    </div> 
    <%-- END Listing --%>
</div>

<script>
    jQuery(document).ready(function () {
        $("#glbAdminContent").addClass("active");
        $("#glbAdminFund").addClass("active");

        //============================
        // Datepicker
        //============================
        $('.txtDateTime').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'vi',
            startDate: '2014/10/10',
            format: 'd/m/Y',
            dateonly: true,
            showHour: false,
            showMinute: false,
            timepicker: false,
            onChangeDateTime: function (dp, $input) { },
            scrollMonth: false,
            scrollTime: false,
            scrollInput: false
        });

        // prevent default enter key
        $('input').keypress(function (event) {

            if (event.keyCode === 10 || event.keyCode === 13)
                event.preventDefault();

        });

        // Load mặc định
        $("#ViewData").click();
    });

    function filterSource(This){
        var arrayIds = [];
        $(".filter-checkbox-list .checkbox input[type='checkbox']").each(function(){
            if($(this).prop("checked")){
                arrayIds.push($(this).attr("data-id"));
            }
        });
        if (arrayIds.length > 0) {
            $("#HDF_SourceIds").val(arrayIds);
        } else {
            $("#HDF_SourceIds").val("");
        }
    }

    //============================
    // Event delete
    //============================
    function del(This, code, name) {
        var code = code || null,
            name = name || null,
            Row = This;
        if (!code) return false;

        // show EBPopup
        $(".confirm-yn").openEBPopup();
        $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn xóa [ " + name + " ] ?");

        $("#EBPopup .yn-yes").bind("click", function () {
            $.ajax({
                type: "POST",
                url: "/GUI/SystemService/Ajax/Del.aspx/Delele_Fund_OffenItem",
                data: '{Code : "' + code + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json", success: function (response) {
                    var mission = JSON.parse(response.d);
                    if (mission.success) {
                        delSuccess();
                        Row.remove();
                    } else {
                        delFailed();
                    }
                },
                failure: function (response) { alert(response.d); }
            });
        });
        $("#EBPopup .yn-no").bind("click", function () {
            autoCloseEBPopup(0);
        });
    }
</script>

</asp:Panel>
</asp:Content>

