﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Fund_Import_Add_V2.aspx.cs" Inherits="_30shine.GUI.BackEnd.Fund.Fund_Import_Add_V2" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="ProductAdd" ContentPlaceHolderID="CtMain" runat="server">
<asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
<asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

<style>
    .list-offen-item { height: 36px; width: 48%; float: left; position:relative; border: 1px solid #ddd; margin-left: 1%; z-index: 2000;}
    .list-offen-item .loi-title { width: 100%; height: 36px; line-height: 36px; text-align: center; float: left; cursor: pointer;}
    .list-offen-item .loi-title.complete-chose-item{ background: #50b347; color: #ffffff; }
    .list-offen-item .list-wrap { width: 100%; float: left; display:none;}
    .list-offen-item .ul-list-offen-item { width: 100%; border: 1px solid #ddd; float: left; background: #fff; max-height: 300px; overflow-y: scroll;}
    .list-offen-item .ul-list-offen-item li { float: left; width: 100%; padding: 5px 10px; border-bottom: 1px solid #ddd; }
    .list-offen-item .ul-list-offen-item li label { float: left; width: 100%; font-weight: normal; line-height: 18px;}
    .list-offen-item .ul-list-offen-item li label input[type="checkbox"] { width: auto; position: relative; top: 2px; margin-right: 4px; }
    .complete-chose-item { font-family: Roboto Condensed Bold; background: #ddd;}

    table#table-item-receipt td.money input[type='text'] { background : #e7e7e7; text-align: right; margin-right: 14px; width: 120px; background: #e7e7e7;}

    .list-source { width: 100%; float: left; padding-top: 3px; }
    .list-source span{ width: auto !important; float: left; margin-left: 7px; margin-top: 7px; border: 1px solid #ddd; padding-left: 7px; padding-right: 7px; height: 26px!important; line-height: 26px!important; background: #e7e7e7; font-size: 13px; }

</style>

<div class="wp sub-menu">
    <div class="wp960">
    <div class="wp960">
        <div class="wp content-wp">
            <ul class="ul-sub-menu" id="subMenu">      
                <li>Quản lý ngân sách &nbsp;&#187; Thu/chi &nbsp;&#187; </li>
                <li class="li-listing"><a href="/admin/ngan-sach/thong-ke.html">Báo cáo dòng tiền</a></li>
                <li class="li-listing"><a href="/admin/ngan-sach/thu-chi/danh-sach.html">Nhật ký thu / chi</a></li>                
                <% if(_IsUpdate){ %>
                    <li class="li-add"><a href="/admin/ngan-sach/thu-chi/them-moi.html">Thêm mới</a></li>                    
                    <li class="li-edit active"><a href="/admin/ngan-sach/thu-chi/<%= _Code %>.html">Cập nhật</a></li>
                <% }else{ %>
                    <li class="li-add active"><a href="/admin/ngan-sach/thu-chi/them-moi.html">Thêm mới</a></li>
                <% } %>
            </ul>
        </div>
    </div>
</div>

<div class="wp customer-add">
    <%-- Add --%>                
    <div class="wp960 content-wp fe-service">
        <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
            <div class="table-wp">
                <table class="table-add admin-product-table-add fe-service-table-add">
                    <tbody>
                        <tr class="title-head">
                            <td><strong>Thông tin nhập thu/chi</strong></td>
                            <td></td>
                        </tr>
                        <tr class="tr-margin" style="height: 20px;"></tr>

                        <tr class="tr-field-ahalf">
                            <td class="col-xs-3 left"><span>Ngày</span></td>
                            <td class="col-xs-9 right">
                                <span class="field-wp">
                                    <asp:TextBox CssClass="txtDateTime st-head" ID="txtDate" placeholder="Chọn ngày"
                                        ClientIDMode="Static" runat="server" style="margin-right: 10px;"></asp:TextBox>
                                </span>
                            </td>
                        </tr>

                        <tr class="tr-field-ahalf">
                            <td class="col-xs-3 left"><span>Tài khoản</span></td>
                            <td class="col-xs-9 right">
                                <span class="field-wp">
                                    <asp:DropDownList ID="ddlAccountType" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                        <asp:RequiredFieldValidator InitialValue="0" 
                                            ID="ValidateAccountType" Display="Dynamic" 
                                            ControlToValidate="ddlAccountType"
                                            runat="server"  Text="Bạn chưa chọn tài khoản!" 
                                            ErrorMessage="Vui lòng chọn tài khoản!"
                                            ForeColor="Red"
                                        CssClass="fb-cover-error">
                                        </asp:RequiredFieldValidator>
                                </span>
                            </td>
                        </tr>

                        <tr class="tr-field-ahalf">
                            <td class="col-xs-3 left"><span>Nguồn</span></td>
                            <td class="col-xs-9 right">
                                <div class="field-wp">
                                    <asp:DropDownList ID="ddlSource" runat="server" ClientIDMode="Static" style="display:none;"></asp:DropDownList>
                                        <asp:RequiredFieldValidator InitialValue="0" 
                                            ID="ValidateSource" Display="Dynamic" 
                                            ControlToValidate="ddlSource"
                                            runat="server"  Text="Bạn chưa chọn nguồn!" 
                                            ErrorMessage="Vui lòng chọn nguồn!"
                                            ForeColor="Red"
                                        CssClass="fb-cover-error">
                                        </asp:RequiredFieldValidator>
                                    <div class="list-offen-item" style="width: 49%; margin-left: 0px;">
                                        <span class="loi-title" onclick="openOffenList($(this));" style="text-align: left; padding-left: 10px;">Chọn nguồn</span>
                                        <div class="list-wrap">
                                            <ul class="ul-list-offen-item" id="listSourceItem">
                                                <% if (listSource != null && listSource.Count > 0)
                                                    { %>
                                                    <% foreach (var v in listSource)
                                                        { %>
                                                <li>
                                                    <label>
                                                        <input type="checkbox" data-id="<%=v.Id %>" data-name="<%=v.Name %>" onclick="pushSourceItem($(this),<%=v.Id%>,'<%=v.Name%>')" <%= listSourceChecked.IndexOf(v.Id) != -1 ? "checked='checked'" : "" %> />
                                                        <%=v.Name %> 
                                                    </label>
                                                </li>
                                                <%} %>
                                                <% } %>
                                            </ul>
                                            <span class="loi-title complete-chose-item" onclick="closeOffenList($(this));">Hoàn tất</span>
                                        </div>                                        
                                    </div>
                                    <div class="list-source" id="listSource">
                                        <% if (listSource.Count > 0)
                                        { %>
                                        <% int loop = 0;
                                            foreach (var v in listSource)
                                            { %>
                                                <% var index = listSourceChecked.IndexOf(v.Id);
                                                    if (index != -1)
                                                    { %>
                                                        <span><%=listSource[loop].Name %></span>
                                                    <%} %>
                                                <% loop++;
                                            } %>
                                        <% } %>
                                    </div>
                                </div>
                            </td>
                        </tr>

                        <tr class="tr-field-ahalf">
                            <td class="col-xs-3 left"><span>Thu / Chi</span></td>
                            <td class="col-xs-9 right">
                                <div class="field-wp">
                                    <asp:DropDownList ID="ddlIsReceipt" runat="server" ClientIDMode="Static" onchange="loadOffenItem($(this).val(), $('#ulListOffen'))">
                                        <asp:ListItem Text="Thu" Value="True"></asp:ListItem>
                                        <asp:ListItem Text="Chi" Value="False"></asp:ListItem>
                                    </asp:DropDownList>
                                    <div class="list-offen-item">
                                        <span class="loi-title" onclick="openOffenList($(this));">Chọn danh mục thu/chi</span>
                                        <div class="list-wrap">
                                            <ul class="ul-list-offen-item" id="ulListOffen">
                                                <% if (listOffenItem != null && listOffenItem.Count > 0)
                                                    { %>
                                                    <% foreach (var v in listOffenItem)
                                                        { %>
                                                <li>
                                                    <label>
                                                        <input type="checkbox" onclick="pushOffenItem($(this),<%=v.Id%>,'<%=v.Title%>')"/>
                                                        <%=v.Title %> 
                                                    </label>
                                                </li>
                                                <%} %>
                                                <% } %>
                                            </ul>
                                            <span class="loi-title complete-chose-item" onclick="closeOffenList($(this));">Hoàn tất</span>
                                        </div>                                        
                                    </div>
                                </div>
                            </td>
                        </tr>

                        <tr class="tr-product">
                            <td class="col-xs-3 left"><span>Nội dung thu chi</span></td>
                            <td class="col-xs-9 right">
                                <div class="row" runat="server">
                                    <%--<asp:Repeater runat="server" ID="rptOffenItem">
                                        <ItemTemplate>
                                            <div class="checkbox cus-no-infor">
                                                <label class="lbl-cus-no-infor">
                                                    <input type="checkbox" data-id="<%# Eval("Id") %>" onclick="pushOffenReceiptItem($(this), <%# Eval("Id") %>, '<%# Eval("Title") %>', <%# Eval("Money") %>)" /> <%# Eval("Title") %>
                                                </label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>--%>

                                    <div class="show-product"  data-item="product" onclick="openAddReceiptFree()"><i class="fa fa-plus-circle"></i>Thêm nội dung thu/chi tự do</div>
                                    <div class="row wrap-add-receipt-item">
                                        <input type="text" class="title" placeholder="Nội dung thu/chi" style="width: 350px;" />
                                        <input type="text" class="money" onkeydown="checkTypingNumeric(event)" onkeyup="formatPrice($(this),event)" placeholder="Chi phí" style="width: 120px; margin-left: 10px;" />
                                        <div class="btn-add-receipt" onclick="addReceiptFree()">Thêm</div>
                                    </div>
                                </div>     
                                <div class="listing-product item-product" id="itemReceiptList" runat="server" ClientIDMode="Static">
                                    <table class="table table-listing-product table-item-product" id="table-item-receipt">
                                        <thead>
                                            <tr>
                                                <th style="width: 60px;">STT</th>
                                                <th>Mục thu/chi</th>
                                                <th style="width: 100px;">Chi phí</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <asp:Repeater ID="rptItemFlow" runat="server">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td class="index" data-id="<%# Eval("ItemId") %>"><%# Container.ItemIndex + 1 %></td>
                                                        <td class="title"><%# Eval("ItemTitle") %></td>
                                                        <td class="money map-edit">
                                                            <input type="text" value="<%# String.Format("{0:#,###}",Eval("ItemMoney")).Replace(',','.') %>" onkeydown="checkTypingNumeric(event)" onkeyup="formatPrice($(this),event)" onblur="updateStoreReceiptItems();" />
                                                            <div class="edit-wp">
                                                                <a class="elm del-btn" onclick="removeReceiptItem($(this).parent().parent().parent(),'<%# Eval("ItemTitle") %>')" href="javascript://" title="Xóa"></a>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </tbody>
                                    </table>
                                </div>
                            </td>
                        </tr>                   

                        <tr class="tr-description">
                            <td class="col-xs-3 left"><span>Ghi chú</span></td>
                            <td class="col-xs-9 right">
                                <span class="field-wp">
                                    <asp:TextBox TextMode="MultiLine" Rows="3" ID="Note" runat="server" ClientIDMode="Static" style="padding: 10px;"></asp:TextBox>
                                </span>
                            </td>
                        </tr>
                    
                        <tr class="tr-send">
                            <td class="col-xs-3 left"></td>
                            <td class="col-xs-9 right no-border">
                                <span class="field-wp">
                                    <asp:Button ID="Send" CssClass="btn-send" runat="server" Text="Hoàn tất" ClientIDMode="Static" OnClick="ExcAddOrUpdate" OnClientClick="addLoading();"></asp:Button>
                                    <%--<span class="btn-send btn-cancel" id="CalcelAddFbcv" style="margin-left: 12px;" >Hủy</span>--%>
                                </span>
                            </td>
                        </tr>
                        <asp:HiddenField runat="server" ID="UrlKeyField"  ClientIDMode="Static"/>
                        <asp:HiddenField runat="server" ID="ImgCoverField"  ClientIDMode="Static"/>
                        <asp:HiddenField runat="server" ID="HDF_ReceiptItems"  ClientIDMode="Static"/>
                        <asp:HiddenField runat="server" ID="HDF_Source"  ClientIDMode="Static"/>
                    </tbody>
                </table>
            </div>
        </div>
    <%-- end Add --%>
</div>

<style>
    /*.customer-add .content-wp { padding: 0 15%;}*/
    .wrap-add-receipt-item { margin-top: 10px; float: left; display:none; }
    .btn-add-receipt { float: left; height: 36px; line-height: 36px; width: 80px; text-align: center; background: red; margin-left: 10px; cursor: pointer; background: #ddd;
        -webkit-transition: 0.1s all ease;
	    -moz-transition: 0.1s all ease;
	    -o-transition: 0.1s all ease;
	    -ms-transition: 0.1s all ease;
	    transition: 0.1s all ease;
    }
    .btn-add-receipt:hover { 
        background: #bababa;
    }
</style>

<script>
    jQuery(document).ready(function () {
        $("#glbAdminContent").addClass("active");
        $("#glbAdminFund").addClass("active");
        //============================
        // Show System Message
        //============================ 
        var qs = getQueryStrings();
        showMsgSystem(qs["msg_update_message"], qs["msg_update_status"]);

        //============================
        // Datepicker
        //============================
        $('.txtDateTime').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'vi',
            startDate: '2014/10/10',
            format: 'd/m/Y',
            dateonly: true,
            showHour: false,
            showMinute: false,
            timepicker: false,
            onChangeDateTime: function (dp, $input) { },
            scrollMonth: false,
            scrollTime: false,
            scrollInput: false
        });

        // Store receipt item
        updateStoreReceiptItems();
    });

    function openOffenList(This){
        This.parent().find(".list-wrap").toggle();
    }

    function closeOffenList(This){
        This.parent().hide();
    }

    function pushOffenItem(This, id, title){
        if(This.prop("checked")){
            if(!isPushItem(id)){
                var index = $("table#table-item-receipt > tbody > tr").length;
                var tr = '<tr>' +
                            '<td class="index" data-id="'+id+'">'+(index + 1)+'</td>' +
                            '<td class="title">'+title+'</td>' +
                            '<td class="money map-edit">' + 
                                '<input type="text" placeholder="Nhập chi phí" onkeydown="checkTypingNumeric(event)" onkeyup="formatPrice($(this),event)" onblur="updateStoreReceiptItems();" />' +
                                '<div class="edit-wp">' +
                                    '<a class="elm del-btn" onclick="removeReceiptItem($(this).parent().parent().parent(),\''+title+'\')" href="javascript://" title="Xóa"></a>' +
                                '</div>' +
                            '</td>' +
                        '</tr>';

                $("table#table-item-receipt > tbody").append(tr);
                $("#itemReceiptList").show();
                updateStoreReceiptItems();
            }else{
                alert("Danh mục đã được chọn.");
            }
        }
    }

    function loadOffenItem(isReceipt, domList){
        $.ajax({
            type: "POST",
            url: "/GUI/BackEnd/Fund/Fund_Import_Add.aspx/loadOffenReceipt",
            data: '{isReceipt : "' + isReceipt + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json", success: function (response) {
                var mission = JSON.parse(response.d);
                if (mission.success) {
                    var list = JSON.parse(mission.msg);
                    var lis = '';
                    if(list.length > 0){                        
                        $.each(list, function(i,v){
                            lis += '<li>' +
                                        '<label>' +
                                            '<input type="checkbox" onclick="pushOffenItem($(this),'+v.Id+',\''+v.Title+'\')" />' +
                                            v.Title +
                                        '</label>' +
                                    '</li>';                            
                        });
                    }
                    domList.empty().append($(lis));
                } else {
                }
            },
            failure: function (response) { alert(response.d); }
        }); 
    }

    function openAddReceiptFree() {
        $(".wrap-add-receipt-item").toggle();
    }

    function addReceiptFree() {
        var title = $(".wrap-add-receipt-item").find("input.title").val();
        var money = $(".wrap-add-receipt-item").find("input.money").val();
        var index = $("table#table-item-receipt > tbody > tr").length;
        
        var tr = '<tr>' +
                    '<td class="index" data-id="0">'+(index + 1)+'</td>' +
                    '<td class="title">'+title+'</td>' +
                    '<td class="money map-edit">' + 
                        '<input type="text" value="'+money+'" placeholder="Nhập chi phí" onkeydown="checkTypingNumeric(event)" onkeyup="formatPrice($(this),event)" onblur="updateStoreReceiptItems();"/>' +
                        '<div class="edit-wp">' +
                            '<a class="elm del-btn" onclick="removeReceiptItem($(this).parent().parent().parent(),\''+title+'\')" href="javascript://" title="Xóa"></a>' +
                        '</div>' +
                    '</td>' +
                '</tr>';

        $("table#table-item-receipt > tbody").append(tr);
        $("#itemReceiptList").show();
        // reset value
        $(".wrap-add-receipt-item").find("input.title").val("");
        $(".wrap-add-receipt-item").find("input.money").val("");
        // update store value
        updateStoreReceiptItems();        
    }

    function removeReceiptItem(This) {
        This.remove();
        updateStoreReceiptItems();
    }

    function pushOffenReceiptItem(This, id, title) {        
        var index = $("table#table-item-receipt > tbody > tr").length;
        var tr = '<tr>' +
                    '<td class="index" data-id="'+id+'">'+(index + 1)+'</td>' +
                    '<td class="title">'+title+'</td>' +
                    '<td class="money map-edit">' + 
                        '<input type="text" placeholder="Nhập chi phí" onkeydown="checkTypingNumeric(event)" onkeyup="formatPrice($(this),event)" onblur="updateStoreReceiptItems();"/>' +
                        '<div class="edit-wp">' +
                            '<a class="elm del-btn" onclick="removeReceiptItem($(this).parent().parent().parent(),\''+title+'\')" href="javascript://" title="Xóa"></a>' +
                        '</div>' +
                    '</td>' +
                '</tr>';

        $("table#table-item-receipt > tbody").append(tr);
        $("#itemReceiptList").show();
        updateStoreReceiptItems();        
    }

    function isPushItem(id){
        var isPush = false;
        $("table#table-item-receipt > tbody tr td.index").each(function(){
            if($(this).attr("data-id") == id){
                isPush = true;
            }
        });
        return isPush;
    }

    function pushSourceItem(This, id, name)
    {
        var sourceIds = [];
        var id,name;
        var strListSource = "";
        $("ul#listSourceItem li").each(function(){
            var input = $(this).find("input[type='checkbox']");
            if(input.prop("checked"))
            {
                id = parseInt(input.attr("data-id"));
                name = input.attr("data-name");
                if(!isNaN(id))
                {
                    sourceIds.push(id);
                    if(name != "")
                    {
                        strListSource += "<span>" + name + "</span>";
                    }                    
                }             
            }
        });
        $("#listSource").empty().append($(strListSource));
        $("#HDF_Source").val(sourceIds);
    }

    function updateStoreReceiptItems() {
        var arrayItems = [];
        var item = {};
        
        $("table#table-item-receipt > tbody > tr").each(function (i, v) {            
            item = {};
            item.Id = $(this).find(".index").attr("data-id");
            item.Title = $(this).find(".title").text();
            var money = parseInt($(this).find(".money").find('input[type="text"]').val().replace(/\./g,''));
            item.Money = !isNaN(money) ? money : 0;
            arrayItems.push(item);
        });
        if (arrayItems.length > 0) {
            $("#HDF_ReceiptItems").val(JSON.stringify(arrayItems));
        }else{
            $("#HDF_ReceiptItems").val("");
        }
    }

    var numeric = [
                   //0  1  2  3  4  5  6  7  8  9
                    48,49,50,51,52,53,54,55,56,57,
                    96,97,98,99,100,101,102,103,104,105,
                    8, // BackSpace
                    46, // Delele
                    110, // Del
                    
                ];
    var arrow = [
                    38, // Arrow Up
                    40, // Arrow Down
                    37, // Arrow Left
                    39 // Arrow Right
                ];
    function checkTypingNumeric(e){
        if(numeric.indexOf(e.keyCode) == -1 && arrow.indexOf(e.keyCode) == -1){
            e.preventDefault();
        }
    }
    function formatPrice(This, e){
        //console.log(e.keyCode);
        if(arrow.indexOf(e.keyCode) == -1){
            This.val(FormatPrice(This.val()));
        }        
    }
</script>

</asp:Panel>
</asp:Content>




