﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Globalization;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.Fund
{
    public partial class Fund_Report : System.Web.UI.Page
    {
        private string PageID = "NS_TK";
        private bool Perm_Access = false;
        private bool Perm_Edit = false;
        private bool Perm_Delete = false;

        protected Paging PAGING = new Paging();        
        protected List<Stored_Fund_Report_Result> FundReport;
        private string sourceIds;
        private int accountTypeId;
        private string whereSource = "";
        private string whereTime = "";
        private DateTime timeFrom;
        private DateTime timeTo;
        private CultureInfo culture = new CultureInfo("vi-VN");

        private Solution_30shineEntities db;
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                //Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                //ExecuteByPermission();

                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        /// <summary>
        /// Constructor
        /// </summary>
        public Fund_Report()
        {
            db = new Solution_30shineEntities();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            checkPermission();

            if (!IsPostBack)
            {
                bindSource();
                TxtDateTimeFrom.Text = String.Format("{0:dd/MM/yyyy}", DateTime.Now);
            }
            else
            {
                //       
            }
        }
        /// <summary>
        /// check permission
        /// </summary>
        protected void checkPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                string perName = Session["User_Permission"].ToString();
                string url = Request.Url.AbsolutePath.ToString();
                Solution_30shineEntities db = new Solution_30shineEntities();
                var pem = (from m in db.Tbl_Permission_Map
                           join mn in db.Tbl_Permission_Menu on m.mID equals mn.mID
                           where m.pName == perName && mn.Url_Main.EndsWith(url) || mn.Url_Rewrite.EndsWith(url)
                           select new { m.aID }
                           ).FirstOrDefault();
                if (pem != null)
                {
                    var pemArray = pem.aID.Split(',');
                    if (Array.IndexOf(pemArray, "1") > -1)
                    {
                        Perm_Access = true;

                        if (Array.IndexOf(pemArray, "4") > -1)
                        {
                            Perm_Edit = true;
                        }
                        if (Array.IndexOf(pemArray, "5") > -1)
                        {
                            Perm_Delete = true;
                        }
                    }
                    else
                    {
                        ContentWrap.Visible = false;
                        NotAllowAccess.Visible = true;
                    }

                }


            }
        }
        public List<Stored_Fund_Report_Result> getTotalReport()
        {
            var report = new List<Stored_Fund_Report_Result>();
            if (TxtDateTimeFrom.Text != "")
            {
                timeFrom = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                if (TxtDateTimeTo.Text != "")
                {
                    timeTo = Convert.ToDateTime(TxtDateTimeTo.Text, culture);
                }
                else
                {
                    timeTo = timeFrom;
                }
                report = db.Stored_Fund_Report(String.Format("{0:yyyy/MM/dd}", timeFrom.AddDays(-1)), 
                                                String.Format("{0:yyyy/MM/dd}", timeFrom), 
                                                String.Format("{0:yyyy/MM/dd}", timeTo)).ToList();                
            }
            return report;
        }

        public void bindSource()
        {
            using (var db = new Solution_30shineEntities())
            {
                var source = db.Fund_Source.Where(w => w.IsDelete != true && w.Publish == true).ToList();
                rptSource.DataSource = source;
                rptSource.DataBind();
            }
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            Rpt.DataSource = getTotalReport();
            Rpt.DataBind();
            RemoveLoading();
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        
    }
}