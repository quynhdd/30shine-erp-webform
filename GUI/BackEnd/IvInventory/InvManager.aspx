﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InvManager.aspx.cs" MasterPageFile="~/TemplateMaster/SiteMaster.Master" Inherits="_30shine.GUI.BackEnd.IvInventory.InvManager" %>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">

    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>

    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <%--<uc1:ReportMenu runat="server" id="ReportMenu" />--%>

        <style>
            .table-wp table th {
                font-family: Roboto Condensed Bold;
                font-weight: normal;
            }

            .form-class {
                border: 1px solid #b4b4b4;
                -moz-border-radius: 5px;
                border-radius: 5px;
                -webkit-border-radius: 5px;
                padding: 20px;
                margin: 20px 20px 20px 20px;
                width: 100%;
            }

            .col-xs-2 {
                line-height: 29px;
            }

            .container-1 {
                float: left;
            }

            #div-table {
                margin: 0px 0px 0px 75px;
                padding: 10px;
                width: 50%;
                /*border:1px solid #b4b4b4;
                -moz-border-radius:5px;
                border-radius:5px;
                -webkit-border-radius:5px;*/
                /*overflow-y: scroll;
                height:500px;*/
            }

            .table-staff tbody {
                display: block;
                max-height: 500px;
                overflow-y: scroll;
            }

            .sw-btn-group, .step-anchor {
                display: none;
            }

            .table-staff thead,
            tbody tr {
                display: table;
                width: 100%;
                table-layout: fixed;
            }


            .table-listing {
                border: 1px solid #bababa !important;
            }

                .table-listing th, .table-listing td {
                    border-left: none !important;
                    border-right: none !important;
                    border-top: none !important;
                    border-bottom: 1px solid #bababa !important;
                }

            .clear {
                clear: both;
            }
            /*.select2-container{
                margin-left:4px;
            }*/
        </style>

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Báo cáo &nbsp;&#187; </li>
                        <li class="be-report-li"><a href="/admin/cung-ung/danh-sach-kho.html"><i class="fa fa-th-large"></i>Quản lý kho</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing be-report">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <!-- Filter -->
                <div class="row">
                    <div class="filter-item" style="margin-left: 0;"></div>
                </div>
                <div class="row">
                    <strong class="st-head"><i class="fa fa-file-text"></i>Tạo mới kho</strong>
                </div>
                <br />
                <div class="row">
                    <div class="container" style="margin: 0 auto; width: 80%">
                        <!-- SmartWizard html -->
                        <div id="smartwizard" class="container-1" style="width: 40%">
                            <ul>
                                <li><a href="#step-1">Bước 1<br />
                                    <small>Tạo mới kho</small></a></li>
                                <li><a href="#step-2">Bước 2<br />
                                    <small>Thêm sản phẩm</small></a></li>
                                <%--<li><a href="#step-3">Step 3<br /><small>This is step description</small></a></li>
                                <li><a href="#step-4">Step 4<br /><small>This is step description</small></a></li>--%>
                            </ul>

                            <div>
                                <div id="step-1" class="">
                                    <h3 class="border-bottom border-gray pb-2" style="margin-left: 20px"><b>Thêm mới Kho</b></h3>
                                    <div class="row form-class">
                                        <div class="row form-group">
                                            <div class="col-xs-3"><b>Loại kho:</b> </div>
                                            <div class="col-xs-9">
                                                <asp:DropDownList ID="InvType" runat="server" CssClass="form-control select" Width="100%">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="row form-group" id="form-name">
                                            <div class="col-xs-3"><b>Tên kho:</b> </div>
                                            <div class="col-xs-9">
                                                <asp:TextBox ID="InvName" runat="server" CssClass="form-control" Style="margin-left: 0px !important;" Width="100%" />
                                            </div>
                                        </div>

                                        <div class="row form-group" id="form-salon">
                                            <div class="col-xs-3"><b>Salon:</b> </div>
                                            <div class="col-xs-9">
                                                <asp:DropDownList ID="Salon" runat="server" CssClass="form-control select" Width="100%">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="row form-group" id="form-inventory">
                                            <div class="col-xs-3"><b>Nhập hàng:</b> </div>
                                            <div class="col-xs-9">
                                                <asp:DropDownList ID="DropDownList1" style="margin:0 auto !important" runat="server" CssClass="form-control" Width="100%">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="row form-group" id="form-province">
                                            <div class="col-xs-3"><b>Tỉnh thành:</b> </div>
                                            <div class="col-xs-9">
                                                <asp:DropDownList ID="DropDownList2" runat="server" CssClass="form-control select" Width="100%">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="row form-group" id="form-staff">
                                            <div class="col-xs-3"><b>Chọn BP:</b> </div>
                                            <div class="col-xs-9">
                                                <asp:DropDownList ID="Department" runat="server" CssClass="form-control select" Width="100%">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="row form-group" id="form-button">
                                            <div class="col-xs-3"></div>
                                            <div class="col-xs-9">
                                                <input type="button" style="width: 150px; margin: 20px 0px; float: right" class="form-control btn btn-info" onclick="GetData()" value="Tạo kho mới" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div id="div-table" class="container-1">
                            <div class="row table-wp">
                                <table class="table-add table-listing table-staff">
                                    <thead>
                                        <tr>
                                            <th><span style="margin-left: -10px !important;">ID</span></th>
                                            <th><span style="margin-left: -10px !important;">Tên</span></th>
                                            <th>
                                                <input type="checkbox" style="margin-left: -30px !important;" class="check-all" /></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <div class="row form-group">
                                <div class="col-xs-3"></div>
                                <div class="col-xs-9">
                                    <input type="button" style="width: 150px; margin: 20px 0px; float: right" class="form-control btn btn-info" onclick="GetData()" value="Tạo kho mới" />
                                </div>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>



                </div>
            </div>
        </div>


        <script>
            $('.select').select2();
        </script>
        <script type="text/javascript">
            var urlInv = "<%= Libraries.AppConstants.URL_API_INVENTORY %>";
            $("#form-staff").attr("style", "display:none");
            $("#form-salon").attr("style", "display:none");
            $("#form-name").attr("style", "display:none");
            $("#form-button").attr("style", "display:none");
            $("#form-province").attr("style", "display:none");
            $("#form-inventory").attr("style", "display:none");
            $("#CtMain_Department").attr("disabled", "disabled");
            //$("#CtMain_DropDownList1").attr("disabled", "disabled");
            //$("#CtMain_DropDownList2").attr("disabled", "disabled");
            $("#div-table").attr("style", "display:none");
            $("#CtMain_InvType").change(function () {
                // 
                if ($(this).val() == 2) {
                    $("#form-staff").attr("style", "display:none");
                    $("#form-salon").attr("style", "display:none");
                    $("#form-name").attr("style", "display:none");
                    $("#form-button").attr("style", "display:none");
                    $("#form-province").removeAttr("style");
                    $("#form-inventory").removeAttr("style");
                    //addLoading();
                    //$.ajax({
                    //    url: urlInv + "/api/inventory/get-salon",
                    //    type: "GET",
                    //    contentType: "application/json; charset=utf-8",
                    //    dataType: "json",
                    //    success: function (response) {
                    //        $(".table-staff tbody").empty();
                    //        $("#div-table").removeAttr("style");
                    //        var tr = "";
                    //        for (var i = 0; i < response.length; i++) {
                    //            tr = "<tr>" +
                    //                "<td class='id'>" + response[i].id + "</td>" +
                    //                "<td>" + response[i].shortName + "</td>" +
                    //                "<td><input type='checkbox' class='checked'/></td>" +
                    //                "</tr>";
                    //            $(".table-staff tbody").append(tr);
                    //        }
                    //        removeLoading();
                    //    }
                    //})
                }
                else if ($(this).val() == 0) {
                    $("#form-staff").attr("style", "display:none");
                    $("#form-salon").attr("style", "display:none");
                    $("#form-name").attr("style", "display:none");
                    $("#form-button").attr("style", "display:none");
                    $("#CtMain_Department").attr("disabled", "disabled");
                    $("#form-province").attr("style", "display:none");
                    $("#form-inventory").attr("style", "display:none");
                }
                else if ($(this).val() == 3) {
                    $("#form-staff").removeAttr("style");
                    $("#form-salon").removeAttr("style");
                    $("#form-province").attr("style", "display:none");
                    $("#form-inventory").attr("style", "display:none");
                    $("#form-name").attr("style", "display:none");
                    $("#form-button").attr("style", "display:none");

                }
                else {
                    $("#form-staff").attr("style", "display:none");
                    $("#form-salon").attr("style", "display:none");
                    $("#form-name").removeAttr("style");
                    $("#div-table").attr("style", "display:none");
                    $("#form-button").removeAttr("style");
                    $("#form-province").attr("style", "display:none");
                    $("#form-inventory").attr("style", "display:none");
                }

            })
            $("#CtMain_DropDownList2").change(function () {
                addLoading();
                $.ajax({
                    url: urlInv + "/api/inventory/get-salon?provinceId=" + $(this).val(),
                    type: "GET",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        $(".table-staff tbody").empty();
                        $("#div-table").removeAttr("style");
                        var tr = "";
                        for (var i = 0; i < response.length; i++) {
                            tr = "<tr>" +
                                "<td class='id'>" + response[i].id + "</td>" +
                                "<td>" + response[i].shortName + "</td>" +
                                "<td><input type='checkbox' class='checked'/></td>" +
                                "</tr>";
                            $(".table-staff tbody").append(tr);
                        }
                        removeLoading();
                    }
                })
            })
            // check all checkbox when click check all
            $(".check-all").click(function () {
                $(".checked").not(this).prop('checked', this.checked);
            });

            //onchange dropdownlist salon
            $("#CtMain_Salon").change(function () {

                if ($(this).val() == 0) {
                    alert("Vui lòng chọn salon");
                    return false;
                }
                $("#CtMain_Department").removeAttr("disabled");
                if ($("#CtMain_Department").val() > 0) {
                    var de = $("#CtMain_Department").val();
                    var salonId = $("#CtMain_Salon").val();
                    addLoading();
                    $.ajax({
                        url: urlInv + "/api/inventory/get-staff?staffType=" + de + "&salonId=" + salonId,
                        type: "GET",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            $(".table-staff tbody").empty();
                            $("#div-table").removeAttr("style");
                            var tr = "";
                            for (var i = 0; i < response.length; i++) {
                                tr = "<tr>" +
                                    "<td class='id'>" + response[i].id + "</td>" +
                                    "<td>" + response[i].fullname + "</td>" +
                                    "<td><input type='checkbox' class='checked'/></td>" +
                                    "</tr>";
                                $(".table-staff tbody").append(tr);
                            }
                            removeLoading();
                        }
                    })
                }
            })
            function GetData() {
                var table = $(".table-staff tbody");
                var list = new Array();
                var name = "";
                var staffId = 0;
                var salonId = 0;
                var salonName = "";
                var de = $("#CtMain_Department").val();
                var pr = $("#CtMain_DropDownList1").val();
                var invType = parseInt($("#CtMain_InvType").val());
                var length = $('.table-staff tbody tr td').find('input[type="checkbox"]:checked').length;
                //check loại kho
                debugger;
                switch (invType) {
                    //kho salon, kho nv
                    case 2:
                    case 3:
                        if (length == 0) {
                            ShowMessage("Lỗi", "Vui lòng chọn salon/ nhân viên để thêm mới", 4);
                            return false;
                        }
                        if (pr == 0 && invType == 2) {
                            ShowMessage("Lỗi", "Vui lòng chọn kho xuất hàng cho các kho salon này", 4);
                            $("#CtMain_DropDownList1").focus();
                            $("#CtMain_DropDownList1").css("border-color", "red");
                            return false;
                        }
                        $('.table-staff tbody tr td').find('input[type="checkbox"]:checked').each(function () {
                            var row = $(this).closest("tr");
                            if (de > 0) {
                                staffId = row.find(".id").text();
                                salonName = row.find("td").eq(1).text();
                            }
                            else {

                                salonId = row.find(".id").text();
                                salonName = row.find("td").eq(1).text();

                            }
                            list.push({
                                "Name": (staffId == 0 && salonId != 0) ? "Kho " + salonName : "Kho NV " + staffId + " - " + salonName,
                                "StaffId": staffId,
                                "SalonId": salonId,
                                "Type": invType,
                                "ProvinceId": (staffId == 0 && salonId != 0) ? pr : 0,
                            });

                        });
                        break;
                    //kho tổng, kho ncc
                    case 1: case 4:
                        if ($("#CtMain_InvName").val() == '') {
                            ShowMessage("Lỗi", "Tên kho không được để trống", 4)
                            $("#CtMain_InvName").focus();
                            $("#CtMain_InvName").css('border-color', 'red');
                            return false;
                        }
                        list.push({
                            "Name": $("#CtMain_InvName").val(),
                            "StaffId": 0,
                            "SalonId": 0,
                            "Type": invType,
                            "ProvinceId": 0
                        });
                        break;
                    default:
                        break;
                }
                //call api with ajax
                var data = JSON.stringify(list);
                addLoading();
                $.ajax({
                    url: urlInv + "/api/inventory",
                    type: "POST",
                    data: data,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        console.log(response);
                        removeLoading();
                        ShowMessage("Thành công", response, 2);
                    },
                    error: function (response) {
                        removeLoading();
                        ShowMessage("Lỗi", response, 4);
                    }
                })

            }
            $(document).ready(function () {

                // Step show event
                $("#smartwizard").on("showStep", function (e, anchorObject, stepNumber, stepDirection, stepPosition) {
                    //alert("You are on step "+stepNumber+" now");
                    if (stepPosition === 'first') {
                        $("#prev-btn").addClass('disabled');
                    } else if (stepPosition === 'final') {
                        $("#next-btn").addClass('disabled');
                    } else {
                        $("#prev-btn").removeClass('disabled');
                        $("#next-btn").removeClass('disabled');
                    }
                });

                // Toolbar extra buttons
                var btnFinish = $('<button></button>').text('Finish')
                    .addClass('btn btn-info')
                    .on('click', function () { alert('Finish Clicked'); });
                var btnCancel = $('<button></button>').text('Cancel')
                    .addClass('btn btn-danger')
                    .on('click', function () { $('#smartwizard').smartWizard("reset"); });


                // Smart Wizard
                $('#smartwizard').smartWizard({
                    selected: 0,
                    theme: 'circles',
                    transitionEffect: 'fade',
                    showStepURLhash: false,
                    toolbarSettings: {
                        //toolbarPosition: 'end',
                        //toolbarButtonPosition: 'end',
                        //toolbarExtraButtons: [btnFinish, btnCancel]
                    }
                });


                // External Button Events
                $("#reset-btn").on("click", function () {
                    // Reset wizard
                    $('#smartwizard').smartWizard("reset");
                    return true;
                });

                $("#prev-btn").on("click", function () {
                    // Navigate previous
                    $('#smartwizard').smartWizard("prev");
                    return true;
                });

                $("#next-btn").on("click", function () {
                    // Navigate next
                    $('#smartwizard').smartWizard("next");
                    return true;
                });

            });

            $("#CtMain_Department").on("change", function () {
                var de = $(this).val();
                var salonId = $("#CtMain_Salon").val();
                if (salonId == 0) {
                    alert("Vui lòng chọn salon");
                    return false;
                }
                addLoading();
                $.ajax({
                    url: urlInv + "/api/inventory/get-staff?staffType=" + de + "&salonId=" + salonId,
                    type: "GET",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        //console.log(response);
                        $(".table-staff tbody").empty();
                        $("#div-table").removeAttr("style");
                        var tr = "";
                        for (var i = 0; i < response.length; i++) {
                            tr = "<tr>" +
                                "<td class='id'>" + response[i].id + "</td>" +
                                "<td>" + response[i].fullname + "</td>" +
                                "<td><input type='checkbox' class='checked'/></td>" +
                                "</tr>";
                            $(".table-staff tbody").append(tr);
                        }
                        //console.log(tr);
                        //$(".table-staff").empty().append($(".table-staff tbody"));
                        removeLoading();
                    }
                })
            });


        </script>

    </asp:Panel>
</asp:Content>
