﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.IvInventory
{
    public partial class ReportsInventory : System.Web.UI.Page
    {
        private bool Perm_Access = false;
        public bool Perm_ViewAllData = false;
        private static bool ViewAllData = false;
        public bool Perm_ShowSalon { get; private set; }
        public int staffId = 0;
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                pageId = permissionModel.GetRegexPageIdSpecial().Replace(pageId, ".html");
                staffId = int.TryParse(Session["User_Id"].ToString(), out var integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_ShowSalon = permissionModel.CheckPermisionByAction("Perm_ShowSalon", pageId, staffId);
                ViewAllData = Perm_ViewAllData;
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                SetPermission();

                if (!IsPostBack)
                {
                    Library.Function.bindInventoryType(new List<DropDownList>() { ddlInventoryType }, Perm_ViewAllData);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [WebMethod]
        public static object GetListInventory(int inventoryType)
        {
            try
            {
                //BindInventory ;
                using (var db = new Solution_30shineEntities())
                {
                    var inventoryTypeId = inventoryType;
                    var listInventory = new List<MODEL.ENTITY.EDMX.IvInventory>();
                    var staffId = int.TryParse(HttpContext.Current.Session["User_Id"].ToString(), out var integer) ? integer : 0;
                    var salonId = db.Staffs.Where(r => r.Id == staffId
                                                    && r.IsDelete == 0
                                                    && r.Active == 1)
                                                    .Select(r => r.SalonId ?? 0).FirstOrDefault();

                    if (ViewAllData)
                    {
                        listInventory = db.IvInventories.Where(r => r.Type == inventoryTypeId && r.IsDelete == false).ToList();
                    }
                    else if (inventoryTypeId == 2)//kho salon
                    {
                        listInventory = (from a in db.IvInventories
                                         join b in db.PermissionSalonAreas on a.SalonId equals b.SalonId
                                         join c in db.IvInventories on b.SalonId equals c.SalonId
                                         where b.StaffId == staffId
                                                && a.IsDelete == false
                                                && b.IsDelete == false
                                                && a.Type == inventoryTypeId
                                         select a).ToList();

                        var InventoryRoot = (from a in db.IvInventories
                                             where a.SalonId == salonId
                                             select a).ToList();

                        listInventory = listInventory.Union(InventoryRoot).ToList();
                    }
                    else if (inventoryTypeId == 3)// kho nhan vien 
                    {
                        listInventory = (from a in db.IvInventories
                                         join b in db.Staffs on a.SalonId equals b.SalonId
                                         join d in db.IvInventories on b.Id equals d.StaffId
                                         join c in db.PermissionSalonAreas on b.SalonId equals c.SalonId
                                         where a.IsDelete == false
                                            && b.IsDelete == 0
                                            && b.Active == 1
                                            && c.IsDelete == false
                                            && c.StaffId == staffId
                                            && b.SalonId == salonId
                                            && a.Type == inventoryTypeId
                                         select d).ToList();

                        var InventoryRoot = (from a in db.IvInventories
                                             join b in db.Staffs on a.SalonId equals b.SalonId
                                             join c in db.IvInventories on b.Id equals c.StaffId
                                             where a.IsDelete == false
                                             && b.IsDelete == 0
                                             && b.Active == 1
                                             && b.SalonId == salonId
                                             select c).ToList();

                        listInventory = listInventory.Union(InventoryRoot).ToList();
                    }

                    return listInventory;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [WebMethod]
        public static object GetDataReporst()
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var list = db.IvInventoryCurrents.ToList();
                    return list;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// CheckViewPriceProduct
        /// </summary>
        private void CheckViewPriceProduct()
        {
            var arrayPermision = Session["User_Permission"].ToString().Split(',');
            if (arrayPermision.Length > 0)
            {
                string checkAdmin = Array.Find(arrayPermision, s => s.Equals("ADMIN"));
                string checkRoot = Array.Find(arrayPermision, s => s.Equals("root"));
                string checkSUPPLY_CHAIN_ADMIN = Array.Find(arrayPermision, s => s.Equals("SUPPLY_CHAIN_ADMIN"));
                string checkACCOUNTANT_ADMIN = Array.Find(arrayPermision, s => s.Equals("ACCOUNTANT_ADMIN"));
                if (!String.IsNullOrEmpty(checkRoot) || !String.IsNullOrEmpty(checkAdmin) || !String.IsNullOrEmpty(checkSUPPLY_CHAIN_ADMIN) || !String.IsNullOrEmpty(checkACCOUNTANT_ADMIN))
                {
                    Perm_ViewAllData = true;
                }
            }
        }
    }
}