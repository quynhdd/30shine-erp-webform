﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InvListing.aspx.cs" MasterPageFile="~/TemplateMaster/SiteMaster.Master" Inherits="_30shine.GUI.BackEnd.IvInventory.InvListing" %>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">

    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>

    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <%--<uc1:ReportMenu runat="server" id="ReportMenu" />--%>
        <style>
            .table-wp table thead th {
                font-weight: normal;
                font-family: Roboto Condensed Bold;
            }
            .btn-add{
                background:none;
                border:none;
            }
            #table-product tbody {
              display: block;
              max-height: 500px;
              overflow-y: scroll;
            }
            #table-product thead,
            #table-product tbody tr {
              display: table;
              width: 100%;
              table-layout: fixed;
            }
            #table-product thead tr td,#table-product tbody tr td{
                border:1px solid #ccc;
                padding-left:10px;
            }
            .btn-action{
               
                float:left !important;
                /*margin: 0 auto;*/
                padding-left: 10px;
            }
        </style>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý kho &nbsp;&#187; </li>
                        <li class="be-report-li"><a href="javascript:void(0);"><i class="fa fa-th-large"></i>Danh sách kho</a></li>
                        <li class="li-add"><a href="/admin/cung-ung/quan-ly-kho.html">Thêm mới</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing be-report" onload="abc()">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <!-- Filter -->
                <div class="row">
                    <div class="filter-item">
                        <asp:DropDownList ID="InvType" CssClass="form-control select" runat="server" ClientIDMode="Static" Style="width: 190px;"></asp:DropDownList>
                    </div>
                    <asp:Panel ID="ViewData" CssClass="st-head btn-viewdata" Text="Xem dữ liệu"
                        ClientIDMode="Static" onclick="excPaging(1)" runat="server">
                        Xem dữ liệu
                    </asp:Panel>

                </div>
                <div class="row">
                    <strong class="st-head"><i class="fa fa-file-text"></i>DANH SÁCH KHO</strong>
                </div>
                 <!-- Row Table Filter -->
                <asp:ScriptManager ID="SM01" runat="server"></asp:ScriptManager>
                <div class="table-func-panel" style="margin-top: -35px;">
                    
                    <div class="table-func-elm">
                        <span>Số hàng / Page : </span>
                        <div class="table-func-input-wp">
                            <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                            <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                            <ul class="ul-opt-segment">
                                <li data-value="10">10</li>
                                <li data-value="20">20</li>
                                <li data-value="30">30</li>
                                <li data-value="40">40</li>
                                <li data-value="50">50</li>
                                <li data-value="1000000">Tất cả</li>
                            </ul>
                            <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                        </div>
                    </div>
                </div>
                <!-- End Row Table Filter -->
                <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                    <ContentTemplate>
                        <div class="row table-wp">
                            <table class="table-add table-listing">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Tên kho</th>
                                        <th>Loại kho</th>
                                        <th>Salon ID</th>
                                        <th>Staff ID</th>
                                        <th>Ngày tạo</th>
                                        <th>Hành động</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="rptDanhsach" runat="server">
                                        <ItemTemplate>
                                            <tr data-id ="<%# Eval("Id") %>">
                                                <td><%# Container.ItemIndex + 1 + PAGING._Offset %></td>
                                                <td><%# Eval("Name") %></td>
                                                <td><%# Eval("TypeName") %></td>
                                                <td><%# Eval("SalonId") %></td>
                                                <td><%# Eval("StaffId") %></td>
                                                <td><%# Eval("CreatedDate") %></td>
                                                <td>
                                                    <asp:Panel runat="server" CssClass="btn-action" style="width:50%" ID="EAadd">
                                                           <a href="#" onclick=" AddOnclick($(this))"  <%# Convert.ToInt32(Eval("Type").ToString()) == 1 ? "" : "style='display:none'" %> data-type="<%# Eval("Type")%>" data-id="<%# Eval("Id") %>" data-name="<%# Eval("Name") %>" data-toggle="modal" data-target="#myModal" title="Thêm SP"><i class="fa fa-plus" style="float:right"></i></a>
                                                        </asp:Panel>
                                                    <asp:Panel runat="server" CssClass="btn-action" style="width:40%" ID="EAdelete" Visible="true">
                                                           <a  href="#"  data-id="<%# Eval("Id") %>" onclick="Delete($(this))" data-name="<%# Eval("Name") %>" title="Xóa"><i class="fa fa-trash" style="float:left"></i></a>
                                                        </asp:Panel>
                                                </td>

                                                    

                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>
                        <!-- Paging -->
                        <div class="site-paging-wp">
                            <% if (PAGING.TotalPage > 1)
                                { %>
                            <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                                <% if (PAGING._Paging.Prev != 0)
                                    { %>
                                <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                                <% } %>
                                <asp:Repeater ID="RptPaging" runat="server">
                                    <ItemTemplate>
                                        <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                            <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>>
                                            <%# Eval("PageNum") %>
                                        </a>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                                    { %>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                                <% } %>
                            </asp:Panel>
                            <% } %>
                        </div>
                        <!-- End Paging -->
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HiddenField1" />

                <asp:HiddenField runat="server" ClientIDMode="Static" ID="IsPaging" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
            </div>
            <%-- END Listing --%>
        </div>
        <!-- Modal -->
        <div id="myModal" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><b>DANH SÁCH SẢN PHẨM</b></h4>
              </div>
              <div class="modal-body">
                  <input type="hidden" id="wareId"/>
                  <input type="hidden" id="wareName"/>
                <div class="row">
                    <div class="filter-item">
                        <asp:DropDownList ID="ddlCategory" CssClass="form-control select" runat="server" ClientIDMode="Static" Style="width: 190px;"></asp:DropDownList>
                    </div>
                </div>
                <div class="row">
                    <div class="row table-wp" style="margin-top:30px;">
                            <table class="table-add table-listing" id="table-product" style ="width:100%">
                                <thead>
                                    <tr>
                                        <th style="width:10%">ID</th>
                                        <th style="width:40%">Tên SP</th>
                                        <th style="width:20%">BarCode</th>
                                        <th >Giá SP</th>
                                        <th>Số lượng</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                     </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="form-control btn btn-info" onclick="ImportCurrent()" >Xác nhận</button>
              </div>
            </div>

          </div>
        </div>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 190px !important;
                margin-top: 5px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }
        </style>
        <script type="text/javascript">
            var urlInv = "<%= Libraries.AppConstants.URL_API_INVENTORY %>";

            $('#myModal').on('hidden.bs.modal', function (e) {
                $("#table-product tbody").empty();
                $("#CtMain_ddlCategory").val(0);

            })
            jQuery(document).ready(function () {

                // Add active menu
                $("#glbAdminSales").addClass("active");
                $("#glbAdminReportSales").addClass("active");
                $("li.be-report-li").addClass("active");

                // Price format
                UP_FormatPrice('.be-report-price');

                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) {
                        //$input.val($input.val().split(' ')[0]);
                    }
                });

                // View data today
                //$(".tag-date-today").click();
            });


            function Delete(This) {
                var id = This.attr("data-id");
                var name = This.attr("data-name");
                var c = confirm("Bạn chắc chắn muốn xóa " + name);
                if (c) {
                    $.ajax({
                        url: urlInv + "/api/inventory?ivId="+id,
                        type: "DELETE",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            removeLoading();
                            This.closest("tr").remove();
                            ShowMessage("Thành công", response + " " + name, 2);
                        },
                        error: function (response) {
                            
                            ShowMessage("Lỗi", response, 4);
                        }
                       })
                    
                }
                else {
                    return false;
                }
            }
            //add value to input hidden in modal
            function AddOnclick(This) {
                $("#wareId").val(This.attr("data-id"));
                $("#wareName").val(This.attr("data-name"));
            }

            // fill table product data
            $("#ddlCategory").on("change", function () {
                
                var cateId = $(this).val();
                var invId = $("#wareId").val();
                if (cateId == 0) {
                    $("#table-product tbody").empty();
                    return false;
                }
                addLoading();
                $.ajax({
                    type: "GET",
                    url: urlInv + "/api/inventory/get-product?cateId=" + cateId + "&invId="+invId,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        $("#table-product tbody").empty();
                        var tr = "";
                        for (var i = 0; i < response.length; i++) {
                            tr += "<tr >" +
                                "<td style='width:10%'>" + response[i].id + "</td>" +
                                "<td style='width:40%'>" + response[i].name + "</td>" +
                                "<td style='width:20%'>" + (response[i].barCode === null ? "KHÔNG" : response[i].barCode) + "</td>" +
                                "<td >" + (response[i].cost === null ? 0 : response[i].cost) + "</td>" +
                                "<td><input type='text' class='form-control quantity' style='width:80%;margin:5px;' value='"+ (response[i].quantity == 0 ? 0 : response[i].quantity) +"'/></td>" +
                                "</tr>";
                        }
                        $("#table-product tbody").append(tr);
                        removeLoading();
                    }
                })
            });

            //import product to inventory current
            function ImportCurrent() {
                var list = new Array();
                var wareId = $("#wareId").val();
                var wareName = $("#wareName").val();
                $('#table-product tbody tr td').find('.quantity').each(function () {
                    var row = $(this).closest("tr");
                    if ($(this).val() > 0) {
                        list.push({
                            "invId": wareId,
                            "invName": wareName,
                            "productId": row.find("td").eq(0).text(),
                            "proName": row.find("td").eq(1).text(),
                            "cost": row.find("td").eq(3).text(),
                            "begin": $(this).val(),
                        });
                    }
                });
                var data = JSON.stringify(list);
                console.log(list);
                addLoading();
               $.ajax({
                   url: urlInv + "/api/inventory/insert-current",
                   type: "POST",
                   data: data,
                   contentType: "application/json; charset=utf-8",
                   dataType: "json",
                   success: function (response) {
                       $("#myModal").modal("hide");
                       //console.log(response);
                       removeLoading();
                       ShowMessage("Thành công", response, 2);
                   },
                   error: function (response) {
                       removeLoading();
                       ShowMessage("Lỗi", response, 4);
                   }
               })
            }
        </script>

    </asp:Panel>
</asp:Content>