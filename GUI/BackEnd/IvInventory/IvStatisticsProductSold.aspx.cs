﻿using _30shine.Helpers;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Web.Script.Serialization;
using Libraries;

namespace _30shine.GUI.BackEnd.IvInventory
{
    public partial class IvStatisticsProductSold : System.Web.UI.Page
    {
        private bool Perm_Access = false;
        private bool Perm_ViewAllData = false;
        public bool Perm_ShowSalon { get; private set; }
        protected int Id;
        public int staffId = 0;
        protected static string ListProduct;
        protected List<MODEL.ENTITY.EDMX.IvInventory> listInventorySalon = new List<MODEL.ENTITY.EDMX.IvInventory>();
        protected string yesterday = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-1));

        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                string pageId = permissionModel.GetRegexPageIdSpecial().Replace(Request.RawUrl, ".html");
                staffId = int.TryParse(Session["User_Id"].ToString(), out var integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_ShowSalon = permissionModel.CheckPermisionByAction("Perm_ShowSalon", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {

                Id = getId();

                TxtDateTimeFrom.Text = String.Format("{0:dd/MM/yyyy}", yesterday);
                TxtDateTimeTo.Text = String.Format("{0:dd/MM/yyyy}", DateTime.Today);

                //ListProduct = getProducts();
                //getServices();
                //getDepartments();
                // Bind kho Salon
                //BindDdlInventorySalon();

                //BindOBJ();
            }
        }

        //protected void _BtnClick(object sender, EventArgs e)
        //{
        //    BindOBJ();
        //    RemoveLoading();
        //}


        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }
        /// <summary>
        /// get Id của config
        /// </summary>
        /// <returns></returns>
        public int getId()
        {
            //return int.TryParse(Request.QueryString["Id"], out var integer) ? integer : 0;
            return int.TryParse(Session["Id"] == null? "0": Session["Id"].ToString(), out var integer) ? integer : 0;             
        }

        private void setID(string ID)
        {
            Session["Id"] = ID;
        }



        protected void FuncComplete(object sender, EventArgs e)
        {
            try
            {
                string productIds = ""; ;
                var serialize = new JavaScriptSerializer();
                var origin = new Origin();

                origin.InventoryId = HDF_InventoryId.Value;
                productIds = HDF_ProductIds.Value != "[]" ? HDF_ProductIds.Value : "";

                if (!String.IsNullOrEmpty(productIds))
                {
                    origin.listOrderNum = serialize.Deserialize<List<OrderNumItem>>(productIds).ToList();
                    var result = new HttpClient().PutAsJsonAsync(Libraries.AppConstants.URL_API_INVENTORY + "/api/orderNum/update", origin).Result;
                    if (result.IsSuccessStatusCode)
                    {
                        setID(origin.InventoryId);
                        var MsgParam = new List<KeyValuePair<string, string>>();
                        MsgParam.Add(new KeyValuePair<string, string>("msg_status", "2"));
                        MsgParam.Add(new KeyValuePair<string, string>("msg_message", "Cập nhật thành công!"));
                        //thanh cong   
                        //neu dua ve trang danh sach
                        UIHelpers.Redirect("/admin/hang-hoa/thu-tu-hang-trong-kho.html", MsgParam);
                    }
                    else
                    {
                        TriggerJsMsgSystem_temp(this, "Cập nhật thất bại!", 4);
                    }
                }
            }
            catch (Exception ex)
            {
                TriggerJsMsgSystem_temp(this, "Đã xảy ra lỗi!", 4);
                new _30shine.Helpers.PushNotiErrorToSlack().PushDefault(ex.Message + ", MAIN: " + ex.StackTrace, this.ToString() + ".updateOrderNum", "");                
            }
        }

        public void TriggerJsMsgSystem_temp(Page _OBJ, string msg, int status)
        {
            ScriptManager.RegisterStartupScript(_OBJ, _OBJ.GetType(), "Message System", "ShowMessage('Thông báo','" + msg + "'," + status + ");", true);
        }



        /// <summary>
        /// Lấy danh sách product
        /// </summary>
        /// <returns></returns>
        private string getProducts()
        {
            using (var db = new Solution_30shineEntities())
            {
                var serialize = new JavaScriptSerializer();
                var data = db.Products.Where(w => w.IsDelete == 0 && w.Publish == 1).ToList();
                if (data.Any())
                {
                    return serialize.Serialize(data);
                }
                return serialize.Serialize(new List<Product>());
            }
        }


        //private void BindDdlInventorySalon()
        //{
        //    try
        //    {
        //        var SalonId = Convert.ToInt32(HttpContext.Current.Session["SalonId"]);
        //        var UserId = Convert.ToInt32(HttpContext.Current.Session["User_Id"]);
        //        using (var db = new Solution_30shineEntities())
        //        {
        //            //var salonCurrent = db.Staffs.Where(r => r.Id == UserId).Select(r => new { Id = r.SalonId ?? 0 });

        //            //var salonArea = db.PermissionSalonAreas.Where(r => ((r.StaffId == UserId && UserId > 0) || UserId == 0) &&
        //            //                                                   r.IsDelete == false &&
        //            //                                                   r.IsActive == true)
        //            //                                                   .Select(r => new { Id = r.SalonId ?? 0 });
        //            listInventorySalon = new List<MODEL.ENTITY.EDMX.IvInventory>();
        //            if (Perm_ShowSalon)
        //            {
        //                listInventorySalon = (from a in db.IvInventories
        //                                      where a.IsDelete == false && (a.Type == 1 || a.Type == 4)
        //                                      select a).ToList();
        //            }
        //            else
        //            {
        //                //var listSalonId = salonCurrent.Union(salonArea).Where(r => r.Id != 0).Select(r => r);

        //                //listInventorySalon = (from a in db.IvInventories
        //                //                      join b in listSalonId on a.SalonId equals b.Id
        //                //                      where a.IsDelete == false && a.Type == 2
        //                //                      select a).ToList();
        //            }
        //            var ddls = new List<DropDownList>() { ddlInventory };
        //            for (var i = 0; i < ddls.Count; i++)
        //            {
        //                ddls[i].DataTextField = "Name";
        //                ddls[i].DataValueField = "Id";
        //                ddls[i].DataSource = listInventorySalon;
        //                ddls[i].DataBind();
        //            }
        //            ddlInventory.Items.Insert(0, new ListItem("Click Chọn", "0"));
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        throw e;
        //    }
        //}

        public partial class Origin
        {
            public string InventoryId { get; set; }
            public List<OrderNumItem> listOrderNum { get; set; }
        }

        public partial class OrderNumItem
        {
            public int ProductId { get; set; }
            public int OrderNum { get; set; }
        }


    }
}