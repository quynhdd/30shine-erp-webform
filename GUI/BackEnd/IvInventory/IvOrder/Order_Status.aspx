﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Order_Status.aspx.cs" Inherits="Project.GUI.BackEnd.IvInventory.IvOrder.Order_Status" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
    <title>Quản lý kho - Lập đơn đặt hàng</title>
    <link href="/Assets/css/bootstrap/bootstrap-toggle.min.css" rel="stylesheet" />
    <script src="/Assets/js/bootstrap/bootstrap-toggle.min.js"></script>
    <script src="/Assets/js/common.js"></script>
    <link rel="stylesheet" href="/Assets/izitoast/css/iziToast.min.css" />
    <script src="/Assets/izitoast/js/iziToast.min.js" type="text/javascript"></script>

    <script src="/Assets/libs/magicsuggest-master/magicsuggest.js"></script>
    <link href="/Assets/libs/magicsuggest-master/magicsuggest.css" rel="stylesheet" />
    <link href="/Assets/css/erp.30shine.com/SemanticUI/dist/semantic.min.css" rel="stylesheet" />
    <script src="/Assets/css/erp.30shine.com/SemanticUI/dist/semantic.min.js"></script>
    <script src="/Assets/js/jquery.scannerdetection.js"></script>


    <link rel="stylesheet" href="/Assets/multistep/css/reset.css" />
    <!-- CSS reset -->
    <link rel="stylesheet" href="/Assets/multistep/css/style.css" />
    <!-- Resource style -->
    <script src="/Assets/multistep/js/modernizr.js"></script>
    <!-- Modernizr -->

    <style>
        .iziToast-overlay{
            z-index:1011 !important;
        }
        .iziToast-wrapper{
            z-index:1012 !important;
        }
        .fe-service .tr-product .table-listing-product tbody input.product-quantity {
            border-bottom: 1px solid #ddd !important;
        }

        .suggestion-wrap {
            width: 420px;
            float: left;
        }

        .ms-sel-ctn input[type='text'] {
            height: auto !important;
            line-height: 24px !important;
            border: none !important;
        }

        .ms-ctn .ms-sel-item {
            line-height: 22px;
            color: #000000;
            font-size: 13px;
        }

        .ms-helper {
            display: none !important;
        }

        .input-quantity-wrap {
            float: left;
            margin-left: 10px;
        }

        .input-quantity {
            width: 90px;
            line-height: 36px;
            padding: 0 10px;
            border: 1px solid #ddd;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
        }

        .btn-add {
            float: left;
            background: #ddd;
            padding: 9px 15px;
            margin-left: 10px;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            cursor: pointer;
        }

        #success-alert {
            top: 100px;
            right: 10px;
            position: fixed;
            width: 20% !important;
            z-index: 2000;
        }

        #upload-image {
            margin: 0 auto;
            margin-top: 20px;
            border: 2px #cccccc;
            border-style: dashed;
            width: 100%;
            height: 250px;
            border-radius: 3px;
            background-color: ghostwhite;
            /*text-align: center;*/
        }

            #upload-image #result div {
                max-height: 100%;
                float: left;
                display: inline-block;
            }

                #upload-image #result div .close-img span {
                    width: 25px;
                    height: 20px;
                    border: 1px solid #ccc;
                    text-align: center;
                    padding-bottom: 21px;
                    border-radius: 15px;
                    background: #fff;
                    position: absolute;
                    margin-left: 5px;
                    top: 5px;
                    z-index: 99999;
                }

            #upload-image #result img {
                max-height: 245px;
            }
    </style>

    <script>
        var itemProduct;
        var quantity;
        var index;
        var ms;
        var fromDate;
        var toDate;
        var salonId;
        var URL_API_INVENTORY;
        var URL_API_DOMAINS3;
        jQuery(document).ready(function () {
            //
            debugger;
            var qs = getQueryStrings();
            ShowMessage("Thông báo", qs["msg_message"], parseInt(qs["msg_status"]));
            //ShowMessage("Thông báo", "Lễ tân chú ý chọn đúng sản phẩm trong danh sách vật tư dịch vụ.", 3);
            // call 
            URL_API_INVENTORY = "<%= Libraries.AppConstants.URL_API_INVENTORY %>";
            URL_API_DOMAINS3 = "<%= Libraries.AppConstants.URL_API_DOMAINS3 %>";
            //getDataQuantityOfUse();
            // bind data to dropdownlist
            ms = $('#ms').magicSuggest({
                maxSelection: 1,
                data: <%=ListProduct %>,
                valueField: 'Id',
                displayField: 'Name',
            });
            //
            getDataOrderNum();
            BindImage();
            //
            $("table.table-item-product tbody tr").each(function () {
                prd = {};
                var THIS = $(this);
                //debugger

                var Id = THIS.find("td.td-product-code").attr("data-id")
                    ;
                 <% if (OBJ != null && OBJ.Status >= 2 && HDF_CheckReason.Value == "1")
        {%> 
                var reasonWH = THIS.find("select.form-control.selectWH"),
                    inputReasonWH = THIS.find("input.select-reason.wh");
                $("#DropDownReasonList option").each(function () {
                    //debugger
                    reasonWH.append($("<option />").val(this.value).text(this.text));
                });
                //
                $(reasonWH).val(inputReasonWH.val());
                <%}%>

                <% if (OBJ != null && OBJ.Status >= 4 && HDF_CheckReason.Value == "1")
        {%> 
                var reasonSalon = THIS.find("select.form-control.selectSalon"),
                    inputReasonSalon = THIS.find("input.select-reason.salon");
                $("#DropDownReasonList option").each(function () {
                    //debugger
                    reasonSalon.append($("<option />").val(this.value).text(this.text));
                });
                $(reasonSalon).val(inputReasonSalon.val());
                <%}%>

            });


            $(ms).on('selectionchange', function (e, m) {
                itemProduct = null;
                var listProduct = this.getSelection();
                console.log(listProduct);
                var listData = ms.getData();
                if (listProduct.length > 0) {
                    for (var i = 0; i < listData.length; i++) {
                        if (listProduct[0].Id == listData[i].Id) {
                            itemProduct = listProduct[0];
                            $("#inputQuantity").focus();
                            break;
                        }
                        else {
                            continue;
                        }
                    }
                    if (itemProduct == null) {
                        //showMsgSystem("Lễ tân chú ý chọn đúng sản phẩm trong danh sách vật tư dịch vụ.", "warning");
                        ShowMessage("Thông báo", "Lễ tân chú ý chọn đúng sản phẩm trong danh sách vật tư dịch vụ.", 3);
                        ms.clear();
                        $(".ms-sel-ctn input[type='text']").focus();
                    }
                }
            });

            //Barcode detection
            $(document).scannerDetection({
                timeBeforeScanTest: 200, // wait for the next character for upto 200ms
                avgTimeByChar: 40, // it's not a barcode if a character takes longer than 100ms
                preventDefault: false,
                endChar: ['\n'],
                onComplete: function (barcode, qty) {
                    debugger
                    var productId = 0;

                    $("table.table-item-product tbody tr").each(function () {
                        prd = {};
                        var THIS = $(this);
                        //debugger;

                        var Id = THIS.find("td.td-product-code").attr("data-id");
                        var barCodeTmp = THIS.find("td.td-bar-code").html().trim();
                        if (barCodeTmp != null && barcode != null & barCodeTmp.trim() == barcode.trim()) {
                            productId = Id;
                            return;
                        }
                        //debugger;
                    });
                    if (productId !== 0) {

                        debugger;
                        //<td class="td-product-code maSPcol" style="width: 50px" data-id
                        var td = $("table.table-item-product tbody tr").find("td[data-id='" + productId + "']");
                        if (td != null) {
                            var tr = td.parent();
                            var import1 = td.parent().find("input.product-quantity.import");
                            var num = import1.val().toString().trim() != "" ? parseInt(import1.val()) : 0;
                            import1.val(num + 1);

                            tr.css("background-color", "yellow");

                            setTimeout(function () {
                                tr.css("background-color", "white").effect("highlight", {}, 3000);
                            }, 2000);

                            debugger;
                        }

                    } else {
                        //alert("Không tìm thấy sản phẩm!!!");
                        ShowMessage("Thông báo", "Không tìm thấy sản phẩm.", 3);
                    }
                    //kiem tra ma barcode co dung la membership
                    //checkBarcode(barcode);
                }
            });
        });

        function addProduct() {
            var kt = true;
            quantity = $("#inputQuantity").val();
            if (quantity == "" || quantity <= "0") {
                ShowMessage("Thông báo", "Nhập số lượng sản phẩm và phải lớn hơn 0!", 3);
                $('#inputQuantity').css("border-color", "red");
                return;
            }

            // check trùng sản phẩm
            $("table.table-item-product tbody tr").each(function () {
                var THIS = $(this);
                var productId = THIS.find("td.td-product-code").attr("data-id");
                if (productId != "undefined" && itemProduct.Id == productId) {
                    kt = false;
                    ShowMessage("Thông báo", "Sản phẩm đã tồn tại trong danh sách. Vui lòng kiểm tra lại!", 3);
                    return;
                }
            });
            if (!kt) {
                return;
            }


            index = $("#table-item-product tbody tr").length + 1;
            if (itemProduct != null && quantity > 0) {
                if (itemProduct.Code != undefined) {
                    //
                    debugger
                    var option = $("#DropDownOrderNum").find("option[data-label='" + itemProduct.Id + "']");
                    var orderNum = 999999;
                    if (option != null && option.attr("value") != null) {
                        orderNum = option.attr("value");
                    }
                    //
                    var tr = '<tr>' +
                        '<td class="td-product-index">' + index + '</td>' +
                        '<td class="td-product-code maSPcol" data-id="' + itemProduct.Id + '" data-order-num="' + orderNum + '">' + itemProduct.Code + '</td>' +
                        '<td class="td-bar-code">' + itemProduct.BarCode + '</td>' +
                        '<td class="td-product-name">' + itemProduct.Name + '</td>' +
                          <% if (Perm_ReviewOrder)
        {%>
                        '<td class="td-product-cost">' + itemProduct.Cost + '</td>' +
                                                            <% } %>
                        '<td class="td-product-quantity map-edit">' +
                        '<input class="product-quantity type1" disabled="disabled"  value="' + 0 + '" type="text" placeholder="Số lượng"/>' +
                        '</td>' +
                        '<td class="td-product-quantity map-edit">' +
                        '<input class="product-quantity type2" disabled="disabled"  value="' + 0 + '" type="text" placeholder="Số lượng"/>' +
                        '</td>' +
                        '<td class="td-product-quantity map-edit">' +
                        '<input class="product-quantity type3" disabled="disabled" value="' + 0 + '" type="text" placeholder="Số lượng"/>' +
                        '</td>' +
                        '<td class="td-product-quantity map-edit">' +
                        '<input class="product-quantity type4" disabled="disabled" value="' + 0 + '" type="text" placeholder="Số lượng"/>' +
                        '</td>' +
                        '<td class="td-product-quantity map-edit">' +
                        '<input class="product-quantity type5" disabled="disabled" value="' + 0 + '" type="text" placeholder="Số lượng"/>' +
                        '</td>' +
                        '</td>' +
                        '<td class="td-product-quantity map-edit">' +
                        '<input class="product-quantity suggest" disabled="disabled" value="' + 0 + '" type="text" placeholder="Số lượng"/>' +
                        '</td>' +
                          <% if (Perm_ReviewOrder && OBJ != null && OBJ.Status == 2)
        {%>
                        '<td class="td-product-quantity map-edit">' +
                        '<input class="product-quantity total" style="color:red;" disabled="disabled" value="' + 0 + '" type="text" onkeypress="return isNumber(event)" placeholder="Số lượng"/>' +
                        '</td>' +
                         <% }
        else
        { %>
                        '<td class="td-product-quantity map-edit">' +
                        '<input class="product-quantity total" style="color:red;" value="' + quantity + '" type="text" onkeypress="return isNumber(event)" placeholder="Số lượng"/>' +
                        '</td>' +
                        <% }  %>
                        <% if (Perm_ReviewOrder && OBJ != null && OBJ.Status == 2)
        {%>
                        '<td class="td-product-quantity map-edit">' +
                        '<input class="product-quantity export" style="color:red;" value="' + quantity + '" type="text" onkeypress="return isNumber(event)" placeholder="Số lượng"/>' +
                        '</td>' + <% } %>
                        <% if (OBJ != null && OBJ.Status == 4)
        {%>
                        '<td class="td-product-quantity map-edit">' +
                        '<input class="product-quantity import" style="color:red;" value="' + quantity + '" type="text" onkeypress="return isNumber(event)" placeholder="Số lượng"/>' +
                        '</td>' + <% } %>

                        <% if (Perm_ReviewOrder && OBJ != null && OBJ.Status == 2 && HDF_CheckReason.Value == "1")
        {%>
                        '<td class="td-product-quantity map-edit">' +
                        '<input type="text" class="select-reason WH" style="visibility: hidden" value="" />' +
                        '<select class="form-control selectWH" style="width:180px">' +
                        '</select>' +
                        '</td>' +
                        <% } %>
                         <% if (OBJ != null && OBJ.Status == 4 && HDF_CheckReason.Value == "1")
        {%>
                        '<td class="td-product-quantity map-edit">' +
                        '<input type="text" class="select-reason salon" style="visibility: hidden" value="" />' +
                        '<select class="form-control selectSalon" style="width:180px">' +
                        '</select>' +
                        '</td>' +
                        <% } %>
                        '<td class="td-product-quantity map-edit">' +
                        '<div class="edit-wp">' +
                        '<a class="elm del-btn" onclick="RemoveItem($(this).parent().parent().parent(),\'' + itemProduct.Name + '\',\'product\')" href="javascript://" title="Xóa"></a>' +
                        '</div>' +
                        '</td>' +
                        '</tr>';
                    //                                      
                    $("#table-item-product tbody").append($(tr));
                    //
                    debugger
                    //

                            <% if (Perm_ReviewOrder && OBJ != null && OBJ.Status == 2 && HDF_CheckReason.Value == "1")
        {%>
                    var reasonSalon = $('#table-item-product tr:last').find("select.form-control.selectWH");
                    $("#DropDownReasonList option").each(function () {
                        //debugger
                        reasonSalon.append($("<option />").val(this.value).text(this.text));
                    });
                    $(reasonSalon).val('6');
                    <% }
        if (OBJ != null && OBJ.Status == 4 && HDF_CheckReason.Value == "1")
        {%>
                    var reasonSalon = $('#table-item-product tr:last').find("select.form-control.selectSalon");
                    $("#DropDownReasonList option").each(function () {
                        //debugger
                        reasonSalon.append($("<option />").val(this.value).text(this.text));
                    });
                    $(reasonSalon).val('1');
                    <% } %>
                    $("#ListingProductWp").show();
                    //itemProduct.Order = itemProduct.Order.toString().trim() != ""? parseInt(Order) : 0;
                    getProductIds();
                    //resetInputForm();
                    //checkElement();
                }
                else {
                    //showMsgSystem("Lễ tân chú ý chọn đúng sản phẩm trong danh sách vật tư dịch vụ.", "warning");
                    ms.clear();
                    $(".ms-sel-ctn input[type='text']").focus();
                    ShowMessage("Thông báo", "Lễ tân chú ý chọn đúng sản phẩm trong danh sách vật tư dịch vụ.", 3);
                    return;
                }

            }
            //}
        }

        function getProductIds() {
            var Ids = [];
            var prd = {};
            $("table.table-item-product tbody tr").each(function () {
                prd = {};
                var THIS = $(this);

                var Id = THIS.find("td.td-product-code").attr("data-id"),
                    Code = THIS.find("td.td-product-code").text().trim(),
                    Name = THIS.find("td.td-product-name").text().trim(),
                    QuantityType1 = THIS.find("input.product-quantity.type1").val(),
                    QuantityType2 = THIS.find("input.product-quantity.type2").val(),
                    QuantityType3 = THIS.find("input.product-quantity.type3").val(),
                    QuantityType4 = THIS.find("input.product-quantity.type4").val(),
                    QuantityType5 = THIS.find("input.product-quantity.type5").val(),
                    QuantitySuggest = THIS.find("input.product-quantity.suggest").val(),
                    QuantityOrder = THIS.find("input.product-quantity.total").val(),
                    QuantityExport = THIS.find("input.product-quantity.export").val(),
                    QuantityImport = THIS.find("input.product-quantity.import").val(),
                    Order = THIS.find("td.td-product-order").attr("data-order"),
                    WhReason = THIS.find("select.form-control.selectWH").val(),
                    SalonReason = THIS.find("select.form-control.selectSalon").val();

                // check value
                Id = Id.toString().trim() != "" ? parseInt(Id) : 0;
                QuantityOrder = QuantityOrder.trim() != "" ? parseInt(QuantityOrder) : 0;
                QuantityExport = QuantityExport != null ? parseInt(QuantityExport) : 0;
                QuantityImport = QuantityImport != null ? parseInt(QuantityImport) : 0;
                QuantitySuggest = QuantitySuggest != null ? parseInt(QuantitySuggest) : 0;

                prd.ProductId = Id;
                prd.Code = Code;
                prd.Name = Name;
                prd.QuantityType1 = QuantityType1;
                prd.QuantityType2 = QuantityType2;
                prd.QuantityType3 = QuantityType3;
                prd.QuantityType4 = QuantityType4;
                prd.QuantityType5 = QuantityType5;
                prd.QuantitySuggest = QuantitySuggest
                prd.QuantityOrder = QuantityOrder;
                prd.QuantityExport = QuantityExport;
                prd.QuantityImport = QuantityImport;
                prd.WHReason = WhReason != null ? parseInt(WhReason) : 1;
                prd.SalonReason = SalonReason != null ? parseInt(SalonReason) : 1;;


                Ids.push(prd);

            });
            Ids = JSON.stringify(Ids);
            $("#HDF_ProductIds").val(Ids);
        }

        function resetInputForm() {
            ms.clear();
            $("#inputQuantity").val("");
            $(ms).focus();
            $(".ms-sel-ctn input[type='text']").focus();
        }



    </script>
</asp:Content>

<asp:Content ID="ServiceAdd" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <%--<div class="alert alert-danger" id="success-alert">
            <span id="msg-alert"></span>
        </div>--%>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý đơn đặt hàng &nbsp;&#187; Đơn đặt hàng</li>
                        <li class="li-listing"><a href="/don-dat-hang/danh-sach.html">Danh sách</a></li>
                        <li class="li-add"><a href="/don-dat-hang/them-moi-don-nhap-hang.html?type=1">Tạo đơn nhập hàng</a></li>
                        <li class="li-add"><a href="/don-dat-hang/them-moi-don-xuat-hang.html?type=2">Tạo đơn xuất hàng</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add admin-product-add fe-service">
            <%-- Add --%>
            <div class="wp960 content-wp">

                <div class="table-wp">
                    <table class="table-add admin-product-table-add fe-service-table-add">
                        <tbody>
                            <tr class="title-head">

                                <td colspan="2">
                                    <nav>
                                        <ol class="cd-multi-steps text-bottom count">
                                            <% if (OBJ != null)
                                                {
                                                    if (OBJ.Status == 1)
                                                    {%>
                                            <li class="current" style="width: 140px"><a href="#0">Chưa Order</a></li>
                                            <% }
                                                else
                                                {%>
                                            <li class="visited" style="width: 140px"><a href="#0">Chưa Order</a></li>
                                            <%}
                                                if (OBJ.Status > 2)
                                                {%>
                                            <li class="visited" style="width: 140px"><a href="#0">Đã order</a></li>
                                            <% }
                                                else if (OBJ.Status == 2)
                                                {%>
                                            <li class="current" style="width: 140px"><a href="#0">Đã order</a></li>
                                            <% }
                                                else
                                                {%>
                                            <li style="width: 140px"><a href="#0">Đã order</a></li>
                                            <% } %>

                                            <% if (OBJ.Status == 3)
                                                {%>
                                            <li class="current" style="width: 140px"><a href="#0">Từ chối</a></li>
                                            <% } %>

                                            <% if (OBJ.Status > 4)
                                                {%>
                                            <li class="visited" style="width: 140px"><a href="#0">Chờ nhận hàng</a></li>
                                            <% }
                                                else if (OBJ.Status == 4)
                                                {%>
                                            <li class="current" style="width: 140px"><a href="#0">Chờ nhận hàng</a></li>
                                            <% }
                                                else
                                                {%>
                                            <li style="width: 140px"><a href="#0">Chờ nhận hàng</a></li>
                                            <% } %>
                                            <% if (OBJ.Status >= 5)
                                                {%>
                                            <li class="visited" style="width: 140px"><a href="#0">Đã nhận hàng</a></li>
                                            <% }
                                                else
                                                {%>
                                            <li style="width: 140px"><a href="#0">Đã nhận hàng</a></li>
                                            <% } %>

                                            <% } %>
                                        </ol>
                                    </nav>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-2 left"><span>Ngày tạo đơn</span></td>
                                <td class="col-xs-9 right">
                                    <table>
                                        <tr>
                                            <td>
                                                <div class="datepicker-wp">
                                                    <asp:TextBox ID="TxtDateTimeFrom" runat="server" ClientIDMode="Static" CssClass="txtDateTime st-head form-control" placeholder="Chọn ngày" Style="width: 180px;"></asp:TextBox>
                                                </div>
                                            </td>
                                            <% if (OBJ != null)
                                                { %>
                                            <td><span style="padding-right: 10px; padding-left: 10px; width: 100px;">Mã đơn hàng</span> </td>
                                            <td><%--<div class="input-quantity-wrap">--%>
                                                <asp:TextBox ID="Code" runat="server" ClientIDMode="Static" CssClass="txtFilter form-control" Enabled="false" Style="width: 180px;"></asp:TextBox>
                                                <%--<input type="text" id="Code" class="" disabled/>--%><%--</div>--%></td>
                                            <% }%>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr id="TrSalon" runat="server" class="tr-field-ahalf">
                                <td class="col-xs-2 left"></td>
                                <td class="col-xs-9 right">
                                    <table>
                                        <tr>
                                            <td>
                                                <div class="salonSelect">
                                                    <div class="field-wp" style="float: left; width: auto;">
                                                        <asp:DropDownList ID="fromInventory" runat="server" ClientIDMode="Static" CssClass="form-control select" Style="width: 160px;">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <span style="padding-right: 10px; padding-left: 10px; width: 100px;"></span>
                                            </td>
                                            <td>
                                                <div class="filter-item">
                                                    <asp:DropDownList ID="OrderType" runat="server" ClientIDMode="Static" CssClass="select form-control" Style="width: 80px;" ToolTip="Loại đơn hàng">
                                                    </asp:DropDownList>
                                                </div>
                                            </td>
                                            <td><span id="spanOrderType" style="padding-right: 10px; padding-left: 10px; width: 80px;"><% if (OBJ != null && OBJ.OrderType == 2)
                                                    { %>sang Kho <% }
                                    else
                                    { %>từ Kho <% }%></span></td>
                                            <td>
                                                <div class="salonSelect">
                                                    <div class="field-wp" style="float: left; width: auto;">
                                                        <asp:DropDownList ID="toInventory" runat="server" ClientIDMode="Static" CssClass="form-control select" Style="width: 160px;">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-2 left"><span>Loại hàng</span></td>
                                <td class="col-xs-9 right">
                                    <div class="field-wp" style="float: left; width: auto;">
                                        <asp:DropDownList ID="CosmeticType" CssClass="form-control select" runat="server" ClientIDMode="Static" Style="width: 160px;"></asp:DropDownList>
                                    </div>
                                </td>
                            </tr>
                            <% if (OBJ != null && OBJ.Status <= 3)
                                {%>
                            <tr>
                                <td class="col-xs-2 left"><span>Sản phẩm</span></td>
                                <td class="col-xs-9 right">
                                    <div class="suggestion-wrap">
                                        <input id="ms" class="form-control" type="text" />
                                    </div>
                                    <div class="input-quantity-wrap">
                                        <input type="number" id="inputQuantity" class="input-quantity" onkeypress="return isNumber(event)" placeholder="Số lượng" />
                                    </div>
                                    <div class="btn-add" onclick="addProduct()">Thêm sản phẩm</div>
                                </td>
                            </tr>
                            <% } %>
                            <tr class="tr-field-ahalf tr-product">
                                <td class="col-xs-2 left"><span>Sản phẩm</span></td>
                                <td class="col-xs-9 right">
                                    <div class="listing-product item-product" id="ListingProductWp" runat="server" clientidmode="Static">
                                        <table class="table table-listing-product table-item-product" id="table-item-product">
                                            <thead>
                                                <tr>
                                                    <th>STT</th>
                                                    <th class="maSPcol">Mã sản phẩm</th>
                                                    <th>BarCode</th>
                                                    <th>Tên sản phẩm</th>
                                                    <% if (Perm_ReviewOrder)
                                                        {%>
                                                    <th>Giá nhập</th>
                                                    <% } %>
                                                    <th style="width: 50px">SL Mỹ phẩm</th>
                                                    <th style="width: 50px">SL Vật tư Nhóm 1</th>
                                                    <th style="width: 50px">SL Vật tư Nhóm 2</th>
                                                    <th style="width: 50px">SL Vật tư Nhóm 3</th>
                                                    <th style="width: 50px">SL Vật tư NCC</th>
                                                    <th style="width: 50px; color: red;">Hệ thống đề xuất</th>
                                                    <th style="width: 50px; color: red;">Tổng Order</th>
                                                    <% if (Perm_ReviewOrder && OBJ != null && OBJ.Status == 2)
                                                        {%>
                                                    <th style="width: 50px; color: red;">Xuất</th>
                                                    <% }
                                                        else if (OBJ != null && OBJ.Status > 2)
                                                        { %>
                                                    <th style="width: 50px; color: red;">Xuất</th>
                                                    <% } %>
                                                    <% if (OBJ != null && OBJ.Status >= 4)
                                                        {%>
                                                    <th style="width: 50px; color: red;">Xác nhận nhập</th>
                                                    <% } %>
                                                    <% if ((Perm_ReviewOrder && OBJ != null && OBJ.Status == 2 && HDF_CheckReason.Value == "1")
                                                                                                                                                          || (OBJ != null && OBJ.Status > 2 && HDF_CheckReason.Value == "1"))
                                                        {%>
                                                    <th style="width: 50px">Nguyên nhân từ kho</th>
                                                    <% } %>
                                                    <% if (OBJ != null && OBJ.Status >= 4 && HDF_CheckReason.Value == "1")
                                                        {%>
                                                    <th style="width: 50px">Nguyên nhân từ salon</th>
                                                    <% } %>
                                                    <th style="width: 50px">Hành động</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater ID="Rpt_Product_Flow" runat="server">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td class="td-product-index"><%# Container.ItemIndex + 1 %></td>
                                                            <td class="td-product-code maSPcol" style="width: 50px" data-id="<%# Eval("ProductId") %>" data-order-num="<%# Eval("OrderNum") %>"><%# Eval("ProductCode") %></td>
                                                            <td class="td-bar-code" style="width: 50px"><%# Eval("BarCode") %></td>
                                                            <td class="td-product-name"><%# Eval("ProductName") %></td>
                                                            <% if (Perm_ReviewOrder)
                                                                {%>
                                                            <td class="td-product-cost"><%# Eval("Cost") %></td>
                                                            <% } %>
                                                            <td class="td-product-quantity map-edit">
                                                                <input type="text" class="product-quantity type1" disabled="disabled" value="<%# Eval("QuantityType1") %>" />
                                                            </td>
                                                            <td class="td-product-quantity map-edit">
                                                                <input type="text" class="product-quantity type2" disabled="disabled" value="<%# Eval("QuantityType2") %>" />
                                                            </td>
                                                            <td class="td-product-quantity map-edit">
                                                                <input type="text" class="product-quantity type3" disabled="disabled" value="<%# Eval("QuantityType3") %>" />
                                                            </td>
                                                            <td class="td-product-quantity map-edit">
                                                                <input type="text" class="product-quantity type4" disabled="disabled" value="<%# Eval("QuantityType4") %>" />
                                                            </td>
                                                            <td class="td-product-quantity map-edit">
                                                                <input type="text" class="product-quantity type5" disabled="disabled" value="<%# Eval("QuantityType5") %>" />
                                                            </td>
                                                            <td class="td-product-quantity map-edit">
                                                                <input type="text" class="product-quantity suggest" style="color: red;" disabled="disabled" value="<%# Eval("QuantitySuggest") %>" />
                                                            </td>
                                                            <% if (Perm_Edit && OBJ != null && OBJ.Status == 1)
                                                                { %>
                                                            <td class="td-product-quantity map-edit">
                                                                <input type="text" class="product-quantity total" style="color: red;" onkeypress="return isNumber(event)" value="" />
                                                            </td>
                                                            <% }
                                                                else if (Perm_Edit && OBJ != null && (OBJ.Status == 2))
                                                                { %>
                                                            <td class="td-product-quantity map-edit">
                                                                <input type="text" class="product-quantity total" style="color: red;" onkeypress="return isNumber(event)" value="<%# Eval("QuantityOrder") %>" />
                                                            </td>
                                                            <% }
                                                                else
                                                                {%>
                                                            <td class="td-product-quantity map-edit">
                                                                <input type="text" class="product-quantity total" style="color: red;" disabled="disabled" onkeypress="return isNumber(event)" value="<%# Eval("QuantityOrder") %>" />
                                                            </td>
                                                            <% } %>
                                                            <% if (Perm_ReviewOrder && OBJ != null && OBJ.Status == 2)
                                                                {%>
                                                            <td class="td-product-quantity map-edit">
                                                                <input type="text" class="product-quantity export" style="color: red;" onkeypress="return isNumber(event)" value="<%# Eval("QuantityOrder") %>" />
                                                            </td>
                                                            <% }
                                                                else if (OBJ != null && OBJ.Status > 2)
                                                                {%>
                                                            <td class="td-product-quantity map-edit">
                                                                <input type="text" class="product-quantity export" style="color: red;" disabled="disabled" value="<%# Eval("QuantityExport") %>" />
                                                            </td>
                                                            <% } %>
                                                            <% if (Perm_Edit && OBJ != null && OBJ.Status == 4)
                                                                {%>
                                                            <td class="td-product-quantity map-edit">
                                                                <input type="text" class="product-quantity import" style="color: red;" onkeypress="return isNumber(event)" value="" />
                                                            </td>
                                                            <%}
                                                                else if (Perm_Edit && OBJ != null && OBJ.Status > 4)
                                                                {%>
                                                            <td class="td-product-quantity map-edit">
                                                                <input type="text" class="product-quantity import" style="color: red;" onkeypress="return isNumber(event)" value="<%# Eval("QuantityImport") %>" />
                                                            </td>
                                                            <% }
                                                                else if (OBJ != null && OBJ.Status >= 4)
                                                                {%>
                                                            <td class="td-product-quantity map-edit">
                                                                <input type="text" class="product-quantity import" style="color: red;" disabled="disabled" value="<%# Eval("QuantityImport") %>" />
                                                            </td>
                                                            <% } %>
                                                            <% if (Perm_ReviewOrder && OBJ != null && OBJ.Status == 2 && HDF_CheckReason.Value == "1")
                                                                {%>
                                                            <td class="td-product-quantity map-edit">
                                                                <input type="text" class="select-reason wh" style="visibility: hidden" value="<%# Eval("WHReason") %>" />
                                                                <select class="form-control selectWH" style="width: 180px">
                                                                </select>
                                                            </td>
                                                            <% }
                                                                else if (OBJ != null && OBJ.Status > 2 && HDF_CheckReason.Value == "1")
                                                                {%>
                                                            <td class="td-product-quantity map-edit">
                                                                <input type="text" class="select-reason wh" style="visibility: hidden" value="<%# Eval("WHReason") %>" />
                                                                <select class="form-control selectWH" style="width: 180px" disabled="disabled">
                                                                </select>
                                                            </td>
                                                            <% } %>
                                                            <% if (Perm_Edit && OBJ != null && OBJ.Status == 4 && HDF_CheckReason.Value == "1")
                                                                {%>
                                                            <td class="td-product-quantity map-edit">
                                                                <input type="text" class="select-reason salon" style="visibility: hidden" value="<%# Eval("SalonReason") %>" />
                                                                <select class="form-control selectSalon" style="width: 180px">
                                                                </select>
                                                            </td>
                                                            <% }
                                                                else if (OBJ != null && OBJ.Status > 4 && HDF_CheckReason.Value == "1")
                                                                {%>
                                                            <td class="td-product-quantity map-edit">
                                                                <input type="text" class="select-reason salon" style="visibility: hidden" value="<%# Eval("SalonReason") %>" />
                                                                <select class="form-control selectSalon" style="width: 180px" disabled="disabled">
                                                                </select>
                                                            </td>
                                                            <% } %>
                                                            <td class="td-product-quantity map-edit">
                                                                <div class="edit-wp">
                                                                    <% if (Perm_Delete && OBJ != null && OBJ.Status <= 2)
                                                                        {%>
                                                                    <%# (Eval("IsAuto") != null && Eval("IsAuto").ToString().Equals("False")) ? String.Concat(" <a class=\"elm del-btn\" onclick=\"RemoveItem($(this).parent().parent().parent(),'"+Eval("ProductName") +"','product')\" href=\"javascript://\" title=\"Xóa\"></a>") : "" %>
                                                                    <%--<a class="elm del-btn" onclick="RemoveItem($(this).parent().parent().parent(),'<%# Eval("ProductName") %>','product')"
                                                                        href="javascript://" title="Xóa"></a>--%>
                                                                    <% } %>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                            </tr>

                            <tr class="tr-description">
                                <td class="col-xs-2 left"><span>Ghi chú</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox TextMode="MultiLine" Rows="5" ID="Note" runat="server" ClientIDMode="Static"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>
                            <% if (OBJ != null && OBJ.Status == 4
                                                                        && ((OBJ.InventoryOrderType == 2 && OBJ.InventoryPartnerType == 1 && OBJ.OrderType == 1)
                                                                        || (OBJ.InventoryOrderType == 1 && OBJ.InventoryPartnerType == 2 && OBJ.OrderType == 2)
                                                                        || (OBJ.InventoryOrderType == 2 && OBJ.InventoryPartnerType == 4 && OBJ.OrderType == 1)))
                                {%>
                            <tr class="tr-description">
                                <td class="col-xs-2 left"><span>Thêm ảnh đơn hàng (<i>Có thể lựa chọn nhiều file ảnh</i>):</span></td>
                                <td class="col-xs-9 right">
                                    <div class="col-auto form-group" style="width: 100% !important">
                                        <div class=" field salon-error">
                                            <%--<label class="font-weight-bold">Hình ảnh lỗi (<i>Có thể lựa chọn nhiều file ảnh</i>): </label>--%>
                                            <table style="width: 100%">
                                                <tr>
                                                    <td style="width: 10%">
                                                        <% if (Perm_Edit && OBJ.Status == 4)
                                                            { %>
                                                        <div class="ui secondary button choose-image" style="margin-left: 20px;">Upload</div>
                                                        <% } %>
                                                    </td>
                                                    <td style="width: 90%">
                                                        <div class="col-xs-2 wr-search position-relative" style="width: 100%">
                                                            <div class=" input full-width" id="upload-image" style="margin-bottom: 0px;">
                                                                <div>
                                                                    <input class="hide" style="display: none" id="files" type="file" multiple />
                                                                </div>
                                                                <output id="result" />
                                                            </div>

                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <% } %>
                            <% if (OBJ != null && OBJ.Status == 5)
                                { %>
                            <tr class="tr-imgs">
                                <td class="col-xs-2 left"><span>Ảnh đơn hàng</span></td>
                                <td style="width: 100%">
                                    <div id="listImgs" />
                                </td>
                            </tr>
                            <%} %>

                            <tr class="tr-send">
                                <td class="col-xs-2 left"></td>
                                <td class="col-xs-9 right no-border">
                                    <span class="field-wp">
                                        <% if (Perm_Edit && OBJ != null && OBJ.Status == 1)
                                            { %>
                                        <input type="button" id="BtnOrder" onclick="beginOrder();" style="padding-left: 10px; padding-right: 10px; margin-right: 15px; margin-left: 15px; width: auto !important; min-width: 90px !important; margin-left: 0 !important;" class="btn-send" value="Hoàn tất" />
                                        <asp:Button ID="BtnFakeOrder" OnClick="ActionBeginOrder" runat="server" Text="Hoàn tất"
                                            ClientIDMode="Static" Style="display: none;"></asp:Button>
                                        <% } %>
                                        <% if (Perm_Edit && OBJ != null && OBJ.Status == 2 && OBJ.CreatedDate.AddHours(2).CompareTo(DateTime.Now) > 0 && !(OBJ.isAuto == 1 && OBJ.CosmeticType == 1))
                                            { %>
                                        <input type="button" id="BtnEdit" onclick="updateOrder();" style="padding-left: 10px; padding-right: 10px; margin-right: 15px; margin-left: 15px; width: auto !important; min-width: 90px !important; margin-left: 0 !important;" class="btn-send" value="Cập nhật" />
                                        <asp:Button ID="BtnFakeEdit" OnClick="ActionUpdate" runat="server" Text="Cập nhật"
                                            ClientIDMode="Static" Style="display: none;"></asp:Button>
                                        <% } %>
                                        <% if (Perm_ReviewOrder && OBJ != null && OBJ.Status == 2)
                                            { %>
                                        <%--<asp:Panel ID="BtnReview" CssClass="btn-send" runat="server" ClientIDMode="Static">Phê duyệt</asp:Panel>--%>
                                        <input type="button" id="BtnReview" onclick="reviewOrder();" style="padding-left: 10px; padding-right: 10px; margin-right: 15px; margin-left: 15px; width: auto !important; min-width: 90px !important; margin-left: 0 !important;" class="btn-send" value="Phê duyệt" />
                                        <asp:Button ID="BtnFakeReview" OnClick="ActionReview" runat="server" Text="Phê duyệt"
                                            ClientIDMode="Static" Style="display: none;"></asp:Button>
                                        <% } %>
                                        <% if (Perm_Edit && OBJ != null && OBJ.Status == 4)
                                            { %>
                                        <input type="button" id="BtnComplete" onclick="completeOrder();" style="padding-left: 10px; padding-right: 10px; margin-right: 15px; margin-left: 15px; width: auto !important; min-width: 90px !important; margin-left: 0 !important;" class="btn-send" value="Xác nhận nhập" />
                                        <asp:Button ID="BtnFakeComplete" OnClick="ActionComplete" runat="server" Text="Xác nhận nhập"
                                            ClientIDMode="Static" Style="display: none;"></asp:Button>
                                        <% } %>

                                        <% if (OBJ != null && OBJ.Status >= 2)
                                            { %>
                                        <input type="button" id="BtnPrint" onclick="PrintOrder();" style="padding-left: 10px; padding-right: 10px; margin-right: 15px; margin-left: 15px; width: auto !important; min-width: 90px !important; margin-left: 0 !important;" class="btn-send" value="In đơn hàng" />
                                        <% } %>
                                        <% if (OBJ != null && OBJ.Status >= 3)
                                            { %>
                                        <input type="button" id="BtnPrintTurn2" onclick="PrintTurn2Order();" style="padding-left: 10px; padding-right: 10px; margin-right: 15px; margin-left: 15px; width: auto !important; min-width: 90px !important; margin-left: 0 !important;" class="btn-send" value="In đơn hàng lần 2" />
                                        <% } %>
                                    </span>
                                </td>
                            </tr>
                            <asp:HiddenField runat="server" ID="UrlKeyField" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="ImgCoverField" ClientIDMode="Static" />
                        </tbody>
                    </table>

                    <!-- input hidden -->
                    <asp:HiddenField ID="HDF_OrderId" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_InventoryOrderId" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_InventoryPartnerId" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_OrderType" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_ProductIds" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_OrderStatus" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_CosmeticType" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_CheckReason" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_CreatedDate" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_CheckUpload" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_Imgs" runat="server" ClientIDMode="Static" />

                    <asp:HiddenField ID="Cosmetic" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_lgId" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_BillId" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_FreeService" ClientIDMode="Static" runat="server" />
                    <asp:HiddenField ID="HDF_Rating" ClientIDMode="Static" runat="server" />
                    <%--<asp:DropDownList ID="DropDownReasonList" CssClass="form-control select" runat="server" ClientIDMode="Static" Visible="false"></asp:DropDownList>--%>
                    <select id="DropDownReasonList" style="visibility: hidden">
                        <% foreach (var item in ListIvReason)
                            { %>
                        <!--thêm Mapid-->
                        <option data-label="<%= item.Label %>" value="<%= item.Value %>"><%= item.Label %></option>
                        <% } %>
                    </select>
                    <select id="DropDownOrderNum" style="visibility: hidden">
                        <%--<option data-label="<%= item.Label %>" value="<%= item.Value %>"><%= item.Label %></option>--%>
                    </select>
                    <select id="inventorySalon" style="visibility: hidden">
                        <% foreach (var item in ListIvInventoryTypeSalon)
                            { %>
                        <!--thêm Mapid-->
                        <option data-key="<%= item.Id %>" data-price="<%= item.Name %>" value="<%= item.Id %>"></option>
                        <% } %>
                    </select>
                    <!-- END input hidden -->
                </div>
            </div>
            <%-- end Add --%>
        </div>

        <script src="/assets/js/jquery.mCustomScrollbar.js"></script>
        <link href="/assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" />
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 180px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 35px !important;
                padding-top: 1px !important;
            }
        </style>
        <script type="text/javascript">
            //
            <% if (Perm_Edit)
            {%>
            if ($("#HDF_OrderStatus").val() == "1"
                || $("#HDF_OrderStatus").val() == "2"
                || $("#HDF_OrderStatus").val() == "3"
            ) {
                $("#fromInventory").prop("disabled", false);
                $("#OrderType").prop("disabled", false);
                $("#toInventory").prop("disabled", false);
                $("#CosmeticType").prop("disabled", false);

            } else {
                $("#fromInventory").prop("disabled", true);
                $("#OrderType").prop("disabled", true);
                $("#toInventory").prop("disabled", true);
                $("#CosmeticType").prop("disabled", true);
            }
            <% }
            else
            {%>
            $("#fromInventory").prop("disabled", true);
            $("#OrderType").prop("disabled", true);
            $("#toInventory").prop("disabled", true);
            $("#CosmeticType").prop("disabled", true);
            <% } %>
            //
            debugger
            let images = [];
            let wrapImages = '';
            if ($("#HDF_Imgs").val() != null) {
                images = $("#HDF_Imgs").val().split(',');
                let itemImage = '<table><tr>';
                //if ($(images).length === 1) {
                //    itemImage += '<div class="column"><div class="image"><img style="width:100%" src="' +
                //        $(images)[0] +
                //        '"></div></div>';
                //} else {
                $(images).each(function (index, value) {
                    itemImage += '<td style="padding-right:10px"><div class="column"><div class="image"><img src="' +
                        value +
                        '" width="150" height="150" onclick="openInNewTab(this);" ></div></div></td>';
                });

                itemImage += '</tr></table>';
                //}
                //wrapImages = '<div class="ui menu position-relative"><img class="ui small image inline img-error" src="' +
                //    (images[0] || '') +
                //    '"><div class="ui custom popup equal width grid" style="display:none">' +
                //    itemImage +
                //    '</div></div></div>';
                $("#listImgs").append(itemImage);
            }

            function openInNewTab(This) {
                var url = This.getAttribute('src');
                var win = window.open(url, '_blank');
                win.focus();
            }

            jQuery(document).ready(function () {


                //checkElement();
                // Add active menu
                $("#glbDatHangNoiBo").addClass("active");
                $("#glbDatHangNoiBo_ChoSalon").addClass("active");
                if ($("#subMenu li.li-edit").length > 0) {
                    $("#subMenu li.li-edit").addClass("active");
                }
                else {
                    $("#subMenu li.li-add").addClass("active");
                }

                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) { },
                    scrollMonth: false,
                    scrollTime: false,
                    scrollInput: false
                });
                $("#OrderType").bind("change", function () {
                    //
                    if ($("#OrderType").val() == 1) {
                        $("#spanOrderType").text("từ Kho");
                    } else {
                        $("#spanOrderType").text("sang Kho");
                    }
                });

            });

            function reviewOrder() {
                //
                getProductIds();
                //
                debugger
                var fromInventory = $('#fromInventory').val();
                var toInventory = $('#toInventory').val();
                var dataProduct = $("#HDF_ProductIds").val();
                if (fromInventory == "" || fromInventory == "0" || toInventory == "" || toInventory == "0") {
                    ShowMessage("Thông báo", "Bạn chưa chọn Kho!", 3);
                    return;
                }
                var salonOrder = $("#inventorySalon").find("option[data-key='" + fromInventory + "']");
                var salonPartner = $("#inventorySalon").find("option[data-key='" + toInventory + "']");
                //
                if (salonOrder != null && salonOrder.val() != null && salonPartner != null && salonPartner.val() != null) {
                    ShowMessage("Thông báo", "Không được phép chuyển hàng giữa các salon!", 3);
                    return;
                }

                if (dataProduct == "[]") {
                    ShowMessage("Thông báo", "Bạn chưa chọn sản phẩm!", 3);
                    return;
                }
                $('#HDF_InventoryOrderId').val($('#fromInventory').val());
                $('#HDF_InventoryPartnerId').val($('#toInventory').val());
                $('#HDF_OrderType').val($('#OrderType').val());
                //

                var difProduct = "";
                var reasonStr = "";
                //validate thong tin so luong xuat cho salon
                $("table.table-item-product tbody tr").each(function () {
                    prd = {};
                    var THIS = $(this);

                    var Id = THIS.find("td.td-product-code").attr("data-id"),
                        Code = THIS.find("td.td-product-code").text().trim(),
                        Name = THIS.find("td.td-product-name").text().trim(),
                        QuantitySuggest = THIS.find("input.product-quantity.suggest").val(),
                        QuantityOrder = THIS.find("input.product-quantity.total").val(),
                        QuantityExport = THIS.find("input.product-quantity.export").val(),
                        QuantityImport = THIS.find("input.product-quantity.import").val(),
                        WhReason = THIS.find("select.form-control.selectWH").val();
                    //SalonReason = THIS.find("select.form-control.selectSalon").val();
                    ;
                    // check value
                    QuantityOrder = QuantityOrder.trim() != "" ? parseInt(QuantityOrder) : 0;
                    QuantityExport = QuantityExport != null ? parseInt(QuantityExport) : 0;
                    QuantityImport = QuantityImport != null ? parseInt(QuantityImport) : 0;
                    if (QuantityOrder > 0 && QuantityOrder != QuantityExport) {
                        difProduct += "<br>[" + Name + "]";
                        if (WhReason == null || WhReason == "1") {
                            reasonStr += "<br>[" + Name + "]";
                        }
                    }
                });
                if (reasonStr.length > 0 && $("#HDF_CheckReason").val() == "1") {
                    ShowMessage("Thông báo", "Đề nghị chọn nguyên nhân lượng xuất khác lượng order ở các sản phẩm: " + reasonStr, 3, 10000);
                    return;
                }
                //
                if (difProduct.length > 0) {

                    var test = {
                        //func khi chon co
                        funcYes: function (instance, toast) {
                            //ShowMessage("Thông báo", "abc " + toast, 3);
                            $("#BtnReview").attr("disabled", true);
                            $("#BtnFakeReview").click();
                            return;
                        },
                        //func khi chon khong
                        funcNo: function (instance, toast) {
                            return;
                        }
                    };
                    ShowConfirm("confirm", 100000, "Xác nhận", "Số lượng xuất khác lượng đặt hàng. Bạn có chắc chắn muốn thực hiện?<br>" + difProduct, "Có", "Không", test);
                } else {
                    $("#BtnFakeReview").click();
                }

            }
            //
            function getHDF() {
                $('#HDF_InventoryOrderId').val($('#fromInventory').val());
                $('#HDF_InventoryPartnerId').val($('#toInventory').val());
                $('#HDF_OrderType').val($('#OrderType').val());
                $('#HDF_CosmeticType').val($('#CosmeticType').val());
            }
            //
            function updateOrder() {
                getProductIds();
                var fromInventory = $('#fromInventory').val();
                var toInventory = $('#toInventory').val();
                var dataProduct = $("#HDF_ProductIds").val();
                if (fromInventory == "" || fromInventory == "0" || toInventory == "" || toInventory == "0") {
                    ShowMessage("Thông báo", "Bạn chưa chọn Kho!", 3);
                    return;
                }
                var salonOrder = $("#inventorySalon").find("option[data-key='" + fromInventory + "']");
                var salonPartner = $("#inventorySalon").find("option[data-key='" + toInventory + "']");
                //
                if (salonOrder != null && salonOrder.val() != null && salonPartner != null && salonPartner.val() != null) {
                    ShowMessage("Thông báo", "Không được phép chuyển hàng giữa các salon!", 3);
                    return;
                }
                if (dataProduct == "[]") {
                    ShowMessage("Thông báo", "Bạn chưa chọn sản phẩm!", 3);
                    return;
                }
                getHDF();

                $("#BtnFakeEdit").click();
            }

            function beginOrder() {
                getProductIds();
                var fromInventory = $('#fromInventory').val();
                var toInventory = $('#toInventory').val();
                var dataProduct = $("#HDF_ProductIds").val();
                if (fromInventory == "" || fromInventory == "0" || toInventory == "" || toInventory == "0") {
                    ShowMessage("Thông báo", "Bạn chưa chọn Kho!", 3);
                    return;
                }
                if (dataProduct == "[]") {
                    ShowMessage("Thông báo", "Bạn chưa chọn sản phẩm!", 3);
                    return;
                }
                //
                var difProduct = "";
                $("table.table-item-product tbody tr").each(function () {
                    prd = {};
                    var THIS = $(this);

                    var Id = THIS.find("td.td-product-code").attr("data-id"),
                        Code = THIS.find("td.td-product-code").text().trim(),
                        Name = THIS.find("td.td-product-name").text().trim(),
                        QuantitySuggest = THIS.find("input.product-quantity.suggest").val(),
                        QuantityOrder = THIS.find("input.product-quantity.total").val(),
                        QuantityExport = THIS.find("input.product-quantity.export").val(),
                        QuantityImport = THIS.find("input.product-quantity.import").val(),
                        WhReason = THIS.find("select.form-control.selectWH").val();
                    // check value
                    if (QuantityOrder == null || QuantityOrder.trim() == "") {
                        difProduct += "<br>[" + Name + "]";
                    }
                });
                if (difProduct.length > 0) {
                    ShowMessage("Thông báo", "Đề nghị nhập lượng Order ở các sản phẩm: " + difProduct, 3, 10000);
                    return;
                }
                //$('#HDF_InventoryOrderId').val($('#fromInventory').val());
                //$('#HDF_InventoryPartnerId').val($('#toInventory').val());
                //$('#HDF_OrderType').val($('#OrderType').val());
                getHDF();

                $("#BtnFakeOrder").click();
            }
            function completeOrder() {
                getProductIds();
                var fromInventory = $('#fromInventory').val();
                var toInventory = $('#toInventory').val();
                var dataProduct = $("#HDF_ProductIds").val();
                if (fromInventory == "" || fromInventory == "0" || toInventory == "" || toInventory == "0") {
                    ShowMessage("Thông báo", "Bạn chưa chọn Kho!", 3);
                    return;
                }
                if (dataProduct == "[]") {
                    ShowMessage("Thông báo", "Bạn chưa chọn sản phẩm!", 3);
                    return;
                }
                //$('#HDF_InventoryOrderId').val($('#fromInventory').val());
                //$('#HDF_InventoryPartnerId').val($('#toInventory').val());
                //$('#HDF_OrderType').val($('#OrderType').val());
                getHDF();

                var difProduct = "";
                var reasonStr = "";
                var missImport = "";
                //validate thong tin so luong xuat cho salon
                $("table.table-item-product tbody tr").each(function () {
                    prd = {};
                    var THIS = $(this);

                    var Id = THIS.find("td.td-product-code").attr("data-id"),
                        Code = THIS.find("td.td-product-code").text().trim(),
                        Name = THIS.find("td.td-product-name").text().trim(),
                        QuantitySuggest = THIS.find("input.product-quantity.suggest").val(),
                        QuantityOrder = THIS.find("input.product-quantity.total").val(),
                        QuantityExport = THIS.find("input.product-quantity.export").val(),
                        QuantityImport = THIS.find("input.product-quantity.import").val(),
                        SalonReason = THIS.find("select.form-control.selectSalon").val();
                    if (QuantityImport == null || QuantityImport.trim() == "") {
                        missImport += "<br>[" + Name + "]";
                    }
                    // check value
                    QuantityOrder = QuantityOrder.trim() != "" ? parseInt(QuantityOrder) : 0;
                    QuantityExport = QuantityExport != null ? parseInt(QuantityExport) : 0;
                    QuantityImport = QuantityImport != null ? parseInt(QuantityImport) : 0;
                    if (QuantityImport != QuantityExport) {
                        difProduct += "<br>[" + Name + "]";
                        if (SalonReason == null || SalonReason == "1") {
                            reasonStr += "<br>[" + Name + "]";
                        }
                    }
                });
                if (missImport.length > 0) {
                    ShowMessage("Thông báo", "Đề nghị nhập vào cột Xác nhận nhập ở các sản phẩm: " + missImport, 3, 10000);
                    return;
                }
                if (reasonStr.length > 0 && $("#HDF_CheckReason").val() == "1") {
                    ShowMessage("Thông báo", "Đề nghị chọn nguyên nhân lượng nhập khác lượng xuất ở các sản phẩm: " + reasonStr, 3, 10000);
                    return;
                }
                debugger
                //
                let dataImages = [];
                //kiem tra truong hop bat buoc phai upload anh
                if ($('#HDF_CheckUpload').val() == "1") {
                    //
                    let arrImage = [];
                    $('#result img').each(function (i, v) {
                        let base64 = $(v).attr('src');
                        //let now = new Date();
                        //let date = now.getDate() + '-' + (now.getMonth() + 1) + '-' + now.getFullYear();
                        let item = {
                            img_name: $('#Code').val() + "_" + Date.now(),
                            img_base64: base64
                        };
                        arrImage.push(item);
                    });

                    dataImages = {
                        images: arrImage
                    };

                    //valid images
                    if (!dataImages.images || dataImages.images.length == 0) {
                        //alert("Bạn hãy thêm ảnh bằng chứng");
                        ShowMessage("Thông báo", "Đề nghị thêm ảnh bằng chứng", 3, 10000);
                        return;
                    }
                }
                //
                if (difProduct.length > 0) {

                    var test = {
                        //func khi chon co
                        funcYes: function (instance, toast) {

                            //
                            if ($('#HDF_CheckUpload').val() == "1") {
                                //post to s3
                                let resultUpload = uploadImagesToS3(dataImages);

                                let arrLinks = [];
                                $.each(resultUpload.images, function (i, v) {
                                    // console.log('image', v);
                                    let link = v.link;
                                    if (link) {
                                        arrLinks.push(link);
                                    }
                                });
                                if (arrLinks.length == 0) {
                                    return;
                                }
                                //set data images
                                $('#HDF_Imgs').val(arrLinks.join());
                            }
                            //
                            debugger
                            $("#BtnComplete").attr("disabled", true);
                            $("#BtnFakeComplete").click();
                            return;
                        },
                        //func khi chon khong
                        funcNo: function (instance, toast) {
                            return;
                        }
                    };
                    ShowConfirm("confirm", 100000, "Xác nhận", "Số lượng nhập khác lượng xuất. Bạn có chắc chắn muốn thực hiện?<br>" + difProduct, "Có", "Không", test);
                } else {
                    if ($('#HDF_CheckUpload').val() == "1") {
                        //post to s3
                        let resultUpload = uploadImagesToS3(dataImages);

                        let arrLinks = [];
                        $.each(resultUpload.images, function (i, v) {
                            // console.log('image', v);
                            let link = v.link;
                            if (link) {
                                arrLinks.push(link);
                            }
                        });
                        if (arrLinks.length == 0) {
                            return;
                        }
                        //set data images
                        $('#HDF_Imgs').val(arrLinks.join());
                    }
                    $("#BtnFakeComplete").click();
                }

            }



            // Update table display
            function UpdateItemDisplay(itemName) {
                var dom = $(".table-item-" + itemName);
                var len = dom.find("tbody tr").length;
                if (len == 0) {
                    dom.parent().hide();
                } else {
                    UpdateItemOrder(dom.find("tbody"));
                }
            }

            // Update order item
            function UpdateItemOrder(dom) {
                var index = 1;
                dom.find("tr").each(function () {
                    $(this).find("td.td-product-index").text(index);
                    index++;
                });
            }

            // Remove item đã được chọn
            function RemoveItem(THIS, name, itemName) {

                var test = {
                    //func khi chon co
                    funcYes: function (instance, toast) {
                        var Code = THIS.find(".td-staff-code").attr("data-code");
                        $("#quick-" + itemName).find("input[data-code='" + Code + "']").removeAttr("checked");
                        THIS.remove();
                        getProductIds();
                        UpdateItemDisplay(itemName);
                        autoCloseEBPopup(0);
                    },
                    //func khi chon khong
                    funcNo: function (instance, toast) {
                        return;
                    }
                };
                ShowConfirm("confirm", 100000, "Xác nhận", "Bạn có chắc chắn muốn hủy [ " + name + " ] ?", "Có", "Không", test);
            }


            function PrintOrder() {
                //Add popup confirm before print
                var confirm = {
                    //func khi chon co
                    funcYes: function (instance, toast) {

                var arrServicesProducts = [];
                var code = $('#Code').val();
                var CosmeticType = $('#CosmeticType option:selected').text();
                var fromInventory = $('#fromInventory option:selected').text();
                var toInventory = $('#toInventory option:selected').text();
                var note = $('#Note').val();
                $("table.table-item-product tbody tr").each(function () {
                    var THIS = $(this);
                    debugger
                    var Id = THIS.find("td.td-product-code").attr("data-id"),
                        index = THIS.find("td.td-product-index").text().trim(),
                        orderNum = THIS.find("td.td-product-code").attr("data-order-num"),
                        //Code = THIS.find("td.td-product-code").text().trim(),
                        Name = THIS.find("td.td-product-name").text().trim(),
                        QuantitySuggest = THIS.find("input.product-quantity.suggest").val(),
                        QuantityOrder = THIS.find("input.product-quantity.total").val(),
                        //QuantityExport = THIS.find("input.product-quantity.export").val(),
                        //QuantityImport = THIS.find("input.product-quantity.import").val();
                        // check value
                        QuantityOrder = QuantityOrder.trim() != "" ? parseInt(QuantityOrder) : 0;
                    QuantitySuggest = QuantitySuggest.trim() != "" ? parseInt(QuantitySuggest) : 0;
                    orderNum = orderNum.trim() != "" ? parseInt(orderNum) : 0;

                    //Push to array
                    arrServicesProducts.push({
                        'STT': index,
                        'STT_in_inven': orderNum,
                        'STT trong kho': orderNum,
                        'Tên': Name,
                        'Hệ thống đề xuất': QuantitySuggest,
                        'Số Order': QuantityOrder
                    })
                });//'STT trong kho', 'Tên', 'Hệ thống đề xuất', 'Số Order'
                debugger
                arrServicesProducts.sort(function (a, b) {
                    // Compare the 2 dates                    
                    if (a.STT_in_inven < b.STT_in_inven) {
                        return -1;
                    }
                    if (a.STT_in_inven > b.STT_in_inven) {

                        return 1;
                    }

                    return 0;
                });
                var indexTmp = 1;
                arrServicesProducts.forEach(function (a) {
                    a.STT = indexTmp++;
                });

                //print order with javascript
                var currentdate = new Date();
                var datetime = currentdate.getDate() + "/" + (currentdate.getMonth() + 1) + "/" + currentdate.getFullYear() + " " + currentdate.getHours() + ":" + currentdate.getMinutes();
                var doc = {
                    content: [
                        //{
                        //    columns: [
                        //        { width: 30, text: "", margin: [0, 5, 0, 5] },
                        //        {
                        //            image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAAA8CAIAAAAfXYiZAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA2ZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDpFRUQ0MDE1RDU1MjVFNTExQjZFRENCOEFENzc4QzkyRCIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo4MDUzM0U1NDg5MDYxMUU1QjQ3MUU3MzUyMjAxNTg5NyIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo4MDUzM0U1Mzg5MDYxMUU1QjQ3MUU3MzUyMjAxNTg5NyIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ1M2IChXaW5kb3dzKSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjk5ODA3REQ5ODE1NTExRTU4MUU2Q0QxQ0U5MzVDNzA5IiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjk5ODA3RERBODE1NTExRTU4MUU2Q0QxQ0U5MzVDNzA5Ii8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+5vkOZAAAFUNJREFUeNrsnHnQl2Pbx5XbLglJSlq0ke1p14iisqQspaKZQllGyxiJ7GVtUZYZI0vSnpZBjShRydZK+564lVJSspP3894fDufzu5dMzfs+ZZ7rj2uu33Wd2/E9j+V7nOd534V+//33/fK5fv311/33379QoUKU2blzJ8+85KFw4cI88JJPPnhZIL1s3GJcv/zyywEHHLDfPnsVKgCskDYVWLx+++03IAMdYRI+31MyKysroBHTn3766aCDDrI6ZbjnRnbvvwoXoFZi5JXCBzSggLSgwyeRojw/eQ9SP//8M8V4tsC8efO+//57GwSp/XOufVGz8gVL7UA2hRQmhM9T9XiflXP588ADD+SBiuD48ccfd+vWDS0TIO5q1j8KrFAiUduZc/EzXNj/Vs756UNYqAZoxQULFjRp0gRFO+aYYyyAPYJXTMA/B6zUAPVQwuF7IMjQEf0XD3rxcePGNWrU6Ouvvz7ppJOsAoh4Lu5OwD535TtoQIkABy4//vjjV1999fnnnx988ME8oyBHH310xYoVixYtGg5bu+MZhXr00UdHjx4tfI0bN+bTATlXRqz4h4Cl2ACxZs2axYsXb9++HY1ATb788kuwAK9mzZqVKVNGWEEB+FatWjV79uxhw4a9++67FFbLDj/88NNPPz0YAxWBe1+lDpKAiIAYiG+AgJ+vvfYaPxFv8+bNmzZt+uabb1Co8847r0aNGpRH0ZYuXfrRRx/Nnz9/4cKF2dnZYJHCTSOXXnrp2LFj1dDcPOuHH3445JBDDJHB4PZ2nhUMMzw3YowcObJ48eIAtGHDhiJFivCyTp06JUqUAJfJkydPnTp1+fLlcIIUGn2Tjl83P23atLPPPjt4VuBCjygpX0HzsMMO0+Xv5b6skONOIxoXEAwZMuSEE07YsmXLjh078NDghZzTp0+fNGkSL8XUyBi1YAx4Nx4OPfRQWkBlIA09e/akGOVBn684PrSPFgB64sSJGOmoUaPCNlPuujf6LBXKu9POA5aFuQETQqJN77///pgxYz744AOUJcPK0p9ggWrQlOpWr169unXrojvoJm94f+yxx9IFqAHW1q1ba9WqhTuLedIk92oHz+jRjuCTDnrdunUIwCRXrVoVBtC3b19JplFPjLzrgwDRn5Y544wz7rvvPuyLgIDKoGj08u233+L10FCwA1Yi6caNG9FZKkpiue/t0TAyj0jxkOG777476qijeMZqBgwYoGPW6CQKkUuHrh2fc5122mktWrTASQEHwREgcEw8gxpY0CzdHXnkkeXKlZOFVKpUKTzm3u/jsxyfw9UYYQmqCVK98MILKBp6JyjIg8xoHG94OOKII7DWsmXLnnjiiaVKlcIBUXH9+vUoI02BL2aFG+Iuoa9SpQqAYoDQEUwSWq/Zhqru5TljViAVPgt0sBQQIUi9/vrrsU5QoUKF8uXLEx8BBctC9YiMAEEVfurCXWbQprhoAUSIpDAy6CswzZ07F9xxVVOmTGnfvn2aLbmGsTdT1qywAvk6JsOca3TkwBgOkhQrVuz8888nOFIADUJskAIIAMITcVfXSpYsyUugQSV5oDB1ARpLXLt2LVaJR2vQoAEgonpXX311ULxY9to9pFwXCtARQfeXLh+FjHsyGVlpZkevdIMuMPlABpOiadSnefPmuqRTTz1VLaOA6zBoGaAAlu+3bdsGNFABv7oUwXtwvOiiiyhMSF2yZMlll11GR8GqQpLdSIZS4xX9NFDYWsC3h2qbFWAHFcCykOqLnAt5mjZtih7hm7A13hTJuXiD9kkXMC7MFt9kbohaEemwVsqLCDLAFUiSVqxYAWqtW7fWi2WIlMHa/u5KwJ/Ddpko3gdAEbL3PHpkhaKmBLp69eq4djw9vgZLRHg4EXdgStNGLRchnVLuFCaYEukwYbBjuJo2wy1dunTLli0xT2DNGHRM+O7NvCHCWXG9LPTLdbTILvbUacUKusJ7IU+rVq0wLhShX79+9GRivMvLfMC80ga1R0lGNOLqGAUsv4cXo7V9OoU/M82+wZ/6PqTbw+72S5ujs/iAcwHKG2+8UanSOg4rZE7hDrAyhhUw+QCCO/+8Ar7dw84qGHjXrl1XrlxJks+wyf/79OlDGKG7jMHvyfVXypKudoLFmWee2atXr06dOpkYq1wWiEV041fKVDMMKnwQ5a3uCr3RNrW73fPuUQVaM3/+/KVLl5I84Cvhuq+++iqkt3DOlbH5ske5IQLo/IJDcxEKoQinnHJKBOBYn0hjtgBJkWLoaYQygLjmpRPBm0QSGuFlt3lDeO527dq9+OKLZFR08dhjj0FxiL/xNd1q2v1VBzcHMwgE8D3wwAPEL/JnO3OpK4Up3UbkzijffvtteAOTTP4M/4ghUhfHP2nSJBJD6D7JELHVNZyM5S2FIbaSZqEXMDuDGu9JxSV0FMMZkTy4nM+oaJx5ha8MHDgQxkO/VCf/r1GjBk3RjgxGuXRk9EKwIoLxgCYyEtrnTih32LzMI4CmHif1WaTBt99+e+pEGE1qwL/lXDqgZ555BraB5AQ7w8L48eOjLhOOMH7lDlKDBw/O7XdsCtvHA3Tv3r1x48bNmjXLzs7208yZMyHGJN4E5XvuuYdZueGGGypXrgz6FCP4UoY09l85Fzr14YcfIk7//v2rVavGeDp27OiYx44dW7NmzVtvvRWI0Yb69euTlsAfO3TocPfdd9euXZsGeQlJytvBx4jd+3OBAePv27dveMc0kKURjYdhw4YZp6+88kp+1qtXj9lgZhCPnyNGjHDhpU2bNpSHuDtXrtCn3nfHjh3dunXjE0iROVGdaSczR00s0LlzZ74yGdBamjIE0dqDDz7o8BYuXIjG8bJLly5WQd+xR9ohwULLFA1w582b5/TTl+PB39EIgzTnJfOnC6PqXw4+dmh0PSoXurpu3TrS4wxXJSEIe9H+J0yYgEZQ97jjjuMnfJVu0OqJEyfyMGjQICTnPVpAedo0pDz33HPhcXyzefPmp556SlaM7aCG9AIuhDaDg9aBADEGnawkiztfJcb07oApoKtatGgRTsAqlAFx7w7ApVobQRaZY+5Voz82uAIylwFWr16NlqKfgshXXU+kL9qgyrhs2TLTHXmpoFOGcM4bUkKXKJwGnRQ/0XNESqPV+vXr5bFxEsBatG/vIEgVBLMXB5ZxJEAhDTvGGafK7V4NgtGKL226SIuw6b4nnyL+5L1vqKapLMuXLwfgIUOGTJs2DS0LHK0c3t2FF2fbDCPocqDgmJzwEMDV55g30yZ9op94gww+O+He7QXfzLMr0bHFbSNx6iB2hS2JVsJXHRWjjQwJLXaTJbZUbJDuUib0F1gREGOhhuSZ5yeffBKO17BhQ70DjVrAFMcBKU9aNyJ09ORMUktMLeOAfCNweApXTSO5sy+rx+YTb1RJ1MF20o1xLcs3FEatihUr5shx+TYCdspLXyLFQ+wqaDqmupmalbGvo9Y0aNCgbdu29jpnzpxGjRoNHz6cPjS9lDG4/qlrk6kxvlh6jnTEpijs3MYEKpsuKdQzgrJfaV+l1ipDm/hJxYxVgGA/vDde4SsJVrzECxNDYyEz9NHWbIc23TRxySRDs7LCxQTbkv4QmIcOHerggP/OO+8kHsMA0hUomuNTjx49cJ88Q6BE06HrKaMnBQt3ni4zeN7GdTRaQJH5BB9GF3hDpxZ2MKFHIWFKm0MXYjsdmyUU4rAIIIRLo1CcRnBITqFU46677lLNU878B1jqdhxX08TcQA0nxwU/BO9wxo7M/VcYWevWraNdvIA7hghZ8AEQJVRmHooXLw5RQKrp06fPmDED6NGIGJ7BQUemeJLV/Bi5KKAmlD/55JNVH1IirMTolMZ0MxDucOnjjz8+NbI8omFGl9ScNWtW0E5G1qpVKwNz2LbmHabET+0fuuRA4ZAFbJqG4RgTeABc+JGd3nvvvXIckRJNg1ecCwsbzG8mIquFpjoSnW+aigVh1A4iFotdhs/6Y78g/abWzJ49O6QCbNhaugOkSC5X8RNf8Pjjj9MUJJCg43IF/DP3ea6MhC7d1mUy0FBY1bhx41AuONdtt92md0uzbvTrk08+Abi5c+dm7F1mrDUbDSgJQUdlICuYIR1FDEltFja/bds2ZkjGn3sZ9t/W4NNvCA+jjeyvRYsWdKbni6N7KVnFqZPlM3RYMlSDB5IVyWEBJwudFQ/YqJ6IQU4KX9myZUvv3r0vuOAC5AzaJUdDcwFr69atK1euDLdVwGE8xsZk4wERCt/y6aefBgdK/TXmv3HjRh5ACr8Wu3OZ1CEMIWI2/JDhBhO59tprY3E26qfHtUqXLg24ixcvXrt27VlnncVoLr/8cvIV09E8r9jLcehBc6pWrYoN8swA0CwJhBtrhkuS0Hbt2nXq1Mnlyb+z6MydpFoSR6ITp4pjsrlfd911JFs33XQTmhW8J8PnFg5XF/Kb65K16LAuvPBC6muAcS40jWiBI8KgGvhp2SnBtGAxYnrTU3B0cfPNN+OGaerNN9+EGEcvxkE0y6kFqaBpefqsWHqjzTp16tgOXiIMKHKAsEdJYmQIGT63cBw0TgM8GZynEHi+/vrrMw6PqIm5CY6n+nQW3MlFC96/CjoWFFePzvs+ffpoSiTJJNKhwhRA02M9dpcL87ptBok4So76GzpS7pJGmzhSnLv9wkHK040QXtavX9+2pkyZwnCtn7H+mSqaeb9bPrqhoIXWDZZgO/7M09/TDrrcsWNHqmdnZ7vRG1oQB8hTRupQbVPW9oci/BlDoT4QCMqQydEmNMrWUkKeMvP0567PlEJzBAWehnfs1asX/i8mxJMgztVVV11FgSpVqjzyyCPiFeTTxDuSFYce8xa+Uuw+++wzvBWOg1BILcxfjopXplbqbiMbT/PZgFuWFymxF2UAy5McUHkTpug6doNGjRrVvXt3BMlI8ncBFkZeqlQp5xlfizkQ1wkZOjI1SGKFMMuWLcO1ExY8mBsbZakqha3l5i9WWbNmzcMPPwwFeeeddyhWpkwZNdd0J+Y57DHVytwmExNGI+4wMf1+wuuZJ+/353l1U30ePDIEayGsF7TqkHHhGuBy4YaZ8DFjxpglqFm8NJs3lgmB3kSAcGGxwBAuJqwpdc9qiubDVxcLw1HG0rPWbVO2o8pEWhr7mHEAkQL+fQOfPNqZBt9YzIncNoKjhxP+7tFuakLB3Who3749cZrBEdfdmg8BYjlMS3F8PoSpqhcRaoN/p1qgJqZLLmolmLrY5FfnJt2djL9aiE2miK16Ul0qTVWsWBHCpQZZN2NhPbKfWCPNY4kmP4Zdr149MwCyBLQMfggvh1LBoaCFgWmwkvAdjk9TdT0r3aMPyU3uXcby3FbkBlSET7liEemUC1imDTKvUMxQilhoS4+5KjZOndwz+EQ68qhI476Mv7f5u5ql22ZiSWjxJhdffDG+CUeGAyZ5xjDxVp7zYzSaZHh9l73NyzRqhcFxKircNSBwxEWKFKlUqRLVBUX2D60955xzhDv+VEj7BWXVViXVhxoH4iRimJjVHUx6usazBOp76KxWknHONlMJc+8ho0qA3aNHj3TpHjNcsWIFysXg0IIlS5aULVuWpmrWrDl27Fg4Gj3xTFhwrxjjFVO8XvXq1XnGeRPCQ61ir99TcDBS/OuAAQMAAqauALwhwigDZILhjRw5UhGg3eQ0tDB58mTfNG/enKyLxmfNmsWUM8ca7IQJE1yVJ1WiBfSABFZQRo8ezcsGDRqogx4UzsAk34MrksZJkyY1bdqU4ZbKuUiamOrOnTujF+7ouYRCNz179nzrrbc2b94MoyGn46fHbRkHoRqS+fLLL0NTyQ1atmx5xx13gJfWYUdx6ggQn3/+eZdWLrnkEtTZ9zBvCL3lSVRxBUwMDbrc2KFDB7TyiSeeACN+MuBmzZrVrVv32WefZWwoFD6XukDw9NNPIx0iMBJS8VdeecUlzHCj7hh06dIldjF2rVleaAcApavRGMWMGTPcPlEdYmGTXhkNgTnjpEks9XgmPD2pEUup0VR6AsWW7SvOmETFcPC2GacoMkaVdpQOSesB2bTxVNlzo5FVwKoAoJIhNmzYcPz48TSBZvXu3Ru9ja1wQGQmMS66NIgwG/4pJu9x2BqvCshdkqk8Ov7CyQXKujYXrWLvAwcvoQteagGjR/wFWmx6mnKnOzSGv1iw1yUZH2JTKt1b42vbtm2VJY+/sMgvEXU3tE2bNrQ4bNgwV7UCSh9c2NZtux0AS8ZbI/n27dt12Hgu1ArjRU6EF1y9u34a90EV3a2HLmfPnk1OzgzRiCdUeQ9x3bRpE2Gal3EyA7EB2upgR5u2P3PmTMJfsWLFJOjEJTwDPN5tzWBbNMXYcG0ql4ECZ5Jpg0TM+++/P79EVG9SoUKFwYMH0yL2iDMK52JUooOBAwe+9957OOBt27YxIIY+b948HDnO4o033uAn/hUZtm7diov54osvgBIngjecM2cOHaGYRsZ+/frxFUMGDhJSFJOSeHGUlKBBU6BAC4sWLfLvW0gqEJJPQ4YMIcJMnToV8SBTeDf8OvCRCTBg+n3ooYcYACm0f7gwYsQI3nvqAIxMrVetWsWsk4SsXr2a8VeuXDn3+lJBf0IXZPKWW26BKAwfPpw4xWgA3hjsbOMpmT28L+BCjooWLeqy56BBg0iGQWrp0qWMEs/NPOPvCJRNmjShBegIg0MMsCZiMkSep02bRo+oCV4cJwBkTHK5cuUoo8KSvl1xxRWISoJFj3QH7hR+6aWXaJ8sDZgI0Mwc4+crc0wj9M7IuW/YsIFQQ0BACnABenpEFajLfMAlqUW2q2FmKFdBPEs6AyLEb4zi3HPPveaaa5gK+EQsAMQ2B0Mhn2DQCxYs0BmVL19+6NChjEY7AlNAJLZicYDCe5SOeUZZ0A4+1a5dm0SaoIZWAhDY8YnJ8A80aJ8yKBcPRDE3L+gdyLBKLK5r165Aj8rTmn/ugDGSnCtOrVq1qOVSeLVq1ZhabBwmSDQsWbKkdoqAaDGuqn///sCacUi1IJ8VoKb/mwADYdLoxuVA/R+2EAu1yMDoEZUpVfMxENABKaBnMlEWsOPOaEABl4T+o/M/5Fy8R0iGS13kJFtQDCADKWeIWlgWs4LOUp424cZMIe95hiKhO/RIXbogvylRogQjROWzs7MpwwhRNN5wRxaQogqNUxcddCKxR1xb7oNWhfb8PNx/9nJVK/dyyv/rX7LuK1fs/WTE8f+Lv9TY5zUrP8bzX7D+w9f/CDAAErc2n3P9FeEAAAAASUVORK5CYII=',
                        //            width: 100, height: 60, alignment: 'center', margin: [0, 5, 0, 5],
                        //        },
                        //    ],
                        //    columnGap: 5
                        //},                        
                        {
                            columns: [
                                { width: 175, text: 'Ngày ' + datetime, fontSize: 10, italics: true, margin: [3, 3, 3, 3], alignment: 'center' }
                            ],
                        },
                        {
                            columns: [
                                { width: 175, text: '-------------------------------', alignment: 'center' }
                            ]
                        },
                        {
                            table: {
                                widths: [300],
                                body: [[{ text: 'Đơn hàng: ' + code }], [{ text: 'Loại hàng: ' + CosmeticType }]
                                    , [{ text: 'Kho đề xuất: ' + fromInventory }]
                                    , [{ text: 'Ghi chú: ' }]
                                    , [{ text: note }]]
                            },
                            layout: 'noBorders'
                        },
                        {
                            columns: [
                                { width: 175, text: '-------------------------------', alignment: 'center' }
                            ]
                        },
                        {
                            table: {
                                headerRows: 1,
                                widths: [50, 50, 300, 50, 50],
                                body: buildTableBody(arrServicesProducts, ['STT', 'STT trong kho', 'Tên', 'Hệ thống đề xuất', 'Số Order'])
                            },
                            //layout: 'noBorders'
                        },
                        //{
                        //    columns: [
                        //        { width: 175, text: '-------------------------------', alignment: 'center' }
                        //    ]
                        //},                       
                        {
                            text: ' \n\n\n\n   .'
                        },
                    ],
                    pageMargins: [20, 20, 20, 20],
                    pageSize: 'letter',
                    pageOrientation: 'portrait'
                };
                pdfMake.createPdf(doc).print();
                    },
                    //func khi chon khong
                    funcNo: function (instance, toast) {
                        return;
                    }


                };
                 var fromInventory = $('#fromInventory option:selected').text();
                 ShowConfirm("confirm", 100000, "Xác nhận", "Bạn đang in đơn hàng tại salon [ " + fromInventory + " ] ?", "Có", "Không", confirm);
            }

            function buildTableBody(data, columns) {
                var body = [];
                var header = [{ text: columns[0], bold: true }, { text: columns[1], bold: true }, { text: columns[2], bold: true }
                    , { text: columns[3], bold: true }, { text: columns[4], bold: true }];
                body.push(header);
                data.forEach(function (row) {
                    var dataRow = [];
                    columns.forEach(function (column) {
                        let alignment = 'left';
                        //if (column == 'Số Order') {
                        //    alignment = 'right';
                        //}
                        let record = {
                            text: row[column].toString(),
                            alignment: alignment
                        }
                        dataRow.push(record);
                    })
                    body.push(dataRow);
                });
                return body;
            }

            function PrintTurn2Order() {
                var arrServicesProducts = [];
                var code = $('#Code').val();
                var CosmeticType = $('#CosmeticType option:selected').text();
                var fromInventory = $('#fromInventory option:selected').text();
                var toInventory = $('#toInventory option:selected').text();
                var note = $('#Note').val();


                $("table.table-item-product tbody tr").each(function () {
                    var THIS = $(this);
                    debugger
                    var Id = THIS.find("td.td-product-code").attr("data-id"),
                        index = THIS.find("td.td-product-index").text().trim(),
                        orderNum = THIS.find("td.td-product-code").attr("data-order-num"),
                        barCode = THIS.find("td.td-bar-code").text().trim(),
                        Name = THIS.find("td.td-product-name").text().trim(),
                        QuantitySuggest = THIS.find("input.product-quantity.suggest").val(),
                        QuantityOrder = THIS.find("input.product-quantity.total").val(),
                        QuantityExport = THIS.find("input.product-quantity.export").val(),
                        //QuantityImport = THIS.find("input.product-quantity.import").val();
                        // check value
                        QuantityOrder = QuantityOrder.trim() != "" ? parseInt(QuantityOrder) : 0;
                    QuantitySuggest = QuantitySuggest.trim() != "" ? parseInt(QuantitySuggest) : 0;
                    orderNum = orderNum.trim() != "" ? parseInt(orderNum) : 0;
                    var reasonWH = THIS.find("select.form-control.selectWH").find(":selected").text();

                    //Push to array
                    arrServicesProducts.push({
                        'STT': index,
                        'STT_in_inven': orderNum,
                        'STT trong kho': orderNum,
                        'Mã barcode': barCode,
                        'Tên SP': Name,
                        //'Hệ thống đề xuất': QuantitySuggest,
                        'Số Order': QuantityOrder,
                        'Số xuất': QuantityExport,
                        'Ghi chú từ kho': reasonWH
                    })
                });//'STT trong kho', 'Tên', 'Hệ thống đề xuất', 'Số Order'
                debugger
                arrServicesProducts.sort(function (a, b) {
                    // Compare the 2 dates                    
                    if (a.STT_in_inven < b.STT_in_inven) {
                        return -1;
                    }
                    if (a.STT_in_inven > b.STT_in_inven) {

                        return 1;
                    }

                    return 0;
                });
                var indexTmp = 1;
                arrServicesProducts.forEach(function (a) {
                    a.STT = indexTmp++;
                });

                //print order with javascript
                var currentdate = new Date();
                var datetime = currentdate.getDate() + "/" + (currentdate.getMonth() + 1) + "/" + currentdate.getFullYear() + " " + currentdate.getHours() + ":" + currentdate.getMinutes();
                var doc = {
                    content: [
                        {
                            columns: [
                                { width: 175, text: 'Ngày ' + datetime, fontSize: 10, italics: true, margin: [3, 3, 3, 3], alignment: 'center' }
                            ],
                        },
                        {
                            columns: [
                                { width: 175, text: '-------------------------------', alignment: 'center' }
                            ]
                        },
                        {
                            table: {
                                widths: [300],
                                body: [[{ text: 'Loại đơn: Đơn hàng đưa shipper giao Salon.' }]
                                    , [{ text: 'Mã đơn hàng: ' + code }]
                                    , [{ text: 'Loại hàng: ' + CosmeticType }]
                                    , [{ text: 'Kho đề xuất: ' + fromInventory }]
                                    , [{ text: 'Ghi chú: ' }]
                                    , [{ text: note }]]
                            },
                            layout: 'noBorders'
                        },
                        {
                            columns: [
                                { width: 175, text: '-------------------------------', alignment: 'center' }
                            ]
                        },
                        {
                            table: {
                                headerRows: 1,
                                widths: [15, 30, 50, 280, 30, 30, 60],
                                body: buildTableBodyTurn2(arrServicesProducts, ['STT', 'STT trong kho', 'Mã barcode', 'Tên SP', 'Số Order', 'Số xuất', 'Ghi chú từ kho'])
                            },
                            //layout: 'noBorders'
                        },
                        //{
                        //    columns: [
                        //        { width: 175, text: '-------------------------------', alignment: 'center' }
                        //    ]
                        //},                       
                        {
                            text: ' \n\n\n\n   .'
                        },
                    ],
                    pageMargins: [20, 20, 20, 20],
                    pageSize: 'letter',
                    pageOrientation: 'portrait'
                };
                pdfMake.createPdf(doc).print();
            }

            // pdfmake
            function buildTableBodyTurn2(data, columns) {
                var body = [];
                var header = [{ text: columns[0], bold: true }, { text: columns[1], bold: true }, { text: columns[2], bold: true }
                    , { text: columns[3], bold: true }, { text: columns[4], bold: true }
                    , { text: columns[5], bold: true }, { text: columns[6], bold: true }];
                body.push(header);
                data.forEach(function (row) {
                    var dataRow = [];
                    columns.forEach(function (column) {
                        let alignment = 'left';
                        //if (column == 'Số Order') {
                        //    alignment = 'right';
                        //}
                        let record = {
                            text: row[column].toString(),
                            alignment: alignment
                        }
                        dataRow.push(record);
                    })
                    body.push(dataRow);
                });
                return body;
            }

            function getDataOrderNum() {
                debugger;
                //
                $.ajax({
                    type: "GET",
                    contentType: "application/json; charset=utf-8",
                    datatype: "JSON",
                    url: URL_API_INVENTORY + "/api/orderNum/listForOrder?ListInventoryId=" + $('#toInventory option:selected').val(),
                    success: function (response) {
                        var data = response;
                        console.log(data);
                        if (data != null) {
                            index = 1;
                            for (var i = 0; i < data.length; i++) {
                        <%--<option data-label="<%= item.Label %>" value="<%= item.Value %>"><%= item.Label %></option>--%>                        
                                var tr = '<option data-label="' + data[i].productId + '" value="' + data[i].orderNum + '">' + data[i].productId + '</option>';
                                debugger;
                                $("#DropDownOrderNum").append($(tr));
                            }
                        }
                        debugger;
                    }
                });
            }


            var elms = {
                wrSearch: '.wr-search',
                calendarDate: '.calendar.date',
                calendarTime: '.calendar.time',
                inputSearch: '.wr-search input',
                inputSearchCategoryErrorHandle: '.category-error-handle .wr-search input',
                inputSearchCategoryErrorSub: '.category-error-sub .wr-search input',
                inputSearchCategoryErrorRoot: '.category-error-root .wr-search input',
                inputSearchStaffReport: '.staff-report .wr-search input',
                inputSearchSalonError: '.salon-error .wr-search input',
                inputSearchDepartmentError: '.departmenr-error-sub .wr-search input',
                inputTimeError: '.time-error .time input',
                inputDateError: '.time-error .date input',
                resultSearch: '.dropdown.result',
                inputNote: '.note input',
                submit: '.button.submit',
                btnClearImage: '.evidence-error .clear',
                btnChooseImage: '.choose-image',
                inputFile: '#files'
            };
            //choose image
            $(elms.btnChooseImage).click(function () {
                console.log("choose image");
                $(elms.inputFile).click();

            });

            function BindImage() {
                //Check File API support
                if (window.File && window.FileList && window.FileReader) {
                    $('#files').on("change", function (event) {
                        var files = event.target.files; //FileList object
                        var output = document.getElementById("result");
                        for (var i = 0; i < files.length; i++) {
                            var file = files[i];
                            //Only pics
                            // if(!file.type.match('image'))
                            console.log(file);
                            if (file.type.match('image.*')) {
                                if (this.files[0].size > 2097152) {
                                    //resize images
                                    // imageData =
                                }
                                // continue;
                                var picReader = new FileReader();
                                picReader.addEventListener("load", function (event) {
                                    var picFile = event.target;
                                    var imageData = picFile.result;
                                    // console.log(imageData);

                                    var div = document.createElement("div");
                                    div.innerHTML = "<img class='thumbnail' width='300px' src='" + imageData + "'" +
                                        "title='preview image'/><div class='close-img'><span class='close-imgs' onclick='CloseImage($(this))'>x</span></div";

                                    console.log(div);
                                    output.insertBefore(div, null);
                                });
                                //Read the image
                                $('#result').show();
                                picReader.readAsDataURL(file);

                            } else {
                                //alert("You can only upload image file.");
                                ShowMessage("Thông báo", "Đề nghị chỉ được phép up file ảnh", 3, 10000);
                                $(this).val("");
                            }
                        }

                    });
                } else {
                    console.log("Your browser does not support File API");
                }
            };

            function CloseImage(This) {
                var pr = This.parent().parent();
                pr.remove();
            }

            function uploadImagesToS3(data) {
                // console.log(JSON.stringify(data));
                let result = '';
                let url = URL_API_DOMAINS3 + "/api/s3/multiUpload";
                $.ajax({
                    contentType: "application/json",
                    type: "POST",
                    url: url,
                    dataType: "json",
                    async: false,
                    crossDomain: true,
                    data: JSON.stringify(data),
                    success: function (response, textStatus, xhr) {
                        if (response) {
                            result = response;
                        }
                    },
                    error: function (jqXhr, textStatus, errorMessager) {
                        console.log(errorMessager + "\n" + jqXhr.responseJSON);
                        //alert("upload images lên S3 bị lỗi. Bạn hãy thông báo với Admin");
                        ShowMessage("Thông báo", "upload images lên S3 bị lỗi. Bạn hãy thông báo với Admin", 3, 10000);
                    }
                });
                console.log(result);
                return result;
            }
        </script>
    </asp:Panel>
</asp:Content>


