﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Web.Helpers;
using System.Text.RegularExpressions;
using System.Globalization;
using LinqKit;
using Project.Helpers;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;
using _30shine.Helpers.Http;
using Libraries;

namespace Project.GUI.BackEnd.IvInventory.IvOrder
{
    public partial class Order_Status : System.Web.UI.Page
    {
        private string PageID = "";
        protected bool Perm_ShowSalon = false;
        protected bool Perm_AllSalon = false;
        private bool Perm_Access = false;
        protected bool IsAccountant = false;
        protected bool Perm_ViewAllData = false;
        protected bool Perm_ReviewOrder = false;
        protected bool Perm_Edit = false;
        protected bool Perm_Delete = false;



        private List<ProductBasic> ProductList = new List<ProductBasic>();
        private List<ProductBasic> ServiceList = new List<ProductBasic>();
        protected int SalonId = 0;
        private List<int> lstFreeService = new List<int>();
        protected int Id;
        private int integer;
        private CultureInfo culture = new CultureInfo("vi-VN");
        protected List<Tbl_Config> ListIvReason = new List<Tbl_Config>();

        protected OutputOrder OBJ;
        protected string ListProduct;
        private static Order_Status instance;
        /// <summary>
        /// get instance, phục vụ trường hợp có hàm statistic, sử dụng instance có thẻ gọi các method không phải static
        /// </summary>
        /// <returns></returns>
        public static Order_Status getInstance()
        {
            if (!(Order_Status.instance is Order_Status))
            {
                Order_Status.instance = new Order_Status();
            }

            return Order_Status.instance;
        }
        protected List<_30shine.MODEL.ENTITY.EDMX.IvInventory> ListIvInventoryTypeSalon = new List<_30shine.MODEL.ENTITY.EDMX.IvInventory>();
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                if (Request.QueryString["Code"] != null)
                {
                    PageID = "DHK_SL_EDIT";
                }
                else
                {
                    PageID = "DV_SLDH_ADD";
                }
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_ShowSalon = permissionModel.GetActionByActionNameAndPageId("Perm_ShowSalon", PageID, permission);
                //Perm_AllSalon = permissionModel.GetActionByActionNameAndPageId("Perm_AllSalon", PageID, permission);
                //ExecuteByPermission();

                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ShowSalon = permissionModel.CheckPermisionByAction("Perm_ShowSalon", pageId, staffId);
                Perm_AllSalon = permissionModel.CheckPermisionByAction("Perm_AllSalon", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_ReviewOrder = permissionModel.CheckPermisionByAction("Perm_ReviewOrder", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (IsAccountant)
            {
                UIHelpers.Redirect("/don-dat-hang/danh-sach", null);
            }
            else
            {
                Id = getId();
                if (!IsPostBack)
                {
                    ListProduct = getProducts();
                    Library.Function.bindOrderType(new List<DropDownList> { OrderType }, false);
                    Library.Function.bindCosmeticType(new List<DropDownList> { CosmeticType }, false);
                    Library.Function.bindInventoryOrder(new List<DropDownList> { fromInventory }, Perm_ViewAllData);
                    Library.Function.bindInventoryPartner(new List<DropDownList> { toInventory });
                    ListIvReason = Library.Function.bindInventoryReason(new List<DropDownList> { }, false);
                    Library.Function.bindInventoryTypeSalon(ListIvInventoryTypeSalon);

                    //Library.Function.bindSalon(new List<DropDownList>() { Salon }, Perm_AllSalon);
                    Bind_OBJ();
                    //Bind_RptProduct();
                    //Bind_RptProductFeatured();
                    //bindProductCategory();
                    PassDataFromServerToClient();
                    //TxtDateTimeFrom.Text = String.Format("{0:dd/MM/yyyy}", DateTime.Today);
                    TxtDateTimeFrom.Enabled = false;
                }


            }
        }

        /// <summary>
        /// get Id của lô chuyển mỹ phẩm
        /// </summary>
        /// <returns></returns>
        private int getId()
        {
            return int.TryParse(Request.QueryString["Id"], out integer) ? integer : 0;
        }

        /// <summary>
        /// get Id đăng nhập và chuyển mỹ phẩm
        /// </summary>
        /// <returns></returns>
        private int getExportUserId()
        {
            if (Session["User_Id"] != null)
            {
                return int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// get Id người nhận mỹ phẩm
        /// </summary>
        /// <returns></returns>
        //private int getRecipientId()
        //{
        //    return int.TryParse(Recipient.Value, out integer) ? integer : 0;
        //}


        /// <summary>
        /// Cập nhật lô mỹ phẩm
        /// </summary>
        /// <param name="Id"></param>
        private void Update(int Id)
        {
            if (Id > 0)
            {
                try
                {
                    if (HDF_ProductIds.Value == "" || HDF_ProductIds.Value == "[]")
                    {
                        TriggerJsMsgSystem_temp(this, "Chuỗi sản phẩm không hợp lệ", 4);
                    }
                    else
                    {
                        var createdDate = HDF_CreatedDate.Value;

                        var serialize = new JavaScriptSerializer();
                        var ProductList = serialize.Deserialize<List<IvOrderDetail>>(HDF_ProductIds.Value);
                        var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                        //                       

                        var data = new Request().RunPutAsync(
                             Libraries.AppConstants.URL_API_INVENTORY + "/api/order/update",
                             new
                             {
                                 id = Convert.ToInt32(HDF_OrderId.Value),
                                 code = "",
                                 inventoryOrderId = Convert.ToInt32(HDF_InventoryOrderId.Value),
                                 inventoryPartnerId = Convert.ToInt32(HDF_InventoryPartnerId.Value),
                                 orderType = Convert.ToInt32(HDF_OrderType.Value),
                                 status = Convert.ToInt32(HDF_OrderStatus.Value),
                                 note = Note.Text,
                                 isAuto = 0,
                                 CosmeticType = Convert.ToInt32(HDF_CosmeticType.Value),
                                 userId = staffId,
                                 listOrderDetail = ProductList
                             }
                         ).Result;
                        if (data.IsSuccessStatusCode)
                        {
                            var result = data.Content.ReadAsStringAsync().Result;
                            var data1 = new JavaScriptSerializer().DeserializeObject(result);

                            if (data1.ToString().Equals("StatusChanged"))
                            {
                                TriggerJsMsgSystem_temp(this, "Đơn hàng đã bị cập nhật, nhấn F5 để cập nhật trạng thái mới nhất!", 4);
                            }
                            else
                            {
                                //thanh cong
                                //TriggerJsMsgSystem_temp(this, "Cập nhật đơn hàng thành công", 2);
                                //reloadPage();
                                var MsgParam = new List<KeyValuePair<string, string>>();
                                MsgParam.Add(new KeyValuePair<string, string>("msg_status", "2"));
                                MsgParam.Add(new KeyValuePair<string, string>("msg_message", "Cập nhật đơn hàng thành công!"));
                                //thanh cong   
                                //neu dua ve trang danh sach
                                //UIHelpers.Redirect("/don-dat-hang/danh-sach.html", MsgParam);
                                UIHelpers.Redirect("/don-dat-hang/trang-thai-don-hang/" + Id + ".html", MsgParam);
                            }
                        }
                        else
                        {
                            TriggerJsMsgSystem_temp(this, "Cập nhật đơn hàng thất bại!", 4);
                        }
                    }
                }
                catch (Exception e)
                {
                    new _30shine.Helpers.PushNotiErrorToSlack().PushDefault(e.Message + ", MAIN: " + e.StackTrace, this.ToString() + ".updateOrder", "");
                    TriggerJsMsgSystem_temp(this, "Đã có lỗi xảy ra. Vui lòng thử lại sau!", 4);
                }
            }
        }
        //[WebMethod]
        //public static void Getdata(string DateTimeFrom, string DateTimeTo, string Code, string InventoryOrder, string InventoryPartner, string OrderType,string OrderStatus,string CosmeticType,string Routing)
        //{
        //    //try
        //    //{
        //    //    //    /GUI/BackEnd/IvInventory/IvOrder/Order_Status.aspx/ClickChangeStatus
        //    //    var instance = Order_Status.getInstance();
        //    //    instance.ChangeStatus(Id, status, goToListPage);
        //    //}
        //    //catch (Exception e)
        //    //{
        //    //    throw e;
        //    //}

        //    var response = new Request().RunGetAsyncV1(
        //      Libraries.AppConstants.URL_API_INVENTORY + "/api/order/list?FromDate=" + DateTimeFrom
        //      + "&ToDate=" + DateTimeTo
        //      + "&InventoryOrderId=" + InventoryOrder
        //      + "&InventoryPartnerId=" + InventoryPartner
        //      + "&Code=" + Code
        //      + "&OrderType=" + OrderType
        //      + "&Status=" + OrderStatus
        //      + "&CosmeticType=" + CosmeticType
        //      + "&RoutingID=" + Routing
        //      ).Result;
        //    if (response.IsSuccessStatusCode)
        //    {
        //        var result = response.Content.ReadAsStringAsync().Result;
        //        var data = new JavaScriptSerializer().Deserialize<List<OutputOrder>>(result);

        //        //var data = response.Content.ReadAsAsync<ResponseData>().Result.data.ToList();
        //        if (data != null)
        //        {
                   
        //        }
        //    }
        //}
        private void ChangeStatus(int Id, int status, bool goToListPage)
        {
            if (Id > 0)
            {
                try
                {
                    if (HDF_ProductIds.Value == "" || HDF_ProductIds.Value == "[]")
                    {
                        TriggerJsMsgSystem_temp(this, "Chuỗi sản phẩm không hợp lệ", 4);
                    }
                    else
                    {
                        var serialize = new JavaScriptSerializer();
                        var ProductList = serialize.Deserialize<List<IvOrderDetail>>(HDF_ProductIds.Value);
                        var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                        var imgs = HDF_Imgs.Value;
                        //                       

                        var data = new Request().RunPutAsync(
                             Libraries.AppConstants.URL_API_INVENTORY + "/api/order/changeStatus",
                             new
                             {
                                 id = Convert.ToInt32(HDF_OrderId.Value),
                                 code = "",
                                 inventoryOrderId = Convert.ToInt32(HDF_InventoryOrderId.Value),
                                 inventoryPartnerId = Convert.ToInt32(HDF_InventoryPartnerId.Value),
                                 orderType = Convert.ToInt32(HDF_OrderType.Value),
                                 status = status,
                                 note = Note.Text,
                                 isAuto = 0,
                                 CosmeticType = Convert.ToInt32(HDF_CosmeticType.Value),
                                 userId = staffId,
                                 listOrderDetail = ProductList,
                                 imgs = imgs
                             }
                         ).Result;
                        if (data.IsSuccessStatusCode)
                        {
                            var result = data.Content.ReadAsStringAsync().Result;
                            var data1 = new JavaScriptSerializer().DeserializeObject(result);

                            if (data1.ToString().Equals("DuplicatedRequest"))
                            {
                                TriggerJsMsgSystem_temp(this, "Đơn hàng đã có trạng thái này, nhấn F5 để cập nhật trạng thái mới nhất!", 4);
                            }
                            else
                            {
                                var MsgParam = new List<KeyValuePair<string, string>>();
                                MsgParam.Add(new KeyValuePair<string, string>("msg_status", "2"));
                                MsgParam.Add(new KeyValuePair<string, string>("msg_message", "Cập nhật đơn hàng thành công!"));
                                //thanh cong   
                                if (goToListPage)
                                {
                                    //neu dua ve trang danh sach
                                    UIHelpers.Redirect("/don-dat-hang/danh-sach.html", MsgParam);
                                }
                                else
                                {
                                    UIHelpers.Redirect("/don-dat-hang/trang-thai-don-hang/" + Id + ".html", MsgParam);
                                }

                                //TriggerJsMsgSystem_temp(this, "Cập nhật đơn hàng thành công", 2);
                                //reloadPage();
                            }
                        }
                        else
                        {
                            TriggerJsMsgSystem_temp(this, "Cập nhật đơn hàng thất bại!", 4);
                        }

                    }
                }
                catch (Exception e)
                {
                    new _30shine.Helpers.PushNotiErrorToSlack().PushDefault(e.Message + ", MAIN: " + e.StackTrace, this.ToString() + ".updateOrder", "");
                    TriggerJsMsgSystem_temp(this, "Đã có lỗi xảy ra. Vui lòng thử lại sau!", 4);
                }
            }
        }

        private void reloadPage()
        {
            ListProduct = getProducts();
            Bind_OBJ();
        }


        protected void ActionReview(object sender, EventArgs e)
        {
            if (Id > 0)
            {
                ChangeStatus(Id, AppConstants.OrderStatus.WAIT_IMPORT, true);
            }

        }

        protected void ActionUpdate(object sender, EventArgs e)
        {
            if (Id > 0)
            {
                Update(Id);
            }
        }

        protected void ActionBeginOrder(object sender, EventArgs e)
        {
            if (Id > 0)
            {
                ChangeStatus(Id, AppConstants.OrderStatus.ORDERED, false);
            }
        }

        protected void ActionComplete(object sender, EventArgs e)
        {
            if (Id > 0)
            {
                ChangeStatus(Id, AppConstants.OrderStatus.IMPORTED, true);
            }
        }




        private bool Bind_OBJ()
        {
            var isset = false;
            using (var db = new Solution_30shineEntities())
            {
                if (Id > 0)
                {
                    var response = new Request().RunGetAsyncV1(Libraries.AppConstants.URL_API_INVENTORY + "/api/order/detail?OrderId=" + Id).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var result = response.Content.ReadAsStringAsync().Result;
                        var data = new JavaScriptSerializer().Deserialize<ResponseOrderDetail>(result);

                        if (data != null)
                        {
                            OBJ = data.Order;
                            //
                            TxtDateTimeFrom.Text = String.Format("{0:dd/MM/yyyy}", data.Order.CreatedDate);
                            HDF_OrderId.Value = data.Order.Id.ToString();
                            HDF_OrderStatus.Value = data.Order.Status.ToString();
                            //
                            HDF_CreatedDate.Value = String.Format("{0:dd/MM/yyyy HH24:mm:ss}", data.Order.CreatedDate);
                            DateTime dt = DateTime.ParseExact(HDF_CreatedDate.Value, "dd/MM/yyyy HH24:mm:ss", CultureInfo.CurrentCulture);
                            //
                            Code.Text = data.Order.Code;
                            Note.Text = data.Order.Note;
                            OrderType.SelectedValue = data.Order.OrderType.ToString();
                            HDF_OrderType.Value = data.Order.OrderType.ToString();
                            //
                            fromInventory.SelectedValue = data.Order.InventoryOrderId.ToString();
                            HDF_InventoryOrderId.Value = data.Order.InventoryOrderId.ToString();
                            toInventory.SelectedValue = data.Order.InventoryPartnerId.ToString();
                            HDF_InventoryPartnerId.Value = data.Order.InventoryPartnerId.ToString();
                            //
                            CosmeticType.SelectedValue = data.Order.CosmeticType.ToString();
                            HDF_CosmeticType.Value = data.Order.CosmeticType.ToString();
                            //
                            if ((OBJ.InventoryOrderType == AppConstants.InventoryType.SALON &&
                                OBJ.InventoryPartnerType == AppConstants.InventoryType.SALON)
                                || OBJ.InventoryOrderType == AppConstants.InventoryType.STAFF
                                || OBJ.InventoryPartnerType == AppConstants.InventoryType.STAFF)
                            {
                                //ko can cap nhat ly do
                                HDF_CheckReason.Value = "0";
                            }
                            else
                            {
                                HDF_CheckReason.Value = "1";
                            }
                            //
                            if (OBJ != null && OBJ.Status >= 4
                                        && ((OBJ.InventoryOrderType == 2 && OBJ.InventoryPartnerType == 1 && OBJ.OrderType == 1)
                                        || (OBJ.InventoryOrderType == 1 && OBJ.InventoryPartnerType == 2 && OBJ.OrderType == 2)
                                        || (OBJ.InventoryOrderType == 2 && OBJ.InventoryPartnerType == 4 && OBJ.OrderType == 1)))
                            {
                                HDF_CheckUpload.Value = "1";
                            }
                            else
                            {
                                HDF_CheckUpload.Value = "0";
                            }
                            HDF_Imgs.Value = OBJ.Imgs;                            
                            //
                            Rpt_Product_Flow.DataSource = data.OrderDetail;
                            Rpt_Product_Flow.DataBind();
                            ListingProductWp.Style.Add("display", "block");
                        }
                    }
                }
            }

            return isset;
        }

        /// <summary>
        /// Lấy danh sách product
        /// </summary>
        /// <returns></returns>
        private string getProducts()
        {
            using (var db = new Solution_30shineEntities())
            {
                var data = db.Products.Where(w => (w.IsDelete != 1 && w.Publish == 1) && w.CheckCombo != true && w.CategoryId != 98).ToList();// && w.CategoryId == 20) || (w.Id == 378 || w.Id == 63 || w.Id == 44 || w.Id == 58 || w.Id == 83 || w.Id == 440 || w.Id == 32 || w.Id == 446)).ToList();
                var serialize = new JavaScriptSerializer();
                return serialize.Serialize(data);
            }
        }


        //public string GenBillCode(int salonId, string prefix, int template = 4)
        //{
        //    using (var db = new Solution_30shineEntities())
        //    {
        //        string code = "";
        //        string temp = "";
        //        int integer;
        //        var Where = PredicateBuilder.True<QLKho_SalonOrder>();
        //        DateTime today = DateTime.Today;
        //        Where = Where.And(w => w.IsDelete != true && w.OrderDate == today);
        //        if (salonId > 0)
        //        {
        //            Where = Where.And(w => w.SalonId == salonId);
        //        }
        //        var LastBill = db.QLKho_SalonOrder.AsExpandable().Where(Where).OrderByDescending(w => w.Id).Take(1).ToList();
        //        if (LastBill.Count > 0)
        //        {
        //            if (LastBill[0].BillCode != null && LastBill[0].BillCode != "")
        //                temp = (int.TryParse(LastBill[0].BillCode.Substring(LastBill[0].BillCode.Length - template, template), out integer) ? ++integer : 0).ToString();
        //            else
        //                temp = "1";
        //        }
        //        else
        //        {
        //            temp = "1";
        //        }

        //        int len = temp.Length;
        //        for (var i = 0; i < template; i++)
        //        {
        //            if (len > 0)
        //            {
        //                code = temp[--len].ToString() + code;
        //            }
        //            else
        //            {
        //                code = "0" + code;
        //            }
        //        }

        //        return prefix + String.Format("{0:ddMMyy}", DateTime.Now) + code;
        //    }
        //}


        private void PassDataFromServerToClient()
        {
            List<ElementEnable> list = listElement();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jSon = serializer.Serialize(list);
            string script = String.Format("<script type=\"text/javascript\">var listElement={0}</script>", jSon);
            if (!this.ClientScript.IsClientScriptBlockRegistered("clientScript"))
            {
                this.ClientScript.RegisterClientScriptBlock(this.GetType(), "clientScript", script, false);
            }
        }
        protected List<ElementEnable> listElement()
        {
            List<ElementEnable> list = new List<ElementEnable>();
            list.Add(new ElementEnable() { ElementName = "salonSelect", Enable = !Perm_ShowSalon, Type = "hidden" });
            //list.Add(new ElementEnable() { ElementName = "maSPcol", Enable = !Perm_ShowSalon, Type = "hidden" });
            return list;
        }

        /// <summary>
        /// Trả thông báo về client 
        /// </summary>
        /// <param name="_OBJ"></param>
        /// <param name="msg"></param>
        /// <param name="status"></param>        
        public void TriggerJsMsgSystem_temp(Page _OBJ, string msg, int status)
        {
            ScriptManager.RegisterStartupScript(_OBJ, _OBJ.GetType(), "Message System", "ShowMessage('Thông báo','" + msg + "'," + status + ");", true);
        }

    }


}