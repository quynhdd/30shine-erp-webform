﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Web.Helpers;
using System.Text.RegularExpressions;
using System.Globalization;
using LinqKit;
using Project.Helpers;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;
using _30shine.Helpers.Http;
using Libraries;

namespace Project.GUI.BackEnd.IvInventory.IvOrder
{
    public partial class Order_Item : System.Web.UI.Page
    {
        private string PageID = "";
        protected bool Perm_ShowSalon = false;
        protected bool Perm_AllSalon = false;
        private bool Perm_Access = false;
        protected bool IsAccountant = false;
        protected bool Perm_ViewAllData = false;


        private List<ProductBasic> ProductList = new List<ProductBasic>();
        private List<ProductBasic> ServiceList = new List<ProductBasic>();
        protected int SalonId = 0;
        private List<int> lstFreeService = new List<int>();
        protected int Id;
        protected string Type;
        private int integer;
        private CultureInfo culture = new CultureInfo("vi-VN");

        protected OutputOrder OBJ;
        protected string ListProduct;
        protected List<_30shine.MODEL.ENTITY.EDMX.IvInventory> ListIvInventoryTypeSalon = new List<_30shine.MODEL.ENTITY.EDMX.IvInventory>();
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                if (Request.QueryString["Code"] != null)
                {
                    PageID = "DHK_SL_EDIT";
                }
                else
                {
                    PageID = "DV_SLDH_ADD";
                }
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_ShowSalon = permissionModel.GetActionByActionNameAndPageId("Perm_ShowSalon", PageID, permission);
                //Perm_AllSalon = permissionModel.GetActionByActionNameAndPageId("Perm_AllSalon", PageID, permission);
                //ExecuteByPermission();

                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageIdSpecial().Replace(Request.RawUrl, ".html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ShowSalon = permissionModel.CheckPermisionByAction("Perm_ShowSalon", pageId, staffId);
                Perm_AllSalon = permissionModel.CheckPermisionByAction("Perm_AllSalon", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (IsAccountant)
            {
                UIHelpers.Redirect("/don-dat-hang/danh-sach", null);
            }
            else
            {
                Id = getId();
                if (!IsPostBack)
                {
                    ListProduct = getProducts();
                    Library.Function.bindOrderType(new List<DropDownList> { OrderType }, false);
                    Library.Function.bindCosmeticType(new List<DropDownList> { CosmeticType }, false);

                    Library.Function.bindInventoryOrder(new List<DropDownList> { fromInventory }, Perm_ViewAllData);
                    Library.Function.bindInventoryPartner(new List<DropDownList> { toInventory });
                    Library.Function.bindInventoryTypeSalon(ListIvInventoryTypeSalon);
                    

                    //Library.Function.bindSalon(new List<DropDownList>() { Salon }, Perm_AllSalon);
                    Bind_OBJ();
                    //Bind_RptProduct();
                    //Bind_RptProductFeatured();
                    //bindProductCategory();
                    PassDataFromServerToClient();
                    TxtDateTimeFrom.Text = String.Format("{0:dd/MM/yyyy}", DateTime.Today);
                    TxtDateTimeFrom.Enabled = false;
                }


            }
        }

        /// <summary>
        /// get Id của lô chuyển mỹ phẩm
        /// </summary>
        /// <returns></returns>
        private int getId()
        {
            return int.TryParse(Request.QueryString["Id"], out integer) ? integer : 0;
        }

        private string getType()
        {
            return Request.QueryString["Type"];
        }

        /// <summary>
        /// get Id đăng nhập và chuyển mỹ phẩm
        /// </summary>
        /// <returns></returns>
        private int getExportUserId()
        {
            if (Session["User_Id"] != null)
            {
                return int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// get Id người nhận mỹ phẩm
        /// </summary>
        /// <returns></returns>
        private int getRecipientId()
        {
            return int.TryParse(Recipient.Value, out integer) ? integer : 0;
        }

        /// <summary>
        /// Xuất lô mỹ phẩm
        /// </summary>
        private void Add()
        {
            try
            {
                if (HDF_ProductIds.Value == "" || HDF_ProductIds.Value == "[]")
                {
                    TriggerJsMsgSystem_temp(this, "Chuỗi sản phẩm không hợp lệ", 4);
                }
                else
                {
                    var serialize = new JavaScriptSerializer();
                    var ProductList = serialize.Deserialize<List<IvOrderDetail>>(HDF_ProductIds.Value);
                    //
                    //var test = new
                    //{
                    //    id = 0,
                    //    code = "",
                    //    inventoryOrderId = Convert.ToInt32(HDF_InventoryOrderId.Value),
                    //    inventoryPartnerId = Convert.ToInt32(HDF_InventoryPartnerId.Value),
                    //    orderType = Convert.ToInt32(HDF_OrderType.Value),
                    //    status = AppConstants.OrderStatus.ORDERED,
                    //    note = Note.Text,
                    //    isAuto = 0,
                    //    listOrderDetail = ProductList
                    //};
                    var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;

                    var data = new Request().RunPostAsync(
                         Libraries.AppConstants.URL_API_INVENTORY + "/api/order/add",
                         new
                         {
                             id = 0,
                             code = "",
                             inventoryOrderId = Convert.ToInt32(HDF_InventoryOrderId.Value),
                             inventoryPartnerId = Convert.ToInt32(HDF_InventoryPartnerId.Value),
                             orderType = Convert.ToInt32(HDF_OrderType.Value),
                             status = AppConstants.OrderStatus.ORDERED,
                             note = Note.Text,
                             isAuto = 0,
                             CosmeticType = Convert.ToInt32(HDF_CosmeticType.Value),
                             userId = staffId,
                             listOrderDetail = ProductList
                         }
                     ).Result;
                    if (data.IsSuccessStatusCode)
                    {
                        //thanh cong
                        //TriggerJsMsgSystem_temp(this, "Thêm mới đơn hàng thành công", 2);
                        var MsgParam = new List<KeyValuePair<string, string>>();
                        MsgParam.Add(new KeyValuePair<string, string>("msg_status", "2"));
                        MsgParam.Add(new KeyValuePair<string, string>("msg_message", "Thêm mới đơn hàng thành công!"));
                        //thanh cong                           
                        //neu dua ve trang danh sach
                        UIHelpers.Redirect("/don-dat-hang/danh-sach.html", MsgParam);
                        //var result = data.Content.ReadAsStringAsync().Result;
                        ////check data trả về từ api
                        //if ("Success!".Equals(result))
                        //{
                        //    //thanh cong
                        //    TriggerJsMsgSystem_temp(this, "Thêm mới đơn hàng thành công", 2);
                        //}
                        //else
                        //{
                        //    TriggerJsMsgSystem_temp(this, "Thêm mới đơn hàng thất bại!", 4);
                        //}
                    }
                    else
                    {
                        TriggerJsMsgSystem_temp(this, "Thêm mới đơn hàng thất bại!", 4);
                    }
                }
            }
            catch (Exception e)
            {
                new _30shine.Helpers.PushNotiErrorToSlack().PushDefault(e.Message + ", MAIN: " + e.StackTrace, this.ToString() + ".addOrder", "");
                TriggerJsMsgSystem_temp(this, "Đã có lỗi xảy ra. Vui lòng thử lại sau!", 4);
            }
        }

        /// <summary>
        /// Cập nhật lô mỹ phẩm
        /// </summary>
        /// <param name="Id"></param>
        private void Update(int Id)
        {
            if (Id > 0)
            {
                try
                {
                    if (HDF_ProductIds.Value == "" || HDF_ProductIds.Value == "[]")
                    {
                        TriggerJsMsgSystem_temp(this, "Chuỗi sản phẩm không hợp lệ", 4);
                    }
                    else
                    {
                        var serialize = new JavaScriptSerializer();
                        var ProductList = serialize.Deserialize<List<IvOrderDetail>>(HDF_ProductIds.Value);
                        var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                        //                       

                        var data = new Request().RunPutAsync(
                             Libraries.AppConstants.URL_API_INVENTORY + "/api/order/update",
                             new
                             {
                                 id = Convert.ToInt32(HDF_OrderId.Value),
                                 code = "",
                                 inventoryOrderId = Convert.ToInt32(HDF_InventoryOrderId.Value),
                                 inventoryPartnerId = Convert.ToInt32(HDF_InventoryPartnerId.Value),
                                 orderType = Convert.ToInt32(HDF_OrderType.Value),
                                 status = AppConstants.OrderStatus.ORDERED,
                                 note = Note.Text,
                                 isAuto = 0,
                                 CosmeticType = Convert.ToInt32(HDF_CosmeticType.Value),
                                 userId = staffId,
                                 listOrderDetail = ProductList
                             }
                         ).Result;
                        if (data.IsSuccessStatusCode)
                        {
                            //thanh cong
                            TriggerJsMsgSystem_temp(this, "Cập nhật đơn hàng thành công", 2);
                        }
                        else
                        {
                            TriggerJsMsgSystem_temp(this, "Cập nhật đơn hàng thất bại!", 4);
                        }
                    }
                }
                catch (Exception e)
                {
                    new _30shine.Helpers.PushNotiErrorToSlack().PushDefault(e.Message + ", MAIN: " + e.StackTrace, this.ToString() + ".updateOrder", "");
                    TriggerJsMsgSystem_temp(this, "Đã có lỗi xảy ra. Vui lòng thử lại sau!", 4);
                }

            }
        }


        protected void Action(object sender, EventArgs e)
        {
            if (Id > 0)
            {
                Update(Id);
            }
            else
            {
                Add();
            }
        }

        private bool Bind_OBJ()
        {
            var isset = false;
            using (var db = new Solution_30shineEntities())
            {
                if (Id > 0)
                {
                    var response = new Request().RunGetAsyncV1(Libraries.AppConstants.URL_API_INVENTORY + "/api/order/detail?OrderId=" + Id).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var result = response.Content.ReadAsStringAsync().Result;
                        var data = new JavaScriptSerializer().Deserialize<ResponseOrderDetail>(result);

                        if (data != null)
                        {
                            OBJ = data.Order;
                            //
                            TxtDateTimeFrom.Text = String.Format("{0:dd/MM/yyyy}", data.Order.CreatedDate);
                            HDF_OrderId.Value = data.Order.Id.ToString();
                            //
                            Code.Text = data.Order.Code;
                            OrderType.SelectedValue = data.Order.OrderType.ToString();
                            HDF_OrderType.Value = data.Order.OrderType.ToString();
                            //
                            fromInventory.SelectedValue = data.Order.InventoryOrderId.ToString();
                            HDF_InventoryOrderId.Value = data.Order.InventoryOrderId.ToString();
                            toInventory.SelectedValue = data.Order.InventoryPartnerId.ToString();
                            HDF_InventoryPartnerId.Value = data.Order.InventoryPartnerId.ToString();
                            //
                            CosmeticType.SelectedValue = data.Order.CosmeticType.ToString();
                            HDF_CosmeticType.Value = data.Order.CosmeticType.ToString();
                            //
                            Rpt_Product_Flow.DataSource = data.OrderDetail;
                            Rpt_Product_Flow.DataBind();
                            ListingProductWp.Style.Add("display", "block");
                        }
                    }

                }
            }

            return isset;
        }

        /// <summary>
        /// Lấy danh sách product
        /// </summary>
        /// <returns></returns>
        private string getProducts()
        {
            using (var db = new Solution_30shineEntities())
            {
                var data = db.Products.Where(w => (w.IsDelete != 1 && w.Publish == 1) && w.CheckCombo != true && w.CategoryId != 98).ToList();// && w.CategoryId == 20) || (w.Id == 378 || w.Id == 63 || w.Id == 44 || w.Id == 58 || w.Id == 83 || w.Id == 440 || w.Id == 32 || w.Id == 446)).ToList();
                var serialize = new JavaScriptSerializer();
                return serialize.Serialize(data);
            }
        }


        public string GenBillCode(int salonId, string prefix, int template = 4)
        {
            using (var db = new Solution_30shineEntities())
            {
                string code = "";
                string temp = "";
                int integer;
                var Where = PredicateBuilder.True<QLKho_SalonOrder>();
                DateTime today = DateTime.Today;
                Where = Where.And(w => w.IsDelete != true && w.OrderDate == today);
                if (salonId > 0)
                {
                    Where = Where.And(w => w.SalonId == salonId);
                }
                var LastBill = db.QLKho_SalonOrder.AsExpandable().Where(Where).OrderByDescending(w => w.Id).Take(1).ToList();
                if (LastBill.Count > 0)
                {
                    if (LastBill[0].BillCode != null && LastBill[0].BillCode != "")
                        temp = (int.TryParse(LastBill[0].BillCode.Substring(LastBill[0].BillCode.Length - template, template), out integer) ? ++integer : 0).ToString();
                    else
                        temp = "1";
                }
                else
                {
                    temp = "1";
                }

                int len = temp.Length;
                for (var i = 0; i < template; i++)
                {
                    if (len > 0)
                    {
                        code = temp[--len].ToString() + code;
                    }
                    else
                    {
                        code = "0" + code;
                    }
                }

                return prefix + String.Format("{0:ddMMyy}", DateTime.Now) + code;
            }
        }


        private void PassDataFromServerToClient()
        {
            List<ElementEnable> list = listElement();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jSon = serializer.Serialize(list);
            string script = String.Format("<script type=\"text/javascript\">var listElement={0}</script>", jSon);
            if (!this.ClientScript.IsClientScriptBlockRegistered("clientScript"))
            {
                this.ClientScript.RegisterClientScriptBlock(this.GetType(), "clientScript", script, false);
            }
        }
        protected List<ElementEnable> listElement()
        {
            List<ElementEnable> list = new List<ElementEnable>();
            list.Add(new ElementEnable() { ElementName = "salonSelect", Enable = !Perm_ShowSalon, Type = "hidden" });
            //list.Add(new ElementEnable() { ElementName = "maSPcol", Enable = !Perm_ShowSalon, Type = "hidden" });
            return list;
        }

        /// <summary>
        /// Trả thông báo về client 
        /// </summary>
        /// <param name="_OBJ"></param>
        /// <param name="msg"></param>
        /// <param name="status"></param>        
        public void TriggerJsMsgSystem_temp(Page _OBJ, string msg, int status)
        {
            ScriptManager.RegisterStartupScript(_OBJ, _OBJ.GetType(), "Message System", "ShowMessage('Thông báo','" + msg + "'," + status + ");", true);
        }

    }

    public partial class ResponseOrderDetail
    {
        public OutputOrder Order { get; set; }

        public List<OutputOrderDetail> OrderDetail { get; set; }
    }
    public partial class OutputOrderDetail
    {
        public int Id { get; set; }
        public int OrderId { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string ProductCode { get; set; }
        public string BarCode { get; set; }
        public int QuantityType1 { get; set; }
        public int QuantityType2 { get; set; }
        public int QuantityType3 { get; set; }
        public int QuantityType4 { get; set; }
        public int QuantityType5 { get; set; }
        public int QuantitySuggest { get; set; }
        public int QuantityOrder { get; set; }
        public int QuantityExport { get; set; }
        public int QuantityImport { get; set; }
        public int Cost { get; set; }
        public bool IsAuto { get; set; }
        public DateTime CreatedDate { get; set; }
        public int SalonReason { get; set; }
        public int WHReason { get; set; }
        public int OrderNum { get; set; }
    }
}