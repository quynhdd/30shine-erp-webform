﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Order_Item.aspx.cs" Inherits="Project.GUI.BackEnd.IvInventory.IvOrder.Order_Item" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
    <title>Quản lý kho - Lập đơn đặt hàng</title>
    <link href="/Assets/css/bootstrap/bootstrap-toggle.min.css" rel="stylesheet" />
    <script src="/Assets/js/bootstrap/bootstrap-toggle.min.js"></script>
    <script src="/Assets/js/common.js"></script>
    <link rel="stylesheet" href="/Assets/izitoast/css/iziToast.min.css" />
    <script src="/Assets/izitoast/js/iziToast.min.js" type="text/javascript"></script>

    <script src="/Assets/libs/magicsuggest-master/magicsuggest.js"></script>
    <link href="/Assets/libs/magicsuggest-master/magicsuggest.css" rel="stylesheet" />

    <style>
        .fe-service .tr-product .table-listing-product tbody input.product-quantity {
            border-bottom: 1px solid #ddd !important;
        }

        .suggestion-wrap {
            width: 420px;
            float: left;
        }

        .ms-sel-ctn input[type='text'] {
            height: auto !important;
            line-height: 24px !important;
            border: none !important;
        }

        .ms-ctn .ms-sel-item {
            line-height: 22px;
            color: #000000;
            font-size: 13px;
        }

        .ms-helper {
            display: none !important;
        }

        .input-quantity-wrap {
            float: left;
            margin-left: 10px;
        }

        .input-quantity {
            width: 90px;
            line-height: 36px;
            padding: 0 10px;
            border: 1px solid #ddd;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
        }

        .btn-add {
            float: left;
            background: #ddd;
            padding: 9px 15px;
            margin-left: 10px;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            cursor: pointer;
        }

        #success-alert {
            top: 100px;
            right: 10px;
            position: fixed;
            width: 20% !important;
            z-index: 2000;
        }
    </style>

    <script>
        var itemProduct;
        var quantity;
        var index;
        var ms;
        var fromDate;
        var toDate;
        var salonId;
        var URL_API_INVENTORY;
        jQuery(document).ready(function () {
            //
            //ShowMessage("abc", "bbbb", 1);
            //            
            var qs = getQueryStrings();
            var type = qs["type"];
            debugger
            if ("1" == type) {
                $("#OrderType").val("1").change();
                $("#CosmeticType").val("1").change();
                $("#OrderType").prop("disabled", true);
                $("#spanOrderType").text("từ kho");
                $("#titleOrder").text("Lập đơn nhập hàng");

            }
            else if ("2" == type) {
                $("#OrderType").val("2").change();
                $("#CosmeticType").val("1").change();
                $("#OrderType").prop("disabled", true);
                $("#spanOrderType").text("sang kho");
                $("#titleOrder").text("Lập đơn xuất hàng");
            }
            else {
                $("#OrderType").val("2").change();
                $("#CosmeticType").val("2").change();
                $("#spanOrderType").text("sang kho");
                $("#titleOrder").text("Lập đơn xuất hàng");
            }

            $("#success-alert").hide();
            // call 
            URL_API_INVENTORY = "<%= Libraries.AppConstants.URL_API_INVENTORY %>";
            //getDataQuantityOfUse();
            // bind data to dropdownlist
            ms = $('#ms').magicSuggest({
                maxSelection: 1,
                data: <%=ListProduct %>,
                valueField: 'Id',
                displayField: 'Name',
            });

            //get data product level of use
            //Author: QuynhDD
            <%--function getDataQuantityOfUse() {
                fromDate = "<%= Session["fromDate"]%>";
                toDate = "<%= Session["toDate"]%>";
                salonId = "<%= Session["salonId"]%>";
                salonId = salonId.toString().trim() != "" ? parseInt(salonId) : 0;
                if (fromDate != null && salonId > 0) {
                    $.ajax({
                        type: "GET",
                        contentType: "application/json; charset=utf-8",
                        datatype: "JSON",
                        url: URL_API_INVENTORY + "/api/inventory-manager/get-level-of-use?fromDate=" + fromDate + "&toDate=" + toDate + "&salonId=" + salonId + "",
                        success: function (response) {
                            var data = response.data;
                            console.log(data);
                            index = 1;
                            for (var i = 0; i < data.length; i++) {
                                var tr = '<tr>' +
                                    '<td>' + index++ + '</td>' +
                                    '<td class="td-product-code maSPcol" data-id="' + data[i].id + '">' + data[i].code + '</td>' +
                                    '<td class="td-product-name">' + data[i].name + '</td>' +
                                    '<td class="td-product-quantity map-edit">' +
                                    '<input class="product-quantity" value="' + parseInt(data[i].quantity) + '" type="text" onkeypress="return isNumber(event)" placeholder="Số lượng"/>' +
                                    '<div class="edit-wp">' +
                                    '<a class="elm del-btn" onclick="RemoveItem($(this).parent().parent().parent(),\'<%# Eval("Name") %>\',\'product\')" href="javascript://" title="Xóa"></a>' +
                                    '</div>' +
                                    '</td>' +
                                    '</tr>';
                                $("#table-item-product tbody").append($(tr));
                                $("#ListingProductWp").show();
                            }
                        }
                    });
                }
            }--%>

            $(ms).on('selectionchange', function (e, m) {
                itemProduct = null;
                var listProduct = this.getSelection();
                console.log(listProduct);
                var listData = ms.getData();
                if (listProduct.length > 0) {
                    for (var i = 0; i < listData.length; i++) {
                        if (listProduct[0].Id == listData[i].Id) {
                            itemProduct = listProduct[0];
                            $("#inputQuantity").focus();
                            break;
                        }
                        else {
                            continue;
                        }
                    }
                    if (itemProduct == null) {
                        showMsgSystem("Lễ tân chú ý chọn đúng sản phẩm trong danh sách vật tư dịch vụ.", "warning");
                        ms.clear();
                        $(".ms-sel-ctn input[type='text']").focus();
                    }
                }
            });
        });

        function addProduct() {
            var kt = true;
            var error = '';
            quantity = $("#inputQuantity").val();
            if (quantity == "" || quantity <= "0") {
                ShowMessage("Thông báo", "Nhập số lượng sản phẩm và phải lớn hơn 0!", 3);
                $('#inputQuantity').css("border-color", "red");
                return;
            }

            // check trùng sản phẩm
            // check trùng sản phẩm
            $("table.table-item-product tbody tr").each(function () {
                var THIS = $(this);
                var productId = THIS.find("td.td-product-code").attr("data-id");
                if (productId != "undefined" && itemProduct.Id == productId) {
                    kt = false;
                    ShowMessage("Thông báo", "Sản phẩm đã tồn tại trong danh sách. Vui lòng kiểm tra lại!", 3);
                    return;
                }
            });
            if (!kt) {
                return;
            }
            //$("table.table-item-product tbody tr").each(function () {
            //    var THIS = $(this);
            //    var productId = THIS.find("td.td-product-code").attr("data-id");
            //    if (productId != "undefined" && itemProduct.Id == productId) {
            //        kt = false;
            //        error += "Sản phẩm đã tồn tại trong danh sách. Vui lòng thêm số lượng!";
            //    }
            //});
            //// !kt xuất hiện thông báo
            //if (!kt) {
            //    $("#success-alert").fadeTo(2000, 2000).slideUp(2000, function () {
            //        $("#success-alert").slideUp(2000);
            //    });
            //    $("#msg-alert").text(error);
            //}
            //else {
            index = $("#table-item-product tbody tr").length + 1;
            if (itemProduct != null && quantity > 0) {
                if (itemProduct.Code != undefined) {
                    var tr = '<tr>' +
                        '<td>' + index + '</td>' +
                        '<td class="td-product-code maSPcol" data-id="' + itemProduct.Id + '">' + itemProduct.Code + '</td>' +
                        '<td class="td-product-name">' + itemProduct.Name + '</td>' +
                        '<td class="td-product-order" data-order="' + itemProduct.Order + '" style="display:none;"> ' + itemProduct.Order + ' </td>' +
                        '<td class="td-product-quantity map-edit">' +
                        '<input class="product-quantity" value="' + quantity + '" type="text" onkeypress="return isNumber(event)" placeholder="Số lượng"/>' +
                        '<div class="edit-wp">' +
                                    '<a class="elm del-btn" onclick="RemoveItem($(this).parent().parent().parent(),\'<%# Eval("Name") %>\',\'product\')" href="javascript://" title="Xóa"></a>' +
                        '</div>' +
                        '</td>' +
                        '</tr>';
                    $("#table-item-product tbody").append($(tr));
                    $("#ListingProductWp").show();
                    //itemProduct.Order = itemProduct.Order.toString().trim() != ""? parseInt(Order) : 0;
                    getProductIds();
                    resetInputForm();
                    //checkElement();
                }
                else {
                    showMsgSystem("Lễ tân chú ý chọn đúng sản phẩm trong danh sách vật tư dịch vụ.", "warning");
                    ms.clear();
                    $(".ms-sel-ctn input[type='text']").focus();
                }

            }
            //}
        }

        function getProductIds() {
            var Ids = [];
            var prd = {};
            $("table.table-item-product tbody tr").each(function () {
                prd = {};
                var THIS = $(this);

                var Id = THIS.find("td.td-product-code").attr("data-id"),
                    Code = THIS.find("td.td-product-code").text().trim(),
                    Name = THIS.find("td.td-product-name").text().trim(),
                    Quantity = THIS.find("input.product-quantity").val(),
                    Order = THIS.find("td.td-product-order").attr("data-order"),

                    // check value
                    Id = Id.toString().trim() != "" ? parseInt(Id) : 0;
                Quantity = Quantity.trim() != "" ? parseInt(Quantity) : 0;
                //Order = Order.toString().trim() != "" ? parseInt(Order) : 0;

                prd.ProductId = Id;
                prd.Code = Code;
                prd.Name = Name;
                prd.QuantityOrder = Quantity;
                //prd.Order = Order;

                Ids.push(prd);

            });
            Ids = JSON.stringify(Ids);
            $("#HDF_ProductIds").val(Ids);
        }

        function resetInputForm() {
            ms.clear();
            $("#inputQuantity").val("");
            $(ms).focus();
            $(".ms-sel-ctn input[type='text']").focus();
        }
    </script>
</asp:Content>

<asp:Content ID="ServiceAdd" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <%--<div class="alert alert-danger" id="success-alert">
            <span id="msg-alert"></span>
        </div>--%>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý đơn đặt hàng &nbsp;&#187; Đơn đặt hàng</li>
                        <li class="li-listing"><a href="/don-dat-hang/danh-sach.html">Danh sách</a></li>
                        <li class="li-add"><a href="/don-dat-hang/them-moi-don-nhap-hang.html?type=1">Tạo đơn nhập hàng</a></li>
                        <li class="li-add"><a href="/don-dat-hang/them-moi-don-xuat-hang.html?type=2">Tạo đơn xuất hàng</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add admin-product-add fe-service">
            <%-- Add --%>
            <div class="wp960 content-wp">
                <!-- System Message -->
                <%--<asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>--%>
                <!-- END System Message -->

                <div class="table-wp">
                    <table class="table-add admin-product-table-add fe-service-table-add">
                        <tbody>
                            <tr class="title-head">
                                <% if (OBJ != null)
                                    { %>
                                <td><strong>Chỉnh sửa đơn hàng</strong></td>
                                <% }
                                    else
                                    { %>
                                <td><strong id="titleOrder">Lập đơn đặt hàng</strong></td>
                                <% }%>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr class="tr-margin" style="height: 20px;"></tr>
                            <tr>
                                <td class="col-xs-2 left"><span>Ngày</span></td>
                                <td class="col-xs-9 right">
                                    <table>
                                        <tr>
                                            <td>
                                                <div class="datepicker-wp">
                                                    <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeFrom" placeholder="Chọn ngày"
                                                        ClientIDMode="Static" runat="server" Style="width: 180px;"></asp:TextBox>
                                                </div>
                                            </td>
                                            <% if (OBJ != null)
                                                { %>
                                            <td>
                                                <span style="padding-right: 10px; padding-left: 10px; width: 100px;">Mã đơn hàng</span>
                                            </td>
                                            <td>
                                                <%--<div class="input-quantity-wrap">--%>
                                                <asp:TextBox ID="Code" runat="server" CssClass="txtFilter form-control" ClientIDMode="Static" Style="width: 180px;" Enabled="false"></asp:TextBox>
                                                <%--<input type="text" id="Code" class="" disabled/>--%>
                                                <%--</div>--%>
                                            </td>
                                            <% }%>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr class="tr-field-ahalf" runat="server" id="TrSalon">
                                <td class="col-xs-2 left"></td>
                                <td class="col-xs-9 right">
                                    <%--<label style="float: left; line-height: 36px; padding-right: 10px; font-weight: normal; font-family: Roboto Condensed Bold;">Ngày</label>
                                    <div class="datepicker-wp">
                                        <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeFrom" placeholder="Chọn ngày"
                                            ClientIDMode="Static" runat="server" Style="width: 160px; margin-right: 10px;"></asp:TextBox>
                                    </div>--%>
                                    <table>
                                        <tr>
                                            <td>
                                                <div class="salonSelect">
                                                    <div class="field-wp" style="float: left; width: auto;">
                                                        <asp:DropDownList ID="fromInventory" CssClass="form-control select" runat="server" ClientIDMode="Static" Style="width: 160px;"></asp:DropDownList>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <span style="padding-right: 10px; padding-left: 10px; width: 100px;"></span>
                                            </td>
                                            <td>
                                                <div class="filter-item">
                                                    <asp:DropDownList ToolTip="Loại đơn hàng" ID="OrderType" CssClass="select form-control" runat="server" ClientIDMode="Static" Style="width: 80px;"></asp:DropDownList>
                                                </div>
                                            </td>

                                            <td><span id="spanOrderType" style="padding-right: 10px; padding-left: 10px; width: 80px;">
                                                <% if (OBJ != null && OBJ.OrderType == 2)
                                                    { %>
                                sang Kho
                                <% }
                                    else
                                    { %>
                                từ Kho
                                <% }%></span> </td>
                                            <td>
                                                <div class="salonSelect">

                                                    <div class="field-wp" style="float: left; width: auto;">
                                                        <asp:DropDownList ID="toInventory" CssClass="form-control select" runat="server" ClientIDMode="Static" Style="width: 160px;"></asp:DropDownList>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>

                                    </table>


                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-2 left"><span>Loại hàng</span></td>
                                <td class="col-xs-9 right">
                                    <div class="field-wp" style="float: left; width: auto;">
                                        <asp:DropDownList ID="CosmeticType" CssClass="form-control select" runat="server" ClientIDMode="Static" Style="width: 160px;"></asp:DropDownList>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-2 left"><span>Sản phẩm</span></td>
                                <td class="col-xs-9 right">
                                    <div class="suggestion-wrap">
                                        <input id="ms" class="form-control" type="text" />
                                    </div>
                                    <div class="input-quantity-wrap">
                                        <input type="number" id="inputQuantity" class="input-quantity" onkeypress="return isNumber(event)" placeholder="Số lượng" />
                                    </div>
                                    <div class="btn-add" onclick="addProduct()">Thêm sản phẩm</div>
                                </td>
                            </tr>



                            <tr class="tr-field-ahalf tr-product">
                                <td class="col-xs-2 left"><span></span></td>
                                <td class="col-xs-9 right">
                                    <div class="listing-product item-product" id="ListingProductWp" runat="server" clientidmode="Static">
                                        <table class="table table-listing-product table-item-product" id="table-item-product">
                                            <thead>
                                                <tr>
                                                    <th>STT</th>
                                                    <th class="maSPcol">Mã sản phẩm</th>
                                                    <th>Tên sản phẩm</th>
                                                    <th style="width: 100px">Số lượng</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater ID="Rpt_Product_Flow" runat="server">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td class="td-product-index"><%# Container.ItemIndex + 1 %></td>
                                                            <td class="td-product-code maSPcol" style="width: 120px" data-id="<%# Eval("ProductId") %>"><%# Eval("ProductCode") %></td>
                                                            <td class="td-product-name"><%# Eval("ProductName") %></td>
                                                            <td class="td-product-quantity map-edit">
                                                                <input type="text" class="product-quantity" value="<%# Eval("QuantityOrder") %>" />
                                                                <div class="edit-wp">
                                                                    <a class="elm del-btn" onclick="RemoveItem($(this).parent().parent().parent(),'<%# Eval("ProductName") %>','product')"
                                                                        href="javascript://" title="Xóa"></a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                            </tr>

                            <tr class="tr-description">
                                <td class="col-xs-2 left"><span>Ghi chú</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox TextMode="MultiLine" Rows="5" ID="Note" runat="server" ClientIDMode="Static"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>



                            <tr class="tr-send">
                                <td class="col-xs-2 left"></td>
                                <td class="col-xs-9 right no-border">
                                    <span class="field-wp">
                                        <asp:Panel ID="BtnSend" CssClass="btn-send" runat="server" ClientIDMode="Static">Hoàn Tất</asp:Panel>
                                        <asp:Button ID="BtnFakeSend" OnClick="Action" runat="server" Text="Hoàn tất"
                                            ClientIDMode="Static" Style="display: none;"></asp:Button>
                                        <%--<span class="btn-send btn-cancel" id="CalcelAddFbcv" style="margin-left: 12px;" >Hủy</span>--%>
                                    </span>
                                </td>
                            </tr>
                            <asp:HiddenField runat="server" ID="UrlKeyField" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="ImgCoverField" ClientIDMode="Static" />
                        </tbody>
                    </table>

                    <!-- input hidden -->
                    <asp:HiddenField ID="HDF_OrderId" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_ProductIds" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_ServiceIds" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_UserImages" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_UserImagesDelete" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="Recipient" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HairMassage" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="Cosmetic" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_InventoryOrderId" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_InventoryPartnerId" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_OrderType" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_lgId" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_BillId" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_FreeService" ClientIDMode="Static" runat="server" />
                    <asp:HiddenField ID="HDF_Rating" ClientIDMode="Static" runat="server" />
                    <asp:HiddenField ID="HDF_CosmeticType" runat="server" ClientIDMode="Static" />

                    <!-- END input hidden -->
                </div>
            </div>
            <select id="inventorySalon" style="visibility: hidden">
                <% foreach (var item in ListIvInventoryTypeSalon)
                    { %>
                <!--thêm Mapid-->
                <option data-key="<%= item.Id %>" data-price="<%= item.Name %>" value="<%= item.Id %>"></option>
                <% } %>
            </select>
            <%-- end Add --%>
        </div>

        <script src="/assets/js/jquery.mCustomScrollbar.js"></script>
        <link href="/assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" />
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 180px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 35px !important;
                padding-top: 1px !important;
            }
        </style>
        <script type="text/javascript">
            $(document).keydown(function (e) {
                if (e.keyCode == '13') {
                    $('.btn-add').click();
                    $('#ms').focus();
                    e.cancelBubble = true;
                    return false;
                }
            });
            jQuery(document).ready(function () {
                //checkElement();
                // Add active menu
                $("#glbDatHangNoiBo").addClass("active");
                $("#glbDatHangNoiBo_ChoSalon").addClass("active");
                if ($("#subMenu li.li-edit").length > 0) {
                    $("#subMenu li.li-edit").addClass("active");
                }
                else {
                    $("#subMenu li.li-add").addClass("active");
                }

                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) { },
                    scrollMonth: false,
                    scrollTime: false,
                    scrollInput: false
                });
                $("#OrderType").bind("change", function () {
                    //
                    if ($("#OrderType").val() == 1) {
                        $("#spanOrderType").text("từ Kho");
                    } else {
                        $("#spanOrderType").text("sang Kho");
                    }
                });

                // Btn Send
                $("#BtnSend").bind("click", function () {
                    getProductIds();
                    //var kt = true;
                    //var error = '';
                    var fromInventory = $('#fromInventory').val();
                    var toInventory = $('#toInventory').val();
                    var dataProduct = $("#HDF_ProductIds").val();
                    if (fromInventory == "" || fromInventory == "0" || toInventory == "" || toInventory == "0") {
                        //kt = false;
                        //error += " [Bạn chưa chọn Salon!], ";
                        //$('#salonId').css("border-color", "red");
                        ShowMessage("Thông báo", "Bạn chưa chọn Kho!", 3);
                        return;
                    }
                    var salonOrder = $("#inventorySalon").find("option[data-key='" + fromInventory + "']");
                    var salonPartner = $("#inventorySalon").find("option[data-key='" + toInventory + "']");
                    //
                    if (salonOrder != null && salonOrder.val() != null && salonPartner != null && salonPartner.val() != null) {
                        ShowMessage("Thông báo", "Không được phép chuyển hàng giữa các salon!", 3);
                        return;
                    }

                    if (dataProduct == "[]") {
                        //kt = false;
                        //error += " [Bạn chưa chọn mỹ phẩm!]";
                        ShowMessage("Thông báo", "Bạn chưa chọn sản phẩm!", 3);
                        return;
                    }
                    $('#HDF_InventoryOrderId').val($('#fromInventory').val());
                    $('#HDF_InventoryPartnerId').val($('#toInventory').val());
                    $('#HDF_OrderType').val($('#OrderType').val());
                    $('#HDF_CosmeticType').val($('#CosmeticType').val());

                    //if (!kt) {
                    //    $("#success-alert").fadeTo(2000, 2000).slideUp(2000, function () {
                    //        $("#success-alert").slideUp(2000);
                    //    });
                    //    $("#msg-alert").text(error);
                    //}
                    //else {
                    $("#BtnFakeSend").click();
                    //$("#table-item-product").empty();
                    //}
                });

                // Init execute service, product quantity
                //getProductIds();

                //============================
                // Show System Message
                //============================ 
                //var qs = getQueryStrings();
                //showMsgSystem(qs["msg_update_message"], qs["msg_update_status"]);
            });

            //function showMsgSystem(msg, status) {
            //    $("#MsgSystem").css("opacity", 0).text(msg).addClass(status).fadeTo(0, 1);
            //    setTimeout(function () {
            //        $("#MsgSystem").fadeTo("slow", 0, function () {
            //            $("#MsgSystem").text("").attr("class", "msg-system").css("opacity", 1);
            //        });
            //    }, 5000);
            //}

            //function getProductIds() {
            //    var Ids = [];
            //    var prd = {};
            //    $("table.table-item-product tbody tr").each(function () {
            //        prd = {};
            //        var THIS = $(this);
            //        var Id = THIS.find("td.td-product-code").attr("data-id"),
            //            Code = THIS.find("td.td-product-code").text().trim(),
            //            Name = THIS.find("td.td-product-name").text().trim(),
            //            //Price = THIS.find(".td-product-price").attr("data-price"),
            //            Quantity = THIS.find("input.product-quantity").val(),
            //            VoucherPercent = THIS.find("input.product-voucher").val(),
            //            //Order = THIS.find("td.td-product-order").attr("data-order"),
            //            Promotion = 0;

            //        THIS.find(".promotion-money input[type='checkbox']:checked").each(function () {
            //            var value = $(this).attr("data-value").trim();
            //            Promotion += (value != "" ? parseInt(value) : 0);
            //        });

            //        // check value
            //        Id = Id.toString().trim() != "" ? parseInt(Id) : 0;
            //        //Price = Price.trim() != "" ? parseInt(Price) : 0;
            //        //Quantity = Quantity.trim() != "" ? parseInt(Quantity) : 0;
            //        VoucherPercent = !isNaN(parseInt(VoucherPercent)) ? parseInt(VoucherPercent) : 0;
            //        //Order = Order.toString().trim() != "" ? parseInt(Id) : 0;

            //        prd.Id = Id;
            //        prd.Code = Code;
            //        prd.Name = Name;
            //        //prd.Price = Price;
            //        prd.Quantity = Quantity;
            //        prd.VoucherPercent = VoucherPercent;
            //        //prd.Order = Order;
            //        prd.Promotion = Promotion;

            //        Ids.push(prd);

            //    });
            //    Ids = JSON.stringify(Ids);
            //    $("#HDF_ProductIds").val(Ids);
            //}

            // Update table display
            function UpdateItemDisplay(itemName) {
                var dom = $(".table-item-" + itemName);
                var len = dom.find("tbody tr").length;
                if (len == 0) {
                    dom.parent().hide();
                } else {
                    UpdateItemOrder(dom.find("tbody"));
                }
            }

            // Update order item
            function UpdateItemOrder(dom) {
                var index = 1;
                dom.find("tr").each(function () {
                    $(this).find("td.td-product-index").text(index);
                    index++;
                });
            }

            // Remove item đã được chọn
            function RemoveItem(THIS, name, itemName) {
                // Confirm
                $(".confirm-yn").openEBPopup();

                $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn hủy [ " + name + " ] ?");
                $("#EBPopup .yn-yes").bind("click", function () {
                    var Code = THIS.find(".td-staff-code").attr("data-code");
                    $("#quick-" + itemName).find("input[data-code='" + Code + "']").removeAttr("checked");
                    THIS.remove();
                    getProductIds();
                    UpdateItemDisplay(itemName);
                    autoCloseEBPopup(0);
                });
                $("#EBPopup .yn-no").bind("click", function () {
                    autoCloseEBPopup(0);
                });
            }
            //function checkElement() {
            //    for (var j = 0; j < listElement.length; j++) {
            //        var nameElement = listElement[j].ElementName;
            //        var enabled = listElement[j].Enable;
            //        var type = listElement[j].Type;
            //        if (type == "hidden" && enabled == true) {
            //            $("." + nameElement).addClass('hidden');
            //        }
            //        else {
            //            $("." + nameElement).prop('disabled', enabled);
            //        }
            //    }
            //}
        </script>
    </asp:Panel>
</asp:Content>


