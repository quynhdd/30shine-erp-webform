﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/TemplateMaster/SiteMasterVersion1.Master" CodeBehind="ReportsInventory.aspx.cs" Inherits="_30shine.GUI.BackEnd.IvInventory.ReportsInventory" %>

<asp:Content ID="CustomerAdd" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel ID="ContentWrap" runat="server" ClientIDMode="Static">
        <div class="container-fluid sub-menu">
            <div class="row">
                <div class="col-md-12 pt-3 pb-2">
                    <ul>
                        <li>Báo cáo &nbsp;&#187;&nbsp; Hàng tồn kho</li>
                    </ul>
                </div>
                <div class="col-md-12 form-group">
                    <div class="sub-menu-boder"></div>
                </div>
            </div>
        </div>
        <form runat="server">
            <div class="container-fluid">
                <div class="row">
                    <div class="form-group col-xl-auto col-lg-auto">
                        <strong><i class="fa fa-filter"></i>Lọc kết quả &nbsp;&#187; </strong>
                    </div>
                    <div class="col-xl-auto form-group col-md-auto col-lg-auto col-md-auto text-center">
                        <div class="float-left menu-time text-center" onclick="ClickToday()">
                            <label class="label">Hôm nay</label><br />
                            <span class="today"></span>
                        </div>
                    </div>
                    <div class="col-xl-auto col-lg-auto col-md-auto col-sm-auto form-group">
                        <input type="text" name="fromDate" placeholder="Từ ngày" class="txtDateTime form-control reset-table" />
                    </div>
                    <div class="col-xl-auto col-lg-auto col-md-auto col-sm-auto form-group">
                        <input type="text" name="toDate" placeholder="Đến ngày" class="txtDateTime form-control reset-table" />
                    </div>

                    <div class="col-xl-auto col-lg-auto col-md-12 col-sm-auto form-group">
                        <label class="mr-2 lable-text">Loại kho</label>
                        <asp:DropDownList ID="ddlInventoryType" onchange="GetListInventory($(this))" runat="server" ClientIDMode="Static" CssClass="select reset-table"></asp:DropDownList>
                    </div>
                    <div class="col-xl-auto col-lg-auto col-md-12 col-sm-auto form-group">
                        <label class="mr-2 lable-text">Kho</label>
                        <select class="form-control select" id="ddlInventory"></select>
                    </div>
                    <div class="col-xl-auto col-lg-auto col-md-12 col-sm-auto form-group">
                        <asp:DropDownList ID="ddlStatus" runat="server" CssClass="select form-control" ClientIDMode="Static" Style="width: 190px;">
                            <asp:ListItem Value="0">Trạng thái đơn hàng</asp:ListItem>
                            <asp:ListItem Value="2">Đã order</asp:ListItem>
                            <asp:ListItem Value="3">Từ chối</asp:ListItem>
                            <asp:ListItem Value="4">Chờ nhận hàng</asp:ListItem>
                            <asp:ListItem Value="5">Đã nhận hàng</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form-group col-xl-auto col-lg-auto col-md-12 col-sm-auto form-group">
                        <div id="ViewDataFilter" class="form-group float-left mr-1" onclick="viewData()">
                            <div class="btn-viewdata">Xem dữ liệu</div>
                        </div>
                        <div id="exportCosmeticOrder" class="form-group float-left mr-1" onclick="exportCosmeticOrder($(this))">
                            <div class="btn-viewdata">Export MP</div>
                        </div>
                        <div id="exportServiceOrder" class="form-group float-left mr-1" onclick="exportServiceOrder($(this))">
                            <div class="btn-viewdata">Export DV</div>
                        </div>
                        <div id="exportServiceOrderToStaff" class="form-group float-left mr-1" onclick="exportServiceOrderToStaff($(this))">
                            <div class="btn-viewdata">Export NV</div>
                        </div>
                    </div>
                    <div class="col-md-12 col-xl-12 form-group">
                        <div class="sub-menu-boder-low"></div>
                    </div>
                </div>
            </div>
            <div class="container-fluid table-listing pt-2">
                <table id="tableListing" class="table table-bordered table-hover" style="float: left;">
                    <thead>
                        <tr>
                            <th rowspan="2" class="text-center align-middle">STT</th>
                            <th rowspan="2" class="text-center align-middle" style="min-width: 95px;">Tên kho</th>
                            <th rowspan="2" class="text-center align-middle" style="min-width: 150px;">Tên vật tư</th>
                            <th rowspan="2" class="text-center align-middle">Mã vật tư</th>
                            <% if (Perm_ViewAllData)
                                { %>
                            <th rowspan="2" class="text-center align-middle">Giá vốn</th>
                            <%} %>
                            <th rowspan="2" class="text-center align-middle">Tồn đầu (G)</th>
                            <th colspan="6" class="text-center align-middle">Xuất</th>
                            <th colspan="6" class="text-center align-middle">Nhập</th>
                            <th rowspan="2" class="text-center align-middle">Vận chuyển (T)</th>
                            <th rowspan="2" class="text-center align-middle">Tồn cuối<br />
                                (G-H+N+T)</th>
                        </tr>
                        <tr>
                            <th class="text-center align-middle">Xuất (H)</th>
                            <th class="text-center align-middle">
                                <div class="d-none">Xuất </div>
                                Bán</th>
                            <th class="text-center align-middle">
                                <div class="d-none">Xuất </div>
                                Dùng DV</th>
                            <th class="text-center align-middle">
                                <div class="d-none">Xuất </div>
                                NV dùng</th>
                            <th class="text-center align-middle">
                                <div class="d-none">Xuất </div>
                                Kho tổng</th>
                            <th class="text-center align-middle">
                                <div class="d-none">Xuất </div>
                                Kho Salon</th>
                            <th class="text-center align-middle">Nhập (N)</th>
                            <th class="text-center align-middle">
                                <div class="d-none">Nhập </div>
                                Kho NCC</th>
                            <th class="text-center align-middle">
                                <div class="d-none">Nhập </div>
                                Kho tổng</th>
                            <th class="text-center align-middle">
                                <div class="d-none">Nhập </div>
                                Khách trả</th>
                            <th class="text-center align-middle">
                                <div class="d-none">Nhập </div>
                                Chuyển salon</th>
                            <th class="text-center align-middle">
                                <div class="d-none">Nhập </div>
                                NV trả</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </form>
        <style>
            .menu-time {
                margin-right: 7px;
                font-size: 13px;
            }

                .menu-time .label {
                    padding: 0px 15px;
                    background: #000000;
                    color: #ffffff;
                    cursor: pointer;
                    border-radius: 15px;
                    line-height: 26px;
                    width: 94px;
                }

            .form-control {
                height: 31px !important;
            }

            .select2.select2-container.select2-container--default {
                width: 180px !important;
            }

            .lable-text {
                width: 46px;
            }

            #tableListing_info {
                float: left;
            }

            #tableListing_paginate {
                clear: both;
            }

            .dt-buttons.btn-group {
                float: left;
            }

            #tableListing_length label {
                float: right;
            }

            #tableListing_wrapper button.buttons-excel {
                background-color: black;
                width: 100px;
            }

                #tableListing_wrapper button.buttons-excel span {
                    color: white
                }

            #tableListing tbody tr td {
                text-align: center !important;
            }

                #tableListing tbody tr td:nth-child(2),
                #tableListing tbody tr td:nth-child(3),
                #tableListing tbody tr td:nth-child(4) {
                    text-align: left !important;
                }
        </style>
        <script>
            var domain = '<% = Libraries.AppConstants.URL_API_INVENTORY %>';
            // Doi tuong dataTable
            var table = null;
            //hoac domain = Assets/js/Config/Domain.js;
            // api cau hinh trong  Assets/js/Config/Api.js
            var elms = {
                today: 'span.today',
                fromDate: 'input[name=fromDate]',
                toDate: 'input[name=toDate]',
                ddlInventoryType: '#ddlInventoryType',
                ddlInventory: '#ddlInventory',
                ddlProduct: '#ddlProduct'
            };
            jQuery(document).ready(function () {
                $('.select').select2();
                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth() + 1; //January is 0!

                var yyyy = today.getFullYear();
                if (dd < 10) {
                    dd = '0' + dd;
                }
                if (mm < 10) {
                    mm = '0' + mm;
                }
                var today = dd + '/' + mm + '/' + yyyy;
                $(elms.today).text(today);
                $(elms.fromDate).val(today);
                $(elms.toDate).val(today);
                $('.reset-table').bind('change', function () {
                    if (table) {
                        $('#tableListing').DataTable().clear();
                        $('#tableListing').DataTable().destroy();
                    }
                });
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) { }
                });
            });
            // get data today
            function ClickToday() {
                var today = $(elms.today).text();
                if (!CheckExits(today)) {
                    ShowMessage("Thông báo", "Không có ngày hiện tại", 3, 7000);
                    return;
                }
                viewData(today);
            }
            // get data
            function viewData(today) {
                var timeFrom = $(elms.fromDate).val();
                var timeTo = $(elms.toDate).val();
                if (today) {
                    timeFrom = today;
                    timeTo = today;
                }
                if (!CheckExits(timeFrom) || !CheckExits(timeTo)) {
                    ShowMessage("Thông báo", " Chưa chọn ngày", 3, 7000);
                    return;
                }
                var inventoryTypeId = $(elms.ddlInventoryType).val();
                if (!CheckExits(inventoryTypeId) || inventoryTypeId === '0') {
                    ShowMessage("Thông báo", "Chưa chọn loại kho", 3, 7000);
                    return;
                }
                var inventoryId = $(elms.ddlInventory);
                var arrayInventoryId = [];
                if (inventoryId.val() !== '0') {
                    arrayInventoryId.push(inventoryId.val());
                }
                else {
                    $(inventoryId).find('option').each(function (index, value) {
                        let inventoryId = $(value).val();
                        if (inventoryId !== '0') {
                            arrayInventoryId.push($(value).val());
                        }
                    });
                }
                var strInventoryId = arrayInventoryId.toString();
                var productId = $(elms.ddlProduct).val();
                startLoading();
                if (table) {
                    $('#tableListing').DataTable().clear();
                    $('#tableListing').DataTable().destroy();
                }
                table = $('#tableListing').DataTable(
                    {

                        dom: 'Blfrtip',
                        buttons: [
                            'excel'
                        ],
                        pagingType: "full_numbers",
                        language: {
                            lengthMenu: "Dòng / trang _MENU_",
                            paginate: {
                                previous: "<",
                                first: "Đầu",
                                last: "Cuối",
                                next: ">"
                            },
                            info: "Dòng _START_ tới _END_ của _TOTAL_ dòng",
                            search: "Nhập tìm kiếm:"
                        },
                        info: false,
                        lengthMenu: [[100, 500, 3000, 5000], [100, 500, 3000, 5000]],
                        ordering: false,
                        serverSide: true,
                        processing: true,
                        searching: false,
                        ajax: {
                            type: "POST",
                            async: true,
                            url: domain + "/api/iv-inventor-history/get-inventory-v2",
                            contentType: "application/json",
                            data: function (d) {
                                d.timeFrom = timeFrom;
                                d.timeTo = timeTo;
                                d.inventoryId = strInventoryId;
                                d.productId = productId;
                                return JSON.stringify(d);
                            },
                            dataSrc: function (json) {
                                if (typeof json.data !== "undefined" || json.data !== null || json.data !== null) {
                                    $(json.data).each(function (index, value) {
                                        value.tonCuoi = value.tonDau - value.quantityExport + value.quantityImport + value.beingTransported;
                                        value.stt = index + 1;
                                        value.cost = addCommas(value.cost);
                                        return value;
                                    });
                                }
                                else {
                                    return false;
                                }
                                return json.data;
                            }
                        },
                        "columns": [
                            { "data": "stt", "name": "stt", "autoWidth": true },
                            { "data": "salonWarehouse", "name": "Tên kho", "autoWidth": true },
                            { "data": "productName", "name": "productName", "autoWidth": true },
                            { "data": "code", "name": "code", "autoWidth": true },
                             <% if (Perm_ViewAllData)
            { %>
                            { "data": "cost", "name": "cost", "autoWidth": true },
                            <%} %>
                            { "data": "tonDau", "name": "tonDau", "autoWidth": true },
                            { "data": "quantityExport", "name": "quantityExport", "autoWidth": true },
                            { "data": "sellOrUse", "name": "sellOrUse", "autoWidth": true },
                            { "data": "usedInService", "name": "usedInService", "autoWidth": true },
                            { "data": "exportToStaff", "name": "exportToStaff", "autoWidth": true },
                            { "data": "exportToMainWH", "name": "exportToMainWH", "autoWidth": true },
                            { "data": "exportToSalon", "name": "exportToSalon", "autoWidth": true },
                            { "data": "quantityImport", "name": "quantityImport", "autoWidth": true },
                            { "data": "importFromPartner", "name": "importFromPartner", "autoWidth": true },
                            { "data": "importFromMainWH", "name": "importFromMainWH", "autoWidth": true },
                            { "data": "importFromReturn", "name": "importFromReturn", "autoWidth": true },
                            { "data": "importFromSalon", "name": "importFromSalon", "autoWidth": true },
                            { "data": "importFromStaff", "name": "importFromStaff", "autoWidth": true },
                            { "data": "beingTransported", "name": "beingTransported", "autoWidth": true },
                            { "data": "tonCuoi", "name": "tonCuoi", "autoWidth": true }
                        ]
                    });
                // finishLoading
                finishLoading();
            }
            // check exist
            function CheckExits(value) {
                if (typeof value === "undefined" || value === null || value === "") {
                    return false;
                }
                else {
                    return true;
                }
            }
            // get list inventory
            function GetListInventory(This) {
                let $value = This.val();
                if (!CheckParameter($value)) {
                    ShowMessage('', 'Vui lòng chọn loại kho', 3, 7000);
                }
                else {
                    $.ajax({
                        type: "POST",
                        url: "/GUI/BackEnd/IvInventory/ReportsInventory.aspx/GetListInventory",
                        data: '{inventoryType: ' + parseInt($value) + ' }',
                        crossDomain: true,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            $("#ddlInventory").html('').val('0');
                            $("#select2-ddlInventory-container").val('0').text('Chọn tất cả');
                            $("#ddlInventory").append($("<option />").val(0).text('Chọn tất cả'));
                            $.each(data.d, function () {
                                $("#ddlInventory").append($("<option />").val(this.Id).text(this.Name));
                            });
                        },
                        failure: function () {
                            ShowMessage('', 'Có lỗi xảy ra, vui lòng liên hệ nhà phát triển!', 4);
                        }
                    });
                }
            }

            // header, body, filename
            var excelHeader = ["STT", "Ngày phê duyệt", "Kho xuất", "Kho nhận", "ID SP", "Tên SP", "Giá", "SL order", "SL Xuất", "SL Nhận"];
            var excelHeaderExportServiceOrderToStaff = ["STT", "Ngày phê duyệt", "Kho xuất", "ID SP", "Tên SP", "Giá", "SL Xuất"];
            var excelBody = [];
            var excelFileName = "";

            //exportCosmeticOrder
            function exportCosmeticOrder() {
                var kt = true;
                var error = '';
                var timeFrom = $(elms.fromDate).val();
                var timeTo = $(elms.toDate).val();
                var inventoryId = $("#ddlInventory").val();
                var statusId = $("#ddlStatus").val();
                if (!CheckParameter(timeFrom)) {
                    kt = false;
                    error += " Bạn phải chọn ngày bắt đầu! ";
                    $('#TxtDateTimeFrom').css("border-color", "red");
                }
                else if (!CheckParameter(!timeTo)) {
                    kt = false;
                    error += " Bạn phải chọn ngày kết thúc! ";
                    $('#TxtDateTimeTo').css("border-color", "red");
                }
                if (!kt) {
                    ShowMessage('', error, 3);
                }
                else {
                    excelBody = [];
                    excelFileName = "Báo cáo order hàng mỹ phẩm bán từ " + timeFrom + " đến " + timeTo + "";
                    startLoading();
                    $.ajax({
                        type: "GET",
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        url: domain + `/api/order/get-list-report-cosmetic-order?timeFrom=${timeFrom}&timeTo=${timeTo}&inventoryId=${inventoryId}&status=${statusId}`,
                        success: function (response) {
                            //console.log(response);
                            if (response.length > 0) {
                                response.forEach(function (v, i) {
                                    let excelRow = [(i + 1) + "",
                                    v.reviewedDate,
                                    v.exportInventory,
                                    v.receivedInventory,
                                    v.productId,
                                    v.productName,
                                    v.cost,
                                    v.quantityOrder,
                                    v.exportQuantity,
                                    v.receivedQuantity
                                    ];
                                    excelBody.push(excelRow);
                                });
                                console.log(excelBody);
                                //
                                exportExcel(excelFileName, excelFileName.substr(-30, 30), excelHeader, excelBody);
                            }
                            finishLoading();
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            var responseText = jqXHR.responseJSON.message;
                            ShowMessage('', responseText, 4);
                            finishLoading();
                        }
                    });
                }
            }

            // export Service order
            function exportServiceOrder() {
                var kt = true;
                var error = '';
                var timeFrom = $(elms.fromDate).val();
                var timeTo = $(elms.toDate).val();
                var inventoryId = $("#ddlInventory").val();
                var statusId = $("#ddlStatus").val();
                if (!CheckParameter(timeFrom)) {
                    kt = false;
                    error += " Bạn phải chọn ngày bắt đầu! ";
                    $('#TxtDateTimeFrom').css("border-color", "red");
                }
                else if (!CheckParameter(timeTo)) {
                    kt = false;
                    error += " Bạn phải chọn ngày kết thúc! ";
                    $('#TxtDateTimeTo').css("border-color", "red");
                }
                if (!kt) {
                    ShowMessage('', error, 3);
                }
                else {
                    excelBody = [];
                    excelFileName = "Báo cáo order hàng dịch vụ từ " + timeFrom + " đến " + timeTo + "";
                    startLoading();
                    $.ajax({
                        type: "GET",
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        url: domain + `/api/order/get-list-report-service-order?timeFrom=${timeFrom}&timeTo=${timeTo}&inventoryId=${inventoryId}&status=${statusId}`,
                        success: function (response) {
                            //console.log(response);
                            if (response.length > 0) {
                                response.forEach(function (v, i) {
                                    let excelRow = [(i + 1) + "",
                                    v.reviewedDate,
                                    v.exportInventory,
                                    v.receivedInventory,
                                    v.productId,
                                    v.productName,
                                    v.cost,
                                    v.quantityOrder,
                                    v.exportQuantity,
                                    v.receivedQuantity
                                    ];
                                    excelBody.push(excelRow);
                                });
                                console.log(excelBody);
                                //
                                exportExcel(excelFileName, excelFileName.substr(-30, 30), excelHeader, excelBody);
                            }
                            finishLoading();
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            var responseText = jqXHR.responseJSON.message;
                            ShowMessage('', responseText, 4);
                            finishLoading();
                        }
                    });
                }
            }

            // export Service order to Staff
            function exportServiceOrderToStaff() {
                var kt = true;
                var error = '';
                var timeFrom = $(elms.fromDate).val();
                var timeTo = $(elms.toDate).val();
                var inventoryId = $("#ddlInventory").val();
                var inventoryType = $("#ddlInventoryType").val();
                if (!CheckParameter(timeFrom)) {
                    kt = false;
                    error += " Bạn phải chọn ngày bắt đầu! ";
                    $('#TxtDateTimeFrom').css("border-color", "red");
                }
                else if (!CheckParameter(timeTo)) {
                    kt = false;
                    error += " Bạn phải chọn ngày kết thúc! ";
                    $('#TxtDateTimeTo').css("border-color", "red");
                }
                else if (!CheckParameter(inventoryType) || inventoryType !== "2") {
                    kt = false;
                    error += " Bạn phải chọn loại kho thuộc kho Salon! ";
                    $('#ddlInventoryType').css("border-color", "red");
                }
                if (!kt) {
                    ShowMessage('', error, 3);
                }
                else {
                    excelBody = [];
                    excelFileName = "Báo cáo hàng dịch vụ salon xuat nhan vien từ " + timeFrom + " đến " + timeTo + "";
                    startLoading();
                    $.ajax({
                        type: "GET",
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        url: domain + `/api/order/get-list-report-service-order-to-staff?timeFrom=${timeFrom}&timeTo=${timeTo}&inventoryId=${inventoryId}`,
                        success: function (response) {
                            //console.log(response);
                            if (response.length > 0) {
                                response.forEach(function (v, i) {
                                    let excelRow = [(i + 1) + "",
                                    v.reviewedDate,
                                    v.exportInventory,
                                    v.productId,
                                    v.productName,
                                    v.cost,
                                    v.exportQuantity
                                    ];
                                    excelBody.push(excelRow);
                                });
                                console.log(excelBody);
                                //
                                exportExcel(excelFileName, excelFileName.substr(-30, 30), excelHeaderExportServiceOrderToStaff, excelBody);
                            }
                            finishLoading();
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            var responseText = jqXHR.responseJSON.message;
                            ShowMessage('', responseText, 4);
                            finishLoading();
                        }
                    });
                }
            }

            // check parameter
            function CheckParameter(param) {
                if (param === undefined || param === null || param === '' || param === '0') {
                    return false;
                }
                return true;
            }
        </script>
    </asp:Panel>
</asp:Content>
