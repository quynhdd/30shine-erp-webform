﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.IvInventory
{
    public partial class IvCheckoutListing : System.Web.UI.Page
    {
        protected Paging PAGING = new Paging();
        CultureInfo culture = new CultureInfo("vi-VN");
        private Solution_30shineEntities db = new Solution_30shineEntities();
        private bool Perm_Access = false;
        protected bool Perm_AddNew = false;
        protected bool Perm_Delete = false;
        protected List<DataReport> datas = new List<DataReport>();
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                TxtDateTimeFrom.Text = DateTime.Now.ToString("dd-MM-yyyy");
                BindTotalInventory();
                Inventory.Enabled = false;
            }
        }
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_AddNew = permissionModel.CheckPermisionByAction("Perm_AddNew", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }

        }
        private void BindTotalInventory()
        {

            using (var db = new Solution_30shineEntities())
            {
                var inv = db.IvInventories.Where(w => w.IsDelete == false && w.Type == 1).OrderBy(o => o.Id).ToList();
                var Key = 0;

                InvType.DataTextField = "Name";
                InvType.DataValueField = "Id";

                ListItem item = new ListItem("Chọn kho tổng", "0");
                InvType.Items.Insert(0, item);

                foreach (var v in inv)
                {
                    Key++;
                    item = new ListItem(v.Name, v.Id.ToString());
                    InvType.Items.Insert(Key, item);
                }
            }
        }

        protected void GetInventory(object sender, EventArgs e)
        {
            Inventory.Enabled = true;
            var invId = Convert.ToInt32(InvType.SelectedValue);
            Inventory.Items.Clear();
            using (var db = new Solution_30shineEntities())
            {
                var inv = db.IvInventories.Where(w => w.IsDelete == false && w.Type == 2 && w.ParentId == invId).OrderBy(o => o.Id).ToList();
                var Key = 0;

                Inventory.DataTextField = "Name";
                Inventory.DataValueField = "Id";

                ListItem item = new ListItem("Chọn kho", "0");
                Inventory.Items.Insert(0, item);

                foreach (var v in inv)
                {
                    Key++;
                    item = new ListItem(v.Name, v.Id.ToString());
                    Inventory.Items.Insert(Key, item);
                }
            }
        }
        protected void _BtnClick(object sender, EventArgs e)
        {
            BindData();
            RemoveLoading();
        }
        protected void BindData()
        {
            var timeFrom = TxtDateTimeFrom.Text;
            var timeTo = TxtDateTimeTo.Text;
            var invId = Convert.ToInt32(Inventory.SelectedValue);
            var result = new HttpClient().GetAsync(Libraries.AppConstants.URL_API_INVENTORY + "/api/iv-checkout/get-data-report?timeFrom="+timeFrom+"&timeTo="+timeTo+"&invId="+invId).Result;

            var invType = Convert.ToInt32(InvType.SelectedValue);
            if (result.IsSuccessStatusCode)
            {
                var data = result.Content.ReadAsStringAsync().Result;
                if (data != null)
                {
                    var _data = new JavaScriptSerializer().Deserialize<List<DataReport>>(data);
                    datas = _data;
                    //Bind_Paging(_data.Count);

                    //rptDanhsach.DataSource = _data.ToList();
                    ////rptDanhsach.DataSource = _data.Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                    //rptDanhsach.DataBind();
                }
            }
        }

        //protected void Bind_Paging(int TotalRow)
        //{
        //    // init Paging value            
        //    PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
        //    PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
        //    PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
        //    PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(TotalRow) : PAGING.TotalPage;
        //    PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
        //    PAGING._Paging = PAGING.Make_Paging();

        //    RptPaging.DataSource = PAGING._Paging.ListPage;
        //    RptPaging.DataBind();
        //}

        protected int Get_TotalPage(int TotalRow)
        {
            TotalRow -= PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }
        public partial class DataReport
        {
            public string WareName { get; set; }
            public string Code { get; set; }
            public string BarCode { get; set; }
            public string ProductName { get; set; }
            public int Quantity { get; set; }
            public DateTime CreatedDate { get; set; }
        }
    }
}