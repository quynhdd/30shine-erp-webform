﻿using _30shine.Helpers;
using _30shine.Helpers.Http;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using Libraries;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.IvInventory
{
    public partial class IvInventoryHistoryListing : System.Web.UI.Page
    {
        protected string Day1 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-2));
        protected string Day2 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-1));
        protected string Day3 = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
        protected Paging PAGING = new Paging();
        protected bool Perm_ViewAllData = false;
        protected bool Perm_ViewAllDate = false;
        private bool Perm_Access = false;
        public bool Perm_ShowSalon { get; private set; }
        public int index = 1;
        public List<Origin> listOrigins = new List<Origin>();

        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_ViewAllDate = permissionModel.CheckPermisionByAction("Perm_ViewAllDate", pageId, staffId);
                Perm_ShowSalon = permissionModel.CheckPermisionByAction("Perm_ShowSalon", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        /// <summary>
        /// Excute by permission
        /// </summary>
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                TxtDateTimeFrom.Text = String.Format("{0:dd/MM/yyyy}", DateTime.Now);
                // Library.Function.bindSalon_NotHoiQuan(new List<DropDownList>() { ddlSalon }, Perm_ViewAllData);
                BindDdlInventorySalon();
            }
        }

        /// <summary>
        /// Get data
        /// </summary>
        private void GetData()
        {
            try
            {
                int integer;
                string timeFrom, timeTo;
                timeFrom = TxtDateTimeFrom.Text;
                if (!String.IsNullOrEmpty(TxtDateTimeTo.Text))
                {
                    timeTo = TxtDateTimeTo.Text;
                }
                else
                {
                    timeTo = timeFrom;
                }
                if (!string.IsNullOrEmpty(ddlSalon.SelectedValue))
                {
                    int inventoryId = int.TryParse(ddlSalon.SelectedValue, out integer) ? integer : 0;
                    var getData = new Request().RunGetAsyncV1(AppConstants.URL_API_INVENTORY + $"/api/iv-inventor-history?timeFrom={timeFrom}&timeTo={timeTo}&inventoryId={inventoryId}").Result;
                    if (getData.IsSuccessStatusCode)
                    {
                        //response data
                        var origin = getData.Content.ReadAsStringAsync().Result;
                        JavaScriptSerializer serializer = new JavaScriptSerializer();
                        var responseData = serializer.Deserialize<List<Origin>>(origin);
                        if (responseData != null)
                        {
                            Bind_Paging(listOrigins.Count);
                            listOrigins = responseData.Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                new Helpers.PushNotiErrorToSlack().PushDefault(ex.Message + ", MAIN: " + ex.InnerException, this.ToString() + ".IvInventoryHistoryListing", "QuynhDD");
            }
        }

        /// <summary>
        /// _BtnClick
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void _BtnClick(object sender, EventArgs e)
        {
            GetData();
            RemoveLoading();
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "finishLoading();", true);
        }

        /// <summary>
        /// Bind Paging
        /// </summary>
        /// <param name="TotalRow"></param>
        protected void Bind_Paging(int TotalRow)
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(TotalRow) : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();
            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        /// <summary>
        /// Bind data InventorySalon
        /// </summary>
        private void BindDdlInventorySalon()
        {
            try
            {
                int[] inventoryType = { 1, 2 };
                var UserId = Convert.ToInt32(HttpContext.Current.Session["User_Id"]);
                using (var db = new Solution_30shineEntities())
                {
                    var salonCurrent = db.Staffs.Where(r => r.Id == UserId).Select(r => new { Id = r.SalonId ?? 0 });

                    var salonArea = db.PermissionSalonAreas.Where(r => ((r.StaffId == UserId && UserId > 0) || UserId == 0) &&
                                                                       r.IsDelete == false &&
                                                                       r.IsActive == true)
                                                                       .Select(r => new { Id = r.SalonId ?? 0 });
                    var listInventorySalon = new List<MODEL.ENTITY.EDMX.IvInventory>();
                    if (Perm_ShowSalon)
                    {
                        listInventorySalon = (from a in db.IvInventories
                                              where a.IsDelete == false && inventoryType.Contains(a.Type)
                                              select a).ToList();
                        var item = new MODEL.ENTITY.EDMX.IvInventory();
                        item.Name = "Chọn kho Salon";
                        item.Id = 0;
                        listInventorySalon.Insert(0, item);
                    }
                    else
                    {
                        var listSalonId = salonCurrent.Union(salonArea).Where(r => r.Id != 0).Select(r => r);

                        listInventorySalon = (from a in db.IvInventories
                                              join b in listSalonId on a.SalonId equals b.Id
                                              where a.IsDelete == false && inventoryType.Contains(a.Type)
                                              select a).ToList();
                    }
                    var ddls = new List<DropDownList>() { ddlSalon };
                    for (var i = 0; i < ddls.Count; i++)
                    {
                        ddls[i].DataTextField = "Name";
                        ddls[i].DataValueField = "Id";
                        ddls[i].DataSource = listInventorySalon;
                        ddls[i].DataBind();
                    }
                    //ddlSalon.Items.Insert(0, new ListItem("Chọn kho Salon", "0"));
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Get total Page
        /// </summary>
        /// <param name="TotalRow"></param>
        /// <returns></returns>
        protected int Get_TotalPage(int TotalRow)
        {
            TotalRow -= PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        }

        /// <summary>
        /// CheckViewPriceProduct
        /// </summary>
        private void CheckViewPriceProduct()
        {
            var arrayPermision = Session["User_Permission"].ToString().Split(',');
            if (arrayPermision.Length > 0)
            {
                string checkAdmin = Array.Find(arrayPermision, s => s.Equals("ADMIN"));
                string checkRoot = Array.Find(arrayPermision, s => s.Equals("root"));
                string checkSUPPLY_CHAIN_ADMIN = Array.Find(arrayPermision, s => s.Equals("SUPPLY_CHAIN_ADMIN"));
                string checkACCOUNTANT_ADMIN = Array.Find(arrayPermision, s => s.Equals("ACCOUNTANT_ADMIN"));
                if (!String.IsNullOrEmpty(checkRoot) || !String.IsNullOrEmpty(checkAdmin) || !String.IsNullOrEmpty(checkSUPPLY_CHAIN_ADMIN) || !String.IsNullOrEmpty(checkACCOUNTANT_ADMIN))
                {
                    Perm_ViewAllData = true;
                }
            }
        }

        /// <summary>
        /// Origin
        /// </summary>
        public class Origin
        {
            public int inventoryId { get; set; }
            public int salonId { get; set; }
            public string salonWarehouse { get; set; }
            public List<ListProduct> product { get; set; }
        }

        /// <summary>
        /// Product
        /// </summary>
        public class ListProduct
        {
            public int productId { get; set; }
            public double cost { get; set; }
            public string productName { get; set; }
            public int tonDau { get; set; }
            public int quantityImport { get; set; }
            public int quantityExport { get; set; }
            public int quantitySell { get; set; }
            public int tonCuoi { get; set; }
        }
    }
}