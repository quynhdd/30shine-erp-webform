﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="IvCheckoutListing.aspx.cs" MasterPageFile="~/TemplateMaster/SiteMaster.Master" Inherits="_30shine.GUI.BackEnd.IvInventory.IvCheckoutListing" %>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">

    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>

    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <%--<uc1:ReportMenu runat="server" id="ReportMenu" />--%>
        <style>
            .table-wp table thead th {
                font-weight: normal;
                font-family: Roboto Condensed Bold;
            }

            .btn-add {
                background: none;
                border: none;
            }

            #table-product tbody {
                display: block;
                max-height: 500px;
                overflow-y: scroll;
            }

                #table-product thead,
                #table-product tbody tr {
                    display: table;
                    width: 100%;
                    table-layout: fixed;
                }

                    #table-product thead tr td, #table-product tbody tr td {
                        border: 1px solid #ccc;
                        padding-left: 10px;
                    }

            .btn-action {
                float: left !important;
                /*margin: 0 auto;*/
                padding-left: 10px;
            }

            .select2-container {
                margin-top: 6px;
            }
            .dataTables_paginate.paging_simple_numbers{
                width:100% !important;
                text-align:center !important;
            }
        </style>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý kho &nbsp;&#187; </li>
                        <li class="be-report-li"><a href="javascript:void(0);"><i class="fa fa-th-large"></i>Danh sách kiểm kho</a></li>
                        <li class="li-add"><a href="/admin/hang-hoa/kiem-kho.html">Thêm mới</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing be-report" onload="abc()">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <!-- Filter -->
                <div class="row">
                    <div class="filter-item">
                        <strong class="st-head"><i class="fa fa-clock-o"></i>Thời gian</strong>
                        <div class="time-wp">
                            <asp:TextBox CssClass="txtDateTime st-head" ID="TxtDateTimeFrom" placeholder="Từ ngày" Style="margin-left: 10px!important" AutoCompleteType="Disabled"
                                ClientIDMode="Static" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="ValidateTime" ControlToValidate="TxtDateTimeFrom" runat="server" CssClass="fb-cover-error" Text="Vui lòng chọn thời gian!"></asp:RequiredFieldValidator>
                        </div>
                        <strong class="st-head" style="margin-left: 10px;">
                            <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                        </strong>
                        <asp:TextBox CssClass="txtDateTime st-head" ID="TxtDateTimeTo" placeholder="Đến ngày" AutoCompleteType="Disabled" Style="margin-left: 10px!important"
                            ClientIDMode="Static" runat="server"></asp:TextBox>
                        <br />

                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="InvType" CssClass="form-control " runat="server" OnSelectedIndexChanged="GetInventory" AutoPostBack="true" ClientIDMode="Static" Style="width: 190px;"></asp:DropDownList>
                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="Inventory" CssClass="form-control select" runat="server" ClientIDMode="Static" Style="width: 190px;"></asp:DropDownList>
                    </div>

                    <asp:Panel ID="ViewData" CssClass="st-head btn-viewdata" Text="Xem dữ liệu"
                        ClientIDMode="Static" onclick="excPaging1(1);" runat="server">
                        Xem dữ liệu
                    </asp:Panel>

                </div>
                <div class="row">
                    <strong class="st-head"><i class="fa fa-file-text"></i>DANH SÁCH KIỂM HÀNG</strong>
                </div>
                <!-- Row Table Filter -->
                <asp:ScriptManager ID="SM01" runat="server"></asp:ScriptManager>
                <%--<div class="table-func-panel" style="margin-top: -35px;">

                    <div class="table-func-elm">
                        <span>Số hàng / Page : </span>
                        <div class="table-func-input-wp">
                            <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                            <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                            <ul class="ul-opt-segment">
                                <li data-value="10">10</li>
                                <li data-value="20">20</li>
                                <li data-value="30">30</li>
                                <li data-value="40">40</li>
                                <li data-value="50">50</li>
                                <li data-value="1000000">Tất cả</li>
                            </ul>
                            <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                        </div>
                    </div>
                </div>--%>
                <div class="row table-wp">
                    <table class="table-add table-listing">
                        <thead>
                            <tr>
                                <%--<th>STT</th>--%>
                                <th>Tên kho</th>
                                <th>Mã SP</th>
                                <th>BarCode</th>
                                <th>Tên SP</th>
                                <th>Số lượng</th>
                                <th>Ngày kiểm</th>
                            </tr>
                        </thead>
                        <tbody>
                            <% foreach (var item in datas)
                                {
                                    Response.Write("<tr>");
                                    Response.Write("<td>" + item.WareName + "</td>");
                                    Response.Write("<td>" + item.Code + "</td>");
                                    Response.Write("<td>" + item.BarCode + "</td>");
                                    Response.Write("<td>" + item.ProductName + "</td>");
                                    Response.Write("<td>" + item.Quantity + "</td>");
                                    Response.Write("<td>" + item.CreatedDate + "</td>");
                                    Response.Write("</tr>");
                                } %>
                        </tbody>
                    </table>
                </div>
                <!-- End Row Table Filter -->
                <%--<asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                    <ContentTemplate>--%>

                        <%--<div class="row table-wp">
                            <table class="table-add table-listing">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Tên kho</th>
                                        <th>Mã SP</th>
                                        <th>BarCode</th>
                                        <th>Tên SP</th>
                                        <th>Số lượng</th>
                                        <th>Ngày kiểm</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="rptDanhsach" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# Container.ItemIndex + 1 + PAGING._Offset %></td>
                                                <td><%# Eval("WareName") %></td>
                                                <td><%# Eval("Code") %></td>
                                                <td><%# Eval("BarCode") %></td>
                                                <td><%# Eval("ProductName") %></td>
                                                <td><%# Eval("Quantity") %></td>
                                                <td><%# Eval("CreatedDate") %></td>
                                               
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>--%>
                        <!-- Paging -->
                        <%--<div class="site-paging-wp">
                            <% if (PAGING.TotalPage > 1)
                                { %>
                            <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                                <% if (PAGING._Paging.Prev != 0)
                                    { %>
                                <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                                <% } %>
                                <asp:Repeater ID="RptPaging" runat="server">
                                    <ItemTemplate>
                                        <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                            <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>>
                                            <%# Eval("PageNum") %>
                                        </a>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                                    { %>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                                <% } %>
                            </asp:Panel>
                            <% } %>
                        </div>--%>
                        <!-- End Paging -->
                    </ContentTemplate>
                     <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
               
                <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server"  OnClick="_BtnClick" Text="Click" Style="display: none;" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HiddenField1" />

                <asp:HiddenField runat="server" ClientIDMode="Static" ID="IsPaging" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
            </div>
            <%-- END Listing --%>
        </div>
        <script type="text/javascript">
            $(document).ready(function () {
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) { }
                });
                $(".select").select2();
                $(".table-listing").DataTable({
                    bFilter: false,
                    bInfo: false,
                    dom: 'Bfrtip',
                    buttons: [
                        'excel', 'pdf'
                    ]
                });
            })
            function excPaging1(page) {
                 if ($("#TxtDateTimeFrom").val() == '' || $("#TxtDateTimeTo").val() == '' || $("#Inventory").val() == 0 || $("#Inventory").val() == null) {
                    ShowMessage("Thông báo", "Vui lòng chọn đầy đủ các filter", 4);
                    return false;
                }
                $("#HDF_Page").val(page);
                $("#BtnFakeUP").click();
                addLoading();
            }
            function ValidControl() {
                // var valid = false;
                console.log($("#Inventory").val());
                if ($("#TxtDateTimeFrom").val() == '' || $("#TxtDateTimeTo").val() == '' || $("#Inventory").val() == 0 || $("#Inventory").val() == null) {
                    ShowMessage("Thông báo", "Vui lòng chọn đầy đủ các filter", 4);
                    return false;
                }
            }
        </script>
    </asp:Panel>
</asp:Content>
