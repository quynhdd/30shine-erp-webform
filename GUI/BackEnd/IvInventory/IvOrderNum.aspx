﻿<%@ Page Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="IvOrderNum.aspx.cs" Inherits="_30shine.GUI.BackEnd.IvInventory.IvOrderNum" %>


<asp:Content ContentPlaceHolderID="head" runat="server">
    <title>Cấu hình số thứ tự các sản phẩm trong kho tổng, kho NCC</title>
    <link href="/Assets/css/bootstrap/bootstrap-toggle.min.css" rel="stylesheet" />
    <script src="/Assets/js/bootstrap/bootstrap-toggle.min.js"></script>
    <link href="/Assets/css/bootstrap/bootstrap-toggle.min.css" rel="stylesheet" />
    <script src="/Assets/js/bootstrap/bootstrap-toggle.min.js"></script>

    <script src="/Assets/libs/magicsuggest-master/magicsuggest.js"></script>
    <link href="/Assets/libs/magicsuggest-master/magicsuggest.css" rel="stylesheet" />
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <!-- Include multi.js -->
    <link rel="stylesheet" type="text/css" href="/Assets/js/Two-Panel-Multi-Selection/multi.css" />
    <script src="/Assets/js/Two-Panel-Multi-Selection/multi.js"></script>
    <script src="/Assets/js/Common/AjaxCallCommon.js"></script>
    <link rel="stylesheet" href="/Assets/izitoast/css/iziToast.min.css" />
    <script src="/Assets/izitoast/js/iziToast.min.js" type="text/javascript"></script>
    <script src="/Assets/js/common.js"></script>
    <link rel="stylesheet" href="/Assets/css/style.css" />

    <style>
        .customer-add .table-add .title-head strong {
            font-family: sans-serif;
            font-weight: 700;
            font-size: 17px;
        }

        .sub-menu ul.ul-sub-menu li a {
            padding-left: 30px;
        }

        .sub-menu ul.ul-sub-menu li:hover a {
            color: green !important;
        }

            .sub-menu ul.ul-sub-menu li:hover a i {
                color: green !important;
            }

        .fe-service .tr-product .table-listing-product tbody input.product-quantify {
            border-bottom: 1px solid #ddd !important;
            width: 100px !important;
        }

        .fe-service .tr-product .table-listing-product tbody input.product-volume {
            border-bottom: 1px solid #ddd !important;
            width: 100px !important;
        }

        .fe-service .tr-product .listing-product {
            display: block !important;
        }

        .suggestion-wrap {
            width: 420px;
            float: left;
        }

        .ms-sel-ctn input[type='text'] {
            height: auto !important;
            line-height: 24px !important;
            border: none !important;
        }

        .ms-ctn .ms-sel-item {
            line-height: 22px;
            color: #000000;
            font-size: 13px;
        }

        .ms-helper {
            display: none !important;
        }

        .input-quantity-wrap {
            float: left;
            margin-left: 10px;
        }

        .input-quantity {
            width: 90px;
            line-height: 36px;
            padding: 0 10px;
            border: 1px solid #ddd;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
        }

        .btn-add {
            float: left;
            background: #ddd;
            padding: 9px 15px;
            margin-left: 10px;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            cursor: pointer;
        }

        .view-ddl {
            height: 36px !important;
            line-height: 36px !important;
            width: 100% !important;
            float: none !important;
        }

        .table-wp table tbody tr td label {
            line-height: 36px;
            float: left;
            margin-right: 5px;
        }

        .custom-salon .active {
            background-color: black;
            color: white;
        }

        /*.container1 {
            box-sizing: border-box;
            margin: 150px auto;
            max-width: 500px;
            padding: 0 20px;
            width: 100%;
        }*/
    </style>
</asp:Content>
<asp:Content ID="ServiceAdd" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Cấu hình số thứ tự các sản phẩm trong kho tổng, kho NCC &#187;</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="wp customer-add admin-product-add fe-service">
            <%-- Add --%>
            <div class="wp960 content-wp">
                <!-- System Message -->
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <!-- END System Message -->
                <div>
                    <table class="table-add admin-product-table-add fe-service-table-add">
                        <tbody>
                            <%-- <tr class="title-head">
                                <td><strong>CẤU HÌNH ĐỊNH LƯỢNG SẢN PHẨM DỊCH VỤ CỦA NHIỀU SALON</strong></td>
                            </tr>
                            <tr class="tr-margin" style="height: 20px;"></tr>--%>
                            <tr>
                                <td>
                                    <div>
                                        <table>
                                            <tr>
                                                <td>
                                                    <label style="float: left; line-height: 36px; font-family: Roboto Condensed Bold;">Kho tổng, NCC&#187;</label></td>
                                                <td>
                                                    <div class="container1" style="padding-left: 0px;">
                                                        <asp:DropDownList ID="ddlInventory" CssClass="form-control select" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                                    </div>
                                                </td>
                                                <%--<td>
                                                    <div class="department-select" style="padding-left: 10px;">
                                                        <label style="float: left; line-height: 36px; font-family: Roboto Condensed Bold;">Bộ phận&#187;</label>
                                                        <div class="field-wp" style="float: left; width: auto; margin-right: 10px;">
                                                            <asp:DropDownList ID="ddlDepartment" CssClass="form-control select" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="service-select" style="padding-left: 0px;">
                                                        <label style="float: left; line-height: 36px; font-family: Roboto Condensed Bold;">Dịch vụ&#187;</label>
                                                        <div class="field-wp" style="float: left; width: auto; margin-right: 10px;">
                                                            <asp:DropDownList ID="ddlService" CssClass="form-control select" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </td>--%>
                                                <td>
                                                    <input type="button" class="btn-add" onclick="viewData()" style="padding: 8px 15px" value="Xem dữ liệu" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>

                                </td>
                            </tr>
                            <%--  <tr>                                
                                <td class="col-xs-9 right">
                                    <hr />
                                </td>
                            </tr>--%>

                            <tr>
                                <%--<td class="col-xs-2 left"></td>--%>
                                <td class="col-xs-9 right">
                                    <hr />
                                </td>
                            </tr>
                            <tr class="tr-field-ahalf tr-product">
                                <%--<td class="col-xs-2 left"><span></span></td>--%>
                                <td class="col-xs-9 right">

                                    <%--<div class="listing-product item-product" id="ListingProductWp" runat="server" clientidmode="Static">
                                        <table class="table table-listing-product table-item-product" id="table-item-product">
                                            <thead>
                                                <tr>
                                                    <th>STT</th>
                                                    <th>Tên sản phẩm</th>
                                                    <th style="width: 200px">Sắp xếp</th>
                                                    <th style="width: auto"></th>
                                                </tr>
                                            </thead>
                                            <tbody>                                               
                                            </tbody>
                                        </table>
                                    </div>--%>
                                    <div class="container table-listing pt-2">
                                    </div>

                                    <div class="table-temp d-none" style="display: none!important">
                                        <table class="table table-bordered table-hover" style="float: left;">
                                            <thead>
                                                <tr>
                                                    <th>STT</th>
                                                    <th>Tên sản phẩm</th>
                                                    <th style="width: 200px">Sắp xếp</th>
                                                    <th style="width: auto"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <%-- <tr>
                                <td>
                                    <div class="suggestion-wrap">
                                        <input id="ms" class="form-control" type="text" />
                                    </div>
                                    <div class="btn-add" onclick="addProduct()" style="padding: 8px 15px">Thêm sản phẩm</div>
                                </td>
                            </tr>--%>

                            <tr class="tr-send">
                                <%--<td class="col-xs-2 left"></td>--%>
                                <td class="col-xs-9 right no-border">
                                    <span class="field-wp">
                                        <asp:Panel ID="BtnSend" CssClass="btn-send" runat="server" ClientIDMode="Static">Hoàn Tất</asp:Panel>
                                        <asp:Button ID="BtnFakeSend" OnClick="FuncComplete" runat="server" Text="Hoàn tất" ClientIDMode="Static" Style="display: none;"></asp:Button>
                                    </span>
                                </td>
                            </tr>
                            <asp:HiddenField runat="server" ID="UrlKeyField" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="ImgCoverField" ClientIDMode="Static" />

                        </tbody>
                    </table>


                    <!-- input hidden -->
                    <asp:HiddenField ID="HDF_InventoryId" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_ProductIds" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_ServiceIds" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_UserImages" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_UserImagesDelete" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="Recipient" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HairMassage" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="Cosmetic" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_SalonId" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_lgId" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_BillId" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_FreeService" ClientIDMode="Static" runat="server" />
                    <asp:HiddenField ID="HDF_Rating" ClientIDMode="Static" runat="server" />
                    <!-- END input hidden -->
                </div>
            </div>

            <%-- end Add --%>
        </div>

        <script src="/assets/js/jquery.mCustomScrollbar.js"></script>
        <link href="/assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" />
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 200px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 35px !important;
                padding-top: 1px !important;
            }
        </style>
        <script type="text/javascript">

            var URL_INVENTORY = "<%= Libraries.AppConstants.URL_API_INVENTORY %>";
            var itemProduct;
            var quantity;
            var index;
            var ms;
            // Btn Send
            $("#BtnSend").bind("click", function () {
                let result = true;

                //var selected = $('div.selected-wrapper a');

                //if (selected == null || selected.length == 0) {
                //    ShowMessage("Chú ý", 'Yêu cầu chọn salon.', 3, 7000);
                //    return;
                //}
                //var ddlDepartment = $('#ddlDepartment').val();
                //if (ddlDepartment == "" || ddlDepartment == "0") {
                //    ShowMessage("Chú ý", "Yêu cầu chọn Bộ phận!", 3);
                //    return;
                //}

                //var ddlService = $('#ddlService').val();
                //if (ddlService == "" || ddlService == "0") {
                //    ShowMessage("Chú ý", "Yêu cầu chọn Dịch vụ!", 3);
                //    return;
                //}
                //var listIv = "";
                //selected.each(function () {
                //    var THIS = $(this);
                //    var ivId = THIS.attr("value");
                //    if (ivId != "undefined" && ivId != "") {
                //        listIv += ivId + ",";
                //    }
                //});
                //if (listIv.length > 0) {
                //    listIv = listIv.substr(0, listIv.length - 1);
                //}
                var ddlInventory = $('#ddlInventory').val();
                if (ddlInventory == "" || ddlInventory == "0") {
                    ShowMessage("Chú ý", "Yêu cầu chọn Kho!", 3);
                    return;
                }

                let index = 0;
                if ($("#tableListing tbody tr").length == 0) {
                    ShowMessage("Chú ý", "Yêu cầu thêm sản phẩm!", 3);
                    return;
                }



                var listNameDuplicate = [];
                var indexArr = [];

                $('.table-temp table tbody tr').each(function () {
                    var THIS = $(this);
                    //let quantify = THIS.find("input.product-quantify").val();
                    //let volume = THIS.find("input.product-volume").val();
                    let orderNum = THIS.find("input.order-num").val();
                    let name = THIS.find("td.td-product-name").text();
                    if (orderNum == '' || orderNum == '0') {
                        ShowMessage("Chú ý", 'Nhập số thứ tự cho sản phẩm: ' + name, 3, 7000);
                        result = false;
                    }

                    //debugger
                    var idx = indexArr.indexOf(orderNum);
                    //var Id = THIS.find("td.td-product-code").attr("data-id");
                    if (idx >= 0) {

                        ShowMessage("Chú ý", "Các sản phẩm [" + listNameDuplicate[idx] + ", " + name + "] đang có số thứ tự giống nhau, cần phải điều chỉnh!", 3);
                        result = false;
                    }
                    if (!result) {
                        return;
                    }
                    if (orderNum != 999999) {
                        indexArr.push(orderNum);
                        listNameDuplicate.push(name);
                    }
                });

                startLoading();
                debugger
                if (result) {
                    var Ids = [];
                    var prd = {};
                    var table = $('#tableListing').DataTable();

                    var tableTemp = $('.table-temp table tbody tr');
                    //$("#tableListing tbody tr")
                    tableTemp.each(function () {
                        prd = {};
                        var THIS = $(this);
                        var Id = THIS.find("td.td-product-name").attr("data-id"),
                            orderNum = THIS.find("input.order-num").val();
                        //debugger;
                        // check value
                        Id = Id.toString().trim() != "" ? parseInt(Id) : 0;
                        prd.ProductId = Id;
                        prd.OrderNum = orderNum;
                        if (orderNum != 999999) {
                            Ids.push(prd);
                        }
                    });
                    Ids = JSON.stringify(Ids);
                    $("#HDF_ProductIds").val(Ids);
                    $("#HDF_InventoryId").val(ddlInventory);

                    //console.log($("#HDF_ProductIds").val());
                    $("#BtnFakeSend").click();

                }
                finishLoading();
            });

            jQuery(document).ready(function () {
                var qs = getQueryStrings();
                ShowMessage("Thông báo", qs["msg_message"], parseInt(qs["msg_status"]));

                debugger
                var inventoryId = "<%= Id %>";
                if (inventoryId != null && inventoryId > 0) {
                    $('#ddlInventory').val(inventoryId);
                    $('#ddlInventory').change();
                    viewData();
                }

                $('#selectMultiple').multi({
                    search_placeholder: 'Search Salon...',
                });

                // bind data to dropdownlist
                ms = $('#ms').magicSuggest({
                    maxSelection: 1,
                    data: <%=ListProduct %>,
                    valueField: 'Id',
                    displayField: 'Name',
                });

                $(ms).on('selectionchange', function (e, m) {
                    itemProduct = null;
                    var listProduct = this.getSelection();
                    console.log(listProduct);
                    var listData = ms.getData();
                    if (listProduct.length > 0) {
                        for (var i = 0; i < listData.length; i++) {
                            if (listProduct[0].Id == listData[i].Id) {
                                itemProduct = listProduct[0];
                                $("#inputVolume").focus();
                                break;
                            }
                            else {
                                continue;
                            }
                        }
                        if (itemProduct == null) {
                            ShowMessage("Chú ý", "Lễ tân chú ý chọn đúng sản phẩm trong danh sách vật tư dịch vụ.", 3, 7000);
                            ms.clear();
                            $(".ms-sel-ctn input[type='text']").focus();
                        }
                    }
                });
            });


            function changeValue(THIS, index) {
                debugger
                var productId = THIS.find("td.td-product-name").attr("data-id");
                var orderNum = THIS.find("input.order-num").val();
                var tableTemp = $('.table-temp table');
                var input = tableTemp.find("td[data-id='" + productId + "']").each(function () {
                    debugger
                    var THIS = $(this).parent();
                    var tmp = THIS.find("input.order-num");
                    tmp.val(orderNum);
                });
            }


            function viewData() {
                let panelStaff = $('#pills-staff').hasClass('active');
                let panelSalon = $('#pills-salon').hasClass('active');
                let panelAll = $('#pills-all').hasClass('active');

                //
                debugger;

                var ddlInventory = $('#ddlInventory').val();
                if (ddlInventory == "" || ddlInventory == "0") {
                    ShowMessage("Chú ý", "Yêu cầu chọn Kho!", 3);
                    return;
                }

                startLoading();

                let api = "/api/orderNum/list" + "?InventoryId=" + ddlInventory;
                let response = AjaxGetApi(URL_INVENTORY + api);
                let objTr = "";
                debugger;
                $("#tableListing tbody").html("");

                if (typeof response === "object") {
                    $('.table-listing').empty();
                    var tableTemp = $('.table-temp table').attr('id', 'tableListing').clone();
                    $('.table-listing').append(tableTemp);
                    $.each(response,
                        function (index, value) {
                            //debugger;
                            objTr += '<tr>' +
                                '<td style="text-align: center;">' + (index + 1) + '</td>' +
                                '<td class="td-product-name" style="text-align:center;" data-id="' + value.productId + '">' + value.productName + '</td>' +
                                '<td style="text-align:center;" class="td-order-num">' +
                                '<input class="order-num" value="' + value.orderNum + '" type="text" onkeypress="return ValidateKeypress(/\\d/,event);" onblur="changeValue($(this).parent().parent(),' + index + ');" style="margin: 0 auto; float: none; text-align: center; width: 80px;" maxlength="6" placeholder="Số thứ tự"/>' +
                                '</td>' +
                                '<td class=" map-edit">' +
                                '<div class="edit-wp">' +
                                //'<a class="elm del-btn" onclick="RemoveItem($(this).parent().parent().parent(),\'' + value.name + '\',\'product\')" href="javascript://" title="Xóa"></a>' +
                                '</div>' +
                                '</td>' +
                                '</tr>';
                        });
                    $("#tableListing tbody").append(objTr);
                    ConfigTableListing();
                }
                finishLoading();
            };
            function ConfigTableListing() {
                var table = $('#tableListing').DataTable(
                    {
                        order: [[0, 'asc'], [1, 'asc']],
                        rowGroup: {
                            dataSrc: 0,
                        },
                        pagingType: "full_numbers",
                        language: {
                            lengthMenu: "Dòng / trang _MENU_",
                            paginate: {
                                previous: "<",
                                first: "Đầu",
                                last: "Cuối",
                                next: ">"
                            },
                            info: "Dòng _START_ tới _END_ của _TOTAL_ dòng",
                            search: "Nhập tìm kiếm:"
                        },
                        lengthMenu: [[10, 25, 50, 200], [10, 25, 50, 200]]
                    });
            }



            // Update order item
            function UpdateItemOrder(dom) {
                var index = 1;
                dom.find("tr").each(function () {
                    $(this).find("td.td-product-index").text(index);
                    index++;
                });
            }

            // Remove item đã được chọn
            function RemoveItem(THIS, name, itemName) {
                // Confirm
                $(".confirm-yn").openEBPopup();

                $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn hủy [ " + name + " ] ?");
                $("#EBPopup .yn-yes").bind("click", function () {
                    var Code = THIS.find(".td-staff-code").attr("data-code");
                    $("#quick-" + itemName).find("input[data-code='" + Code + "']").removeAttr("checked");
                    THIS.remove();
                    //getDataProduct();
                    UpdateItemDisplay(itemName);
                    autoCloseEBPopup(0);
                });
                $("#EBPopup .yn-no").bind("click", function () {
                    autoCloseEBPopup(0);
                });
            }

            //reset input
            function resetInputForm() {
                ms.clear();
                $("#inputQuantify").val('');
                $("#inputVolume").val('');
                $(ms).focus();
                $(".ms-sel-ctn input[type='text']").focus();
            }

            // Update table display
            function UpdateItemDisplay(itemName) {
                var dom = $(".table-item-" + itemName);
                var len = dom.find("tbody tr").length;
                if (len == 0) {
                    dom.parent().hide();
                } else {
                    UpdateItemOrder(dom.find("tbody"));
                }
            }

            // Validate keypress
            function ValidateKeypress(numcheck, e) {
                var keynum, keychar, numcheck;
                if (window.event) {
                    keynum = e.keyCode;
                }
                else if (e.which) {
                    keynum = e.which;
                }
                if (keynum == 8 || keynum == 127 || keynum == null || keynum == 9 || keynum == 0 || keynum == 13) return true;
                keychar = String.fromCharCode(keynum);
                var result = numcheck.test(keychar);
                return result;
            }
            function activeStaff() {
                preventDefault();
                var list = $('.custom-salon button');
                $.each(list, function (index, value) {
                    $(value).removeClass('active');
                });
                $('#ddlDepartment').removeAttr('disabled');
                $('#ddlDepartment').val("0");

                $('#ddlInventorySalon').removeAttr('disabled');
                $('#ddlInventorySalon').val("0");
                $('select').select2();
                $('.custom-salon button.staff').addClass('active');
            }
            function activeSalon() {
                preventDefault();
                var list = $('.custom-salon button');
                $.each(list, function (index, value) {
                    $(value).removeClass('active');
                });
                $('#ddlDepartment').attr('disabled', 'disabled');
                $('#ddlDepartment').val("0");

                $('#ddlInventorySalon').removeAttr('disabled');
                $('#ddlInventorySalon').val("0");

                $('select').select2();
                $('.custom-salon button.salon').addClass('active');
            }
            function activeAll() {
                preventDefault();
                var list = $('.custom-salon button');
                $.each(list, function (index, value) {
                    $(value).removeClass('active');
                });
                $('#ddlDepartment').attr('disabled', 'disabled');
                $('#ddlDepartment').val("0");

                $('#ddlInventorySalon').attr('disabled', 'disabled');
                $('#ddlInventorySalon').val("0");
                $('select').select2();
                $('.custom-salon button.all').addClass('active');
            }
        </script>
        <!-- Page loading -->
        <div class="page-loading">
            <p>Vui lòng đợi trong giây lát...</p>
        </div>
    </asp:Panel>
</asp:Content>


