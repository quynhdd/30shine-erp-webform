﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Migrations;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.IvInventory
{
    public partial class IvCheckout : System.Web.UI.Page
    {

        protected List<DataView> dataViews = new List<DataView>();
        protected static int validDay = 0;
        CultureInfo culture = new CultureInfo("vi-VN");
        protected string WHName = "";
        protected int SalonId = 0;
        private bool Perm_Access = false;
        protected bool Perm_AddNew = false;
        protected bool Perm_Delete = false;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                Solution_30shineEntities db = new Solution_30shineEntities();
                SalonId = Convert.ToInt32(Session["SalonId"]);
                WHName = db.IvInventories.FirstOrDefault(w => w.SalonId == SalonId && w.IsDelete == false).Name;
                GetData();
                
                var dayOfWeek = DateTime.Now.DayOfWeek.ToString();
                //lblDay.Text = Convert.ToInt32(dayOfWeek).ToString();//((DateTime.Now.Day - 1)/7+1).ToString();
                var today = DateTime.Now;
                var tommorow = DateTime.Now.AddDays(1);
                var checkTime = Convert.ToInt32(ConfigurationManager.AppSettings["setTime"]);
                var st_date_1 = ConfigurationManager.AppSettings["1st_date_1"].ToString();
                var st_date_2 = ConfigurationManager.AppSettings["1st_date_2"].ToString();
                                                                           
                var nd_date_1 = ConfigurationManager.AppSettings["2nd_date_1"].ToString();
                var nd_date_2 = ConfigurationManager.AppSettings["2nd_date_2"].ToString();
                var time_begin = ConfigurationManager.AppSettings["time_begin"].ToString();
                var time_end = ConfigurationManager.AppSettings["time_end"].ToString();
                var _time_begin = DateTime.ParseExact(time_begin, "HH:mm:ss",
                                        culture);
                var _time_end = DateTime.ParseExact(time_end, "HH:mm:ss",
                                        culture);
                if (checkTime == 0)
                {
                    validDay = 1;
                }
                else
                {
                    if (dayOfWeek == st_date_1 && today.Hour >= _time_begin.Hour && today.Minute >= _time_begin.Minute && today.Second >= _time_begin.Second)
                    {
                        validDay = 1;
                    }
                    else if (dayOfWeek == st_date_2 && today.Hour <= _time_end.Hour && today.Minute <= _time_end.Minute && today.Second <= _time_end.Second)
                    {
                        validDay = 1;
                    }
                    else if(dayOfWeek == nd_date_1 && today.Hour >= _time_begin.Hour && today.Minute >= _time_begin.Minute && today.Second >= _time_begin.Second)
                    {
                        validDay = 1;
                    }
                    else if (dayOfWeek == nd_date_2 && today.Hour <= _time_end.Hour && today.Minute <= _time_end.Minute && today.Second <= _time_end.Second)
                    {
                        validDay = 1;
                    }
                }
                //if (dayOfWeek == "Monday" && today.Hour >= 21 && tommorow.Hour < 00)
                //{
                //    validDay = 1;
                //}
                //else if(dayOfWeek == "Thursday" && ((today.Hour >= 00 && today.Hour < 02) || (today.Hour >= 21 && tommorow.Hour < 00)))
                //{
                //    validDay = 1;
                //}
                //else if (dayOfWeek == "Wednesday" && today.Hour >= 00 && today.Hour <02)
                //{
                //    validDay = 1;
                //}
            }
        }

        /// <summary>
        /// set permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_AddNew = permissionModel.CheckPermisionByAction("Perm_AddNew", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        /// <summary>
        /// execute
        /// </summary>
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }

        }

        /// <summary>
        /// get data
        /// </summary>
        private void GetData()
        {
            var result = new HttpClient().GetAsync(Libraries.AppConstants.URL_API_INVENTORY + "/api/iv-checkout?salonId=" + SalonId).Result;
            if (result.IsSuccessStatusCode)
            {
                var data = result.Content.ReadAsStringAsync().Result;
                if (data != null)
                {
                    dataViews = new JavaScriptSerializer().Deserialize<List<DataView>>(data);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected class DataView
        {
            public int CheckoutId { get; set; }
            public int ProductId { get; set; }
            public string Code { get; set; }
            public string BarCode { get; set; }
            public string ProductName { get; set; }
            public int Quantity { get; set; }
        }
    }
}