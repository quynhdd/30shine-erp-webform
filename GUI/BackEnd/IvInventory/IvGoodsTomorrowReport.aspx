﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMasterVersion1.Master" AutoEventWireup="true" CodeBehind="IvGoodsTomorrowReport.aspx.cs" Inherits="_30shine.GUI.BackEnd.IvInventory.IvGoodsTomorrowReport" %>

<asp:Content ID="Content2" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel ID="ContentWrap" runat="server" ClientIDMode="Static">
        <div class="container-fluid sub-menu">
            <div class="row">
                <div class="col-md-12 pt-3 pb-2">
                    <ul>
                        <li>Báo cáo &nbsp;&#187;&nbsp; Thống kê vật tư Salon order ngày mai</li>
                    </ul>
                </div>
                <div class="col-md-12 form-group">
                    <div class="sub-menu-boder"></div>
                </div>
            </div>
        </div>
        <form runat="server">
            <div class="container-fluid">
                <div class="row">
                    <div class="form-group col-xl-auto col-lg-auto" style="margin-top: 5px">
                        <strong><i class="fa fa-filter"></i>Lọc kết quả &nbsp;&#187; </strong>
                    </div>
                    <div class="col-xl-auto col-lg-auto col-md-auto col-sm-auto form-group">
                        <asp:TextBox CssClass="txtDateTime st-head form-control reset-table" ID="TxtDateTimeFrom" placeholder="Chọn ngày"
                            ClientIDMode="Static" runat="server"></asp:TextBox>
                    </div>
                    <div class="col-xl-auto col-lg-auto col-md-12 col-sm-auto form-group">
                        <label class="mr-2 lable-text">Loại kho</label>
                        <asp:DropDownList ID="ddlInventoryType" onchange="GetListInventory()" runat="server" ClientIDMode="Static" CssClass="select reset-table"></asp:DropDownList>
                    </div>
                    <div class="col-xl-auto col-lg-auto col-md-12 col-sm-auto form-group">
                        <label class="mr-2 lable-text">Kho/NCC</label>
                        <select class="form-control select" id="ddlInventory" onchange="GetSalonInventoryId();">
                        </select>
                    </div>
                    <div class="form-group col-md-12 col-xl-auto">
                        <label class="label">Kho Salon&nbsp;</label>
                        <asp:DropDownList ID="ddlInventorySalon" data-name="inventoryId" CssClass="form-control select" runat="server" ClientIDMode="Static"></asp:DropDownList>
                    </div>
                    <div class="col-xl-auto col-lg-auto col-md-12 col-sm-auto form-group">
                        <label class="mr-2 lable-text">Vật tư</label>
                        <asp:DropDownList ID="ddlProduct" runat="server" ClientIDMode="Static" CssClass="select reset-table" Style="width: 200px"></asp:DropDownList>
                    </div>
                    <div class="form-group col-xl-auto col-lg-auto col-md-12 col-sm-auto form-group">
                        <div class="form-group col-md-12 col-xl-auto pl-0">
                            <asp:Panel ID="ViewDataFilter" CssClass="st-head btn-viewdata" ClientIDMode="Static" runat="server">
                                Xem dữ liệu
                            </asp:Panel>
                            <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                        </div>
                    </div>
                    <div class="col-md-12 col-xl-12 form-group">
                        <div class="sub-menu-boder-low"></div>
                    </div>
                </div>
            </div>
            <% if (TotalReport.listInventory.Count > 0)
                { %>
            <div class="container-fluid table-listing pt-2">
                <table id="tableListing" class="table table-bordered" style="float: left;">
                    <thead>
                        <tr>
                            <th class="text-center align-middle" style="max-width: 20px">STT</th>
                            <th class="text-center align-middle" style="max-width: 100px">Kho Salon</th>
                            <%foreach (var v in TotalReport.listInventory)
                                { %>
                            <th class="text-center align-middle">
                                <%=v.productName %>
                            </th>
                            <%} %>
                        </tr>
                    </thead>
                    <tbody>
                        <%-- Sumary --%>
                        <tr role="row" class="odd total-report" style="background: #ddd">
                            <td></td>
                            <td style="text-align: center !important">Tổng </td>
                            <% foreach (var v in TotalReport.listInventory)
                                { %>
                            <td>
                                <%=v.quantityOrder %>
                            </td>
                            <%} %>
                        </tr>

                        <%-- Detail --%>
                        <%var index = 1; foreach (var v in listData)
                            {%>
                        <tr role="row" class="odd">
                            <td>
                                <%=index ++ %>
                            </td>
                            <td>
                                <%=v.inventoryName %>
                                
                            </td>
                            <%foreach (var item in v.listInventory)
                                { %>
                            <td>
                                <%if (item.quantityOrder > 0)
                                    {%>
                                <span style="color: red"><%= String.Format("{0:#,###}", item.quantityOrder).Replace(",", ".") %></span>
                                <%} %>
                               
                            </td>
                            <%} %>
                        </tr>
                        <%} %>
                    </tbody>
                </table>
            </div>
            <%} %>
            <asp:HiddenField runat="server" ID="HDFInventoryID" ClientIDMode="Static" />
            <asp:HiddenField runat="server" ID="HDFSalonInventoryID" ClientIDMode="Static" />
            <asp:HiddenField runat="server" ID="HDFProductID" ClientIDMode="Static" />
        </form>
        <div class="page-loading">
            <p>Vui lòng đợi trong giây lát...</p>
        </div>
        <style>
            .menu-time {
                margin-right: 7px;
                font-size: 13px;
            }

                .menu-time .label {
                    padding: 0px 15px;
                    background: #000000;
                    color: #ffffff;
                    cursor: pointer;
                    border-radius: 15px;
                    line-height: 26px;
                    width: 94px;
                }

            .form-control {
                height: 31px !important;
            }

            .select2.select2-container.select2-container--default {
                width: 180px !important;
            }

            .lable-text {
                width: 46px;
            }

            #tableListing_info {
                float: left;
            }

            #tableListing_paginate {
                clear: both;
            }

            .dt-buttons.btn-group {
                float: left;
            }

            #tableListing_wrapper button.buttons-excel {
                background-color: black;
                width: 100px;
            }

                #tableListing_wrapper button.buttons-excel span {
                    color: white
                }

            #tableListing tbody tr td {
                text-align: center !important;
            }

                #tableListing tbody tr td:nth-child(2) {
                    text-align: left !important;
                }
        </style>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                ConfigTableListing();
                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) { }
                });
                GetListInventory();
            });

            //
            var elms = {
                inventoryIdExport: '#ddlInventory',
                invetoryIdOrder: '#ddlInventorySalon',
                productId: '#ddlProduct'
            };

            // ConfigTableListing
            function ConfigTableListing() {
                var height = $(document).height();
                var table = $('#tableListing').DataTable(
                    {
                        dom: 'Bfrtip',
                        buttons: [
                            'excelHtml5',
                        ],
                        paging: false,
                        searching: false,
                        info: false,
                        fixedHeader: true,
                        ordering: false,
                        fixedColumns: {
                            leftColumns: 2
                        },
                        scrollY: "70vh",
                        scrollX: true,
                        scrollCollapse: true,
                    });
            }

            // Check validate
            $("#ViewDataFilter").bind("click", function () {
                var kt = true;
                var error = '';
                var dateTime = $("#TxtDateTimeFrom").val();
                var inventoryTypeID = $("#ddlInventoryType").val();
                var inventoryId = $("#ddlInventory").val();
                if (!CheckParameter(dateTime)) {
                    kt = false;
                    error = "Bạn phải chọn ngày tháng!";
                    $('#TxtDateTimeFrom').css("border-color", "red");
                }
                else if (!CheckParameter(inventoryTypeID)) {
                    kt = false;
                    error = "Bạn phải chọn loại kho!";
                    $('#ddlInventoryType').css("border-color", "red");
                }
                else if (!CheckParameter(inventoryId)) {
                    kt = false;
                    error = "Bạn phải chọn kho hoặc nhà cung cấp!";
                    $('#ddlInventory').css("border-color", "red");
                }
                if (!kt) {
                    ShowMessage('', error, 3);
                }
                else {
                    var inventoryIdExport = $(elms.inventoryIdExport).val();
                    var invetoryIdOrder = $(elms.invetoryIdOrder);
                    var productId = $(elms.productId).val();
                    var arrSalonInventoryId = [];
                    if (invetoryIdOrder.val() !== '0') {
                        arrSalonInventoryId.push(invetoryIdOrder.val());
                    }
                    else {
                        $(invetoryIdOrder).find('option').each(function (index, value) {
                            let id = $(value).val();
                            if (id !== '0') {
                                arrSalonInventoryId.push($(value).val());
                            }
                        });
                    }
                    $("#HDFInventoryID").val(inventoryIdExport);
                    $("#HDFSalonInventoryID").val(arrSalonInventoryId);
                    $("#HDFProductID").val(productId);
                    startLoading();
                    $("#BtnFakeUP").click();
                }
            });

            // get list inventory
            function GetListInventory() {
                let $value = $('#ddlInventoryType').val();
                if (!CheckParameter($value)) {
                    ShowMessage('', 'Vui lòng chọn loại kho', 3, 7000);
                }
                else {
                    $.ajax({
                        type: "POST",
                        url: "/GUI/BackEnd/IvInventory/IvProgressReport.aspx/GetListInventory",
                        data: '{inventoryType: ' + parseInt($value) + ' }',
                        crossDomain: true,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            $("#ddlInventory").html('').val('0');
                            $("#select2-ddlInventory-container").val('0').text('Chọn tất cả');
                            $("#ddlInventory").append($("<option />").val(0).text('Chọn tất cả'));
                            $.each(data.d, function () {
                                $("#ddlInventory").append($("<option />").val(this.Id).text(this.Name));
                            });
                        },
                        failure: function () {
                            ShowMessage('', 'Có lỗi xảy ra, vui lòng liên hệ nhà phát triển!', 4);
                        }
                    });
                }
            }

            // get salon inventoryId
            function GetSalonInventoryId() {
                let $value = $("#ddlInventory").val();
                if (!CheckParameter($value)) {
                    ShowMessage('', '', 3, 5000);
                }
                else {
                    var inventoryId = parseInt($value);
                    console.log(inventoryId);
                    $.ajax({
                        type: "POST",
                        url: "/GUI/BackEnd/IvInventory/IvGoodsTomorrowReport.aspx/GetListSalonInventory",
                        data: '{inventoryId:' + inventoryId + '}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            $("#ddlInventorySalon").html('').val('0');
                            $("#select2-ddlInventorySalon-container").val('0').text("Chọn tất cả");
                            $("#ddlInventorySalon").append($("<option />").val(0).text("Chọn tất cả"));
                            $.each(data.d, function () {
                                $("#ddlInventorySalon").append($("<option />").val(this.Id).text(this.Name));
                            });
                        },
                        failure: function () {
                            ShowMessage('', 'Có lỗi xảy ra, vui lòng liên hệ nhà phát triển!', 4);
                        }
                    });
                }
            }

            // check parameter
            function CheckParameter(param) {
                if (param === undefined || param === null || param === '' || param === '0') {
                    return false;
                }
                return true;
            }
        </script>
    </asp:Panel>
</asp:Content>
