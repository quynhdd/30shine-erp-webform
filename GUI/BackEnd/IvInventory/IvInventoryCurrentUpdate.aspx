﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="IvInventoryCurrentUpdate.aspx.cs" Inherits="_30shine.GUI.BackEnd.IvInventory.IvInventoryCurrentUpdate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Quản lý kho - Lập đơn đặt hàng</title>
</asp:Content>
<asp:Content ID="ServiceAdd" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý hàng tồn kho  &nbsp;&#187;</li>
                        <li class="li-add"><a href="javascript:void(0);">Nhập hàng tồn cuối ngày</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="wp customer-add admin-product-add fe-service">
            <div class="wp960 content-wp">
                <div class="table-wp">
                    <table class="table-add admin-product-table-add fe-service-table-add">
                        <tbody>
                            <tr class="title-head">
                                <td><strong>Theo dõi nhập/xuất tồn kho trong ngày</strong></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr class="tr-field-ahalf" runat="server" id="TrSalon">
                                <td class="col-xs-2 left"></td>
                                <td class="col-xs-9 right">
                                    <label style="float: left; line-height: 36px; padding-right: 10px; font-weight: normal; font-family: Roboto Condensed Bold;">Ngày: </label>
                                    <div class="datepicker-wp">
                                        <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeFrom" placeholder="Chọn ngày"
                                            ClientIDMode="Static" runat="server" Style="width: 160px; margin-right: 10px;"></asp:TextBox>
                                    </div>
                                    <label style="float: left; line-height: 36px; padding-right: 10px; font-weight: normal; font-family: Roboto Condensed Bold; margin-left: 10px">Salon: </label>
                                    <div class="field-wp" style="float: left; width: auto;">
                                        <asp:DropDownList ID="Salon" runat="server" CssClass="form-control select" ClientIDMode="Static" Style="width: 300px;"></asp:DropDownList>
                                    </div>
                                    <div class="field-wp" style="float: left; width: auto;">
                                        <input type="button" class="btn btn-info" value="Xem dữ liệu" style="margin-left: 20px; width: 120px" onclick="bindAllGoods()" id="btnView" />
                                    </div>
                                </td>
                            </tr>
                            <tr class="tr-field-ahalf tr-product">
                                <td class="col-xs-2 left"></td>
                                <td class="col-xs-9 right">
                                    <div class="listing-product item-product" id="ListingProductWp" runat="server" clientidmode="Static">
                                        <table class="table table-listing-product table-item-product" id="table-item-product">
                                            <thead>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr class="button" style="display: none">
                                <td></td>
                                <td>
                                    <div class="popup-product-btn btn-complete" style="margin-left: 20px" id="btnSuccess" data-item="product" onclick="Success()">Hoàn tất</div>
                                    <div class="popup-product-btn btn-esc" id="btnCancel" onclick="Cancel()">Hủy bỏ</div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
        <script src="/assets/js/jquery.mCustomScrollbar.js"></script>
        <link href="/assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" />
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 300px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 35px !important;
                padding-top: 1px !important;
            }
        </style>
        <script type="text/javascript">
            var check = '<%=CheckUpdateInventory()%>';
            jQuery(document).ready(function () {
                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) { },
                    scrollMonth: false,
                    scrollTime: false,
                    scrollInput: false
                });
            });

            //get url
            var URL_API_INVENTORY = "<%= Libraries.AppConstants.URL_API_INVENTORY %>";
            var inventoryCurrent;
            // bind data in table
            function bindAllGoods() {
                var kt = true;
                var error = '';
                var date = $("#TxtDateTimeFrom").val();
                var inventoryId = $("#Salon").val();
                if (date === "") {
                    kt = false;
                    error += " Bạn phải chọn ngày tháng! ";
                    $('#TxtDateTimeFrom').css("border-color", "red");
                }
                if (inventoryId === "" || inventoryId === "0") {
                    kt = false;
                    error += " Bạn phải chọn Salon ";
                    $('#salonId').css("border-color", "red");
                }
                if (!kt) {
                    ShowMessage('', error, 3);
                }
                else {
                    startLoading();
                    $.ajax({
                        type: "GET",
                        url: URL_API_INVENTORY + `/api/iv-inventory-current/get-list-product-inventory-current?inventoryId=${inventoryId}&createdDate=${date}`,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            $("#table-item-product").empty();
                            $(".button").hide();
                            //console.log(response);
                            if (response.length > 0) {
                                var str = "";
                                str += ` <thead>
                                            <tr>
                                                <th>STT</th>
                                                <th>Tên sản phẩm</th>
                                                <th>Mã sản phẩm</th>
                                                <th style="width: 100px;">SL tồn đầu</th>
                                                <th style="width: 100px;">SL nhập kho</th>
                                                <th style="width: 100px;">SL xuất kho</th>
                                                <th style="width: 100px;">SL tồn cuối</th>
                                <% if (Perm_ViewAllData)
            { %>
                                                <th style="width: 100px;">Giá sản phẩm</th>
                                            <%} %>
                                            </tr>
                                         </thead>`;
                                $.each(response, function (i, v) {
                                    str += `
                                    <tr>
                                    <td class="td-product-index"> ${i + 1} </td> 
                                    <td class="td-product-name" > ${v.productName} </td>
                                    <td class="td-product-code" data-id='${v.productId}' data-code='${v.codeProduct}'>${v.codeProduct}</td>
                                    <td class="td-product-quantity map-edit"> ${v.quantityBegin}</td>
                                    <td class="td-product-quantity map-edit">${v.import}</td>
                                    <td class="td-product-quantity map-edit">${v.export} </td>
 
                                    <td class="td-product-quantity map-edit">
                                    <input type="text" class="product-quantity-end" style="width:80px;text-align: center;" value='${v.quantityEnd}' onkeypress="return ValidateKeypress(/\\d/,event);" onchange="checkInput();" />
                                </td>
                        <% if (Perm_ViewAllData)
            { %>
                                    <td class="td-product-price" > ${v.price} </td> 
                                <%} %>
                                    </tr>`;
                                });
                                $("#table-item-product").empty().append($(str));
                                $("#ListingProductWp").show();
                                $(".button").show();
                            }
                            finishLoading();
                        },
                        failure: function (response) {
                            ShowMessage('', response, 4);
                        }
                    });
                }
            }
            //check input
            function checkInput() {
                var kt = true;
                var error = '';
                $("table.table-item-product tbody tr").each(function () {
                    var THIS = $(this);
                    var quantityEnd = THIS.find("input.product-quantity-end").val();
                    if (quantityEnd === "" || quantityEnd === "undefined") {
                        error = "Bạn phải nhập số lượng";
                        $('input.product-quantity-end').css("border-color", "red");
                        kt = false;
                    }
                    // !kt xuất hiện thông báo
                    if (!kt) {
                        ShowMessage('', error, 3);
                    }
                });
                return kt;
            }
            //Get products
            function getProductIds() {
                var Ids = [];
                var prd = {};
                $("table.table-item-product tbody tr").each(function () {
                    prd = {};
                    var THIS = $(this);
                    var id = THIS.find("td.td-product-code").attr("data-id");
                    var quantityEnd = THIS.find("input.product-quantity-end").val();
                    id = id.toString().trim() !== "" ? parseInt(id) : 0;
                    quantityEnd = quantityEnd.toString().trim() !== "" ? parseInt(quantityEnd) : 0;
                    prd.id = id;
                    prd.quantityEnd = parseFloat(quantityEnd);
                    Ids.push(prd);
                });
                inventoryCurrent = Ids;
            }
            // add product
            function Success() {
                if (check === "True") {
                    if (checkInput()) {
                        getProductIds();
                        var product = inventoryCurrent;
                        var inventoryId = $('#Salon').val();
                        inventoryId = inventoryId.toString().trim() !== "" ? parseInt(inventoryId) : 0;
                        var data = JSON.stringify({ inventoryId, product: product });
                        startLoading();
                        $.ajax({
                            url: URL_API_INVENTORY + `/api/iv-inventory-current`,
                            type: "PUT",
                            contentType: "application/json;charset:UTF-8",
                            dataType: "json",
                            data: data,
                            success: function (data) {
                                var status = data.status;
                                finishLoading();
                                if (status === 1) {
                                    ShowMessage('', data.message, 2);
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                var responseText = jqXHR.responseJSON.message;
                                ShowMessage('', responseText, 4);
                                finishLoading();
                            }
                        });
                    }
                }
                else {
                    var message = 'Hết thời gian cập nhật hàng tồn cuối ngày!';
                    ShowMessage('', message, 3);
                }
            }
            //Cancel
            function Cancel() {
                $("#table-item-product").empty();
                $(".button").hide();
            }
            // Validate keypress
            function ValidateKeypress(numcheck, e) {
                var keynum, keychar, numcheck;
                if (window.event) {
                    keynum = e.keyCode;
                }
                else if (e.which) {
                    keynum = e.which;
                }
                if (keynum === 8 || keynum === 127 || keynum === null || keynum === 9 || keynum === 0 || keynum === 13) return true;
                keychar = String.fromCharCode(keynum);
                var result = numcheck.test(keychar);
                return result;
            }
        </script>
        <!-- Page loading -->
        <div class="page-loading">
            <p>Vui lòng đợi trong giây lát...</p>
        </div>
    </asp:Panel>
</asp:Content>
