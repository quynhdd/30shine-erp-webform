﻿<%@ Page Language="C#" MasterPageFile="~/TemplateMaster/SiteMasterVersion1.Master" AutoEventWireup="true" CodeBehind="IvProductQuantifyListing.aspx.cs" Inherits="_30shine.GUI.BackEnd.IvInventory.IvProductQuantifyListing" %>

<%@ Import Namespace="Libraries" %>


<asp:Content ID="CustomerAdd" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel ID="ContentWrap" runat="server" ClientIDMode="Static">
       
        <section>
            <div class="container-fluid sub-menu">
                <div class="row">
                    <div class="col-md-12 pt-3 pb-2">
                        <ul>
                            <li>Danh sách cấu hình định lượng &nbsp;&#187; Dịch vụ &#187;</li>
                            <li><a href="/admin/cau-hinh-dinh-luong-v1/danh-sach.html"><i class="fa fa-list-ul"></i>&nbsp Danh sách định lượng</a></li>
                            <li><a class="align-middle" href="/admin/cau-hinh-dinh-luong-v1/them-moi.html"><i class="fa fa-list-ul"></i>&nbsp Thêm mới định lượng</a></li>
                            <li><a class="align-middle" href="/admin/cau-hinh-dinh-luong-v1/cap-nhat-nhieu-salon.html"><i class="fa fa-list-ul"></i>&nbsp Cập nhật nhiều salon</a></li>
                        </ul>
                    </div>
                    <div class="col-md-12">
                        <div class="sub-menu-boder"></div>
                    </div>
                </div>
            </div>

            <form runat="server">
                <div class="container pt-2 pb-2">
                    <div class="row">
                        <div class="form-group col-xl-12 sub-search">
                            <strong style="margin-right: -25px; float: left"><i class="fa fa-filter"></i>Lọc kết quả &nbsp;&#187; </strong>
                        </div>
                        <div class="col-xl-12">
                            <div class="container-fluid">
                                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="pills-staff-tab" data-toggle="pill" href="#pills-staff" role="tab" aria-controls="pills-staff" aria-selected="true">Xem theo nhân viên</a>
                                    </li>
                                    <%--<li class="nav-item">
                                        <a class="nav-link" id="pills-salon-tab" data-toggle="pill" href="#pills-salon" role="tab" aria-controls="pills-salon" aria-selected="false">Xem theo salon</a>
                                    </li>--%>
                                     <li class="nav-item">
                                        <a class="nav-link" id="pills-salon-all" data-toggle="pill" href="#pills-all" role="tab" aria-controls="pills-all" aria-selected="false">Xem toàn hệ thống</a>
                                    </li>
                                </ul>
                                <div class="tab-content p-2" id="pills-tabContent" style="border: solid 1px #e6e6e6; border-radius: 5px">
                                    <div class="tab-pane fade show active" id="pills-staff" role="tabpanel" aria-labelledby="pills-staff-tab">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="col-xl-3 pl-0">
                                                    <span>Kho Salon&#187;</span>
                                                    <asp:DropDownList ID="ddlInventorySalon" data-name="inventoryId" CssClass="form-control select" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                                </div>
                                                <div class="col-xl-3 pl-0">
                                                    <span>Bộ phận&#187;</span>
                                                    <asp:DropDownList ID="ddlDepartment" data-name="departmentId" CssClass="form-control select" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                                </div>
                                                <div class="col-xl-4 form-group " onclick="viewData()">
                                                    <div class="btn-viewdata">Xem dữ liệu</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="pills-salon" role="tabpanel" aria-labelledby="pills-salon-tab">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="col-xl-3 pl-0">
                                                    <span>Kho Salon&#187;</span>
                                                    <asp:DropDownList ID="ddlInventorySalon1" data-name="inventoryId" CssClass="form-control select" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                                </div>
                                               
                                                <div class="col-xl-4 form-group " onclick="viewData()">
                                                    <div class="btn-viewdata">Xem dữ liệu</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="pills-all" role="tabpanel" aria-labelledby="pills-all-tab">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="col-xl-4 form-group " onclick="viewData()">
                                                    <div class="btn-viewdata">Xem dữ liệu</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="container table-listing pt-2">
                </div>

                <div class="table-temp d-none">
                    <table class="table table-bordered table-hover" style="float: left;">
                        <thead>
                            <tr>
                                <th>Tên Kho </th>
                                <th>Dịch vụ</th>
                                <th>Mỹ phẩm</th>
                                <th>Bộ phận</th>
                                <th>Số khách PV</th>
                                <th>Khối lượng</th>
                                <th>Định lượng</th>
                                <th>Chức năng</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>

            </form>
        </section>
        <style>
            .select2-container--default .select2-selection--single {
                height: 31px !important;
            }

            .select2-container {
                width: 180px !important;
            }

            .nav-pills .nav-link.active, .nav-pills .show > .nav-link {
                color: #fff;
                background-color: black;
            }
            a{
                text-decoration: none !important;
            }
            a:hover{
                color: green !important;
            }
            .hover-delete:hover a.delete{
                display: block !important;
            }
        </style>
        <script>

            var domain = '<% = Libraries.AppConstants.URL_API_INVENTORY %>'
            //hoac domain = Assets/js/Config/Domain.js;
            // api cau hinh trong  Assets/js/Config/Api.js

            jQuery(document).ready(function () {

                $('.select').select2();
                $('#pills-tab').on("click",
                    function() {
                        $("#tableListing tbody").empty();
                    });
            })
            function viewData() {
                let panelStaff = $('#pills-staff').hasClass('active');
                let panelSalon = $('#pills-salon').hasClass('active');
                let panelAll = $('#pills-all').hasClass('active');
                
                let inventoryId = ~~$('.tab-pane.show.active').find('select[data-name=inventoryId]').val();
                let departmentId = ~~$('.tab-pane.show.active').find('select[data-name=departmentId]').val();
                let inventoryType = -1; 

                if (panelStaff) {
                    //inventoryType = config.inventory.type.staff;
                    //inventoryType = -1;
                    inventoryId = inventoryId == 0 ? -1 : inventoryId;
                    departmentId = departmentId == 0 ? -1 : departmentId;
                }
                else if (panelSalon) {
                    //inventoryType = config.inventory.type.salon;
                    departmentId = 0;
                    inventoryId = inventoryId == 0 ? -1 : inventoryId;
                }
                else if(panelAll){
                    departmentId = 0;
                    inventoryId = 0;
                }

                let api = IvProductQuantify.GetList + "?inventoryId=" + inventoryId + "&departmentId=" + departmentId;
                let response = AjaxGetApi(domain + api);
                let objTr = "";

                $("#tableListing tbody").html("");
                if (typeof response === "object") {
                    $('.table-listing').empty();
                    var tableTemp = $('.table-temp table').attr('id', 'tableListing').clone();
                    $('.table-listing').append(tableTemp);
                    $.each(response,
                        function (index, value) {
                            objTr += '<tr class="hover-delete" data-id= ' + value.id + '>' +
                                '<td><a href="#"  onclick="EditingPage($(this))">' + CheckExits(value.inventoryName) + '</a></td>' +
                                '<td>' + value.serviceName + '</td>' +
                                '<td>' + value.productName + '</td>' +
                                '<td>' + CheckExits(value.deparmentName) + '</td>' +
                                '<td>' + value.totalNumberService + '</td>' +
                                '<td>' + value.volume + '</td>' +
                                '<td>' + value.quantify + '</td>' +
                                '<td style="min-width:87px"><a class="d-none delete float-left mr-2" onclick="Delete($(this))" href="#"><i class="fa fa-trash mr-1" aria-hidden="true"></i><span>Xóa</span></a>'+
                                '<a class="d-none delete" href="/admin/cau-hinh-dinh-luong-v1/' + value.id + '.html"><i class="fa fa-edit mr-1" aria-hidden="true"></i><span>Sửa</span></a>' +
                                '</td>' +
                                '</tr>';
                        });
                    $("#tableListing tbody").append(objTr);
                    ConfigTableListing();
                }
            };

            // ConfigTableListing
            function ConfigTableListing() {
                var table = $('#tableListing').DataTable(
                    {
                        order: [[0, 'asc'], [1, 'asc']],
                        rowGroup: {
                            dataSrc: 0,
                        },
                        pagingType: "full_numbers",
                        language: {
                            lengthMenu: "Dòng / trang _MENU_",
                            paginate: {
                                previous: "<",
                                first: "Đầu",
                                last: "Cuối",
                                next: ">"
                            },
                            info: "Dòng _START_ tới _END_ của _TOTAL_ dòng",
                            search: "Nhập tìm kiếm:"
                        },
                        lengthMenu: [[10, 25, 50, 200], [10, 25, 50, 200]]
                    });
            }
            function EditingPage(t) {
                let id = $(t).parent().parent().data('id');
                if (typeof id === 'undefined') return;
                window.open('/admin/cau-hinh-dinh-luong-v1/' + id + '.html');
            }

            function Delete(t) {
                var id = t.parent().parent().data('id');
                if (id != null && typeof id != "undefined") {
                    api = IvProductQuantify.Delete + id;
                    let response = AjaxDeleteFromBodyApi(domain + api);
                    if (response == 200 || typeof response == "object") {
                        ShowMessage("Thông báo", "Xóa thành công !", 2);
                        t.parent().parent().remove();
                    }
                }
            }
            function CheckExits(value) {
                if (typeof value == "undefined" || value == null) {
                    return "";
                }
                else {
                    return value;
                }
            }

        </script>
        
    </asp:Panel>
</asp:Content>
