﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMasterVersion1.Master" AutoEventWireup="true" CodeBehind="IvProgressReport.aspx.cs" Inherits="_30shine.GUI.BackEnd.IvInventory.IvProgressReport" %>

<asp:Content ID="Content2" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel ID="ContentWrap" runat="server" ClientIDMode="Static">
        <div class="container-fluid sub-menu">
            <div class="row">
                <div class="col-md-12 pt-3 pb-2">
                    <ul>
                        <li>Báo cáo  &nbsp;&#187;&nbsp;Tiến độ hoàn thành đơn hàng</li>
                    </ul>
                </div>
                <div class="col-md-12 form-group">
                    <div class="sub-menu-boder"></div>
                </div>
            </div>
        </div>
        <form runat="server">
            <div class="container-fluid">
                <div class="row">
                    <div class="form-group col-xl-auto col-lg-auto">
                        <strong><i class="fa fa-filter"></i>Lọc kết quả &nbsp;&#187; </strong>
                    </div>
                    <div class="col-xl-auto col-lg-auto col-md-auto col-sm-auto form-group">
                        <input type="text" name="fromDate" placeholder="Từ ngày" class="datetime-picker form-control reset-table" />
                    </div>
                    <div class="col-xl-auto col-lg-auto col-md-auto col-sm-auto form-group">
                        <input type="text" name="toDate" placeholder="Đến ngày" class="datetime-picker form-control reset-table" />
                    </div>
                    <div class="col-xl-auto col-lg-auto col-md-12 col-sm-auto form-group">
                        <label class="mr-2 lable-text">Loại kho</label>
                        <asp:DropDownList ID="ddlInventoryType" onchange="GetListInventory($(this))" runat="server" ClientIDMode="Static" CssClass="select reset-table"></asp:DropDownList>
                    </div>
                    <div class="col-xl-auto col-lg-auto col-md-12 col-sm-auto form-group">
                        <label class="mr-2 lable-text">Kho</label>
                        <select class="form-control select" id="ddlInventory"></select>
                    </div>
                    <div class="form-group col-xl-auto col-lg-auto col-md-12 col-sm-auto form-group">
                        <div id="ViewDataFilter" class="form-group col-md-12 col-xl-auto pl-0" onclick="GetData()">
                            <div class="btn-viewdata">Xem dữ liệu</div>
                        </div>
                    </div>
                    <div class="col-md-12 col-xl-12 form-group">
                        <div class="sub-menu-boder-low"></div>
                    </div>
                </div>
            </div>
            <div class="container-fluid table-listing pt-2">
                <table id="tableListing" class="table table-bordered table-hover" style="float: left;">
                    <thead>
                        <tr>
                            <th class="text-center align-middle">STT</th>
                            <th class="text-center align-middle" style="min-width: 150px">Tên kho</th>
                            <th class="text-center align-middle">Tổng đơn order</br> (a)</th>
                            <th class="text-center align-middle">Tỉ lệ hoàn thành</br> (b = b/a)</th>
                            <th class="text-center align-middle">Tỉ lệ xuất hàng</br> (c = c/a)</th>
                            <th class="text-center align-middle">Tỉ lệ lỗi kho</br> (d = ( h + l + m)/a)</th>
                            <th class="text-center align-middle">Tỉ lệ lỗi Salon</br> (e = k/a)</th>
                            <th class="text-center align-middle">NCC tự giao</br> (f)</th>
                            <th class="text-center align-middle">SP dừng CC</br> (g)</th>
                            <th class="text-center align-middle">Kho thiếu hàng, trả sau</br> (h)</th>
                            <th class="text-center align-middle">Trả bằng SP khác</br> (i)</th>
                            <th class="text-center align-middle">Kho sai đơn hàng</br> (j)</th>
                            <th class="text-center align-middle">Lỗi salon xác nhận muộn</br> (k)</th>
                            <th class="text-center align-middle">Lỗi kho trả hàng muộn</br> (l)</th>
                            <th class="text-center align-middle">Lỗi kho không xuất hàng</br> (m)</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </form>
        <style>
            .menu-time {
                margin-right: 7px;
                font-size: 13px;
            }

                .menu-time .label {
                    padding: 0px 15px;
                    background: #000000;
                    color: #ffffff;
                    cursor: pointer;
                    border-radius: 15px;
                    line-height: 26px;
                    width: 94px;
                }

            .form-control {
                height: 31px !important;
            }

            .select2.select2-container.select2-container--default {
                width: 180px !important;
            }

            .lable-text {
                width: 46px;
            }

            #tableListing_info {
                float: left;
            }

            #tableListing_paginate {
                clear: both;
            }

            .dt-buttons.btn-group {
                float: left;
            }

            #tableListing_wrapper button.buttons-excel {
                background-color: black;
                width: 100px;
            }

                #tableListing_wrapper button.buttons-excel span {
                    color: white
                }

            #tableListing tbody tr td {
                text-align: center !important;
            }

                #tableListing tbody tr td:nth-child(2) {
                    text-align: left !important;
                }
        </style>
        <script type="text/javascript">
            var domain = '<% = Libraries.AppConstants.URL_API_INVENTORY %>';
            var table = null;
            var elms = {
                fromDate: 'input[name=fromDate]',
                toDate: 'input[name=toDate]',
                ddlInventoryType: '#ddlInventoryType',
                ddlInventory: '#ddlInventory'
            };
            jQuery(document).ready(function () {
                $('.select').select2();
                var date = new Date();
                var today = date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();
                $(elms.fromDate).val(today);
                $(elms.toDate).val(today);
                $('.reset-table').bind('change', function () {
                    if (table) {
                        $('#tableListing').DataTable().clear();
                        $('#tableListing').DataTable().destroy();
                    }
                });
            });
            // getdata
            function GetData() {
                var timeFrom = $(elms.fromDate).val();
                var timeTo = $(elms.toDate).val();
                if (!CheckExits(timeFrom) || !CheckExits(timeTo)) {
                    ShowMessage("Thông báo", " Chưa chọn ngày", 3, 7000);
                    return;
                }
                var inventoryTypeId = $(elms.ddlInventoryType).val();
                if (!CheckExits(inventoryTypeId) || inventoryTypeId === '0') {
                    ShowMessage("Thông báo", "Chưa chọn loại kho", 3, 7000);
                    return;
                }
                var inventoryId = $(elms.ddlInventory);
                var arrayInventoryId = [];
                if (inventoryId.val() !== '0') {
                    arrayInventoryId.push(inventoryId.val());
                }
                else {
                    $(inventoryId).find('option').each(function (index, value) {
                        let inventoryId = $(value).val();
                        if (inventoryId !== '0') {
                            arrayInventoryId.push($(value).val());
                        }
                    });
                }
                var strInventoryId = arrayInventoryId.toString();
                startLoading();
                if (table) {
                    $('#tableListing').DataTable().clear();
                    $('#tableListing').DataTable().destroy();
                }
                table = $('#tableListing').DataTable(
                    {
                        pagingType: "full_numbers",
                        language: {
                            lengthMenu: "Dòng / trang _MENU_",
                            paginate: {
                                previous: "<",
                                first: "Đầu",
                                last: "Cuối",
                                next: ">"
                            },
                            info: "Dòng _START_ tới _END_ của _TOTAL_ dòng",
                            search: "Nhập tìm kiếm:"
                        },
                        lengthMenu: [[10, 50, 200, 1000], [10, 50, 200, 1000]],
                        ordering: false,
                        serverSide: true,
                        //processing: true,
                        searching: false,
                        ajax: {
                            type: "POST",
                            async: true,
                            url: domain + '/api/order/get-process-report',
                            contentType: "application/json",
                            data: function (d) {
                                d.timeFrom = timeFrom;
                                d.timeTo = timeTo;
                                d.inventoryId = strInventoryId;
                                return JSON.stringify(d);
                            },
                            dataSrc: function (json) {
                                if (typeof json.data !== "undefined" || json.data !== null || json.data !== null) {
                                    $(json.data).each(function (index, value) {
                                        value.stt = index + 1;
                                        value.tong_order_hoan_thanh = round((value.tong_order_hoan_thanh / value.tong_order) * 100, 2) + " %";
                                        value.tong_don_xuat_du = round((value.tong_don_xuat_du / value.tong_order) * 100, 2) + " %";
                                        value.tong_don_loi_kho = round(((value.kho_thieu_hang + value.kho_tra_hang_muon + value.kho_khong_tra_hang) / value.tong_order) * 100, 2) + " %";
                                        value.tong_don_loi_salon = round((value.salon_xac_nhan_muon / value.tong_order) * 100, 2) + " %";
                                        return value;
                                    });
                                }
                                else {
                                    return false;
                                }
                                return json.data;
                            }
                        },
                        "columns": [
                            { "data": "stt", "autoWidth": true },
                            { "data": "ten_kho", "autoWidth": true },
                            { "data": "tong_order", "autoWidth": true },
                            { "data": "tong_order_hoan_thanh", "autoWidth": true },
                            { "data": "tong_don_xuat_du", "autoWidth": true },
                            { "data": "tong_don_loi_kho", "autoWidth": true },
                            { "data": "tong_don_loi_salon", "autoWidth": true },
                            { "data": "ncc_tu_giao", "autoWidth": true },
                            { "data": "sp_dung_cc", "autoWidth": true },
                            { "data": "kho_thieu_hang", "autoWidth": true },
                            { "data": "tra_bang_sp", "autoWidth": true },
                            { "data": "kho_xep_sai_dh", "autoWidth": true },
                            { "data": "salon_xac_nhan_muon", "autoWidth": true },
                            { "data": "kho_tra_hang_muon", "autoWidth": true },
                            { "data": "kho_khong_tra_hang", "autoWidth": true }
                            //{ "data": "orderSummaryErrorStaffSalon", "autoWidth": true }

                        ]
                    });
                // finishLoading
                finishLoading();
            }
            // get list inventory
            function GetListInventory(This) {
                let $value = This.val();
                if (!CheckParameter($value)) {
                    ShowMessage('', 'Vui lòng chọn loại kho', 3, 7000);
                }
                else {
                    $.ajax({
                        type: "POST",
                        url: "/GUI/BackEnd/IvInventory/IvProgressReport.aspx/GetListInventory",
                        data: '{inventoryType: ' + parseInt($value) + ' }',
                        crossDomain: true,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            $("#ddlInventory").html('').val('0');
                            $("#select2-ddlInventory-container").val('0').text('Chọn tất cả');
                            $("#ddlInventory").append($("<option />").val(0).text('Chọn tất cả'));
                            $.each(data.d, function () {
                                $("#ddlInventory").append($("<option />").val(this.Id).text(this.Name));
                            });
                        },
                        failure: function () {
                            ShowMessage('', 'Có lỗi xảy ra, vui lòng liên hệ nhà phát triển!', 4);
                        }
                    });
                }
            }
            // check parameter
            function CheckParameter(param) {
                if (param === undefined || param === null || param === '' || param === '0') {
                    return false;
                }
                return true;
            }
            // check exist
            function CheckExits(value) {
                if (typeof value === "undefined" || value === null || value === "") {
                    return false;
                }
                else {
                    return true;
                }
            }
        </script>
    </asp:Panel>
</asp:Content>
