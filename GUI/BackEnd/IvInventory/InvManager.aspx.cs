﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.IvInventory
{
    public partial class InvManager : System.Web.UI.Page
    {
        private Solution_30shineEntities db = new Solution_30shineEntities();
        CultureInfo culture = new CultureInfo("vi-VN");
        protected int a = 0;
        private bool Perm_ViewAllData = true;
        private bool Perm_Access = false;
        private bool Perm_Export = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            //SetPermission();
            if (!IsPostBack)
            {
                //Library.Function.bindSalon(new List<DropDownList> { Salon }, Perm_ViewAllData);
                BindInventoryType();
                BindDeparment();
                BindSalon();
                BindInventory();
                BindTinhThanh();
            }
        }

        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                //Perm_Export = permissionModel.CheckPermisionByAction("Perm_Export", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        protected void BindSalon()
        {
            using (var db = new Solution_30shineEntities())
            {
                var listSalon = new List<Tbl_Salon>();
                var SalonId = Convert.ToInt32(HttpContext.Current.Session["SalonId"]);
                var UserId = Convert.ToInt32(HttpContext.Current.Session["User_Id"]);
                if (Perm_ViewAllData)
                {
                    listSalon = db.Tbl_Salon.Where(w => w.IsDelete == 0 && w.Publish == true && w.IsSalonHoiQuan == false).OrderByDescending(o => o.CityId).ToList();
                    var item = new Tbl_Salon();
                    item.Name = "Chọn salon";
                    item.Id = 0;
                    listSalon.Insert(0, item);
                }
                else
                {
                    listSalon = (
                                        from p in db.PermissionSalonAreas.AsNoTracking()
                                        join s in db.Tbl_Salon.AsNoTracking() on p.SalonId equals s.Id
                                        where
                                        p.IsDelete == false && p.StaffId == UserId
                                        && s.Publish == true && s.IsDelete == 0 && s.IsSalonHoiQuan == false
                                        select s
                                   ).ToList();
                    if (listSalon.Count <= 0)
                    {
                        listSalon = db.Tbl_Salon.Where(w => w.Id == SalonId).ToList();
                    }
                }

                    Salon.DataTextField = "Name";
                    Salon.DataValueField = "Id";
                    Salon.DataSource = listSalon;
                    Salon.DataBind();

            }
        }
        protected void BindInventoryType()
        {
            var result = new HttpClient().GetAsync(Libraries.AppConstants.URL_API_INVENTORY + "/api/inventory/get-warehouse-type").Result;
            if (result.IsSuccessStatusCode)
            {
                var data = result.Content.ReadAsStringAsync().Result;
                if (data != null)
                {
                    var type = new JavaScriptSerializer().Deserialize<List<Tbl_Config>>(data);
                    var Key = 0;

                    InvType.DataTextField = "Label";
                    InvType.DataValueField = "Value";

                    ListItem item = new ListItem("Chọn kiểu kho", "0");
                    InvType.Items.Insert(0, item);

                    foreach (var v in type)
                    {
                        Key++;
                        item = new ListItem(v.Label, v.Value);
                        InvType.Items.Insert(Key, item);
                    }
                }
            }
        }
        protected void BindTinhThanh()
        {
            var data = db.TinhThanhs.ToList();
            if (data != null)
            {
                //var type = new JavaScriptSerializer().Deserialize<List<Tbl_Config>>(data);
                var Key = 0;

                DropDownList2.DataTextField = "TenTinhThanh";
                DropDownList2.DataValueField = "ID";

                ListItem item = new ListItem("Chọn tỉnh thành", "0");
                DropDownList2.Items.Insert(0, item);

                foreach (var v in data)
                {
                            Key++;
                            item = new ListItem(v.TenTinhThanh, v.ID.ToString());
                            DropDownList2.Items.Insert(Key, item);
                }
            }
        }
        protected void BindInventory()
        {
            //var result = new HttpClient().GetAsync(Libraries.AppConstants.URL_API_INVENTORY + "/api/inventory/get-warehouse-type").Result;
            //if (result.IsSuccessStatusCode)
            //{
            var data = db.IvInventories.Where(w => w.Type == 1).ToList();
                if (data != null)
                {
                    //var type = new JavaScriptSerializer().Deserialize<List<Tbl_Config>>(data);
                    var Key = 0;

                    DropDownList1.DataTextField = "Name";
                    DropDownList1.DataValueField = "Id";

                    ListItem item = new ListItem("Chọn kho xuất hàng", "0");
                    DropDownList1.Items.Insert(0, item);

                    foreach (var v in data)
                    {
                        switch (v.Type)
                        {
                            case 1:
                            //case 2:
                                Key++;
                                item = new ListItem(v.Name, v.Id.ToString());
                                DropDownList1.Items.Insert(Key, item);
                                break;
                            default:
                                break;
                        }
                        
                    }
                }
            //}
        }
        protected void BindDeparment()
        {
            var result = new HttpClient().GetAsync(Libraries.AppConstants.URL_API_INVENTORY + "/api/inventory/get-department").Result;
            if (result.IsSuccessStatusCode)
            {
                var data = result.Content.ReadAsStringAsync().Result;
                if (data != null)
                {
                    var type = new JavaScriptSerializer().Deserialize<List<Staff_Type>>(data);
                    var Key = 0;

                    Department.DataTextField = "Name";
                    Department.DataValueField = "Id";

                    ListItem item = new ListItem("Chọn bộ phận", "0");
                    Department.Items.Insert(0, item);

                    foreach (var v in type)
                    {
                        Key++;
                        item = new ListItem(v.Name, v.Id.ToString());
                        Department.Items.Insert(Key, item);
                    }
                }
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }

        }
    }
}