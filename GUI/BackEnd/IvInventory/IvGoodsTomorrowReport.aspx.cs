﻿using _30shine.Helpers;
using _30shine.Helpers.Http;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.IvInventory
{
    public partial class IvGoodsTomorrowReport : System.Web.UI.Page
    {
        public bool Perm_ShowSalon { get; private set; }
        private bool Perm_Access = false;
        private bool Perm_ViewAllData = false;
        public int staffId = 0;
        protected string Today = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-1));
        private int integer;
        public CusOutputQuantityGoodsTomorrow TotalReport = new CusOutputQuantityGoodsTomorrow();
        public List<CusOutputQuantityGoodsTomorrow> listData = new List<CusOutputQuantityGoodsTomorrow>();

        /// <summary>
        /// Set permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                staffId = int.TryParse(Session["User_Id"].ToString(), out var integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_ShowSalon = permissionModel.CheckPermisionByAction("Perm_ShowSalon", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        /// <summary>
        /// ExecuteByPermission
        /// </summary>
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        //constructor
        public IvGoodsTomorrowReport()
        {
            TotalReport.listInventory = new List<CusOutputInventorySalon>();
        }

        /// <summary>
        /// Page load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                Library.Function.bindProduct(new List<DropDownList>() { ddlProduct });
                BindInventoryType();
                BindDdlInventorySalon();
                TxtDateTimeFrom.Text = Today;
            }
        }

        /// <summary>
        ///  Bind inventoryType
        /// </summary>
        private void BindInventoryType()
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var listType = new List<Tbl_Config>();
                    listType = db.Tbl_Config.Where(r => r.IsDelete == 0 && r.Key == "iv_inventory_type" && (r.Value == "1" || r.Value == "4")).ToList();
                    var item = new Tbl_Config();
                    item.Label = "Chọn loại kho";
                    item.Value = "0";
                    listType.Insert(0, item);
                    var ddls = new List<DropDownList>() { ddlInventoryType };
                    for (var i = 0; i < ddls.Count; i++)
                    {
                        ddls[i].DataTextField = "Label";
                        ddls[i].DataValueField = "Value";
                        ddls[i].DataSource = listType;
                        ddls[i].DataBind();
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// bind inventory salon
        /// </summary>
        private void BindDdlInventorySalon()
        {
            try
            {
                var SalonId = Convert.ToInt32(HttpContext.Current.Session["SalonId"]);
                var UserId = Convert.ToInt32(HttpContext.Current.Session["User_Id"]);
                using (var db = new Solution_30shineEntities())
                {
                    var salonCurrent = db.Staffs.Where(r => r.Id == UserId).Select(r => new { Id = r.SalonId ?? 0 });

                    var salonArea = db.PermissionSalonAreas.Where(r => ((r.StaffId == UserId && UserId > 0) || UserId == 0) &&
                                                                       r.IsDelete == false &&
                                                                       r.IsActive == true)
                                                                       .Select(r => new { Id = r.SalonId ?? 0 });
                    var listInventorySalon = new List<MODEL.ENTITY.EDMX.IvInventory>();
                    if (Perm_ShowSalon)
                    {
                        listInventorySalon = (from a in db.IvInventories
                                              where a.IsDelete == false && a.Type == 2
                                              select a).ToList();
                    }
                    else
                    {
                        var listSalonId = salonCurrent.Union(salonArea).Where(r => r.Id != 0).Select(r => r);

                        listInventorySalon = (from a in db.IvInventories
                                              join b in listSalonId on a.SalonId equals b.Id
                                              where a.IsDelete == false && a.Type == 2
                                              select a).ToList();
                    }
                    var ddls = new List<DropDownList>() { ddlInventorySalon };
                    for (var i = 0; i < ddls.Count; i++)
                    {
                        ddls[i].DataTextField = "Name";
                        ddls[i].DataValueField = "Id";
                        ddls[i].DataSource = listInventorySalon;
                        ddls[i].DataBind();
                    }
                    ddlInventorySalon.Items.Insert(0, new ListItem("Chọn tất cả", "0"));
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="inventoryId"></param>
        /// <returns></returns>
        [WebMethod]
        public static object GetListSalonInventory(int inventoryId)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var listInventory = new List<MODEL.ENTITY.EDMX.IvInventory>();
                    List<int> lstType = new List<int>() { 3 };
                    var data = db.IvInventories.AsNoTracking().Where(a => a.IsDelete == false && !lstType.Contains(a.Type)).ToList();
                    // get type
                    var type = data.Where(a => a.Id == inventoryId)
                                   .Select(a => a.Type)
                                   .FirstOrDefault();
                    if (type == 1)
                    {
                        listInventory = data.Where(r => r.ParentId == inventoryId)
                                            .Select(a => new MODEL.ENTITY.EDMX.IvInventory
                                            {
                                                Id = a.Id,
                                                Name = a.Name
                                            }).ToList();
                    }
                    else
                    {
                        listInventory = data.Where(r => r.Type == 2)
                                            .Select(a => new MODEL.ENTITY.EDMX.IvInventory
                                            {
                                                Id = a.Id,
                                                Name = a.Name
                                            }).ToList();
                    }

                    return listInventory;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        ///  get data
        /// </summary>
        private void getData()
        {
            try
            {
                var input = new InputGoodsTomorrow();
                if (TxtDateTimeFrom.Text != "")
                {
                    input.timeFrom = TxtDateTimeFrom.Text;
                }
                else
                {
                    input.timeFrom = Today;
                }
                input.inventoryIdExport = Convert.ToInt32(HDFInventoryID.Value);
                input.invetoryIdOrder = HDFSalonInventoryID.Value;
                input.productId = Convert.ToInt32(HDFProductID.Value);
                var response = new Request().RunPostAsync(Libraries.AppConstants.URL_API_INVENTORY + "/api/order/get-list-goods-tomorrow", input).Result;
                if (response.IsSuccessStatusCode)
                {
                    var result = response.Content.ReadAsStringAsync().Result;
                    var data = new JavaScriptSerializer().Deserialize<List<OutputQuantityGoodsTomorrow>>(result).ToList();
                    var list = new List<CusOutputQuantityGoodsTomorrow>();
                    var item = new CusOutputQuantityGoodsTomorrow();
                    var itemInventory = new CusOutputInventorySalon();
                    var index = -1;
                    if (data.Count > 0)
                    {
                        foreach (var v in data)
                        {
                            index = list.FindIndex(f => f.inventoryId == v.inventoryId);
                            if (index == -1)
                            {
                                item = new CusOutputQuantityGoodsTomorrow();
                                item.inventoryId = v.inventoryId;
                                item.inventoryName = v.inventoryName;
                                item.listInventory = new List<CusOutputInventorySalon>();
                                itemInventory = new CusOutputInventorySalon();
                                itemInventory.productId = v.productId;
                                itemInventory.productName = v.productName;
                                itemInventory.quantityOrder = v.quantityOrder;
                                item.listInventory.Add(itemInventory);
                                list.Add(item);
                            }
                            else
                            {
                                item = list[index];
                                itemInventory = new CusOutputInventorySalon();
                                itemInventory.productId = v.productId;
                                itemInventory.productName = v.productName;
                                itemInventory.quantityOrder = v.quantityOrder;
                                item.listInventory.Add(itemInventory);
                                list[index] = item;
                            }
                            //handle sumary report
                            index = TotalReport.listInventory.FindIndex(f => f.productId == v.productId);
                            if (index == -1)
                            {
                                itemInventory = new CusOutputInventorySalon();
                                itemInventory.productId = v.productId;
                                itemInventory.productName = v.productName;
                                itemInventory.quantityOrder = v.quantityOrder;
                                TotalReport.listInventory.Add(itemInventory);
                            }
                            else
                            {
                                itemInventory = TotalReport.listInventory[index];
                                itemInventory.quantityOrder += v.quantityOrder;
                                TotalReport.listInventory[index] = itemInventory;
                            }
                        }
                    }
                    //
                    listData = list.ToList();
                }
            }
            catch (Exception ex)
            {
                new PushNotiErrorToSlack().PushDefault(ex.Message + ", StackTrace: " + ex.StackTrace, this.ToString() + ".getDataIvGoodsTomorrowReport", "");
            }
        }

        /// <summary>
        /// btn click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void _BtnClick(object sender, EventArgs e)
        {
            getData();
            //RemoveLoading();
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "GetSalonInventoryId();finishLoading();", true);
        }

        // input
        public class InputGoodsTomorrow
        {
            public string timeFrom { get; set; }
            public int inventoryIdExport { get; set; }
            public string invetoryIdOrder { get; set; }
            public int productId { get; set; }
        }

        //output quantity goods tomorrow
        public class OutputQuantityGoodsTomorrow
        {
            public int inventoryId { get; set; }
            public string inventoryName { get; set; }
            public int productId { get; set; }
            public string productName { get; set; }
            public int quantityOrder { get; set; }
        }

        // customize class ouput
        public class CusOutputQuantityGoodsTomorrow
        {
            public int inventoryId { get; set; }
            public string inventoryName { get; set; }
            public List<CusOutputInventorySalon> listInventory { get; set; }
        }

        // output inventory salon
        public class CusOutputInventorySalon
        {
            public int productId { get; set; }
            public string productName { get; set; }
            public int quantityOrder { get; set; }
        }
    }
}