﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.IvInventory
{
    public partial class IvProgressReport : System.Web.UI.Page
    {
        private bool Perm_Access = false;
        private bool Perm_ViewAllData = false;
        private static bool ViewAllData = false;
        public bool Perm_ShowSalon { get; private set; }

        /// <summary>
        /// Set permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                pageId = permissionModel.GetRegexPageIdSpecial().Replace(pageId, ".html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out var integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_ShowSalon = permissionModel.CheckPermisionByAction("Perm_ShowSalon", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        /// <summary>
        /// Page load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                BindInventoryType();
            }
        }

        /// <summary>
        ///  Bind inventoryType
        /// </summary>
        private void BindInventoryType()
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var listType = new List<Tbl_Config>();
                    listType = db.Tbl_Config.Where(r => r.IsDelete == 0 && r.Key == "iv_inventory_type" && (r.Value == "1" || r.Value == "4")).ToList();
                    var item = new Tbl_Config();
                    item.Label = "Chọn loại kho";
                    item.Value = "0";
                    listType.Insert(0, item);
                    var ddls = new List<DropDownList>() { ddlInventoryType };
                    for (var i = 0; i < ddls.Count; i++)
                    {
                        ddls[i].DataTextField = "Label";
                        ddls[i].DataValueField = "Value";
                        ddls[i].DataSource = listType;
                        ddls[i].DataBind();
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Get list inventory
        /// </summary>
        /// <param name="inventoryType"></param>
        /// <returns></returns>
        [WebMethod]
        public static object GetListInventory(int inventoryType)
        {
            try
            {
                //BindInventory ;
                using (var db = new Solution_30shineEntities())
                {
                    var inventoryTypeId = inventoryType;
                    var listInventory = new List<MODEL.ENTITY.EDMX.IvInventory>();
                    listInventory = db.IvInventories.Where(r => r.Type == inventoryTypeId && r.IsDelete == false).ToList();

                    return listInventory;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}