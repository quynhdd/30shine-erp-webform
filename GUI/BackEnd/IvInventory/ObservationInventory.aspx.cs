﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.IvInventory
{
    public partial class ObservationInventory : System.Web.UI.Page
    {
        private bool Perm_Access = false;
        private bool Perm_ViewAllData = false;
        public bool Perm_ShowSalon { get; private set; }
        public int staffId = 0;

        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                staffId = int.TryParse(Session["User_Id"].ToString(), out var integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_ShowSalon = permissionModel.CheckPermisionByAction("Perm_ShowSalon", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                BindDdlInventorySalon();
                BindDepartment();
            }
        }

        private void BindDepartment()
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    //chỉ lấy bộ phận stylist, skinner
                    var lstDepartment = db.Staff_Type.Where(w => w.IsDelete == 0 && w.Publish == true).OrderBy(o => o.Id).ToList();
                    var item = new Staff_Type();
                    item.Name = "Chọn tất cả bộ phận";
                    item.Id = 0;
                    lstDepartment.Insert(0, item);
                    ddlDepartment.DataTextField = "Name";
                    ddlDepartment.DataValueField = "Id";
                    ddlDepartment.DataSource = lstDepartment;
                    ddlDepartment.DataBind();
                }
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        private void BindDdlInventorySalon()
        {
            try
            {
                var SalonId = Convert.ToInt32(HttpContext.Current.Session["SalonId"]);
                var UserId = Convert.ToInt32(HttpContext.Current.Session["User_Id"]);
                using (var db = new Solution_30shineEntities())
                {
                    var salonCurrent = db.Staffs.Where(r => r.Id == UserId).Select(r => new { Id = r.SalonId ?? 0 });

                    var salonArea = db.PermissionSalonAreas.Where(r => ((r.StaffId == UserId && UserId > 0) || UserId == 0) &&
                                                                       r.IsDelete == false &&
                                                                       r.IsActive == true)
                                                                       .Select(r => new { Id = r.SalonId ?? 0 });
                    var listInventorySalon = new List<MODEL.ENTITY.EDMX.IvInventory>();
                    if (Perm_ShowSalon)
                    {
                        listInventorySalon = (from a in db.IvInventories
                                              where a.IsDelete == false && a.Type == 2
                                              select a).ToList();
                    }
                    else
                    {
                        var listSalonId = salonCurrent.Union(salonArea).Where(r => r.Id != 0).Select(r => r);

                        listInventorySalon = (from a in db.IvInventories
                                              join b in listSalonId on a.SalonId equals b.Id
                                              where a.IsDelete == false && a.Type == 2
                                              select a).ToList();
                    }
                    var ddls = new List<DropDownList>() { ddlInventorySalon };
                    for (var i = 0; i < ddls.Count; i++)
                    {
                        ddls[i].DataTextField = "Name";
                        ddls[i].DataValueField = "Id";
                        ddls[i].DataSource = listInventorySalon;
                        ddls[i].DataBind();
                    }
                    //ddlInventorySalon.Items.Insert(0, new ListItem("Chọn tất cả", "0"));
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}