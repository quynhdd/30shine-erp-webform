﻿<%@ Page Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="IvProductQuantifyUpdateList.aspx.cs" Inherits="_30shine.GUI.BackEnd.IvInventory.IvProductQuantifyUpdateList" %>


<asp:Content ContentPlaceHolderID="head" runat="server">
    <title>Cấu hình định lượng sản phẩm</title>
    <link href="/Assets/css/bootstrap/bootstrap-toggle.min.css" rel="stylesheet" />
    <script src="/Assets/js/bootstrap/bootstrap-toggle.min.js"></script>
    <link href="/Assets/css/bootstrap/bootstrap-toggle.min.css" rel="stylesheet" />
    <script src="/Assets/js/bootstrap/bootstrap-toggle.min.js"></script>

    <script src="/Assets/libs/magicsuggest-master/magicsuggest.js"></script>
    <link href="/Assets/libs/magicsuggest-master/magicsuggest.css" rel="stylesheet" />
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <!-- Include multi.js -->
    <link rel="stylesheet" type="text/css" href="/Assets/js/Two-Panel-Multi-Selection/multi.css" />
    <script src="/Assets/js/Two-Panel-Multi-Selection/multi.js"></script>
    <script src="/Assets/js/Common/AjaxCallCommon.js"></script>
    <link rel="stylesheet" href="/Assets/izitoast/css/iziToast.min.css" />
    <script src="/Assets/izitoast/js/iziToast.min.js" type="text/javascript"></script>
    <script src="/Assets/js/common.js"></script>

    <style>
        .customer-add .table-add .title-head strong {
            font-family: sans-serif;
            font-weight: 700;
            font-size: 17px;
        }

        .sub-menu ul.ul-sub-menu li a {
            padding-left: 30px;
        }

        .sub-menu ul.ul-sub-menu li:hover a {
            color: green !important;
        }

            .sub-menu ul.ul-sub-menu li:hover a i {
                color: green !important;
            }

        .fe-service .tr-product .table-listing-product tbody input.product-quantify {
            border-bottom: 1px solid #ddd !important;
            width: 100px !important;
        }

        .fe-service .tr-product .table-listing-product tbody input.product-volume {
            border-bottom: 1px solid #ddd !important;
            width: 100px !important;
        }

        .fe-service .tr-product .listing-product {
            display: block !important;
        }

        .suggestion-wrap {
            width: 420px;
            float: left;
        }

        .ms-sel-ctn input[type='text'] {
            height: auto !important;
            line-height: 24px !important;
            border: none !important;
        }

        .ms-ctn .ms-sel-item {
            line-height: 22px;
            color: #000000;
            font-size: 13px;
        }

        .ms-helper {
            display: none !important;
        }

        .input-quantity-wrap {
            float: left;
            margin-left: 10px;
        }

        .input-quantity {
            width: 90px;
            line-height: 36px;
            padding: 0 10px;
            border: 1px solid #ddd;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
        }

        .btn-add {
            float: left;
            background: #ddd;
            padding: 9px 15px;
            margin-left: 10px;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            cursor: pointer;
        }

        .view-ddl {
            height: 36px !important;
            line-height: 36px !important;
            width: 100% !important;
            float: none !important;
        }

        .table-wp table tbody tr td label {
            line-height: 36px;
            float: left;
            margin-right: 5px;
        }

        .custom-salon .active {
            background-color: black;
            color: white;
        }

        /*.container1 {
            box-sizing: border-box;
            margin: 150px auto;
            max-width: 500px;
            padding: 0 20px;
            width: 100%;
        }*/
    </style>
</asp:Content>
<asp:Content ID="ServiceAdd" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Cấu hình định lượng nhiều salon &#187;</li>
                        <li><a style="text-decoration: none !important" href="/admin/cau-hinh-dinh-luong-v1/danh-sach.html"><i class="fa fa-list-ul"></i>&nbsp Danh sách định lượng</a></li>
                        <li><a style="text-decoration: none !important" href="/admin/cau-hinh-dinh-luong-v1/them-moi.html"><i class="fa fa-list-ul"></i>&nbsp Thêm mới định lượng</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="wp customer-add admin-product-add fe-service">
            <%-- Add --%>
            <div class="wp960 content-wp">
                <!-- System Message -->
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <!-- END System Message -->
                <div>
                    <table class="table-add admin-product-table-add fe-service-table-add">
                        <tbody>
                            <tr class="title-head">
                                <td><strong>CẤU HÌNH ĐỊNH LƯỢNG SẢN PHẨM DỊCH VỤ CỦA NHIỀU SALON</strong></td>
                            </tr>
                            <tr class="tr-margin" style="height: 20px;"></tr>
                            <tr>
                                <td>
                                    <div>
                                        <table>
                                            <tr>
                                                <td>
                                                    <label style="float: left; line-height: 36px; font-family: Roboto Condensed Bold;">Kho Salon&#187;</label></td>
                                                <td>
                                                    <div class="container1" style="padding-left: 0px; width: 400px;">
                                                        <select id="selectMultiple" multiple="multiple">
                                                            <% foreach (var item in listInventorySalon)
                                                                { %>
                                                            <option data-label="<%= item.Name %>" value="<%= item.Id %>"><%= item.Name %></option>
                                                            <% } %>
                                                        </select>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="department-select" style="padding-left: 10px;">
                                                        <label style="float: left; line-height: 36px; font-family: Roboto Condensed Bold;">Bộ phận&#187;</label>
                                                        <div class="field-wp" style="float: left; width: auto; margin-right: 10px;">
                                                            <asp:DropDownList ID="ddlDepartment" CssClass="form-control select" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="service-select" style="padding-left: 0px;">
                                                        <label style="float: left; line-height: 36px; font-family: Roboto Condensed Bold;">Dịch vụ&#187;</label>
                                                        <div class="field-wp" style="float: left; width: auto; margin-right: 10px;">
                                                            <asp:DropDownList ID="ddlService" CssClass="form-control select" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <input type="button" class="btn-add" onclick="viewData()" style="padding: 8px 15px" value="Xem dữ liệu" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>

                                </td>
                            </tr>
                            <%--  <tr>                                
                                <td class="col-xs-9 right">
                                    <hr />
                                </td>
                            </tr>--%>

                            <tr>
                                <td></td>
                            </tr>
                            <tr class="tr-field-ahalf tr-product">
                                <%--<td class="col-xs-2 left"><span></span></td>--%>
                                <td class="col-xs-9 right">

                                    <div class="listing-product item-product" id="ListingProductWp" runat="server" clientidmode="Static">
                                        <table class="table table-listing-product table-item-product" id="table-item-product">
                                            <thead>
                                                <tr>
                                                    <th>STT</th>
                                                    <th class="maSPcol">Mã sản phẩm</th>
                                                    <th>Tên sản phẩm</th>
                                                    <th style="width: 200px">Số lần sử dụng tối thiểu</th>
                                                    <th style="width: 200px">Trọng lượng</th>
                                                    <th style="width: 200px">Định lượng</th>
                                                    <th style="width: auto"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater ID="RptConfigInventory" runat="server">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td style="text-align: center;" class="td-product-index"><%# Container.ItemIndex + 1 %></td>
                                                            <td class="td-product-code maSPcol" style="width: 120px; text-align: center;" data-id="<%# Eval("ProductId") %>"><%# Eval("Code") %></td>
                                                            <td style="text-align: center;" class="td-product-name"><%# Eval("Name") %></td>
                                                            <td style="text-align: center; align-items: center;" class="td-total-number-service">
                                                                <input style="text-align: center;" type="text" class="product-quantify" value="<%# Eval("TotalNumberService") %>" />
                                                            </td>
                                                            <td style="text-align: center;" class="td-product-volume">
                                                                <input style="text-align: center;" type="text" class="product-volume" value="<%# Eval("Volume") %>" />
                                                            </td>

                                                            <td style="text-align: center;" class="td-product-quantify">
                                                                <input style="text-align: center;" type="text" class="product-quantify" value="<%# Eval("Quantify") %>" />
                                                            </td>
                                                            <td class="map-edit">
                                                                <div class="edit-wp">
                                                                    <a class="elm del-btn" onclick="RemoveItem($(this).parent().parent().parent(),'<%# Eval("Name") %>','product')"
                                                                        href="javascript://" title="Xóa"></a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="suggestion-wrap">
                                        <input id="ms" class="form-control" type="text" />
                                    </div>
                                    <div class="btn-add" onclick="addProduct()" style="padding: 8px 15px">Thêm sản phẩm</div>
                                </td>
                            </tr>

                            <tr class="tr-send">
                                <%--<td class="col-xs-2 left"></td>--%>
                                <td class="col-xs-9 right no-border">
                                    <span class="field-wp">
                                        <asp:Panel ID="BtnSend" CssClass="btn-send" runat="server" ClientIDMode="Static">Hoàn Tất</asp:Panel>
                                        <asp:Button ID="BtnFakeSend" OnClick="FuncComplete" runat="server" Text="Hoàn tất" ClientIDMode="Static" Style="display: none;"></asp:Button>
                                    </span>
                                </td>
                            </tr>
                            <asp:HiddenField runat="server" ID="UrlKeyField" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="ImgCoverField" ClientIDMode="Static" />
                        </tbody>
                    </table>


                    <!-- input hidden -->
                    <asp:HiddenField ID="HDF_ListInventoryId" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_ProductIds" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_ServiceIds" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_UserImages" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_UserImagesDelete" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="Recipient" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HairMassage" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="Cosmetic" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_SalonId" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_lgId" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_BillId" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_FreeService" ClientIDMode="Static" runat="server" />
                    <asp:HiddenField ID="HDF_Rating" ClientIDMode="Static" runat="server" />
                    <!-- END input hidden -->
                </div>
            </div>

            <%-- end Add --%>
        </div>

        <script src="/assets/js/jquery.mCustomScrollbar.js"></script>
        <link href="/assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" />
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 200px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 35px !important;
                padding-top: 1px !important;
            }
        </style>
        <script type="text/javascript">

            var URL_INVENTORY = "<%= Libraries.AppConstants.URL_API_INVENTORY %>";
            var itemProduct;
            var quantity;
            var index;
            var ms;
            // Btn Send
            $("#BtnSend").bind("click", function () {
                let result = true;
                var Code = "<%= Id %>";


                //let serviceId = $("#ddlService :selected").val();
                //let inventoryId = $('#ddlInventorySalon :selected').val();
                //let departmentId = $('#ddlDepartment :selected').val();
                //let totalNumberService = $('#txtTotalNumberService').val();
                var selected = $('div.selected-wrapper a');

                if (selected == null || selected.length == 0) {
                    ShowMessage("Chú ý", 'Yêu cầu chọn salon.', 3, 7000);
                    return;
                }
                var ddlDepartment = $('#ddlDepartment').val();
                if (ddlDepartment == "" || ddlDepartment == "0") {
                    ShowMessage("Chú ý", "Yêu cầu chọn Bộ phận!", 3);
                    return;
                }

                var ddlService = $('#ddlService').val();
                if (ddlService == "" || ddlService == "0") {
                    ShowMessage("Chú ý", "Yêu cầu chọn Dịch vụ!", 3);
                    return;
                }
                var listIv = "";
                selected.each(function () {
                    var THIS = $(this);
                    var ivId = THIS.attr("value");
                    if (ivId != "undefined" && ivId != "") {
                        listIv += ivId + ",";
                    }
                });
                if (listIv.length > 0) {
                    listIv = listIv.substr(0, listIv.length - 1);
                }

                let index = 0;
                if ($("table.table-item-product tbody tr").length == 0) {
                    ShowMessage("Chú ý", "Yêu cầu thêm sản phẩm!", 3);
                    return;
                }
                

                var listId = ",";
                var listNameDuplicate = "";

                $("table.table-item-product tbody tr").each(function () {
                    var THIS = $(this);
                    let quantify = THIS.find("input.product-quantify").val();
                    let volume = THIS.find("input.product-volume").val();
                    let totalNumberService = THIS.find("input.total-number-service").val();
                    let name = THIS.find("td.td-product-name").attr("data-name");
                    if (totalNumberService == '' || totalNumberService == '0') {
                        ShowMessage("Chú ý", 'Số lần sử dụng tối thiểu phải lớn hơn 0', 3, 7000);
                        result = false;
                    }
                    else if (volume == '') {
                        ShowMessage("Chú ý", 'Nhập trọng lượng cho sản phẩm.', 3, 7000);
                        result = false;
                    }
                    else if (volume == '0') {
                        ShowMessage("Chú ý", 'Trọng lượng phải lớn hơn 0.', 3, 7000);
                        result = false;
                    }
                    else if (quantify == '') {
                        ShowMessage("Chú ý", 'Nhập định lượng cho sản phẩm.', 3, 7000);
                        result = false;
                    }
                    else if (quantify == '0') {
                        ShowMessage("Chú ý", 'Định lượng phải lớn hơn 0.', 3, 7000);
                        result = false;
                    }

                    if (!result) {
                        return;
                    }
                    //
                    var Id = THIS.find("td.td-product-code").attr("data-id");
                    if (listId.includes("," + Id + ",")) {
                        listNameDuplicate += name + ", ";
                    } else {
                        listId += Id + ",";
                    }
                });
                if (listNameDuplicate.length > 0) {
                    ShowMessage("Chú ý", "Các sản phẩm [" + listNameDuplicate.substr(0, listNameDuplicate.length - 2) + "] đang bị lặp, cần phải xóa!", 3);
                    return;
                }
                startLoading();
                debugger
                if (result) {
                    var Ids = [];
                    var prd = {};
                    $("table.table-item-product tbody tr").each(function () {
                        prd = {};
                        var THIS = $(this);
                        var Id = THIS.find("td.td-product-code").attr("data-id"),
                            Quantify = THIS.find("input.product-quantify").val(),
                            Volume = THIS.find("input.product-volume").val(),
                            TotalNumberService = THIS.find("input.total-number-service").val(),
                            // check value
                            Id = Id.toString().trim() != "" ? parseInt(Id) : 0;
                        prd.ProductId = Id;
                        prd.Quantify = Quantify;
                        prd.Volume = Volume;
                        prd.TotalNumberService = TotalNumberService;
                        Ids.push(prd);
                    });
                    Ids = JSON.stringify(Ids);
                    $("#HDF_ProductIds").val(Ids);
                    $("#HDF_ListInventoryId").val(listIv);

                    console.log($("#HDF_ProductIds").val());
                    $("#BtnFakeSend").click();

                }
                finishLoading();
            });

            jQuery(document).ready(function () {
                var qs = getQueryStrings();
                ShowMessage("Thông báo", qs["msg_message"], parseInt(qs["msg_status"]));

                $('#selectMultiple').multi({
                    search_placeholder: 'Search Salon...',
                });

                // bind data to dropdownlist
                ms = $('#ms').magicSuggest({
                    maxSelection: 1,
                    data: <%=ListProduct %>,
                    valueField: 'Id',
                    displayField: 'Name',
                });

                $(ms).on('selectionchange', function (e, m) {
                    itemProduct = null;
                    var listProduct = this.getSelection();
                    console.log(listProduct);
                    var listData = ms.getData();
                    if (listProduct.length > 0) {
                        for (var i = 0; i < listData.length; i++) {
                            if (listProduct[0].Id == listData[i].Id) {
                                itemProduct = listProduct[0];
                                $("#inputVolume").focus();
                                break;
                            }
                            else {
                                continue;
                            }
                        }
                        if (itemProduct == null) {
                            ShowMessage("Chú ý", "Lễ tân chú ý chọn đúng sản phẩm trong danh sách vật tư dịch vụ.", 3, 7000);
                            ms.clear();
                            $(".ms-sel-ctn input[type='text']").focus();
                        }
                    }
                });
            });

            //get product to form body
            function getDataProduct() {
                var Ids = [];
                var prd = {};
                $("table.table-item-product tbody tr").each(function () {
                    prd = {};
                    var THIS = $(this);
                    var Id = THIS.find("td.td-product-code").attr("data-id"),
                        Quantify = THIS.find("input.product-quantify").val(),
                        Volume = THIS.find("input.product-volume").val(),
                        TotalNumberService = THIS.find("input.total-number-service").val(),
                        // check value
                        Id = Id.toString().trim() != "" ? parseInt(Id) : 0;
                    prd.ProductId = Id;
                    prd.Quantify = Quantify;
                    prd.Volume = Volume;
                    prd.TotalNumberService = TotalNumberService;
                    Ids.push(prd);
                });
                Ids = JSON.stringify(Ids);
                $("#HDF_ProductIds").val(Ids);
            }

            function addProduct() {
                var result = false;
                var error = '';
                // check trùng sản phẩm
                //quantify = $("#inputQuantify").val();
                //volume = $("#inputVolume").val();
                if (itemProduct == null) {
                    ShowMessage("Chú ý", 'Yêu cầu chọn sản phẩm.', 3, 7000);
                    result = true;
                }
                //else if (quantify == '') {
                //    ShowMessage("Chú ý", 'Nhập định lượng cho sản phẩm.', 3, 7000);
                //    result = true;
                //}
                //else if (quantify == '0') {
                //    ShowMessage("Chú ý", 'Định lương phải lớn hơn 0.', 3, 7000);
                //    result = true;
                //}
                //else if (volume == '') {
                //    ShowMessage("Chú ý", 'Nhập trọng lượng cho sản phẩm.', 3, 7000);
                //    result = true;
                //}
                //else if (volume == '0') {
                //    ShowMessage("Chú ý", 'Trọng lượng phải lớn hơn 0.', 3, 7000);
                //    result = true;
                //}
                else if (itemProduct != null) {
                    $("table.table-item-product tbody tr").each(function () {
                        var THIS = $(this);
                        var productId = THIS.find("td.td-product-code").attr("data-id");
                        if (productId != "undefined" && itemProduct.Id == productId) {
                            ShowMessage("Chú ý", 'Sản phẩm đã tồn tại trong danh sách.', 3, 7000);
                            //$("#inputQuantify").val('');
                            //$("#inputVolume").val('');
                            result = true;
                        }
                    });
                }
                if (!result) {
                    index = $("#table-item-product tbody tr").length + 1;
                    if (itemProduct != null) {
                        if (typeof itemProduct.Code != 'undefined') {
                            var tr = '<tr>' +
                                '<td style="text-align: center;">' + index + '</td>' +
                                '<td style="text-align: center;" class="td-product-code maSPcol" data-id="' + itemProduct.Id + '">' + itemProduct.Code + '</td>' +
                                '<td class="td-product-name" style="text-align:center;" data-name="' + itemProduct.Name + '">' + itemProduct.Name + '</td>' +
                                '<td style="text-align:center;" class="td-total-number-service">' +
                                '<input class="total-number-service" value="' + '" type="text" onkeypress="return ValidateKeypress(/\\d/,event);" style="margin: 0 auto; float: none; text-align: center; width: 80px;" maxlength="3" placeholder="Số lần"/>' +
                                '</td>' +
                                '<td style="text-align: center;" class="td-product-volume">' +
                                '<input class="product-volume" value="' + '" type="text" onkeypress="return ValidateKeypress(/\\d/,event);" style="margin: 0 auto; float: none; text-align: center; width: 80px;" maxlength="10" placeholder="Trọng lượng"/>' +
                                '</td>' +
                                '<td style="text-align: center;" class="td-product-quantify">' +
                                '<input class="product-quantify" value="' + '" type="text" onkeypress="return ValidateKeypress(/\\d/,event);" style="margin: 0 auto; float: none; text-align: center; width: 80px;" maxlength="10" placeholder="Định lượng"/>' +
                                '</td>' +
                                '<td class=" map-edit">' +
                                '<div class="edit-wp">' +
                                '<a class="elm del-btn" onclick="RemoveItem($(this).parent().parent().parent(),\'' + itemProduct.Name + '\',\'product\')" href="javascript://" title="Xóa"></a>' +
                                '</div>' +
                                '</td>' +
                                '</tr>';
                            $("#table-item-product tbody").append($(tr));
                            $("#ListingProductWp").show();
                            getDataProduct();
                            //resetInputForm();
                        }
                        else {
                            ShowMessage("Chú ý", "Lễ tân chú ý chọn đúng sản phẩm trong danh sách vật tư dịch vụ.", 3, 7000);
                            ms.clear();
                            $(".ms-sel-ctn input[type='text']").focus();
                        }

                    }
                }
            }

            function viewData() {
                let panelStaff = $('#pills-staff').hasClass('active');
                let panelSalon = $('#pills-salon').hasClass('active');
                let panelAll = $('#pills-all').hasClass('active');

                //
                debugger;

                var selected = $('div.selected-wrapper a');

                if (selected == null || selected.length == 0) {
                    ShowMessage("Chú ý", 'Yêu cầu chọn salon.', 3, 7000);
                    return;
                }
                var ddlDepartment = $('#ddlDepartment').val();
                if (ddlDepartment == "" || ddlDepartment == "0") {
                    ShowMessage("Chú ý", "Yêu cầu chọn Bộ phận!", 3);
                    return;
                }

                var ddlService = $('#ddlService').val();
                if (ddlService == "" || ddlService == "0") {
                    ShowMessage("Chú ý", "Yêu cầu chọn Dịch vụ!", 3);
                    return;
                }
                var listIv = "";
                selected.each(function () {
                    var THIS = $(this);
                    var ivId = THIS.attr("value");
                    if (ivId != "undefined" && ivId != "") {
                        listIv += ivId + ",";
                    }
                });
                if (listIv.length > 0) {
                    listIv = listIv.substr(0, listIv.length - 1);
                }
                //
                startLoading();

                //
                //let inventoryId = ~~$('.tab-pane.show.active').find('select[data-name=inventoryId]').val();
                //let departmentId = ~~$('.tab-pane.show.active').find('select[data-name=departmentId]').val();
                //let inventoryType = -1;

                //if (panelStaff) {
                //    inventoryId = inventoryId == 0 ? -1 : inventoryId;
                //    departmentId = departmentId == 0 ? -1 : departmentId;
                //}
                //else if (panelSalon) {
                //    //inventoryType = config.inventory.type.salon;
                //    departmentId = 0;
                //    inventoryId = inventoryId == 0 ? -1 : inventoryId;
                //}
                //else if (panelAll) {
                //    departmentId = 0;
                //    inventoryId = 0;
                //}

                let api = "/api/product-quantify/getByListIv" + "?listInventoryId=" + listIv + "&departmentId=" + ddlDepartment + "&serviceId=" + ddlService;
                let response = AjaxGetApi(URL_INVENTORY + api);
                let objTr = "";
                debugger;
                $("#table-item-product tbody").html("");
                if (typeof response === "object") {
                    //$('.table-listing').empty();
                    //var tableTemp = $('.table-temp table').attr('id', 'tableListing').clone();
                    //$('.table-listing').append(tableTemp);
                    $.each(response,
                        function (index, value) {
                            debugger;
                            objTr += '<tr>' +
                                '<td style="text-align: center;">' + (index + 1) + '</td>' +
                                '<td style="text-align: center;" class="td-product-code maSPcol" data-id="' + value.productId + '">' + value.code + '</td>' +
                                '<td class="td-product-name" style="text-align:center;" data-name="' + value.name + '">' + value.name + '</td>' +
                                '<td style="text-align:center;" class="td-total-number-service">' +
                                '<input class="total-number-service" value="' + value.totalNumberService + '" type="text" onkeypress="return ValidateKeypress(/\\d/,event);" style="margin: 0 auto; float: none; text-align: center; width: 80px;" maxlength="3" placeholder="Trọng lượng"/>' +
                                '</td>' +
                                '<td style="text-align: center;" class="td-product-volume">' +
                                '<input class="product-volume"  value="' + value.volume + '" type="text" onkeypress="return ValidateKeypress(/\\d/,event);" style="margin: 0 auto; float: none; text-align: center; width: 80px;" maxlength="10" placeholder="Trọng lượng"/>' +
                                '</td>' +
                                '<td style="text-align: center;" class="td-product-quantify">' +
                                '<input class="product-quantify"  value="' + value.quantify + '" type="text" onkeypress="return ValidateKeypress(/\\d/,event);" style="margin: 0 auto; float: none; text-align: center; width: 80px;" maxlength="10" placeholder="Định lượng"/>' +
                                '</td>' +
                                '<td class=" map-edit">' +
                                '<div class="edit-wp">' +
                                '<a class="elm del-btn" onclick="RemoveItem($(this).parent().parent().parent(),\'' + value.name + '\',\'product\')" href="javascript://" title="Xóa"></a>' +
                                '</div>' +
                                '</td>' +
                                '</tr>';
                        });
                    $("#table-item-product tbody").append(objTr);
                    //ConfigTableListing();
                }
                finishLoading();
            };



            // Update order item
            function UpdateItemOrder(dom) {
                var index = 1;
                dom.find("tr").each(function () {
                    $(this).find("td.td-product-index").text(index);
                    index++;
                });
            }

            // Remove item đã được chọn
            function RemoveItem(THIS, name, itemName) {
                // Confirm
                $(".confirm-yn").openEBPopup();

                $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn hủy [ " + name + " ] ?");
                $("#EBPopup .yn-yes").bind("click", function () {
                    var Code = THIS.find(".td-staff-code").attr("data-code");
                    $("#quick-" + itemName).find("input[data-code='" + Code + "']").removeAttr("checked");
                    THIS.remove();
                    getDataProduct();
                    UpdateItemDisplay(itemName);
                    autoCloseEBPopup(0);
                });
                $("#EBPopup .yn-no").bind("click", function () {
                    autoCloseEBPopup(0);
                });
            }

            //reset input
            function resetInputForm() {
                ms.clear();
                $("#inputQuantify").val('');
                $("#inputVolume").val('');
                $(ms).focus();
                $(".ms-sel-ctn input[type='text']").focus();
            }

            // Update table display
            function UpdateItemDisplay(itemName) {
                var dom = $(".table-item-" + itemName);
                var len = dom.find("tbody tr").length;
                if (len == 0) {
                    dom.parent().hide();
                } else {
                    UpdateItemOrder(dom.find("tbody"));
                }
            }

            // Validate keypress
            function ValidateKeypress(numcheck, e) {
                var keynum, keychar, numcheck;
                if (window.event) {
                    keynum = e.keyCode;
                }
                else if (e.which) {
                    keynum = e.which;
                }
                if (keynum == 8 || keynum == 127 || keynum == null || keynum == 9 || keynum == 0 || keynum == 13 || keynum == 46) return true;
                keychar = String.fromCharCode(keynum);
                var result = numcheck.test(keychar);
                return result;
            }
            function activeStaff() {
                preventDefault();
                var list = $('.custom-salon button');
                $.each(list, function (index, value) {
                    $(value).removeClass('active');
                });
                $('#ddlDepartment').removeAttr('disabled');
                $('#ddlDepartment').val("0");

                $('#ddlInventorySalon').removeAttr('disabled');
                $('#ddlInventorySalon').val("0");
                $('select').select2();
                $('.custom-salon button.staff').addClass('active');
            }
            function activeSalon() {
                preventDefault();
                var list = $('.custom-salon button');
                $.each(list, function (index, value) {
                    $(value).removeClass('active');
                });
                $('#ddlDepartment').attr('disabled', 'disabled');
                $('#ddlDepartment').val("0");

                $('#ddlInventorySalon').removeAttr('disabled');
                $('#ddlInventorySalon').val("0");

                $('select').select2();
                $('.custom-salon button.salon').addClass('active');
            }
            function activeAll() {
                preventDefault();
                var list = $('.custom-salon button');
                $.each(list, function (index, value) {
                    $(value).removeClass('active');
                });
                $('#ddlDepartment').attr('disabled', 'disabled');
                $('#ddlDepartment').val("0");

                $('#ddlInventorySalon').attr('disabled', 'disabled');
                $('#ddlInventorySalon').val("0");
                $('select').select2();
                $('.custom-salon button.all').addClass('active');
            }
        </script>
        <!-- Page loading -->
        <div class="page-loading">
            <p>Vui lòng đợi trong giây lát...</p>
        </div>
    </asp:Panel>
</asp:Content>


