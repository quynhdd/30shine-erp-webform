﻿    <%@ Page Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="IvStatisticsProductSold.aspx.cs" Inherits="_30shine.GUI.BackEnd.IvInventory.IvStatisticsProductSold" %>


<asp:Content ContentPlaceHolderID="head" runat="server">
    <title>Gợi ý order nhà cung cấp</title>
    <link href="/Assets/css/bootstrap/bootstrap-toggle.min.css" rel="stylesheet" />
    <script src="/Assets/js/bootstrap/bootstrap-toggle.min.js"></script>
    <link href="/Assets/css/bootstrap/bootstrap-toggle.min.css" rel="stylesheet" />
    <script src="/Assets/js/bootstrap/bootstrap-toggle.min.js"></script>

    <script src="/Assets/libs/magicsuggest-master/magicsuggest.js"></script>
    <link href="/Assets/libs/magicsuggest-master/magicsuggest.css" rel="stylesheet" />
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <!-- Include multi.js -->
    <link rel="stylesheet" type="text/css" href="/Assets/js/Two-Panel-Multi-Selection/multi.css" />
    <script src="/Assets/js/Two-Panel-Multi-Selection/multi.js"></script>
    <script src="/Assets/js/Common/AjaxCallCommon.js"></script>
    <link rel="stylesheet" href="/Assets/izitoast/css/iziToast.min.css" />
    <script src="/Assets/izitoast/js/iziToast.min.js" type="text/javascript"></script>
    <script src="/Assets/js/common.js"></script>
    <link rel="stylesheet" href="/Assets/css/style.css" />

    <style>
        .customer-add .table-add .title-head strong {
            font-family: sans-serif;
            font-weight: 700;
            font-size: 17px;
        }

        .sub-menu ul.ul-sub-menu li a {
            padding-left: 30px;
        }

        .sub-menu ul.ul-sub-menu li:hover a {
            color: green !important;
        }

            .sub-menu ul.ul-sub-menu li:hover a i {
                color: green !important;
            }

        .fe-service .tr-product .table-listing-product tbody input.product-quantify {
            border-bottom: 1px solid #ddd !important;
            width: 100px !important;
        }

        .fe-service .tr-product .table-listing-product tbody input.product-volume {
            border-bottom: 1px solid #ddd !important;
            width: 100px !important;
        }

        .fe-service .tr-product .listing-product {
            display: block !important;
        }

        .suggestion-wrap {
            width: 420px;
            float: left;
        }

        .ms-sel-ctn input[type='text'] {
            height: auto !important;
            line-height: 24px !important;
            border: none !important;
        }

        .ms-ctn .ms-sel-item {
            line-height: 22px;
            color: #000000;
            font-size: 13px;
        }

        .ms-helper {
            display: none !important;
        }

        .input-quantity-wrap {
            float: left;
            margin-left: 10px;
        }

        .input-quantity {
            width: 90px;
            line-height: 36px;
            padding: 0 10px;
            border: 1px solid #ddd;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
        }

        .btn-add {
            float: left;
            background: #ddd;
            padding: 9px 15px;
            margin-left: 10px;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            cursor: pointer;
        }

        .view-ddl {
            height: 36px !important;
            line-height: 36px !important;
            width: 100% !important;
            float: none !important;
        }

        .table-wp table tbody tr td label {
            line-height: 36px;
            float: left;
            margin-right: 5px;
        }

        .custom-salon .active {
            background-color: black;
            color: white;
        }

        /*.container1 {
            box-sizing: border-box;
            margin: 150px auto;
            max-width: 500px;
            padding: 0 20px;
            width: 100%;
        }*/
    </style>
</asp:Content>
<asp:Content ID="ServiceAdd" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Gợi ý order nhà cung cấp</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="wp customer-add admin-product-add fe-service">
            <%-- Add --%>
            <div class="wp960 content-wp">
                <!-- System Message -->
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <!-- END System Message -->
                <div>
                    <table class="table-add admin-product-table-add fe-service-table-add">
                        <tbody>
                            <tr>
                                <td>
                                    <div>
                                        <table>
                                            <tr>
                                                <td>
                                                    <strong class="st-head"><i class="fa fa-clock-o"></i>Thời gian</strong>

                                                </td>
                                                <td>
                                                    <div class="time-wp">
                                                        <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeFrom" placeholder="Từ ngày" Style="margin-left: 10px!important"
                                                            ClientIDMode="Static" runat="server"></asp:TextBox>
                                                    </div>
                                                </td>
                                                <td>
                                                    <strong class="st-head" style="margin-left: 20px;">
                                                        <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                                                    </strong>
                                                </td>
                                                <td>

                                                    <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeTo" placeholder="Đến ngày" Style="margin-right: 10px; margin-left: 10px!important"
                                                        ClientIDMode="Static" runat="server"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <%-- <div class="wp960 content-wp">
                                                        <div class="row">
                                                            <div class="container1" style="padding-left: 0px;">
                                                                --<asp:DropDownList ID="ddlInventory" CssClass="form-control select" runat="server" ClientIDMode="Static"></asp:DropDownList>--

                                                                <div class="filter-item">
                                                                    <strong class="st-head"><i class="fa fa-clock-o"></i>Thời gian</strong>
                                                                    <div class="time-wp">
                                                                        <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeFrom" placeholder="Từ ngày" Style="margin-left: 10px!important"
                                                                            ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="ValidateTime" ControlToValidate="TxtDateTimeFrom" runat="server" CssClass="fb-cover-error" Text="Vui lòng chọn thời gian!"></asp:RequiredFieldValidator>
                                                                    </div>
                                                                    <strong class="st-head" style="margin-left: 10px;">
                                                                        <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                                                                    </strong>
                                                                    <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeTo" placeholder="Đến ngày" Style="margin-left: 10px!important"
                                                                        ClientIDMode="Static" runat="server"></asp:TextBox>
                                                                    <br />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>--%>
                                                </td>
                                                <td>
                                                    <input type="button" class="btn-add" onclick="viewData()" style="padding: 8px 15px; margin-left: 20px!important" value="Xem dữ liệu" />
                                                </td>
                                                <td>
                                                    <input type="button" class="btn-export" onclick="exportData()" style="padding: 8px 15px; margin-left: 20px!important" value="Export excel" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>

                                </td>
                            </tr>

                            <tr>
                                <td class="col-xs-9 right">
                                    <hr />
                                </td>
                            </tr>
                            <tr class="tr-field-ahalf tr-product">
                                <td class="col-xs-9 right">
                                    <div class="container table-listing pt-2">
                                    </div>

                                    <div class="table-temp d-none" style="display: none!important">
                                        <table class="table table-bordered table-hover" style="float: left;">
                                            <thead>
                                                <tr>
                                                    <th style="width: 100px; text-align: center;">STT</th>
                                                    <th style="width: 100px; text-align: center;">Kì thống kê</th>
                                                    <th style="text-align: center;">Tên sản phẩm</th>
                                                    <th style="width: 100px; text-align: center;">Tổng bán</th>
                                                    <th style="width: 100px; text-align: center;">Order kì này</th>
                                                    <%--<th style="width: auto"></th>--%>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                            </tr>

                            <%--<tr class="tr-send">
                                <td class="col-xs-9 right no-border">
                                    <span class="field-wp">
                                        <asp:Panel ID="BtnSend" CssClass="btn-send" runat="server" ClientIDMode="Static">Hoàn Tất</asp:Panel>
                                        <asp:Button ID="BtnFakeSend" OnClick="FuncComplete" runat="server" Text="Hoàn tất" ClientIDMode="Static" Style="display: none;"></asp:Button>
                                    </span>
                                </td>
                            </tr>--%>
                            <asp:HiddenField runat="server" ID="UrlKeyField" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="ImgCoverField" ClientIDMode="Static" />

                        </tbody>
                    </table>


                    <!-- input hidden -->
                    <asp:HiddenField ID="HDF_InventoryId" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_ProductIds" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_ServiceIds" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_UserImages" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_UserImagesDelete" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="Recipient" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HairMassage" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="Cosmetic" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_SalonId" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_lgId" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_BillId" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_FreeService" ClientIDMode="Static" runat="server" />
                    <asp:HiddenField ID="HDF_Rating" ClientIDMode="Static" runat="server" />
                    <!-- END input hidden -->
                </div>
            </div>

            <%-- end Add --%>
        </div>

        <script src="/assets/js/jquery.mCustomScrollbar.js"></script>
        <link href="/assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" />
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 200px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 35px !important;
                padding-top: 1px !important;
            }
        </style>
        <script type="text/javascript">

            var URL_INVENTORY = "<%= Libraries.AppConstants.URL_API_INVENTORY %>";
            var itemProduct;
            var quantity;
            var index;
            var ms;
            // Btn Send
            //$("#BtnSend").bind("click", function () {
            //    let result = true;


            //    var TxtDateTimeFrom = $('#TxtDateTimeFrom').val();
            //    if (TxtDateTimeFrom == "" || TxtDateTimeFrom == "0") {
            //        ShowMessage("Chú ý", "Yêu cầu chọn từ ngày!", 3);
            //        return;
            //    }

            //    let index = 0;
            //    if ($("#tableListing tbody tr").length == 0) {
            //        ShowMessage("Chú ý", "Yêu cầu thêm sản phẩm!", 3);
            //        return;
            //    }



            //    var listNameDuplicate = [];
            //    var indexArr = [];

            //    $('.table-temp table tbody tr').each(function () {
            //        var THIS = $(this);
            //        //let quantify = THIS.find("input.product-quantify").val();
            //        //let volume = THIS.find("input.product-volume").val();
            //        let orderNum = THIS.find("input.order-num").val();
            //        let name = THIS.find("td.td-product-name").text();
            //        if (orderNum == '' || orderNum == '0') {
            //            ShowMessage("Chú ý", 'Nhập số thứ tự cho sản phẩm: ' + name, 3, 7000);
            //            result = false;
            //        }

            //        //debugger
            //        var idx = indexArr.indexOf(orderNum);
            //        //var Id = THIS.find("td.td-product-code").attr("data-id");
            //        if (idx >= 0) {

            //            ShowMessage("Chú ý", "Các sản phẩm [" + listNameDuplicate[idx] + ", " + name + "] đang có số thứ tự giống nhau, cần phải điều chỉnh!", 3);
            //            result = false;
            //        }
            //        if (!result) {
            //            return;
            //        }
            //        if (orderNum != 999999) {
            //            indexArr.push(orderNum);
            //            listNameDuplicate.push(name);
            //        }
            //    });

            //    startLoading();
            //    debugger
            //    if (result) {
            //        var Ids = [];
            //        var prd = {};
            //        var table = $('#tableListing').DataTable();

            //        var tableTemp = $('.table-temp table tbody tr');
            //        //$("#tableListing tbody tr")
            //        tableTemp.each(function () {
            //            prd = {};
            //            var THIS = $(this);
            //            var Id = THIS.find("td.td-product-name").attr("data-id"),
            //                orderNum = THIS.find("input.order-num").val();
            //            //debugger;
            //            // check value
            //            Id = Id.toString().trim() != "" ? parseInt(Id) : 0;
            //            prd.ProductId = Id;
            //            prd.OrderNum = orderNum;
            //            if (orderNum != 999999) {
            //                Ids.push(prd);
            //            }
            //        });
            //        Ids = JSON.stringify(Ids);
            //        $("#HDF_ProductIds").val(Ids);
            //        $("#HDF_InventoryId").val(ddlInventory);

            //        //console.log($("#HDF_ProductIds").val());
            //        $("#BtnFakeSend").click();

            //    }
            //    finishLoading();
            //});

            jQuery(document).ready(function () {

                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) { }
                });

                var qs = getQueryStrings();
                ShowMessage("Thông báo", qs["msg_message"], parseInt(qs["msg_status"]));

                debugger
                <%--var inventoryId = "<%= Id %>";
                if (inventoryId != null && inventoryId > 0) {
                    $('#ddlInventory').val(inventoryId);
                    $('#ddlInventory').change();
                    viewData();
                }--%>

                $('#selectMultiple').multi({
                    search_placeholder: 'Search Salon...',
                });

                // bind data to dropdownlist
                <%--ms = $('#ms').magicSuggest({
                    maxSelection: 1,
                    data: <%=ListProduct %>,
                    valueField: 'Id',
                    displayField: 'Name',
                });--%>

                $(ms).on('selectionchange', function (e, m) {
                    itemProduct = null;
                    var listProduct = this.getSelection();
                    console.log(listProduct);
                    var listData = ms.getData();
                    if (listProduct.length > 0) {
                        for (var i = 0; i < listData.length; i++) {
                            if (listProduct[0].Id == listData[i].Id) {
                                itemProduct = listProduct[0];
                                $("#inputVolume").focus();
                                break;
                            }
                            else {
                                continue;
                            }
                        }
                        if (itemProduct == null) {
                            ShowMessage("Chú ý", "Lễ tân chú ý chọn đúng sản phẩm trong danh sách vật tư dịch vụ.", 3, 7000);
                            ms.clear();
                            $(".ms-sel-ctn input[type='text']").focus();
                        }
                    }
                });
            });


            function changeValue(THIS, index) {
                debugger
                var productId = THIS.find("td.td-product-name").attr("data-id");
                var orderNum = THIS.find("input.order-num").val();
                var tableTemp = $('.table-temp table');
                var input = tableTemp.find("td[data-id='" + productId + "']").each(function () {
                    debugger
                    var THIS = $(this).parent();
                    var tmp = THIS.find("input.order-num");
                    tmp.val(orderNum);
                });
            }


            function viewData() {
                let panelStaff = $('#pills-staff').hasClass('active');
                let panelSalon = $('#pills-salon').hasClass('active');
                let panelAll = $('#pills-all').hasClass('active');

                //
                debugger;

                var TxtDateTimeFrom = $('#TxtDateTimeFrom').val();
                if (TxtDateTimeFrom == "" || TxtDateTimeFrom == "0") {
                    ShowMessage("Chú ý", "Yêu cầu chọn Từ ngày!", 3);
                    return;
                }

                var TxtDateTimeTo = $('#TxtDateTimeTo').val();
                if (TxtDateTimeTo == "" || TxtDateTimeTo == "0") {
                    ShowMessage("Chú ý", "Yêu cầu chọn Đến ngày!", 3);
                    return;
                }

                startLoading();

                let api = "/api/product-sold" + "?FromDate=" + TxtDateTimeFrom + "&ToDate=" + TxtDateTimeTo;
                let response = AjaxGetApi(URL_INVENTORY + api);
                let objTr = "";
                debugger;
                $("#tableListing tbody").html("");

                if (typeof response === "object") {
                    $('.table-listing').empty();
                    var tableTemp = $('.table-temp table').attr('id', 'tableListing').clone();
                    $('.table-listing').append(tableTemp);
                    $.each(response,
                        function (index, value) {
                            //debugger;
                            objTr += '<tr>' +
                                '<td style="text-align: center;align-items:center;"><span style="width:100%">' + (index + 1) + '</span></td>' +
                                '<td style="text-align:center;margin-left: 10px!important" class="td-statistic-date"> <span style="width:100%;text-align:center;" >' + value.formatStatisticDate + '</span></td>' +
                                '<td class="td-product-name" style="text-align:left;margin-left: 10px!important" data-id="' + value.productId + '"><span style="width:100%;text-align:left;margin-left: 10px!important" >' + value.productName + '</span></td>' +
                                '<td style="text-align:left;" class="td-total-sold"><span style="width:100%;text-align:left;margin-left: 50px!important" >' + value.totalSold + '</span></td>' +
                                '<td style="text-align:left;" class="td-order-num"><span style="width:100%;text-align:left;margin-left: 50px!important" >' + value.orderNum + '</span></td>' +
                                //'<td class=" map-edit">' +
                                //'<div class="edit-wp">' +
                                //'</div>' +
                                '</td>' +
                                '</tr>';
                        });
                    $("#tableListing tbody").append(objTr);
                    ConfigTableListing();
                }
                finishLoading();
            };
            function ConfigTableListing() {
                var table = $('#tableListing').DataTable(
                    {
                        order: [[0, 'asc'], [1, 'asc']],
                        rowGroup: {
                            dataSrc: 0,
                        },
                        pagingType: "full_numbers",
                        language: {
                            lengthMenu: "Dòng / trang _MENU_",
                            paginate: {
                                previous: "<",
                                first: "Đầu",
                                last: "Cuối",
                                next: ">"
                            },
                            info: "Dòng _START_ tới _END_ của _TOTAL_ dòng",
                            search: "Nhập tìm kiếm:"
                        },
                        lengthMenu: [[10, 25, 50, 200], [10, 25, 50, 200]]
                    });
            }



            //// Update order item
            //function UpdateItemOrder(dom) {
            //    var index = 1;
            //    dom.find("tr").each(function () {
            //        $(this).find("td.td-product-index").text(index);
            //        index++;
            //    });
            //}

            //// Remove item đã được chọn
            //function RemoveItem(THIS, name, itemName) {
            //    // Confirm
            //    $(".confirm-yn").openEBPopup();

            //    $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn hủy [ " + name + " ] ?");
            //    $("#EBPopup .yn-yes").bind("click", function () {
            //        var Code = THIS.find(".td-staff-code").attr("data-code");
            //        $("#quick-" + itemName).find("input[data-code='" + Code + "']").removeAttr("checked");
            //        THIS.remove();
            //        //getDataProduct();
            //        UpdateItemDisplay(itemName);
            //        autoCloseEBPopup(0);
            //    });
            //    $("#EBPopup .yn-no").bind("click", function () {
            //        autoCloseEBPopup(0);
            //    });
            //}

            ////reset input
            //function resetInputForm() {
            //    ms.clear();
            //    $("#inputQuantify").val('');
            //    $("#inputVolume").val('');
            //    $(ms).focus();
            //    $(".ms-sel-ctn input[type='text']").focus();
            //}

            //// Update table display
            //function UpdateItemDisplay(itemName) {
            //    var dom = $(".table-item-" + itemName);
            //    var len = dom.find("tbody tr").length;
            //    if (len == 0) {
            //        dom.parent().hide();
            //    } else {
            //        UpdateItemOrder(dom.find("tbody"));
            //    }
            //}

            // Validate keypress
            function ValidateKeypress(numcheck, e) {
                var keynum, keychar, numcheck;
                if (window.event) {
                    keynum = e.keyCode;
                }
                else if (e.which) {
                    keynum = e.which;
                }
                if (keynum == 8 || keynum == 127 || keynum == null || keynum == 9 || keynum == 0 || keynum == 13) return true;
                keychar = String.fromCharCode(keynum);
                var result = numcheck.test(keychar);
                return result;
            }
            //function activeStaff() {
            //    preventDefault();
            //    var list = $('.custom-salon button');
            //    $.each(list, function (index, value) {
            //        $(value).removeClass('active');
            //    });
            //    $('#ddlDepartment').removeAttr('disabled');
            //    $('#ddlDepartment').val("0");

            //    $('#ddlInventorySalon').removeAttr('disabled');
            //    $('#ddlInventorySalon').val("0");
            //    $('select').select2();
            //    $('.custom-salon button.staff').addClass('active');
            //}
            //function activeSalon() {
            //    preventDefault();
            //    var list = $('.custom-salon button');
            //    $.each(list, function (index, value) {
            //        $(value).removeClass('active');
            //    });
            //    $('#ddlDepartment').attr('disabled', 'disabled');
            //    $('#ddlDepartment').val("0");

            //    $('#ddlInventorySalon').removeAttr('disabled');
            //    $('#ddlInventorySalon').val("0");

            //    $('select').select2();
            //    $('.custom-salon button.salon').addClass('active');
            //}
            //function activeAll() {
            //    preventDefault();
            //    var list = $('.custom-salon button');
            //    $.each(list, function (index, value) {
            //        $(value).removeClass('active');
            //    });
            //    $('#ddlDepartment').attr('disabled', 'disabled');
            //    $('#ddlDepartment').val("0");

            //    $('#ddlInventorySalon').attr('disabled', 'disabled');
            //    $('#ddlInventorySalon').val("0");
            //    $('select').select2();
            //    $('.custom-salon button.all').addClass('active');
            //}

            // header, body, filename
            var excelHeader = ["STT", "Kì thống kê", "Tên sản phẩm", "Tổng bán", "Order kì này"];
            var excelBody = [];
            var excelFileName = "";
            //
            function exportData() {
                var kt = true;
                //var error = '';
                //var timeFrom = $(elms.fromDate).val();
                //var timeTo = $(elms.toDate).val();
                //var inventoryId = $("#ddlInventory").val();
                //var statusId = $("#ddlStatus").val();
                //if (!CheckParameter(timeFrom)) {
                //    kt = false;
                //    error += " Bạn phải chọn ngày bắt đầu! ";
                //    $('#TxtDateTimeFrom').css("border-color", "red");
                //}
                //else if (!CheckParameter(!timeTo)) {
                //    kt = false;
                //    error += " Bạn phải chọn ngày kết thúc! ";
                //    $('#TxtDateTimeTo').css("border-color", "red");
                //}
                var TxtDateTimeFrom = $('#TxtDateTimeFrom').val();
                if (TxtDateTimeFrom == "" || TxtDateTimeFrom == "0") {
                    ShowMessage("Chú ý", "Yêu cầu chọn Từ ngày!", 3);
                    return;
                }

                var TxtDateTimeTo = $('#TxtDateTimeTo').val();
                if (TxtDateTimeTo == "" || TxtDateTimeTo == "0") {
                    ShowMessage("Chú ý", "Yêu cầu chọn Đến ngày!", 3);
                    return;
                }

                //if (!kt) {
                //    ShowMessage('', error, 3);
                //}
                //else {
                let api = "/api/product-sold" + "?FromDate=" + TxtDateTimeFrom + "&ToDate=" + TxtDateTimeTo;
                let response = AjaxGetApi(URL_INVENTORY + api);

                excelBody = [];
                excelFileName = "Báo cáo gợi ý số order nhà cung cấp từ " + TxtDateTimeFrom + " đến " + TxtDateTimeTo + "";
                startLoading();
                $.ajax({
                    type: "GET",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    url: URL_INVENTORY + "/api/product-sold" + "?FromDate=" + TxtDateTimeFrom + "&ToDate=" + TxtDateTimeTo,
                    success: function (response) {
                        //console.log(response);
                        if (response.length > 0) {
                            response.forEach(function (v, i) {
                                let excelRow = [(i + 1) + "",
                                v.formatStatisticDate,
                                v.productName,
                                v.totalSold,
                                v.orderNum
                                ];
                                excelBody.push(excelRow);
                            });
                            console.log(excelBody);
                            //
                            exportExcel(excelFileName, "report", excelHeader, excelBody);
                        }
                        finishLoading();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        var responseText = jqXHR.responseJSON.message;
                        ShowMessage('', responseText, 4);
                        finishLoading();
                    }
                });
                //}
            }
        </script>
        <!-- Page loading -->
        <div class="page-loading">
            <p>Vui lòng đợi trong giây lát...</p>
        </div>
    </asp:Panel>
</asp:Content>


