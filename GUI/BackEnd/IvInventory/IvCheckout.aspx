﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="IvCheckout.aspx.cs" MasterPageFile="~/TemplateMaster/SiteMaster.Master" Inherits="_30shine.GUI.BackEnd.IvInventory.IvCheckout" %>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">

    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <link href="/Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="/Assets/js/select2/select2.min.js"></script>
        <script src="/Assets/js/jquery.scannerdetection.js"></script>
        <link href="/Assets/css/erp.30shine.com/timekeeping/timekeeping.common.css" rel="stylesheet" />
        <style>
            .btn-confirm {
                margin-right: 10px;
            }

                .btn-confirm i, .btn-delete i {
                    color: #fff;
                    padding-right: 5px;
                }

            h3, .span-title {
                font-size: 16px;
            }

            th {
                font-size: 14px !important;
            }
        </style>
        <section>
            <div class="wp sub-menu row">
                <div class="wp960">
                    <div class="wp content-wp">
                        <ul class="ul-sub-menu" id="subMenu">
                            <li>Kiểm kho &nbsp;&#187; </li>
                            <li class="li-add"><a href="#">Tạo mới</a></li>
                            <li class="li-listing"><a href="/admin/hang-hoa/danh-sach-kiem-kho.html">Danh sách kiểm kho</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <div class="alert" id="success-alert" role="alert">
        </div>
        <section>
            <header id="header">
                <div class="container-fluid box-header">
                    <div class="box-top worktime-listing-header">
                        <span class="span-title">Tên kho: <span class="wh-name" style="font-size: 16px !important; font-weight: 600"><%=WHName %></span></span>
                        <div class="customer-add customer-listing be-report timekeeping-box">
                            <div class="row" style="clear: both; border: 1px solid #ccc; padding-bottom: 5px;">
                                <div class="wp960 content-wp date-timekeeping">
                                    <div class="row">
                                        <div class="filter-item" style="margin-left: 0;">
                                            <strong class="st-head" style="margin-right: 10px;"><i class="fa fa-barcode"></i>Barcode</strong>
                                            <div class="datepicker-wp">
                                                <input type="hidden" id="barcode" />
                                                <input type="text" class="form-control" id="txtBarCode" placeholder="Barcode" style="width: 190px !important;" />
                                            </div>
                                        </div>
                                        <button type="button" id="btnViewData" onclick="GetInfoBarcode($('#txtBarCode').val())" class="st-head btn btn-viewdata" style="line-height: 30px !important;">Thêm sản phẩm</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <div id="body">
                <div class="wp960 content-wp date-timekeeping">
                    <div class="row">
                        <h3>Danh sách kiểm hàng</h3>
                        <%--<asp:Label runat="server" ClientIDMode="Static" ID="lblDay" Visible="true"></asp:Label>--%>
                    </div>
                </div>
                <div class="row box-department-staff">
                    <div class="row box-table-add-staff">
                        <div class="col-md-12">
                            <table id="table-header-timekeeping" class="table table-bordered table-header">
                                <thead style="background: #ccc">
                                    <tr>
                                        <th id="th-hour-timekeeping">Mã SP</th>
                                        <th id="th-stt-timekeeping">Barcode</th>
                                        <th id="th-id-timekeeping">Tên SP</th>
                                        <th id="th-name-timekeeping" style="max-width: 150px !important; width: 120px;">Số lượng</th>
                                        <th id="th-department-timekeeping">Cập nhật</th>
                                    </tr>
                                </thead>
                                <tbody id="body-content-timekeeping">
                                    <% foreach (var item in dataViews)
                                        {
                                            Response.Write("<tr>");
                                            Response.Write("<td class='td-code'>" + item.Code + "</td>");
                                            Response.Write("<td class='td-barcode'>" + item.BarCode + "</td>");
                                            Response.Write("<td class='td-name'>" + item.ProductName + "</td>");
                                            Response.Write("<td><input type='number' class='form-control txt-quantity' onchange='ValidNumber($(this))' value='" + item.Quantity + "' style='width: 100px' /></td>");
                                            Response.Write("<td><button type='button' data-id='" + item.CheckoutId + "' onclick='UpdateRecord($(this))' class='btn btn-warning btn-confirm' " + (validDay == 0 ? "disabled='disabled'" : "") + "><i class='fa fa-check' aria-hidden='true'></i>Cập nhật</button>" +
                                                "<button type='button' class='btn btn-danger btn-delete' onclick='DeleteProduct($(this))' data-id='" + item.CheckoutId + "' " + (validDay == 0 ? "disabled='disabled'" : "") + "><i class='fa fa-times' aria-hidden='true'></i></i>Xóa</button>" +
                                                "</td>");
                                            Response.Write("</tr>");
                                        } %>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <script type="text/javascript">
            let validDay = 0;
            let domain = '<%=Libraries.AppConstants.URL_API_INVENTORY%>';
            let salonId = <%=Convert.ToInt32(Session["SalonId"])%>;
            let text = "";
            $(document).ready(function () {
                validDay = <%=validDay%>;
            })
            //Barcode detection
            $(document).scannerDetection({
                timeBeforeScanTest: 1000, // wait for the next character for upto 200ms
                avgTimeByChar: 40, // it's not a barcode if a character takes longer than 100ms
                preventDefault: false,
                endChar: ['\n'],
                onComplete: function (barcode, qty) {
                    $("#barcode").val('');
                    $("#barcode").val(barcode);
                    var bc = barcode.substring(0, barcode.length - 1);
                    GetInfoBarcode(bc);
                }
            });
            function GetInfoBarcode(barcode) {
                if (validDay == 0) {
                    ShowMessage("Thông báo", "Kiểm hàng chỉ được thực hiện trong khoảng thời gian cố định hàng tuần", 4);
                    return;
                }
                $.ajax({
                    url: domain+"/api/iv-checkout",
                    type: "POST",
                    dataType: "json",
                    contentType: "application/json;charset=utf-8",
                    data: "{salonId:" + salonId + ",barCode:'" + barcode + "'}",
                    success: function (data, textStatus, xhr) {
                        var tbody = $("#body-content-timekeeping");
                        var rs = "";
                        if (data == null) {
                            ShowMessage("Thông báo", "Barcode không tồn tại hoặc quét quá nhanh. Vui lòng quét lại", 4);
                        }
                        else {
                            //console.log(data);
                            var duplicate = true;
                            tbody.find("tr").each(function () {
                                var s = $(this).find(".td-barcode").text();
                                if ($(this).find(".td-code").text() === data.code) {
                                    duplicate = false;
                                }
                                else {
                                    duplicate = true;
                                }
                                var quantity = parseInt($(this).find(".txt-quantity").val()) + 1;
                                if (s == barcode) {
                                    $(this).find(".txt-quantity").val(quantity);
                                    return false;
                                }
                            });
                            if (duplicate) {
                                rs += "<tr>";
                                rs += "<td class='td-code'>" + data.code + "</td>";
                                rs += "<td class='td-barcode'>" + data.barCode + "</td>";
                                rs += "<td class='td-name'>" + data.productName + "</td>";
                                rs += "<td><input type='number' class='form-control txt-quantity' value='1' style='width: 100px' /></td>";
                                rs += "<td><button type='button' data-id='" + data.checkoutId + "' onclick='UpdateRecord($(this))' class='btn btn-warning btn-confirm'><i class='fa fa-check' aria-hidden='true'></i>Cập nhật</button>";
                                rs += "<button type='button' class='btn btn-danger btn-delete' onclick='DeleteProduct($(this))' data-id='" + data.checkoutId + "'><i class='fa fa-times' aria-hidden='true'></i></i>Xóa</button>";
                                rs += "</td > ";
                                rs += "</tr>";
                                tbody.append(rs);
                            }
                        }
                       
                    },
                     error: function (xhr, textStatus, errorThrown) {
                            console.log('Error in Operation');
                        }
                })
            }
            function ValidNumber(This) {
                if (This.val() < 0) {
                    ShowMessage("Thông báo", "Số lượng phải lớn hơn 0", 4);
                    This.focus();
                    This.css("border-color", "pink");
                    //This.val(1);
                    return false;
                }
            }
            function DeleteProduct(This) {
                //if (validDay == 0) {
                //    This.attr("disabled", "disabled");
                //    //ShowMessage("Thông báo", "Kiểm hàng chỉ được thực hiện từ 09h tối thứ 2 đến 02h đêm thứ 3 hoặc 09h tối thứ 3 đến 02h đêm thứ 4 hàng tuần", 4);
                //    return;
                //}
                var id = This.attr("data-id");
                var c = confirm("Bạn chắc chắn muốn xóa sản phẩm này không?");
                if (c) {
                    $.ajax({
                        url: domain+"/api/iv-checkout?checkOutId="+id,
                        type: "DELETE",
                        dataType: "json",
                        contentType: "application/json;charset=utf-8",
                        //data: "{checkOutId:'" + id + "'}",
                         success: function (data, textStatus, xhr) {
                               if (data === true) {
                                This.closest("tr").remove();
                                ShowMessage("Thông báo", "Xóa sản phẩm thành công", 1);
                            }
                            else {
                                ShowMessage("Thông báo", "Có lỗi xảy ra. Vui lòng thử lại", 4);
                            }
                        },
                        error: function (xhr, textStatus, errorThrown) {
                            console.log('Error in Operation');
                        }
                    })
                }
                else {
                    return;
                }
            }

            function UpdateRecord(This) {
                //if (validDay == 0) {
                //    This.attr("disabled", "disabled");
                //    //ShowMessage("Thông báo", "Kiểm hàng chỉ được thực hiện từ 09h tối thứ 2 đến 02h đêm thứ 3 hoặc 09h tối thứ 3 đến 02h đêm thứ 4 hàng tuần", 4);
                //    return;
                //}
                var id = This.attr("data-id");
                var quantity = This.closest("tr").find(".txt-quantity").val();
                if (quantity <=0) {
                    ShowMessage("Thông báo", "Số lượng phải lớn hơn 0", 4);
                    This.closest("tr").find(".txt-quantity").focus();
                    This.closest("tr").find(".txt-quantity").css("border-color", "pink");
                    return;
                }
                $.ajax({
                    url: domain+'/api/iv-checkout',
                    type: 'PUT',
                    dataType: 'json',
                    contentType: "application/json;charset=utf-8",
                    data: "{checkoutId:" + id + ",quantity:" + quantity + "}",
                    success: function (data, textStatus, xhr) {
                        if (data === true) {
                            ShowMessage("Thông báo", "Cập nhật sản phẩm thành công", 1);
                        }
                        else {
                            ShowMessage("Thông báo", "Thời gian cập nhật sản phẩm phải cùng 1 lần kiểm hàng", 4);
                        }
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        console.log('Error in Operation');
                    }
                });
            }
        </script>
    </asp:Panel>
</asp:Content>
