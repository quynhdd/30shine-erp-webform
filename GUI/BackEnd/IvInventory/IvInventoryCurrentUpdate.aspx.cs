﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.IvInventory
{
    public partial class IvInventoryCurrentUpdate : System.Web.UI.Page
    {
        private bool Perm_ShowSalon = false;
        private bool Perm_Access = false;
        private bool Perm_AllSalon = false;
        protected bool Perm_ViewAllData = false;
        private bool Perm_Edit = false;
        private bool Perm_UpdateInventory = false;

        /// <summary>
        /// set permission
        /// </summary>
        protected void SetPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                pageId = permissionModel.GetRegexPageIdSpecial().Replace(pageId, ".html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ShowSalon = permissionModel.CheckPermisionByAction("Perm_ShowSalon", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_AllSalon = permissionModel.CheckPermisionByAction("Perm_AllSalon", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        /// <summary>
        /// Excute
        /// </summary>
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
            else if (Perm_ShowSalon)
            {
                TrSalon.Visible = true;
            }
        }

        /// <summary>
        /// Page load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            // check permission
            CheckViewPriceProduct();
            if (!IsPostBack)
            {
                BindDdlInventorySalon();
                TxtDateTimeFrom.Text = String.Format("{0:dd/MM/yyyy}", DateTime.Today);
                TxtDateTimeFrom.Enabled = false;
            }
        }

        /// <summary>
        /// Bind data InventorySalon
        /// </summary>
        private void BindDdlInventorySalon()
        {
            try
            {
                var UserId = Convert.ToInt32(HttpContext.Current.Session["User_Id"]);
                using (var db = new Solution_30shineEntities())
                {
                    var salonCurrent = db.Staffs.Where(r => r.Id == UserId).Select(r => new { Id = r.SalonId ?? 0 });

                    var salonArea = db.PermissionSalonAreas.Where(r => ((r.StaffId == UserId && UserId > 0) || UserId == 0) &&
                                                                       r.IsDelete == false &&
                                                                       r.IsActive == true)
                                                                       .Select(r => new { Id = r.SalonId ?? 0 });
                    var listInventorySalon = new List<MODEL.ENTITY.EDMX.IvInventory>();
                    if (Perm_ShowSalon)
                    {
                        listInventorySalon = (from a in db.IvInventories
                                              where a.IsDelete == false && a.Type == 2
                                              select a).ToList();
                        var item = new MODEL.ENTITY.EDMX.IvInventory();
                        item.Name = "Chọn kho Salon";
                        item.Id = 0;
                        listInventorySalon.Insert(0, item);
                    }
                    else
                    {
                        var listSalonId = salonCurrent.Union(salonArea).Where(r => r.Id != 0).Select(r => r);

                        listInventorySalon = (from a in db.IvInventories
                                              join b in listSalonId on a.SalonId equals b.Id
                                              where a.IsDelete == false && a.Type == 2
                                              select a).ToList();
                    }
                    var ddls = new List<DropDownList>() { Salon };
                    for (var i = 0; i < ddls.Count; i++)
                    {
                        ddls[i].DataTextField = "Name";
                        ddls[i].DataValueField = "Id";
                        ddls[i].DataSource = listInventorySalon;
                        ddls[i].DataBind();
                    }
                    //Salon.Items.Insert(0, new ListItem("Chọn kho Salon", "0"));
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// CheckViewPriceProduct
        /// </summary>
        private void CheckViewPriceProduct()
        {
            var arrayPermision = Session["User_Permission"].ToString().Split(',');
            if (arrayPermision.Length > 0)
            {
                string checkAdmin = Array.Find(arrayPermision, s => s.Equals("ADMIN"));
                string checkRoot = Array.Find(arrayPermision, s => s.Equals("root"));
                string checkSUPPLY_CHAIN_ADMIN = Array.Find(arrayPermision, s => s.Equals("SUPPLY_CHAIN_ADMIN"));
                string checkACCOUNTANT_ADMIN = Array.Find(arrayPermision, s => s.Equals("ACCOUNTANT_ADMIN"));
                if (!String.IsNullOrEmpty(checkRoot) || !String.IsNullOrEmpty(checkAdmin) || !String.IsNullOrEmpty(checkSUPPLY_CHAIN_ADMIN) || !String.IsNullOrEmpty(checkACCOUNTANT_ADMIN))
                {
                    TxtDateTimeFrom.Enabled = true;
                    Perm_ViewAllData = true;
                    Perm_UpdateInventory = true;
                }
            }
        }

        /// <summary>
        /// check không cho phép cập nhập tồn kho sau 22h15 hàng ngày
        /// </summary>
        public bool CheckUpdateInventory()
        {
            DateTime datetimeNow = DateTime.Now;
            if (Perm_UpdateInventory)
            {
                return true;
            }
            var minuteEditBill = datetimeNow.Hour * 60 + datetimeNow.Minute;
            var minuteEndDate = 22 * 60 + 15;
            if (minuteEditBill <= minuteEndDate)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}