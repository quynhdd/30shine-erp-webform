﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.IvInventory
{
    public partial class InvListing : System.Web.UI.Page
    {
        protected Paging PAGING = new Paging();
        CultureInfo culture = new CultureInfo("vi-VN");
        private Solution_30shineEntities db = new Solution_30shineEntities();
        private bool Perm_Access = false;
        protected bool Perm_AddNew = false;
        protected bool Perm_Delete = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            //SetPermission();
            if (!IsPostBack)
            {
                BindInventoryType();
                BindCategory();
            }
        }
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_AddNew = permissionModel.CheckPermisionByAction("Perm_AddNew", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }

        }

        public void Repeater_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
              || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (Perm_AddNew == true)
                    ((Panel)(e.Item.FindControl("EAadd"))).Visible = true;
                if (Perm_Delete == true)
                    ((Panel)(e.Item.FindControl("EAdelete"))).Visible = true;
            }
        }
        protected void _BtnClick(object sender,EventArgs e)
        {
            BindData();
            RemoveLoading();
        }

        protected void BindInventoryType()
        {
            var result = new HttpClient().GetAsync(Libraries.AppConstants.URL_API_INVENTORY + "/api/inventory/get-warehouse-type").Result;
            if (result.IsSuccessStatusCode)
            {
                var data = result.Content.ReadAsStringAsync().Result;
                if (data != null)
                {
                    var type = new JavaScriptSerializer().Deserialize<List<Tbl_Config>>(data);
                    var Key = 0;

                    InvType.DataTextField = "Label";
                    InvType.DataValueField = "Value";

                    ListItem item = new ListItem("Chọn tất cả", "0");
                    InvType.Items.Insert(0, item);

                    foreach (var v in type)
                    {
                        Key++;
                        item = new ListItem(v.Label, v.Value);
                        InvType.Items.Insert(Key, item);
                    }
                }
            }
        }
        protected void BindCategory()
        {
            var result = new HttpClient().GetAsync(Libraries.AppConstants.URL_API_INVENTORY + "/api/inventory/get-product-category").Result;
            if (result.IsSuccessStatusCode)
            {
                var data = result.Content.ReadAsStringAsync().Result;
                if (data != null)
                {
                    var type = new JavaScriptSerializer().Deserialize<List<Tbl_Category>>(data);
                    var Key = 0;

                    ddlCategory.DataTextField = "Name";
                    ddlCategory.DataValueField = "Id";

                    ListItem item = new ListItem("Chọn loại SP", "0");
                    ddlCategory.Items.Insert(0, item);

                    foreach (var v in type)
                    {
                        Key++;
                        item = new ListItem(v.Name, v.Id.ToString());
                        ddlCategory.Items.Insert(Key, item);
                    }
                }
            }
        }

        protected void BindData()
        {
            var result = new HttpClient().GetAsync(Libraries.AppConstants.URL_API_INVENTORY + "/api/inventory").Result;
           
            var invType = Convert.ToInt32(InvType.SelectedValue);
            if (result.IsSuccessStatusCode)
            {
                var data = result.Content.ReadAsStringAsync().Result;
                if (data != null)
                {
                    var _data = new JavaScriptSerializer().Deserialize<List<OutputInventory>>(data);
                    List<OutputInventory> _return = null;
                    switch (invType)
                    {
                        case 0:
                            _return = _data.ToList();
                            break;
                        default:
                            _return = _data.Where(w => w.Type == invType).ToList();
                            
                            break;
                    }
                    Bind_Paging(_return.Count);
                    rptDanhsach.DataSource = _return.Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                    rptDanhsach.DataBind();
                }
            }
        }

        protected void Bind_Paging(int TotalRow)
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(TotalRow) : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage(int TotalRow)
        {
            TotalRow -= PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        public class OutputInventory {
            public int Id { get; set; }
            public string Name { get; set; }
            public string TypeName { get; set; }
            public int Type { get; set; }
            public int SalonId { get; set; }
            public int StaffId { get; set; }
            public DateTime CreatedDate { get; set; }
        }
    }
}