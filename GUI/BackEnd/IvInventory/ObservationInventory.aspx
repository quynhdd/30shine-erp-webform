﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/TemplateMaster/SiteMasterVersion1.Master" CodeBehind="ObservationInventory.aspx.cs" Inherits="_30shine.GUI.BackEnd.IvInventory.ObservationInventory" %>

<asp:Content ID="CustomerAdd" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel ID="ContentWrap" runat="server" ClientIDMode="Static">
        <section>
            <div class="container-fluid sub-menu">
                <div class="row">
                    <div class="col-md-12 pt-3 pb-2">
                        <ul>
                            <li>Theo dõi tồn đầu - mức dùng &nbsp;&#187; Dịch vụ &#187;</li>
                        </ul>
                    </div>
                    <div class="col-md-12">
                        <div class="sub-menu-boder"></div>
                    </div>
                </div>
            </div>
            <form runat="server">
                <div class="container-fluid pt-2 pb-2">
                    <div class="row">
                        <div class="form-group sub-search pr-0 col-md-12 col-xl-1">
                            <strong class="align-middle"><i class="fa fa-filter"></i>Lọc kết quả </strong>
                        </div>



                        <div class="col-md-12 col-xl-auto">
                            <strong class="mr-2 form-group" style="width: 75px; float: left"><i class="fa fa-clock middle" aria-hidden="true"></i><span class="middle">Thời gian</span></strong>
                            <input type="text" name="fromDate" class="form-control auto-width datetime-picker float-left mr-2 form-group" placeholder="Từ ngày" />
                            <strong class="align-middle mr-2 form-group" style="width: 15px; float: left"><i class="fa fa-arrow-circle-right middle"></i></strong>
                            <input type="text" name="toDate" class="form-control auto-width datetime-picker float-left form-group" placeholder="Đến ngày" />
                        </div>

                        <div class="form-group col-md-12 col-xl-auto">
                            <label class="label">Qúa khứ/hiện tại</label>
                            <select class="select form-control" data-name="the-time" id="the-time">
                                <option value="1">Quá khứ</option>
                                <option value="2">Hiện tại</option>
                            </select>
                        </div>

                        <div class="form-group col-md-12 col-xl-auto">
                            <label class="label">Kho Salon</label>
                            <asp:DropDownList ID="ddlInventorySalon" data-name="inventoryId" CssClass="form-control select" runat="server" ClientIDMode="Static"></asp:DropDownList>
                        </div>

                        <div class="form-group col-md-12 col-xl-auto">
                            <label class="label">Chọn bộ phận</label>
                            <asp:DropDownList ID="ddlDepartment" data-name="departmentId" runat="server" CssClass="select form-control" ClientIDMode="Static"></asp:DropDownList>
                        </div>


                        <div class="form-group col-md-12 col-xl-auto pl-0">
                            <div id="ViewDataFilter" class="form-group" onclick="viewData()">
                                <div class="btn-viewdata">Xem dữ liệu</div>
                            </div>
                        </div>

                        <div class="col-md-12 col-xl-12">
                            <div class="sub-menu-boder-low"></div>
                        </div>
                    </div>
                </div>
                <div class="container-fluid table-listing pt-2">
                </div>

                <div class="table-temp d-none">
                    <table class="table table-bordered table-hover" style="float: left;">
                        <thead>
                            <tr>
                                <th rowspan="2" class="text-center align-middle">SALON</th>
                                <th rowspan="2" class="text-center align-middle">BỘ PHẬN</th>
                                <th rowspan="2" class="text-center align-middle">NHÂN VIÊN</th>
                                <th rowspan="2" class="text-center align-middle">VẬT TƯ MỸ PHẨM</th>
                                <th rowspan="2" class="text-center align-middle">KHỐI LƯỢNG</th>
                                <th colspan="2" class="text-center align-middle">TỒN ĐẦU</th>
                                <th rowspan="2" class="text-center align-middle">NHẬP( NHÂN VIÊN NHẬN )</th>
                                <th rowspan="2" class="text-center align-middle">XUẤT( LƯỢNG DÙNG TRONG NGÀY )</th>
                                <th colspan="2" class="text-center align-middle">TỒN CUỐI NGÀY</th>
                                <th rowspan="2" class="text-center align-middle">SỐ ORDER</th>
                            </tr>
                            <tr>

                                <th class="text-center align-middle">Khối lượng(ml)</th>
                                <th class="text-center align-middle">Số khách</th>
                                <th class="text-center align-middle">Khối lượng(ml)</th>
                                <th class="text-center align-middle">Số khách</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </form>
        </section>

        <style>
            .select2-container--default .select2-selection--single {
                height: 31px !important;
            }

            .select2-container {
                width: 180px !important;
            }

            .auto-width {
                width: 180px;
                height: 31px;
                font-family: Roboto Condensed Regular;
                font-size: 14px;
            }

            .label {
                width: 110px;
            }

            .middle {
                vertical-align: -webkit-baseline-middle;
            }

            #tableListing_paginate {
                display: none;
            }
        </style>
        <script>
            var domain = '<% = Libraries.AppConstants.URL_API_INVENTORY %>'
            //hoac domain = Assets/js/Config/Domain.js;
            // api cau hinh trong  Assets/js/Config/Api.js
            var elms = {
                fromDate: 'input[name=fromDate]',
                toDate: 'input[name=toDate]',
                inventoryId: 'select[data-name=inventoryId]',
                departmentId: 'select[data-name=departmentId]',
                theTime: 'select[data-name=the-time]'
            };

            function viewData() {
                let timeFrom = $(elms.fromDate).val();
                let timeTo = $(elms.toDate).val();

                let inventoryId = $(elms.inventoryId).val();
                let departmentId = $(elms.departmentId).val();
                let theTime = ~~$(elms.theTime).val();
                let api = "";
                if (theTime === 2) {
                    api = ReportInventory.ProductUsedCurrent + "?inventoryId=" + inventoryId + "&departmentId=" + departmentId;
                }
                else {
                    if (typeof timeFrom === "undefined" || timeFrom === "" || timeFrom === null ||
                        typeof timeTo === "undefined" || timeTo === "" || timeTo === null) {
                        finishLoading();
                        ShowMessage("Thông báo", "Vui lòng chọn ngày!", 3);
                        return;
                    }
                    api = ReportInventory.ProductUsedV1 + "?inventoryId=" + inventoryId + "&departmentId=" + departmentId + "&timeFrom=" + timeFrom + "&timeTo=" + timeTo;
                }
                startLoading();
                let response = AjaxGetApi(domain + api);
                let objTr = "";
                console.log(response);

                $("#tableListing tbody").html("");
                if (typeof response === "object") {
                    $('.table-listing').empty();
                    var tableTemp = $('.table-temp table').attr('id', 'tableListing').clone();
                    $('.table-listing').append(tableTemp);
                    $.each(response,
                        function (index, value) {
                            var totalProduct = value.products.length;
                            $.each(value.products,
                                function (index1, value1) {
                                    objTr += '<tr>';
                                    if (index1 === 0) {
                                        objTr += '<td class="align-middle" rowspan="' + totalProduct + '">' + value.salonName + '</td>' +
                                            '<td class="align-middle" rowspan="' + totalProduct + '">' + value.department + '</td>' +
                                            '<td class="align-middle" rowspan="' + totalProduct + '">' + value.fullName + '</td>' +
                                            '<td>' + value1.productName + '</td>' +
                                            '<td>' + addCommas(value1.volume) + '</td>' +
                                            '<td>' + addCommas(value1.tonDau * value1.volume) + '</td>' +
                                            '<td>' + round(value1.tonDau * value1.volume / value1.quantify, 2) + '</td>' +
                                            '<td>' + value1.import + '</td>' +
                                            '<td>' + addCommas(value1.export + value1.sell + value1.usedInService) + '</td>' +
                                            '<td>' + addCommas(value1.end * value1.volume + value1.volumeRemain) + '</td>' +
                                            '<td>' + round((value1.end * value1.volume + value1.volumeRemain) / value1.quantify, 2) + '</td>' +
                                            '<td>' + value1.suggestOrder + '</td>';

                                    } else {
                                        objTr += '<td class="d-none">' + value.salonName + '</td>' +
                                            '<td class="d-none">' + value.department + '</td>' +
                                            '<td class="d-none">' + value.fullName + '</td>' +
                                            '<td>' + value1.productName + '</td>' +
                                            '<td>' + addCommas(value1.volume) + '</td>' +
                                            '<td>' + addCommas(value1.tonDau * value1.volume) + '</td>' +
                                            '<td>' + round(value1.tonDau * value1.volume / value1.quantify, 2) + '</td>' +
                                            '<td>' + value1.import + '</td>' +
                                            '<td>' + addCommas(value1.export + value1.sell + value1.usedInService) + '</td>' +
                                            '<td>' + addCommas(value1.end * value1.volume + value1.volumeRemain) + '</td>' +
                                            '<td>' + round((value1.end * value1.volume + value1.volumeRemain) / value1.quantify, 2) + '</td>' +
                                            '<td>' + value1.suggestOrder + '</td>';
                                    }
                                    objTr += '</tr>';
                                });
                        });
                    $("#tableListing tbody").append(objTr);
                    ConfigTableListing();
                }
                finishLoading();
            }

            // ConfigTableListing
            function ConfigTableListing() {
                var table = $('#tableListing').DataTable(
                    {
                        ordering: false,
                        searching: false,
                        info: false,
                        iDisplayLength: 100,
                        language: {
                            "lengthMenu": "Dòng / trang _MENU_",
                        },
                        lengthMenu: [[10, 50, 100, 100000], [10, 50, 100, 100000]]
                    });
            }


        </script>
    </asp:Panel>
</asp:Content>
