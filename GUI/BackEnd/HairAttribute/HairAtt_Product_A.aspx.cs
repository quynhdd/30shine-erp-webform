﻿using _30shine.Helpers;
using _30shine.MODEL.ENTITY.EDMX;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.HairAttribute
{
    public partial class HairAtt_Product_A : System.Web.UI.Page
    {
        protected Hair_Attribute_Product OBJ;
        protected string _Code;
        protected bool _IsUpdate = false;
        public UIHelpers _UIHelpers = new UIHelpers();
        protected cls_media Thumb = new cls_media();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BillProduct();
                FillLoaiTocByParentId(1, ddlMatDo, "Chọn Mật độ tóc...");
                FillLoaiTocByParentId(2, ddlDoCung, "Chọn Độ cứng tóc...");
                FillLoaiTocByParentId(7, ddlHuTon, "Chọn Hư tổn...");
                FillLoaiTocByParentId(8, ddlDaDau, "Chọn Da đầu...");
                if (IsUpdate())
                {
                    if (Bind_OBJ())
                    {
                    }
                }
            }
        }

        //List<CartItem> _LstCart = (List<CartItem>)JsonConvert.DeserializeObject(_CartItem, typeof(List<CartItem>)); {"Id": 1, "Name": "biofractal"}

        protected void BillProduct()
        {
            using (var db = new Solution_30shineEntities())
            {
                var _Lst = db.Products.Where(a => a.IsDelete == 0 && a.Publish == 1).ToList();

                lsbSanPham.DataTextField = "Name";
                lsbSanPham.DataValueField = "Id";
                lsbSanPham.DataSource = _Lst;
                lsbSanPham.DataBind();

            }
        }

        protected void ExcAddOrUpdate(object sender, EventArgs e)
        {
            if (IsUpdate())
            {
                Update();
            }
            else
            {
                Add();
            }
        }

        private void Add()
        {
            using (var db = new Solution_30shineEntities())
            {
                var obj = new Hair_Attribute_Product();
                var obj_Att = new Temp_Hair_Att();
                string _Hair_Att_Ids = "";
                string _Hair_Att_Ids_Json = "";

                obj_Att.MatDo = Convert.ToInt32(ddlMatDo.SelectedValue);
                obj_Att.DoCung = Convert.ToInt32(ddlDoCung.SelectedValue);
                obj_Att.HuTon = Convert.ToInt32(ddlHuTon.SelectedValue);
                obj_Att.DaDau = Convert.ToInt32(ddlDaDau.SelectedValue);

                if (obj_Att.MatDo == 0 && obj_Att.DoCung == 0 && obj_Att.HuTon == 0 && obj_Att.DaDau == 0)
                {
                    MsgSystem.Text = "Bạn chưa chọn thuộc tính nào cho tổ hợp.";
                    MsgSystem.CssClass = "msg-system warning";

                    //var msg = "Bạn chưa chọn thuộc tính nào cho tổ hợp.";
                    //var status = "warning";
                    //UIHelpers.TriggerJsMsgSystem(Page, msg, status, 50000);
                    return;
                }

                _Hair_Att_Ids_Json = JsonConvert.SerializeObject(obj_Att);

                _Hair_Att_Ids += obj_Att.MatDo.ToString() + ",";
                _Hair_Att_Ids += obj_Att.DoCung.ToString() + ",";
                _Hair_Att_Ids += obj_Att.HuTon.ToString() + ",";
                _Hair_Att_Ids += obj_Att.DaDau.ToString();

                obj.Hair_AttributeId = _Hair_Att_Ids;
                obj.Hair_AttributeId_Json = _Hair_Att_Ids_Json;

                string _ProductIds = "";
                foreach (ListItem item in lsbSanPham.Items)
                {
                    if (item.Selected == true)
                    {
                        _ProductIds += item.Value + ",";
                    }
                }
                if (_ProductIds.EndsWith(","))
                    _ProductIds = _ProductIds.Remove(_ProductIds.Length - 1, 1);

                obj.ProductId = _ProductIds;
                obj.IsDelete = false;
                obj.Description = txtMota.Text;
                obj.Title = txtLoiKhuyen.Text;

                // Validate
                var Error = false;

                if (!Error)
                {
                    db.Hair_Attribute_Product.Add(obj);
                    var exc = db.SaveChanges();

                    if (exc > 0)
                    {
                        var MsgParam = new List<KeyValuePair<string, string>>();
                        MsgParam.Add(new KeyValuePair<string, string>("msg_add_status", "success"));
                        MsgParam.Add(new KeyValuePair<string, string>("msg_add_message", "Cập nhật thành công!"));
                        UIHelpers.Redirect("/admin/to-hop-toc/danh-sach.html", MsgParam);
                    }
                    else
                    {
                        var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển.";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                    }
                }
            }
        }

        private void Update()
        {
            using (var db = new Solution_30shineEntities())
            {
                int Id;
                //int integer;
                if (int.TryParse(_Code, out Id))
                {
                    OBJ = db.Hair_Attribute_Product.FirstOrDefault(w => w.Id == Id);

                    if (!OBJ.Equals(null))
                    {

                        var obj_Att = new Temp_Hair_Att();
                        string _Hair_Att_Ids = "";
                        string _Hair_Att_Ids_Json = "";

                        obj_Att.MatDo = Convert.ToInt32(ddlMatDo.SelectedValue);
                        obj_Att.DoCung = Convert.ToInt32(ddlDoCung.SelectedValue);
                        obj_Att.HuTon = Convert.ToInt32(ddlHuTon.SelectedValue);
                        obj_Att.DaDau = Convert.ToInt32(ddlDaDau.SelectedValue);
                        _Hair_Att_Ids_Json = JsonConvert.SerializeObject(obj_Att);


                        _Hair_Att_Ids += obj_Att.MatDo.ToString() + ",";
                        _Hair_Att_Ids += obj_Att.DoCung.ToString() + ",";
                        _Hair_Att_Ids += obj_Att.HuTon.ToString() + ",";
                        _Hair_Att_Ids += obj_Att.DaDau.ToString();

                        OBJ.Hair_AttributeId = _Hair_Att_Ids;
                        OBJ.Hair_AttributeId_Json = _Hair_Att_Ids_Json;

                        string _ProductIds = "";
                        foreach (ListItem item in lsbSanPham.Items)
                        {
                            if (item.Selected == true)
                            {
                                _ProductIds += item.Value + ",";
                            }
                        }
                        if (_ProductIds.EndsWith(","))
                            _ProductIds = _ProductIds.Remove(_ProductIds.Length - 1, 1);
                        OBJ.ProductId = _ProductIds;
                        OBJ.IsDelete = false;
                        OBJ.Description = txtMota.Text;
                        OBJ.Title = txtLoiKhuyen.Text;

                        db.Hair_Attribute_Product.AddOrUpdate(OBJ);
                        var exc = db.SaveChanges();
                        if (exc > 0)
                        {
                            var MsgParam = new List<KeyValuePair<string, string>>();
                            MsgParam.Add(new KeyValuePair<string, string>("msg_add_status", "success"));
                            MsgParam.Add(new KeyValuePair<string, string>("msg_add_message", "Cập nhật thành công!"));
                            UIHelpers.Redirect("/admin/to-hop-toc/" + OBJ.Id + ".html", MsgParam);
                        }
                        else
                        {
                            var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển!";
                            var status = "warning";
                            UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                        }
                    }
                    else
                    {
                        var msg = "Lỗi! Không tìm thấy danh mục.";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);

                    }
                }
                else
                {
                    MsgSystem.Text = "Lỗi! Không tìm thấy danh mục.";
                    MsgSystem.CssClass = "msg-system warning";
                }

                Context.RewritePath("+", "", "");
            }
        }

        private bool Bind_OBJ()
        {
            using (var db = new Solution_30shineEntities())
            {
                var ExistOBJ = true;
                int Id;
                if (int.TryParse(_Code, out Id))
                {
                    OBJ = db.Hair_Attribute_Product.Where(w => w.Id == Id).FirstOrDefault();

                    if (!OBJ.Equals(null))
                    {
                        var obj_Att = new Temp_Hair_Att();

                        if (!string.IsNullOrEmpty(OBJ.Hair_AttributeId_Json))
                        {
                            obj_Att = JsonConvert.DeserializeObject<Temp_Hair_Att>(OBJ.Hair_AttributeId_Json);
                            if (obj_Att != null)
                            {
                                var _ItemMatDo = ddlMatDo.Items.FindByValue(obj_Att.MatDo.ToString());
                                if (_ItemMatDo != null)
                                    _ItemMatDo.Selected = true;

                                var _ItemDoCung = ddlDoCung.Items.FindByValue(obj_Att.DoCung.ToString());
                                if (_ItemDoCung != null)
                                    _ItemDoCung.Selected = true;

                                var _ItemHuTon = ddlHuTon.Items.FindByValue(obj_Att.HuTon.ToString());
                                if (_ItemHuTon != null)
                                    _ItemHuTon.Selected = true;

                                var _ItemDaDau = ddlDaDau.Items.FindByValue(obj_Att.DaDau.ToString());
                                if (_ItemDaDau != null)
                                    _ItemDaDau.Selected = true;
                            }
                        }

                        //Set active product
                        string _ProductIds = OBJ.ProductId;
                        if (_ProductIds != "")
                        {

                            string[] a = _ProductIds.Split(',');

                            for (int i = 0; i < a.Length; i++)
                            {
                                if (a[i].ToString() != "" && a[i].ToString() != "")
                                    lsbSanPham.Items.FindByValue(a[i].ToString()).Selected = true;
                            }
                        }

                        txtMota.Text = OBJ.Description;
                        txtLoiKhuyen.Text = OBJ.Title;

                    }
                    else
                    {
                        ExistOBJ = false;
                        var msg = "Lỗi! Danh mục không tồn tại";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                    }
                }
                else
                {
                    ExistOBJ = false;
                    var msg = "Lỗi! Danh mục không tồn tại";
                    var status = "warning";
                    UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                }

                return ExistOBJ;
            }
        }

        public void ReloadPage()
        {
            HttpRequest Request = HttpContext.Current.Request;
            string host = "http://" + Request.Url.Host.ToString();
            string rawUrl = Request.RawUrl.ToString();
            string Url = host + rawUrl;
            Response.Redirect(Url);
        }

        private bool IsUpdate()
        {
            _Code = Request.QueryString["Code"];
            if (_Code != null)
            {
                _IsUpdate = true;
                return true;
            }
            else
            {
                _IsUpdate = false;
                return false;
            }
        }

        public class cls_media
        {
            public string url { get; set; }
            public string title { get; set; }
            public string description { get; set; }
        }

        protected void Pid_SelectedIndexChanged(object sender, EventArgs e)
        {
            //FillLoaiTocByParentId();
        }

        protected void FillLoaiTocByParentId(int _ParentId, DropDownList ddl, string _DefaultName)
        {
            using (var db = new Solution_30shineEntities())
            {
                // int _PId = Convert.ToInt32(Pid.SelectedValue);

                var LST = db.Hair_Attribute.Where(w => w.IsDelete != true && w.ParentId == _ParentId && w.ParentId != 0).ToList();
                var Key = 0;
                var Count = LST.Count;

                ddl.Items.Clear();

                ddl.DataTextField = "CategoryName";
                ddl.DataValueField = "Id";
                ListItem item = new ListItem(_DefaultName, "0");
                ddl.Items.Insert(Key, item);
                Key++;

                if (Count > 0)
                {
                    foreach (var v in LST)
                    {
                        item = new ListItem(v.HairTypeName, v.Id.ToString());
                        ddl.Items.Insert(Key, item);
                        Key++;
                    }
                }

                //if (_IsUpdate)
                //{
                //    int _Hair_Att_Id = Convert.ToInt32(OBJ.Hair_AttributeId);
                //    var obj_Hair_Att = db.Hair_Attribute.Find(_Hair_Att_Id);
                //    if (obj_Hair_Att != null)
                //    {

                //        var Item_Parent_Selected = Pid.Items.FindByValue(obj_Hair_Att.ParentId.ToString());
                //        if (Item_Parent_Selected != null)
                //            Item_Parent_Selected.Selected = true;
                //    }

                //    var ItemSelected = ddlLoaiToc.Items.FindByValue(OBJ.Hair_AttributeId.ToString());
                //    if (ItemSelected != null)
                //        ItemSelected.Selected = true;
                //}
                //else
                //{
                //    ddlLoaiToc.SelectedIndex = 0;
                //}
            }
            //ScriptManager.RegisterStartupScript(this, GetType(), "key", "ResetSelect();", true);
        }

        protected class Temp_Hair_Att
        {
            public int MatDo { get; set; }
            public int DoCung { get; set; }
            public int HuTon { get; set; }
            public int DaDau { get; set; }
        }
    }
}