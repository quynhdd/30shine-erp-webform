﻿using _30shine.MODEL.ENTITY.EDMX;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Globalization;
using System.Linq;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.Helpers;
using _30shine.MODEL.BO;
using System.Web.Services;
using System.Collections;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;

namespace _30shine.GUI.BackEnd.Quanlity
{
    public partial class Staff_Mistake_Add_V2 : System.Web.UI.Page
    {
        private string PageID = "";
        Solution_30shineEntities db = new Solution_30shineEntities();
        public Salary_Income_Change OBJ;
        protected string _Code;
        protected bool HasImages = false;
        protected List<string> ListImagesUrl = new List<string>();
        protected string[] ListImagesName;
        protected bool isRoot = false;
        private bool Perm_Access = false;
        protected bool Perm_ShowSalon = false;
        protected bool IsAccountant = false;
        private bool Perm_ViewAllData = false;
        protected bool _IsUpdate = false;
        CultureInfo culture = new CultureInfo("vi-VN");

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (!IsPostBack)
            {
                txtCreateDate.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
                Library.Function.bindSalon(new List<DropDownList> { ddlSalon }, Perm_ViewAllData);
                BindSaff();
                BindFormMisstake();
                if (IsUpdate())
                {
                    BindOBJ();
                    txtCreateDate.Enabled = false;
                    txtCreateDate.ReadOnly = true;
                    ddlSalon.Enabled = false;
                    ddlStaff.Enabled = false;
                }
            }
        }

        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //if (Request.QueryString["Code"] != null)
                //{
                //    PageID = "STAFF_MISTAKE_V2_EDIT";
                //}
                //else
                //{
                //    PageID = "STAFF_MISTAKE_V2_ADD";
                //}
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        /// <summary>
        /// IsUpdate (check Id record )
        /// </summary>
        /// <returns></returns>
        private bool IsUpdate()
        {
            _Code = Request.QueryString["Code"];
            if (_Code != null)
            {
                _IsUpdate = true;
                return true;
            }
            else
            {
                _IsUpdate = false;
                return false;
            }
        }
        /// <summary>
        /// Bind hình thức các lỗi
        /// </summary>
        private void BindFormMisstake()
        {
            using (var db = new Solution_30shineEntities())
            {
                var list = db.tbl_Form_Misstake.Where(w => w.isDelete == false && w.isPuslish == false).ToList();
                if (list.Count > 0)
                {
                    ddlMisstake.Items.Clear();
                    ddlMisstake.Items.Add(new ListItem("Chọn hình thức", "0"));
                    for (int i = 0; i < list.Count; i++)
                    {
                        ddlMisstake.Items.Add(new ListItem(list[i].Formality, list[i].Id.ToString()));
                    }
                    ddlMisstake.DataBind();
                }
            }
        }
        /// <summary>
        /// Bind data to DropDowlist Staff
        /// </summary>
        private void BindSaff()
        {
            using (var db = new Solution_30shineEntities())
            {
                int interger;
                var salon = int.TryParse(ddlSalon.SelectedValue, out interger) ? interger : 0;
                var staff = new List<Staff>();
                var whereSalon = "";
                var sql = "";
                if (salon > 0)
                {
                    whereSalon = " and SalonId = " + salon;
                }
                sql = @"select * from Staff 
                        where IsDelete != 1 and Active = 1 
                        and (isAccountLogin != 1 or isAccountLogin is null)" + whereSalon;
                staff = db.Staffs.SqlQuery(sql).ToList();

                var first = new Staff();
                first.Id = 0;
                first.Fullname = "Chọn nhân viên";
                staff.Insert(0, first);
                ddlStaff.DataTextField = "Fullname";
                ddlStaff.DataValueField = "Id";
                ddlStaff.DataSource = staff;
                ddlStaff.DataBind();
            }
        }
        /// <summary>
        /// Bind Obj
        /// </summary>
        private void BindOBJ()
        {
            try
            {
                int Id = 0;
                if (int.TryParse(_Code, out Id))
                {
                    using (var client = new HttpClient())
                    {
                        ServicePointManager.Expect100Continue = true;
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                        client.BaseAddress = new Uri(Libraries.AppConstants.URL_API_FINANCIAL);
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        var response = client.GetAsync("/api/salary-income-change/get/" + Id).Result;
                        if (response.IsSuccessStatusCode)
                        {
                            var result = response.Content.ReadAsStringAsync().Result;
                            var serializer = new JavaScriptSerializer();
                            var responeData = serializer.Deserialize<Origin>(result).data;
                            var obj = responeData;
                            ddlMisstake.SelectedValue = obj.changedTypeId.ToString();
                            ddlStaff.SelectedValue = obj.staffId.ToString();
                            ddlSalon.SelectedValue = obj.salonId.ToString();
                            HDF_MainImg.Value = obj.Images ?? "";
                            txtMoney.Text = (Math.Abs(obj.Point.Value)).ToString();
                            txtCreateDate.Text = obj.WorkDate.ToString();
                            txtNote.Text = obj.description;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// ddlSalon_SelectedIndexChanged
        /// </summary>
        protected void ddlSalon_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindSaff();
        }

        protected void ExcAddOrUpdate(object sender, EventArgs e)
        {
            AddLoading();
            if (IsUpdate())
            {
                Update();
            }
            else
            {
                Add();
            }
        }

        private void Update()
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    int Id = 0;
                    int integer;
                    if (int.TryParse(_Code, out Id))
                    {
                        var salaryIncome = new Salary_Income_Change();
                        salaryIncome.id = Id;
                        salaryIncome.staffId = int.TryParse(ddlStaff.SelectedValue, out integer) ? integer : 0;
                        salaryIncome.salonId = int.TryParse(ddlSalon.SelectedValue, out integer) ? integer : 0;
                        salaryIncome.changedTypeId = int.TryParse(ddlMisstake.SelectedValue, out integer) ? integer : 0;
                        try
                        {
                            var formMisstake = db.tbl_Form_Misstake.FirstOrDefault(f => f.Id == salaryIncome.changedTypeId);
                            salaryIncome.changedType = formMisstake.Slug;
                        }
                        catch
                        {
                            salaryIncome.changedType = "";
                        }
                        salaryIncome.description = txtNote.Text;
                        if (HDF_MainImg.Value != "")
                            salaryIncome.Images = HDF_MainImg.Value;
                        else
                            salaryIncome.Images = "";
                        if (salaryIncome.changedTypeId != 7)
                        {
                            salaryIncome.Point = (int.TryParse(txtMoney.Text, out integer) ? integer : 0) * -1;
                        }
                        else { salaryIncome.Point = int.TryParse(txtMoney.Text, out integer) ? integer : 0; }
                        salaryIncome.WorkDate = Convert.ToDateTime(txtCreateDate.Text, culture);
                        salaryIncome.IsDeleted = false;
                        var callApi = PushNotificationUpdate(salaryIncome);
                    }
                    else
                    {
                        MsgSystem.Text = "Lỗi! Bản ghi không tồn tại.";
                        MsgSystem.CssClass = "msg-system warning";
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        /// <summary>
        /// Add
        /// </summary>
        private void Add()
        {
            string status = "";
            string msg = "";
            try
            {
                int integer;
                var salaryIncome = new Salary_Income_Change();
                salaryIncome.staffId = int.TryParse(ddlStaff.SelectedValue, out integer) ? integer : 0;
                salaryIncome.salonId = int.TryParse(ddlSalon.SelectedValue, out integer) ? integer : 0;
                salaryIncome.changedTypeId = int.TryParse(ddlMisstake.SelectedValue, out integer) ? integer : 0;
                try
                {
                    var formMisstake = db.tbl_Form_Misstake.FirstOrDefault(f => f.Id == salaryIncome.changedTypeId);
                    salaryIncome.changedType = formMisstake.Slug;
                }
                catch
                {
                    salaryIncome.changedType = "";
                }
                salaryIncome.description = txtNote.Text;
                if (HDF_MainImg.Value != "")
                    salaryIncome.Images = HDF_MainImg.Value;
                else
                    salaryIncome.Images = "";
                if (salaryIncome.changedTypeId != 7)
                {
                    salaryIncome.Point = (int.TryParse(txtMoney.Text, out integer) ? integer : 0) * -1;
                }
                else { salaryIncome.Point = int.TryParse(txtMoney.Text, out integer) ? integer : 0; }
                salaryIncome.WorkDate = Convert.ToDateTime(txtCreateDate.Text, culture);
                salaryIncome.CreatedTime = DateTime.Now;
                salaryIncome.IsDeleted = false;
                var callApi = PushNotificationInsert(salaryIncome);
            }
            catch
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                msg = "Thêm mới thất bại!!";
                status = "msg-system warning";
                UIHelpers.TriggerJsMsgSystem(Page, msg, status, 2000);
                MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Thêm mới thất bại!"));
            }
        }

        /// <summary>
        /// check permission
        /// </summary>
        private void CheckPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                string[] Allow = new string[] { "root", "admin" };
                string[] AllowViewAllDate = new string[] { "root", "admin" };
                string[] Root = new string[] { "root" };
                var Permission = Session["User_Permission"].ToString().Trim();
                if (Array.IndexOf(Allow, Permission) != -1)
                {
                    Perm_Access = true;
                }

                if (Array.IndexOf(AllowViewAllDate, Permission) != -1)
                {
                    Perm_ViewAllData = true;
                }
                if (Array.IndexOf(Root, Permission) != -1)
                {
                    isRoot = true;
                }
            }

            // Call execute function
            ExecuteByPermission();
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        public async Task PushNotificationInsert(Salary_Income_Change salaryIncome)
        {
            var msg = "";
            var status = "";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Libraries.AppConstants.URL_API_FINANCIAL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var Result = client.PostAsJsonAsync("/api/salary-income-change", salaryIncome).Result;
                    if (Result.IsSuccessStatusCode)
                    {
                        var result = Result.Content.ReadAsStringAsync().Result;
                        JavaScriptSerializer serializer = new JavaScriptSerializer();
                        var responseData = serializer.Deserialize<ResponseData>(result);
                        if (responseData.status == 1)
                        {
                            var MsgParam = new List<KeyValuePair<string, string>>();
                            msg = "Thêm mới thành công!!";
                            status = "msg-system warning";
                            UIHelpers.TriggerJsMsgSystem(Page, msg, status, 2000);
                            MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                            MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Thêm mới thành công"));
                            UIHelpers.Redirect("/admin/thuong-phat/danh-sach.html", MsgParam);
                        }
                        else
                        {
                            var MsgParam = new List<KeyValuePair<string, string>>();
                            msg = "Thêm mới thất bại!!";
                            status = "msg-system warning";
                            UIHelpers.TriggerJsMsgSystem(Page, msg, status, 2000);
                            MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                            MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Thêm mới thất bại!"));
                        }

                    }
                    else
                    {
                        var MsgParam = new List<KeyValuePair<string, string>>();
                        msg = "Thêm mới thất bại!!";
                        status = "msg-system warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 2000);
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Thêm mới thất bại!"));
                    }
                }
            }
            catch
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                msg = "Thêm mới thất bại!!";
                status = "msg-system warning";
                UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Thêm mới thất bại!"));
            }
        }
        public async Task PushNotificationUpdate(Salary_Income_Change salaryIncome)
        {
            var msg = "";
            var status = "";
            try
            {
                using (var client = new HttpClient())
                {
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    client.BaseAddress = new Uri(Libraries.AppConstants.URL_API_FINANCIAL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var Result = client.PutAsJsonAsync("/api/salary-income-change", salaryIncome).Result;
                    if (Result.IsSuccessStatusCode)
                    {
                        var result = Result.Content.ReadAsStringAsync().Result;
                        JavaScriptSerializer serializer = new JavaScriptSerializer();
                        var responseData = serializer.Deserialize<ResponseData>(result);
                        if (responseData.status == 1)
                        {
                            var MsgParam = new List<KeyValuePair<string, string>>();
                            msg = "Thêm mới thành công!!";
                            status = "msg-system warning";
                            UIHelpers.TriggerJsMsgSystem(Page, msg, status, 2000);
                            MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                            MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thành công"));
                            UIHelpers.Redirect("/admin/thuong-phat/danh-sach.html", MsgParam);
                        }
                        else
                        {
                            var MsgParam = new List<KeyValuePair<string, string>>();
                            msg = "Thêm mới thất bại!!";
                            status = "msg-system warning";
                            UIHelpers.TriggerJsMsgSystem(Page, msg, status, 2000);
                            MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                            MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thất bại!"));
                        }
                    }
                    else
                    {
                        var MsgParam = new List<KeyValuePair<string, string>>();
                        msg = "Cập nhật thất bại!!";
                        status = "msg-system warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 2000);
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thất bại!"));
                    }
                }
            }
            catch
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                msg = "Cập nhật thất bại!!";
                status = "msg-system warning";
                UIHelpers.TriggerJsMsgSystem(Page, msg, status, 2000);
                MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thất bại!"));
            }
        }

        public void AddLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "addLoading(); console.log('addLoading...');", true);
        }
    }


    public class ResponseData
    {
        public int status { get; set; }
        public string message { get; set; }
        public List<Salary_Income_Change_V2> data { get; set; }
    }


    public class Origin
    {
        public int status { get; set; }
        public string message { get; set; }
        public Salary_Income_Change data { get; set; }
    }
    public partial class Salary_Income_Change
    {
        public int id { get; set; }
        public Nullable<int> staffId { get; set; }
        public Nullable<int> salonId { get; set; }
        public string changedType { get; set; }
        public Nullable<int> changedTypeId { get; set; }
        public string description { get; set; }
        public string Images { get; set; }
        public Nullable<int> Point { get; set; }
        public int? scoreFactor { get; set; }
        public DateTime? WorkDate { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? ModifiedTime { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Guid? Uid { get; set; }
        public byte? migrateStatus { get; set; }
        public Salary_Income_Change()
        {
            migrateStatus = 0;
            scoreFactor = 0;
            //ModifiedTime = new DateTime();
        }
    }

    public partial class Salary_Income_Change_V2
    {
        public int id { get; set; }
        public Nullable<int> staffId { get; set; }
        public Nullable<int> salonId { get; set; }
        public string changedType { get; set; }
        public Nullable<int> changedTypeId { get; set; }
        public string description { get; set; }
        public string Images { get; set; }
        public Nullable<int> Point { get; set; }
        public int? scoreFactor { get; set; }
        public string WorkDate { get; set; }
        public string fullName { get; set; }
        public string changeName { get; set; }
        public string CreatedTime { get; set; }
        public string ModifiedTime { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
    }
}