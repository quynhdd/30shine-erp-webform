﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Staff_Mistake_Add_V2.aspx.cs" Inherits="_30shine.GUI.BackEnd.Quanlity.Staff_Mistake_Add_V2" %>
<asp:Content ID="ProductAdd" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <style>
            #Send {
                width: 80px !important;
                height: 30px;
                line-height: 30px;
                float: left;
                width: auto;
                background: #000000;
                color: #ffffff;
                text-align: center;
                cursor: pointer;
                -webkit-border-radius: 2px;
                -moz-border-radius: 2px;
                border-radius: 2px;
                margin-left: 204px;
                margin-top: 30px;
            }

            #success-alert {
                top: 100px;
                right: 10px;
                position: fixed;
                width: 20% !important;
                z-index: 2000;
            }

            .customer-add .table-add tr.tr-field-ahalf input, .customer-add .table-add tr.tr-field-ahalf textarea, .customer-add .table-add tr.tr-field-ahalf select {
                width: 100% !important;
            }
        </style>

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="Ul1">
                        <li>Phạt &nbsp;&#187; </li>
                        <li class="li-listing"><a href="/admin/thuong-phat/danh-sach.html">Danh sách</a></li>
                        <li class="li-add active"><a href="/admin/thuong-phat/them-moi.html">Thêm mới</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div class="alert alert-danger" id="success-alert">
            <span id="msg-alert"></span>
        </div>
        <div class="wp customer-add admin-product-add">
            <%-- Add --%>
            <div class="wp960 content-wp">
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <div class="table-wp">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <table class="table-add admin-product-table-add">
                                <tbody>
                                    <tr style="margin-bottom:10px;" class="title-head">
                                        <td><strong>Thông tin Phạt</strong></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr class="tr-field-ahalf">
                                        <td class="col-xs-3 left"><span>Salon</span></td>
                                        <td class="col-xs-9 right">
                                            <span class="field-wp">
                                                <asp:DropDownList onkeyup="checkColor()" ID="ddlSalon" CssClass="form-control select" runat="server" ClientIDMode="Static" AutoPostBack="true" OnSelectedIndexChanged="ddlSalon_SelectedIndexChanged"></asp:DropDownList>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-xs-3 left"><span>Chọn nhân viên</span></td>
                                        <td class="col-xs-9 right">
                                            <span class="field-wp">
                                                <asp:DropDownList onkeyup="checkColor()" ID="ddlStaff" style="width:55% !important;" CssClass="form-control" runat="server" ClientIDMode="Static">
                                                </asp:DropDownList>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr class="tr-field-ahalf">
                                        <td class="col-xs-3 left"><span>Hình thức</span></td>
                                        <td class="col-xs-9 right">
                                            <span class="field-wp">
                                                <asp:DropDownList onkeyup="checkColor()" ID="ddlMisstake" style="width:55% !important;" CssClass="form-control" runat="server" ClientIDMode="Static">
                                                </asp:DropDownList>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr class="tr-field-ahalf" id="price">
                                        <td class="col-xs-3 left"><span>Số tiền</span></td>
                                        <td class="col-xs-9 right">
                                            <span class="field-wp">
                                                <asp:TextBox  onkeyup="checkColor()" ID="txtMoney" style="width:55% !important;" CssClass="form-control" onkeypress="return ValidateKeypress(/\d/,event);"  runat="server" ClientIDMode="Static" placeholder="vd: 100000"></asp:TextBox>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr class="tr-upload">
                                        <td class="col-xs-3 left"><span>Ảnh</span></td>
                                        <td class="col-xs-7 right">
                                            <div class="wrap btn-upload-wp HDF_MainImg" style="margin-bottom: 5px; width: auto;width:55% !important;">
                                        <div id="Btn_UploadImg" class="btn-upload-img" onclick="popupImageIframe('HDF_MainImg')">Ảnh</div>
                                        <asp:FileUpload ID="UploadFile" ClientIDMode="Static" Style="display: none;"
                                            CssClass="input-file-upload" runat="server" AllowMultiple="true" />
                                        <div class="wrap listing-img-upload">
                                            <% if (OBJ != null && OBJ.Images != null && OBJ.Images != "")
                                                { %>
                                            <div class="thumb-wp">
                                                <img style="width:100% !important; height:100% !important" class="thumb" alt="" title="" src="<%=OBJ.Images %>"
                                                    data-img="<%=OBJ.Images %>"/>
                                                <span class="delete-thumb" onclick="deleteThumbnail($(this), 'HDF_MainImg')"></span>
                                            </div>
                                            <% } %>
                                        </div>
                                    </div>
                                        </td>
                                    </tr>
                                    <tr class="tr-field-ahalf">
                                        <td class="col-xs-3 left"><span>Ngày tạo</span></td>
                                        <td class="col-xs-9 right">
                                            <span class="field-wp">
                                                <asp:TextBox onkeyup="checkColor()" CssClass="form-control" style="width:55% !important;" ID="txtCreateDate" placeholder="Từ ngày" ClientIDMode="Static" runat="server"></asp:TextBox>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr class="tr-field-ahalf">
                                        <td class="col-xs-3 left"><span>Ghi chú(loại lỗi)</span></td>
                                        <td class="col-xs-9 right">
                                            <span class="field-wp">
                                                <asp:TextBox ID="txtNote" runat="server" style="width:55% !important;" CssClass="form-control" onkeyup="checkColor()" TextMode="multiline" Rows="5" ClientIDMode="Static"></asp:TextBox>
                                            </span>
                                        </td>
                                    </tr>
                                     <tr class="tr-field-ahalf"">
                                        <td class="col-xs-3 left"></td>
                                        <td class="col-xs-9 right">
                                            <span class="field-wp">
                                                <asp:Button ID="Send" ClientIDMode="Static" style="margin-top: 90px;float: left; margin-left: 0px;" runat="server" Text="Hoàn Tất"  OnClick="ExcAddOrUpdate" OnClientClick="if(!ExcAddOrUpdate()){ return false;}else{addLoading();}"/>
                                            </span>
                                        </td>
                                    </tr>
                                    <asp:HiddenField runat="server" ID="UrlKeyField" ClientIDMode="Static" />
                                    <asp:HiddenField runat="server" ID="ImgCoverField" ClientIDMode="Static" />
                                    <asp:HiddenField runat="server" ID="HDF_MainImg" ClientIDMode="Static" />
                                    <asp:HiddenField ID="HiddenField1" runat="server" />
                                </tbody>
                            </table>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ddlSalon" EventName="SelectedIndexChanged" />
                            <asp:AsyncPostBackTrigger ControlID="Send" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
            <%-- end Add --%>
        </div>
          <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 55% !important;
                margin-top: 5px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }
        </style>
        <script>
            jQuery(document).ready(function () {
                $("#glbQuanlity").addClass("active");
                $("#glbMistake").addClass("active");
                $("#subMenu").addClass("active");
                $('#txtCreateDate').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) {
                        //$input.val($input.val().split(' ')[0]);
                    },
                    scrollMonth: false,
                    scrollTime: false,
                    scrollInput: false
                });
            });
        </script>

        <script type="text/javascript">
            $("#success-alert").hide();
            //============================
            // Datepicker
            //============================
            function checkColor(This) {
                $("#txtNote").css("border-color", "#ddd");
                $("#txtMoney").css("border-color", "#ddd");
                $("#ddlSalon").css("border-color", "#ddd");
                $("#ddlStaff").css("border-color", "#ddd");
                $("#ddlMisstake").css("border-color", "#ddd");
            }
            function ExcAddOrUpdate(This) {
                var kt = true;
                var error = 'Vui lòng nhập đầy đủ thông tin!';
                var money = $('#txtMoney').val();
                var salon = $('#ddlSalon :selected').val();
                var staff = $('#ddlStaff :selected').val();
                var note = $('#txtNote').val();
                var mistake = $('#ddlMisstake :selected').val();
                if (mistake != "6") {
                    if (money == "") {
                        kt = false;
                        error += " [Số tiền] ";
                        $("#txtMoney").css("border-color", "red");
                        $("#success-alert").show();
                    }
                    else {
                        $("#txtMoney").css("border-color", "#ddd");
                    }
                }
                else {
                    $("#txtMoney").css("border-color", "#ddd");
                }
                if (note == "") {
                    kt = false;
                    error += " [Ghi chú] ";
                    $("#txtNote").css("border-color", "red");
                    $("#success-alert").show();
                }
                else {
                    $("#txtNote").css("border-color", "#ddd");
                }
                if (salon == "" || salon == 0) {
                    kt = false;
                    error += "[Salon] ";
                    $("#ddlSalon").css("border-color", "red");
                    $("#success-alert").show();
                }
                else {
                    $("#ddlSalon").css("border-color", "#ddd");
                }
                if (staff == "" || staff == 0) {
                    kt = false;
                    error += "[Nhân viên!], ";
                    $("#ddlStaff").css("border-color", "red");
                    $("#success-alert").show();
                }
                else {
                    $("#ddlStaff").css("border-color", "#ddd");
                }
                if (mistake == "" || mistake == 0) {
                    kt = false;
                    error += "[Hình thức!], ";
                    $("#ddlMisstake").css("border-color", "red");
                    $("#success-alert").show();
                }
                else {
                    $("#ddlMisstake").css("border-color", "#ddd");
                }
                if (!kt) {
                    $("#success-alert").fadeTo(2000, 2000).slideUp(2000, function () {
                        $("#success-alert").slideUp(2000);
                    });
                    $("#msg-alert").text(error);
                }
                return kt;
            }

            ///Input is only number
            function ValidateKeypress(numcheck, e) {
                var keynum, keychar, numcheck;
                if (window.event) {
                    keynum = e.keyCode;
                }
                else if (e.which) {
                    keynum = e.which;
                }
                if (keynum == 8 || keynum == 127 || keynum == null || keynum == 9 || keynum == 0 || keynum == 13) return true;
                keychar = String.fromCharCode(keynum);
                var result = numcheck.test(keychar);
                return result;
            }
            function popupImageIframe(StoreImgField) {
                var Width = 960;
                var Height = 560;
                var ImgList = [];
                var iframe = $("<div class='iframe-image-upload' style='width:" + Width + "px; height:" + Height + "px; float:left; display:none;'><iframe src='/GUI/BackEnd/UploadImage/Upload_V2.aspx?Code=img&StoreImgField=" + StoreImgField + "&callback=getThumbFromIframe&imgWidth=800&imgHeight=600&multi_img=false&Folder=Common' style='width:100%; height: 100%;'></iframe></div");

                $("body").find(".iframe-image-upload").remove().end().append(iframe).find(".iframe-image-upload").openEBPopup();
            }

            window.getThumbFromIframe = function (Imgs, StoreImgField) {
                var imgs = "";
                var thumb = "";
                if (Imgs.length > 0) {
                    thumb = '<div class="thumb-wp">' +
                        '<img class="thumb" alt="" title="" src="' + Imgs[0] + '"' +
                        'data-img="" />' +
                        '<span class="delete-thumb" onclick="deleteThumbnail($(this), \'' + StoreImgField + '\')"></span>' +
                        '</div>';
                    $("." + StoreImgField).find(".listing-img-upload").empty().append(thumb);
                    excThumbWidth(120, 120);
                    $("#" + StoreImgField).val(Imgs[0]);
                }
                autoCloseEBPopup();
            }

            function deleteThumbnail(This, StoreImgField) {
                var imgs = "";
                var lst = This.parent().parent();
                This.parent().remove();
                lst.find("img.thumb").each(function () {
                    imgs += $(this).attr("src") + ",";
                });
                $("#" + StoreImgField).val(imgs);
            }

        </script>
    </asp:Panel>
</asp:Content>
