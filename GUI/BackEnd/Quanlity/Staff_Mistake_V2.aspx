﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Staff_Mistake_V2.aspx.cs" Inherits="_30shine.GUI.BackEnd.Quanlity.Staff_Mistake_V2" %>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">

    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>

    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Phạt &nbsp;&#187; </li>
                        <li class="li-listing active"><a href="javascript:void(0);">Danh sách</a></li>
                        <li class="li-add "><a href="/admin/thuong-phat/them-moi.html">Thêm mới</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing be-report">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <!-- Filter -->
                <div class="row">
                    <div class="filter-item" style="margin-left: 0;">
                        <strong class="st-head" style="margin-right: 10px;"><i class="fa fa-clock-o"></i>Thời gian</strong>
                        <div class="datepicker-wp">
                            <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeFrom" placeholder="Từ ngày" Style="width: 120px!important;"
                                ClientIDMode="Static" runat="server"></asp:TextBox>

                            <strong class="st-head" style="margin-left: 10px; margin-right: 10px;">
                                <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                            </strong>
                            <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeTo" placeholder="Đến ngày" Style="width: 120px!important;"
                                ClientIDMode="Static" runat="server"></asp:TextBox>
                        </div>

                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="Salon" runat="server" CssClass="form-control select" ClientIDMode="Static"></asp:DropDownList>
                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="StaffType" runat="server" CssClass="form-control" ClientIDMode="Static" Style="width: 150px;"></asp:DropDownList>

                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="ddlHinhThuc" runat="server" CssClass="form-control" ClientIDMode="Static" Style="width: 150px;"></asp:DropDownList>

                    </div>
                    <div class="filter-item" style="display: none;">
                        <asp:TextBox CssClass="st-head ip-short eb-select eb-suggestion form-control" data-field="staff.name" data-value="0"
                            AutoCompleteType="Disabled" ID="StaffName" ClientIDMode="Static" placeholder="Nhập tên Stylist/Skinner" runat="server"></asp:TextBox>
                        <div class="listing-staff-wp eb-select-data eb-suggestion-listing">
                            <ul class="ul-listing-staff ul-listing-suggestion" id="UlCustomerName">
                            </ul>
                        </div>
                    </div>

                    <asp:Panel ID="ViewData" CssClass="st-head btn-viewdata" Text="Xem dữ liệu"
                        ClientIDMode="Static" onclick="excPaging(1)" runat="server">
                        Xem dữ liệu
                    </asp:Panel>
                    <a href="/admin/diem-tru/danh-sach.html" class="st-head btn-viewdata">Reset Filter</a>
                    <div class="export-wp drop-down-list-wp" style="display: none;">
                        <asp:Panel ID="fff" CssClass="st-head btn-viewdata"
                            ClientIDMode="Static" runat="server" onclick="EntryExportExcel('all')">
                            Xuất File Excel
                        </asp:Panel>
                        <ul class="ul-drop-down-list" style="display: none;">
                            <li onclick="EntryExportExcel('all')">Tất cả dữ liệu</li>
                            <li onclick="EntryExportExcel('current page')">Trang hiện tại</li>
                        </ul>
                    </div>
                </div>
                <!-- End Filter -->
                <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
                <div class="row">
                    <strong class="st-head"><i class="fa fa-file-text"></i>Phạt</strong>
                </div>
                <!-- Row Table Filter -->
                <div class="table-func-panel" style="margin-top: -33px;">
                    <div class="table-func-elm">
                        <span>Số hàng / Page : </span>
                        <div class="table-func-input-wp">
                            <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                            <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                            <ul class="ul-opt-segment">
                                <li data-value="10">10</li>
                                <li data-value="20">20</li>
                                <li data-value="30">30</li>
                                <li data-value="40">40</li>
                                <li data-value="50">50</li>
                                <li data-value="200">200</li>
                                <%--<li data-value="1000000">Tất cả</li>--%>
                            </ul>
                            <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                        </div>
                    </div>
                </div>
                <!-- End Row Table Filter -->
                <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                    <ContentTemplate>
                        <div class="row table-wp">
                            <table class="table-add table-listing report-sales-listing">
                                <thead>
                                    <tr>
                                        <th style="width: 80px">STT</th>
                                        <th>ID</th>
                                        <th>Tên NV</th>
                                        <th>Hình thức</th>
                                        <th>Số tiền</th>
                                        <th style="width: 120px">Ngày tạo</th>
                                        <th>Ảnh</th>
                                        <%--<th style="width:100px">Chức năng</th>--%>
                                        <th style="width: 100px">Ghi chú</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <div id="myModalimage" style="z-index: 2000;" class="modal modal-image">
                                        <img class="modal-content-image" id="img01">
                                        <div id="captionimage"></div>
                                    </div>

                                    <asp:Repeater ID="rptMistake" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# Container.ItemIndex + 1 + PAGING._Offset %></td>
                                                <td><%# Eval("staffId") %></td>
                                                <td><%# Eval("fullName") %></td>
                                                <td><%# Eval("changedTypeId")==null? "": GetValue(Convert.ToInt32(Eval("changedTypeId"))).ToString() %></td>
                                                <td><%# String.Format("{0:#,####}", Math.Abs((int)Eval("Point"))).Replace(',', '.') %></td>
                                                <td><%# string.Format("{0:dd/MM/yyyy}", Eval("WorkDate")) %></td>
                                                <td>
                                                    <img id="myImg1" src='<%# Eval("Images") %>' alt="" data-zoom-image='<%#Eval("Images") %>' onclick="zoom()" style="width: 50px; height: 50px" />
                                                </td>
                                                <td><%# Eval("description") %></td>
                                                <td class="map-edit" style="width: 10%">
                                                    <div class="edit-wp">
                                                        <a class="elm edit-btn" href="/admin/thuong-phat/<%# Eval("Id") %>.html" title="Sửa"></a>
                                                        <a class="elm del-btn" onclick="del(this.parentNode.parentNode.parentNode,'<%# Eval("Id") %>', '<%# Eval("fullName") %>','<%# Eval("changedTypeId")==null? "": GetValue(Convert.ToInt32(Eval("changedTypeId"))).ToString() %>')" href="javascript://" title="Xóa"></a>
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>
                        <!-- Paging -->
                        <div class="site-paging-wp">
                            <% if (PAGING.TotalPage > 1)
                                { %>
                            <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                                <% if (PAGING._Paging.Prev != 0)
                                    { %>
                                <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                                <% } %>
                                <asp:Repeater ID="RptPaging" runat="server">
                                    <ItemTemplate>
                                        <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                            <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>>
                                            <%# Eval("PageNum") %>
                                        </a>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                                    { %>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                                <% } %>
                            </asp:Panel>
                            <% } %>
                        </div>
                        <!-- End Paging -->
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" Text="Click" Style="display: none;" OnClick="BtnFakeUP_Click" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="IsPaging" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_ExcelType" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_EXCELPage" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_EXCELSegment" />
                <asp:HiddenField ID="HDF_StaffId" ClientIDMode="Static" runat="server" />
            </div>
            <%-- END Listing --%>
        </div>
        <script src="/Assets/js/jquery.elevateZoom-3.0.8.min.js"></script>
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 150px !important;
                margin-top: 5px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }
        </style>
        <script type="text/javascript">
            $(".report-sales-listing #myImg1").elevateZoom();
            $(document).ready(function () {
                $(function () {
                    $('.txtDateTime').datetimepicker({
                        dayOfWeekStart: 1,
                        lang: 'vi',
                        startDate: '2014/10/10',
                        format: 'd/m/Y',
                        dateonly: true,
                        showHour: false,
                        showMinute: false,
                        timepicker: false,
                        onChangeDateTime: function (dp, $input) {
                        }
                    });
                });
            });
            jQuery(document).ready(function () {
                // Add active menu
                $("#glbQuanlity").addClass("active");
                $("#glbMistake").addClass("active");
                $("li.li-listing").addClass("active");

                // Price format
                UP_FormatPrice('.be-report-price');

                //============================
                // Datepicker
                //============================


                // View data today
                $(".tag-date-today").click();

                //============================
                // Show System Message
                //============================ 
                var qs = getQueryStrings();
                showMsgSystem(qs["msg_update_message"], qs["msg_update_status"]);
            });

            function SubTableFixHeight() {
                $("tbody table.sub-table").each(function () {
                    $(this).height($(this).parent().height() + 1);
                });
            }

            function viewDataByDate(This, time) {
                $(".tag-date.active").removeClass("active");
                This.addClass("active");
                $("#TxtDateTimeTo").val("");
                $("#TxtDateTimeFrom").val(time);
                $("#ViewData").click();
            }

            //============================
            // Event delete
            //============================
            function del(This, code, name, formMistake) {
                debugger;
                var code = code || null,
                    name = name || null,
                    Row = This;
                if (!code) return false;

                // show EBPopup
                $(".confirm-yn").openEBPopup();
                $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn xóa [ " + formMistake + " ] của [ " + name + " ] ?");

                $("#EBPopup .yn-yes").bind("click", function () {
                    $.ajax({
                        type: "POST",
                        url: "/GUI/BackEnd/Quanlity/Staff_Mistake_V2.aspx/Delele",
                        data: '{Id : "' + code + '"}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json", success: function (response) {
                            var mission = JSON.parse(response.d);
                            if (mission.success) {
                                delSuccess();
                                Row.remove();
                            } else {
                                delFailed();
                            }
                        },
                        failure: function (response) { alert(response.d); }
                    });
                });
                $("#EBPopup .yn-no").bind("click", function () {
                    autoCloseEBPopup(0);
                });
            }

            function BindIdToHDF(THIS, Code, Field, HDF_StoreValue, Input_DomId) {
                var text = THIS.text().trim();
                $(HDF_StoreValue).val(Code);
                $(Input_DomId).val(text);
                $(Input_DomId).parent().find(".eb-select-data").hide();

                // Auto post server
                $("#BtnFakeUP").click();
            }

            function EBSelect_HideBox() {
                $(".eb-select-data").hide();
                $("ul.ul-listing-suggestion li.active").removeClass("active");
            }

            function showMsgSystem(msg, status) {
                $("#MsgSystem").css("opacity", 0).text(msg).addClass(status).fadeTo(0, 1);
                setTimeout(function () {
                    $("#MsgSystem").fadeTo("slow", 0, function () {
                        $("#MsgSystem").text("").attr("class", "msg-system").css("opacity", 1);
                    });
                }, 5000);
            }
            //============================//

        </script>
        <style type="text/css">
            /*.modal-content-image {
                margin: auto;
                margin-top: 30px;
                display: block;
                width: 50%;
                max-width: 500px;
                height: auto;
                max-height: 600px;
            }*/
        </style>
        <script src="/Assets/js/jquery.elevateZoom-3.0.8.min.js"></script>
        <script type="text/javascript">
            //var modal = document.getElementById('myModalimage');

            //// Get the image and insert it inside the modal - use its "alt" text as a caption
            //var img = document.getElementById('myImg1');
            //var modalImg = document.getElementById("img01");
            //var captionText = document.getElementById("captionimage");
            //img.onclick = function () {
            //    modal.style.display = "block";
            //    modalImg.src = this.src;
            //    captionText.innerHTML = this.alt;
            //}
            //$('#myModalimage').click(function () {
            //    $("#myModalimage").css("display", "none")
            //});
            //$('.modal-content-image').click(function (event) {
            //    event.stopPropagation();
            //});

            //$(function () {
            //    $(".report-sales-listing #myImg1").elevateZoom({
            //        cursor: 'pointer',
            //        tint: true,
            //        tintColour: '#F90',
            //        tintOpacity: 0.5,
            //    });
            //});
            $(".report-sales-listing #myImg1").elevateZoom();
        </script>

    </asp:Panel>
</asp:Content>
