﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using LinqKit;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.BO;
using _30shine.MODEL.IO;
using System.Net.Http;
using System.Web.Script.Serialization;
using System.Net.Http.Headers;
using System.Web.Services;
using System.Net;

namespace _30shine.GUI.BackEnd.Quanlity
{
    public partial class Staff_Mistake_V2 : System.Web.UI.Page
    {
        protected Paging PAGING = new Paging();
        CultureInfo culture = new CultureInfo("vi-VN");
        private string PageID = "STAFF_MISTAKE_V2";
        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        protected bool Perm_ViewAllData = false;
        protected bool Perm_ViewAllDate = false;
        private bool Perm_Access = false;
        protected bool IsAccountant = false;
        protected string Day1 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-2));
        protected string Day2 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-1));
        protected string Day3 = string.Format("{0:dd/MM/yyyy}", DateTime.Now);

        private DateTime timeFrom = new DateTime();
        private DateTime timeTo = new DateTime();
        //protected int staffType;
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //    IPermissionModel permissionModel = new PermissionModel();
                //    var permission = Session["User_Permission"].ToString();
                //    Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //    Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                //    Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //    Perm_ViewAllDate = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllDate", PageID, permission);
                //    ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_ViewAllDate = permissionModel.CheckPermisionByAction("Perm_ViewAllDate", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                Library.Function.bindSalon(new List<DropDownList> { Salon }, Perm_ViewAllData);
                Bind_StaffType();
                BindData();
                BindHinhthuc();
            }
        }

        /// <summary>
        /// Bind StaffType
        /// </summary>
        private void Bind_StaffType()
        {
            using (var db = new Solution_30shineEntities())
            {
                var staffType = db.Staff_Type.Where(w => w.IsDelete != 1 && w.Publish == true).OrderBy(o => o.Id).ToList();
                var Key = 0;

                StaffType.DataTextField = "Name";
                StaffType.DataValueField = "Id";

                ListItem item2 = new ListItem();
                foreach (var v in staffType)
                {
                    // stylist | skinner | check-in
                    if (v.Id == 1 || v.Id == 2 || v.Id == 5)
                    {
                        item2 = new ListItem(v.Name, v.Id.ToString());
                        StaffType.Items.Insert(Key++, item2);
                    }
                }

                ListItem item = new ListItem("Chọn bộ phận", "0");
                StaffType.Items.Insert(0, item);

                StaffType.SelectedIndex = -1;
            }
        }
        private void BindHinhthuc()
        {
            using (Solution_30shineEntities db = new Solution_30shineEntities())
            {
                var data = db.tbl_Form_Misstake.Where(o => o.isDelete == false).ToList().OrderBy(o => o.Id);
                var key = 0;
                ddlHinhThuc.DataTextField = "Formality";
                ddlHinhThuc.DataValueField = "Id";
                ddlHinhThuc.Items.Insert(key++, new ListItem("Chọn hình thức", "0"));

                ListItem item = new ListItem();
                foreach (var v in data)
                {
                    item = new ListItem(v.Formality, v.Id.ToString());
                    ddlHinhThuc.Items.Insert(key++, item);
                }
            }
        }

        /// <summary>
        /// Get List Thưởng/Phạt
        /// </summary>
        private void BindData()
        {
            try
            {
                var clsSearch = new Input_SalaryFilter();
                int integer;
                clsSearch.TimeFrom = new DateTime();
                clsSearch.TimeTo = new DateTime();
                clsSearch.SalonId = int.TryParse(Salon.SelectedValue, out integer) ? integer : 0;
                clsSearch.DepartmentId = int.TryParse(StaffType.SelectedValue, out integer) ? integer : 0;
                if (TxtDateTimeFrom.Text != "")
                {
                    clsSearch.TimeFrom = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                    if (TxtDateTimeTo.Text != "")
                    {
                        if (TxtDateTimeFrom.Text == TxtDateTimeTo.Text)
                        {
                            clsSearch.TimeTo = clsSearch.TimeFrom;
                        }
                        else
                        {
                            clsSearch.TimeTo = Convert.ToDateTime(TxtDateTimeTo.Text, culture);
                        }
                    }
                    else
                    {
                        clsSearch.TimeTo = clsSearch.TimeFrom;
                    }
                }
                using (var client = new HttpClient())
                {
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    client.BaseAddress = new Uri(Libraries.AppConstants.URL_API_FINANCIAL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var response = client.PostAsJsonAsync("/api/salary-income-change/get-list", clsSearch).Result;
                    var result = response.Content.ReadAsStringAsync().Result;
                    var serializer = new JavaScriptSerializer();
                    var salaryIncomeChange = serializer.Deserialize<ResponseData>(result).data;
                    if (salaryIncomeChange.Count > 0)
                    {
                        Bind_Paging(salaryIncomeChange.Count());
                    }
                    rptMistake.DataSource = salaryIncomeChange.Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                    rptMistake.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        protected void BtnFakeUP_Click(object sender, EventArgs e)
        {
            BindData();
            //ScriptManager.RegisterStartupScript(UPTableListing, this.GetType(), "Format Price", "UP_FormatPrice('.be-report-price'); SubTableFixHeight();SetActiveTableListing();", true);
            RemoveLoading();
        }
        protected void Bind_Paging(int TotalRow)
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(TotalRow) : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }
        /// <summary>
        /// Get Form Misstake
        /// </summary>
        public string GetValue(int Id)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var list = db.tbl_Form_Misstake.Where(w => w.Id == Id).ToList();
                    foreach (var v in list)
                    {
                        return v.Formality.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return "";
        }

        protected int Get_TotalPage(int TotalRow)
        {
            TotalRow -= PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        }

        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        /// <summary>
        /// Delete 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [WebMethod]
        public static string Delele(int Id)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();
            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                var salaryIncome = (from c in db.SalaryIncomeChanges
                                    where c.Id == Id && c.IsDeleted == false
                                    select c).FirstOrDefault();
                salaryIncome.IsDeleted = true;
                var exc = db.SaveChanges();
                if (exc > 0)
                {
                    Msg.success = true;
                }
                else
                {
                    Msg.success = false;
                }
            }
            return serializer.Serialize(Msg);
        }

        /// <summary>
        /// Input salary filter 
        /// author: Quynh DD
        /// </summary>
        public class Input_SalaryFilter
        {
            public DateTime TimeFrom { get; set; }
            public DateTime TimeTo { get; set; }
            public int SalonId { get; set; }
            public int DepartmentId { get; set; }
            public int StaffId { get; set; }
            public int Active { get; set; }
            public int ChangedTypeId { get; set; }
        }
    }
}


