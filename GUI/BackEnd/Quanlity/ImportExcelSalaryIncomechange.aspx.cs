﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.Quanlity
{
    public partial class ImportExcelSalaryIncomechange : System.Web.UI.Page
    {
        public bool Perm_ViewAllData = false;
        private bool Perm_Access = false;

        /// <summary>
        /// set permission
        /// </summary>
        public void SetPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        /// <summary>
        /// ExecuteByPermission
        /// </summary>
        private void ExecuteByPermission()
        {

            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        /// <summary>
        /// page load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
        }

        /// <summary>
        /// Insert data to table SalaryIncomechange
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [WebMethod]
        public static object InsertDataSalaryIncomechange(List<DataInputJson> data)
        {
            CultureInfo culture = new CultureInfo("vi-VN");
            DateTime FirstDayOfMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            DateTime Today = DateTime.Today;
            try
            {
                var exc = 0;
                var message = new Library.Class.cls_message();
                if (data == null)
                {
                    throw new Exception();
                }

                var staffIds = data.Select(a => a.StaffId).Distinct();
                foreach (var v in staffIds)
                {
                    //check workDate and ChangedType List Input 
                    var check = data.Where(a => a.StaffId == v).Select(a => new
                    {
                        workDate = a.WorkDate,
                        changedType = a.ChangedType
                    });
                    if (check.Distinct().Count() != check.Count())
                    {
                        throw new Exception();
                    }
                }
                using (var db = new Solution_30shineEntities())
                {
                    var listDynamodb = new List<ReqSalaryIncomeChange>();
                    var listObj = new List<SalaryIncomeChange>();
                    foreach (var v in data)
                    {
                        int staffId = Convert.ToInt32(v.StaffId);
                        DateTime workDate = Convert.ToDateTime(v.WorkDate, culture);
                        //check tồn tại trong db
                        var checkDuplicate = (from c in db.SalaryIncomeChanges
                                              where c.StaffId == staffId
                                              && c.ChangedType == v.ChangedType
                                              && c.WorkDate.Value.Month == workDate.Month
                                              && c.WorkDate.Value.Year == workDate.Year
                                              select c).FirstOrDefault();

                        if (checkDuplicate != null)
                        {
                            checkDuplicate.Point = Convert.ToInt32(v.Point.ToString().Trim().Replace(",", ""));
                            checkDuplicate.WorkDate = Convert.ToDateTime(v.WorkDate, culture);
                            checkDuplicate.ModifiedTime = DateTime.Now;
                            db.SalaryIncomeChanges.AddOrUpdate(checkDuplicate);
                            listObj.Add(checkDuplicate);
                        }
                        else
                        {
                            var obj = new SalaryIncomeChange();
                            obj.StaffId = Convert.ToInt32(v.StaffId);
                            obj.ChangedType = v.ChangedType;
                            obj.Point = Convert.ToInt32(v.Point.ToString().Trim().Replace(",", ""));
                            obj.WorkDate = Convert.ToDateTime(v.WorkDate, culture);
                            obj.IsDeleted = false;
                            obj.CreatedTime = DateTime.Now;
                            db.SalaryIncomeChanges.Add(obj);
                            listObj.Add(obj);
                        }
                    }
                    //save changes
                    exc = db.SaveChanges();

                    //check
                    if (exc > 0)
                    {
                        message.success = true;
                        // Dẩy lên dynamodb
                        if (listObj.Any())
                        {
                            var list = listObj.Select(r => new
                            {
                                r.Id,
                                r.StaffId,
                                r.WorkDate,
                                r.Point,
                                r.ChangedTypeId,
                                r.ChangedType,
                                r.SalonId
                            }).ToList();
                            var response = new HttpClient().PostAsJsonAsync(Libraries.AppConstants.URL_API_MANAGER_APP + "/api/salary-income-change/push-dynamodb", list).Result;

                        }
                    }
                    else
                    {
                        message.success = false;
                    }
                }

                return message;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// input data json
        /// </summary>
        public class DataInputJson
        {
            public string StaffId { get; set; }
            public string Point { get; set; }
            public string ChangedType { get; set; }
            public string WorkDate { get; set; }
        }
        public class ReqSalaryIncomeChange
        {
            public int Id { get; set; }
            public int SalonId { get; set; }
            public int StaffId { get; set; }
            public string Key { get; set; }
            public string WorkDate { get; set; }
            public double Point { get; set; }
            public int ChangedTypeId { get; set; }

        }
    }
}