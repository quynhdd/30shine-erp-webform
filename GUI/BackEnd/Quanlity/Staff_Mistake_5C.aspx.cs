﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using _30shine.Helpers;
using _30shine.MODEL.CustomClass;
using _30shine.MODEL.BO;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.IO;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.Interface;
using System.Globalization;

namespace _30shine.GUI.BackEnd.Quanlity
{
    public partial class Staff_Mistake_5C : System.Web.UI.Page
    {
        private static IStaffMistakeModel Model5C = new StaffMistakeModel();
        private static IFormMistakeModel FormMistake = new FormMistakeModel();
        private static IStaffModel ModelStaff = new StaffModel();
        private static ISalonModel ModelSalon = new SalonModel();
        private string PageID = "THUONG_5C";
        private bool Perm_ShowSalon = false;
        private bool Perm_Access = false;
        private bool Perm_ViewAllData = false;
        private string permission = "";
        private int salonId = 0;
        protected void SetPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ShowSalon = permissionModel.CheckPermisionByAction("Perm_ShowSalon", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            int integer = 0;
            salonId = Session["SalonId"] != null ? int.TryParse(Session["SalonId"].ToString(), out integer) ? integer : 0 : 0;
            SetPermission();
            if (!IsPostBack)
            {
                Library.Function.bindSalon_NotHoiQuan(new List<DropDownList> { ddlSalon }, Perm_ViewAllData);
                BindDepartment();
            }
        }
       
        public void BindDepartment()
        {
            var ListDeparment = ModelStaff.ListDepartment();
            if (ListDeparment.Count > 0)
            {
                ddlDepartment.DataTextField = "Name";
                ddlDepartment.DataValueField = "Id";
                ddlDepartment.DataSource = ListDeparment;
                ddlDepartment.DataBind();
                ddlDepartment.Items.Insert(0, new ListItem("Chọn bộ phận", "0"));
            }


        }
        [WebMethod(EnableSession = true)]
        public static object GetListStaff5C(string _WorkDate, int _salonId, int _DepartmentId)
        {
            Msg message = new Msg();
            // khoi tao gia tri mac dinh.
            message.success = false;
            DateTime? WorkDate = new DateTime();
            try
            {
                WorkDate = Convert.ToDateTime(_WorkDate,new CultureInfo("vi-VN"));
                int SalonId = _salonId;
                //Lấy ra list nhân viên của salon đó
                List<StaffMistake5CClass> staff = ModelStaff.GetList5CStaffBySalon(SalonId, _DepartmentId, WorkDate.Value);
                //Lấy list salary income change
                List<StaffMistake5CClass> incomchange = Model5C.GetList5C(SalonId, WorkDate);
                if (staff.Count > 0)
                {
                    for (var i = 0; i < staff.Count(); i++)
                    {
                        if (incomchange.Count() > 0)
                        {
                            foreach (var j in incomchange)
                            {
                                if (j.StaffId == staff[i].StaffId)
                                {
                                    staff[i].WorkDate = j.WorkDate;
                                    staff[i].ScoreFactor = j.ScoreFactor;
                                    staff[i].InComeChangeId = j.InComeChangeId;
                                    staff[i].ChangedTypeId = j.ChangedTypeId;
                                    staff[i].ChangedType = j.ChangedType;
                                    staff[i].Description = j.Description;
                                    staff[i].FormMistake = j.FormMistake;
                                }
                            }
                        }
                    }
                    message.data = staff.OrderByDescending(r => r.Department);
                    message.success = true;
                }
                else
                {
                    message.msg = "Không tìm thấy nhân viên";
                }

            }
            catch (Exception ex) { message.msg = ex.Message; }
            return message;
        }
        [WebMethod(EnableSession = true)]
        public static object InsertOrUpdate5C(int id, int staffId, int salonId, string changedTypeId, string scoreFactor, string workDate, string decription,int ratingSalary)
        {
            int number=0;
            SalaryIncomeChange obj5C = new SalaryIncomeChange();
            Msg message = new Msg();
            try
            {
                obj5C.Id = id;
                if (changedTypeId != null) { obj5C.ChangedTypeId = Int32.TryParse(changedTypeId, out number) == true ? number : 0; }
                else { throw new Exception("Chưa nhập hình thức"); }
                obj5C.Description = decription;
                obj5C.StaffId = staffId;
                obj5C.SalonId = salonId;
                if (scoreFactor != null) { obj5C.ScoreFactor = Int32.TryParse(scoreFactor, out number) == true ? number : 0; }
                else { throw new Exception("Chưa nhập điểm: "); }
                obj5C.WorkDate = Convert.ToDateTime(workDate, new CultureInfo ("vi-VN")).Date;
                obj5C.IsDeleted = false;
                obj5C.ChangedType = "luong_5c";
                obj5C.Point = ratingSalary * Convert.ToInt32(scoreFactor);
                
                message = Model5C.AddOrUpdate5C(obj5C);
            }
            catch (Exception ex)
            {
                return "Tham số truyền không hợp lệ: " + ex.Message;
            }
           
            return message;
        }
        [WebMethod(EnableSession = true)]
        public static object GetIncomgeChange()
        {
            return FormMistake.GetList5C();

        }
        protected void GetListStaff5C_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Click", "GetListStaff5C();", true);
        }

        
        protected void AddOrUpdate5C_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Click", "AddOrUpdate5C();", true);
        }
    }
}