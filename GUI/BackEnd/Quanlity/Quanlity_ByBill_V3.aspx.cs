﻿using _30shine.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;
using System.Globalization;
using _30shine.MODEL.ENTITY.EDMX;
using System.Net.Http;
using System.Web.Script.Serialization;
using System.Net;

namespace _30shine.GUI.BackEnd.Quanlity
{
    public partial class Quanlity_ByBill_V3 : System.Web.UI.Page
    {
        protected Paging PAGING = new Paging();
        CultureInfo culture = new CultureInfo("vi-VN");
        private bool Perm_ViewAllData = false;
        private bool Perm_Access = false;
        protected bool isRoot = false;
        protected string FirstDayOfMonth = string.Format("{0:dd/MM/yyyy}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1));
        protected string Today = String.Format("{0:dd/MM/yyyy}", DateTime.Now);
        /// <summary>
        /// set permission
        /// </summary>
        public void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        /// <summary>
        /// ExecuteByPermission
        /// </summary>
        private void ExecuteByPermission()
        {

            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                TxtDateTimeFrom.Text = FirstDayOfMonth;
                TxtDateTimeTo.Text = Today;
                var staffId = Convert.ToInt32(Session["User_Id"].ToString());
                Library.Function.bindSalon(new List<DropDownList> { Salon }, Perm_ViewAllData);
                bindDepartment();
                BindData();
            }
            RemoveLoading();
        }

        /// <summary>
        /// Load department
        /// </summary>
        private void bindDepartment()
        {
            using (var db = new Solution_30shineEntities())
            {
                int[] ccArrDepartment = { 1, 2, 4, 5, 6 };
                var department = db.Staff_Type.Where(w => w.IsDelete == 0 && w.Publish == true && ccArrDepartment.Contains(w.Id)).ToList();
                var ddls = new List<DropDownList> { StaffType };
                for (var i = 0; i < ddls.Count; i++)
                {
                    ddls[i].DataTextField = "Name";
                    ddls[i].DataValueField = "Id";
                    ddls[i].DataSource = department;
                    ddls[i].DataBind();
                }
            }
        }

        /// <summary>
        /// Get data
        /// </summary>
        private List<Output_ScoreQuanlity> Getdata()
        {
            try
            {
                int integer;
                int departmentId = int.TryParse(StaffType.SelectedValue, out integer) ? integer : 0;
                int salonId = int.TryParse(Salon.SelectedValue, out integer) ? integer : 0;
                DateTime timeFrom = new DateTime();
                DateTime timeTo = new DateTime();
                if (TxtDateTimeFrom.Text != "")
                {
                    timeFrom = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                    if (TxtDateTimeTo.Text != "")
                    {
                        if (TxtDateTimeFrom.Text == TxtDateTimeTo.Text)
                        {
                            timeTo = timeFrom;
                        }
                        else
                        {
                            timeTo = Convert.ToDateTime(TxtDateTimeTo.Text, culture);
                        }
                    }
                    else
                    {
                        timeTo = timeFrom;
                    }
                }
                using (var db = new Solution_30shineEntities())
                {
                    string sql = @"	DECLARE
		                                @staffId INT,
		                                @departmentId INT,
		                                @salonId INT,
		                                @timeFrom DATE,
		                                @timeTo DATE
		                                SET @departmentId = " + departmentId + @"
		                                SET @salonId = " + salonId + @"
		                                SET @timeFrom = '" + string.Format("{0:yyyy-MM-dd}", timeFrom) + @"'
		                                SET @timeTo = '" + string.Format("{0:yyyy-MM-dd}", timeTo) + @"'

	                                BEGIN
	                                WITH tempWait AS (

		                                SELECT wait.StaffId, SUM(ISNULL(wait.Star5, 0)) AS Star5, SUM(ISNULL(wait.Star4, 0)) AS Star4, 
			                                SUM(ISNULL(wait.Star3, 0)) AS Star3, SUM(ISNULL(wait.Star2, 0)) AS Star2, SUM(ISNULL(wait.Star1, 0)) AS Star1,staff.Fullname, 
											SUM(ISNULL(wait.TotalBillService,0)) AS TotalBillSerivce
		                                FROM dbo.StaticRatingWaitTime AS wait 
			                                INNER JOIN dbo.Staff AS staff ON wait.StaffId = staff.Id
		                                WHERE wait.WorkDate BETWEEN @timeFrom AND @timeTo AND wait.IsDelete = 0 AND
			                                wait.StaffType = @departmentId AND ((staff.SalonId = @salonId AND @salonId > 0) OR @salonId = 0)
		                                GROUP BY wait.StaffId,staff.Fullname
	                                )
		                                SELECT tempWait.*, SUM(ISNULL(income.RatingPoint,0)) AS TotalRatingPoint FROM tempWait
			                                INNER JOIN dbo.SalaryIncome AS income ON tempWait.StaffId = income.StaffId
		                                WHERE income.WorkDate BETWEEN @timeFrom AND @timeTo
		                                GROUP BY tempWait.StaffId,tempWait.Star1,tempWait.Star2,tempWait.Star3,tempWait.Star4,tempWait.Star5,tempWait.Fullname,tempWait.TotalBillSerivce
		                                ORDER BY TotalRatingPoint DESC
	                                END ";
                    var list = db.Database.SqlQuery<Output_ScoreQuanlity>(sql).ToList();
                    return list;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Bind data
        /// </summary>
        private void BindData()
        {
            try
            {
                var data = Getdata();
                //if (data.Count > 0)
                //{
                    Bind_Paging(data.Count());
                    Rpt_Servey.DataSource = data.Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                    Rpt_Servey.DataBind();
                //}
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Btn
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void _BtnClick(object sender, EventArgs e)
        {
            try
            {
                BindData();
                RemoveLoading();
            }
            catch (Exception ex)
            {
                TriggerJsMsgSystem_temp(this, "ĐÃ CÓ LỖI XẢY RA, XIN LIÊN HỆ NHÀ PHÁT TRIỂN", 30000);
                RemoveLoading();
            }
        }
        /// <summary>
        /// thong bao ve client loi
        /// </summary>
        /// <param name="_OBJ"></param>
        /// <param name="msg"></param>
        /// <param name="status"></param>
        /// <param name="duration"></param>
        public void TriggerJsMsgSystem_temp(Page objectPage, string msg, int duration, string status = "msg-system warning")
        {
            ScriptManager.RegisterStartupScript(objectPage, objectPage.GetType(), "Message System", "showMsgSystem('" + msg.Replace("'", "") + "','" + status + "'," + duration + ");", true);
        }
        /// <summary>
        /// Paging
        /// </summary>
        /// <param name="TotalRow"></param>
        protected void Bind_Paging(int TotalRow)
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(TotalRow) : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage(int TotalRow)
        {
            TotalRow -= PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        }
        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }
        /// <summary>
        /// Output điểm chất lượng
        /// </summary>
        public class Output_ScoreQuanlity
        {
            public int StaffId { get; set; }
            public string FullName { get; set; }
            public int Star1 { get; set; }
            public int Star2 { get; set; }
            public int Star3 { get; set; }
            public int Star4 { get; set; }
            public int Star5 { get; set; }
            public int TotalBillSerivce { get; set; }
            public double TotalRatingPoint { get; set; }
        }
    }
}