﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Globalization;
using System.Web.Script.Serialization;
using ExportToExcel;
using Excel;
using LinqKit;
using System.Data.Entity.Migrations;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.QuanlityV2
{
    public partial class Quanlity_ByBill_V2: System.Web.UI.Page
    {
        protected Paging PAGING = new Paging();
        protected List<List<ProductBasic>> ProductList;
        private Expression<Func<BillService, bool>> Where = PredicateBuilder.True<BillService>();
        CultureInfo culture = new CultureInfo("vi-VN");

        private string PageID = "CL_DHL";
        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        protected bool Perm_ViewAllData = false;
        protected bool Perm_ViewAllDate = false;
        private bool Perm_Access = false;
        protected bool IsAccountant = false;
        List<_30shine.MODEL.ENTITY.EDMX.Staff> _LstNhanVien;
        protected string Day1 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-2));
        protected string Day2 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-1));
        protected string Day3 = string.Format("{0:dd/MM/yyyy}", DateTime.Now);

        protected string table = "";        
        private DateTime timeFrom = new DateTime();
        private DateTime timeTo = new DateTime();
        protected int qstaffType;
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
            //    IPermissionModel permissionModel = new PermissionModel();
            //    var permission = Session["User_Permission"].ToString();
            //    Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
            //    Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
            //    Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
            //    Perm_ViewAllDate = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllDate", PageID, permission);
            //    ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_ViewAllDate = permissionModel.CheckPermisionByAction("Perm_ViewAllDate", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        private void ExecuteByPermission()
        {

            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (!IsPostBack)
            {
                var staffId = Convert.ToInt32(Session["User_Id"].ToString());
                Library.Function.bindSalon(new List<DropDownList> { Salon }, Perm_ViewAllData);
                Bind_StaffType();
                GenWhere();
                Bind_Data();
            }
            else
            {
                GenWhere();
            }
        }

        protected void FillLstStaff()
        {
            using (var db = new Solution_30shineEntities())
            {
                _LstNhanVien = db.Staffs.ToList();
            }
        }

        private void GenWhere()
        {                 
            //timeFrom = Convert.ToDateTime("29/3/2016", culture);
            timeFrom = Convert.ToDateTime("1/7/2016", culture);
            timeTo = DateTime.Now;

            if (TxtDateTimeFrom.Text != "")
            {
                timeFrom = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                if (TxtDateTimeTo.Text != "")
                {
                    timeTo = Convert.ToDateTime(TxtDateTimeTo.Text, culture);
                }
                else
                {
                    timeTo = timeFrom;
                }
            }
        }

        private string genSql(Solution_30shineEntities db)
        {
            int integer;
            int salonId = int.TryParse(Salon.SelectedValue, out integer) ? integer : 0;
            int staffType = int.TryParse(StaffType.SelectedValue, out integer) ? integer : 0;
            qstaffType = staffType;
            string wSalon = "";
            string wDepartment = "";
            if (salonId > 0)
            {
                wSalon = " and staff.SalonId = " + salonId;
            }
            else
            {
                wSalon = " and staff.SalonId > 0";
            }
            if (staffType > 0)
            {
                wDepartment = " and staff.[Type] = " + staffType;
            }
            else
            {
                wDepartment = " and staff.[Type] > 0";
            }
            string sql = @"select a.*, staff.Fullname as staffName
                            from
                            (
	                            select staffId,
		                            Coalesce(SUM(Coalesce(ratingPoint, 0)), 0) as point,
		                            Coalesce(SUM(Coalesce(bill_normal, 0)), 0) as bill_normal,
		                            Coalesce(SUM(Coalesce(bill_normal_great, 0)), 0) as bill_normal_great,
		                            Coalesce(SUM(Coalesce(bill_normal_good, 0)), 0) as bill_normal_good,
		                            Coalesce(SUM(Coalesce(bill_normal_bad, 0)), 0) as bill_normal_bad,
		                            Coalesce(SUM(Coalesce(bill_normal_norating, 0)), 0) as bill_normal_norating,
		                            Coalesce(SUM(Coalesce(bill_special, 0)), 0) as bill_special,
		                            Coalesce(SUM(Coalesce(bill_special_great, 0)), 0) as bill_special_great,
		                            Coalesce(SUM(Coalesce(bill_special_good, 0)), 0) as bill_special_good,
		                            Coalesce(SUM(Coalesce(bill_special_bad, 0)), 0) as bill_special_bad,
		                            Coalesce(SUM(Coalesce(bill_special_norating, 0)), 0) as bill_special_norating,
                                    Coalesce(SUM(Coalesce(Mistake_Point, 0)), 0) as mistake_point
	                            from FlowSalary
	                            where sDate between '" + string.Format("{0:yyyy/MM/dd}", timeFrom)+"' and '"+string.Format("{0:yyyy/MM/dd}", timeTo)+@"'
	                            group by staffId
                            ) as a
                            inner join Staff as staff
                            on a.staffId = staff.Id
                            where staff.IsDelete != 1 and staff.Active = 1 " + wSalon + wDepartment;
            return sql;
        }

        private void Bind_Data()
        {
            using (var db = new Solution_30shineEntities())
            {
                var sql = genSql(db);
                if (sql != "")
                {
                    var lst = db.Database.SqlQuery<Library.Class.cls_rating>(sql).OrderByDescending(o => o.point).ToList();                    
                    // Bind data
                    Rpt_Servey.DataSource = lst;
                    Rpt_Servey.DataBind();
                }
            }
        }


        private void Bind_StaffType()
        {
            using (var db = new Solution_30shineEntities())
            {
                var staffType = db.Staff_Type.Where(w => w.IsDelete != 1 && w.Publish == true).OrderBy(o => o.Id).ToList();
                var Key = 0;

                StaffType.DataTextField = "Name";
                StaffType.DataValueField = "Id";

                //ListItem item = new ListItem("Chọn bộ phận", "0");
                //StaffType.Items.Insert(0, item);

                ListItem item = new ListItem();
                foreach (var v in staffType)
                {
                    // stylist | skinner | check-in
                    if (v.Id == 1 || v.Id == 2 || v.Id == 5)
                    {
                        item = new ListItem(v.Name, v.Id.ToString());
                        StaffType.Items.Insert(Key++, item);
                    }
                }
                StaffType.SelectedIndex = 0;
            }
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            //Bind_Paging();
            Bind_Data();
            ScriptManager.RegisterStartupScript(UPTableListing, this.GetType(), "Format Price", "UP_FormatPrice('.be-report-price'); SubTableFixHeight();", true);
            RemoveLoading();
        }

        protected void Bind_Paging()
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? (HDF_Page.Value != "" ? Convert.ToInt32(HDF_Page.Value) : 1) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage() : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage()
        {
            using (var db = new Solution_30shineEntities())
            {
                var Count = 0;
                var sql = genSql(db);
                if (sql != "")
                {
                    Count = db.Database.SqlQuery<Library.Class.cls_rating>(sql).Count();
                }
                int TotalRow = Count - PAGING._TopNewsNum;
                int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
                return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
            }
        }

        protected void Btn_SwitchExcel(object sender, EventArgs e)
        {
            if (HDF_ExcelType.Value == "Export")
            {
                //Exc_ExportExcel();
            }
            else if (HDF_ExcelType.Value == "Import")
            {
                //Exc_ImportExcel();
            }

        }

        protected void Exc_ImportExcel()
        {
            var _ExcelHeadRow = new List<string>();
            _ExcelHeadRow.Add("STT");
            _ExcelHeadRow.Add("Field1");
            _ExcelHeadRow.Add("Field2");
            _ExcelHeadRow.Add("Field3");

            var LST = new List<Tuple<int, string, string, string>>();

            for (var i = 0; i < 5; i++)
            {
                var v = Tuple.Create(i, "str1-" + i, "str2-" + i, "str3-" + i);
                LST.Add(v);
            }

            // import
            var Path = Server.MapPath("~") + "Public/Excel/Hoa.Don/" + "Hoa_Don_28_07_2015_06_28_13.xlsx";
            Response.Write(Path);
            ImportXcel(Path);
        }

        private static void ExportXcel(List<string> _ExcelHeadRow, List<List<string>> List, string Path)
        {
            try
            {
                CreateExcelFile.ExcelHeadRow = _ExcelHeadRow;
                CreateExcelFile.CreateExcel(List, Path);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi. Không tạo được file excel.\r\nException: " + ex.Message);
            }
        }

        private void ImportXcel(string Path)
        {
            var tblDom = "<table><tbody>";
            foreach (var worksheet in Workbook.Worksheets(@Path))
                foreach (var row in worksheet.Rows)
                {
                    tblDom += "<tr>";
                    foreach (var cell in row.Cells)
                    {
                        if (cell != null)
                        {
                            tblDom += "<td>" + cell.Text + "</td>";
                        }
                    }
                    tblDom += "<tr>";
                }
            // if (cell != null) // Do something with the cells
            tblDom += "</tbody></table>";
            Response.Write(tblDom);
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        public string GenTime(int minutes)
        {
            string time = "";
            int hour = minutes / 60;
            int minute = minutes % 60;
            if (hour > 0)
            {
                time += hour + "h";
                time += minute + "p";
            }
            else
            {
                if (minute > 0)
                {
                    time += minute + " p";
                }
                else
                {
                    time += "-";
                }
            }
            return time;
        }

       

        public void Repeater_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
              || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (Perm_Edit == true)
                    ((Panel)(e.Item.FindControl("EAedit"))).Visible = true;
                if (Perm_Delete == true)
                    ((Panel)(e.Item.FindControl("EAdelete"))).Visible = true;
            }
        }
    }

    public class cls_quanlity
    {
        public int staffId { get; set; }
        public string staffName { get; set; }
        public int normalBills { get; set; }
        public int specialBills { get; set; }
        public double totalPoint { get; set; }
    }

    public class cls_billservice : BillService
    {
        public string staffName { get; set; }
        public int ratingPoint { get; set; }
    }

    public class cls_staffPoint
    {
        public int staffId { get; set; }
        public string staffName { get; set; }
        public int bill_special { get; set; }
        public int bill_normal { get; set; }
        public double point { get; set; }
        public int Mark { get; set; }
    }

    public class cls_staffRatingReport
    {
        public int staffId { get; set; }
        public string staffName { get; set; }
        public cls_ratingLevel normalBill { get; set; }
        public cls_ratingLevel specialBill { get; set; }
        public double point { get; set; }
    }

    public class cls_ratingLevel
    {
        /// <summary>
        /// Rất hài lòng
        /// </summary>
        public int great { get; set; }
        /// <summary>
        /// Hài lòng
        /// </summary>
        public int good { get; set; }
        /// <summary>
        /// Không hài lòng
        /// </summary>
        public int bad { get; set; }
        /// <summary>
        /// Không đánh giá
        /// </summary>
        public int noRating { get; set; }
    }
}
