﻿using _30shine.MODEL.ENTITY.EDMX;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Globalization;
using System.Linq;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.Helpers;
using _30shine.MODEL.BO;
using System.Web.Services;
using System.Collections;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.Quanlity
{
    public partial class Staff_Mistake_Add : System.Web.UI.Page
    {
        private string PageID = "";
        public StaffMistakeModel dbModel = new StaffMistakeModel();
        protected string _Code;
        protected bool HasImages = false;
        protected List<string> ListImagesUrl = new List<string>();
        protected string[] ListImagesName;
        protected bool isRoot = false;
        private bool Perm_Access = false;
        protected bool Perm_ShowSalon = false;
        protected bool IsAccountant = false;
        private bool Perm_ViewAllData = false;
        protected bool _IsUpdate = false;
        static int Id;
        CultureInfo culture = new CultureInfo("vi-VN");

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (!IsPostBack)
            {
                txtNgayTao.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
                // Library.Function.bindSalon(new List<DropDownList> { ddlSalon }, Perm_Access);
                BindSaff();
                BildSalon();
                //var staffId = Convert.ToInt32(Session["User_Id"].ToString());
                //Library.Function.bindSalon(new List<DropDownList> { ddlSalon }, staffId, Perm_ViewAllData);
                BindFormMisstake();
                BindError();

                if (IsUpdate())
                {
                    BindObj();
                }
            }

        }

        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        /// <summary>
        /// IsUpdate (check Id record )
        /// </summary>
        /// <returns></returns>
        private bool IsUpdate()
        {
            _Code = Request.QueryString["Code"];
            if (_Code != null)
            {
                _IsUpdate = true;
                return true;
            }
            else
            {
                _IsUpdate = false;
                return false;
            }
        }
        /// <summary>
        /// Bind Salon
        /// </summary>
        protected void BildSalon()
        {
            using (var db = new Solution_30shineEntities())
            {
                var list = db.Tbl_Salon.Where(a => a.IsDelete != 1 && a.Publish == true).ToList();
                if (list.Count > 0)
                {
                    ddlSalon.Items.Clear();
                    ddlSalon.Items.Add(new ListItem("Chọn salon", "0"));
                    for (int i = 0; i < list.Count; i++)
                    {
                        ddlSalon.Items.Add(new ListItem(list[i].Name, list[i].Id.ToString()));
                    }
                    ddlSalon.DataBind();
                }

            }
        }
        /// <summary>
        /// Bind hình thức các lỗi
        /// </summary>
        private void BindFormMisstake()
        {
            using (var db = new Solution_30shineEntities())
            {
                var list = db.tbl_Form_Misstake.Where(w => w.isDelete == false && w.isPuslish == false).ToList();
                if (list.Count > 0)
                {
                    ddlMisstake.Items.Clear();
                    ddlMisstake.Items.Add(new ListItem("Chọn hình thức", "0"));
                    for (int i = 0; i < list.Count; i++)
                    {
                        ddlMisstake.Items.Add(new ListItem(list[i].Formality, list[i].Id.ToString()));
                    }
                    ddlMisstake.DataBind();
                }
            }
        }
        /// <summary>
        /// Bind Error ( ví dụ: Sai quy trình....)
        /// </summary>
        private void BindError()
        {
            //List<string> error = new List<string>() { "Vệ sinh 3S", "Sai quy trình", "Sai đồng phục", "Đi làm muộn", "Văn hóa/Ý thức kém" };
            //for (int i = 0; i < error.Count; i++)
            //{
            //    ddlDescription.Items.Add(new ListItem(error[i], i.ToString()));
            //}
            ddlDescription.Items.Clear();
            ddlDescription.Items.Add(new ListItem("Chọn loại lỗi", "0"));
            ddlDescription.Items.Add(new ListItem("Vệ sinh 3S", "1"));
            ddlDescription.Items.Add(new ListItem("Sai quy trình", "2"));
            ddlDescription.Items.Add(new ListItem("Sai đồng phục", "3"));
            ddlDescription.Items.Add(new ListItem("Đi làm muộn", "4"));
            ddlDescription.Items.Add(new ListItem("Văn hóa/Ý thức kém", "5"));
            ddlDescription.DataBind();
        }
        /// <summary>
        /// Bind data to DropDowlist Staff
        /// </summary>
        private void BindSaff()
        {
            using (var db = new Solution_30shineEntities())
            {
                int interger;
                var salon = int.TryParse(ddlSalon.SelectedValue, out interger) ? interger : 0;
                var staff = new List<Staff>();
                var whereSalon = "";
                var sql = "";
                if (salon > 0)
                {
                    whereSalon = " and SalonId = " + salon;
                }
                sql = @"select * from Staff 
                        where IsDelete != 1 and Active = 1 
                        and (isAccountLogin != 1 or isAccountLogin is null)" + whereSalon;
                staff = db.Staffs.SqlQuery(sql).ToList();

                var first = new Staff();
                first.Id = 0;
                first.Fullname = "Chọn nhân viên";
                staff.Insert(0, first);

                ddlNhanVien.DataTextField = "Fullname";
                ddlNhanVien.DataValueField = "Id";
                ddlNhanVien.DataSource = staff;
                ddlNhanVien.DataBind();
            }
        }
        /// <summary>
        /// Bind Obj
        /// </summary>
        private bool BindObj()
        {
            var existObj = true;
            try
            {
                if (int.TryParse(_Code, out Id))
                {
                    var objStaffMistake = dbModel.GetListId(Id);

                    if (!objStaffMistake.Equals(null))
                    {
                        txtNgayTao.Text = objStaffMistake.CreateDate.ToString();
                        txtPrice.Text = objStaffMistake.Point.Value.ToString("#,##0");
                        txtGhiChu.Text = objStaffMistake.Note;

                        //ddlDescription.Items.Add(new ListItem(objStaffMistake.Description));
                        var itemDecription = ddlDescription.Items.FindByValue(objStaffMistake.Description);
                        if (itemDecription != null)
                        {
                            itemDecription.Selected = true;
                        }
                        var itemForm = ddlMisstake.Items.FindByValue(objStaffMistake.FormMissTakeId.ToString());
                        if (itemForm != null)
                        {
                            itemForm.Selected = true;
                        }
                        var itemStaff = ddlNhanVien.Items.FindByValue(objStaffMistake.StaffId.ToString());
                        if (itemStaff != null)
                        {
                            itemStaff.Selected = true;
                        }
                        var itemSalon = ddlSalon.Items.FindByValue(objStaffMistake.SalonId.ToString());
                        if (itemSalon != null)
                        {
                            itemSalon.Selected = true;
                        }
                        if (objStaffMistake.Images != "" && objStaffMistake.Images != null)
                        {
                            ListImagesName = objStaffMistake.Images.Split(',');
                            var Len = ListImagesName.Length;
                            for (var i = 0; i < Len; i++)
                            {
                                if (ListImagesName[i] != "")
                                {
                                    ListImagesUrl.Add(ListImagesName[i]);
                                }
                            }
                            if (Len > 0) HasImages = true;
                        }
                        txtNgayTao.Enabled = false;
                        txtNgayTao.ReadOnly = true;
                        ddlSalon.Enabled = false;
                        ddlNhanVien.Enabled = false;
                    }
                    else
                    {
                        existObj = false;
                        MsgSystem.Text = "Lỗi! Không tồn tại nhân viên!";
                        MsgSystem.CssClass = "msg-system warning";
                    }
                }                
            }
            catch
            {
                existObj = false;
                MsgSystem.Text = "Lỗi! Không tồn tại nhân viên!";
                MsgSystem.CssClass = "msg-system warning";
            }

            return existObj;
        }
        /// <summary>
        /// ddlSalon_SelectedIndexChanged
        /// </summary>
        protected void ddlSalon_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindSaff();
            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "text", "GetDateTimePicker()", true);

        }
        /// <summary>
        /// AddOrUpdate
        /// </summary>
        private void AddOrUpdate()
        {
            int interger;
            var obj = new Staff_Mistake();
            obj.StaffId = int.TryParse(ddlNhanVien.SelectedValue, out interger) ? interger : 0;
            obj.Description = ddlDescription.SelectedItem.Text;
            obj.FormMissTakeId = int.TryParse(ddlMisstake.SelectedValue, out interger) ? interger : 0;
            obj.Note = txtGhiChu.Text;
            if (txtPrice.Text != "")
                obj.Point = GetPrice(txtPrice.Text);
            else
                obj.Point = 0;
            obj.IsDelete = false;
            if (HDF_MainImg.Value != "")
                obj.Images = HDF_MainImg.Value;
            else
                obj.Images = "";
            if (!IsUpdate())
            {
                obj.CreateDate = Convert.ToDateTime(txtNgayTao.Text, culture);
            }
            else
            {
                obj.CreateDate = Convert.ToDateTime(txtNgayTao.Text);
            }


            var MsgParam = new List<KeyValuePair<string, string>>();

            if (IsUpdate())
            {
                obj.Id = Id;
                dbModel.Update(obj);

                //int pointToday = db.Database.SqlQuery<int>("select Coalesce(SUM(Coalesce(Point, 0)), 0) as point from Staff_Mistake where StaffId = " + _NhanVienId + " and IsDelete != 1 and CreateDate between '" + String.Format("{0:yyyy/MM/dd}", _NgayTao) + "' and '" + String.Format("{0:yyyy/MM/dd}", _NgayTao.AddDays(1)) + "'").FirstOrDefault();

                SetValueFlowSalary();
                MsgParam.Add(new KeyValuePair<string, string>("msg_add_status", "success"));
                MsgParam.Add(new KeyValuePair<string, string>("msg_add_message", "Cập nhập thành công!"));

                UIHelpers.Redirect("/admin/diem-tru/danh-sach.html", MsgParam);
            }

            else
            {
                dbModel.Add(obj);
                SetValueFlowSalary();

                MsgParam.Add(new KeyValuePair<string, string>("msg_add_status", "success"));
                MsgParam.Add(new KeyValuePair<string, string>("msg_add_message", "Thêm mới thành công!"));
                UIHelpers.Redirect("/admin/diem-tru/danh-sach.html", MsgParam);
            }
        }
        /// <summary>
        /// Update Thưởng/phạt vào bảng lương
        /// </summary>
        protected void SetValueFlowSalary()
        {
            DateTime NgayChamcong = DateTime.Now;
            FlowSalary objFlowSalary = new FlowSalary();

            if (!string.IsNullOrEmpty(txtNgayTao.Text))
            {
                if (!IsUpdate())
                {
                    NgayChamcong = Convert.ToDateTime(txtNgayTao.Text, culture);
                }
                else
                    NgayChamcong = Convert.ToDateTime(txtNgayTao.Text);
            }
            var nhanVienId = Convert.ToInt32(ddlNhanVien.SelectedItem.Value);

            using (Solution_30shineEntities db = new Solution_30shineEntities())
            {
                try
                {

                    objFlowSalary = db.FlowSalaries.Where(o => o.staffId == nhanVienId && o.sDate == NgayChamcong && o.IsDelete == 0).FirstOrDefault();

                    int? MistakePoint = getSumMistake(1, NgayChamcong, nhanVienId);
                    int? MistakeTemporary = getSumMistake(3, NgayChamcong, nhanVienId);
                    int? MistakeOrther = getSumMistake(4, NgayChamcong, nhanVienId);
                    int? MistakeInsurrance = getSumMistake(5, NgayChamcong, nhanVienId);
                    int? MistakeExtra = getSumMistake(7, NgayChamcong, nhanVienId);

                    if (objFlowSalary != null)
                    {
                        objFlowSalary.Mistake_Point = MistakePoint;
                        objFlowSalary.MistakeExtra = MistakeExtra;
                        objFlowSalary.MistakeTemporary = MistakeTemporary;
                        objFlowSalary.MistakeOrther = MistakeOrther;
                        objFlowSalary.MistakeInsurrance = MistakeInsurrance;

                        db.FlowSalaries.AddOrUpdate(objFlowSalary);
                        var exc = db.SaveChanges();
                    }
                }
                catch (Exception ex)
                {

                    throw ex;
                }
            }

        }
        /// <summary>
        /// Cộng dồn Thưởng/phạt trong ngày, để update vào flowsalary
        /// </summary>
        /// <param name="idFormMistake"></param>
        /// <param name="ngayTao"></param>
        /// <param name="nhanVienId"></param>
        /// <returns></returns>
        protected int? getSumMistake(int idFormMistake, DateTime ngayTao, int nhanVienId)
        {
            using (Solution_30shineEntities db = new Solution_30shineEntities())
            {
                int? sum = 0;
                try
                {
                    sum = db.Staff_Mistake.Where(o => o.StaffId == nhanVienId && o.IsDelete == false &&
                                    o.CreateDate == ngayTao && o.FormMissTakeId == idFormMistake)
                                    .Select(o => o.Point).Sum();
                }
                catch
                {
                    sum = 0;
                }
                return sum != null ? sum : 0;
            }
        }
        /// <summary>
        /// Button
        /// </summary>
        protected void AddStaff(object sender, EventArgs e)
        {
            AddOrUpdate();
        }
        /// <summary>
        /// Convert số tiền có dấu phẩy sang dạng Number
        /// </summary>
        /// <param name="strPrice"></param>
        /// <returns></returns>
        private int GetPrice(string strPrice)
        {
            string temp = "";
            try
            {
                for (int i = 0; i <strPrice.Length; i++)
                {
                    if (strPrice[i] != ',')
                    {
                        temp += strPrice[i];
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return Convert.ToInt32(temp.Trim());

        }
        /// <summary>
        /// Hàm kiểm tra Nhân viên đã chấm công trong bảng lương hay chua!!!
        /// </summary>
        /// <param name="SalaryDate"></param>
        /// <param name="StaffID"></param>
        /// <returns></returns>
        [WebMethod]
        public static object CheckFlowSalary(DateTime SalaryDate, int StaffID)
        {
            var message = new Library.Class.cls_message();
            using (var db = new Solution_30shineEntities())
            {
                try
                {
                    var data = db.FlowSalaries.Where(o => o.staffId == StaffID && o.sDate == SalaryDate).FirstOrDefault();
                    if (data==null)
                    {
                        message.success = false;
                        message.message = "Nhân viên này chưa chấm công.";
                    }
                    else
                    {
                        message.success = true;
                        message.message = "Nhân viên đã chấm công";
                    }
                    
                }
                catch (Exception ex)
                {

                    throw ex;
                }
                return message;
            }
        }
        /// <summary>
        /// check permission
        /// </summary>
        private void CheckPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                string[] Allow = new string[] { "root", "admin" };
                string[] AllowViewAllDate = new string[] { "root", "admin" };
                string[] Root = new string[] { "root" };
                var Permission = Session["User_Permission"].ToString().Trim();
                if (Array.IndexOf(Allow, Permission) != -1)
                {
                    Perm_Access = true;
                }

                if (Array.IndexOf(AllowViewAllDate, Permission) != -1)
                {
                    Perm_ViewAllData = true;
                }
                if (Array.IndexOf(Root, Permission) != -1)
                {
                    isRoot = true;
                }
            }

            // Call execute function
            ExecuteByPermission();
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }


    }
}