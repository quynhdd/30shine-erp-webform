﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="ImportExcelSalaryIncomechange.aspx.cs" Inherits="_30shine.GUI.BackEnd.Quanlity.ImportExcelSalaryIncomechange" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="/Assets/js/libs.xlsx/libs-xlsx-0.8.0-jszip.js"></script>
    <script src="/Assets/js/libs.xlsx/libs-xlsx-0.8.0-xlsx.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <style>
            .wp-page {
                margin: auto;
                width: 50%;
                padding: 10px;
            }

            .wp_table {
                overflow: scroll;
                width: 100%;
                float: left;
                height: 700px;
            }

            .wp_import-file-excel {
                padding-top: 10px;
            }

            .container {
                background-color: #e6e6e6;
            }

            .cls_table {
                padding: 5px;
                width: 100%;
                overflow: scroll;
            }


            .cls_table, td, th {
                border: 1px solid black;
                padding: 10px;
                min-width: 100px;
                text-align: center;
            }

                .cls_table, td:first-child, th:first-child {
                    min-width: 30px
                }

            .h2-cls-excel {
                text-align: center;
                font-size: 20px;
                padding: 10px;
                text-transform: uppercase;
            }

            .h1-title-timport-file-excel {
                float: left;
                font-size: 20px;
                text-transform: uppercase;
                margin-bottom: 10px;
            }
            /*.wp_import-file-excel { width: 100%; float: left; text-align: center; }*/
            #my_file_input {
            }

                #my_file_input::-webkit-file-upload-button {
                    color: red;
                }

                #my_file_input::before {
                    color: red;
                    background: none;
                    border: none;
                }

            .wp-submit {
                float: left;
                margin: 15px 0px 20px;
                text-align: right;
            }

            #my_file_input {
                float: left;
            }

            .cls_tempfile {
                float: left;
                text-decoration: underline !important;
                color: #0277bd;
                margin-left: 30px;
            }
        </style>
        <div class="wp-page">
            <div class="container">
                <div class="wp_import-file-excel">
                    <input type="file" id="my_file_input" />
                    <div id='my_file_output'></div>
                    <a href="/TemplateFile/ImportExcelBCKD/MAU_FILE_UPLOAD_TIEN_BH_UL_THUETHUNHAP.xlsx" class="cls_tempfile">Tải File Excel Import MẪu</a>
                </div>
                <div class="col-xs-6">
                    <h2 class="h2-cls-excel">Thông tin Import file excel</h2>
                </div>
                <div class="wp_table">
                    <table class="cls_table" id="table-item">
                        <thead>
                            <tr>
                                <th style="width: 20px">STT</th>
                                <th>StaffId</th>
                                <th>Slug key</th>
                                <th>Total Money</th>
                                <th>WorkDate </th>
                            </tr>
                        </thead>
                        <tbody id="body-content">
                        </tbody>
                    </table>
                </div>
                <div class="wp-submit">
                    <a href="javascript://" onclick="UploadData($(this))" class="btn btn-success">UpLoad File</a>
                </div>
            </div>
        </div>
        <div class="page-loading">
            <p>Vui lòng đợi trong giây lát...</p>
        </div>
        <script type="text/javascript">
            $(document).ready(function () {
                var maxheight = $(window).height();
                $('.wp_table').css('max-height', maxheight);
            });

            var objListData = {};
            var FileIn;
            $(function () {
                FileIn = document.getElementById('my_file_input');
                if (FileIn.addEventListener) {
                    FileIn.addEventListener('change', FilePicked, false);
                }
            });

            // read data in file excel
            function FilePicked(data) {
                // Get The File From The Input
                var oFile = data.target.files[0];
                var sFilename = oFile.name;
                // Create A File Reader HTML5
                var reader = new FileReader();
                // Ready The Event For When A File Gets Selected
                reader.onload = function (e) {
                    var data = e.target.result;
                    var cfb = XLSX.read(data, { type: 'binary' });
                    // Here is your object
                    var XL_row_object = XLSX.utils.sheet_to_row_object_array(cfb.Sheets["Sheet1"]);
                    var obj = XLSX.utils.sheet_to_row_object_array()
                    objListData = XL_row_object;
                    console.log("XL_row_object", XL_row_object);

                    // append html 
                    if (objListData.length > 0) {
                        AppendData(objListData);
                    }
                    else {
                        $("#body-content").html('');
                        objListData = {};
                        alert("Bạn vui lòng check lại file excel");
                    }
                };
                reader.readAsBinaryString(oFile);
            }

            // append html
            function AppendData(objData) {
                var str = "";
                var total = 0;
                $("#body-content").html('');
                const count = objData.length;
                var stt = 0;
                if (count > 0) {
                    for (var i = 0; i < count; i++) {
                        total = total + parseFloat(objData[i].ElectricityAndWaterBill);
                        stt++;
                        str += '<tr> ' +

                            //stt
                            '<td>' +
                            stt +
                            ' </td>' +
                            // staffId
                            '<td>' +
                            objData[i].StaffId +
                            ' </td>' +
                            // key
                            '<td>' +
                            objData[i].ChangedType +
                            ' </td>' +
                            // Total money
                            '<td>' +
                            objData[i].Point +
                            ' </td>' +
                            // date
                            '<td>' +
                            objData[i].WorkDate +
                            ' </td>' +

                            '</tr>';
                    }
                    if (str != '') {
                        $("#body-content").append(str);
                    }
                }
            }

            // function update data
            function UploadData(This) {

                if (objListData.length > 0) {
                    startLoading();
                    var data = JSON.stringify({ data: objListData });
                    $.ajax({
                        type: "POST",
                        url: "/GUI/BackEnd/Quanlity/ImportExcelSalaryIncomechange.aspx/InsertDataSalaryIncomechange",
                        data: data,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            alert("Import file thành công!");
                            finishLoading();
                            location.reload();
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            var responseText = jqXHR.responseJSON.message;
                            objListDailyExpense = {};
                            alert("Lỗi :  " + responseText + '! Bạn vui lòng check lại file excel');
                            location.reload();
                        }
                    });
                }
            }

        </script>
    </asp:Panel>
</asp:Content>
