﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/TemplateMaster/SiteMaster.Master" CodeBehind="Staff_Mistake_5C.aspx.cs" Inherits="_30shine.GUI.BackEnd.Quanlity.Staff_Mistake_5C" %>

<asp:Content ID="Mistake5C" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <style>
        </style>

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="Ul1">
                        <li>Thưởng Phạt 5C &nbsp;&#187; </li>
                        <li class="li-listing"><a href="/">Cập nhật</a></li>
                        <%--<li class="li-add active"><a href="/admin/diem-tru/them-moi.html">Thêm mới</a></li>--%>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing be-report">
            <div class="wp960 content-wp">
                <div class="row">
                    <div class="filter-item" style="margin-left: 0;">
                        <strong class="st-head" style="margin-right: 10px;"><i class="fa fa-clock-o"></i>Thời gian</strong>
                        <div class="datepicker-wp">
                            <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeFrom" placeholder="Chọn ngày" Style="width: 150px;" ClientIDMode="Static" runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="ddlSalon" CssClass="form-control" runat="server" ClientIDMode="Static" Style="width: 150px;"></asp:DropDownList>
                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="ddlDepartment" CssClass="form-control" runat="server" ClientIDMode="Static" Style="width: 150px;"></asp:DropDownList>
                    </div>
                    <asp:Button ID="btnXem" Width="120px" ClientIDMode="Static" runat="server" class="st-head btn-viewdata" Text="Xem dữ liệu"   OnClientClick="TempClick()" OnClick="GetListStaff5C_Click" />
                    <button type="button" style="width: 120px" id="btnAutofill" class="st-head btn-viewdata" value="Tự động điền" onclick="autoFillPointReason();">Tự động điền</button>
                    <asp:Button ID="btnHoantat" Width="120px" ClientIDMode="Static" runat="server" class="st-head btn-viewdata" Text="Hoàn tất" OnClientClick="TempClick()" OnClick="AddOrUpdate5C_Click" />
                    
                </div>

                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <div class="row table-wp">
                    <table class="table-add table-listing report-sales-listing">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Tên</th>
                                <th>Bộ phận</th>
                                <th style="width: 20%">Điểm</th>
                                <th>Lý do</th>
                                <th>Ghi chú</th>
                            </tr>
                        </thead>
                        <tbody id="list5C">
                        </tbody>
                    </table>
                </div>
            </div>
            <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
            <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                <ContentTemplate>
                </ContentTemplate>
                 <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnXem" EventName="Click" />
                     <asp:AsyncPostBackTrigger ControlID="btnHoantat" EventName="Click" />
                    </Triggers>
            </asp:UpdatePanel>

            <asp:HiddenField ID="HDF_SalonValue" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="HDF_DepartmentValue" runat="server"  ClientIDMode="Static"/>
            <asp:HiddenField ID="HDF_WorkDate" runat="server" ClientIDMode="Static"/>
            
        </div>
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 180px !important;
                margin-top: 5px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }
        </style>
        <script>
            jQuery(document).ready(function () {
                GetDateTimePicker();
            });
            function GetDateTimePicker() {
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) {
                    }
                });
            }
            //call lên server để tạo loadding
            function TempClick() {
                addLoading();
                return true;
            }
            //Bind dữ liệu
            function GetListStaff5C() {
                var SalonId =$("#ddlSalon").val();
                var Department = $("#ddlDepartment").val();
                var WorkDate = $("#TxtDateTimeFrom").val();

                if (SalonId == "0" || WorkDate == "" || SalonId == "0") {
                    removeLoading();
                    if (SalonId == "0" || SalonId == "0") showMsgSystem("Chưa nhập salon ", "success");
                    if (WorkDate == "" || WorkDate == "0" ) showMsgSystem("Chưa nhập ngày ", "success");
                }
                else {

                    var data = {
                        _WorkDate: WorkDate, _salonId: SalonId, _DepartmentId: Department
                    };
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "/GUI/BackEnd/Quanlity/Staff_Mistake_5C.aspx/GetListStaff5C",
                        data: JSON.stringify(data),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json", success: function (response) {
                            if (response.d.success && response.d.data != "") {
                                console.log(response.d);
                                BindList5C(response.d.data);
                            }
                            else {
                                console.log(response.d)
                                $('#list5C').html("");
                            }
                        },
                        failure: function (response) { alert(response.d); }
                    });
                    removeLoading();
                }
            }
            //Show thông báo lên màn hình
            function showMsgSystem(msg, status) {
                $("#MsgSystem").css("opacity", 0).text(msg).addClass(status).fadeTo(0, 1);
                setTimeout(function () {
                    $("#MsgSystem").fadeTo("slow", 0, function () {
                        $("#MsgSystem").text("").attr("class", "msg-system").css("opacity", 1);
                    });
                }, 5000);
                $('html, body').animate({ scrollTop: 0 }, 'fast');

            }
            //Lấy list hình thức 5C
            function getFormIncomeChange() {
                var data = [];
                $.ajax({
                    type: "POST",
                    async: false,
                    url: "/GUI/BackEnd/Quanlity/Staff_Mistake_5C.aspx/GetIncomgeChange",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        data = response.d;
                    },
                    failure: function (response) { alert(response.d); }
                });
                return data;
            }
            //Nút tự động điền hình thức tốt và cộng 1 điểm.
            function autoFillPointReason() {
                
                var data = $(".Staff5C");
                $.each(data, function (key, value) {
                    $(this).find(".OPReason").val(16);
                    $(this).find(".OPPoint").val(1);
                })
            }
            //Thêm hoặc update 5C
            function AddOrUpdate5C(This) {
                var data = "";
                //trường hợp hoàn tất. xử lý toàn bộ bản ghi
                if (This === 'undefined') {
                    data = $(".Staff5C");
                }
                else {
                    //Trường hợp sự kiện onchange 1 bản ghi
                    data = $(This).parent().parent();
                }
                console.log(data);
                var data = $(".Staff5C");
                $.each(data, function (key, value) {
                    debugger;
                    var RatingSalary = $(this).data("rating-salary");
                    var InComeChangeId = $(this).data("id-incomge-change");
                    var StaffName = $(this).find("td.StaffName").text();
                    var StaffId = $(this).find("td.StaffId").text();
                    var Department = $(this).find("td.Department").text();
                    var OPReason = $(this).find(".OPReason").val();
                    if (OPReason == "-1000") {
                        OPReason = null; return true;
                    }
                    var OPPoint = $(this).find(".OPPoint").val();
                    if (OPPoint == "-1000") {
                        OPPoint = null; return true;
                    }
                    var Decription = $(this).find("td.Decription :input").val();
                    var salonId = $("#ddlSalon").val();
                    var department = $("#ddlDepartment").val();
                    var WorkDate = $("#TxtDateTimeFrom").val();
                    var data = { id: InComeChangeId, staffId: StaffId, salonId: salonId, changedTypeId: OPReason, scoreFactor: OPPoint, workDate: WorkDate, decription: Decription, ratingSalary: RatingSalary };

                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "/GUI/BackEnd/Quanlity/Staff_Mistake_5C.aspx/InsertOrUpdate5C",
                        data: JSON.stringify(data),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json", success: function (response) {
                        },
                        failure: function (response) { alert(response.d); }
                    });
                });
                removeLoading();
                showMsgSystem("Hoàn tất thành công", "success");
            }
            //Bind dữ liệu vào bảng
            function BindList5C(data) {
                var strTB5C = "";
                var data5C = data;
                var ArrOptionPoint = [-1000, +1, 0, -1];
                var ArrOptionReason = getFormIncomeChange();

                for (var i = 0; i < data5C.length; i++) {
                    strTB5C += '<tr class="Staff5C" data-id-incomge-change="' + data5C[i].InComeChangeId + '"' + 'data-rating-salary="' + data5C[i].RatingSalary + '">' +
                        '<td class="StaffId">' + data5C[i].StaffId + '</td>' +
                        '<td class="StaffName">' + data5C[i].StaffName + '</td>' +
                        '<td class="Department">' + data5C[i].Department + '</td>' +
                        '<td style="width: 10%">' +
                        '<select class="OPPoint" style="width: 100%;margin-left: auto !important">';
                    //bind option điểm (Point)
                    for (var j = 0; j < ArrOptionPoint.length; j++) {
                        if (j == 0) {
                            strTB5C += '<option value=' + ArrOptionPoint[j] + '>Chọn điểm</option>'
                        }
                        else {
                            strTB5C += '<option value="' + ArrOptionPoint[j] + '"';
                            if (ArrOptionPoint[j] == data5C[i].ScoreFactor) {
                                strTB5C += 'selected >';
                            }
                            else { strTB5C += '>'; }
                            strTB5C += ArrOptionPoint[j] + '</option >';
                        }
                    }
                    strTB5C += '</select>' +
                        '</td>' +
                        '<td style="width: 15%">' +
                        '<select class="OPReason" style="width: 100%;margin-left: auto !important">';
                    //bind option lý do
                    for (var f = 0; f < ArrOptionReason.length; f++) {
                        if (f == 0) { strTB5C += '<option value="-1000">Chọn lý do</option>'; }
                        strTB5C += '<option value="' + ArrOptionReason[f].Id;
                        if (data5C[i].ChangedTypeId == ArrOptionReason[f].Id) { strTB5C += '"selected>' }
                        else { strTB5C += '">' }
                        strTB5C += ArrOptionReason[f].Formality + '</option>';

                    }
                    strTB5C += '</select>' +
                        '</td>' +
                        '<td class="Decription"><input type="text" onchange="AddOrUpdate5C(this)" style="margin-left: auto !important" ';
                    strTB5C += data5C[i].Description == null ? '' : ('value="' + data5C[i].Description + '"');
                    strTB5C += '></td></tr>';
                    $('#list5C').html(strTB5C);
                }
            }
        </script>
    </asp:Panel>
</asp:Content>
