﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Staff_Mistake_Add.aspx.cs" Inherits="_30shine.GUI.BackEnd.Quanlity.Staff_Mistake_Add" %>


<asp:Content ID="ProductAdd" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <script src="../../../Assets/js/S4M.js"></script>
        <style>
            #Send {
                width: 80px !important;
                height: 30px;
                line-height: 30px;
                float: left;
                width: auto;
                background: #000000;
                color: #ffffff;
                text-align: center;
                cursor: pointer;
                -webkit-border-radius: 2px;
                -moz-border-radius: 2px;
                border-radius: 2px;
                margin-left: 204px;
                margin-top: 30px;
            }

            #txtGhiChu {
                padding-top: 8px;
            }
        </style>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="Ul1">
                        <li>Phạt &nbsp;&#187; </li>
                        <li class="li-listing"><a href="/admin/diem-tru/danh-sach.html">Danh sách</a></li>
                        <li class="li-add active"><a href="/admin/diem-tru/them-moi.html">Thêm mới</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

        <div class="wp customer-add admin-product-add">
            <%-- Add --%>
            <div class="wp960 content-wp">
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <div class="table-wp">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <table class="table-add admin-product-table-add">
                                <tbody>
                                    <tr class="title-head">
                                        <td><strong>Thông tin Phạt</strong></td>
                                        <td></td>
                                    </tr>

                                    <tr class="tr-field-ahalf">
                                        <td class="col-xs-3 left"><span>Salon</span></td>
                                        <td class="col-xs-9 right">
                                            <span class="field-wp">
                                                <asp:DropDownList ID="ddlSalon" CssClass="form-control select" runat="server" ClientIDMode="Static" AutoPostBack="true" OnSelectedIndexChanged="ddlSalon_SelectedIndexChanged"></asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="ddlSalon" runat="server" CssClass="fb-cover-error" InitialValue="0" Text="Bạn chưa chọn tên Salon!"></asp:RequiredFieldValidator>
                                            </span>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="col-xs-3 left"><span>Chọn nhân viên</span></td>
                                        <td class="col-xs-9 right">
                                            <span class="field-wp">
                                                <asp:DropDownList ID="ddlNhanVien" CssClass="form-control" runat="server" ClientIDMode="Static" Width="49%">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="ddlNhanVien" runat="server" CssClass="fb-cover-error" InitialValue="0" Text="Bạn chưa chọn tên nhân viên!"></asp:RequiredFieldValidator>
                                            </span>
                                        </td>
                                    </tr>

                                    <tr class="tr-field-ahalf">
                                        <td class="col-xs-3 left"><span>Hình thức</span></td>
                                        <td class="col-xs-9 right">
                                            <span class="field-wp">
                                                <asp:DropDownList ID="ddlMisstake" CssClass="form-control" runat="server" ClientIDMode="Static">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="ddlMisstake" runat="server" CssClass="fb-cover-error" InitialValue="0" Text="Bạn chưa chọn hình thức!"></asp:RequiredFieldValidator>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr class="tr-field-ahalf">
                                        <td class="col-xs-3 left"><span>Loại lỗi</span></td>
                                        <td class="col-xs-9 right">
                                            <span class="field-wp" style="height: auto;">
                                                <asp:DropDownList ID="ddlDescription" CssClass="form-control" runat="server" ClientIDMode="Static">
                                                </asp:DropDownList>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr class="tr-field-ahalf" id="price">
                                        <td class="col-xs-3 left"><span>Số tiền</span></td>
                                        <td class="col-xs-9 right">
                                            <span class="field-wp">
                                                <asp:TextBox ID="txtPrice" CssClass="form-control" runat="server" ClientIDMode="Static" placeholder="vd: 100,000" onkeyup="reformatText(this)" onkeypress="return isNumber(event)"></asp:TextBox>
                                            </span>
                                        </td>
                                    </tr>

                                    <tr class="tr-upload">
                                        <td class="col-xs-3 left"><span>Ảnh</span></td>
                                        <td class="col-xs-7 right">
                                            <div class="wrap btn-upload-wp HDF_Images">
                                                <div id="Btn_UploadImg" class="btn-upload-img" onclick="popupImageIframe('HDF_MainImg')">Chọn ảnh </div>
                                                <asp:FileUpload ID="UploadFile" ClientIDMode="Static" Style="display: none;"
                                                    CssClass="input-file-upload" runat="server" AllowMultiple="true" />
                                                <div class="wrap listing-img-upload">
                                                    <% if (HasImages)
                                                        { %>
                                                    <% for (var i = 0; i < ListImagesUrl.Count; i++)
                                                        { %>
                                                    <div class="thumb-wp">
                                                        <img class="thumb" alt="" title="" src="<%=ListImagesUrl[i] %>"
                                                            data-img="<%=ListImagesUrl[i] %>" />
                                                        <span class="delete-thumb" onclick="deleteThumbnail($(this), 'HDF_MainImg')"></span>
                                                    </div>
                                                    <% } %>
                                                    <% } %>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="tr-field-ahalf">
                                        <td class="col-xs-3 left"><span>Ngày tạo</span></td>
                                        <td class="col-xs-9 right">
                                            <span class="field-wp">
                                                <asp:TextBox CssClass="form-control txtdatetime" ID="txtNgayTao" placeholder="Từ ngày" ClientIDMode="Static" runat="server"></asp:TextBox>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr class="tr-field-ahalf" id="ghichu">
                                        <td class="col-xs-3 left"><span>Ghi chú</span></td>
                                        <td class="col-xs-9 right">
                                            <span class="field-wp">
                                                <asp:TextBox ID="txtGhiChu" CssClass="form-control" runat="server" TextMode="multiline" Rows="5" ClientIDMode="Static"></asp:TextBox>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr class="tr-send">
                                        <td class="col-xs-3 left"></td>
                                        <td class="col-xs-9 right no-border">
                                            <span class="field-wp">
                                                <span class="error-msg">
                                                    <asp:Literal Text="" Visible="false" ID="ltrMsg" runat="server" /></span>
                                            </span>
                                        </td>
                                    </tr>

                                    <asp:HiddenField runat="server" ID="UrlKeyField" ClientIDMode="Static" />
                                    <asp:HiddenField runat="server" ID="ImgCoverField" ClientIDMode="Static" />
                                    <asp:HiddenField runat="server" ID="HDF_MainImg" ClientIDMode="Static" />
                                    <asp:HiddenField ID="HiddenField1" runat="server" />
                                </tbody>
                            </table>
                            <asp:Button ID="Send" ClientIDMode="Static" runat="server" Text="Hoàn Tất" OnClick="AddStaff" OnClientClick="return CheckFlowSalary();" />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ddlSalon" EventName="SelectedIndexChanged" />
                            <asp:AsyncPostBackTrigger ControlID="Send" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>

                </div>
            </div>
            <%-- end Add --%>
        </div>
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 49% !important;
                margin-top: 5px !important;
            }
            .customer-add .table-add td span, .customer-add .table-add td input[type="text"], .customer-add .table-add td select{
                  height: 36px !important;
            }
            .select2-container--default .select2-selection--single {
                padding-top: 1px !important;
            }
        </style>
        <script>
            jQuery(document).ready(function () {
                $("#glbQuanlity").addClass("active");
                $("#glbMistake").addClass("active");
                $("#subMenu").addClass("active");
                GetDateTimePicker();
            });

            function GetDateTimePicker() {
                $('#txtNgayTao').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    scrollMonth: true,
                    scrollTime: false,
                    scrollInput: false,
                    onChangeDateTime: function (dp, $input) {
                    }
                });
            }
        </script>

        <!-- Popup plugin image -->
        <script type="text/javascript">
            function popupImageIframe(StoreImgField) {

                var Width = 960;
                var Height = 560;
                var ImgList = [];
                var iframe = $("<div class='iframe-image-upload' style='width:" + Width + "px; height:" + Height + "px; float:left; display:none;'><iframe src='/GUI/BackEnd/UploadImage/Upload_V2.aspx?Code=img&StoreImgField=" + StoreImgField + "&callback=getThumbFromIframe&imgWidth=800&imgHeight=600&multi_img=false&Folder=NhanVien' style='width:100%; height: 100%;'></iframe></div");

                $("body").find(".iframe-image-upload").remove().end().append(iframe).find(".iframe-image-upload").openEBPopup();
            }

            window.getThumbFromIframe = function (Imgs, StoreImgField) {
                debugger;
                var imgs = "";
                var thumb = "";
                if (Imgs.length > 0) {
                    thumb = '<div class="thumb-wp">' +
                        '<img class="thumb" alt="" title="" src="' + Imgs[0] + '"' +
                        'data-img="" />' +
                        '<span class="delete-thumb" onclick="deleteThumbnail($(this), \'' + StoreImgField + '\')"></span>' +
                        '</div>';
                    $("." + StoreImgField).find(".listing-img-upload").empty().append(thumb);
                    excThumbWidth(120, 120);
                    $("#" + StoreImgField).val(Imgs[0]);
                }
                autoCloseEBPopup();
            }

            function deleteThumbnail(This, StoreImgField) {
                var imgs = "";
                var lst = This.parent().parent();
                This.parent().remove();
                lst.find("img.thumb").each(function () {
                    imgs += $(this).attr("src") + ",";
                });
                $("#" + StoreImgField).val(imgs);
            }
            function initThumbnail(StoreImgField) {
                var imgs = "";
                var lst = $("." + StoreImgField).find("img.thumb");
                lst.each(function () {
                    imgs += $(this).attr("src") + ",";
                });
                $("#" + StoreImgField).val(imgs);
            }
            // hàm kiểm tra xem, nhân viên đã chấm công chưa. nếu chấm công thì trả về true, ngược lại trả về false, ngừng hoạt động thêm mới/update vào csdl
            function CheckFlowSalary() {

                var stringDate = $('#txtNgayTao').val().split("/");
                var SalaryDate = stringDate[2] + "/" + stringDate[1] + "/" + stringDate[0];
                var StaffId = $('#ddlNhanVien').val();
                var Message = $('#ddlSalon').val() == '0' ? "Bạn chưa chọn Salon--- " : "";
                Message += $('#ddlNhanVien').val() == '0' ? "Bạn chưa chọn Nhân viên---" : Message;
                Message += $('#ddlMisstake').val() == '0' ? "Bạn chưa chọn hình thức" : Message;
                if (Message == "") {
                    //test Flowsalary
                    var testFS = true;
                    $.ajax({
                        type: "post",
                        async: false,
                        url: "/GUI/BackEnd/Quanlity/Staff_Mistake_Add.aspx/CheckFlowSalary",
                        data: "{SalaryDate:'" + SalaryDate + "',StaffID:'" + StaffId + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (respone) {
                            if (respone.d.success == false) {
                                alert("Nhân viên này chưa chấm công!");
                                testFS = false;
                            }

                        },
                    });

                    if (testFS == true) { addLoading(); return true; }
                    else
                        return testFS;
                }
                else {
                    alert(Message);
                    return false;

                }
            }

        </script>



    </asp:Panel>
</asp:Content>
