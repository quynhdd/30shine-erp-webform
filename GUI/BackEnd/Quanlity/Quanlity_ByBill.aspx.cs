﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Globalization;
using System.Web.Script.Serialization;
using ExportToExcel;
using Excel;
using LinqKit;
using System.Data.Entity.Migrations;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.Quanlity
{
    public partial class Quanlity_ByBill : System.Web.UI.Page
    {
        protected Paging PAGING = new Paging();
        protected List<List<ProductBasic>> ProductList;
        private Expression<Func<BillService, bool>> Where = PredicateBuilder.True<BillService>();
        CultureInfo culture = new CultureInfo("vi-VN");

        private string PageID = "";
        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        protected bool Perm_ViewAllData = false;
        protected bool Perm_ViewAllDate = false;
        private bool Perm_Access = false;
        protected bool IsAccountant = false;
        List<_30shine.MODEL.ENTITY.EDMX.Staff> _LstNhanVien;
        protected string Day1 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-2));
        protected string Day2 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-1));
        protected string Day3 = string.Format("{0:dd/MM/yyyy}", DateTime.Now);

        protected string table = "";

        private string sql = "";
        private string where = "";
        private DateTime timeFrom = new DateTime();
        private DateTime timeTo = new DateTime();
        private int salonId = 0;
        protected int qstaffType;
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //Perm_ViewAllDate = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllDate", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_ViewAllDate = permissionModel.CheckPermisionByAction("Perm_ViewAllDate", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        private void ExecuteByPermission()
        {

            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (!IsPostBack)
            {
                Library.Function.bindSalon(new List<DropDownList> { Salon }, Perm_ViewAllData);
                //Bind_StaffType();
                GenWhere();
                Bind_Data();
            }
            else
            {
                GenWhere();
            }
        }

        protected void FillLstStaff()
        {
            using (var db = new Solution_30shineEntities())
            {
                _LstNhanVien = db.Staffs.ToList();
            }
        }

        private void GenWhere()
        {
            int integer;
            salonId = int.TryParse(Salon.SelectedValue, out integer) ? integer : 0;
            int staffType = int.TryParse(StaffType.SelectedValue, out integer) ? integer : 0;
            int staffId = int.TryParse(HDF_StaffId.Value, out integer) ? integer : 0;
            timeFrom = Convert.ToDateTime("29/3/2016", culture);
            timeTo = DateTime.Now;

            if (TxtDateTimeFrom.Text != "")
            {
                timeFrom = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                if (TxtDateTimeTo.Text != "")
                {
                    timeTo = Convert.ToDateTime(TxtDateTimeTo.Text, culture);
                    if (timeTo.Year >= timeFrom.Year && timeTo.Month >= timeFrom.Month && timeTo.Day >= timeFrom.Day)
                    {
                        timeTo = timeTo.AddDays(1);
                    }
                }
                else
                {
                    timeTo = timeFrom.AddDays(1);
                }
            }            
        }

        private string genSql(int staffType)
        {
            var sql = "";
            var chemistryIds = "14,16,17,24";
            int coefficient = 1;
            int coefficient_spc = 2;
            string strStaffType = "";
            switch (staffType)
            {
                case 1:
                    coefficient = 1;
                    coefficient_spc = 2;
                    strStaffType = "Staff_Hairdresser_Id";
                    break;
                case 2:
                    coefficient = 1;
                    coefficient_spc = 2;
                    strStaffType = "Staff_HairMassage_Id";
                    break;
                case 3:
                    coefficient = 1;
                    coefficient_spc = 1;
                    strStaffType = "ReceptionId";
                    break;
                default: break;
            }
            sql = @"declare @coefficient float;
                    declare @coefficient_spc float;
                    declare @timeFrom nvarchar(50);
                    declare @timeTo nvarchar(50);

                    set @coefficient = " + coefficient + @";
                    set @coefficient_spc = " + coefficient_spc + @";
                    set @timeFrom = '" + String.Format("{0:yyyy/MM/dd}", timeFrom) + @"';
                    set @timeTo = '" + String.Format("{0:yyyy/MM/dd}", timeTo) + @"';
                            
                    select f.*, g.Fullname as staffName
                    from
                    (
                        select 
	                        e."+ strStaffType + @" as staffId, e.Mark,
	                        Coalesce(SUM(e.IsSpecial), 0) as bill_special, 
	                        Coalesce(SUM(e.IsNormal), 0) as bill_normal,
	                        Coalesce(SUM(e.point), 0) as point
                        from
                        (
	                        select c.*,
			                        (Case
				                        when (/*c.IsCusFamiliar = 1 or */c.ServiceId is not null ) then c.ConventionPoint * @coefficient_spc
				                        when ( c.IsCusFamiliar = 0 and c.ServiceId is null ) then c.ConventionPoint * @coefficient
			                        end) as point,
			                        (Case
				                        when (/*c.IsCusFamiliar = 1 or */c.ServiceId is not null ) then 1
				                        when ( c.IsCusFamiliar = 0 and c.ServiceId is null ) then 0
			                        end) as IsSpecial,
			                        (Case
				                        when (/*c.IsCusFamiliar = 1 or */c.ServiceId is not null ) then 0
				                        when ( c.IsCusFamiliar = 0 and c.ServiceId is null ) then 1
			                        end) as IsNormal
	                        from
	                        (
		                        select 
			                        --a.*, b.ConventionPoint
			                        a.Id, a."+strStaffType+ @", a.Mark, a.IsCusFamiliar, b.ConventionPoint,
			                        d.ServiceId,
			                        ROW_NUMBER() OVER(PARTITION BY a.Id ORDER BY a." + strStaffType + @") AS Row
		                        from BillService as a
		                        left join Rating_ConfigPoint as b
		                        on a.Mark = b.RealPoint and b.Hint = 1
		                        left join FlowService as d
		                        on a.Id = d.BillId and d.ServiceId in (" + chemistryIds + @") and d.IsDelete != 1
		                        where a.IsDelete != 1 and a.Pending != 1
		                        and a.ServiceIds != '' and a.ServiceIds is not null
		                        and a.CreatedDate between @timeFrom and @timeTo
		                        and a." + strStaffType + @" > 0
	                        ) as c
	                        where c.Row = 1
                        )as e
                        group by e." + strStaffType + @", e.Mark
                    ) as f
                    inner join Staff as g
                    on f.staffId = g.Id
                    order by f.staffId asc, f.Mark desc";
            return sql;
        }

        private void Bind_Data()
        {
            using (var db = new Solution_30shineEntities())
            {
                var chemistryIds = "14,16,17,24";
                var sql = @"declare @coefficient float;
                            declare @coefficient_spc float;
                            declare @timeFrom nvarchar(50);
                            declare @timeTo nvarchar(50);

                            set @coefficient = 1;
                            set @coefficient_spc = 1.2;
                            set @timeFrom = '" + String.Format("{0:yyyy/MM/dd}", timeFrom) + @"';
                            set @timeTo = '" + String.Format("{0:yyyy/MM/dd}", timeTo) + @"';
                            
                            select f.*, g.Fullname as staffName
                            from
                            (
                                select 
	                                e.Staff_Hairdresser_Id as staffId, e.Mark,
	                                Coalesce(SUM(e.IsSpecial), 0) as bill_special, 
	                                Coalesce(SUM(e.IsNormal), 0) as bill_normal,
	                                Coalesce(SUM(e.point), 0) as point
                                from
                                (
	                                select c.*,
			                                (Case
				                                when (c.IsCusFamiliar = 1 or c.ServiceId is not null ) then c.ConventionPoint * @coefficient_spc
				                                when ( c.IsCusFamiliar = 0 and c.ServiceId is null ) then c.ConventionPoint * @coefficient
			                                end) as point,
			                                (Case
				                                when (c.IsCusFamiliar = 1 or c.ServiceId is not null ) then 1
				                                when ( c.IsCusFamiliar = 0 and c.ServiceId is null ) then 0
			                                end) as IsSpecial,
			                                (Case
				                                when (c.IsCusFamiliar = 1 or c.ServiceId is not null ) then 0
				                                when ( c.IsCusFamiliar = 0 and c.ServiceId is null ) then 1
			                                end) as IsNormal
	                                from
	                                (
		                                select 
			                                --a.*, b.ConventionPoint
			                                a.Id, a.Staff_Hairdresser_Id, a.Mark, a.IsCusFamiliar, b.ConventionPoint,
			                                d.ServiceId,
			                                ROW_NUMBER() OVER(PARTITION BY a.Id ORDER BY a.Staff_Hairdresser_Id) AS Row
		                                from BillService as a
		                                left join Rating_ConfigPoint as b
		                                on a.Mark = b.RealPoint and b.Hint = 1
		                                left join FlowService as d
		                                on a.Id = d.BillId and d.ServiceId in ("+chemistryIds+@")
		                                where a.IsDelete != 1 and a.Pending != 1
		                                and a.ServiceIds != '' and a.ServiceIds is not null
		                                and a.CreatedDate between @timeFrom and @timeTo
		                                and a.Staff_Hairdresser_Id > 0
	                                ) as c
	                                where c.Row = 1
                                )as e
                                group by e.Staff_Hairdresser_Id, e.Mark
                            ) as f
                            inner join Staff as g
                            on f.staffId = g.Id
                            order by f.staffId asc, f.Mark desc";
                var lst = db.Database.SqlQuery<cls_staffPoint>(sql).OrderByDescending(o => o.point).ToList();
                var index = -1;
                var lstStaff = new List<cls_staffRatingReport>();
                var item = new cls_staffRatingReport();
                if (lst.Count > 0)
                {
                    foreach (var v in lst)
                    {
                        item = new cls_staffRatingReport();
                        item.normalBill = new cls_ratingLevel();
                        item.specialBill = new cls_ratingLevel();
                        index = lstStaff.FindIndex(fi => fi.staffId == v.staffId);
                        if (index == -1)
                        {
                            item.staffId = v.staffId;
                            item.staffName = v.staffName;
                            switch (v.Mark)
                            {
                                case 3: item.normalBill.great = v.bill_normal; item.specialBill.great = v.bill_special; break;
                                case 2: item.normalBill.good = v.bill_normal; item.specialBill.good = v.bill_special; break;
                                case 1: item.normalBill.bad = v.bill_normal; item.specialBill.bad = v.bill_special; break;
                                case 0: item.normalBill.noRating = v.bill_normal; item.specialBill.noRating = v.bill_special; break;
                                default: break;
                            }
                            item.point = v.point;
                            lstStaff.Add(item);
                        }
                        else
                        {
                            item = lstStaff[index];
                            switch (v.Mark)
                            {
                                case 3: item.normalBill.great += v.bill_normal; item.specialBill.great += v.bill_special; break;
                                case 2: item.normalBill.good += v.bill_normal; item.specialBill.good += v.bill_special; break;
                                case 1: item.normalBill.bad += v.bill_normal; item.specialBill.bad += v.bill_special; break;
                                case 0: item.normalBill.noRating += v.bill_normal; item.specialBill.noRating += v.bill_special; break;
                                default: break;
                            }
                            item.point += v.point;
                            lstStaff[index] = item;
                        }
                    }
                }
                // Bind data
                Rpt_Servey.DataSource = lstStaff;
                Rpt_Servey.DataBind();
            }
        }

        //private void Bind_Salon()
        //{
        //    using (var db = new Solution_30shineEntities())
        //    {
        //        var _Salons = db.Tbl_Salon.Where(w => w.IsDelete != 1).OrderBy(o => o.Order).ToList();
        //        var Key = 0;
        //        var SalonId = Convert.ToInt32(Session["SalonId"]);

        //        Salon.DataTextField = "Salon";
        //        Salon.DataValueField = "Id";

        //        ListItem item = new ListItem("Chọn Salon", "0");
        //        Salon.Items.Insert(0, item);

        //        foreach (var v in _Salons)
        //        {
        //            Key++;
        //            item = new ListItem(v.Name, v.Id.ToString());
        //            Salon.Items.Insert(Key, item);
        //        }
        //        Salon.SelectedIndex = 0;
        //    }
        //}

        private void Bind_StaffType()
        {
            using (var db = new Solution_30shineEntities())
            {
                var staffType = db.Staff_Type.Where(w => w.IsDelete != 1 && w.Publish == true).OrderBy(o => o.Id).ToList();
                var Key = 0;

                StaffType.DataTextField = "Type";
                StaffType.DataValueField = "Id";

                //ListItem item = new ListItem("Chọn bộ phận", "0");
                //StaffType.Items.Insert(0, item);

                ListItem item = new ListItem();
                foreach (var v in staffType)
                {
                    if (v.Id == 1 || v.Id == 2 || v.Id == 3)
                    {
                        item = new ListItem(v.Name, v.Id.ToString());
                        StaffType.Items.Insert(Key++, item);
                    }                    
                }
                StaffType.SelectedIndex = 0;
            }
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            //Bind_Paging();
            Bind_Data();
            ScriptManager.RegisterStartupScript(UPTableListing, this.GetType(), "Format Price", "UP_FormatPrice('.be-report-price'); SubTableFixHeight();SetActiveTableListing();", true);
            RemoveLoading();
        }

        protected void Bind_Paging()
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? (HDF_Page.Value != "" ? Convert.ToInt32(HDF_Page.Value) : 1) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage() : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage()
        {
            using (var db = new Solution_30shineEntities())
            {
                var Count = 0;
                var sql = genSql(qstaffType);
                if (sql != "")
                {
                    Count = db.Tbl_Staff_Survey.SqlQuery(sql).Count();
                }
                int TotalRow = Count - PAGING._TopNewsNum;
                int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
                return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
            }
        }

        protected void Btn_SwitchExcel(object sender, EventArgs e)
        {
            if (HDF_ExcelType.Value == "Export")
            {
                //Exc_ExportExcel();
            }
            else if (HDF_ExcelType.Value == "Import")
            {
                //Exc_ImportExcel();
            }

        }

        //        protected void Exc_ExportExcel(object sender, EventArgs e)
        //        {
        //            using (var db = new Solution_30shineEntities())
        //            {
        //                if (TxtDateTimeFrom.Text.Trim() != "")
        //                {
        //                    int Segment = !HDF_EXCELSegment.Value.Equals("") ? Convert.ToInt32(HDF_EXCELSegment.Value) : PAGING._Segment;
        //                    int PageNumber = IsPostBack ? (HDF_EXCELPage.Value != "" ? Convert.ToInt32(HDF_EXCELPage.Value) : 1) : 1;
        //                    PageNumber = PageNumber > 0 ? PageNumber : 1;
        //                    int Offset = (PageNumber - 1) * Segment;
        //                    DateTime d1 = Convert.ToDateTime(TxtDateTimeFrom.Text.Trim(), culture);
        //                    DateTime d2;
        //                    if (TxtDateTimeTo.Text.Trim() != "")
        //                    {
        //                        d2 = Convert.ToDateTime(TxtDateTimeTo.Text.Trim(), culture).AddDays(1);
        //                    }
        //                    else
        //                    {
        //                        d2 = d1.AddDays(1);
        //                    }
        //                    string timeFrom = String.Format("{0:MM/dd/yyyy}", d1);
        //                    string timeTo = String.Format("{0:MM/dd/yyyy}", d2);

        //                    var LST = Get_DataByDate(0, 1000000, "CreatedDate");

        //                    var _ExcelHeadRow = new List<string>();
        //                    var serializer = new JavaScriptSerializer();
        //                    var BillRowLST = new List<List<string>>();
        //                    var BillRow = new List<string>();

        //                    _ExcelHeadRow.Add("Ngày");
        //                    _ExcelHeadRow.Add("Tgian hoàn thành (phút)");
        //                    _ExcelHeadRow.Add("Tên KH");
        //                    _ExcelHeadRow.Add("Mã KH");
        //                    _ExcelHeadRow.Add("Số ĐT");
        //                    _ExcelHeadRow.Add("Tổng số lần sử dụng DV");
        //                    _ExcelHeadRow.Add("Đánh giá");

        //                    // Listing service
        //                    //var LST_Service = db.Services.Where(w=>w.IsDelete != 1).OrderByDescending(o => o.Id).ToList();
        //                    string sql_service = @"select b.* 
        //                                    from
        //                                    (
        //                                    select ServiceId from FlowService
        //                                    where IsDelete != 1
        //                                    and (CreatedDate between '" + timeFrom + "' and '" + timeTo + "')" +
        //                                        @"group by ServiceId
        //                                    ) as a
        //                                    join Service as b
        //                                    on a.ServiceId = b.Id";
        //                    var LST_Service = db.Services.SqlQuery(sql_service).ToList();
        //                    if (LST_Service.Count > 0)
        //                    {

        //                        foreach (var v in LST_Service)
        //                        {
        //                            _ExcelHeadRow.Add(v.Name + " ( " + v.Code + " ) ");
        //                        }
        //                        _ExcelHeadRow.Add("D.Thu Dịch vụ");
        //                    }

        //                    // Listing product
        //                    //var LST_Product = db.Products.Where(w => w.IsDelete != 1).OrderByDescending(o => o.Id).ToList();
        //                    string sql_product = @"select b.* 
        //                                    from
        //                                    (
        //                                    select ProductId from FlowProduct
        //                                    where IsDelete != 1
        //                                    and (CreatedDate between '" + timeFrom + "' and '" + timeTo + "')" +
        //                                        @"group by ProductId
        //                                    ) as a
        //                                    join Product as b
        //                                    on a.ProductId = b.Id";
        //                    var LST_Product = db.Products.SqlQuery(sql_product).ToList();
        //                    if (LST_Product.Count > 0)
        //                    {
        //                        foreach (var v in LST_Product)
        //                        {
        //                            _ExcelHeadRow.Add(v.Name + " ( " + v.Code + " ) ");
        //                        }
        //                        _ExcelHeadRow.Add("D.Thu Sản phẩm");
        //                    }
        //                    _ExcelHeadRow.Add("Tổng D.Thu");
        //                    _ExcelHeadRow.Add("Stylist");
        //                    _ExcelHeadRow.Add("Skinner");
        //                    _ExcelHeadRow.Add("NV bán mỹ phẩm");

        //                    if (LST.Count > 0)
        //                    {
        //                        var ListService_Quantity = new List<int>();
        //                        var ListProduct_Quantity = new List<int>();
        //                        var ServiceListThis = new List<ProductBasic>();
        //                        var ProductListThis = new List<ProductBasic>();
        //                        var Service_TotalMoney = 0;
        //                        var Product_TotalMoney = 0;
        //                        var TotalMoney = 0;

        //                        foreach (var v in LST)
        //                        {
        //                            Service_TotalMoney = 0;
        //                            Product_TotalMoney = 0;
        //                            TotalMoney = 0;
        //                            BillRow = new List<string>();

        //                            BillRow.Add(Convert.ToDateTime(v.CreatedDate).ToString("dd/MM/yyyy"));
        //                            BillRow.Add(v.TimeCompleteBill_Int.ToString());
        //                            BillRow.Add(v.CustomerName);
        //                            BillRow.Add(v.CustomerCode);
        //                            BillRow.Add(v.CustomerPhone);
        //                            BillRow.Add(v.CustomerReturn.ToString());
        //                            BillRow.Add(v.Mark.ToString());

        //                            if (v.ServiceIds != null)
        //                            {
        //                                ServiceListThis = serializer.Deserialize<List<ProductBasic>>(v.ServiceIds);
        //                            }

        //                            if (v.ProductIds != null)
        //                            {
        //                                ProductListThis = serializer.Deserialize<List<ProductBasic>>(v.ProductIds);
        //                            }

        //                            if (LST_Service.Count > 0)
        //                            {
        //                                foreach (var v2 in LST_Service)
        //                                {
        //                                    if (ServiceListThis != null)
        //                                    {
        //                                        var ExistId = ServiceListThis.Find(x => x.Id == v2.Id);
        //                                        if (ExistId.Code != null)
        //                                        {
        //                                            Service_TotalMoney += (ExistId.Quantity * ExistId.Price * (100 - ExistId.VoucherPercent) / 100);
        //                                            BillRow.Add(ExistId.Quantity.ToString());
        //                                        }
        //                                        else
        //                                        {
        //                                            BillRow.Add("0");
        //                                        }
        //                                    }
        //                                    else
        //                                    {
        //                                        BillRow.Add("0");
        //                                    }
        //                                }
        //                            }

        //                            BillRow.Add(Service_TotalMoney.ToString());

        //                            if (LST_Product.Count > 0)
        //                            {
        //                                foreach (var v2 in LST_Product)
        //                                {
        //                                    if (ProductListThis != null)
        //                                    {
        //                                        var ExistId = ProductListThis.Find(x => x.Id == v2.Id);
        //                                        if (ExistId.Code != null)
        //                                        {
        //                                            Product_TotalMoney += ExistId.Promotion != 0 ? (ExistId.Quantity * ExistId.Price - ExistId.Promotion) : (ExistId.Quantity * ExistId.Price * (100 - ExistId.VoucherPercent) / 100);
        //                                            BillRow.Add(ExistId.Quantity.ToString());
        //                                        }
        //                                        else
        //                                        {
        //                                            BillRow.Add("0");
        //                                        }
        //                                    }
        //                                    else
        //                                    {
        //                                        BillRow.Add("0");
        //                                    }
        //                                }
        //                            }

        //                            TotalMoney = Service_TotalMoney + Product_TotalMoney;

        //                            BillRow.Add(Product_TotalMoney.ToString());
        //                            BillRow.Add(TotalMoney.ToString());
        //                            BillRow.Add(v.HairdresserName);
        //                            BillRow.Add(v.HairMassageName);
        //                            BillRow.Add(v.CosmeticName);
        //                            BillRowLST.Add(BillRow);
        //                        }
        //                    }

        //                    // export
        //                    var ExcelStorePath = Server.MapPath("~") + "Public/Excel/Hoa.Don/";
        //                    var FileName = "Hoa_Don_" + string.Format("{0:dd_MM_yyyy_hh_mm_ss}", DateTime.Now) + ".xlsx";
        //                    var UrlFileName = "http://" + Request.Url.Host + "/Public/Excel/Hoa.Don/" + FileName;

        //                    ExportXcel(_ExcelHeadRow, BillRowLST, ExcelStorePath + FileName);
        //                    Bind_Paging();
        //                    ScriptManager.RegisterStartupScript(BtnFakeExcel, this.GetType(), "Download Excel", "CallBackExportExcel('" + UrlFileName + "');UP_FormatPrice('.be-report-price'); SubTableFixHeight();SetActiveTableListing();", true);  
        //                }              
        //            }            
        //        }

        protected void Exc_ImportExcel()
        {
            var _ExcelHeadRow = new List<string>();
            _ExcelHeadRow.Add("STT");
            _ExcelHeadRow.Add("Field1");
            _ExcelHeadRow.Add("Field2");
            _ExcelHeadRow.Add("Field3");

            var LST = new List<Tuple<int, string, string, string>>();

            for (var i = 0; i < 5; i++)
            {
                var v = Tuple.Create(i, "str1-" + i, "str2-" + i, "str3-" + i);
                LST.Add(v);
            }

            // import
            var Path = Server.MapPath("~") + "Public/Excel/Hoa.Don/" + "Hoa_Don_28_07_2015_06_28_13.xlsx";
            Response.Write(Path);
            ImportXcel(Path);
        }

        private static void ExportXcel(List<string> _ExcelHeadRow, List<List<string>> List, string Path)
        {
            try
            {
                CreateExcelFile.ExcelHeadRow = _ExcelHeadRow;
                CreateExcelFile.CreateExcel(List, Path);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi. Không tạo được file excel.\r\nException: " + ex.Message);
            }
        }

        private void ImportXcel(string Path)
        {
            var tblDom = "<table><tbody>";
            foreach (var worksheet in Workbook.Worksheets(@Path))
                foreach (var row in worksheet.Rows)
                {
                    tblDom += "<tr>";
                    foreach (var cell in row.Cells)
                    {
                        if (cell != null)
                        {
                            tblDom += "<td>" + cell.Text + "</td>";
                        }
                    }
                    tblDom += "<tr>";
                }
            // if (cell != null) // Do something with the cells
            tblDom += "</tbody></table>";
            Response.Write(tblDom);
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        public string GenTime(int minutes)
        {
            string time = "";
            int hour = minutes / 60;
            int minute = minutes % 60;
            if (hour > 0)
            {
                time += hour + "h";
                time += minute + "p";
            }
            else
            {
                if (minute > 0)
                {
                    time += minute + " p";
                }
                else
                {
                    time += "-";
                }
            }
            return time;
        }

        

        public void Repeater_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
              || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (Perm_Edit == true)
                    ((Panel)(e.Item.FindControl("EAedit"))).Visible = true;
                if (Perm_Delete == true)
                    ((Panel)(e.Item.FindControl("EAdelete"))).Visible = true;
            }
        }
    }

    public class cls_quanlity
    {
        public int staffId { get; set; }
        public string staffName { get; set; }
        public int normalBills { get; set; }
        public int specialBills { get; set; }
        public double totalPoint { get; set; }
    }

    public class cls_billservice : BillService
    {
        public string staffName { get; set; }
        public int ratingPoint { get; set; }
    }

    public class cls_staffPoint
    {
        public int staffId { get; set; }
        public string staffName { get; set; }
        public int bill_special { get; set; }
        public int bill_normal { get; set; }
        public double point { get; set; }
        public int Mark { get; set; }
    }

    public class cls_staffRatingReport
    {
        public int staffId { get; set; }
        public string staffName { get; set; }
        public cls_ratingLevel normalBill { get; set; }
        public cls_ratingLevel specialBill { get; set; }
        public double point { get; set; }
    }

    public class cls_ratingLevel
    {
        /// <summary>
        /// Rất hài lòng
        /// </summary>
        public int great { get; set; }
        /// <summary>
        /// Hài lòng
        /// </summary>
        public int good { get; set; }
        /// <summary>
        /// Không hài lòng
        /// </summary>
        public int bad { get; set; }
        /// <summary>
        /// Không đánh giá
        /// </summary>
        public int noRating { get; set; }
    }
}

//==================
// Store
//==================
/*
private void Bind_Data_v2()
{
    using (var db = new Solution_30shineEntities())
    {
        string chemistryIds = "14,16,17,24";
        var sql = @"declare @coefficient float;
                            declare @coefficient_spc float;
                            declare @timeFrom nvarchar(50);
                            declare @timeTo nvarchar(50);

                            set @coefficient = 1;
                            set @coefficient_spc = 1.2;
                            set @timeFrom = '" + String.Format("{0:yyyy/MM/dd}", timeFrom) + @"';
                            set @timeTo = '" + String.Format("{0:yyyy/MM/dd}", timeTo) + @"';
                            
                            select f.*, g.Fullname as staffName
                            from
                            (
                                select 
	                                e.Staff_Hairdresser_Id as staffId, 
	                                Coalesce(SUM(e.IsSpecial), 0) as bill_special, 
	                                Coalesce(SUM(e.IsNormal), 0) as bill_normal,
	                                Coalesce(SUM(e.point), 0) as point
                                from
                                (
	                                select c.*,
			                                (Case
				                                when (c.IsCusFamiliar = 1 or c.ServiceId is not null ) then c.ConventionPoint * @coefficient_spc
				                                when ( c.IsCusFamiliar = 0 and c.ServiceId is null ) then c.ConventionPoint * @coefficient
			                                end) as point,
			                                (Case
				                                when (c.IsCusFamiliar = 1 or c.ServiceId is not null ) then 1
				                                when ( c.IsCusFamiliar = 0 and c.ServiceId is null ) then 0
			                                end) as IsSpecial,
			                                (Case
				                                when (c.IsCusFamiliar = 1 or c.ServiceId is not null ) then 0
				                                when ( c.IsCusFamiliar = 0 and c.ServiceId is null ) then 1
			                                end) as IsNormal
	                                from
	                                (
		                                select 
			                                --a.*, b.ConventionPoint
			                                a.Id, a.Staff_Hairdresser_Id, a.Mark, a.IsCusFamiliar, b.ConventionPoint,
			                                d.ServiceId,
			                                ROW_NUMBER() OVER(PARTITION BY a.Id ORDER BY a.Staff_Hairdresser_Id) AS Row
		                                from BillService as a
		                                left join Rating_ConfigPoint as b
		                                on a.Mark = b.RealPoint and b.Hint = 1
		                                left join FlowService as d
		                                on a.Id = d.BillId and d.ServiceId in (" + chemistryIds + @")
		                                where a.IsDelete != 1 and a.Pending != 1
		                                and a.ServiceIds != '' and a.ServiceIds is not null
		                                and a.CreatedDate between @timeFrom and @timeTo
		                                and a.Staff_Hairdresser_Id > 0
	                                ) as c
	                                where c.Row = 1
                                )as e
                                group by e.Staff_Hairdresser_Id
                            ) as f
                            inner join Staff as g
                            on f.staffId = g.Id";
        var lst = db.Database.SqlQuery<cls_staffPoint>(sql).OrderByDescending(o => o.point).ToList();
        // Bind data
        Rpt_Servey.DataSource = lst;
        Rpt_Servey.DataBind();
    }
}

private void Bind_Data_01()
    {
        using (var db = new Solution_30shineEntities())
        {
            var lstStaff = new List<cls_quanlity>();
            var staffQ = new cls_quanlity();
            double point;
            int index = -1;
            int integer;
            int chemistry;
            var familiar = new List<BillService>();
            var billCus = new List<BillService>();
            bool isSpecial = false;
                
            sql = @"select a.*, b.Fullname as staffName, Coalesce(c.ConventionPoint, 0) as ratingPoint
                    from BillService as a
                    inner join Staff as b
                    on a.Staff_Hairdresser_Id = b.Id
                    left join Rating_ConfigPoint as c
                    on a.Mark = c.RealPoint and c.Hint = 1
                    where a.IsDelete != 1 and a.Pending != 1
                    and (a.CreatedDate between '" + String.Format("{0:yyyy/MM/dd}", timeFrom) + "' and '" + String.Format("{0:yyyy/MM/dd}", timeTo) + "' )" +
                    @"and a.ServiceIds != '' and a.ServiceIds is not null
                    and a.Staff_Hairdresser_Id > 0";
            if (salonId > 0)
            {
                sql += " and a.SalonId = " + salonId;
            }

            var bills = db.Database.SqlQuery<cls_billservice>(sql).ToList();
            if (bills.Count > 0)
            {
                foreach (var v in bills)
                {
                    isSpecial = false;
                    staffQ = new cls_quanlity();
                    index = lstStaff.FindIndex(fi => fi.staffId == v.Staff_Hairdresser_Id);
                    /// Thêm stylist vào danh sách
                    if (index == -1)
                    {
                        staffQ.staffId = int.TryParse(v.Staff_Hairdresser_Id.ToString(), out integer) ? integer : 0;
                        staffQ.staffName = v.staffName;
                    }
                    else
                    {
                        staffQ = lstStaff[index];
                    }

                    /// Cập nhật giá trị cho stylist
                    /// 1. Kiểm tra có phải bill hóa chất
                    /// 2. Kiểm tra có phải khách quen (là khách đã dùng dịch vụ bởi stylist này ít nhất 2 lần, hoặc 1 lần nhưng là lần gần đây nhất)

                    // 1. Kiểm tra có phải bill hóa chất
                    sql = @"select 
                            --*
                            COUNT(*) as times
                            from FlowService
                            where IsDelete != 1
                            and ServiceId in (14,16,17,24)
                            and BillId = " + v.Id;
                    chemistry = db.Database.SqlQuery<int>(sql).FirstOrDefault();
                    if (chemistry > 0)
                    {
                        isSpecial = true;
                    }
                    // 2. Kiểm tra có phải khách quen 
                    sql = @"select *
                            from BillService
                            where IsDelete != 1 and Pending != 1
                            and CustomerCode = '" + v.CustomerCode + @"' 
                            and CustomerCode != '' and CustomerCode is not null
                            and CreatedDate < '" + String.Format("{0:yyyy/MM/dd}", timeFrom) + "'";
                    billCus = db.BillServices.SqlQuery(sql).ToList();
                    familiar = billCus.Where(w=>w.Staff_Hairdresser_Id == v.Staff_Hairdresser_Id).ToList();
                    // Nếu khách đã sử dụng dịch vụ bởi stylist này ít nhất 2 lần
                    if (familiar.Count > 1)
                    {
                        isSpecial = true;
                    }
                    // hoặc dùng 1 lần nhưng là lần gần đây nhất
                    else if (familiar.Count == 1)
                    {
                        if (billCus[billCus.Count - 1].Staff_Hairdresser_Id == v.Staff_Hairdresser_Id)
                        {
                            isSpecial = true;
                        }
                    }

                    /// Tính điểm
                    if (isSpecial)
                    {
                        point = (double)v.ratingPoint * 1.2;
                        staffQ.specialBills++;
                    }
                    else
                    {
                        point = (double)v.ratingPoint;
                        staffQ.normalBills++;
                    }

                    staffQ.totalPoint += point;

                    // Return staffQ value to lstStaff
                    index = lstStaff.FindIndex(fi => fi.staffId == v.Staff_Hairdresser_Id);
                    if (index != -1)
                    {
                        lstStaff[index] = staffQ;
                    }
                    else
                    {
                        lstStaff.Add(staffQ);
                    }
                }
            }

            // Bind data
            Rpt_Servey.DataSource = lstStaff.OrderByDescending(o=>o.totalPoint).ToList();
            Rpt_Servey.DataBind();
        }
    }
*/
