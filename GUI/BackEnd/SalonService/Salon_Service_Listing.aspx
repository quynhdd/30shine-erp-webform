﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Salon_Service_Listing.aspx.cs" Inherits="_30shine.GUI.BackEnd.SalonService.Salon_Service_Listing" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="ServiceListing" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Cấu hình dịch vụ và salon &nbsp;&#187; </li>
                        <li class="li-listing"><a href="/admin/salon-service/salon-dich-vu.html">Danh sách</a></li>
                        <li class="li-add"><a href="/admin/salon-service/them-moi.html">Thêm mới</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing be-report">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <!-- Filter -->
                <div class="row row-filter">
                    <strong class="st-head"><i class="fa fa-filter"></i>Lọc kết quả</strong>
                    <div style="padding-top: 10px;">
                        <label class="radio-inline" style="padding-left:40px">
                            <input type="radio" name="optradio" checked="checked" onchange="OnchangeSalon()" />Salon
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="optradio" onchange="OnchangeService()" />Dịch vụ
                        </label>
                    </div>
                </div>
                <!-- End Filter -->

                <!-- Row Table Filter -->
                <div class="table-func-panel">
                    <div class="table-func-elm">
                        <span>Số hàng / Page : </span>
                        <div class="table-func-input-wp">
                            <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                            <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                            <ul class="ul-opt-segment">
                                <li data-value="10">10</li>
                                <li data-value="20">20</li>
                                <li data-value="30">30</li>
                                <li data-value="40">40</li>
                                <li data-value="50">50</li>
                                <li data-value="1000000">Tất cả</li>
                            </ul>
                            <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                        </div>
                    </div>
                </div>
                <!-- End Row Table Filter -->
                <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
                <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                    <ContentTemplate>
                        <div class="table-wp">
                            <table class="table-add table-listing">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th class="title-rename">Salon</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="RptService" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# Container.ItemIndex + 1 + PAGING._Offset %></td>
                                                <td><a href="/admin/salon-service/<%# Eval("Id") %>.html"><%# Eval("Name") %></a></td>
                                                <td class="map-edit">
                                                    <div class="edit-wp">
                                                        <a class="elm edit-btn" href="/admin/salon-service/<%# Eval("Id") %>.html" title="Sửa"></a>
                                                        <%--<a class="elm del-btn" onclick="del(this.parentNode.parentNode.parentNode,'<%# Eval("Code") %>', '<%# Eval("Name") %>')" href="javascript://" title="Xóa"></a>--%>
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>

                        <!-- Paging -->
                        <div class="site-paging-wp">
                            <% if (PAGING.TotalPage > 1)
                                { %>
                            <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                                <% if (PAGING._Paging.Prev != 0)
                                    { %>
                                <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                                <% } %>
                                <asp:Repeater ID="RptPaging" runat="server">
                                    <ItemTemplate>
                                        <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                            <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>>
                                            <%# Eval("PageNum") %>
                                        </a>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                                    { %>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                                <% } %>
                            </asp:Panel>
                            <% } %>
                        </div>
                        <!-- End Paging -->
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
            </div>
            <%-- END Listing --%>
        </div>
        <style>
            #glbAdminConfig.active a { border-bottom: 1px solid white; }
        </style>
        <script>
            jQuery(document).ready(function () {
                $("#glbAdminContent").addClass("active");
                $("#glbAdminService").addClass("active");
                $("#glbAdminConfig").addClass("active");
            });

            //============================
            // Event delete
            //============================
            function del(This, code, name) {
                var code = code || null,
                    name = name || null,
                    Row = This;
                if (!code) return false;

                // show EBPopup
                $(".confirm-yn").openEBPopup();
                $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn xóa [ " + name + " ] ?");

                $("#EBPopup .yn-yes").bind("click", function () {
                    $.ajax({
                        type: "POST",
                        url: "/GUI/SystemService/Ajax/Del.aspx/Delele_Service",
                        data: '{Code : "' + code + '"}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json", success: function (response) {
                            var mission = JSON.parse(response.d);
                            if (mission.success) {
                                delSuccess();
                                Row.remove();
                            } else {
                                delFailed();
                            }
                        },
                        failure: function (response) { alert(response.d); }
                    });
                });
                $("#EBPopup .yn-no").bind("click", function () {
                    autoCloseEBPopup(0);
                });
            }

            //============================
            // Event publish, featured, is free
            //============================
            function checkFeatured(This, table) {
                var Id = This.attr("data-id");
                var isChecked = This.is(":checked");
                $.ajax({
                    type: "POST",
                    url: "/GUI/SystemService/Ajax/Publish_Featured.aspx/Featured",
                    data: '{Id : "' + Id + '", Table : "' + table + '", isChecked : "' + isChecked + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        var mission = JSON.parse(response.d);
                        if (mission.success) {
                            alert("Chuyển trạng thái thành công.");
                        } else {
                        }
                    },
                    failure: function (response) { alert(response.d); }
                });
            }

            function checkPublish(This, table) {
                var Id = This.attr("data-id");
                var isChecked = This.is(":checked");
                $.ajax({
                    type: "POST",
                    url: "/GUI/SystemService/Ajax/Publish_Featured.aspx/Publish",
                    data: '{Id : "' + Id + '", Table : "' + table + '", isChecked : "' + isChecked + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        var mission = JSON.parse(response.d);
                        if (mission.success) {
                            alert("Chuyển trạng thái thành công.");
                        } else {
                        }
                    },
                    failure: function (response) { alert(response.d); }
                });
            }

            function checkIsFree(This, table) {
                var Id = This.attr("data-id");
                var isChecked = This.is(":checked");
                $.ajax({
                    type: "POST",
                    url: "/GUI/SystemService/Ajax/Publish_Featured.aspx/IsFree",
                    data: '{Id : "' + Id + '", Table : "' + table + '", isChecked : "' + isChecked + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        var mission = JSON.parse(response.d);
                        if (mission.success) {
                            alert("Chuyển trạng thái thành công.");
                        } else {
                        }
                    },
                    failure: function (response) { alert(response.d); }
                });
            }

            function OnchangeSalon() {
                $(".title-rename").text("Salon");
                $.ajax({
                    type: "post",
                    url: "/GUI/BackEnd/SalonService/Salon_Service_Add.aspx/OnchangeService",
                    data: '{ ServiceId: "' + 2 + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        console.log(response.d);
                        if (response.d != null) {
                            var jsonObj = $.parseJSON(response.d);
                            var html = "";
                            if (jsonObj != null) {
                                $(".table-add tbody tr").remove();
                                var linkrew = "salon-service";
                                for (var i = 0; i < jsonObj.length; i++) {
                                    var STT = i + 1;
                                    $(".table-add").append($(appendTable(STT, jsonObj[i].Id, jsonObj[i].Name, linkrew)));
                                }
                            }
                        }
                    },
                    failure: function (response) { alert(response.d); }
                });
            }
           //
            function OnchangeService() {
                $(".title-rename").text("Dịch vụ");
                $.ajax({
                    type: "post",
                    url: "/GUI/BackEnd/SalonService/Salon_Service_Add.aspx/OnchangeSalon",
                    data: '{ SalonId: "' + 2 + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        console.log(response.d);
                        if (response.d != null) {
                            var jsonObj = $.parseJSON(response.d);
                            var html = "";
                            if (jsonObj != null) {
                                $(".table-add tbody tr").remove();
                                var linkrew = "salon-serviceId";
                                for (var i = 0; i < jsonObj.length; i++) {
                                    var stt = i + 1;
                                    $(".table-add").append($(appendTable(stt, jsonObj[i].Id, jsonObj[i].Name, linkrew)));
                                }
                            }
                        }
                    },
                    failure: function (response) { alert(response.d); }
                });
            }

            function appendTable(STT, Id, Name, linkrew) {
                return '<tr>' +
                            '<td>' + STT + '</td>' +
                             '<td><a href="/admin/' + linkrew + '/' + Id + '.html">' + Name + '</a></td>' +
                              '<td class="map-edit">' +
                                '<div class="edit-wp">' +
                                   '<a class="elm edit-btn" href="/admin/' + linkrew + '/' + Id + '.html" title="Sửa"></a>' +
                                  '</div>' +
                                '</td>' +
                        '</tr>';
            }


        </script>

    </asp:Panel>
</asp:Content>
