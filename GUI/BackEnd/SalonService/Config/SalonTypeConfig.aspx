﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMasterVersion1.Master" AutoEventWireup="true" CodeBehind="SalonTypeConfig.aspx.cs" Inherits="_30shine.GUI.BackEnd.SalonService.Config.SalonTypeConfig" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false">
        <i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.
    </asp:Panel>
    <title>Noti-tool</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="Panel1" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <style>
            #wrapper {
                font-family: 'Times New Roman';
                width: 100% !important;
                height: 750px;
            }

            td, th {
                border: 1px solid black;
                border-collapse: collapse;
                padding: 5px;
                min-width: 100px;
            }

            .h2-cls-excel {
                width: 100%;
                float: left;
                text-align: center;
                font-size: 20px;
                padding: 10px;
                text-transform: uppercase;
                margin-top: 10px;
            }

            .h1-title-timport-file-excel {
                width: 100%;
                float: left;
                font-size: 20px;
                text-transform: uppercase;
                margin-bottom: 10px;
            }

            #my_file_input::-webkit-file-upload-button {
                color: red;
            }

            #my_file_input::before {
                color: red;
                background: none;
                border: none;
            }

            .wp-submit {
                width: 100%;
                float: left;
                margin: 15px 0px 20px;
                text-align: right;
            }

            #my_file_input {
                float: left;
                margin-bottom: 6px;
            }

            .cls_tempfile {
                text-decoration: underline !important;
                color: #0277bd;
            }

            th, td {
                text-align: center;
                font-size: 16px;
            }

            .wp_import-file-excel {
                float: right;
                padding-top: 20px;
            }

            .btn-complete {
                background-color: black;
                color: white;
            }

                .btn-complete:hover {
                    background-color: black;
                    color: red;
                }

            .container {
                margin-top: 30px;
            }

            .card {
                width: 100%;
            }

            .table {
                margin-top: 15px;
            }

            .btn-success {
                margin-right: 5px;
            }

            /*#div-wrapper {
                max-height: 540px;
                overflow-y: scroll;
            }*/

            table {
                display: flex;
                flex-flow: column;
                height: 100%;
                width: 100%;
            }

                table thead {
                    flex: 0 0 auto;
                    width: calc(100% - 1.2em);
                }

                table tbody {
                    flex: 1 1 auto;
                    display: block;
                    overflow-y: scroll;
                    max-height: 440px;
                }

                    table tbody tr {
                        width: 100%;
                    }

                    table thead,
                    table tbody tr {
                        display: table;
                        table-layout: fixed;
                    }
            /* decorations */
            .table-container {
                border: 1px solid black;
                padding: 0.3em;
            }

            table {
                border: 1px solid lightgrey;
            }

                table td, table th {
                    padding: 0.3em;
                    border: 1px solid lightgrey;
                }

                table th {
                    border: 1px solid grey;
                }
        </style>
        <div id="wrapper">
            <div class="container">
                <div class="row">
                    <div class="card mb-6 shadow-sm">
                        <div class="card-body">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="col-lg-12">
                                    <div class="col-lg-6 float-left">
                                        <h2 class="h2-cls-excel">CẤU HÌNH XẾP HẠNG SALON</h2>
                                    </div>
                                    <div class="col-lg-6 float-left">
                                        <div class="wp_import-file-excel">
                                            <input type="file" id="my_file_input" />
                                            <div id='my_file_output'></div>
                                            <a href="/TemplateFile/ImportExcelConfigSalon/file_example_import_data_config_salon.xlsx" class="cls_tempfile">Tải File Excel Import MẪu</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="div-wrapper">
                                <table class="table table-striped table-bordered">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th scope="col">STT</th>
                                            <th scope="col">SALON - ID</th>
                                            <th scope="col">SALON - TYPE</th>
                                        </tr>
                                    </thead>
                                    <tbody id="body-content">
                                    </tbody>
                                </table>
                            </div>
                            <div class="wp-submit">
                                <a href="javascript://" onclick="UploadData();" class="btn btn-success">Hoàn tất</a>
                                <a href="javascript://" onclick="ClearData();" class="btn btn-danger">Huỷ bỏ</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            var UserId = '<%= HttpContext.Current.Session["User_Id"] %>';
            var URL_API_REPORTS = '<%= Libraries.AppConstants.URL_API_REPORTS %>';
            var objList = {};
            var oFileIn;
            $(function () {
                oFileIn = document.getElementById('my_file_input');
                if (oFileIn.addEventListener) {
                    oFileIn.addEventListener('change', filePicked, false);
                }
            });

            // func get data
            function filePicked(oEvent) {
                // Get The File From The Input
                var oFile = oEvent.target.files[0];
                var sFilename = oFile.name;
                // Create A File Reader HTML5
                var reader = new FileReader();
                // Ready The Event For When A File Gets Selected
                reader.onload = function (e) {
                    var data = e.target.result;
                    var cfb = XLSX.read(data, { type: 'binary' });
                    // Here is your object
                    var XL_row_object = XLSX.utils.sheet_to_row_object_array(cfb.Sheets["Sheet1"]);
                    objList = XL_row_object;
                    // append html 
                    if (objList.length > 0) {
                        appendData(objList);
                    }
                    else {
                        $("#body-content").html('');
                        objList = {};
                        ShowMessage('', 'Kiểm tra lại file excel!', 4, 5000);
                    }
                };
                // Tell JS To Start Reading The File.. You could delay this if desired
                reader.readAsBinaryString(oFile);
            }

            // func apeend html
            function appendData(objData) {
                var str = "";
                $("#body-content").html('');
                const Count = objData.length;
                var stt = 0;
                if (Count > 0) {
                    for (var i = 0; i < Count; i++) {
                        stt++
                        str += `<tr>
                                    <th scope='col'>${stt}</th>
                                    <td>${objData[i].SalonId}</td>
                                    <td>${objData[i].SalonType}</td>
                                </tr>`;
                    }
                    if (str != '') {
                        $("#body-content").append(str);
                    }
                }
            }

            // func click luu data
            function UploadData() {
                if (objList.length > 0) {
                    startLoading();
                    var dataJson = {
                        UserId: parseInt(UserId),
                        Data: objList
                    };
                    $.ajax({
                        type: "POST",
                        url: URL_API_REPORTS + '/api/salon-type-config',
                        data: JSON.stringify(dataJson),
                        contentType: "application/json; charset=utf-8",
                        dataType: "JSON",
                        success: function (response) {
                            finishLoading();
                            if (response.message === 'Success!') {
                                $("#body-content").html('');
                                objListNoti = {};
                                $("#my_file_input").val('');
                                ShowMessage('', 'Hoàn tất thành công!', 2, 5000);
                            }
                            else {
                                ShowMessage('', 'Hoàn tất thất bại', 4, 5000);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            var responseText = jqXHR.responseJSON.message;
                            ShowMessage('', `${responseText}`, 3, 5000);
                            finishLoading();
                        }
                    });
                }
                else {
                    ShowMessage('', 'Vui lòng chọn file excel', 4, 5000);
                }
            };

            //onchange noti
            function ClearData() {
                $("#body-content").html('');
                objListNoti = {};
                $("#my_file_input").val('');
            }
        </script>
        <script src="/Assets/js/libs.xlsx/libs-xlsx-0.8.0-jszip.js"></script>
        <script src="/Assets/js/libs.xlsx/libs-xlsx-0.8.0-xlsx.js"></script>
        <!-- Page loading -->
        <div class="page-loading">
            <p>Vui lòng đợi trong giây lát...</p>
        </div>
    </asp:Panel>
</asp:Content>
