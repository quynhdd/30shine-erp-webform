﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Web.Services;
using System.Data.Entity.Migrations;
using System.Web.Script.Serialization;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.SalonService
{
    public partial class Salon_Service_Add : System.Web.UI.Page
    {
        private string PageID = "";
        private bool Perm_Access = false;
        protected bool Perm_ViewAllData = false;
        public int Id;
        public List<Service> listSer;
        public List<Tbl_Salon> listSalon;
        public List<Store_SalonService_GetList_Result> list;
        public string ServiceId;
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //if (Request.QueryString["Code"] != null)
                //{
                //    PageID = "SLSV_EDIT";
                //}
                //else
                //{
                //    PageID = "SLSV_ADD";
                //}
                //if (Request.QueryString["ServiceId"] != null)
                //{
                //    PageID = "SLSV_ID_EDIT";
                //}
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                ////Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                ////Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        private void ExecuteByPermission()
        {

            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                Library.Function.bindSalon(new List<DropDownList> { ddlSalon }, Perm_ViewAllData);
                Bind_Service();

                using (var db = new Solution_30shineEntities())
                {
                    listSer = new List<Service>();
                    listSer = db.Services.Where(p => p.IsDelete != 1 && p.Publish == 1).ToList();

                    listSalon = new List<Tbl_Salon>();
                    listSalon = db.Tbl_Salon.Where(p => p.IsDelete != 1 && p.Publish == true).ToList();
                    IsUpdate();
                    Bindata();
                }
            }
        }

    

        private void Bind_Service()
        {
            using (var db = new Solution_30shineEntities())
            {
                var _Service = db.Services.Where(w => w.IsDelete != 1 && w.Publish == 1).OrderBy(o => o.Id).ToList();
                var Key = 0;

                ddlService.DataTextField = "Name";
                ddlService.DataValueField = "Id";
                ListItem item = new ListItem("Chọn dịch vụ", "0");
                ddlService.Items.Insert(0, item);

                foreach (var v in _Service)
                {
                    Key++;
                    item = new ListItem(v.Name, v.Id.ToString());
                    ddlService.Items.Insert(Key, item);
                }
                ddlService.SelectedIndex = 0;

            }
        }

        public void Bindata()
        {
            using (var db = new Solution_30shineEntities())
            {
                list = new List<Store_SalonService_GetList_Result>();
                ServiceId = Request.QueryString["ServiceId"];
                if (ServiceId != null)
                {
                    ddlService.SelectedValue = ServiceId;
                    list = db.Store_SalonService_GetList(0, Convert.ToInt32(ServiceId)).ToList();
                }
                else if (Id != 0)
                {
                    list = db.Store_SalonService_GetList(Id, 0).ToList();
                    ddlSalon.SelectedValue = Id.ToString();
                }
                else
                {
                    list = db.Store_SalonService_GetList(3, 0).ToList();
                    ddlSalon.SelectedValue = 3.ToString();
                }
            }
        }

        /// <summary>
        /// Lưu bản ghi quan hệ Salon-Service
        /// </summary>
        /// <param name="SalonId"></param>
        /// <param name="ServiceId"></param>
        /// <param name="ListItem"></param>
        /// <returns></returns>
        [WebMethod]
        public static string AddListItem(int SalonId, int ServiceId, string ListItem)
        {
            var msg = new Message();
            using (var db = new Solution_30shineEntities())
            {
                var serializer = new JavaScriptSerializer();
                var index = -1;
                var item = new Salon_Service();
                var itemData = new ItemData();
                if (ListItem != "[]")
                {
                    try
                    {

                        var listItem = serializer.Deserialize<List<ItemData>>(ListItem).ToList();
                        if (SalonId > 0)
                        {
                            //I. Lưu dịch vụ theo salon
                            //I.1 Kiểm tra các item salon-service đã được ghi chưa
                            //  Nếu chưa ghi, ghi mới
                            //  Nếu ghi rồi, update
                            var listBySalon = db.Salon_Service.Where(w => w.SalonId == SalonId).ToList();
                            // Kiểm tra thêm mới bản ghi
                            if (listItem.Count > 0)
                            {
                                foreach (var v in listItem)
                                {
                                    index = listBySalon.FindIndex(w => w.ServiceId == v.Id);
                                    if (index == -1)
                                    {
                                        item = new Salon_Service();
                                        item.SalonId = SalonId;
                                        item.ServiceId = v.Id;
                                        item.HeSoHL = v.HeSo;
                                        item.Price = v.Price;
                                        item.IsCheck = v.Checked;
                                        item.IsDelete = false;
                                    }
                                    else
                                    {
                                        item = listBySalon[index];
                                        item.HeSoHL = v.HeSo;
                                        item.Price = v.Price;
                                        item.IsCheck = v.Checked;
                                        item.IsDelete = false;
                                    }
                                    db.Salon_Service.AddOrUpdate(item);
                                    db.SaveChanges();
                                }

                                if (listBySalon.Count > 0)
                                {
                                    foreach (var v in listBySalon)
                                    {
                                        index = listItem.FindIndex(w => w.Id == v.ServiceId);
                                        if (index == -1)
                                        {
                                            item = v;
                                            itemData = listItem[index];
                                            item.HeSoHL = itemData.HeSo;
                                            item.Price = itemData.Price;
                                            item.IsCheck = false;
                                            item.IsDelete = false;
                                            db.Salon_Service.AddOrUpdate(item);
                                            db.SaveChanges();
                                        }
                                    }
                                }


                            }
                        }
                        else if (ServiceId > 0)
                        {
                            //II. Lưu salon theo dịch vụ
                            //II. Lưu salon theo dịch vụ
                            //I.1 Kiểm tra các item salon-service đã được ghi chưa
                            //  Nếu chưa ghi, ghi mới
                            //  Nếu ghi rồi, update
                            var listByService = db.Salon_Service.Where(w => w.ServiceId == ServiceId).ToList();
                            // Kiểm tra thêm mới bản ghi
                            if (listItem.Count > 0)
                            {
                                foreach (var v in listItem)
                                {
                                    index = listByService.FindIndex(w => w.SalonId == v.Id);
                                    if (index == -1)
                                    {
                                        item = new Salon_Service();
                                        item.SalonId = v.Id;
                                        item.ServiceId = ServiceId;
                                        item.HeSoHL = v.HeSo;
                                        item.Price = v.Price;
                                        item.IsDelete = false;
                                        item.IsCheck = v.Checked;
                                    }
                                    else
                                    {
                                        item = listByService[index];
                                        item.HeSoHL = v.HeSo;
                                        item.Price = v.Price;
                                        item.IsDelete = false;
                                        item.IsCheck = v.Checked;
                                    }
                                    db.Salon_Service.AddOrUpdate(item);
                                    db.SaveChanges();
                                }
                            }

                            if (listByService.Count > 0)
                            {
                                foreach (var v in listByService)
                                {
                                    index = listItem.FindIndex(w => w.Id == v.SalonId);
                                    if (index == -1)
                                    {
                                        item = v;
                                        itemData = listItem[index];
                                        item.HeSoHL = itemData.HeSo;
                                        item.Price = itemData.Price;
                                        item.IsCheck = false;
                                        item.IsDelete = false;
                                        db.Salon_Service.AddOrUpdate(item);
                                        db.SaveChanges();
                                    }
                                }
                            }
                        }
                        else
                        {
                            msg.msg = "Giá trị không hợp lệ. Vui lòng kiểm tra lại!";
                        }

                        msg.msg = "Dữ liệu đã được lưu";
                    }
                    catch (Exception ex)
                    {
                        msg.msg = ex.Message;
                    }
                }

            }
            return msg.msg;
        }

        [WebMethod]
        public static string AddListItem1(int SalonId, string ListService, string ListServiceNot)
        {
            var msg = new Message();
            using (var db = new Solution_30shineEntities())
            {
                var serializer = new JavaScriptSerializer();

                if (ListService != "")
                {
                    try
                    {

                        var listItem = serializer.Deserialize<List<ItemData>>(ListService).ToList();
                        foreach (var item in listItem)
                        {
                            var CountTable = db.Salon_Service.Where(p => p.SalonId == SalonId && p.ServiceId == item.Id).Count();

                            var listNotCheck = db.Tbl_Salon.Where(p => p.IsDelete != 1 && p.Publish == true && p.Id != SalonId);
                            foreach (var salonnot in listNotCheck)
                            {
                                if (CountTable == 0)
                                {
                                    var objSL = new Salon_Service();
                                    objSL.SalonId = salonnot.Id;
                                    objSL.ServiceId = item.Id;
                                    objSL.Price = item.Price;
                                    objSL.HeSoHL = item.HeSo;
                                    db.Salon_Service.AddOrUpdate(objSL);
                                }
                            }
                            db.SaveChanges();


                            var obj = new Salon_Service();
                            if (CountTable > 0)
                            {
                                var objc = db.Salon_Service.Single(p => p.SalonId == SalonId && p.ServiceId == item.Id);
                                objc.SalonId = SalonId;
                                objc.ServiceId = item.Id;
                                objc.Price = item.Price;
                                objc.HeSoHL = item.HeSo;
                                if (objc.IsCheck == false || objc.IsCheck == null)
                                    objc.IsCheck = true;
                                db.Salon_Service.AddOrUpdate(objc);
                            }
                            else
                            {
                                obj.SalonId = SalonId;
                                obj.ServiceId = item.Id;
                                obj.Price = item.Price;
                                obj.HeSoHL = item.HeSo;
                                obj.IsCheck = true;
                                db.Salon_Service.AddOrUpdate(obj);
                            }
                            db.SaveChanges();
                        }

                        msg.msg = "Dữ liệu đã được lưu";
                    }
                    catch (Exception ex)
                    {
                        msg.msg = ex.Message;
                    }
                }
                if (ListServiceNot != "[]")
                {
                    try
                    {
                        var listItemNot = serializer.Deserialize<List<ItemData>>(ListServiceNot).ToList();
                        foreach (var item in listItemNot)
                        {
                            var CountTable = db.Salon_Service.Where(p => p.SalonId == SalonId && p.ServiceId == item.Id).Count();

                            var listSalon = db.Tbl_Salon.Where(p => p.IsDelete != 1 && p.Publish == true && p.Id != SalonId).ToList();
                            foreach (var salon in listSalon)
                            {
                                if (CountTable == 0)
                                {
                                    var objSL = new Salon_Service();
                                    objSL.SalonId = salon.Id;
                                    objSL.ServiceId = item.Id;
                                    objSL.Price = item.Price;
                                    objSL.HeSoHL = item.HeSo;
                                    db.Salon_Service.AddOrUpdate(objSL);
                                }
                            }
                            db.SaveChanges();

                            var objNot = new Salon_Service();
                            if (CountTable > 0)
                            {
                                var objn = db.Salon_Service.Single(p => p.SalonId == SalonId && p.ServiceId == item.Id);
                                objn.SalonId = SalonId;
                                objn.ServiceId = item.Id;
                                objn.Price = item.Price;
                                objn.HeSoHL = item.HeSo;
                                if (objn.IsCheck == true || objn.IsCheck == null)
                                    objn.IsCheck = false;
                                db.Salon_Service.AddOrUpdate(objn);
                            }
                            else
                            {
                                objNot.SalonId = SalonId;
                                objNot.ServiceId = item.Id;
                                objNot.Price = item.Price;
                                objNot.HeSoHL = item.HeSo;
                                objNot.IsCheck = false;
                                db.Salon_Service.AddOrUpdate(objNot);
                            }

                            db.SaveChanges();
                        }
                        msg.msg = "Dữ liệu đã được lưu";
                    }
                    catch (Exception ex)
                    {
                        msg.msg = ex.Message;
                    }
                }
            }
            return msg.msg;
        }

        [WebMethod]
        public static string AddListSalon(int ServiceId, string ListSalon, string ListSalonNot)
        {
            var msg = new Message();
            using (var db = new Solution_30shineEntities())
            {
                var serializer = new JavaScriptSerializer();

                if (ListSalon != "")
                {
                    try
                    {
                        var listItem = serializer.Deserialize<List<ItemData>>(ListSalon).ToList();
                        foreach (var item in listItem)
                        {
                            var CountTable = db.Salon_Service.Where(p => p.SalonId == item.Id && p.ServiceId == ServiceId).Count();
                            var listNotCheck = db.Services.Where(p => p.IsDelete != 1 && p.Publish == 1 && p.Id != ServiceId).ToList();
                            foreach (var serviceNot in listNotCheck)
                            {
                                if (CountTable == 0)
                                {
                                    var objSL = new Salon_Service();
                                    objSL.SalonId = item.Id;
                                    objSL.ServiceId = serviceNot.Id;
                                    objSL.Price = item.Price;
                                    objSL.HeSoHL = item.HeSo;
                                    objSL.IsCheck = false;
                                    db.Salon_Service.AddOrUpdate(objSL);
                                }
                            }
                            db.SaveChanges();


                            if (CountTable > 0)
                            {
                                var objc = db.Salon_Service.Single(p => p.SalonId == item.Id && p.ServiceId == ServiceId);
                                objc.SalonId = item.Id;
                                objc.ServiceId = ServiceId;
                                objc.Price = item.Price;
                                objc.HeSoHL = item.HeSo;
                                if (objc.IsCheck == false || objc.IsCheck == null)
                                    objc.IsCheck = true;
                                db.Salon_Service.AddOrUpdate(objc);
                            }
                            else
                            {
                                var obj = new Salon_Service();
                                obj.SalonId = item.Id;
                                obj.ServiceId = ServiceId;
                                obj.Price = item.Price;
                                obj.HeSoHL = item.HeSo;
                                obj.IsCheck = true;
                                db.Salon_Service.AddOrUpdate(obj);
                            }

                            db.SaveChanges();
                        }
                        msg.msg = "Dữ liệu đã được lưu";
                    }
                    catch (Exception ex)
                    {
                        msg.msg = ex.Message;
                    }
                }
                if (ListSalonNot != "[]")
                {
                    try
                    {
                        var listItemNot = serializer.Deserialize<List<ItemData>>(ListSalonNot).ToList();
                        foreach (var item in listItemNot)
                        {
                            var CountTable = db.Salon_Service.Where(p => p.SalonId == item.Id && p.ServiceId == ServiceId).Count();

                            var listService = db.Services.Where(p => p.IsDelete != 1 && p.Publish == 1 && p.Id != ServiceId).ToList();
                            foreach (var ser in listService)
                            {
                                if (CountTable == 0)
                                {
                                    var objSL = new Salon_Service();
                                    objSL.SalonId = item.Id;
                                    objSL.ServiceId = ser.Id;
                                    objSL.Price = item.Price;
                                    objSL.HeSoHL = item.HeSo;
                                    objSL.IsCheck = false;
                                    db.Salon_Service.AddOrUpdate(objSL);
                                }
                            }
                            db.SaveChanges();

                            var objNot = new Salon_Service();
                            if (CountTable > 0)
                            {
                                var objn = db.Salon_Service.Single(p => p.SalonId == item.Id && p.ServiceId == ServiceId);
                                objn.SalonId = item.Id;
                                objn.ServiceId = ServiceId;
                                objn.Price = item.Price;
                                objn.HeSoHL = item.HeSo;
                                if (objn.IsCheck == true || objn.IsCheck == null)
                                    objn.IsCheck = false;
                                db.Salon_Service.AddOrUpdate(objn);
                            }
                            else
                            {
                                objNot.SalonId = item.Id;
                                objNot.ServiceId = ServiceId;
                                objNot.Price = item.Price;
                                objNot.HeSoHL = item.HeSo;
                                objNot.IsCheck = false;
                                db.Salon_Service.AddOrUpdate(objNot);
                            }

                            db.SaveChanges();
                        }
                        msg.msg = "Dữ liệu đã được lưu";
                    }
                    catch (Exception ex)
                    {
                        msg.msg = ex.Message;
                    }
                }
            }
            return msg.msg;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="SalonId"></param>
        /// <returns></returns>
        [WebMethod]
        public static object OnchangeSalon(int SalonId)
        {
            using (var db = new Solution_30shineEntities())
            {
                JavaScriptSerializer serialize = new JavaScriptSerializer();
                try
                {
                    var list = db.Store_SalonService_GetList(SalonId, 0).ToList();
                    return serialize.Serialize(list);
                }
                catch (Exception ex)
                {
                    var error = ex.Message;
                    var Ltservice = db.Services
                        .Select(p => new { p.Id, p.Name, p.Price, p.CoefficientRating, p.IsDelete, p.Publish })
                        .OrderBy(p => p.Id).Where(p => p.IsDelete != 1 && p.Publish == 1).ToList();
                    return serialize.Serialize(Ltservice);
                }

                //var lst = db.Salon_Service_Select(SalonId, 0).OrderBy(p => p.SalonId).ToList();
                //if (lst.Count() > 0)
                //    return serialize.Serialize(lst);
                //else
                //{
                //    var Ltservice = db.Services
                //        .Select(p => new { ServiceId = p.Id, Service = p.Name, p.Price, HeSoHL = p.CoefficientRating, p.IsDelete, p.Publish })
                //        .OrderBy(p => p.ServiceId).Where(p => p.IsDelete != 1 && p.Publish == 1).ToList();
                //    return serialize.Serialize(Ltservice);
                //}
            }
        }

        [WebMethod]
        public static object OnchangeService(int ServiceId)
        {
            using (var db = new Solution_30shineEntities())
            {
                JavaScriptSerializer serialize = new JavaScriptSerializer();

                var list = db.Store_SalonService_GetList(0, ServiceId).ToList();
                return serialize.Serialize(list);

                //var lst = db.Salon_Service_Select(0, ServiceId).OrderBy(p => p.ServiceId).ToList();
                //if (lst.Count() > 0)
                //    return serialize.Serialize(lst);
                //else
                //{
                //    try
                //    {
                //        var objSer = db.Services.Single(p => p.Id == ServiceId);
                //        var Ltservice = db.Tbl_Salon
                //            .Select(p => new { p.Id, p.Name, objSer.Price, objSer.CoefficientRating, p.IsDelete, p.Publish })
                //            .OrderBy(p => p.Id).Where(p => p.IsDelete != 1 && p.Publish == true).ToList();
                //        return serialize.Serialize(Ltservice);
                //    }
                //    catch
                //    {
                //        var Ltservice = db.Tbl_Salon
                //            .Select(p => new { p.Id, p.Name, p.IsDelete, p.Publish })
                //            .OrderBy(p => p.Id).Where(p => p.IsDelete != 1 && p.Publish == true).ToList();
                //        return serialize.Serialize(Ltservice);
                //    }

            }
        }

        public string GenCode(int CodeInt)
        {
            string Code = "";
            int CodeLen = 5;
            try
            {
                Code = (CodeInt + 1).ToString();
                if (Code.Length < CodeLen)
                {
                    int loop = CodeLen - Code.Length;
                    for (int i = 0; i < loop; i++)
                    {
                        Code = "0" + Code;
                    }
                }
                return "SP" + Code;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        
        private bool IsUpdate()
        {
            int integer;
            Id = int.TryParse(Request.QueryString["Code"], out integer) ? integer : 0;
            return Id > 0 ? true : false;
        }

        public class ItemData
        {
            public int Id { get; set; }
            public int Price { get; set; }
            public int HeSo { get; set; }
            public bool Checked { get; set; }
        }
        public class Message
        {
            public string msg { get; set; }
            public bool status { get; set; }
        }
    }
}
