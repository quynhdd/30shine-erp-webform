﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Web.Services;
using System.Data.Entity.Migrations;
using System.Web.Script.Serialization;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.SalonService
{
    public partial class SalonServiceConfigAdd : System.Web.UI.Page
    {
        private bool Perm_Access = false;
        protected bool Perm_ViewAllData = false;
        public List<_30shine.MODEL.CustomClass.SalonServiceConfig> LstServiceConfig;
        private static SalonServiceConfigAdd instance;
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        private void ExecuteByPermission()
        {

            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        /// <summary>
        /// Get SalonServiceConfig instance
        /// </summary>
        /// <returns></returns>
        public static SalonServiceConfigAdd getInstance()
        {
            if (!(SalonServiceConfigAdd.instance is SalonServiceConfigAdd))
            {
                return new SalonServiceConfigAdd();
            }
            else
            {
                return SalonServiceConfigAdd.instance;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                Library.Function.bindSalon(new List<DropDownList> { ddlSalon }, Perm_ViewAllData);
                Bind_Service();
                BindDepartment();
                ddlService.SelectedIndex = 1;
                BindData();
            }
        }

        /// <summary>
        /// Bind Department to ddl
        /// </summary>
        protected void BindDepartment()
        {
            using (var db = new Solution_30shineEntities())
            {
                //chỉ lấy bộ phận stylist, skinner
                int[] iDepartmentId = { 1, 2, 4, 5, 6 };
                IEnumerable<Staff_Type> ccLstStaffDepartment = db.Staff_Type.Where(w => w.IsDelete == 0 && w.Publish == true && iDepartmentId.Contains(w.Id)).OrderBy(o => o.Id).ToList();
                ddlDepartment.DataTextField = "Name";
                ddlDepartment.DataValueField = "Id";
                ddlDepartment.DataSource = ccLstStaffDepartment;
                ddlDepartment.DataBind();
            }
        }

        /// <summary>
        /// Bind service to ddl
        /// </summary>
        protected void Bind_Service()
        {
            using (var db = new Solution_30shineEntities())
            {
                var _Service = db.Services.Where(w => w.IsDelete == 0 && w.Publish == 1).OrderBy(o => o.Id).ToList();
                var Key = 0;
                ddlService.DataTextField = "Name";
                ddlService.DataValueField = "Id";
                ListItem item = new ListItem("Chọn dịch vụ", "0");
                ddlService.Items.Insert(0, item);
                foreach (var v in _Service)
                {
                    Key++;
                    item = new ListItem(v.Name, v.Id.ToString());
                    ddlService.Items.Insert(Key, item);
                }
                ddlService.SelectedIndex = 0;
            }
        }

        /// <summary>
        /// onchange dropdownlist get data
        /// </summary>
        /// <param name="iSalonId"></param>
        /// <param name="iDepartmentId"></param>
        /// <param name="iServiceId"></param>
        [WebMethod]
        public static object OnchangeDropdownlist(string iSalonId, string iDepartmentId, string iServiceId)
        {
            var list = new List<_30shine.MODEL.CustomClass.SalonServiceConfig>();
            JavaScriptSerializer serialize = new JavaScriptSerializer();
            int integer;
            int salonId = int.TryParse(iSalonId, out integer) ? integer : 0;
            int departmentId = int.TryParse(iDepartmentId, out integer) ? integer : 0;
            int serviceId = int.TryParse(iServiceId, out integer) ? integer : 0;
            try
            {
                ISalonServiceConfig model = new SalonServiceConfig();
                if (salonId > 0)
                {
                    list = model.GetListBySalon(salonId, departmentId);
                }
                else if (serviceId > 0)
                {
                    list = model.GetListByService(serviceId, departmentId);
                }
            }
            //Retry
            //Libraries.Retry.Do(() => getInstance().GetData(salonId, departmentId, serviceId), TimeSpan.FromSeconds(1));
            catch (Exception ex)
            {
                // Push thông báo cho mọi người(by slack) khi có lỗi xảy ra
                new Helpers.PushNotiErrorToSlack().Push("GA1LZN9AM", ex.Message + ex.StackTrace + ".SalonServiceConfigAdd", "OnchangeDropdownlist", "dungnm",
                                                        HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host);
            }
            return serialize.Serialize(list);
        }

        /// <summary>
        /// onchange dropdownlist get data
        /// </summary>
        /// <param name="iSalonId"></param>
        /// <param name="iDepartmentId"></param>
        /// <param name="iServiceId"></param>
        [WebMethod]
        public static bool UpdateIsPublish(IEnumerable<_30shine.MODEL.CustomClass.SalonServiceConfig> obj)
        {
            ServiceSalonConfig record = new ServiceSalonConfig();
            var message = new Library.Class.cls_message();
            var exc = 0;
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    ISalonServiceConfig model = new SalonServiceConfig();
                    if (obj.Any())
                    {
                        foreach (var item in obj)
                        {
                            record = db.ServiceSalonConfigs.FirstOrDefault(f => f.SalonId == item.SalonId &&
                                                                                f.DepartmentId == item.DepartmentId &&
                                                                                f.ServiceId == item.ServiceId);
                            if (record != null)
                            {
                                record.IsPublish = item.IsPublish == 1 ? true : false;
                                exc = model.UpdatePublish(record.Id, Convert.ToBoolean(record.IsPublish));
                            }
                            else
                            {
                                record = new ServiceSalonConfig();
                                record.SalonId = item.SalonId;
                                record.ServiceId = item.ServiceId;
                                record.DepartmentId = item.DepartmentId;
                                record.ServiceCoefficient = item.ServiceCoefficient;
                                record.ServiceBonus = item.ServiceBonus;
                                record.CoefficientOvertimeHour = item.CoefficientOvertimeHour;
                                record.CoefficientOvertimeDay = item.CoefficientOvertimeDay;
                                record.IsPublish = item.IsPublish == 1 ? true : false;
                                exc = model.Add(record);
                            }
                        }
                    }
                }
                if (exc > 0)
                    message.success = true;
                else
                    message.success = false;
            }
            catch (Exception ex)
            {
                // Push thông báo cho mọi người(by slack) khi có lỗi xảy ra
                new Helpers.PushNotiErrorToSlack().Push("GA1LZN9AM", ex.Message + ex.StackTrace + ".SalonServiceConfigAdd", "UpdateIsPublish", "dungnm",
                                                        HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host);
            }
            return message.success;
        }

        /// <summary>
        /// bind data 
        /// </summary>
        private void BindData()
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    int integer;
                    int ISalon = int.TryParse(ddlSalon.SelectedValue, out integer) ? integer : 0;
                    int IService = int.TryParse(ddlService.SelectedValue, out integer) ? integer : 0;
                    int IDepartment = int.TryParse(ddlDepartment.SelectedValue, out integer) ? integer : 0;
                    ISalonServiceConfig model = new SalonServiceConfig();
                    if (ISalon > 0)
                    {
                        LstServiceConfig = model.GetListBySalon(ISalon, IDepartment);
                    }
                    else if (IService > 0)
                    {
                        LstServiceConfig = model.GetListByService(IService, IDepartment);
                    }
                }
            }
            catch (Exception ex)
            {
                // Push thông báo cho mọi người(by slack) khi có lỗi xảy ra
                new Helpers.PushNotiErrorToSlack().Push("GA1LZN9AM", ex.Message + ex.StackTrace, this.ToString() + ".SalonServiceConfigAdd", "dungnm",
                                                        HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host);
            }
        }

        [WebMethod]
        public static object AddOrUpdate(IEnumerable<_30shine.MODEL.CustomClass.SalonServiceConfig> obj)
        {
            var message = new Library.Class.cls_message();
            ServiceSalonConfig record = new ServiceSalonConfig();
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    ISalonServiceConfig model = new SalonServiceConfig();
                    if (obj.Any())
                    {
                        foreach (var item in obj)
                        {
                            record = db.ServiceSalonConfigs.FirstOrDefault(f => f.SalonId == item.SalonId &&
                                                                                f.DepartmentId == item.DepartmentId &&
                                                                                f.ServiceId == item.ServiceId);
                            if (record != null)
                            {
                                record.SalonId = item.SalonId;
                                record.ServiceId = item.ServiceId;
                                record.DepartmentId = item.DepartmentId;
                                record.ServiceCoefficient = item.ServiceCoefficient;
                                record.ServiceBonus = item.ServiceBonus;
                                record.CoefficientOvertimeHour = item.CoefficientOvertimeHour;
                                record.CoefficientOvertimeDay = item.CoefficientOvertimeDay;
                                record.IsPublish = item.IsPublish == 1 ? true : false;
                                record.ModifiedTime = DateTime.Now;
                                record.IsDelete = false;
                                db.ServiceSalonConfigs.AddOrUpdate(record);
                            }
                            else
                            {
                                record = new ServiceSalonConfig();
                                record.SalonId = item.SalonId;
                                record.ServiceId = item.ServiceId;
                                record.DepartmentId = item.DepartmentId;
                                record.ServiceCoefficient = item.ServiceCoefficient;
                                record.ServiceBonus = item.ServiceBonus;
                                record.CoefficientOvertimeHour = item.CoefficientOvertimeHour;
                                record.CoefficientOvertimeDay = item.CoefficientOvertimeDay;
                                record.IsPublish = item.IsPublish == 1 ? true : false;
                                record.CreatedTime = DateTime.Now;
                                record.IsDelete = false;
                                db.ServiceSalonConfigs.Add(record);
                            }
                        }
                        var exc = db.SaveChanges();
                        if (exc > 0)
                        {
                            message.success = true;
                        }
                        else
                        {
                            message.success = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // Push thông báo cho mọi người(by slack) khi có lỗi xảy ra
                new Helpers.PushNotiErrorToSlack().Push("GA1LZN9AM", ex.Message + ex.StackTrace + ".SalonServiceConfigAdd", "AddOrUpdate", "dungnm",
                                                        HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host);
            }
            return message;
        }
    }
}