﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="SalonServiceConfigAdd.aspx.cs" Inherits="_30shine.GUI.BackEnd.SalonService.SalonServiceConfigAdd" %>

<asp:Content ID="ServiceAdd" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <link href="../../../Assets/css/erp.30shine.com/salon-service-config/common.min.css" rel="stylesheet" />
        <style>
            div.complete input.btn-complete {
                color: #fff !important;
                background-color: black !important;
                padding: 10px 37px !important;
                margin-left: 5% !important;
                margin-bottom: 15px !important;
                font-size: 16px !important;
            }
        </style>
        <div class="alert alert-danger" id="success-alert">
            <span id="msg-alert"></span>
        </div>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Cấu hình dịch vụ và salon &nbsp;&#187; </li>
                        <%-- <li class="li-listing"><a href="/admin/salon-service/salon-dich-vu.html">Danh sách</a></li>
                        <li class="li-add"><a href="/admin/salon-service/them-moi.html">Thêm mới</a></li>--%>
                    </ul>
                </div>
            </div>
        </div>
        <div class="wp customer-add admin-product-add admin-service-add">
            <%-- Add --%>
            <div class="container wp960 content-wp">
                <!-- System Message -->
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <!-- END System Message -->
                <div class="row div-wp">
                    <div class="table-wp">
                        <div class="title-head">
                            <span>
                                <strong class="main-name">Cấu hình dịch vụ và salon
                                </strong>
                            </span>
                        </div>
                        <table class="table-add admin-product-table-add admin-service-table-add container">
                            <tbody>
                                <tr class="tr-margin" style="height: 20px;">
                                </tr>
                                <tr id="tr-filter">
                                    <td class="col-xs-2">
                                        <span class="field-wp">
                                            <asp:DropDownList ID="ddlSalon" CssClass="form-control select" onchange="onchangeSalon();" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                        </span>
                                    </td>
                                    <td class="col-xs-2">
                                        <span class="field-wp">
                                            <asp:DropDownList ID="ddlDepartment" CssClass="form-control select" onchange="onchangeDepartment();" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                        </span>
                                    </td>
                                    <td class="col-xs-5">
                                        <span class="field-wp"></span>
                                    </td>
                                    <td class="col-xs-2 field-service">
                                        <span class="field-wp">
                                            <asp:DropDownList ID="ddlService" CssClass="form-control select" onchange="onchangeService();" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <table class="admin-product-table-add admin-service-table-add table-sub">
                                        <thead>
                                            <tr id="tr-header">
                                                <th width="10%">Salon</th>
                                                <th>Service</th>
                                                <th style="display: none">SalonId</th>
                                                <th style="display: none">ServiceId</th>
                                                <th style="display: none">ConfigId</th>
                                                <th>Dịch vụ (hệ số)</th>
                                                <th>Tiền thưởng</th>
                                                <th>Tăng ca giờ (hệ số)</th>
                                                <th>Tăng ca ngày (hệ số)</th>
                                                <th>Publish</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="remove-check">
                                                <td></td>
                                                <td></td>
                                                <td style="display: none">
                                                    <input class="input-sub" onkeypress="return ValidateKeypress(/\d/,event);" value="" disabled /></td>
                                                <td style="display: none">
                                                    <input class="input-sub" onkeypress="return ValidateKeypress(/\d/,event);" value="" disabled /></td>
                                                <td style="display: none">
                                                    <input class="input-sub" onkeypress="return ValidateKeypress(/\d/,event);" value="" disabled /></td>
                                                <td>
                                                    <input class="input-sub box-notification onchange-coefficient" onchange="changeValue($(this));" onkeypress="return ValidateKeypress(/\d/,event);" onkeyup="reformatText(this)" value="" /></td>
                                                <td>
                                                    <input class="input-sub box-notification onchange-service-bonus" onchange="changeValue($(this));" onkeypress="return ValidateKeypress(/\d/,event);" onkeyup="reformatText(this)" value="" /></td>
                                                <td>
                                                    <input class="input-sub box-notification onchange-coefficient-hour" onchange="changeValue($(this));" onkeypress="return ValidateKeypress(/\d/,event);" onkeyup="reformatText(this)" value="" /></td>
                                                <td>
                                                    <input class="input-sub box-notification onchange-coefficient-day" onchange="changeValue($(this));" onkeypress="return ValidateKeypress(/\d/,event);" onkeyup="reformatText(this)" value="" /></td>
                                                <td>
                                                    <div class="checkbox box-notification">
                                                        <label>
                                                            <input type="checkbox" onclick="" class="check-for-all" />
                                                            <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                        </label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <%if (LstServiceConfig != null)
                                                {%>
                                            <%foreach (var item in LstServiceConfig)
                                                {%>
                                            <tr class="remove-check remove">
                                                <td><%=item.SalonName %></td>
                                                <td><%=item.ServiceName %></td>
                                                <td style="display: none">
                                                    <input class="input-sub salon-id" onkeypress="return ValidateKeypress(/\d/,event);" value="<%=item.SalonId %>" disabled /></td>
                                                <td style="display: none">
                                                    <input class="input-sub service-id" onkeypress="return ValidateKeypress(/\d/,event);" value="<%=item.ServiceId %>" disabled /></td>
                                                <td style="display: none">
                                                    <input class="input-sub config-id" onkeypress="return ValidateKeypress(/\d/,event);" value="<%=item.ConfigId %>" disabled /></td>
                                                <td>
                                                    <input class="input-sub service-coefficient" onkeypress="return ValidateKeypress(/\d/,event);" onkeyup="reformatText(this)" value="<%=item.ServiceCoefficient %>" /></td>
                                                <td>
                                                    <input class="input-sub service-bonus" onkeypress="return ValidateKeypress(/\d/,event);" onkeyup="reformatText(this)" value="<%=item.ServiceBonus %>" /></td>
                                                <td>
                                                    <input class="input-sub coefficient-hour" onkeypress="return ValidateKeypress(/\d/,event);" onkeyup="reformatText(this)" value="<%=item.CoefficientOvertimeHour %>" /></td>
                                                <td>
                                                    <input class="input-sub coefficient-day" onkeypress="return ValidateKeypress(/\d/,event);" onkeyup="reformatText(this)" value="<%=item.CoefficientOvertimeDay %>" /></td>
                                                <td>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input class="" type="checkbox" <%=item.IsPublish == 1 ? "Checked=\"True\"" : "" %>
                                                                onclick="UpdateIsPublishRecord($(this),<%=item.ConfigId %>, <%=item.SalonId %>, <%=item.ServiceId %>, <%=item.ServiceCoefficient %>,
                                                                                <%=item.ServiceBonus %>, <%=item.CoefficientOvertimeHour %>, <%=item.CoefficientOvertimeDay %>);" />
                                                            <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                        </label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <%} %>
                                            <%} %>
                                        </tbody>
                                    </table>
                                </tr>
                                <asp:HiddenField runat="server" ID="UrlKeyField" ClientIDMode="Static" />
                                <asp:HiddenField runat="server" ID="ImgCoverField" ClientIDMode="Static" />
                                <asp:HiddenField runat="server" ID="HDF_Images" ClientIDMode="Static" />
                                <asp:HiddenField runat="server" ID="HDF_Videos" ClientIDMode="Static" />
                                <asp:HiddenField runat="server" ID="HDF_ListItem" ClientIDMode="Static" />
                            </tbody>
                        </table>
                    </div>
                    <div class="complete">
                        <input class="btn-complete" onclick="AddOrUpdate();" type="button" value="Hoàn tất" />
                    </div>
                </div>
            </div>
        </div>
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <script type="text/javascript">
            $("#success-alert").hide();
            let salonId;
            let departmentId;
            let serviceId;
            let ArrDatas = [];
            let index;

            //onchange ddlService
            function onchangeService() {
                this.salonId = 0;
                this.departmentId = 0;
                let $selectSalon = $('#ddlSalon');
                let $selectDepartment = $('#ddlDepartment');
                //get value;
                let serviceId = $("#ddlService :selected").val();
                if (serviceId > 0 && serviceId != '') {
                    $selectSalon.val(`${this.salonId}`).trigger('change.select2');
                    bindData();
                }
            }

            //onchange ddlSalon
            function onchangeSalon() {
                this.serviceId = 0;
                let $selectService = $('#ddlService');
                let salonId = $("#ddlSalon :selected").val();
                if (salonId > 0 && salonId != '') {
                    //change val
                    $selectService.val(`${this.serviceId}`).trigger('change.select2');
                    bindData();
                }

            }

            //onchange ddlDepartment
            function onchangeDepartment() {
                this.serviceId = 0;
                let $selectService = $('#ddlService');
                //get value;
                let departmentId = $("#ddlDepartment :selected").val();
                let salonId = $("#ddlSalon :selected").val();
                let serviceId = $("#ddlService :selected").val();
                if ((salonId == '' || salonId == 0) && (serviceId == '' || serviceId == 0)) {
                    //noti
                    $("#success-alert").fadeTo(2500, 2500).slideUp(2500, function () {
                        $("#success-alert").slideUp(2500);
                    });
                    $("#msg-alert").text('Vui lòng chọn salon!!!');
                }
                else if ((salonId == '' || salonId == 0) && (departmentId > 0 && departmentId != '')) {
                    //noti
                    $("#success-alert").fadeTo(2500, 2500).slideUp(2500, function () {
                        $("#success-alert").slideUp(2500);
                    });
                    $("#msg-alert").text('Vui lòng chọn salon hoặc service!!!');
                    //change val
                    $selectService.val(`${this.serviceId}`).trigger('change.select2');
                }
                else if ((salonId != '' || salonId > 0) && (departmentId > 0 && departmentId != '')) {
                    //change val
                    $selectService.val(`${this.serviceId}`).trigger('change.select2');
                    bindData();
                }
            }

            //call ajax
            function bindData() {
                let departmentId = $("#ddlDepartment :selected").val();
                let salonId = $("#ddlSalon :selected").val();
                let serviceId = $("#ddlService :selected").val();
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/SalonService/SalonServiceConfigAdd.aspx/OnchangeDropdownlist",
                    data: '{iSalonId : "' + salonId + '", iDepartmentId : "' + departmentId + '", iServiceId : "' + serviceId + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        if (response.d != null) {
                            var jsonObj = $.parseJSON(response.d);
                            if (jsonObj != null) {
                                $(".table-sub tbody .remove").remove();
                                for (var i = 0; i < jsonObj.length; i++) {
                                    $(".table-sub").append($(getTable(jsonObj[i].SalonName, jsonObj[i].ServiceName, jsonObj[i].ConfigId,
                                        jsonObj[i].SalonId, jsonObj[i].ServiceId,
                                        jsonObj[i].ServiceCoefficient, jsonObj[i].ServiceBonus,
                                        jsonObj[i].CoefficientOvertimeHour,
                                        jsonObj[i].CoefficientOvertimeDay, jsonObj[i].IsPublish)));
                                }
                            }
                        }
                        removeLoading();
                    },
                    failure: function (response) { alert(response.d); }
                });
            }

            //create table
            function getTable(SalonName, ServiceName, ConfigId, SalonId, ServiceId, ServiceCoefficient,
                ServiceBonus, CoefficientOvertimeHour, CoefficientOvertimeDay, IsPublish) {
                let table = '<tr class="remove-check remove">' +
                    '<td>' + SalonName + '</td>' +
                    '<td>' + ServiceName + '</td>' +
                    '<td style="display:none">' + '<input class="input-sub salon-id" onkeypress="return ValidateKeypress(/\\d/,event);" onkeyup="reformatText(this)" value="' + SalonId + '"/>' + '</td>' +
                    '<td style="display:none">' + '<input class="input-sub service-id" onkeypress="return ValidateKeypress(/\\d/,event);" onkeyup="reformatText(this)" value="' + ServiceId + '"/>' + '</td>' +
                    '<td style="display:none">' + '<input class="input-sub config-id" onkeypress="return ValidateKeypress(/\\d/,event);" onkeyup="reformatText(this)" value="' + ConfigId + '"/>' + '</td>' +
                    '<td>' + '<input class="input-sub box-input1 service-coefficient" onkeypress="return ValidateKeypress(/\\d/,event);" onkeyup="reformatText(this)" value="' + ServiceCoefficient + '"/>' + '</td>' +
                    '<td>' + '<input class="input-sub box-input2 service-bonus" onkeypress="return ValidateKeypress(/\\d/,event);" onkeyup="reformatText(this)" value="' + ServiceBonus + '"/>' + '</td>' +
                    '<td>' + '<input class="input-sub box-input3 coefficient-hour" onkeypress="return ValidateKeypress(/\\d/,event);" onkeyup="reformatText(this)" value="' + CoefficientOvertimeHour + '"/>' + '</td>' +
                    '<td>' + '<input class="input-sub box-input4 coefficient-day" onkeypress="return ValidateKeypress(/\\d/,event);" onkeyup="reformatText(this)" value="' + CoefficientOvertimeDay + '"/>' + '</td>' +
                    '<td>' +
                    '<div class="checkbox"> ' +
                    '<label>' +
                    '<input type="checkbox" ' + (IsPublish == 1 ? 'checked = "checked"' : "") + ' onclick="UpdateIsPublishRecord($(this),' + ConfigId + ' , ' + SalonId + ', ' + ServiceId + ', ' + ServiceCoefficient + ',' + ServiceBonus + ',' + CoefficientOvertimeHour + ',' + CoefficientOvertimeDay + ');" />' +
                    '<span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>' +
                    '</label>' +
                    '</div>' +
                    '</td>' +
                    '</tr>';
                return table;
            }

            //get list id salon
            function getListData(ConfigId, SalonId, ServiceId, DepartmentId, ServiceCoefficient, ServiceBonus, CoefficientOvertimeHour, CoefficientOvertimeDay, IsPublish) {
                var recd = {};
                recd.ConfigId = ConfigId.toString().trim() != "" ? parseInt(ConfigId) : 0;
                recd.SalonId = SalonId.toString().trim() != "" ? parseInt(SalonId) : 0;
                recd.ServiceId = ServiceId.toString().trim() != "" ? parseInt(ServiceId) : 0;
                recd.DepartmentId = DepartmentId.toString().trim() != "" ? parseInt(DepartmentId) : 0;
                recd.ServiceCoefficient = ServiceCoefficient.toString().trim() != "" ? parseFloat(ServiceCoefficient.toString().replace(',', '')) : 0;
                recd.ServiceBonus = ServiceBonus.toString().trim() != "" ? parseFloat(ServiceBonus.toString().replace(',', '')) : 0;
                recd.CoefficientOvertimeHour = CoefficientOvertimeHour.toString().trim() != "" ? parseFloat(CoefficientOvertimeHour.toString().replace(',', '')) : 0;
                recd.CoefficientOvertimeDay = CoefficientOvertimeDay.toString().trim() != "" ? parseFloat(CoefficientOvertimeDay.toString().replace(',', '')) : 0;
                recd.IsPublish = IsPublish ? 1 : 0;
                ArrDatas.push(recd);
            }

            function UpdateIsPublishRecord(This, ConfigId, SalonId, ServiceId, ServiceCoefficient, ServiceBonus, CoefficientOvertimeHour, CoefficientOvertimeDay) {
                //get data to class tmp
                let DepartmentId = $("#ddlDepartment :selected").val();
                let IsPublish = This.prop("checked");
                getListData(ConfigId, SalonId, ServiceId, DepartmentId, ServiceCoefficient, ServiceBonus, CoefficientOvertimeHour, CoefficientOvertimeDay, IsPublish);
                if (ArrDatas != null) {
                    let data = JSON.stringify({ obj: ArrDatas });
                    $.ajax({
                        type: "POST",
                        url: "/GUI/BackEnd/SalonService/SalonServiceConfigAdd.aspx/UpdateIsPublish",
                        data: data,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            if (response.d != null) {
                                let $value = response.d;
                                if ($value) {
                                    //noti
                                    $("#success-alert").css("background-color", "#4CAF50");
                                    $("#msg-alert").css("color", "white");
                                    $("#success-alert").fadeTo(2500, 2500).slideUp(2500, function () {
                                        $("#success-alert").slideUp(2500);
                                    });
                                    $("#msg-alert").text('Cập nhật thành công!!!');
                                }
                                else {
                                    //noti
                                    $("#success-alert").fadeTo(2500, 2500).slideUp(2500, function () {
                                        $("#success-alert").slideUp(2500);
                                    });
                                    $("#msg-alert").text('Cập nhật thất bại!!!');
                                }
                            }
                            else {
                                //noti
                                $("#success-alert").fadeTo(2500, 2500).slideUp(2500, function () {
                                    $("#success-alert").slideUp(2500);
                                });
                                $("#msg-alert").text('Cập nhật thất bại!!!');
                            }
                        },
                        failure: function (response) { alert(response.d); }
                    });
                }
                else {
                    //noti
                    $("#success-alert").fadeTo(2500, 2500).slideUp(2500, function () {
                        $("#success-alert").slideUp(2500);
                    });
                    $("#msg-alert").text('Cập nhật thất bại!!!');
                }
            }

            //scroll tr-filter
            window.onscroll = function () { fucnFilter() };
            let filter = document.getElementById("tr-filter");
            let filters = filter.offsetTop;
            function fucnFilter() {
                if (window.pageYOffset >= filters) {
                    filter.classList.add("filter-fixed");
                } else {
                    filter.classList.remove("filter-fixed");
                }
            }

            // Validate keypress
            function ValidateKeypress(numcheck, e) {
                var keynum, keychar, numcheck;
                if (window.event) {
                    keynum = e.keyCode;
                }
                else if (e.which) {
                    keynum = e.which;
                }
                if (keynum == 8 || keynum == 127 || keynum == null || keynum == 9 || keynum == 0 || keynum == 13 || keynum == 46) return true;
                keychar = String.fromCharCode(keynum);
                var result = numcheck.test(keychar);
                return result;
            }

            //noti
            $(".table-sub tbody .remove-check td .box-notification").hover(
                function () {
                    //noti
                    index = $(this).parentsUntil('th').hover().index();
                    var value = $('.table-sub th').eq(index).text();
                    $("#success-alert").css("background-color", "yellow");
                    $("#success-alert").fadeTo(2500, 2500).slideUp(2500, function () {
                        $("#success-alert").slideUp(2500);
                    });
                    $("#msg-alert").text(`Nhập dữ liệu cho toàn bộ bản ghi của ${value}`);
                }
            );

            //Tự động nhập theo value all 
            function changeValue(This) {
                let data = $('.remove-check');
                let $value = This.val();
                if (This.hasClass("onchange-coefficient")) {
                    $.each(data, function (key, value) {
                        if ($value != '') {
                            $(this).find('.service-coefficient').val($value);
                        }
                    })
                }
                else if (This.hasClass("onchange-service-bonus")) {
                    $.each(data, function (key, value) {
                        if ($value != '') {
                            $(this).find('.service-bonus').val($value);
                        }
                    })
                }
                else if (This.hasClass("onchange-coefficient-hour")) {
                    $.each(data, function (key, value) {
                        if ($value != '') {
                            $(this).find('.coefficient-hour').val($value);

                        }
                    })
                }
                else if (This.hasClass("onchange-coefficient-day")) {
                    $.each(data, function (key, value) {
                        if ($value != '') {
                            $(this).find('.coefficient-day').val($value);

                        }
                    })
                }
            }

            //Show thông báo lên màn hình
            function showMsgSystem(msg, status) {
                $("#MsgSystem").css("opacity", 0).text(msg).addClass(status).fadeTo(0, 1);
                setTimeout(function () {
                    $("#MsgSystem").fadeTo("slow", 0, function () {
                        $("#MsgSystem").text("").attr("class", "msg-system").css("opacity", 1);
                    });
                }, 5000);
                $('html, body').animate({ scrollTop: 0 }, 'fast');

            }

            //checkbox for all
            $(".check-for-all").change(function () {
                $("input:checkbox").prop('checked', $(this).prop("checked"));
            });

            //000.000
            String.prototype.reverse = function () {
                return this.split("").reverse().join("");
            }

            //format money
            function reformatText(input) {
                var x = input.value;
                x = x.replace(/,/g, "");
                x = x.reverse();
                x = x.replace(/.../g, function (e) {
                    return e + ",";
                });
                x = x.reverse();
                x = x.replace(/^,/, "");
                input.value = x;
            }

            //Thêm hoặc update 
            function AddOrUpdate() {
                startLoading();
                var data = $(".remove-check");
                $.each(data, function (key, value) {
                    let ConfigId = $(this).find('td input.config-id').val();
                    let SalonId = $(this).find('td input.salon-id').val();
                    let ServiceId = $(this).find('td input.service-id').val();
                    let DepartmentId = $("#ddlDepartment :selected").val();
                    let ServiceCoefficient = $(this).find('td input.service-coefficient').val();
                    ServiceCoefficient = typeof ServiceCoefficient == 'undefined' ? 0 : ServiceCoefficient.toString().replace(',', '');
                    let ServiceBonus = $(this).find('td input.service-bonus').val();
                    ServiceBonus = typeof ServiceBonus == 'undefined' ? 0 : ServiceBonus.toString().replace(',', '');
                    let CoefficientOvertimeHour = $(this).find('td input.coefficient-hour').val();
                    CoefficientOvertimeHour = typeof CoefficientOvertimeHour == 'undefined' ? 0 : CoefficientOvertimeHour.toString().replace(',', '');
                    let CoefficientOvertimeDay = $(this).find('td input.coefficient-day').val();
                    CoefficientOvertimeDay = typeof CoefficientOvertimeDay == 'undefined' ? 0 : CoefficientOvertimeDay.toString().replace(',', '');
                    let IsPublish = $(this).find("input[type='checkbox']").prop("checked");
                    if (typeof ConfigId != 'undefined' && typeof SalonId != 'undefined' && typeof ServiceId != 'undefined') {
                        getListData(ConfigId, SalonId, ServiceId, DepartmentId, ServiceCoefficient, ServiceBonus, CoefficientOvertimeHour, CoefficientOvertimeDay, IsPublish);
                    }
                });
                if (ArrDatas != null) {
                    let data = JSON.stringify({ obj: ArrDatas });
                    $.ajax({
                        type: "POST",
                        url: "/GUI/BackEnd/SalonService/SalonServiceConfigAdd.aspx/AddOrUpdate",
                        data: data,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json", success: function (response) {
                            if (response.d.success) {
                                showMsgSystem("Hoàn tất thành công", "success");
                            }
                            else {
                                showMsgSystem("Hoàn tất thất bại", "danger");
                            }
                            finishLoading();
                        },
                        failure: function (response) { alert(response.d); }
                    });
                }
            }
        </script>
        <!-- Page loading -->
        <div class="page-loading">
            <p>Vui lòng đợi trong giây lát...</p>
        </div>
    </asp:Panel>
</asp:Content>
