﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Salon_Service_Add.aspx.cs" Inherits="_30shine.GUI.BackEnd.SalonService.Salon_Service_Add" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="ServiceAdd" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

        <style>
            .customer-add .content-wp { padding: 0 15%; }
        </style>

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Cấu hình dịch vụ và salon &nbsp;&#187; </li>
                        <li class="li-listing"><a href="/admin/salon-service/salon-dich-vu.html">Danh sách</a></li>
                        <li class="li-add"><a href="/admin/salon-service/them-moi.html">Thêm mới</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add admin-product-add admin-service-add">
            <%-- Add --%>
            <div class="wp960 content-wp">
                <!-- System Message -->
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <!-- END System Message -->

                <div class="table-wp">
                    <table class="table-add admin-product-table-add admin-service-table-add">
                        <tbody>
                            <tr class="title-head">
                                <td><strong>Cấu hình dịch vụ</strong></td>
                                <td></td>
                            </tr>
                            <tr class="tr-margin" style="height: 20px;"></tr>
                            <tr>
                                <td class="col-xs-2 left"><span>Salon&nbsp;</span></td>
                                <td class="col-xs-4 right">
                                    <span class="field-wp">
                                        <asp:DropDownList ID="ddlSalon" CssClass="form-control select" runat="server" ClientIDMode="Static" onchange="OnchangSalon()"></asp:DropDownList>
                                        <%--<asp:RequiredFieldValidator ID="ddlSalonValidate" ControlToValidate="Name" runat="server" CssClass="fb-cover-error" Text="Bạn chưa chọn salon!"></asp:RequiredFieldValidator>--%>
                                    </span>

                                </td>
                                <td class="col-xs-2 left"><span>Dịch vụ</span></td>
                                <td class="col-xs-4 right">
                                    <span class="field-wp">
                                        <asp:DropDownList ID="ddlService" CssClass="form-control" runat="server" ClientIDMode="Static" onchange="OnchangService()"></asp:DropDownList>
                                        <%--<asp:RequiredFieldValidator ID="ddlSalonValidate" ControlToValidate="Name" runat="server" CssClass="fb-cover-error" Text="Bạn chưa chọn salon!"></asp:RequiredFieldValidator>--%>
                                    </span>
                                </td>
                            </tr>

                            <tr>
                                <td class="col-xs-2 left rename-text"><span>Dịch vụ</span></td>
                                <td colspan="3" class="col-xs-4 right">
                                    <table class="table service-check">
                                        <tr>
                                            <td>
                                                <div class="checkbox" style="min-height: initial; margin-top: 0px; margin-bottom: 0px;">
                                                    <label class="lbl-cus-no-infor">
                                                        <input runat="server" clientidmode="Static" type="checkbox" onclick="checkAll(this)" />
                                                        Tất cả
                                                    </label>
                                                </div>
                                                <%--<asp:CheckBox ID="cbCheckAll1" runat="server" ClientIDMode="Static" OnClick="checkAll(this)" Text="Tất cả" />--%>
                                            </td>
                                            <td>Giá</td>
                                            <td>Hệ số điểm HL</td>
                                        </tr>

                                        <%if (list != null)
                                            {%>
                                        <%foreach (var item in list)
                                            { %>
                                        <tr class="remove-check">
                                            <td>
                                                <div class="checkbox" style="min-height: initial; margin-top: 0px; margin-bottom: 0px;">
                                                    <label class="lbl-cus-no-infor">                                                        
                                                        <input type="checkbox" <%=  Convert.ToBoolean(item.IsCheck) ? "checked='checked'" : ""%> value="<%= item.Id %>" class="checkbox-item" />
                                                        <%= item.Name %>                                                        
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <input type="number" name="price" class="price" value="<%= item.Price%>" />
                                            </td>
                                            <td>
                                                <input type="number" name="heso" class="heso" value="<%=item.HeSoHL %>" /></td>
                                        </tr>
                                        <%} %>
                                        <%}
                                            else
                                            {%>

                                        <%if (listSer != null)
                                            {%>

                                        <%foreach (var itemSer in listSer)
                                            { %>
                                        <tr class="remove-check">
                                            <td>
                                                <input type="checkbox" value="<%= itemSer.Id %>" class="checkbox-item" />
                                                <%= itemSer.Name %>
                                            </td>
                                            <td>
                                                <input type="number" name="price" class="price" value="<%=string.Format("{0:#,0.##}", itemSer.Price) %>" />
                                            </td>
                                            <td>
                                                <input type="number" name="heso" class="heso" value="<%=itemSer.CoefficientRating %>" /></td>
                                        </tr>
                                        <%} %>
                                        <%} %>
                                        <%} %>
                                    </table>
                                </td>
                            </tr>

                            <tr class="tr-send">
                                <td class="col-xs-2 left"></td>
                                <td colspan="3" class="col-xs-4 right no-border">
                                    <span class="field-wp">
                                        <a runat="server" class="btn-send" onclick="getListData()">Hoàn tất</a>
                                    </span>
                                </td>
                            </tr>
                            <asp:HiddenField runat="server" ID="UrlKeyField" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="ImgCoverField" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="HDF_Images" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="HDF_Videos" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="HDF_ListItem" ClientIDMode="Static" />
                        </tbody>
                    </table>
                </div>
            </div>
            <%-- end Add --%>
        </div>
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 190px !important;
                margin-top: 5px !important;
            }
            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }
        </style>
        <style>
            .be-report table.table-listing.table-listing-salary td {
                padding: 7px 5px !important;
            }
        </style>
        <script>
            jQuery(document).ready(function () {
                $("#glbAdminContent").addClass("active");
                $("#glbAdminService").addClass("active");
            });
        </script>

        <!-- Popup plugin image -->
        <script type="text/javascript">
            function popupImageIframe(StoreImgField) {
                var Width = 960;
                var Height = 560;
                var ImgList = [];
                var iframe = $("<div class='iframe-image-upload' style='width:" + Width + "px; height:" + Height + "px; float:left; display:none;'><iframe src='/GUI/BackEnd/UploadImage/Upload_V2.aspx?Code=img&StoreImgField=" + StoreImgField + "&callback=getThumbFromIframe&imgWidth=1334&imgHeight=750&multi_img=false&Folder=Common' style='width:100%; height: 100%;'></iframe></div");

                $("body").find(".iframe-image-upload").remove().end().append(iframe).find(".iframe-image-upload").openEBPopup();
            }

            window.getThumbFromIframe = function (Imgs, StoreImgField) {
                var imgs = "";
                var thumb = "";
                if (Imgs.length > 0) {
                    for (var i = 0; i < Imgs.length; i++) {
                        thumb += '<div class="thumb-wp">' +
                                    '<div class="left">' +
                                            '<img class="thumb" alt="" title="" src="' + Imgs[i] + '" data-img=""/>' +
                                    '</div>' +
                                    '<div class="right">' +
                                        '<input class="thumb-title" type="text" placeholder="Tiêu đề ảnh" />' +
                                        '<textarea class="thumb-des" placeholder="Mô tả"></textarea>' +
                                        '<div class="action">        ' +
                                            '<div class="action-item action-add" onclick="popupImageIframe(\'HDF_Images\')"><i class="fa fa-plus-square" aria-hidden="true"></i>Thêm ảnh</div>' +
                                            '<div class="action-item action-delete" onclick="deleteThumbnail($(this), \'' + StoreImgField + '\')"><i class="fa fa-trash-o" aria-hidden="true"></i>Xóa</div>' +
                                        '</div>' +
                                    '</div>' +
                                '</div>';
                    }
                    $("." + StoreImgField).find(".listing-img-upload").append($(thumb));
                    excThumbWidth(120, 120);
                }
                autoCloseEBPopup();
            }

            function imgLink(StoreImgField) {
                var imgs = [], src, title, description;
                $("." + StoreImgField).find(".thumb-wp").each(function () {
                    src = $(this).find("img.thumb").attr("src");
                    title = $(this).find("input.thumb-title").val();
                    description = $(this).find("textarea.thumb-des").val();
                    if (src.trim() != "") {
                        imgs.push({ url: src, thumb: executeThumPath(src), title: title, description: description });
                    }
                });
                return imgs;
            }

            function initImgStore(StoreImgField) {
                $("#" + StoreImgField).val(JSON.stringify(imgLink(StoreImgField)))
            }

            function initVideoStore(StoreVideoField) {
                var videos = [];
                var url = $("#VideoLink").val();
                var title = $("#VideoTitle").val();
                var description = $("#VideoDescription").val();
                videos.push({ url: url, title: title, description: description });
                $("#" + StoreVideoField).val(JSON.stringify(videos));
            }
             //
            function deleteThumbnail(This, StoreImgField) {
                This.parent().parent().parent().remove();
            }

            function clientScript() {
                initImgStore('HDF_Images');
                initVideoStore("HDF_Videos");

            }

            var itemData = function () {
                this.Id = 0;
                this.Price = 0;
                this.HeSo = 0;
                this.Checked = false;
            }

            function getListData1() {
                var salonId = $("#ddlSalon :selected").val();
                salonId = !isNaN(salonId) ? salonId : 0;
                var serviceId = $("#ddlService :selected").val();
                serviceId = !isNaN(serviceId) ? serviceId : 0;
                var isCheck = $("#ddlService :selected").val();
                if (salonId != 0 && serviceId != 0) {
                    alert("Vui lòng chọn salon hoặc dịch vụ");
                    return;
                }
                var list = [];
                var listNot = [];
                console.log("salon: " + salonId + " Service: " + serviceId);
                addLoading();
                if (salonId != 0) {
                    $(".service-check td input.checkbox-item").each(function () {
                        if ($(this).prop("checked") == true) {
                            var tr = $(this).parent().parent();
                            var id = parseInt($(this).val());
                            id = !isNaN(id) ? id : 0;
                            var price = parseInt(tr.find(".price").val());
                            price = !isNaN(price) ? price : 0;
                            var heso = parseInt(tr.find(".heso").val());
                            heso = !isNaN(heso) ? heso : 0;

                            var item = new itemData();
                            item.Id = id;
                            item.Price = price;
                            item.HeSo = heso;

                            list.push(item);
                        } else {
                            var tr = $(this).parent().parent();
                            var id = parseInt($(this).val());
                            id = !isNaN(id) ? id : 0;
                            var price = parseInt(tr.find(".price").val());
                            price = !isNaN(price) ? price : 0;
                            var heso = parseInt(tr.find(".heso").val());
                            heso = !isNaN(heso) ? heso : 0;

                            var item = new itemData();
                            item.Id = id;
                            item.Price = price;
                            item.HeSo = heso;

                            listNot.push(item);
                        }
                    });
                    console.log(list);

                    $.ajax({
                        type: "post",
                        url: "/GUI/BackEnd/SalonService/Salon_Service_Add.aspx/AddListItem",
                        data: '{SalonId : "' + salonId + '", ListService : \'' + JSON.stringify(list) + '\', ListServiceNot : \'' + JSON.stringify(listNot) + '\'}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            console.log(response.d);
                            if (response.d != null) {
                                alert(response.d);
                            } else {
                                console.log(" not ok");
                            }
                            removeLoading();
                        },
                        failure: function (response) { alert(response.d); }
                    });
                } else {
                    $(".service-check td input.checkbox-item").each(function () {
                        if ($(this).prop("checked") == true) {
                            var tr = $(this).parent().parent();
                            var id = parseInt($(this).val());
                            id = !isNaN(id) ? id : 0;
                            var price = parseInt(tr.find(".price").val());
                            price = !isNaN(price) ? price : 0;
                            var heso = parseInt(tr.find(".heso").val());
                            heso = !isNaN(heso) ? heso : 0;

                            var item = new itemData();
                            item.Id = id;
                            item.Price = price;
                            item.HeSo = heso;

                            list.push(item);
                        } else {
                            var tr = $(this).parent().parent();
                            var id = parseInt($(this).val());
                            id = !isNaN(id) ? id : 0;
                            var price = parseInt(tr.find(".price").val());
                            price = !isNaN(price) ? price : 0;
                            var heso = parseInt(tr.find(".heso").val());
                            heso = !isNaN(heso) ? heso : 0;

                            var item = new itemData();
                            item.Id = id;
                            item.Price = price;
                            item.HeSo = heso;

                            listNot.push(item);
                        }
                    });
                    console.log(list);

                    $.ajax({
                        type: "post",
                        url: "/GUI/BackEnd/SalonService/Salon_Service_Add.aspx/AddListSalon",
                        data: '{ServiceId : "' + serviceId + '", ListSalon : \'' + JSON.stringify(list) + '\', ListSalonNot : \'' + JSON.stringify(listNot) + '\'}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            console.log(response.d);
                            if (response.d != null) {
                                alert(response.d);
                            } else {
                                console.log(" not ok");
                            }
                            removeLoading();
                        },
                        failure: function (response) { alert(response.d); }
                    });
                }
            }

            function getListData() {
                var salonId = $("#ddlSalon :selected").val();
                salonId = !isNaN(salonId) ? salonId : 0;
                var serviceId = $("#ddlService :selected").val();
                serviceId = !isNaN(serviceId) ? serviceId : 0;
                var isCheck = $("#ddlService :selected").val();
                if (salonId != 0 && serviceId != 0) {
                    alert("Vui lòng chọn salon hoặc dịch vụ");
                    return;
                }
                var list = [];
                var listNot = [];
                console.log("salon: " + salonId + " Service: " + serviceId);
                addLoading();
                $(".service-check td input.checkbox-item").each(function () {
                    if ($(this).prop("checked") == true) {
                        var tr = $(this).parent().parent();
                        var trp = $(this).parent().parent().parent().parent();
                        var id = parseInt($(this).val());
                        id = !isNaN(id) ? id : 0;
                        var price = parseInt(trp.find(".price").val());
                        price = !isNaN(price) ? price : 0;
                        var heso = parseInt(trp.find(".heso").val());
                        heso = !isNaN(heso) ? heso : 0;
                        //console.log(price);

                        var item = new itemData();
                        item.Id = id;
                        item.Price = price;
                        item.HeSo = heso;
                        item.Checked = true;
                    } else {
                        var tr = $(this).parent().parent();
                        var trp = $(this).parent().parent().parent().parent();
                        var id = parseInt($(this).val());
                        id = !isNaN(id) ? id : 0;
                        var price = parseInt(trp.find(".price").val());
                        price = !isNaN(price) ? price : 0;
                        var heso = parseInt(trp.find(".heso").val());
                        heso = !isNaN(heso) ? heso : 0;

                        var item = new itemData();
                        item.Id = id;
                        item.Price = price;
                        item.HeSo = heso;
                        item.Checked = false;

                        //listNot.push(item);
                    }
                    list.push(item);
                });
                console.log(list);

                $.ajax({
                    type: "post",
                    url: "/GUI/BackEnd/SalonService/Salon_Service_Add.aspx/AddListItem",
                    data: '{SalonId : "' + salonId + '", ServiceId : ' + serviceId + ', ListItem : \'' + JSON.stringify(list) + '\'}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        console.log(response.d);
                        if (response.d != null) {
                            alert(response.d);
                        } else {
                            console.log(" not ok");
                        }
                        removeLoading();
                    },
                    failure: function (response) { alert(response.d); }
                });
            }

            function executeThumPath(src) {
                var str = "";
                var fileExtension = "";
                var strLeft = "";
                var strPush = "x350";
                if (src != null && src != "") {
                    var loop = src.length - 1;
                    for (var i = loop; i >= 0; i--) {
                        if (src[i] == ".") {
                            str = src.substring(0, i) + strPush + "." + fileExtension;
                            break;
                        } else {
                            fileExtension = src[i] + fileExtension;
                        }
                    }
                }
                return str;
            }

            function checkAll(cb) {
                //alert("abc");
                var ctrls = document.getElementsByTagName('input');
                for (var i = 0; i < ctrls.length; i++) {
                    var cbox = ctrls[i];
                    if (cbox.type == "checkbox") {
                        cbox.checked = cb.checked;
                    }
                }
            }

            function OnchangSalon() {
                var salonId = $("#ddlSalon :selected").val();
                $("#ddlService").val('0');
                $(".rename-text span").text("Dịch vụ");

                console.log(salonId);

                $.ajax({
                    type: "post",
                    url: "/GUI/BackEnd/SalonService/Salon_Service_Add.aspx/OnchangeSalon",
                    data: '{ SalonId: "' + salonId + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        console.log(response.d);
                        if (response.d != null) {
                            var jsonObj = $.parseJSON(response.d);
                            var html = "";
                            if (jsonObj != null) {
                                $(".service-check tbody .remove-check").remove();
                                for (var i = 0; i < jsonObj.length; i++) {
                                    //var ischeck = jsonObj[i].IsCheck;
                                    //ischeck = !isNaN(ischeck) ? "Co" : "khong";
                                    //console.log(ischeck);
                                    //if (ischeck == "Co")
                                    $(".service-check").append($(getTable(jsonObj[i].IsCheck, jsonObj[i].Id, jsonObj[i].Name, jsonObj[i].Price, jsonObj[i].HeSoHL)));
                                    //else {
                                    //    $(".service-check").append($(getTableNull(jsonObj[i].Id, jsonObj[i].Name, jsonObj[i].Price, jsonObj[i].CoefficientRating)));
                                    //}
                                }
                            }
                        }
                    },
                    failure: function (response) { alert(response.d); }
                });
            }

            function getTable(IsCheck, ServiceId, Service, Price, HeSoHL) {
                return '<tr class="remove-check">' +
                          '<td>' +
                            '<div class="checkbox" style="min-height: initial; margin-top: 0px; margin-bottom: 0px;">' +
                                '<label class="lbl-cus-no-infor"> ' +
                                    '<input type="checkbox" ' + ((IsCheck != null && Boolean(IsCheck)) ? 'checked = "checked"' : "") + ' value="' +
                                     ServiceId + '" class="checkbox-item" />' + Service + '' +
                            '</label></div>' +
                        '</td>' +
                         '<td>' +
                             '<input type="number" name="price" class="price" value="' + Price + '" />' +
                          '</td>' +
                          '<td>' +
                             '<input type="number" name="heso" class="heso" value="' + HeSoHL + '" />' +
                         '</td>' +
                    '</tr>';
            }

            function getTableNull(Id, Name, Price, CoefficientRating) {
                return '<tr class="remove-check">' +
                           '<td>' +
                            '<input type="checkbox" value="' + Id + '" class="checkbox-item" />' + Name + '' +
                          '</td>' +
                          '<td>' +
                            '<input type="number" name="price" class="price" value="' + Price + '" /> ' +
                           '</td>' +
                           '<td>' +
                            '<input type="number" name="heso" class="heso" value="' + HeSoHL + '" /></td>' +
                         '</tr>';
            }

            function OnchangService() {
                var serviceId = $("#ddlService :selected").val();
                $("#ddlSalon").val('0');
                $(".rename-text span").text("Salon");

                console.log(serviceId);

                $.ajax({
                    type: "post",
                    url: "/GUI/BackEnd/SalonService/Salon_Service_Add.aspx/OnchangeService",
                    data: '{ ServiceId: "' + serviceId + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        console.log(response.d);
                        if (response.d != null) {
                            var jsonObj = $.parseJSON(response.d);
                            var html = "";
                            if (jsonObj != null) {
                                $(".service-check tbody .remove-check").remove();
                                for (var i = 0; i < jsonObj.length; i++) {
                                    //var ischeck = jsonObj[i].IsCheck;
                                    //ischeck = !isNaN(ischeck) ? "Co" : "khong";
                                    //console.log(ischeck);
                                    //if (ischeck == "Co")
                                    $(".service-check").append($(getTable(jsonObj[i].IsCheck, jsonObj[i].Id, jsonObj[i].Name, jsonObj[i].Price, jsonObj[i].HeSoHL)));
                                    //else {
                                    //    $(".service-check").append($(getTableNull(jsonObj[i].Id, jsonObj[i].Name, jsonObj[i].Price, jsonObj[i].CoefficientRating)));
                                    //}
                                }
                            }
                        }
                    },
                    failure: function (response) { alert(response.d); }
                });
            }

        </script>
        <!-- Popup plugin image -->

    </asp:Panel>
</asp:Content>

