﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Edit_Roles_V3.aspx.cs" Inherits="_30shine.GUI.BackEnd.Permission.Edit_Roles_V3" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">


        <style>
            .module{
                display:block;
                
                font-family: Roboto Condensed Regular !important;
               overflow:hidden;

            }
            .module .content{
                height:300px;
                overflow-y:scroll;
                
                margin-bottom:30px;
            }
            .module h3 {
                width: 100%;
                text-align: center;
                padding: 5px 0px;
                font-weight: bold;
                background: #222222;
                color:white;

            }
            .clear{
                clear:both;
            }
            .customer-listing table.table-listing > tbody > tr.active {
    background: #fff !important;
}
            .customer-add .table-add td span, .customer-add .table-add td input[type="text"], .customer-add .table-add td select {
                 height: auto !important; 
                 line-height: 20px !important; 
                width: 100%;
                float: left;
            }
            .table-listing .uv-avatar { width: 120px; }
            .fb-cover-error{
                 top:100px !important;
                right:10px !important;
                position:fixed !important;
                width:20% !important;
                z-index:2000;
            }
            #txtSearch{
                /*width:220px !important;*/
                margin:0 auto;
                background:url("/Assets/images/search-icon.png");
                background-size: 20px 20px !important;
                background-color:#f7f5f5; 
                background-position: 14px 11px; /* Position the search icon */
                background-repeat: no-repeat; /* Do not repeat the icon image */
                width: 90%; /* Full-width */
                float:right;
                font-size: 15px !important; /* Increase font-size */
                padding: 20px 20px 20px 40px; /* Add some padding */
                border: 1px solid #ddd; /* Add a grey border */
                margin-bottom: 12px; /* Add some space below the input */
                -moz-border-radius:5px;
                -webkit-border-radius:5px;
                border-radius:5px;
                margin-right:20px
            }
            #txtSearch:focus {
                border: 1px solid #696969;
            }
            .be-report table.table-listing td .checkbox, .be-report table.table-listing td .radio {
                left: 0 !important;
                margin: 1px 4px;
            }
          #insert-perm{
            padding:6px 20px;
            display:block;
            background:#000000;
            color:white;
            border:none;
            width:auto;
            line-height:24px;
      
            margin:0 auto;
        }
         #insert-perm:hover{
             color:yellow;
        }
         .slAction {
             width: 90% !important;
         }
         .customer-listing table.table-listing th, .customer-listing table.table-listing td{
             padding:7px 0px !important;
         }
         .btn-group {
             width: 100%;
         }
            .btn-group>.btn, .btn-group-vertical>.btn, .multiselect-container{
            width: 90%;
            margin-left: 20px !important;
        }
        </style>

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Phân quyền &nbsp;&#187; </li>
                        <li class="li-listing active"><a href="/admin/phan-quyen/danh-sach-phan-quyen.html">Danh sách phân quyền</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing be-report be-report-timekeeping">
            <div class="wp960 content-wp">
       
               <div class="row">
                   <div class="col-xs-4"></div>
                   <div class="col-xs-5">
                   <div clas="dropdown-list">
                       <asp:DropDownList runat="server" ID="ddlPermission" ClientIDMode="Static" CssClass="form-control" Width="225px">
                       </asp:DropDownList>
                       <asp:DropDownList runat="server" ID="ddlModule" ClientIDMode="Static" CssClass="form-control" OnChange="BindMenu()" Width="225px">
                       </asp:DropDownList>
                   </div>
                   <div class="checkbox" style="margin-left: 500px; line-height: 12px;">
                       <label><input type="checkbox" id="all-permission" style="margin-top: 0px !important"/>Gán full quyền</label>
                   </div>
                   </div>
                </div>
                <div class="row">
                    <strong class="st-head left"><i class="fa fa-file-text"></i>Danh sách Menu</strong>
                    <div id="div-search" style="width: 100%;background: #dddddd !important">
                    <input type="text" name="txtSearch" class="right" id="txtSearch"  placeholder="Tìm kiếm theo đường dẫn" onkeyup="SearchMenu()"/>
                    </div>
                </div>
                        <div class="row table-wp" id="show-menu">
                        </div>
                        <div class="row show-alert"></div>
                        <div class="row button">
                            <button type="button" id="insert-perm" class="be-report btn-viewdata" onclick="AddPermission()">Cập nhật</button>
                        </div>
            </div>
        </div>
        <div class="loading-frame" style="display:none;"></div>
        <link href="/Assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" />
        <script src="/Assets/js/jquery.mCustomScrollbar.js"></script>
        <script src="/Assets/js/bootstrap-multiselect.js"></script>
        <script>
            // lấy toàn bộ dữ liệu từ bảng Tbl_Permission_Menu
            function BindMenu() {
                var moduleID = $("#ddlModule").val();
                if (moduleID == 0) {
                    alert("Vui lòng chọn MODULE");
                } else {
                    addLoading();
                    $.ajax({
                        url: "/GUI/BackEnd/Permission/Edit_Roles_V3.aspx/GetModule",
                        data: "{moduleId:" + moduleID + "}",
                        contentType: "application/json;charset:UTF-8",
                        datatype: "JSON",
                        type: "POST",
                        success:function(data) {
                            var jsonData = JSON.parse(data.d);
                            var table = "<table id='table-" + $("#ddlModule").val() +"' class='table-add table-listing table-menu' style='margin:15px 0px'>";
                            table +="<thead>";
                            table +="<tr style='background:#666666 !important'><th colspan='5'>" +
                                    "<span style='float:left;padding-left:20px;color:#fff'>" + $("#ddlModule option:selected").text() + "</span>" +
                                    "<span style='float:right;padding-right:2px;color:#fff'>" +
                                    "<button type='button' style='border:none;background:none;' onclick='RemoveTable($(this),"+moduleID+")' title='Xóa Module'>X</button>" +
                                    "</span></th></tr>";
                            table += "<tr>";
                            table +="<th>MenuID</th>" +
                                    "<th>Menu Name</th>" +
                                    "<th>Link</th>" +
                                    "<th>Action</th>" +
                                    "<th><input type='checkbox' name='checkall[]' class='checkall' title='Chọn tất cả' onchange='checkAll(this)'></th>";
                            table += "</tr>";
                            table += "</thead>";
                            table += "<tbody>";
                            for (var i = 0; i < jsonData.length;  i++) {
                                table += "<tr>";
                                table +="<td class='menu-id' style='width:10%'>" + jsonData[i].Id + "</td>" +
                                        "<td class='menu-name' style='width:20%'>" + jsonData[i].MenuName + "</td>" +
                                        "<td class='menu-link'style='width:auto'>" + jsonData[i].UrlRewrite +"</td>" +
                                        "<td class='menu-action' style='width:20%'><select class='form-control slAction' multiple='multiple'>";
                                for (var j = 0; j < jsonData[i].ClsActions.length ; j++) {
                                    table += "<option value='" + jsonData[i].ClsActions[j].ActionId + "'";
                                    jsonData[i].ClsActions[j].Isset > 0 ? table += "selected='selected'" : "";
                                    table += "title ='" + jsonData[i].ClsActions[j].ActionDesc + "'>" + jsonData[i].ClsActions[j].ActionName + "</option>";
                                }
                                table +="</select></td>" +
                                        "<td style='width:20px' class='get-all-page'><input type='checkbox'  name='checkbox' ";
                                jsonData[i].Isset > 0 ? table += "checked='checked'" : table += "";
                                table += "class='checkbox'></td>";
                                table += "</tr>";   
                            }
                            table += "</tbody>";
                            table += "</table>";
                            table += "</div>";
                            $("#show-menu").append(table);
                            $(".slAction").multiselect({
                                includeSelectAllOption: true
                            });
                        },
                        complete:function() {
                            removeLoading();
                        }
                    });
                }
            }
            //remove table 
            function RemoveTable(This, moduleID) {
                This.parents().closest("table").remove();
            }
            //check all checkbox in one table
            function checkAll(ele) {
                var checkboxes = $(ele).parent().closest("table").find(".get-all-page").find(".checkbox");
                if (ele.checked) {
                    for (var i = 0; i < checkboxes.length; i++) {
                        if (checkboxes[i].type == 'checkbox') {
                            checkboxes[i].checked = true;
                        }
                    }
                } else {
                    for (var i = 0; i < checkboxes.length; i++) {
                        if (checkboxes[i].type == 'checkbox') {
                            checkboxes[i].checked = false;
                        }
                    }
                }
            }
            function AddPermission() {
                if ($("#all-permission").prop("checked") == false) {
                    AddOrUpdate();
                }
                // gán full quyền cho nhóm quyền
                else {
                    var c = confirm("Bạn có chắc chắn muốn gán full quyền cho nhóm quyền này không?");
                    if (c == false) {
                        $("#all-permission").prop("checked",false);
                    } else {
                        $("#all-permission").prop("checked", true);
                        addLoading();
                        $.ajax({
                            url: "/GUI/BackEnd/Permission/Edit_Roles_V3.aspx/SetFullPermission",
                            contentType: "application/json;charset:UTF-8",
                            datatype: "JSON",
                            type: "POST",
                            success: function(data) {
                                $(".show-alert").empty().html(JSON.parse(data.d));
                            },
                            complete:function() {
                                removeLoading();
                            }
                        });
                    }
                }  
            }
            //Add new or update permission
            function AddOrUpdate() {
                var MenuArray = [];
                var tableControl = $("div#show-menu table.table-menu");
                var pID = <%=Request.QueryString["Code"]%>;
                var pName = $("#ddlPermission option:selected").text();
                $(".checkbox:checked", tableControl).each(function () {
                    var aID = null;
                    var mID = $(this).closest('tr').find('.menu-id').text();
                    var mName = $(this).closest('tr').find('.menu-name').text();
                    var mLink = $(this).closest('tr').find('.menu-link').text();
                    var mAction = $(this).closest('tr').find('.menu-action').find(".slAction").val();
                    if (mAction != null) {
                        mAction.forEach(function(aID) {
                            MenuArray.push({ pID, pName, mID, mName, mLink, aID });
                        });
                    } else {
                        MenuArray.push({ pID, pName, mID, mName, mLink, aID });
                    }
                });
                if (MenuArray.length > 0) {
                    console.log(MenuArray);
                    addLoading();
                    $.ajax({
                        type: "POST",
                        contentType: "application/json;charset:UTF-8",
                        datatype: "JSON",
                        url: "/GUI/BackEnd/Permission/Edit_Roles_V3.aspx/ChangeMenu",
                        data: "{list:" + JSON.stringify(MenuArray) + "}",
                        success: function (data) {
                            $(".show-alert").empty().html(JSON.parse(data.d));
                        },
                        complete: function () {
                            removeLoading();
                        }
                    });
                }
                else {
                    $(".show-alert").empty().append("<span style='color:red'>Vui lòng chọn module để thêm mới hoặc update</span>");
                }
            }
            //search menu theo link
            function SearchMenu() {
                var input, filter, table, tr, td, i;
                input = document.getElementById("txtSearch");
                filter = input.value.toUpperCase();
                table = document.getElementsByClassName("table-menu");
                for (var k = 0; k < table.length; k++) {
                tr = table[k].getElementsByTagName("tr");
                // Loop through all table rows, and hide those who don't match the search query
                for (i = 0; i < tr.length; i++) {
                    td = tr[i].getElementsByTagName("td")[2];
                    if (td) {
                        if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                            tr[i].style.display = "";
                        } else {
                            tr[i].style.display = "none";
                        }
                    }
                }
                }
            }

            //fix searchbox on top
            jQuery(function ($) {
                function fixDiv() {
                    var $cache = $('#div-search');
                    if ($(window).scrollTop() > 200)
                        $cache.css({
                            'position': 'fixed',
                            'left': '2px',
                            'top': '1px',
                            'background': 'inherit',
                            'z-index': '999',
                            
                        });
                    else
                        $cache.css({
                            'position': 'relative',
                            'top': 'auto'
                        });
                }
                $(window).scroll(fixDiv);
                fixDiv();
            });
        </script>
    </asp:Panel>
</asp:Content>
