﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.Permission
{

    public partial class PermisionMenuList : System.Web.UI.Page
    {
        IPermisionMenu menuModel = new PermisionMenuModel();
        protected bool Perm_Access = false;
        protected Paging PAGING = new Paging();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindDdlMenuName();
                BindData();
            }
        }

        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void BindData()
        {
            try
            {
                int integer = -1;
                int MenuId = int.TryParse(ddlParentMenu.SelectedValue, out integer) ? integer : -1;
                List<MODEL.ENTITY.EDMX.PermissionMenu> list = new List<PermissionMenu>();
                if (MenuId == -1)
                {
                    Msg result = menuModel.GetList();
                    if (result.success)
                    {
                        list = (List<PermissionMenu>)result.data;
                    }
                    else
                    {
                        WaringClient(result.msg);
                        return;
                    }
                }
                else
                {
                    Msg result = menuModel.GetList(MenuId);
                    if (result.success)
                    {
                        list = (List<PermissionMenu>)result.data;
                    }
                    else
                    {
                        WaringClient(result.msg);
                        return;
                    }
                }

                Bind_Paging(list.Count());
                Rpt.DataSource = list.Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                Rpt.DataBind();
            }
            catch (Exception ex)
            {
                WaringClient(ex.Message);
            }

        }
        /// <summary>
        /// button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void _BtnClick(object sender, EventArgs e)
        {
            BindData();
            RemoveLoading();
        }

        /// <summary>
        /// Paging
        /// </summary>
        /// <param name="TotalRow"></param>
        protected void Bind_Paging(int TotalRow)
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(TotalRow) : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        /// <summary>
        /// Get_TotalPage
        /// </summary>
        /// <param name="TotalRow"></param>
        /// <returns></returns>
        protected int Get_TotalPage(int TotalRow)
        {
            TotalRow -= PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        }
        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        /// <summary>
        /// Gửi thông tin cảnh báo về client
        /// </summary>
        /// <param name="message"></param>
        public void WaringClient(string message)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Click", "ShowWarning('" + message + "');", true);
        }
        /// <summary>
        /// Bind dữ liệu vào dropplist menu
        /// </summary>
        private void BindDdlMenuName()
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var _list = db.PermissionMenus.Where(r=>r.IsDelete == false).OrderBy(o => o.Id).ToList();
                    ddlParentMenu.Items.Clear();
                    ddlParentMenu.Items.Add(new ListItem("--- Chọn menu ---", "-1"));
                    if (_list.Count > 0)
                    {
                        for (int i = 0; i < _list.Count; i++)
                        {
                            ddlParentMenu.Items.Add(new ListItem(_list[i].Name, _list[i].Id.ToString()));
                        }
                        ddlParentMenu.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                ddlParentMenu.Items.Clear();
                ddlParentMenu.Items.Add(new ListItem("--- Không tìm thấy menu ---", "0"));
                //throw new Exception(ex.Message);
            }
        }

        [WebMethod(EnableSession = true)]
        public static Msg DeleleMenu(int Id)
        {
            Msg result = new Msg();
            using (Solution_30shineEntities db = new Solution_30shineEntities())
            {
                try
                {
                    var record = db.PermissionMenus.Where(r=>r.Id == Id).FirstOrDefault();
                    if (record != null)
                    {
                        record.IsDelete = true;
                        db.PermissionMenus.AddOrUpdate(record);
                        var exc = db.SaveChanges();
                        if (exc > 0)
                        {
                            result.success = true;
                            result.msg = "Xóa thành công bản ghi";
                        }
                        else
                        {
                            result.success = false;
                            result.msg = "Không xóa được bản ghi";
                        }
                    }
                    else
                    {
                        result.success = false;
                        result.msg = "Không tìm thấy bản ghi";
                    }
                }
                catch (Exception ex)
                {
                    result.success = false;
                    result.msg = ex.Message;
                }
            }
            return result;

        }
    }
}