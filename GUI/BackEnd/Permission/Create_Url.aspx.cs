﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Data.Entity.Migrations;
using System.Xml;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.Permission
{
    public partial class Create_Url : System.Web.UI.Page
    {
        private string PageID = "";
        public static List<cls_url> list = new List<cls_url>();
        private bool Perm_Access = false;
        protected bool _IsUpdate = false;
        protected string _Code;
        protected Tbl_Permission_Menu OBJ;
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                BindMenuCha();
                BindRewriteUrl();
                if (IsUpdate())
                {
                    Bind_OBJ();
                }
            }
        }
        #region[IsUpdate]
        private bool IsUpdate()
        {
            _Code = Request.QueryString["Code"];
            if (_Code != null)
            {
                _IsUpdate = true;
                return true;
            }
            else
            {
                _IsUpdate = false;
                return false;
            }
        }
        #endregion
        #region[BindOBJ]
        private bool Bind_OBJ()
        {
            using (var db = new Solution_30shineEntities())
            {
                var ExistOBJ = true;
                int Id;
                if (int.TryParse(_Code, out Id))
                {
                    OBJ = db.Tbl_Permission_Menu.FirstOrDefault(w => w.mID == Id);
                    if (!OBJ.Equals(null))
                    {
                        if (OBJ.mParentID != 0)
                        {
                            ddlMenuName.Text = OBJ.mParentID.ToString();
                        }
                        Name.Text = OBJ.mName;
                        ddlURL.Items.Add(new ListItem(OBJ.Url_Rewrite));
                        ddlMainUrl.Items.Add(new ListItem(OBJ.Url_Main));
                        if (OBJ.mPublish == true)
                            Publish.Checked = true;
                        else
                            Publish.Checked = false;
                        if (OBJ.MenuShow == true)
                        {
                            ShowHideMenu.Checked = true;
                        }
                        else ShowHideMenu.Checked = false;
                    }
                    else
                    {
                        ExistOBJ = false;
                        var msg = "Lỗi! Tên menu  không tồn tại!";
                        var status = "error warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                    }
                }
                else
                {
                    ExistOBJ = false;
                    ExistOBJ = false;
                    var msg = "Lỗi! Tên menu không tồn tại!";
                    var status = "error warning";
                    UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                }
                return ExistOBJ;
            }

        }
        #endregion
        #region[Button]
        protected void ExcAddOrUpdate(object sender, EventArgs e)
        {
            if (IsUpdate())
            {
                Update();
            }
            else
            {
                Add();
            }
        }
        #endregion
        #region[Update]
        protected void Update()
        {
            using (var db = new Solution_30shineEntities())
            {
                int Id;
                int integer;
                if (int.TryParse(_Code, out Id))
                {
                    OBJ = db.Tbl_Permission_Menu.FirstOrDefault(w => w.mID == Id);
                    if (!OBJ.Equals(null))
                    {
                        OBJ.mName = Name.Text;
                        OBJ.Url_Rewrite = ddlURL.SelectedItem.ToString();
                        OBJ.Url_Main = ddlMainUrl.SelectedItem.ToString();
                        OBJ.mParentID = int.TryParse(ddlMenuName.SelectedValue,out integer) ? integer : 0;
                        if (Publish.Checked)
                            OBJ.mPublish = true;
                        else
                            OBJ.mPublish = false;
                        db.Tbl_Permission_Menu.AddOrUpdate(OBJ);
                        var exc = db.SaveChanges();
                        if (exc > 0)
                        {
                            var MsgParam = new List<KeyValuePair<string, string>>();
                            MsgParam.Add(new KeyValuePair<string, string>("msg_add_status", "success"));
                            MsgParam.Add(new KeyValuePair<string, string>("msg_add_message", "Cập nhật thành công!"));
                            UIHelpers.Redirect("/admin/phan-quyen/danh-sach-menu.html", MsgParam);
                        }
                        else
                        {
                            var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển!";
                            var status = "warning";
                            UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                        }
                    }
                    else
                    {
                        var msg = "Lỗi!Không tìm thấy tên menu!";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                    }
                }
            }
        }
        #endregion
        #region[Add]
        protected void Add()
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var _obj = new Tbl_Permission_Menu();
                    _obj.mName = Name.Text;
                    _obj.Url_Rewrite = ddlURL.SelectedItem.ToString();
                    _obj.Url_Main = ddlMainUrl.SelectedItem.ToString();
                    if (ddlMainUrl.SelectedValue == "0")
                        _obj.mParentID = 0;
                    else
                        _obj.mParentID = Convert.ToInt32(ddlMenuName.SelectedValue);
                    if (Publish.Checked == true)
                        _obj.mPublish = true;
                    else
                        _obj.mPublish = false;

                     
                    if (ShowHideMenu.Checked == true)
                    {
                        _obj.MenuShow = true;
                    }
                    else if (ShowHideMenu.Checked == false)
                    {
                        _obj.MenuShow = false;
                    }
                    else
                    {
                        _obj.MenuShow = true;
                    }
                    var Error = false;
                    if (!Error)
                    {
                        db.Tbl_Permission_Menu.Add(_obj);
                        db.SaveChanges();
                        var MsgParam = new List<KeyValuePair<string, string>>();
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Thêm mới thành công!"));
                        UIHelpers.Redirect("/admin/phan-quyen/danh-sach-menu.html", MsgParam);
                    }
                    else
                    {
                        var msg = "Thêm mới không thành công. Vui lòng liên hệ với nhóm phát triển!";
                        var status = "msg-system warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 1000);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                if (Request.QueryString["Code"] != null)
                {
                    PageID = "PQ_MENU_EDIT";
                }
                else
                {
                    PageID = "PQ_MENU_ADD";
                }
                IPermissionModel permissionModel = new PermissionModel();
                var permission = Session["User_Permission"].ToString();
                Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_ShowSalon = permissionModel.GetActionByActionNameAndPageId("Perm_ShowSalon", PageID, permission);
                //Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //Perm_ViewAllDate = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllDate", PageID, permission);
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        #region[BindRewriteMainUrl]
        private void BindRewriteUrl()
        {
            try
            {
                int id = 1;
                var sPath = HttpContext.Current.Server.MapPath("\\Config\\Config.xml");
                var xmldoc = new XmlDocument();
                xmldoc.Load(sPath);
                var node = xmldoc.SelectSingleNode("CONFIG");
                list.Clear();
                foreach (XmlNode v in node)
                {
                    var item = new cls_url();
                    item.miD = id;
                    item.rewriteurl = v.Attributes["url"].Value;
                    item.mainUrl = v.Attributes["to"].Value;
                    list.Add(item);
                    id++;
                }
                if (list.Count > 0)
                {
                    ddlURL.Items.Add(new ListItem("--- Chọn liên kết ---", "0"));
                    for (int j = 0; j < list.Count; j++)
                    {
                        var key = list[j].rewriteurl.Substring(2);
                        var item = ddlURL.Items.FindByText(key);
                        if (item == null)
                        {
                            ddlURL.Items.Add(new ListItem(list[j].rewriteurl.Substring(2), list[j].miD.ToString()));
                        }                        
                    }
                    ddlURL.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        protected void ddlURL_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int _iD = Convert.ToInt32(ddlURL.SelectedValue);
                ddlMainUrl.Items.Clear();
                if (_iD != 0)
                {
                    var _list = list.FirstOrDefault(c => c.miD == _iD);
                    if (_list != null)
                    {
                        ddlMainUrl.Items.Add(new ListItem(_list.mainUrl));
                    }
                    ddlMainUrl.DataBind();
                }
                else
                { ddlMainUrl.Items.Clear(); }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        #endregion
        #region[BindMenuCha]
        private void BindMenuCha()
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var _list = db.Tbl_Permission_Menu.OrderBy(o => o.mID).ToList();
                    ddlMenuName.Items.Clear();
                    ddlMenuName.Items.Add(new ListItem("--- Chọn danh mục menu ---", "0"));
                    if (_list.Count > 0)
                    {
                        for (int i = 0; i < _list.Count; i++)
                        {
                            ddlMenuName.Items.Add(new ListItem(_list[i].mName, _list[i].mID.ToString()));
                        }
                        ddlMenuName.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        #endregion
        public class cls_url
        {
            public int miD { get; set; }
            public string rewriteurl { get; set; }
            public string mainUrl { get; set; }
        }
    }
}