﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/TemplateMaster/SiteMaster.Master" CodeBehind="Edit_Roles_V4.aspx.cs" Inherits="_30shine.GUI.BackEnd.Permission.Edit_Roles_V4" %>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">


        <style>
            .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
                background-color: #fff;
            }

            .module {
                display: block;
                font-family: Roboto Condensed Regular !important;
                overflow: hidden;
                margin-top: 10px;
            }

                .module .content {
                    height: 300px;
                    overflow-y: scroll;
                    margin-bottom: 30px;
                }

            #systemModal {
                margin-top: 70px !important;
            }

            .module h3 {
                width: 100%;
                text-align: center;
                font-weight: bold;
                background: #222222;
                color: white;
                height: 35px;
                padding-top: 8px;
                font-size: 17px;
                font-family: sans-serif;
            }

            .module .table-responsive thead tr th {
                text-align: center;
            }

            .clear {
                clear: both;
            }

            .modal-backdrop.in {
                opacity: 0.9 !important;
            }

            .customer-listing table.table-listing > tbody > tr.active {
                background: #fff !important;
            }

            .customer-add .table-add td span, .customer-add .table-add td input[type="text"], .customer-add .table-add td select {
                height: auto !important;
                line-height: 20px !important;
                width: 100%;
                float: left;
            }

            .table-listing .uv-avatar {
                width: 120px;
            }

            .mAction .center {
                margin: auto;
                padding: 5px;
                font-size: 12px;
            }

            .fb-cover-error {
                top: 100px !important;
                right: 10px !important;
                position: fixed !important;
                width: 20% !important;
                z-index: 2000;
            }

            .mID {
                text-align: center;
            }

            .mName {
                padding: 13px !important;
            }

            .mLink {
                padding: 13px !important;
            }

            .mAction {
                text-align: center;
            }

            .customer-listing table.table-listing th, .customer-listing table.table-listing td {
                padding: 7px 0px !important;
            }
        </style>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Phân quyền &nbsp;&#187; </li>
                        <li class="li-listing active"><a href="/admin/phan-quyen/danh-sach-nhom-quyen.html">Danh sách phân quyền</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing be-report main-class">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <div class="row">
                    <strong class="st-head left"><i class="fa fa-file-text"></i>Danh sách Menu</strong>
                </div>

                <div class="row table-wp">
                    <% foreach (var item in menu)
                        {
                    %>
                    <div class="col-xs-6 module" mid="<%=item.menuID %>">
                        <h3><%=item.menuName %></h3>
                        <div class="content">
                            <table class="table table-bordered table-responsive" id="permissionAction" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Menu Name</th>
                                        <th>Link</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <% foreach (var child in item.lstMenu)
                                        {
                                    %>
                                    <tr>
                                        <td class="mID"><%=child.childId %></td>
                                        <td class="mName"><%=child.childName %></td>
                                        <td class="mLink"><%=child.childLink %></td>
                                        <td class="mAction">
                                            <button type="button" class="btn btn-success center details" menuid="<%=child.childId %>" menuname="<%=child.childName %>" menulink="<%=child.childLink %>">Set action</button>
                                            <button type="button" class="btn btn-success center setApi" id="<%=child.childId %>" menuname="<%=child.childName %>" menulink="<%=child.childLink %>">Set Api</button>
                                        </td>
                                    </tr>
                                    <%
                                        } %>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <%
                        } %>
                    <div class="col-xs-6 module out-module">
                        <h3>Menu khác / Menu cha
                        </h3>
                        <div class="content">
                            <table class="table table-bordered table-responsive" id="tableMenu" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Menu Name</th>
                                        <th>Link</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="row show-result"></div>

                <!-- END Hidden Field-->
            </div>
            <%-- END Listing --%>
        </div>
        <div class="loading-frame" style="display: none;">
        </div>
        <div id="systemModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">

                    <div style="margin-top: 20px;">
                        <div class="row" style="margin-left: 20px; font-weight: bold; font-size: 18px;">
                            Set Action
                        </div>
                        <hr />
                        <div class="row">
                            <div class="col-md-2" style="margin-top: 5px;">ID/Menu name:</div>
                            <div class="col-md-5">
                                <input class="form-control" id="ID" disabled="disabled" />
                            </div>
                            <div class="col-md-5">
                                <input class="form-control" id="Name" disabled="disabled" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2" style="margin-top: 5px;">Link URL:</div>
                            <div class="col-md-10">
                                <input class="form-control" id="Link" disabled="disabled" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2" style="margin-top: 5px;">Action:</div>
                            <div class="col-md-10">
                                <span class="field-wp" id="pem">
                                    <asp:ListBox SelectionMode="Multiple" ID="ddlPermissionAction" runat="server" ClientIDMode="Static" CssClass="select perm-ddl-field"></asp:ListBox>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="button" id="btnAdd" onclick="update();" class="btn btn-success" value="Cập nhật" />
                        <button type="button" class="btn btn-danger" id="closeModal">Đóng</button>
                    </div>
                </div>
            </div>
        </div>
        <div id="apiModal" class="modal fade" role="dialog">
            <div class="modal-api-dialog">
                <!-- Modal content-->
                <div class="modal-content">

                    <div style="margin-top: 20px;">
                        <div class="row" style="margin-left: 20px; font-weight: bold; font-size: 18px;">
                            Set Action map Api
                        </div>
                        <hr />
                        <%-- <div class="row">
                            <div class="col-md-2" style="margin-top: 5px;">ID/Menu name:</div>
                            <div class="col-md-5">
                                <input class="form-control" id="IDPage" disabled="disabled" />
                            </div>
                            <div class="col-md-5">
                                <input class="form-control" id="NamePage" disabled="disabled" />
                            </div>
                        </div>--%>
                        <%--<div class="row">
                            
                            
                        </div>--%>
                        <div class="row">
                            <table>
                                <tr class="tr-field-ahalf tr-product">
                                    <td class="col-xs-10 right">
                                        <div class="col-md-10" style="margin-top: 5px;">Link URL:</div>
                                    </td>
                                    <td class="col-xs-10 right">
                                        <div class="col-md-10">
                                <input class="form-control" id="LinkPage" disabled="disabled" style="margin: 12px 0; width: 190px;" />
                            </div>
                                    </td>
                                </tr>
                                <tr class="tr-field-ahalf tr-product">
                                    <td class="col-xs-10 right">
                                        <div class="col-md-2" style="margin-top: 5px;">Action:</div>
                                    </td>
                                    <td class="col-xs-10 right">
                                        <div class="col-md-10">
                                            <span class="field-wp" id="pemApi">
                                                <asp:DropDownList ToolTip="Action" ID="actionList" CssClass="select form-control" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 190px;"></asp:DropDownList>
                                            </span>
                                        </div>
                                    </td>
                                    <td class="col-xs-10 right">
                                        <div class="col-md-2" style="margin-top: 5px;">Api:</div>
                                    </td>
                                    <td class="col-xs-10 right">
                                        <div class="col-md-10">
                                            <asp:DropDownList ToolTip="Api" ID="apiList" CssClass="select form-control" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 190px;"></asp:DropDownList>
                                        </div>
                                    </td>
                                    <td class="col-xs-10 right">
                                        <input type="button" id="btnAddApi" onclick="addApi();" class="btn btn-success" value="Thêm mới" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <asp:HiddenField ID="IDPage" ClientIDMode="Static" runat="server" />
                    <asp:HiddenField ID="Hidden_PermissionId" ClientIDMode="Static" runat="server" />
                    <hr />
                    <div class="row" style="margin-left: 20px; font-weight: bold; font-size: 18px;">
                        Danh sách hiện tại
                    </div>
                    <div class="row" style="margin-left: 20px; font-size: 18px;">
                        <table id="table-map-detail" class="table table-bordered" style="width: 800px;">
                            <thead>
                                <tr>
                                    <th style="width: 20px;">STT</th>
                                    <th style="width: 50px;" class="maSPcol">Action</th>
                                    <th style="width: 100px;">Api path</th>
                                    <th style="width: 50px;">Method</th>
                                    <th style="width: 50px;"></th>
                                </tr>
                            </thead>
                            <tbody id="tbodyDetail">
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" id="closeApiModal">Đóng</button>
                    </div>
                </div>
            </div>
        </div>
        <style>
            #systemModal {
                margin-top: 110px
            }

            .modal-content .row {
                margin-bottom: 20px;
            }

            #pem {
                width: 100%;
            }

                #pem span {
                    width: 100%;
                    line-height: 22px;
                    height: 22px;
                }

                    #pem span div.btn-group .multiselect, #pem span div.btn-group, .multiselect-container {
                        width: 100% !important;
                    }

            ul.multiselect-container.dropdown-menu {
                max-height: 300px;
                overflow-y: scroll;
                border-radius: 2px
            }

            .modal-dialog {
                margin-top: 110px;
            }

            .modal {
                z-index: 9998;
            }

            .be-report .row-filter {
                z-index: 0;
            }

            .modal-api-dialog {
                width: auto !important;
                margin: auto;
            }


            @media(min-width: 900px) {
                .modal-content, .modal-dialog {
                    width: 900px !important;
                    margin: auto;
                }
            }

            .elm {
                width: 20px;
                height: 20px;
                float: left;
                display: block;
            }

                .elm.del-btn {
                    background: url(/Assets/images/icon.delete.small.01.png) center no-repeat !important;
                }

                    .elm.del-btn:hover,
                    .elm.del-btn.active {
                        background: url(/Assets/images/icon.delete.small.png) center no-repeat !important;
                    }
        </style>
        <script src="/Assets/js/Permission.js"></script>
        <link href="/Assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" />
        <script src="/Assets/js/jquery.mCustomScrollbar.js"></script>
        <script src="/Assets/js/common.js"></script>
        <link rel="stylesheet" href="/Assets/izitoast/css/iziToast.min.css" />
        <script src="/Assets/izitoast/js/iziToast.min.js" type="text/javascript"></script>
        <script type="text/ecmascript">
            jQuery(document).ready(function () {
                // call ham
                GetListPermission();
            });

            $("Hidden_Permission<%--Id").val("<%=HttpContext.Current.Session["Code"]%>");
            debugger
            var tmp = $("Hidden_P--%>ermissionId").val();
            var menuId;
            var permissionId;
            //open modal
            $(document).on("click", ".details", function () {
                $("#systemModal").modal();
                // gán staffId
                menuId = $(this).attr("menuId");
                permissionId = <%=HttpContext.Current.Session["Code"]%>;
                if (menuId > 0) {
                    //remove checked
                    $(".multiselect-native-select").find("input[value='multiselect-all']").prop("checked", false);
                    $(".multiselect-native-select").find("input[value='multiselect-all']").parent().parent().parent().removeClass('active');
                    for (var i = 0; i < jsonListPermAction.length; i++) {
                        $(".multiselect-native-select").find("input[value='" + jsonListPermAction[i].Id + "']").prop("checked", false);
                        $(".multiselect-native-select").find("input[value='" + jsonListPermAction[i].Id + "']").parent().parent().parent().removeClass('active');
                    }
                    //call
                    bindMenu(menuId, $(this).attr("menuName"), $(this).attr("menuLink"));
                    bindPermissonAction(menuId, permissionId);
                }
            });

            //close modal
            $(document).on("click", "#closeModal", function () {
                $("#systemModal").modal("hide");
            });

            //-------------------------------------------//
            //open api modal
            $(document).on("click", ".setApi", function () {
                $("#apiModal").modal();
                // gán staffId
                debugger
                menuId = $(this).attr("Id");
                permissionId = <%=HttpContext.Current.Session["Code"]%>;
                if (menuId > 0) {
                    //call
                    bindMenuApi(menuId, $(this).attr("menuName"), $(this).attr("menuLink"), <%=HttpContext.Current.Session["Code"]%>);
                    GetListMapApi(menuId,permissionId);
                    //bindPermissonAction(menuId, permissionId);
                    $("#actionList").val("0").change();
                    $("#apiList").val("0").change();
                }
            });

            //close api modal
            $(document).on("click", "#closeApiModal", function () {
                $("#apiModal").modal("hide");
            });

            jQuery(document).ready(function () {
                $(".loading-frame").show();
                $.ajax({
                    type: "POST",
                    contentType: "application/json;charset:UTF-8",
                    datatype: "JSON",

                    url: "/GUI/BackEnd/Permission/Edit_Roles_V4.aspx/GetChildMenuWithoutModule",
                    success: function (data) {
                        dataJSON = JSON.parse(data.d);
                        //debugger
                        for (var i = 0; i < dataJSON.length; i++) {
                            var content = "<tr>";
                            content += "<td class='mID'>" + dataJSON[i].childId + "</td>";
                            content += "<td class='mName'>" + dataJSON[i].childName + "</td>";
                            content += "<td class='mUrl'>" + dataJSON[i].childLink + "</td>";
                            content += "<td class='mAction'><button type='button' class='btn btn-success center details' menuid=" + dataJSON[i].childId + ">Set action</button> ";
                            content += "<button type='button' class='btn btn-success center setApi' id="+ dataJSON[i].childId +" menuname=" + dataJSON[i].childName +" menulink="+ dataJSON[i].childLink + ">Set Api</button></td>";
                            content += "</tr>";
                            $(".out-module table tbody").append($(content));
                        }
                    },
                    complete: function () {
                        $(".loading-frame").hide();
                    }
                })
                // Add active menu
                $("#glbTuyendungV2").addClass("active");
                $("#glbReportConfirm").addClass("active");
            })
        </script>
    </asp:Panel>
</asp:Content>
