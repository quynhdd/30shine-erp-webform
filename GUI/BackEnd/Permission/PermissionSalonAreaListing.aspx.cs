﻿using _30shine.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Data.Entity.Migrations;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.Permission
{
    public partial class PermissionSalonAreaListing : System.Web.UI.Page
    {
        protected bool Perm_Access = false;
        protected Paging PAGING = new Paging();
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                getData();
            }
        }
        protected void _BtnClick(object sender, EventArgs e)
        {
            try
            {
                getData();
            }
            catch (Exception ex)
            {
                UIHelpers.TriggerJsMsgSystem_v1(this,ex.Message.Replace("'",""),"warning",15000);
            }
            RemoveLoading();
        }
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        /// <summary>
        /// get data 
        /// author: QuynhDD
        /// </summary>
        private void getData()
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var list = (from a in db.PermissionSalonAreas
                                join b in db.Staffs on a.StaffId equals b.Id
                                join c in db.Staff_Type on b.Type equals c.Id
                                where a.IsDelete == false
                                && b.IsDelete == 0 && b.Active == 1
                                && c.IsDelete == 0
                                select new
                                {
                                    a.StaffId,
                                    b.Fullname,
                                    c.Name
                                } into d
                                group d by new
                                {
                                    d.StaffId,
                                    d.Fullname,
                                    d.Name
                                } into g
                                select new
                                {
                                    StaffId = g.Key.StaffId,
                                    Fullname = g.Key.Fullname,
                                    Name = g.Key.Name
                                }).ToList();

                    if (list.Count > 0)
                    {
                        Bind_Paging(list.Count);
                        RptPermissionSalonArea.DataSource = list.Skip(PAGING._Offset).Take(PAGING._Segment).OrderByDescending(o => o.StaffId);
                        RptPermissionSalonArea.DataBind();
                    }
                }
            }
            catch
            {
                RptPermissionSalonArea.DataSource = "";
                RptPermissionSalonArea.DataBind();
            }
        }

        /// <summary>
        /// Delete PermissonSalonArea
        /// Author: QuynhDD
        /// </summary>
        [WebMethod]
        public static object Delete(int StaffId)
        {
            try
            {
                var message = new Library.Class.cls_message();
                var exc = 0;
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                using (var db = new Solution_30shineEntities())
                {
                    var list = (from c in db.PermissionSalonAreas
                                where c.StaffId == StaffId && c.IsDelete == false
                                select c
                                ).ToList();
                    if (list.Count > 0)
                    {
                        foreach (var item in list)
                        {
                            item.IsDelete = true;
                            item.ModifiedTime = DateTime.Now;
                            db.PermissionSalonAreas.AddOrUpdate(item);
                        }
                        exc = db.SaveChanges();
                    }

                    if (exc > 0)
                        message.success = true;
                    else
                        message.success = false;
                }

                return message;
            }

            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Get list salonId
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public static object GetListSalonId(int staffId)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
          
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                   var data = (from c in db.PermissionSalonAreas
                            join d in db.Tbl_Salon on c.SalonId equals d.Id
                            where c.IsDelete == false
                              && c.StaffId == staffId
                            select new 
                            {
                                SalonId = c.SalonId,
                                Name = d.Name
                            }).ToList();

                    return serializer.Serialize(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        /// <summary>
        /// BindPaging
        /// </summary>
        /// <param name="totalRecord"></param>
        protected void Bind_Paging(int totalRecord)
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(totalRecord) : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        /// <summary>
        /// Get totalpage
        /// </summary>
        /// <param name="totalRecord"></param>
        /// <returns></returns>
        protected int Get_TotalPage(int totalRecord)
        {
            int TotalRow = totalRecord - PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        }

    }
}