﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/TemplateMaster/SiteMaster.Master" CodeBehind="Create_Url.aspx.cs" Inherits="_30shine.GUI.BackEnd.Permission.Create_Url" %>


<asp:Content ID="ProductAdd" ContentPlaceHolderID="CtMain" runat="server">
    <style>
        .disabled {
            pointer-events: none;
            cursor: default;
        }
    </style>
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <link href="/Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="/Assets/js/select2/select2.min.js"></script>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý menu&nbsp;&#187; </li>
                        <li class="li-listing"><a href="/admin/phan-quyen/danh-sach-menu.html">Danh sách đường dẫn</a></li>

                        <li class="li-add active"><a href="/admin/phan-quyen/them-moi-menu.html" class="disabled">Thêm mới</a></li>

                    </ul>
                </div>
            </div>
        </div>
        <div class="wp customer-add">
            <%-- Add --%>
            <div class="wp960 content-wp">
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <div class="table-wp">
                    <table class="table-add admin-product-table-add">
                        <tbody>
                            <tr class="title-head">
                                <td><strong>Thông tin đường dẫn</strong></td>
                                <td></td>
                            </tr>
                            <tr id="_menu">
                                <td class="col-xs-2 left"><span>Chọn Menu Cha</span></td>
                                <td class="col-xs-9 right">
                                    <asp:DropDownList runat="server" ID="ddlMenuName" ClientIDMode="Static"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-2 left"><span>Tên Menu<i style="color: red">&nbsp*</i></span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="Name" Style="width: 675px" runat="server" ClientIDMode="Static"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="NameValidate" ControlToValidate="Name" runat="server" CssClass="fb-cover-error" Text="<i style='color:red'>Bạn chưa nhập tên menu!</i>" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-2 left"><span>Link liên kết<i style="color: red">&nbsp*</i></span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:DropDownList ID="ddlURL" runat="server" AutoPostBack="true" ClientIDMode="Static" OnSelectedIndexChanged="ddlURL_SelectedIndexChanged"></asp:DropDownList>
                                        <asp:RequiredFieldValidator InitialValue="0" ID="RequiredFieldValidator3" ControlToValidate="ddlURL" runat="server" CssClass="fb-cover-error" Text="<i style='color:red'>Bạn chưa chọn link liên kết!</i>"></asp:RequiredFieldValidator>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-2 left"><span>Đường dẫn vật lý</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:DropDownList runat="server" ID="ddlMainUrl" ClientIDMode="Static"></asp:DropDownList>
                           <%--             <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="ddlMainUrl" runat="server" CssClass="fb-cover-error" Text="<i style='color:red'>Bạn chưa chọn đường dẫn vật lý!</i>"></asp:RequiredFieldValidator>--%>
                                    </span>
                                </td>
                            </tr>
                        <tr class="tr-product-category">
                            <td class="col-xs-2 left"><span> Ẩn - Hiện trên menu</span></td>
                            <td class="col-xs-9 right">
                                <span class="field-wp">
                                    <asp:CheckBox ID="ShowHideMenu"  runat="server" />
                                </span>
                            </td>
                        </tr>
                            <tr class="tr-product-category">
                                <td class="col-xs-2 left"><span>Ẩn/Hiện</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:CheckBox ID="Publish" Checked="true" runat="server" />
                                    </span>
                                </td>
                            </tr>
                            <tr class="tr-send">
                                <td class="col-xs-2 left"></td>
                                <td class="col-xs-9 right no-border">
                                    <span class="field-wp">
                                        <asp:Button ID="Send" CssClass="btn-send" runat="server" Text="Hoàn tất" ClientIDMode="Static" OnClick="ExcAddOrUpdate"></asp:Button>
                                    </span>
                                </td>
                            </tr>
                            <asp:HiddenField runat="server" ID="UrlKeyField" ClientIDMode="Static" />
                        </tbody>
                    </table>
                </div>
            </div>
            <%--End Add --%>
        </div>
        <script>
            $("#glbTuyendungV2").addClass("active");
            $('select').select2();
            $("#ddlMainUrl").attr("disabled", "disabled");
        </script>
    </asp:Panel>
</asp:Content>
