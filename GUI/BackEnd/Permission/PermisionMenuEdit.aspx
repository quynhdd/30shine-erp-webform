﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/TemplateMaster/SiteMaster.Master" CodeBehind="PermisionMenuEdit.aspx.cs" Inherits="_30shine.GUI.BackEnd.Permission.PermisionMenuEdit" %>

<asp:Content ID="ProductEdit" ContentPlaceHolderID="CtMain" runat="server">
    <style>
        .disabled {
            pointer-events: none;
            cursor: default;
        }
    </style>
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <link href="/Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="/Assets/js/select2/select2.min.js"></script>

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý menu&nbsp;&#187; </li>
                        <li class="li-listing"><a href="/admin/phan-quyen/danh-sach-menu-v1.html">Danh sách menu</a></li>
                        <li class="li-add active"><a href="/admin/phan-quyen/them-moi-menu-v1.html"">Thêm mới</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="wp customer-add">
            <%-- Add --%>
            <div class="wp960 content-wp">
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <div class="table-wp">
                    <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
                    <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                        <ContentTemplate>
                            <table class="table-add admin-product-table-add">
                                <tbody>
                                    <tr class="title-head">
                                        <td><strong>Thông tin menu</strong></td>
                                        <td></td>
                                    </tr>
                                    <tr id="_menu">
                                        <td class="col-xs-3 left"><span>Chọn Menu Cha</span></td>
                                        <td class="col-xs-9 right">
                                            <asp:TextBox ID="txtId" runat="server" ClientIDMode="Static"></asp:TextBox>
                                            <asp:DropDownList runat="server" ID="ddlParentMenu" ClientIDMode="Static" CssClass="form-control"></asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-xs-3 left"><span>Nhập tên Menu<i style="color: red">&nbsp*</i></span></td>
                                        <td class="col-xs-9 right">
                                            <span class="field-wp">
                                                <asp:TextBox ID="txtName" runat="server" CssClass="form-control" ClientIDMode="Static"></asp:TextBox>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-xs-3 left"><span>Nhập liên kết<i style="color: red">&nbsp*</i></span></td>
                                        <td class="col-xs-9 right">
                                            <span class="field-wp">
                                                <asp:TextBox ID="txtLink" runat="server" CssClass="form-control" ClientIDMode="Static"></asp:TextBox>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-xs-3 left"><span>Nhập đường dẫn</span></td>
                                        <td class="col-xs-9 right">
                                            <span class="field-wp">
                                                <asp:TextBox ID="txtPath" runat="server" CssClass="form-control" ClientIDMode="Static"></asp:TextBox>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-xs-3 left"><span>Mã trang web (PageId)</span></td>
                                        <td class="col-xs-9 right">
                                            <span class="field-wp">
                                                <asp:TextBox ID="txtPageId" CssClass="form-control" runat="server" ClientIDMode="Static"></asp:TextBox>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr class="tr-product-category">
                                        <td class="col-xs-3 left"><span>Ẩn/Hiện</span></td>
                                        <td class="col-xs-9 right">
                                            <span class="field-wp">
                                                <asp:CheckBox ID="Publish" Checked="true" runat="server" />
                                            </span>
                                        </td>
                                    </tr>
                                    <tr class="tr-send">
                                        <td class="col-xs-3 left"></td>
                                        <td class="col-xs-9 right no-border">
                                            <span class="field-wp">
                                                <asp:Button ID="Send" OnClientClick="checkInput()" CssClass="btn-send" runat="server" Text="Hoàn tất" ClientIDMode="Static" OnClick="EditMenu"></asp:Button>
                                            </span>
                                        </td>
                                    </tr>
                                    <asp:HiddenField runat="server" ID="UrlKeyField" ClientIDMode="Static" />
                                </tbody>
                            </table>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="Send" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
            <%--End Add --%>
        </div>
        <script>
            //$("#glbTuyendungV2").addClass("active");
            $('select').select2();
            //$("#ddlMainUrl").attr("disabled", "disabled");
            $(document).ready(function () {
                $("#txtLink").bind("change", function () {
                    autoFillPageId();
                })
                $("#txtLink").bind("click", function () {
                    $('#txtLink').css("border-color", "");
                })
                $("#txtPath").bind("click", function () {
                    $('#txtPath').css("border-color", "");
                })
                $("#txtPageId").bind("click", function () {
                    $('#txtPageId').css("border-color", "");
                })
            });
            function checkInput() {
                if ($('#txtLink').val() == "" || $('#txtPath').val() == "") {
                    $('#txtLink').val() == "" ? $('#txtLink').css("border-color", "red") : "";
                    $('#txtPath').val() == "" ? $('#txtPath').css("border-color", "red") : "";
                    $('#txtPageId').val() == "" ? (autoFillPageId()) : "";
                    event.preventDefault();
                    return false;
                }
                else {
                    autoFillPageId();
                    return true;
                }
            }
            function autoFillPageId() {
                if ($('#txtLink').val() != "") {
                    var link = $('#txtLink').val();
                    var regex = /\(\[([^\[]*)\]\+\)/;
                    var pageId = link.replace(regex, 'edit');
                    pageId = pageId == 'javascript:void(0)' ? "" : pageId;
                    $('#txtPageId').val(pageId);
                    return true;
                }
                else {
                    $('#txtLink').val() == "" ? $('#txtLink').css("border-color", "red") : "";
                    $('#txtPath').val() == "" ? $('#txtPath').css("border-color", "red") : "";
                    event.preventDefault();
                }
            }
            function ShowWarning(message) {
                $('#MsgSystem').text(message);
                $('#MsgSystem').css("color", "red");
            }
        </script>
    </asp:Panel>
</asp:Content>

