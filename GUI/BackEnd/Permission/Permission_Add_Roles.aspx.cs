﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Web.Services;
using System.Web.Script.Serialization;
using _30shine.Helpers;

namespace _30shine.GUI.BackEnd.Permission
{
    public partial class Permission_Add_Roles : System.Web.UI.Page
    {
        protected bool Perm_Access = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            checkPermission();
            if (!IsPostBack)
            {
                getPermission();
                getModules();
            }
        }
        /// <summary>
        /// check Permission
        /// </summary>
        public void checkPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                string perName = Session["User_Permission"].ToString();
                string url = Request.Url.AbsolutePath.ToString();
                Solution_30shineEntities db = new Solution_30shineEntities();
                var pem = (from m in db.Tbl_Permission_Map
                           join mn in db.Tbl_Permission_Menu on m.mID equals mn.mID
                           where m.pName == perName && mn.Url_Main.EndsWith(url) || mn.Url_Rewrite.EndsWith(url)
                           select new { m.aID }
                           ).FirstOrDefault();
                if (pem != null)
                {
                    var pemArray = pem.aID.Split(',');
                    if (Array.IndexOf(pemArray, "1") > -1)
                    {
                        Perm_Access = true;
                        //if (Array.IndexOf(pemArray, "2") > -1)
                        //{
                        //    Perm_ViewAllData = true;
                        //}
                        //if (Array.IndexOf(pemArray, "3") > -1)
                        //{
                        //    Perm_Add = true;
                        //}
                        //if (Array.IndexOf(pemArray, "4") > -1)
                        //{
                        //    Perm_Edit = true;
                        //}
                        //if (Array.IndexOf(pemArray, "5") > -1)
                        //{
                        //    Perm_Delete = true;
                        //}
                    }
                    else
                    {
                        ContentWrap.Visible = false;
                        NotAllowAccess.Visible = true;

                    }

                }


            }
        }
        protected void getPermission()
        {
            Solution_30shineEntities db = new Solution_30shineEntities();
            var p = db.Tbl_Permission.ToList();
            var item = new Tbl_Permission();
            item.pName = "Chọn Quyền";
            item.pID = 99;
            p.Insert(0, item);
            ddlPermission.DataTextField = "pName";
            ddlPermission.DataValueField = "pID";
            ddlPermission.SelectedIndex = 99;

            ddlPermission.DataSource = p;
            ddlPermission.DataBind();
        }
        protected void getModules()
        {
            Solution_30shineEntities db = new Solution_30shineEntities();
            var menu = db.Tbl_Permission_Menu.Where(m=>m.mParentID==0).ToList();
            var item = new Tbl_Permission_Menu();
            item.mName = "Chọn Modules";
            item.mID = 99;
            menu.Insert(0, item);
            ddlModule.DataTextField = "mName";
            ddlModule.DataValueField = "mID";
            ddlModule.SelectedIndex = 99;

            ddlModule.DataSource = menu;
            ddlModule.DataBind();
        }

        [WebMethod]

        public static object getChildMenu(int mID)
        {
            Solution_30shineEntities db = new Solution_30shineEntities();
            var listMenu = db.Store_Permission_Get_Menu(mID,1).ToList();
            var js = new JavaScriptSerializer();
            return js.Serialize(listMenu);
        }
        [WebMethod]
        public static object getAction()
        {
            Solution_30shineEntities db = new Solution_30shineEntities();

            var action = db.Tbl_Permission_Action.ToList();
            return new JavaScriptSerializer().Serialize(action);
        }
        [WebMethod]
        public static void addMenu(List<cls_Add_Menu> list)
        {
            try
            {
                Solution_30shineEntities db = new Solution_30shineEntities();

                for (int i = 0; i < list.Count; i++)
                {
                    Tbl_Permission_Map map = new Tbl_Permission_Map();
                    map.pID = list[i].p_Id;
                    map.pName = list[i].p_Name;
                    map.mID = list[i].m_Id;
                    map.mName = list[i].m_Name;
                    map.aID = list[i].action;
                    map.mapPublish = true;
                    db.Tbl_Permission_Map.Add(map);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
            

            
        }
        public class cls_Add_Menu:Tbl_Permission_Map
        {
            public int p_Id { get; set; }
            public string p_Name { get; set; }
            public int m_Id { get; set; }
            public string m_Name { get; set; }
            public string action { get; set; }
            public bool mPublish { get; set; }
        }
    }
}