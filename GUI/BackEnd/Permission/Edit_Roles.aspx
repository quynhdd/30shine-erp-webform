﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/TemplateMaster/SiteMaster.Master" CodeBehind="Edit_Roles.aspx.cs" Inherits="_30shine.GUI.BackEnd.Permission.Edit_Roles" %>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">


        <style>
            .module{
                display:block;
                
                font-family: Roboto Condensed Regular !important;
               overflow:hidden;

            }
            .module .content{
                height:300px;
                overflow-y:scroll;
                
                margin-bottom:30px;
            }
            .module h3 {
                width: 100%;
                text-align: center;
                padding: 5px 0px;
                font-weight: bold;
                background: #222222;
                color:white;

            }
            .clear{
                clear:both;
            }
            .customer-listing table.table-listing > tbody > tr.active {
    background: #fff !important;
}
            .customer-add .table-add td span, .customer-add .table-add td input[type="text"], .customer-add .table-add td select {
                 height: auto !important; 
                 line-height: 20px !important; 
                width: 100%;
                float: left;
            }
            .table-listing .uv-avatar { width: 120px; }
            .fb-cover-error{
                 top:100px !important;
                right:10px !important;
                position:fixed !important;
                width:20% !important;
                z-index:2000;
            }
            #txtSearch{
                /*width:220px !important;*/
                margin:0 auto;

                 
                background:url("/Assets/images/search.png");
                background-color:#ddd; 
                background-position: 10px 7px; /* Position the search icon */
                background-repeat: no-repeat; /* Do not repeat the icon image */
                width: 99%; /* Full-width */
                /*float:right;*/
                font-size: 13px !important; /* Increase font-size */
                padding: 12px 20px 12px 40px; /* Add some padding */
                border: 1px solid #ddd; /* Add a grey border */
                /*margin-bottom: 12px;*/ /* Add some space below the input */
                -moz-border-radius:5px;
                -webkit-border-radius:5px;
                border-radius:5px;
                left:0;
                bottom:0;
                position:fixed;
                z-index:9999;
                

            }
          #btn-Edit{
            padding:6px 20px;
            display:block;
            background:#000000;
            color:white;
            border:none;
            width:auto;
            line-height:24px;
      
            margin:0 auto;
        }

         #btn-Edit:hover{
             color:yellow;

        }
         .customer-listing table.table-listing th, .customer-listing table.table-listing td{
             padding:7px 0px !important;
         }
        </style>

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Phân quyền &nbsp;&#187; </li>
                        <li class="li-listing active"><a href="/admin/phan-quyen/danh-sach-phan-quyen.html">Danh sách phân quyền</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing be-report be-report-timekeeping">
            <%-- Listing --%>
            <div class="wp960 content-wp">
       
               <div class="row">
                    <input type="checkbox" id="checkallpage" onchange="checkAllpage(this)"/> Chọn tất cả
                    <asp:DropDownList runat="server" ClientIDMode="Static" ID="ddlPermission" Enabled="false" style="display:none"></asp:DropDownList>
                </div>
                <div class="row">
                    <strong class="st-head left"><i class="fa fa-file-text"></i>Danh sách Menu</strong><input type="text" name="txtSearch" class="right" id="txtSearch"  placeholder="Tìm kiếm theo đường dẫn" onkeyup="SearchMenu()"/>
                </div>
             
                        <div class="row table-wp">
                            <% foreach (var item in menu)
                                {
                                    %>
                            <div class="col-xs-6 module <%=item.mClassTag %>" mID ="<%=item.mID %>">
                               <h3><%=item.mName %></h3>
                               <div class="content">
                                   <table class="table table-bordered table-responsive" style="width:100%">
                                       <thead>
                                           <tr>
                                           <th>ID</th>
                                           <th>Menu Name</th>
                                           <th>Link</th>
                                           <th>Action</th>
                                           <th><input type="checkbox" class="checkall" onchange="checkAll(this)" name="checkall[]"/></th>
                                               </tr>
                                       </thead>
                                       <tbody>
                                           <% foreach (var child in item.lstMenu) {
                                           %>
                                           <tr>
                                               <td class="mID"><%=child.id %></td>
                                               <td class="mName"><%=child.name %></td>
                                               <td><%=child.link %></td>
                                               <td class="mAction"><select name="slAction" class="slAction" multiple="multiple">
                                                    <%foreach (var a in child.clsAction) {
                                                            %>
                                                   <option value="<%=a.aID %>" title ="<%=a.aDescription %>""
                                                       <%if (a.aPUBLISH == 1) { Response.Write("selected='selected'"); }
                                                       else {Response.Write(""); }
                                                          %>
                                                       ><%=a.aName %></option>
                                                        <% } %>
                                                   </select></td>
                                               <td class="action">
                                                   <%if (child.isPublish == 1) {
                                                           Response.Write("<input type='checkbox' class='checkbox' checked='checked'/>");
                                                       }
                                                       else
                                                       {
                                                            Response.Write("<input type='checkbox' class='checkbox'/>");
                                                       }

                                                      %>
                                               </td>
                                           </tr>
                                           <%
                                               } %>
                                       </tbody>
                                   </table>
                               </div>
                           </div>
                            <%
                                } %>
                            <div class="col-xs-6 module out-module">
                               <h3>Menu khác</h3>
                                <div class="content">
                                    <table class="table table-bordered table-responsive" id="tableMenu" style="width:100%">
                                       <thead>
                                           <tr>
                                           <th>ID</th>
                                           <th>Menu Name</th>
                                           <th>Link</th>
                                           <th>Action</th>
                                           <th><input type="checkbox" class="checkall" onchange="checkAll(this)" name="checkall[]"/></th>
                                               </tr>
                                       </thead>
                                       <tbody>
                                        </tbody>
                                        </table>
                                </div>
                           </div>
                            <div class="clear"></div>
                        </div>
                        <div class="row show-result"></div>
                      <div class="row" style="width:100%;margin-top:50px;">
                          <button type="button" id="btn-Edit" onclick="AddOrUpdate()">Xác nhận</button>
                      </div>
            

         
                <!-- END Hidden Field-->
            </div>
            <%-- END Listing --%>
        </div>
        <div class="loading-frame" style="display:none;">
    
        </div>
        <link href="/Assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" />
        <script src="/Assets/js/jquery.mCustomScrollbar.js"></script>
        <script type="text/ecmascript">


            jQuery(document).ready(function () {
                $(".slAction").multiselect({
                    includeSelectAllOption: true
                })
                $(".loading-frame").show();
                $.ajax({
                    type: "POST",
                    contentType: "application/json;charset:UTF-8",
                    datatype: "JSON",
                    
                    url: "/GUI/BackEnd/Permission/Edit_Roles.aspx/GetChildMenuWithoutModule",
                    data: "{c:<%=Convert.ToInt32(Request.QueryString["Code"])%>}",
                    success: function (data) {
                        //console.log(data.d);
                        dataJSON= JSON.parse(data.d);
                        //console.log(dataJSON);
                        for (var i = 0; i < dataJSON.length; i++) {
                            var content = "<tr>";
                            content +="<td class='mID'>"+dataJSON[i].id+"</td>";
                            content +="<td class='mName'>"+dataJSON[i].name+"</td>";
                            content +="<td class='mUrl'>"+dataJSON[i].link+"</td>";
                            content +="<td class='mAction'><select name='slAction' class='slAction' multiple='multiple'>";
                            for (var j = 0; j < dataJSON[i].clsAction.length; j++) {
                                content += "<option value='"+dataJSON[i].clsAction[j].aID+"'";
                                if (dataJSON[i].clsAction[j].aPUBLISH == 1) {
                                    content += "selected='selected'";
                                }
                                content += ">"+dataJSON[i].clsAction[j].aName+"</option>";
                            }
                            content +="</select></td>";
                            content +="<td class='action'><input type='checkbox' class='checkbox' name='checkbox'";
                            if (dataJSON[i].isPublish == 1) {
                                content += "checked='checked'";
                            }
       
                            content +="></td>";
                            content +="</tr>";

                            $(".out-module table tbody").append($(content));
                            $(".slAction").multiselect({
                                includeSelectAllOption: true
                            })
                        }
                    },
                    complete:function(){
                        $(".loading-frame").hide();
                    }
                })
                // Add active menu
                $("#glbTuyendungV2").addClass("active");
                $("#glbReportConfirm").addClass("active");
                
            })
        </script>
        <script>
            function SearchMenu() {
              // Declare variables 
              var input, filter, table, tr, td, i;
              input = document.getElementById("txtSearch");
              filter = input.value.toUpperCase();
              table = document.getElementById("tableMenu");
              tr = table.getElementsByTagName("tr");

              // Loop through all table rows, and hide those who don't match the search query
              for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[2];
                if (td) {
                  if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                  } else {
                    tr[i].style.display = "none";
                  }
                } 
              }
            }
            function checkAll(ele) {
                //debugger;
                var checkboxes = $(ele).parent().closest("table").find(".action").find(".checkbox");
                //console.log(checkboxes.length);
                if (ele.checked) {
                    for (var i = 0; i < checkboxes.length; i++) {
                        if (checkboxes[i].type == 'checkbox') {
                            checkboxes[i].checked = true;
                        }
                    }
                } else {
                    for (var i = 0; i < checkboxes.length; i++) {
                        //console.log(i)
                        if (checkboxes[i].type == 'checkbox') {
                            checkboxes[i].checked = false;
                        }
                    }
                }
            }
            function checkAllpage(ele) {
                //debugger;
                var checkboxes = $(document).find("input[type='checkbox']");
                //console.log(checkboxes.length);
                if (ele.checked) {
                    for (var i = 0; i < checkboxes.length; i++) {
                        if (checkboxes[i].type == 'checkbox') {
                            checkboxes[i].checked = true;
                        }
                    }
                } else {
                    for (var i = 0; i < checkboxes.length; i++) {
                        //console.log(i)
                        if (checkboxes[i].type == 'checkbox') {
                            checkboxes[i].checked = false;
                        }
                    }
                }
            }
            function AddOrUpdate() {
                var arr_Menu = [];
                var tableControl = $(".module div.content table.table");
                var p_Id = <%=Request.QueryString["Code"]%>;
                var p_Name = $("#ddlPermission option:selected").text();

                $(".checkbox:checked", tableControl).each(function () {
                    var m_Id = $(this).closest('tr').find('.mID').text();
                    var m_Name = $(this).closest('tr').find('.mName').text();
                    var m_Url = $(this).closest('tr').find('.mUrl').text();
                    var m_Action = $(this).closest('tr').find('.mAction').find(".slAction").val();
                    //console.log(m_Action);
                    if (m_Action != null) {
                        var action = m_Action.join();
                    }
                    else{
                        action = null;
                    }
                        
                    arr_Menu.push({ p_Id,p_Name,m_Id, m_Name,m_Url,action });
                       
                });

                console.log(arr_Menu);
                //console.log(JSON.stringify(arr_Menu));
                $(".loading-frame").show();
                $.ajax({
                    type: "POST",
                    contentType: "application/json;charset:UTF-8",
                    datatype: "JSON",
                    url: "/GUI/BackEnd/Permission/Edit_Roles.aspx/AddOrUpdate_Menu",
                    data: "{list:" + JSON.stringify(arr_Menu) + "}",
                    success: function (data) {
                        $(".show-result").html(data.d);
                        if (data.d == "<span>Cập nhật thành công</span>") {
                            $(".show-result span").css("color","green");
                        }
                        else{
                            $(".show-result span").css("color","red");
                        }
                           
                    },
                    complete:function(){
                        $(".loading-frame").hide();  
                    }
                });

            }
                
             
        </script>
    </asp:Panel>
</asp:Content>
