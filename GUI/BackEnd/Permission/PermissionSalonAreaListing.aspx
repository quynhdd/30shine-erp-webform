﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="PermissionSalonAreaListing.aspx.cs" Inherits="_30shine.GUI.BackEnd.Permission.PermissionSalonAreaListing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
    <script src="/Assets/js/PermissionSalonArea.js"></script>
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <div class="wp sub-menu">
        <div class="wp960">
            <div class="wp content-wp">
                <ul class="ul-sub-menu" id="subMenu">
                    <li>Quản lý phân vùng Salon &nbsp;&#187;</li>
                    <li class="li-listing active"><a href="/admin/phan-quyen/danh-sach-phan-vung-salon.html">Danh sách</a></li>
                    <li class="li-add"><a href="/admin/phan-quyen/set-phan-vung-salon.html">Thêm mới</a></li>
                </ul>
            </div>
        </div>
    </div>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <div class="wp customer-add customer-listing be-report main-class">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <!-- Row Table Filter -->
                <div class="table-func-panel">
                    <div class="table-func-elm">
                        <span>Số hàng / Page : </span>
                        <div class="table-func-input-wp">
                            <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                            <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                            <ul class="ul-opt-segment">
                                <li data-value="10">10</li>
                                <li data-value="20">20</li>
                                <li data-value="30">30</li>
                                <li data-value="40">40</li>
                                <li data-value="50">50</li>
                                <li data-value="1000000">Tất cả</li>
                            </ul>
                            <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                        </div>
                    </div>
                </div>
                <!-- End Row Table Filter -->
                <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
                <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                    <ContentTemplate>
                        <div class="table-wp">
                            <table class="table-add table-listing" id="staff_contents">
                                <thead>
                                    <tr>
                                        <th style="width: 100px">STT</th>
                                        <th>ID</th>
                                        <th>Tên nhân viên</th>
                                        <th>Bộ phận</th>
                                        <th>Chức năng</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="RptPermissionSalonArea" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# Container.ItemIndex + 1 + PAGING._Offset %></td>
                                                <td><%# Eval("StaffId") %></td>
                                                <td>
                                                    <a href="/admin/nhan-vien/<%# Eval("StaffId")%>.html"><%# Eval("Fullname") %></a>
                                                </td>
                                                <td><%# Eval("Name") %></td>
                                                <td class="map-edit" style="width: 100px">
                                                    <button type="button" class="btn btn-info btn-xs details" staffid="<%#Eval("StaffId") %>">Sửa</button>
                                                    <div class="edit-wp" style="display: block">

                                                        <a class="elm del-btn" onclick="del(this.parentNode.parentNode.parentNode,'<%# Eval("StaffId") %>', '<%# Eval("Fullname") %>')" href="javascript://" title="Xóa"></a>
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>

                        <!-- Paging -->
                        <div class="site-paging-wp">
                            <% if (PAGING.TotalPage > 1)
                                { %>
                            <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                                <% if (PAGING._Paging.Prev != 0)
                                    { %>
                                <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                                <% } %>
                                <asp:Repeater ID="RptPaging" runat="server">
                                    <ItemTemplate>
                                        <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                            <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>>
                                            <%# Eval("PageNum") %>
                                        </a>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                                    { %>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                                <% } %>
                            </asp:Panel>
                            <% } %>
                        </div>
                        <!-- End Paging -->
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
                <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
            </div>
            <%-- END Listing --%>
        </div>
        <div id="systemModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div style="margin-top: 40px;">
                        <div class="row" style="margin-left: 20px; font-weight: bold; font-size: 15px;">
                            Phân vùng Salon
                        </div>
                        <hr />
                        <div class="row">
                            <div class="col-md-3">ID/Tên nhân viên:</div>
                            <div class="col-md-4">
                                <input class="form-control" id="ID" disabled="disabled" />
                            </div>
                            <div class="col-md-5">
                                <input class="form-control" id="Name" disabled="disabled" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">Salon:</div>
                            <div class="col-md-9">
                                <span class="field-wp" id="pem">
                                    <%--  <select class="salons" id="ddlSalon" multiple="multiple"></select>--%>
                                    <asp:ListBox SelectionMode="Multiple" ID="ddlSalon" runat="server" ClientIDMode="Static" CssClass="select perm-ddl-field"></asp:ListBox>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="button" id="btnAdd" onclick="update();" class="btn btn-success" value="Cập nhật" />
                        <button type="button" class="btn btn-danger" id="closeModal">Đóng</button>
                    </div>
                </div>
            </div>
        </div>
        <style>
            .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
                background-color: #fff;
            }

            .modal-backdrop.in {
                opacity: 0.9 !important;
            }

            #systemModal {
                margin-top: 60px;
            }

            .modal-content .row {
                margin-bottom: 20px;
            }

            #pem {
                width: 100%;
            }

                #pem span {
                    width: 100%;
                    line-height: 22px;
                    height: 22px;
                }

                    #pem span div.btn-group .multiselect, #pem span div.btn-group, .multiselect-container {
                        width: 100% !important;
                    }

            ul.multiselect-container.dropdown-menu {
                max-height: 300px;
                overflow-y: scroll;
                border-radius: 2px
            }

            .modal-dialog {
                margin-top: 110px;
            }

            .modal {
                z-index: 9998;
            }

            .be-report .row-filter {
                z-index: 0;
            }

            @media(min-width: 768px) {
                .modal-content, .modal-dialog {
                    width: 700px !important;
                    margin: auto;
                }
            }
        </style>
        <script type="text/javascript">
            
            jQuery(document).ready(function () {
                // call ham
                GetListSalon();
            });

            //open modal
            $(document).on("click", ".details", function () {
                // gán staffId
                staffID = $(this).attr("staffID");
                if (staffID > 0) {
                    //remove checked
                    $(".multiselect-native-select").find("input[value='multiselect-all']").prop("checked", false);
                    $(".multiselect-native-select").find("input[value='multiselect-all']").parent().parent().parent().removeClass('active');
                    for (var i = 0; i < jsonListSalon.length; i++) {
                        $(".multiselect-native-select").find("input[value='" + jsonListSalon[i].Id + "']").prop("checked", false);
                        $(".multiselect-native-select").find("input[value='" + jsonListSalon[i].Id + "']").parent().parent().parent().removeClass('active');
                    }
                    //call
                    bindOBJSalon(staffID);
                    $("#systemModal").modal();
                }
            });

            //close modal
            $(document).on("click", "#closeModal", function () {
                $("#systemModal").modal("hide");
            });

            //get staffId, FullName
            $(document).on("click", 'tr', function () {
                var table = document.getElementById("staff_contents");
                var tableIndex = $(this).index() + 1;
                $("#ID").val($(table.rows[tableIndex].cells[1]).text());
                $("#Name").val($(table.rows[tableIndex].cells[2]).text().trim());
            });

            ///delete
            function del(This, code, name) {
                var StaffId = code || null,
                    name = name || null,
                    Row = This;
                if (!StaffId) return false;

                // show EBPopup
                $(".confirm-yn").openEBPopup();
                $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn xóa [ " + name + " ] ?");

                $("#EBPopup .yn-yes").bind("click", function () {
                    $.ajax({
                        type: "POST",
                        url: "/GUI/BackEnd/Permission/PermissionSalonAreaListing.aspx/Delete",
                        data: '{StaffId:' + StaffId + '}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            if (response.d.success == true) {
                                var msg = "Dữ liệu đã được xóa thành công!";
                                $("#MsgSuccess").find(".row").text(msg).end().openEBPopup();
                                autoCloseEBPopup(0);
                                location.reload();
                            }
                            else {
                                delFailed();
                            }
                        },
                        failure: function (response) { alert(response.d); }
                    });
                });
            }
        </script>
    </asp:Panel>
</asp:Content>
