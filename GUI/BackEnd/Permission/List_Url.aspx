﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/TemplateMaster/SiteMaster.Master" CodeBehind="List_Url.aspx.cs" Inherits="_30shine.GUI.BackEnd.Permission.List_Url" %>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
    <style>
        .show-error {
            top: 70px;
            right: 20px;
            position: fixed;
            display: none;
            border: 2px solid #800000;
            border-radius: 5px;
            z-index: 99999;
            background: white;
            padding: 10px 20px;
        }
    </style>
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý menu &nbsp;&#187; </li>
                        <li class="li-listing active"><a href="/admin/phan-quyen/danh-sach-menu.html">Danh sách đường dẫn</a></li>
                        <li class="li-add"><a id="_add" href="/admin/phan-quyen/them-moi-menu.html">Thêm mới</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <span class="show-error">
            
        </span>
        <div class="wp customer-add customer-listing">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <div class="table-wp">
                    <table class="table" id="tableUrl">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Tên</th>
                                <th>Đường dẫn</th>
                                <th>Ẩn hiện trên Menu</th>
                                <th>Ẩn hiện</th>
                                <th>Chức năng</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <%-- END Listing --%>
        </div>
        <script>
            $(document).ready(function () {

                $("#tableUrl").dataTable({
                    //"bSort": false,
                    "pageLength": 10,
                    "lengthMenu": [[10, 25, 50, 100,1000000], [10, 25, 50,100,1000000]],
                    "language": {
                        "lengthMenu": " Bản ghi / trang _MENU_",
                        "zeroRecords": "Không tồn tại bản ghi",
                        "info": "Hiển thị trang _PAGE_ trên _PAGES_",
                        "infoEmpty": "Không có dữ liệu",
                        "infoFiltered": "(Lọc kết quả từ _MAX_ tổng số bản ghi)",
                        "search": "Tìm kiếm",
                        "paginate": {
                            "first": "Đầu",
                            "last": "Cuối",
                            "next": "+",
                            "previous": "-"
                        },
                    },
                    //"bLengthChange": false,
                    "bInfo": false,
                    //"bFilter": false,
                    //"serverSide": true,
                    columns: [
                    { 'data': 'Id' },
                    { 'data': 'FrontName' },
                    { 'data': 'RewriteUrl' },
                    { 'data': 'showHideMenu' },
                    { 'data': 'checkBox' },
                    { 'data': 'Action', }
                    ],
                    bServerSide: true,
                    sAjaxSource: '/GUI/SystemService/Webservice/Permission.asmx/GetUrl',
                    sServerMethod: 'POST'
                });
            });
        </script>
        <script>
            $(document).ready(function () {
                $("#glbPermission").addClass("active");
                $("#glbListPermission").addClass("active");
            });
            $(document).on("change", ".checkPublish", function (e) {
                var Id = $(this).attr("data-id");
                var isPublish;
                if (this.checked)
                    isPublish = 1;
                else
                    isPublish = 0;
                $.ajax({
                    type: "POST",
                    contentType: "application/json;charset=UTF-8",
                    data: '{Id:' + Id + ',isPublish:' + isPublish + '}',
                    dataType: "JSON",
                    url: "/GUI/BackEnd/Permission/List_Url.aspx/Publish",
                    success: function (data) {
                        alert("Thay đổi trạng thái thành công!");
                    },
                    error: function (result) {
                        alert("Có lỗi xảy ra! Vui lòng liên hệ với nhóm phát triển!");
                    }
                });
            });

            $(document).on("change", ".show-hide-menu", function (e) {
                var Id = $(this).attr("data-id");
                var ShowMenu;
                if (this.checked)
                    ShowMenu = 1;
                else
                    ShowMenu = 0;
                $.ajax({
                    type: "POST",
                    contentType: "application/json;charset=UTF-8",
                    data: '{Id:' + Id + ',showMenu:' + ShowMenu + '}',
                    dataType: "JSON",
                    url: "/GUI/BackEnd/Permission/List_Url.aspx/ShowMenu",
                    success: function (data) {
                        $(".show-error").show();
                        setTimeout(function () {
                            $('.show-error').fadeOut();
                        }, 3000);
                        $(".show-error").empty().append("Ẩn hiện menu thành công");
                        //alert("Thay đổi trạng thái thành công!");
                    },
                    error: function (result) {
                        alert("Có lỗi xảy ra! Vui lòng liên hệ với nhóm phát triển!");
                    }
                });
            });
        </script>
    </asp:Panel>
</asp:Content>
