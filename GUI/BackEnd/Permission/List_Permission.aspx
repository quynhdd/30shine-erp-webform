﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/TemplateMaster/SiteMaster.Master" CodeBehind="List_Permission.aspx.cs" Inherits="_30shine.GUI.BackEnd.Permission.List_Permission" %>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
    <style>
        #slMenu{
            font-family:Roboto Condensed Regular;
        }
        #slMenu option.prUrl{
            font-weight:bold;
        }
        #slMenu option.childUrl{
            font-style:italic;
 
        }
    </style>
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý phân quyền &nbsp;&#187; </li>
                        <li class="li-listing active"><a href="/admin/phan-quyen/danh-sach-phan-quyen.html">Danh sách phân quyền</a></li>
                        <li class="li-add"><a href="/admin/phan-quyen/them-moi-quyen.html">Thêm mới</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="wp customer-add customer-listing">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <div class="table-wp">
                    <table class="table" id="tablePermission" cell-spacing="0">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Tên</th>
                                <%-- <th>Kí hiệu</th>--%>
                                <%--<th>Ngày tạo</th>--%>
                                <th>Ẩn/Hiện</th>
                                <th>Chức năng</th>
                            </tr>
                        </thead>

                    </table>
                </div>
            </div>
            <%-- END Listing --%>
        </div>
        <%--Modal--%>
            <div id="myModal" class="modal fade" role="dialog">
              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                  </div>
                  <div class="modal-body">
                      <input type="hidden" id="pID" /><input type="hidden" id="pName" />
                    <select class="form-control" id="slMenu">
                        
                    </select>
                    <div class="show-error" style="display:none"></div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" id="btnConfirm" >Đồng ý</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </div>

              </div>
            </div>
        <script>
            $(document).ready(function () {
                $("#tablePermission").dataTable({
                    //"bSort": false,
                    "pageLength": 10,
                    "language": {
                        "lengthMenu": " Bản ghi / trang _MENU_",
                        "zeroRecords": "Không tồn tại bản ghi",
                        "info": "Hiển thị trang _PAGE_ trên _PAGES_",
                        "infoEmpty": "Không có dữ liệu",
                        "infoFiltered": "(Lọc kết quả từ _MAX_ tổng số bản ghi)",
                        "search": "Tìm kiếm",
                        "paginate": {
                            "first": "Đầu",
                            "last": "Cuối",
                            "next": "+",
                            "previous": "-"
                        },
                    },
                    //"bLengthChange": false,
                    //"bFilter": false,
                    //"serverSide": true,
                    columns: [
                    { 'data': 'Id' },
                    { 'data': 'PermissionName' },
                    //{ 'data': 'Title' },
                    //{ 'data': 'CreatedDate' },
                    { 'data': 'checkBox' },
                    { 'data': 'Action' }
                    ],
                    bServerSide: true,
                    sAjaxSource: '/GUI/SystemService/Webservice/Permission.asmx/GetPem',
                    sServerMethod: 'POST'

                });
            });
        </script>
        <script>
            $(document).ready(function () {
                $("#glbPermission").addClass("active");
                $("#glbListPermission").addClass("active");
     
            })
            //get value chkPublish
            $(document).on("change", "#checkPublish", function (e) {
                var Id = $(this).attr("data-id");
                var isPublish;
                if (this.checked)
                    isPublish = 1;
                else
                    isPublish = 0;
                $.ajax({
                    type: "POST",
                    contentType: "application/json;charset=UTF-8",
                    data: '{Id:' + Id + ',isPublish:' + isPublish + '}',
                    datatype: "JSON",
                    url: "/GUI/BackEnd/Permission/List_Permission.aspx/Publish",
                    success: function (data) {
                        alert("Thay đổi trạng thái thành công!");
                    },
                    error: function (result) {
                        alert("Có lỗi xảy ra! Vui lòng liên hệ với nhóm phát triển!");
                    }
                });
            });
            //bill data to dropdownlist slMenu
            $(document).on("click", ".btn-Edit-Default", function (e) {
                $("#myModal").modal("show");
                
                var pID = $(this).attr("pID");
                var pName = $(this).attr("pName");
                $("#pID").val(pID);
                $("#pName").val(pName);
                $(".modal-title").html("Thay đổi trang mặc định cho nhóm quyền <b>" + pName + "</b>");
                $.ajax({
                    type: "POST",
                    contentType: "application/json;charset=UTF-8",
                    data: '{pID:' + pID + '}',
                    datatype: "JSON",
                    url: '/GUI/SystemService/Webservice/Permission.asmx/GetLinkForChangeDefaultPage',
                    success: function (data) {
                        jsdt = JSON.parse(data.d);
                        $("#slMenu").empty();
                        var option = "<option value='' selected='selected'>--- Chọn Page mặc định ---</option>";
                        for (var i = 0; i < jsdt.length; i++) {
                            $("#slMenu").empty();
                            option += "<option value='" + jsdt[i].Id + "' class='prUrl'  disabled='disabled'>" + jsdt[i].FrontName + "</option>";
                            for (var j = 0; j < jsdt[i]._listUrl.length; j++) {
                                $("#slMenu").empty();
                                option += "<option url='" + jsdt[i]._listUrl[j].RewriteUrl +"' value='" + jsdt[i]._listUrl[j].Id + "' class='childUrl'";
                                if (jsdt[i]._listUrl[j].RewriteUrl == "javascript:void(0)") {
                                  option +=  "disabled='disabled'";
                                }
                                option +=  ">------&nbsp" + jsdt[i]._listUrl[j].FrontName + "&nbsp(" + jsdt[i]._listUrl[j].RewriteUrl + ")</option>";
                               
                            }
                            $("#slMenu").append(option);
                        }
                    }
                })
            })

            //event change default page for permission group
            $(document).on("click", "#btnConfirm", function (e) {
                if ($("#slMenu").val()== "") {
                    var msg = "<span>Xin vui lòng chọn 1 trong các đường dẫn trên</span>";
                    $(".show-error").html(msg);
                    $(".show-error").show();
                    $(".show-error span").css({ "color": "red", "font-style": "italic" });
                    $(this).focus();
                    
                }
                else {
                    //debugger;
                    var mID = $("#slMenu").val();
                    var mName = $("#slMenu option:selected").attr("mName");
                    var url = $("#slMenu option:selected").attr("url");
                    var permID = $("#pID").val();
                    var permName = $("#pName").val();
                    $.ajax({
                        type: "POST",
                        contentType: "application/json;charset=UTF-8",
                        data: '{pID:' + permID + ',pName:"' + permName + '",mID:' + mID + ',mName:"' + mName + '",url:"' + url + '"}',
                        datatype: "JSON",
                        url: '/GUI/SystemService/Webservice/Permission.asmx/ChangeDefaultPage',
                        success: function (data) {
                            $(".show-error").html(data.d);
                            $(".show-error").show();
                            $(".show-error span").css({ "color": "green", "font-style": "italic" });
                        }
                    })
                }
            })
        </script>
    </asp:Panel>
</asp:Content>
