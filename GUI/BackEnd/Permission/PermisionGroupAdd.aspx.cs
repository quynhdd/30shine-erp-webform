﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.Permission
{
    public partial class PermisionGroupAdd : System.Web.UI.Page
    {
        IPermisionGroup PermisionModel = new PermisionGroupModel();
        protected bool Perm_Access = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
        }

        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                pageId = permissionModel.GetRegexPageIdSpecial().Replace(pageId, ".html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        protected void AddGroupPermision(object sender, EventArgs e)
        {
            string namePermision = txtNamePermision.Text;
            string description = txtDescription.Text;
            bool IsActive = CkPublish.Checked;
            if (namePermision == "")
            {
                WaringClient("Chưa nhập nhóm quyền!");
            }
            else
            {
                Msg resultCheck = PermisionModel.IsDuplicateGroupPermision(namePermision);
                if (resultCheck.success)
                {
                    var obj = new MODEL.ENTITY.EDMX.PermissionErp();
                    obj.CreateTime = DateTime.Now;
                    obj.Description = description;
                    obj.IsActive = IsActive;
                    obj.Name = namePermision;
                    obj.IsDelete = false;
                    Msg resultAdd = PermisionModel.AddGroupPermision(obj);
                    if (resultAdd.success)
                    {
                        var MsgParam = new List<KeyValuePair<string, string>>();
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Thêm mới thành công!"));
                        UIHelpers.Redirect("/admin/phan-quyen/danh-sach-nhom-quyen.html", MsgParam);
                    }
                    else
                    {
                        WaringClient(resultAdd.msg);
                    }
                }
                else
                {
                    WaringClient(resultCheck.msg);
                }
            }
        }
        /// <summary>
        /// Gửi thông tin cảnh báo về client
        /// </summary>
        /// <param name="message"></param>
        public void WaringClient(string message)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Click", "ShowWarning('" + message + "');", true);
        }

    }
}