﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="PermissionSalonArea_Add.aspx.cs" Inherits="_30shine.GUI.BackEnd.Permission.PermissionSalonArea_Add" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CtMain" runat="server">
    <script src="/Assets/js/PermissionSalonArea.js"></script>
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <style>
            #success-alert {
                top: 100px;
                right: 10px;
                position: fixed;
                width: 20% !important;
                z-index: 2000;
            }

            #pem {
                width: 100%;
            }

                #pem span {
                    width: 100%;
                    line-height: 22px;
                    height: 22px;
                }

                    #pem span div.btn-group .multiselect, #pem span div.btn-group, .multiselect-container {
                        width: 100% !important;
                    }

            ul.multiselect-container.dropdown-menu {
                max-height: 300px;
                overflow-y: scroll;
            }
        </style>
        <div class="alert alert-danger" id="success-alert">
            <span id="msg-alert"></span>
        </div>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý phân vùng Salon &nbsp;&#187;</li>
                        <li class="li-listing active"><a href="/admin/phan-quyen/danh-sach-phan-vung-salon.html">Danh sách</a></li>
                        <li class="li-add"><a href="/admin/phan-quyen/set-phan-vung-salon.html">Thêm mới</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="wp customer-add">
            <%-- Add --%>
            <div class="wp960 content-wp">
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>

                <div class="table-wp" style="">
                    <table class="table-add admin-product-table-add">
                        <tbody>
                            <tr class="title-head">
                                <td><strong>Nhập thông tin</strong></td>
                                <td></td>
                            </tr>
                            <tr class="tr-field-ahalf">
                                <td class="col-xs-3 left"><span>ID/ Tên nhân viên<strong style="color: red"> &nbsp*</strong></span></td>
                                <td class="col-xs-9 right">
                                    <div class="field-wp">
                                        <div class="div_col">
                                            <input type="text" id="ID" placeholder="Nhập ID nhân viên" class="form-control" onchange="GetListStaffASM()" onkeypress="return ValidateKeypress(/\d/,event);" />
                                        </div>
                                        <div class="div_col">
                                            <input type="text" id="Name" style="margin-left: 10px" class="form-control" placeholder="Họ tên nhân viên" disabled="disabled" />
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-2 left"><span>Salon</span></td>
                                <td class="col-xs-9 right" style="padding: 20px 0px !important;">
                                    <span class="field-wp" id="pem">
                                        <select name="ddlSalon" class="ddlSalon form-control" id="ddlSalon" multiple="multiple"></select>
                                    </span>
                                </td>
                            </tr>
                            <tr class="tr-send">
                                <td class="col-xs-2 left"></td>
                                <td class="col-xs-9 right no-border">
                                    <span id="nhaphoc" class="field-wp">
                                        <input type="button" id="btnAdd" class="btn-send" onclick="add()" value="Hoàn tất" />
                                    </span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <%-- end Add --%>
        </div>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                $("#success-alert").hide();
                // call ham
                GetListSalon();
            });
        </script>
    </asp:Panel>
</asp:Content>
