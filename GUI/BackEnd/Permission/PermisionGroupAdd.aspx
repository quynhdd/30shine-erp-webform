﻿<%@ Page Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="PermisionGroupAdd.aspx.cs" Inherits="_30shine.GUI.BackEnd.Permission.PermisionGroupAdd" %>

<asp:Content ID="ProductAdd" ContentPlaceHolderID="CtMain" runat="server">
    <style>
        .disabled {
            pointer-events: none;
            cursor: default;
        }
    </style>
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý nhóm quyền&nbsp;&#187; </li>
                        <li class="li-listing"><a href="/admin/phan-quyen/danh-sach-nhom-quyen.html">Danh sách nhóm quyền</a></li>
                        <li class="li-add active"><a href="/admin/phan-quyen/them-moi-nhom-quyen.html">Thêm mới</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="wp customer-add">
            <%-- Add --%>
            <div class="wp960 content-wp">
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <div class="table-wp">
                    <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
                    <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                        <ContentTemplate>
                            <table class="table-add admin-product-table-add">
                                <tbody>
                                    <tr class="title-head">
                                        <td><strong>Thông tin nhóm quyền</strong></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="col-xs-3 left"><span>Nhập tên nhóm quyền<i style="color: red">&nbsp*</i></span></td>
                                        <td class="col-xs-9 right">
                                            <span class="field-wp">
                                                <asp:TextBox ClientIDMode="Static" ID="txtNamePermision" runat="server"></asp:TextBox>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-xs-3 left"><span>Mô tả</span></td>
                                        <td class="col-xs-9 right">
                                            <span class="field-wp">
                                                <asp:TextBox ID="txtDescription" Rows="5" TextMode="MultiLine" runat="server" ClientIDMode="Static"></asp:TextBox>
                                            </span>
                                        </td>
                                    </tr>
                                <tr class="tr-send">
                                        <td class="col-xs-3 left"><span style="margin-top:70px !important">Ẩn/Hiện</span></td>
                                        <td class="col-xs-9 right">
                                            <span class="field-wp">
                                                <asp:CheckBox ID="CkPublish"  style="margin-top:34px !important" Checked="true" runat="server" />
                                            </span>
                                        </td>
                                    </tr>
                                    <tr class="tr-send">
                                        <td class="col-xs-3 left"></td>
                                        <td class="col-xs-9 right no-border">
                                            <span class="field-wp">
                                                <asp:Button ID="Send" style="margin-top:-20px !important" OnClientClick="checkInput()" CssClass="btn-send" runat="server" Text="Hoàn tất" ClientIDMode="Static" OnClick="AddGroupPermision"></asp:Button>
                                            </span>
                                        </td>
                                    </tr>
                                    <asp:HiddenField runat="server" ID="UrlKeyField" ClientIDMode="Static" />
                                </tbody>
                            </table>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="Send" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
            <%--End Add --%>
        </div>
        <script>
            $(document).ready(function () {
                $("#txtNamePermision").bind("click", function () {
                    $('#txtNamePermision').css("border-color", "");
                })
            });
            function checkGroupName() {
                var permisionName = $('#txtNamePermision').val();
                var regex = /[ÁÀẢÃẠÂẤẦẨẪẬĂẮẰẲẴẶÉÈẺẼẸÊẾỀỂỄỆÍÌỈĨỊÓÒỎÕỌÔỐỒỔỖỘƠỚỜỞỠỢÚÙỦŨỤƯỨỪỬỮỰÝỲỶỸỴĐáàảãạâấầẩẫậăắằẳẵặéèẻẽẹêếềểễệíìỉĩịóòỏõọôốồổỗộơớờởỡợúùủũụưứừửữựýỳỷỹỵđ]/;
                var checkName = permisionName.match(regex);

                if (checkName != null) {
                    ShowWarning("Tên nhóm quyền không được nhập tiếng việt có dấu")
                    return true
                }
                else
                    return false;
            }
        
            function checkInput() {
                if ($('#txtNamePermision').val() == "" || checkGroupName()) {
                    $('#txtNamePermision').val() == "" ? $('#txtNamePermision').css("border-color", "red") : "";
                    event.preventDefault();
                    return false;
                }
                else {
                    return true;
                }
            }
            function ShowWarning(message) {
                $('#MsgSystem').text(message);
                $('#MsgSystem').css("color", "red");
            }
        </script>
    </asp:Panel>
</asp:Content>
