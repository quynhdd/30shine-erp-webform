﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Data.Entity.Migrations;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.Permission
{
    public partial class PermissionApiAdd : System.Web.UI.Page
    {
        private string PageID = "";
        protected bool CheckUpdate = false;
        private bool Perm_Access = false;
        protected bool Perm_ViewAllData = false;
        protected IPermissionActionModel iPermissionModel = new PermissionActionModel();
        private static PermissionApiAdd instance;
        protected string permissionId;
        protected _30shine.MODEL.ENTITY.EDMX.PermissionApi OBJ;
        CultureInfo culture = new CultureInfo("vi-VN");
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                Library.Function.bindApiMethod(new List<DropDownList> { ApiMethod }, true);
                if (IsUpdate())
                {
                    BindOBJ();
                }
            }
        }

        /// <summary>
        /// get instance, phục vụ trường hợp có hàm statistic, sử dụng instance có thẻ gọi các method không phải static
        /// </summary>
        /// <returns></returns>
        public static PermissionApiAdd getInstance()
        {
            if (!(PermissionApiAdd.instance is PermissionApiAdd))
            {
                PermissionApiAdd.instance = new PermissionApiAdd();
            }

            return PermissionApiAdd.instance;
        }

        /// <summary>
        ///  set phân quyền 
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //if (Request.QueryString["Code"] != null)
                //{
                //    PageID = "PermissonActionEdit";
                //}
                //else
                //{
                //    PageID = "PermissonActionAdd";
                //}
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                pageId = permissionModel.GetRegexPageIdSpecial().Replace(pageId, ".html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        /// <summary>
        /// Bind Data 
        /// </summary>
        private void BindOBJ()
        {
            try
            {
                //OBJ = iPermissionModel.GetById(Convert.ToInt32(permissionId));
                using (var db = new Solution_30shineEntities())
                {
                    var id = Convert.ToInt32(permissionId);
                    OBJ = (
                                                  from p in db.PermissionApis
                                                  where p.Id == id && p.IsDelete == false
                                                  select p
                                              )
                                              .OrderByDescending(w => w.Id)
                                              .FirstOrDefault();
                }
                if (OBJ != null)
                {
                    ApiPath.Text = OBJ.ApiPath;
                    ApiMethod.SelectedValue = OBJ.ApiMethod.ToString();
                    DescriptionAction.Text = OBJ.Description;
                }
            }
            catch (Exception ex)
            {
                new _30shine.Helpers.PushNotiErrorToSlack().PushDefault(ex.Message + ", MAIN: " + ex.StackTrace, this.ToString() + ".bindData", "");
                throw new Exception();
            }
        }

        private bool IsUpdate()
        {
            if (Request.QueryString["Code"] == null) { CheckUpdate = false; return false; }

            permissionId = Request.QueryString["Code"];
            if (permissionId != "")
            {
                CheckUpdate = true;
                return true;
            }
            else
            {
                CheckUpdate = false;
                return false;

            }

        }

        protected void AddOrUpdate(object sender, EventArgs e)
        {
            if (IsUpdate())
            {
                Update();

            }
            else
            {
                Add();
            }
        }

        /// <summary>
        /// Insert permisson action
        /// </summary>
        protected void Add()
        {
            try
            {
                //_30shine.MODEL.ENTITY.EDMX.PermissionApi record = new _30shine.MODEL.ENTITY.EDMX.PermissionApi();
                //record.Name = NameAction.Text.Trim();
                //record.Description = DescriptionAction.Text;
                //iPermissionModel.Add(record);
                try
                {
                    //check Duplicate
                    Library.Class.cls_message message = (Library.Class.cls_message)checkDuplicate(ApiPath.Text, Convert.ToInt32(ApiMethod.SelectedValue), 0);
                    if (message.success)
                    {
                        //neu da bi trung
                        TriggerJsMsgSystem_temp(this, "Api path, method đã tồn tại", 4);
                        return;
                    }

                    //
                    PermissionApi record = new PermissionApi();
                    using (var db = new Solution_30shineEntities())
                    {
                        //if (obj != null)
                        //{
                        //    record = obj;
                        //}
                        record.ApiPath = ApiPath.Text;
                        record.ApiMethod = Convert.ToInt32(ApiMethod.SelectedValue);
                        record.Description = DescriptionAction.Text;
                        record.CreatedTime = DateTime.Now;
                        record.IsDelete = false;
                        db.PermissionApis.Add(record);
                        db.SaveChanges();
                    }
                }
                catch
                {
                    throw new Exception();
                }

                var MsgParam = new List<KeyValuePair<string, string>>();
                MsgParam.Add(new KeyValuePair<string, string>("msg_status", "2"));
                MsgParam.Add(new KeyValuePair<string, string>("msg_message", "Thêm mới thành công!"));
                UIHelpers.Redirect("/admin/phan-quyen/danh-sach-api.html", MsgParam);
            }
            catch (Exception ex)
            {
                new _30shine.Helpers.PushNotiErrorToSlack().PushDefault(ex.Message + ", MAIN: " + ex.StackTrace, this.ToString() + ".Add", "");
                //throw new Exception("Có lỗi xảy ra xin vui lòng liên hệ nhà phát triển!!!");
                TriggerJsMsgSystem_temp(this, "Có lỗi xảy ra xin vui lòng liên hệ nhà phát triển!!!", 4);
                return;

            }
        }

        /// <summary>
        /// Update permisson action
        /// </summary>
        protected void Update()
        {
            try
            {
                //_30shine.MODEL.ENTITY.EDMX.PermissionAction record = new _30shine.MODEL.ENTITY.EDMX.PermissionAction();
                //record = iPermissionModel.GetById(Convert.ToInt32(permissionId));
                //record.Name = NameAction.Text.Trim();
                //record.Description = DescriptionAction.Text;
                //var exc = iPermissionModel.Update(record);
                //check Duplicate
                Library.Class.cls_message message = (Library.Class.cls_message)checkDuplicate(ApiPath.Text, Convert.ToInt32(ApiMethod.SelectedValue), Convert.ToInt32(permissionId));
                if (message.success)
                {
                    //neu da bi trung
                    TriggerJsMsgSystem_temp(this, "Api path, method đã tồn tại", 4);
                    return;
                }

                //
                //PermissionApi record = new PermissionApi();
                var id = Convert.ToInt32(permissionId);
                using (var db = new Solution_30shineEntities())
                {
                    PermissionApi record = Load(w => w.Id == id);
                    record.ApiPath = ApiPath.Text;
                    record.ApiMethod = Convert.ToInt32(ApiMethod.SelectedValue);
                    record.Description = DescriptionAction.Text;
                    record.ModifiedTime = DateTime.Now;
                    record.IsDelete = false;
                    db.PermissionApis.AddOrUpdate(record);
                    db.SaveChanges();
                }

                var MsgParam = new List<KeyValuePair<string, string>>();
                MsgParam.Add(new KeyValuePair<string, string>("msg_status", "2"));
                MsgParam.Add(new KeyValuePair<string, string>("msg_message", "Cập nhật thành công!"));
                UIHelpers.Redirect("/admin/phan-quyen/danh-sach-api.html", MsgParam);
            }
            catch (Exception ex)
            {                
                new _30shine.Helpers.PushNotiErrorToSlack().PushDefault(ex.Message + ", MAIN: " + ex.StackTrace, this.ToString() + ".Update", "");
                //throw new Exception("Có lỗi xảy ra xin vui lòng liên hệ nhà phát triển!!!");
                TriggerJsMsgSystem_temp(this, "Có lỗi xảy ra xin vui lòng liên hệ nhà phát triển!!!", 4);
                return;
            }
        }

        /// <summary>
        /// Trả thông báo về client 
        /// </summary>
        /// <param name="_OBJ"></param>
        /// <param name="msg"></param>
        /// <param name="status"></param>        
        public void TriggerJsMsgSystem_temp(Page _OBJ, string msg, int status)
        {
            ScriptManager.RegisterStartupScript(_OBJ, _OBJ.GetType(), "Message System", "ShowMessage('Thông báo','" + msg + "'," + status + ");", true);
        }

        /// <summary>
        /// Check duplicate name action
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [WebMethod]
        public static object checkDuplicate(string apiPath, int apiMethod, int id)
        {
            var message = new Library.Class.cls_message();
            try
            {
                PermissionApi record = new PermissionApi();
                if (id == 0)
                {
                    record = Load(w => w.ApiPath == apiPath && w.ApiMethod == apiMethod);
                }
                else
                {
                    record = Load(w => w.ApiPath == apiPath && w.ApiMethod == apiMethod && w.Id != id);
                }
                if (record != null)
                {
                    message.success = true;
                }
                else
                {
                    message.success = false;
                }
            }
            catch (Exception ex)
            {
                new _30shine.Helpers.PushNotiErrorToSlack().PushDefault(ex.Message + ", MAIN: " + ex.StackTrace, "checkDuplicate", "");
            }
            return message;
        }

        public static PermissionApi Load(Expression<Func<PermissionApi, bool>> expression)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    PermissionApi record = (
                                                  from p in db.PermissionApis
                                                  where p.IsDelete == false
                                                  select p
                                              ).Where(expression)
                                              .OrderByDescending(w => w.Id)
                                              .FirstOrDefault();
                    return record;
                }
            }
            catch
            {
                throw new Exception();
            }
        }
    }
}