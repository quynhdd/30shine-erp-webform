﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PermissionApiList.aspx.cs" Inherits="_30shine.GUI.BackEnd.Permission.PermissionApiList" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>


<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <link href="/Assets/css/bootstrap/bootstrap-toggle.min.css" rel="stylesheet" />
        <script src="/Assets/js/bootstrap/bootstrap-toggle.min.js"></script>
        <style>
            .table-listing .uv-avatar {
                width: 120px;
            }

            .table-row-detail {
                display: inline-block;
                width: 47% !important;
                min-width: 500px;
                float: none !important;
            }

            .product-QuantityExport {
                width: 50px !important;
                height: 22px !important;
                line-height: 22px !important;
                text-align: center;
                border-bottom: 1px solid #ddd !important;
                border-top: none !important;
                border-right: none !important;
                border-left: none !important;
                background: none;
            }

            @media(min-width: 768px) {
                .modal-content, .modal-dialog {
                    width: 900px !important;
                    margin: auto;
                }
            }
        </style>

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý API</li>
                        <li class="li-listing active"><a href="/admin/phan-quyen/danh-sach-api.html">Danh sách api</a></li>
                        <li class="li-add"><a href="/admin/phan-quyen/them-moi-api.html">Thêm mới</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing be-report be-report-timekeeping">
            <%-- Listing --%>
            <label id="lblSalonId" hidden="hidden"><%=salonId%></label>
            <div class="wp960 content-wp">
                <asp:ScriptManager ID="SM01" runat="server"></asp:ScriptManager>
                <!-- System Message -->
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <!-- END System Message -->
                <div class="row">
                    <%--<div class="filter-item">
                        <strong class="st-head"><i class="fa fa-clock-o"></i>Thời gian</strong>
                        <div class="time-wp">
                            <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeFrom" placeholder="Từ ngày" Style="margin-left: 10px!important"
                                ClientIDMode="Static" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="ValidateTime" ControlToValidate="TxtDateTimeFrom" runat="server" CssClass="fb-cover-error" Text="Vui lòng chọn thời gian!"></asp:RequiredFieldValidator>
                        </div>
                        <strong class="st-head" style="margin-left: 10px;">
                            <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                        </strong>
                        <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeTo" placeholder="Đến ngày" Style="margin-left: 10px!important"
                            ClientIDMode="Static" runat="server"></asp:TextBox>
                        <br />
                    </div>--%>
                    <div class="filter-item">
                        <asp:TextBox ID="apiPath" runat="server" CssClass="txtFilter form-control" ClientIDMode="Static" Style="width: 180px;" ToolTip="Api path" placeholder="Api path"> </asp:TextBox>
                    </div>
                    <div class="filter-item">                        
                        <asp:DropDownList ToolTip="Api method" ID="apiMethod" CssClass="select form-control" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 190px;"></asp:DropDownList>
                    </div>                  
                    <asp:Panel ID="ViewData" CssClass="st-head btn-viewdata" ClientIDMode="Static"
                        onclick="excPaging(1)" runat="server">
                        Xem dữ liệu
                    </asp:Panel>

                </div>
                <div class="row">
                    <strong class="st-head"><i class="fa fa-file-text"></i>Danh sách api</strong>
                </div>
                <!-- Row Table Filter -->
                <div class="table-func-panel" style="margin-top: -33px;">
                    <div class="table-func-elm">
                        <span>Số hàng / Page : </span>
                        <div class="table-func-input-wp">
                            <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                            <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                            <ul class="ul-opt-segment">
                                <li data-value="10">10</li>
                                <li data-value="20">20</li>
                                <li data-value="30">30</li>
                                <li data-value="40">40</li>
                                <li data-value="50">50</li>
                                <li data-value="1000000">Tất cả</li>
                            </ul>
                            <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                        </div>
                    </div>
                </div>
                <!-- End Row Table Filter -->
                <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                    <ContentTemplate>
                        <div class="row table-wp">
                            <table class="table-add table-listing" id="tblDonHangKho">
                                <thead>
                                    <tr>
                                        <th>STT</th>                                        
                                        <th>Api path</th>
                                        <th>Api method</th>                                      
                                        <th>Hành động</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="rptDanhsach" runat="server">
                                        <ItemTemplate>
                                            <tr class="parent" >
                                                <td onclick="viewDetail(<%# Eval("Id") %>,$(this));" class="td-index" ><%# Container.ItemIndex + 1 + PAGING._Offset %></td>                                                
                                                <td onclick="viewDetail(<%# Eval("Id") %>,$(this));" class="tdOrderID"><%#Eval ("ApiPath") %></td>
                                                <td onclick="viewDetail(<%# Eval("Id") %>,$(this));"><%#Eval ("ApiMethodName") %></td>                                              
                                                <td class="map-edit">
                                                    <div class="edit-wp">                                                        
                                                        <a class="elm edit-btn" href="/admin/phan-quyen/sua-api/<%# Eval("id") %>.html" title="Sửa"></a>                                                        
                                                        <%if (Perm_Delete)
                                                            { %>
                                                        <a class="elm del-btn del_Bill" onclick="del($(this),'<%# Eval("Id") %>','<%# Eval("ApiPath") %>-<%#Eval ("ApiMethodName") %>')" href="javascript://" title="Xóa"></a>
                                                        <%} %>
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>
                        <!-- Paging -->
                        <div class="site-paging-wp">
                            <% if (PAGING.TotalPage > 1)
                                { %>
                            <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                                <% if (PAGING._Paging.Prev != 0)
                                    { %>
                                <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                                <% } %>
                                <asp:Repeater ID="RptPaging" runat="server">
                                    <ItemTemplate>
                                        <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                            <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>>
                                            <%# Eval("PageNum") %>
                                        </a>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                                    { %>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                                <% } %>
                            </asp:Panel>
                            <% } %>
                        </div>
                        <!-- End Paging -->
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />

                <!-- Hidden Field-->
                <asp:HiddenField ID="HDF_TypeStaff" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="HDF_Staff" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="HDF_ExcelType" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="HDF_ExportType" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="HDF_UserId" ClientIDMode="Static" runat="server" />
                <!-- END Hidden Field-->
            </div>
            <%-- END Listing --%>
        </div>
        <div id="viewModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">

                    <div style="margin-top: 20px;">
                        <div class="row" style="margin-left: 20px; font-weight: bold; font-size: 18px;">
                            CHI TIẾT ĐƠN HÀNG
                        </div>
                        <hr />
                        <div>
                            <table>
                                <tr class="tr-field-ahalf tr-product">
                                    <%-- <td class="col-xs-2 left">
                                        <span>Dịch vụ</span>
                                    </td>--%>
                                    <td class="col-xs-10 right">
                                        <div id="Div1" class="listing-product item-service">
                                            <table id="table-order-detail" class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>STT</th>
                                                        <th class="maSPcol">Mã sản phẩm</th>
                                                        <th>BarCode</th>
                                                        <th>Tên sản phẩm</th>                                                      
                                                        <th style="width: 50px">SL Mỹ phẩm</th>
                                                        <th style="width: 50px">SL Vật tư Nhóm 1</th>
                                                        <th style="width: 50px">SL Vật tư Nhóm 2</th>
                                                        <th style="width: 50px">SL Vật tư Nhóm 3</th>
                                                        <th style="width: 50px">SL Vật tư NCC</th>
                                                        <th style="width: 50px">Hệ thống đề xuất</th>
                                                        <th style="width: 50px">Tổng Order</th>
                                                        <th style="width: 50px">Xuất</th>
                                                        <th style="width: 50px">Xác nhận nhập</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="tbodyDetail">
                                                </tbody>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <hr />
                        <div>
                            <table>
                                <tr class="tr-field-ahalf tr-product">
                                    <td class="col-xs-1 right">
                                        <span>Ghi chú</span>
                                    </td>
                                     <td class="col-xs-10 right">
                                        <textarea style="width:100%; height:100px;" disabled="disabled" id="Note"></textarea>
                                    </td>
                                </tr>
                            </table>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" id="closeModal">Đóng</button>
                    </div>
                </div>
            </div>
        </div>


        <link href="/Assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" />
        <script src="/Assets/js/jquery.mCustomScrollbar.js"></script>
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 180px !important;
                margin-top: 5px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }
        </style>
        <script type="text/ecmascript">
            jQuery(document).ready(function () {
                var qs = getQueryStrings();
                ShowMessage("Thông báo", qs["msg_message"], parseInt(qs["msg_status"]));

                $("#subMenu li.li-listing").addClass("active");

                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) { }
                });

                //============================
                // Bind Staff
                //============================
                //BindStaffFilter();

                // View data yesterday
                $(".tag-date-today").click();

                //var qs = getQueryStrings();
                //showMsgSystem(qs["msg_update_message"], qs["msg_update_status"]);

                //for (var j = 0; j < listElement.length; j++) {
                //    var nameElement = listElement[j].ElementName;
                //    var enabled = listElement[j].Enable;
                //    var type = listElement[j].Type;
                //    if (type == "hidden" && enabled == true) {
                //        $("." + nameElement).addClass('hidden');
                //    }
                //    else {
                //        $("." + nameElement).prop('disabled', enabled);
                //    }
                //}
            });

            //function showMsgSystem(msg, status) {
            //    $("#MsgSystem").css("opacity", 0).text(msg).addClass(status).fadeTo(0, 1);
            //    setTimeout(function () {
            //        $("#MsgSystem").fadeTo("slow", 0, function () {
            //            $("#MsgSystem").text("").attr("class", "msg-system").css("opacity", 1);
            //        });
            //    }, 5000);
            //}

            //function showTrPrice() {
            //    $(".tr-price").show();
            //}

            //function BindStaffFilter() {
            //    $(".eb-select").bind("focus", function () {
            //        EBSelect_ShowBox($(this).parent().find(".eb-select-data"));
            //    });
            //    $(window).bind("click", function (e) {
            //        console.log(e);
            //        if ((!e.target.className.match("eb-select") && e.target.parentElement.className.match("ul-listing-staff")) ||
            //            (!e.target.className.match("eb-select") && !e.target.parentElement.className.match("ul-listing-staff"))
            //            && !e.target.className.match("mCSB_dragger_bar")) {
            //            EBSelect_HideBox();
            //        }
            //    });
            //    //============================
            //    // Scroll Staff Filter
            //    //============================
            //    $('.eb-select-data').each(function () {
            //        if ($(this).height() > 230) {
            //            $(this).mCustomScrollbar({
            //                theme: "dark-2",
            //                scrollInertia: 100
            //            });
            //        }
            //    });
            //}

            function BindIdToHDF(THIS, id, HDF_DomId, Input_DomId) {
                var text = THIS.innerText.trim(),
                    HDF_Dom = document.getElementById(HDF_DomId),
                    InputText = document.getElementById(Input_DomId);
                HDF_Dom.value = id;
                InputText.value = text;
                InputText.setAttribute("data-value", id);
                ajaxGetStaffsByType(id);
                if (HDF_DomId == "HDF_TypeStaff") {
                    $("#StaffName").val("");
                    $("#HDF_Staff").val("");
                }
            }

            function ajaxGetStaffsByType(type) {
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/Report/TimeKeeping.aspx/Load_Staff_ByType",
                    data: '{type : "' + type + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        var mission = JSON.parse(response.d);
                        if (mission.success) {
                            var staffs = JSON.parse(mission.msg);
                            if (staffs.length > 0) {
                                var tmp = "";
                                $.each(staffs, function (i, v) {
                                    tmp += '<li data-code="' + v.Code + '" data-id="' + v.Id + '"' +
                                        'onclick="BindIdToHDF(this,' + v.Id + ',\'HDF_Staff\',\'StaffName\')">' +
                                        v.Fullname + '</li>';
                                });
                                $("#UlListStaff").empty().append(tmp);
                            }

                        } else {
                            var msg = "Không tìm thấy mã khách hàng trong hệ thống.";
                            showMsgSystem(msg, "warning");
                        }
                    },
                    failure: function (response) { alert(response.d); }
                });
            }

            function EBSelect_ShowBox(Dom) {
                EBSelect_HideBox();
                Dom.show();
            }

            //function EBSelect_HideBox() {
            //    $(".eb-select-data").hide();
            //}

            //var orderIdCurrent, currentOrderDate;

            $(document).on("click", "#closeModal", function () {
                $("#viewModal").modal("hide");
            });

            function viewDetail(orderId,THIS) {
                debugger;
                var URL_INVENTORY = "<%= Libraries.AppConstants.URL_API_INVENTORY %>";
                $.ajax({
                    type: "GET",
                    url: URL_INVENTORY + "/api/order/detail?OrderId=" + orderId,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        debugger;

                        if (response.orderDetail != null && response.orderDetail.length > 0) {
                            //
                            debugger
                            $("#viewModal").modal();
                            $("#Note").text(THIS.parent().find("td.td-index").attr("data-note"));
                            
                            //
                            var index = 1;
                            $("#table-order-detail tbody").html("");
                            $.each(response.orderDetail, function (i, v) {
                                //
                                var tr = '<tr>' +
                                    '<td>' + index++ + '</td>' +
                                    '<td class="td-product-code maSPcol" data-id="' + v.id + '">' + v.productCode + '</td>' +
                                    '<td class="td-bar-code">' + v.barCode + '</td>' +
                                    '<td class="td-product-name">' + v.productName + '</td>' +                 
                                    '<td class="td-product-quantity map-edit">' + v.quantityType1 +
                                    //'<input class="product-quantity type1" disabled="disabled"  value="' + v.quantityType1 + '" type="text" placeholder="Số lượng"/>' +
                                    '</td>' +
                                    '<td class="td-product-quantity map-edit">' + v.quantityType2 +
                                    //'<input class="product-quantity type2" disabled="disabled"  value="' + v.quantityType2 + '" type="text" placeholder="Số lượng"/>' +
                                    '</td>' +
                                    '<td class="td-product-quantity map-edit">' + v.quantityType3 +
                                    //'<input class="product-quantity type3" disabled="disabled" value="' + v.quantityType3 + '" type="text" placeholder="Số lượng"/>' +
                                    '</td>' +
                                    '<td class="td-product-quantity map-edit">' + v.quantityType4 +
                                    //'<input class="product-quantity type4" disabled="disabled" value="' + v.quantityType4 + '" type="text" placeholder="Số lượng"/>' +
                                    '</td>' +
                                    '<td class="td-product-quantity map-edit">' + v.quantityType5 +
                                    //'<input class="product-quantity type5" disabled="disabled" value="' + v.quantityType5 + '" type="text" placeholder="Số lượng"/>' +
                                    '</td>' +
                                    '</td>' +
                                    '<td class="td-product-quantity map-edit">' + v.quantitySuggest +
                                    //'<input class="product-quantity suggest" disabled="disabled" value="' + v.quantitySuggest + '" type="text" placeholder="Số lượng"/>' +
                                    '</td>'
                                    + '<td class="td-product-quantity map-edit">' + v.quantityOrder +
                                    //'<input class="product-quantity total" disabled="disabled" value="' + v.quantityOrder + '" type="text" onkeypress="return isNumber(event)" placeholder="Số lượng"/>' +
                                    '</td>' +
                                    '<td class="td-product-quantity map-edit">' + v.quantityExport +
                                    //'<input class="product-quantity export" value="' + v.quantityExport + '" type="text" onkeypress="return isNumber(event)" placeholder="Số lượng"/>' +
                                    '</td>' +
                                    '<td class="td-product-quantity map-edit">' + v.quantityImport +
                                    //'<input class="product-quantity import" value="' + v.quantityImport + '" type="text" onkeypress="return isNumber(event)" placeholder="Số lượng"/>' +
                                    '</td>' +
                                    '</tr>';
                                $("#table-order-detail tbody").append($(tr));
                            });
                        }

                    },
                    error: function (error) {
                        console.log(error);
                    },
                    timeout: function (error) {
                        console.log(error);
                    }
                });
            }


            //============================
            // Event delete
            //============================
            function del(This, Id,name, e) {

                var Id = Id || null,
                    name = name || null,
                    Row = This;
                if (!Id) return false;

                // show EBPopup
                $(".confirm-yn").openEBPopup();
                $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn xóa [ " + name + " ] ?");

                $("#EBPopup .yn-yes").bind("click", function () {
                    debugger;
                    $.ajax({
                        type: "POST",
                        url: "/GUI/BackEnd/Permission/PermissionApiList.aspx/Delele_Api",
                        data: '{Id : ' + Id + ', UserId:' + $('#HDF_UserId').val() + '}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            //ShowMessage("Thông báo", response, 2);
                            debugger;
                            var mission = JSON.parse(response.d);
                            if (mission.success) {
                                if (mission.msg == "NotAllow") {
                                    ShowMessage("Thông báo", "Không được phép xóa bản ghi đã nhận hàng, chờ nhận hàng!!", 3);
                                }
                                else {
                                    ShowMessage("Thông báo", "Dữ liệu đã được xóa thành công!!", 2);

                                    //delSuccess();
                                    Row.remove();
                                    setTimeout(function () {
                                        $("#ViewData").click();
                                    }, 1000);
                                }

                            } else {
                                ShowMessage("Thông báo", "Xóa thất bại!!", 3);
                                //delFailed();
                            }
                            autoCloseEBPopup(0);
                        },
                        failure: function (response) { alert(response.d); }
                    });
                });
                $("#EBPopup .yn-no").bind("click", function () {
                    autoCloseEBPopup(0);
                });
                if (!e) var e = window.event;
                e.cancelBubble = true;
                if (e.stopPropagation) e.stopPropagation();
            }
         
        </script>

    </asp:Panel>
</asp:Content>




