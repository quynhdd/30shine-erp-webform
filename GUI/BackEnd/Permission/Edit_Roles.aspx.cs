﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Web.Services;
using static _30shine.GUI.BackEnd.Permission.Permission_Add_Roles;
using System.Data.Entity.Migrations;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;
using _30shine.Helpers;
using System.IO;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.Permission
{
    public partial class Edit_Roles : System.Web.UI.Page
    {
        private string PageID = "PQ_EDIT_ROLES";
        protected bool Perm_Access=false;
        protected List<Menu> menu = new List<Menu>();
        protected void Page_Load(object sender, EventArgs e)
        {
            var _Code = Request.QueryString["Code"];
            SetPermission();
            Session["Code"] = _Code;
            if (!IsPostBack)
            {
                GetMenu();
                getPermission();
                
            }
         
        }
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                var permission = Session["User_Permission"].ToString();
                Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);

                if (Request.QueryString["Code"] == "0" && permission != "root")
                {
                    Perm_Access = false;
                }
                //Perm_ShowSalon = permissionModel.GetActionByActionNameAndPageId("Perm_ShowSalon", PageID, permission);
                //Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //Perm_ViewAllDate = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllDate", PageID, permission);
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        [WebMethod]
        public static object GetChildMenuWithoutModule(int c)
        {
            try
            {
                Solution_30shineEntities db = new Solution_30shineEntities();
                List<cls_ChildMenu> lsChild = new List<cls_ChildMenu>();
                var lst = db.Store_Permission_GetMenu_Without_Module(c).ToList();
            
                foreach (var childmn in lst)
                {
                    cls_ChildMenu childMenu = new cls_ChildMenu();
                    childMenu.id = (int)childmn.mID;
                    childMenu.name = childmn.mName;
                    childMenu.link = childmn.Url_Rewrite;
                    childMenu.isPublish = (int)childmn.isPublish;
                    if (childMenu.isPublish == 1)
                    {
                        var a = db.Tbl_Permission_Map.FirstOrDefault(map => map.mID == childMenu.id && map.pID == c);
                        Int32[] array = a.aID.Split(',').Select(str => Convert.ToInt32(str)).ToArray();
                        childMenu.clsAction = db.Database.SqlQuery<cls_Action>("select ac.*, (CASE WHEN ac.aID IN (" + String.Join(",", array) + ") THEN 1 ELSE 0 END) as aPUBLISH from Tbl_Permission_Action ac").ToList();
                    }
                    else
                    {
                        childMenu.clsAction = db.Database.SqlQuery<cls_Action>("select ac.*, 0 as aPUBLISH from Tbl_Permission_Action ac").ToList();
                    }
                    lsChild.Add(childMenu);
                }
                return new JavaScriptSerializer().Serialize(lsChild);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        protected void GetMenu()
        {
            try
            {
                var _Code = Convert.ToInt32(Request.QueryString["Code"]);
                Solution_30shineEntities db = new Solution_30shineEntities();
                var prMenu = db.Tbl_Permission_Menu.Where(m=> m.mParentID == 0 && m.mParentID != null).ToList();
                foreach (var item in prMenu)
                {
                    Menu _menu = new Menu();
                    _menu.mID = item.mID;
                    _menu.mName = item.mName;
                    _menu.mClassTag = item.mClassTag;
                    var childMenu = db.Store_Permission_Get_Menu(item.mID,_Code).ToList();
                    _menu.lstMenu = new List<cls_ChildMenu>();
                    foreach (var child in childMenu)
                    {
                        cls_ChildMenu __menu = new cls_ChildMenu();
                        __menu.id = (int)child.id;
                        __menu.name = child.name;
                        __menu.link = child.link;
                        __menu.isPublish = (int)child.isPublish;
                        if (__menu.isPublish == 1)
                        {
                            var a = db.Tbl_Permission_Map.FirstOrDefault(map => map.mID == __menu.id && map.pID == _Code);
                            if (a.aID !=null)
                            {
                                Int32[] array = a.aID.Split(',').Select(str => Convert.ToInt32(str)).ToArray();
                                __menu.clsAction = db.Database.SqlQuery<cls_Action>("select ac.*, (CASE WHEN ac.aID IN (" + String.Join(",", array) + ") THEN 1 ELSE 0 END) as aPUBLISH from Tbl_Permission_Action ac").ToList();
                            }
                            else
                            {
                                __menu.clsAction = db.Database.SqlQuery<cls_Action>("select ac.*, 0 as aPUBLISH from Tbl_Permission_Action ac").ToList();
                            }
                            
                        }
                        else
                        {
                            __menu.clsAction = db.Database.SqlQuery<cls_Action>("select ac.*, 0 as aPUBLISH from Tbl_Permission_Action ac").ToList();
                        }
                        _menu.lstMenu.Add(__menu);
                    }
                    menu.Add(_menu);
                }
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        protected void getPermission()
        {
            Solution_30shineEntities db = new Solution_30shineEntities();
            var p = db.Store_Permission_Selected(Convert.ToInt32(Request.QueryString["Code"].ToString())).Where(s => s.SELECTED == 1).ToList();
            foreach (var item in p)
            {
                ddlPermission.Items.Add(new ListItem(item.pName, item.pID.ToString()));
            }
            ddlPermission.DataBind();
        }

        [WebMethod(EnableSession =true)]
        public static string AddOrUpdate_Menu(List<cls_Add_Menu> list)
        {
            var ss = Convert.ToInt32(HttpContext.Current.Session["Code"]);
            var jsonData = "";
            try
            {
                Solution_30shineEntities db = new Solution_30shineEntities();
                var s = db.Tbl_Permission.FirstOrDefault(p => p.pID == ss);
                var del = db.Database.ExecuteSqlCommand("delete from Tbl_Permission_Map where pID = {0}", ss);
                if (del>= 0)
                {
                    for (int i = 0; i < list.Count; i++)
                    {
                        Tbl_Permission_Map map = new Tbl_Permission_Map();
                        map.pID = list[i].p_Id;
                        if (ss == 0)
                        {
                            map.pName = "root";
                        }
                        else
                        {
                            map.pName = s.pName;
                        }
                        
                        map.mID = list[i].m_Id;
                        map.mName = list[i].m_Name;
                        map.aID = list[i].action;
                        map.mapPublish = true;
                        jsonData +=  Environment.NewLine + new JavaScriptSerializer().Serialize(map);
                        db.Tbl_Permission_Map.AddOrUpdate(map);
                        db.SaveChanges();
                    }
                    try
                    {
                        var perName = "";
                        if (ss == 0)
                        {
                            perName = "root";
                        }
                        else
                        {
                            perName = s.pName;
                        }


                        using (StreamWriter log =
                            new StreamWriter(HttpContext.Current.Server.MapPath("/Config/log_permission.txt"),
                                true))
                        {
                            var logs = Environment.NewLine + DateTime.Now.ToString() + ": User " +
                                       HttpContext.Current.Session["User_Email"] + " đã chỉnh sửa nhóm quyền " +
                                       perName;
                            logs += Environment.NewLine +  jsonData;
                            log.WriteLine(logs);
                            log.Close();
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }
                return "<span>Cập nhật thành công</span>";
            }
            catch (Exception ex)
            {
                return "<span>Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển</span>";
                throw new Exception(ex.Message);
            }

        }
        public class Menu
        {
            public int mID { get; set; }
            public string mName { get; set; }
            public string mClassTag { get; set; }
            public string Url_Rewrite { get; set; }
            //public List<cls_Action> Action { get; set; }
            public List<cls_ChildMenu> lstMenu { get; set; }
            public string action { get; set; }

        }
        public class cls_Action
        {
            public int aID { get; set; }
            public string aName { get; set; }
            public int aPUBLISH { get; set; }
            public string aDescription { get; set; }
        }
        public class cls_ChildMenu
        {
            public int id { get; set; }
            public string name { get; set; }
            public string link { get; set; }
            public int isPublish { get; set; }
            public List<cls_Action> clsAction {get;set;}

        }
        public class cls_Add_Menu : Tbl_Permission_Map
        {
            public int p_Id { get; set; }
            public string p_Name { get; set; }
            public int m_Id { get; set; }
            public string m_Name { get; set; }
            public string m_Url { get; set; }
            public string action { get; set; }
            public bool mPublish { get; set; }
        }
    }
}