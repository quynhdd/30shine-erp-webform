﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="PermissionActionAdd.aspx.cs" Inherits="_30shine.GUI.BackEnd.Permission.PermissionActionAdd" %>

<asp:Content ID="PermissionAction" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <style>
            #success-alert {
                top: 100px;
                right: 10px;
                position: fixed;
                width: 20% !important;
                z-index: 2000;
            }

            #MsgSystem {
                color: green;
                font-size: 16px;
            }
            .box-btn{
                margin-top:30px;
            }
            #DescriptionAction{
                padding-top:6px;
            }
        </style>

        <div class="alert alert-danger" id="success-alert">
            <span id="msg-alert"></span>
        </div>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                    <li>Quản lý action &nbsp;&#187;</li>
                        <li class="li-listing active"><a href="/admin/phan-quyen/danh-sach-action-V2.html">Danh sách action</a></li>
                        <li class="li-add"><a href="/admin/phan-quyen/them-moi-action-v2.html">Thêm mới</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add">
            <%-- Add --%>
            <div class="wp960 content-wp">
                <div class="table-wp">
                    <table class="table-add permission-action-table-add">
                        <tbody>
                            <tr class="title-head">
                                <td><strong>Thông tin Action</strong></td>
                                <td></td>
                            </tr>
                            <hr />
                            <tr>
                                <td class="col-xs-3 left"><span>Tên Action:</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp ">
                                        <asp:TextBox ID="NameAction" onkeyup="checkColor();checkDuplicate();" runat="server" CssClass="form-control" ClientIDMode="Static" placeholder="Nhập tên action..." Width="70%"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-3 left"><span>Mô tả</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="DescriptionAction" TextMode="multiline" Columns="50" Rows="5" CssClass="form-control"  placeholder="Nhập mô tả..." runat="server" ClientIDMode="Static" Width="70%"></asp:TextBox>
                                    </span>
                                    <span style="clear:both"></span>
                                </td>
                            </tr>
                            <tr class="tr-send ">
                                <td class="col-xs-3 left"></td>
                                <td class="col-xs-9 right no-border">
                                    <span class="field-wp">
                                        <asp:Panel ID="btnAdd" CssClass="btn-send box-btn" runat="server" ClientIDMode="Static" onclick="Add()">Hoàn tất</asp:Panel>
                                        <asp:Button ID="btnAdd_hidden" ClientIDMode="Static" runat="server" OnClick="AddOrUpdate" Text="Click" Style="display: none;" />
                                    </span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <script>
            var kt = true;
            function checkDuplicate() {
                var Id = <%= Convert.ToInt32(Request.QueryString["Code"]) >0 ? Convert.ToInt32(Request.QueryString["Code"]) : 0%>;
                var nameAction = $("#NameAction").val();
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    async: false,
                    datatype: "JSON",
                    url: "/GUI/BackEnd/Permission/PermissionActionAdd.aspx/checkDuplicate",
                    data: '{name:"' + nameAction + '", id: ' + Id + '}',
                    success: function (response) {
                        if (response.d.success == true) {
                            $("#success-alert").fadeTo(2500, 2500).slideUp(2500, function () {
                                $("#success-alert").slideUp(2500);
                            });
                            $("#msg-alert").text("Tên Action đã tồn tại. Vui lòng chọn tên Action khác!!!");
                            $("#NameAction").css("border-color", "red");
                            kt = false;
                        }
                    }
                });
                return kt;
            }

            $("#success-alert").hide();
            function checkColor() {
                $("#NameAction").css("border-color", "#ddd");
            }
            // validate
            function Add() {
                kt = true;
                var i = 0;
                var error = 'Vui lòng nhập đầy đủ thông tin!';
                var name = $('#NameAction').val();
                if (name == "") {
                    kt = false;
                    error += "[Tên Action]";
                    $("#NameAction").css("border-color", "red");
                }
                if (kt == false) {
                    $("#success-alert").fadeTo(2500, 2500).slideUp(2500, function () {
                        $("#success-alert").slideUp(2500);
                    });
                    $("#msg-alert").text(error);
                    i++;
                }
                if (i == 0) {
                    var check = checkDuplicate();
                    if (check == false) {
                        $("#success-alert").fadeTo(2500, 2500).slideUp(2500, function () {
                            $("#success-alert").slideUp(2500);
                        });
                        $("#msg-alert").text("Tên Action đã tồn tại. Vui lòng chọn tên Action khác!!!");
                        $("#NameAction").css("border-color", "red");
                    }
                    else {
                        //chạy hàm thêm AddOrUpadte trong codebehind
                        addLoading();
                        $('#btnAdd_hidden').click();
                        $("#success-alert").fadeTo(2500, 2500).slideUp(2500, function () {
                            $("#success-alert").slideUp(2500);
                        });
                        $("#msg-alert").text("Hoàn tất thành công!!!");
                    }
                }
            }
        </script>
    </asp:Panel>
</asp:Content>

