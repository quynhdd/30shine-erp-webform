﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="List_Action.aspx.cs" Inherits="_30shine.GUI.BackEnd.Permission.List_Action" %>

<asp:Content ID="ListtingContent" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý action &nbsp;&#187; </li>
                        <li class="li-listing active"><a href="/admin/phan-quyen/danh-sach-action.html">Danh sách các action</a></li>
                        <li class="li-add"><a href="/admin/phan-quyen/them-moi-action.html">Thêm mới</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="wp customer-add customer-listing">
            <%-- Listing --%>
           <div class="wp960 content-wp">
                <!-- Row Table Filter
                <div class="table-func-panel">
                    <div class="table-func-elm">
                        <span>Số hàng / Page : </span>
                        <div class="table-func-input-wp">
                            <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                            <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                            <ul class="ul-opt-segment">
                                <li data-value="10">10</li>
                                <li data-value="20">20</li>
                                <li data-value="30">30</li>
                                <li data-value="40">40</li>
                                <li data-value="50">50</li>
                                <li data-value="1000000">Tất cả</li>
                            </ul>
                            <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                        </div>
                    </div>
                </div>
                <!-- End Row Table Filter -->
                <asp:ScriptManager runat="server" ID="ScriptManager1"></asp:ScriptManager>
                <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                    <ContentTemplate>
                        <div class="table-wp">

                            <table class="table" id="tableAction" cell-spacing="0">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Tên</th>
                                        <th>Mô tả</th>
                                        <th>Ẩn/Hiện</th>
                                        <%--<th>Chức năng</th>--%>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater runat="server" ID="rptAction">
                                        <ItemTemplate>
                                            <tr>
                                                <td><%#Container.ItemIndex+1 %></td>
                                                <td><%#Eval("aName") %> </td>
                                                <td><%#Eval("aDescription") %> </td>
                                                <td>
                                                    <input type="checkbox" <%#Convert.ToInt32(Eval("aPublish"))==1? "Checked=\"True\"":"" %> class="chkPublish" data-id="<%#Eval("aID") %>" />
                                                </td>
                                                <%--<td class="map-edit">
                                            <div class="edit-wp">
                                                <button type='button' class='btn btn-info btn-xs btn-Edit' data-id="<%#Eval("aID") %>" style='margin-right: 10px'>Sửa</button>
                                            </div>
                                        </td>--%>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>
                        <!-- Paging -->
                        <%--<div class="site-paging-wp">
                            <% if (PAGING.TotalPage > 1)
                                        { %>
                            <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                                <% if (PAGING._Paging.Prev != 0)
                                            { %>
                                <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                                <% } %>
                                <asp:Repeater ID="RptPaging" runat="server">
                                    <ItemTemplate>
                                        <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                            <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>>
                                            <%# Eval("PageNum") %>
                                        </a>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                                            { %>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                                <% } %>
                            </asp:Panel>--%>
                            <%--<% } %>--%>
                        </div>
                    </ContentTemplate>
                    </asp:UpdatePanel>
            </div>
        </div>
        <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
        <script>
            $(document).ready(function () {
                $("#tableAction").DataTable();
                $(document).on("click", ".btn-Edit", function () {
                    //debugger;
                    var aID = $(this).attr("data-id");
                    document.location = "/admin/phan-quyen/sua-action/" + aID + ".html";
                })
            })
            $(document).on("change", ".chkPublish", function (e) {
                var Id = $(this).attr("data-id");
                var isPublish
                if (this.checked)
                    isPublish = 1;
                else
                    isPublish = 0;
                $.ajax({
                    type: "POST",
                    contentType: "application/json;charset=UTF-8",
                    data: '{Id:' + Id + ',isPublish:' + isPublish + '}',
                    dataType: "JSON",
                    url: "/GUI/BackEnd/Permission/List_Action.aspx/Publish",
                    success: function (data) {
                        alert("Thay đổi trạng thái thành công!");
                    },
                    error: function (result) {
                        alert("Có lỗi xảy ra! Vui lòng liên hệ với nhóm phát triển!");
                    }
                });
            });
        </script>
    </asp:Panel>
</asp:Content>

