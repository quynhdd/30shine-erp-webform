﻿﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.GUI.SystemService.Webservice;
using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;

namespace _30shine.GUI.BackEnd.Permission
{
    public partial class Edit_Roles_V3 : System.Web.UI.Page
    {
        private string PageID = "PQ_EDIT_ROLES";
        private bool Perm_Access = false;
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                var permission = Session["User_Permission"].ToString();
                Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            string Code = Request.QueryString["Code"];
            Session["Code"] = Code;
            if (!IsPostBack)
            {
                Bind_Module();
                Bind_Permission();
            }
        }
        /// <summary>
        /// bind dropdownlist ddlModule
        /// </summary>
        protected void Bind_Module()
        {
            Solution_30shineEntities db = new Solution_30shineEntities();
            ddlModule.Items.Add(new ListItem("Chọn Module", "0"));
            ddlModule.SelectedIndex = 0;
            var ListModule = db.Tbl_Permission_Menu.Where(m => m.mParentID == 0).ToList();
            foreach (var v in ListModule)
            {
                ddlModule.Items.Add(new ListItem(v.mName, v.mID.ToString()));
            }
            ddlModule.DataBind();
        }
        protected void Bind_Permission()
        {
            ddlPermission.Enabled = false;
            Solution_30shineEntities db = new Solution_30shineEntities();
            ddlPermission.Items.Add(new ListItem("Chọn Nhóm quyền", "0"));
            var listPermission = db.Tbl_Permission.Where(p => p.pPublish == true).ToList();
            foreach (var v in listPermission)
            {
                ddlPermission.Items.Add(new ListItem(v.pName, v.pID.ToString()));
            }
            ddlPermission.SelectedValue = Request.QueryString["Code"];
            ddlPermission.DataBind();
        }
        /// <summary>
        /// get all Module and Menu of Module
        /// </summary>
        /// <param name="moduleId"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public static object GetModule(int moduleId)
        {
            try
            {
                var pID = Convert.ToInt32(HttpContext.Current.Session["Code"].ToString());
                Solution_30shineEntities db = new Solution_30shineEntities();
                var list = db.Store_Permission_Get_Menu(moduleId, pID).ToList();
                List<clsMenu> clsMenus = new List<clsMenu>();
                foreach (var v in list)
                {
                    clsMenu _clsMenu = new clsMenu();
                    _clsMenu.Id = (int)v.id;
                    _clsMenu.MenuName = v.name;
                    _clsMenu.UrlRewrite = v.link;
                    _clsMenu.Isset = (int)v.isPublish;
                    _clsMenu.ClsActions = new List<clsAction>();
                    var action = db.Store_Permission_Get_Action_V2(_clsMenu.Id, pID).ToList();
                    foreach (var a in action)
                    {
                        clsAction clsAction = new clsAction();
                        clsAction.ActionId = a.aID;
                        clsAction.ActionName = a.aName;
                        clsAction.ActionDesc = a.aDescription;
                        clsAction.Isset = a.isset;
                        _clsMenu.ClsActions.Add(clsAction);
                    }
                    clsMenus.Add(_clsMenu);
                }
                return new JavaScriptSerializer().Serialize(clsMenus);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// set full Action, Menu for a Permission
        /// </summary>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public static string SetFullPermission()
        {
            var _Code = Convert.ToInt32(HttpContext.Current.Session["Code"]);
            Solution_30shineEntities db = new Solution_30shineEntities();
            var perm = db.Tbl_Permission.FirstOrDefault(p => p.pID == _Code);
            var action = db.Tbl_Permission_Action.ToList();
            var query = 0;
            var delete = db.Database.ExecuteSqlCommand("DELETE from Tbl_Permission_Map where pID = {0}", _Code);
            if (delete >= 0)
            {
                foreach (var a in action)
                {
                    query = db.Database.ExecuteSqlCommand(
                        "INSERT INTO Tbl_Permission_Map (pID,pName,mID,mName,aId,mapPublish,ModifineDate) select " + _Code + ",'" +
                        perm.pName + "',m.mID,m.mName," + a.aID + ",1,GETDATE() from Tbl_Permission_Menu m");
                }
            }
            else
            {
                //
            }
            if (query > 0)
            {
                return new JavaScriptSerializer().Serialize("<span style='color:green'>Cập nhật thành công</span>");
            }
            else
            {
                return new JavaScriptSerializer().Serialize(
                    "<span style='color:red'>Cập nhật thất bại. Vui lòng liên hệ bộ phận phát triển</span>");
            }
        }
        /// <summary>
        /// Insert or Update data in Tbl_Permission_Map
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public static string ChangeMenu(List<clsAddMenu> list)
        {
            var _Code = Convert.ToInt32(HttpContext.Current.Session["Code"]);
            var result = 0;
            var jsonData = "";
            try
            {
                Solution_30shineEntities db = new Solution_30shineEntities();
                if (list != null)
                {
                    var delete = db.Database.ExecuteSqlCommand("UPDATE Tbl_Permission_Map set mapPublish = 0 where pID = {0}", _Code);
                    if (delete >= 0)
                    {
                        foreach (var v in list)
                        {

                            Tbl_Permission_Map m = new Tbl_Permission_Map();
                            m.pID = v.pID;
                            m.pName = v.pName;
                            m.mID = v.mID;
                            m.mName = v.mName;
                            m.aID = v.aID;
                            m.ModifiedDate = DateTime.Now;
                            m.mapPublish = true;
                            jsonData += Environment.NewLine + new JavaScriptSerializer().Serialize(m);
                            db.Tbl_Permission_Map.Add(m);
                            result = db.SaveChanges();
                        }
                        // lưu log ra file txt
                        var pername = (from p in db.Tbl_Permission where p.pID == _Code select new { p.pName })
                            .FirstOrDefault();
                        using (StreamWriter log =
                            new StreamWriter(HttpContext.Current.Server.MapPath("/Config/log_permission.txt"), true))
                        {
                            var logs = Environment.NewLine + DateTime.Now.ToString() + ": User " +
                                       HttpContext.Current.Session["User_Email"] + " đã chỉnh sửa nhóm quyền " +
                                       pername.pName;
                            logs += Environment.NewLine + jsonData;
                            log.WriteLine(logs);
                            log.Close();
                        }
                    }
                    else
                    {
                        return new JavaScriptSerializer().Serialize("<span style='color:green'>Cập nhật thất bại. Vui lòng liên hệ bộ phận phát triển</span>");
                    }

                }
                if (result > 0)
                {
                    return new JavaScriptSerializer().Serialize("<span style='color:green'>Cập nhật thành công</span>");
                }
                else
                {
                    return new JavaScriptSerializer().Serialize("<span style='color:green'>Cập nhật thất bại. Vui lòng liên hệ bộ phận phát triển</span>");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public class clsMenu
        {
            public int Id { get; set; }
            public string MenuName { get; set; }
            public string UrlRewrite { get; set; }
            public int ParentID { get; set; }
            public int Isset { get; set; }
            public List<clsAction> ClsActions;
        }
        public class clsAction
        {
            public int ActionId { get; set; }
            public string ActionName { get; set; }
            public string ActionDesc { get; set; }
            public int Isset { get; set; }
        }

        public class clsAddMenu
        {
            public int pID { get; set; }
            public string pName { get; set; }
            public int mID { get; set; }
            public string mName { get; set; }
            public string mLink { get; set; }
            public string aID { get; set; }
            public bool mPublish { get; set; }
        }

    }
}