﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/TemplateMaster/SiteMaster.Master" CodeBehind="Permission_Add_Roles.aspx.cs" Inherits="_30shine.GUI.BackEnd.Permission.Permission_Add_Roles" %>
<asp:Content ID="PermissionAdd" ContentPlaceHolderID="CtMain" runat="server">
    <link href="/Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="/Assets/js/select2/select2.min.js"></script>
    <style>
       #child-menu td span{
            height: auto !important; 
            line-height: 20px !important; 

        }
        #add_Menu{
            width:auto !important;
            margin:0 !important;
            line-height:0 !important;
        }
        .btn-send{
            padding:6px 20px;
            margin-left:20px;
            background:#000000;
            color:white;
            border:none;
            width:auto;
            line-height:24px;
        }
        .btn-send>i{
            color:#fff;
        }
         .btn-send:hover{
             color:yellow;

        }
         .dropdown-wrapper{
             width:80% !important;
         }
         #child-menu{
             text-align:center !important;
         }
    </style>
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <div class="wp sub-menu">
            <div class="wp960">

                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý nhóm quyền &nbsp;&#187; </li>
                        <li class="li-listing"><a href="/admin/phan-quyen/danh-sach-phan-quyen.html">Danh sách</a></li>
                        <li class="li-add active"><a href="/admin/phan-quyen/them-moi-quyen.html">Thêm mới</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="wp customer-add admin-product-add fe-service">
            <div class="wp960 content-wp">
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <div class="table-wp">
                    <table class="table-add admin-product-table-add fe-service-table-add">
                        <tbody>
                            <tr class="title-head">
                                <td>
                                    <strong>Thông tin nhóm quyền</strong>
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="col-xs-2-left">
                                    <span>Tên nhóm quyền</span>
                                </td>
                                <td class="col-xs-9-right">
                                    <asp:DropDownList ID="ddlPermission" runat="server" ClientIDMode="Static" CssClass="form-control"></asp:DropDownList>
   
                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-2-left">
                                    <span>Tên Module</span>
                                </td>
                                <td class="col-xs-9-right">
                                    <asp:DropDownList ID="ddlModule" runat="server" ClientIDMode="Static" CssClass="form-control" Width="80%"></asp:DropDownList>
                                    <button type="button" class="btn-send" id="getMenu">Thêm</button>
                                </td>
                      
                            </tr>
                            <tr>
                                <td class="col-xs-2-left">
                                    <span id="menuLabel">Menu</span>
                                </td>
                                <td class="col-xs-9-right">
                                    <table id="child-menu" class="table table-bordered">
                                        <thead>
                                            <tr>
                                            <th style="text-align:center">ID</th>
                                            <th style="text-align:center">Tên Menu</th>
                                            <th style="text-align:center">Action</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </td>
                            </tr>
                            
                            <tr class="tr-send">
                                <td class="col-xs-2 left"></td>
                                <td class="col-xs-9 right no-border">
                             
                                        <button type="button" class="btn-send" id="add_Menu" onclick="get_Menu_Id()">Xác nhận</button>
                       
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


    
        <script>
            
            $("#glbTuyendungV2").addClass("active");
            $('select').select2();
            $("#ddlMainUrl").attr("disabled", "disabled");
        </script>
        <script>

            $("#child-menu").hide();
            $("#menuLabel").hide();
            $(document).on("click", "#getMenu", function (e) {
 
                if ($("#ddlModule").val() != 99) {
                    $("#child-menu").show();
                    $("#menuLabel").show();
                    getChildMenu($("#ddlModule").val(), $("#child-menu tbody"));
                }
                else {
                    alert("Bạn chưa chọn Module cần thêm");
                }
                e.preventDefault();
            })
            function getChildMenu(menuID, parent) {
                $.ajax({
                    url: "/GUI/BackEnd/Permission/Permission_Add_Roles.aspx/getChildMenu",
                    type: "POST",
                    data: "{mID:" + menuID + "}",
                    contentType: "application/JSON; charset:UTF-8",
                    datatype: "JSON",
                    success: function (data) {
                        jsondt = JSON.parse(data.d);
                        for (var i = 0; i < jsondt.length; i++) {
                            var tr = $("<tr id="+jsondt[i].id+"></tr>");
                            var menuId = $("<td class='mID'>" + jsondt[i].id + "</td><td class='mName'>" + jsondt[i].name + "</td>");
                            var add = $("<td class='action' style='border:none !important;border-bottom:1px solid #ddd !important;'></td>");
                            //var ac = null;
                            
                            getAction(add);
                            //console.log(add.html);
                            var delrow = $("<td class='delrow'><button type='button' class='btn-send del_row' mID ='"+jsondt[i].id+"' id='deleteRow' style='line-height:10px'><i class='fa fa-trash-o' aria-hidden='true'></i>&nbspXóa</button></td>");
                            tr.append(menuId,add,delrow);
                            parent.append(tr);
                        }
                        
                    }
                })
            }

            function getAction(td) {
                $.ajax({
                    url: "/GUI/BackEnd/Permission/Permission_Add_Roles.aspx/getAction",
                    type: "POST",
                    //data: "{mID:" + menuID + "}",
                    contentType: "application/JSON; charset:UTF-8",
                    datatype: "JSON",
                    success: function (response) {
                        datajs = JSON.parse(response.d);
                        var sl = $("<select class='slAction' multiple='multiple'></select>");
                        for (var j = 0; j < datajs.length; j++) {
                            //console.log(datajs[j]);
                            var ac = $("<option value='" + datajs[j].aID + "'>" + datajs[j].aName + "</option>");
                            sl.append(ac);
                            
                        }
                        
                        td.append(sl);
                        $(".slAction").multiselect({
                            includeSelectAllOption: true
                        })
                    }

                })
               
            }
            $(document).on("click", ".del_row", function () {
                var m_ID = $(this).attr("mid");
                $.ajax({
                    type: "POST",
                    contentType:"application/json;charset:UTF-8",
                    datatype:"JSON",
                    url: "/GUI/BackEnd/Permission/Permission_Add_Roles.aspx/getChildMenu",
                    data: "{mID:" + m_ID + "}",
                    success: function (data) {
                        //debugger;
                        if (data.d.length == 2) {
              
                           $("#" + m_ID).remove();
                        }
                        else {
                            jsdt = JSON.parse(data.d);
                            for (var i = 0; i < jsdt.length; i++) {
                                $("#" + jsdt[i].id).remove();
                            }
                        }
                        if ($("#child-menu tbody tr").length == 0) {
                            $("#child-menu").hide();
                            $("#menuLabel").hide();
                        }
                    }
                })
                return false;
            })
            function get_Menu_Id() {
                if ($("#ddlPermission").val()==99 ||$("#ddlModule").val()==99 ) {
                    alert("Bạn phải chọn nhóm quyền cần phân và thêm các Menu cho nhóm quyền đó");
                }
                else{
          
                    var arr_Menu = [];
                
                    //console.log(arr_Menu);
                    if ($("#child-menu tbody tr").length > 0) {
                        $("#child-menu tbody tr").each(function () {
                            var m_id = $(this).find(".mID").text();
                            var m_name = $(this).find(".mName").text();
                            var p_Id = $("#ddlPermission").val();
                            var p_Name = $("#ddlPermission option:selected").text();
                            var m_action = $(this).find('.action').find(".slAction").val();
                            if (m_action != null) {
                                var action = m_action.join();
                            }
                            else {
                                action = null;
                            }
                            arr_Menu.push({ p_Id,p_Name,m_id, m_name,action});

                            });
                   
                        $.ajax({
                            type: "POST",
                            contentType: "application/json;charset:UTF-8",
                            datatype: "JSON",
                            url: "/GUI/BackEnd/Permission/Permission_Add_Roles.aspx/addMenu",
                            data: "{list:" + JSON.stringify(arr_Menu) + "}",
                            success: function (data) {
                                console.log("abc");
                            }
                        })
                        console.log(JSON.stringify(arr_Menu));
                    }
                    else {
                        alert("Bạn chưa chọn menu để phân quyền");
                    }
                }
            }
        </script>
</asp:Panel>
</asp:Content>
