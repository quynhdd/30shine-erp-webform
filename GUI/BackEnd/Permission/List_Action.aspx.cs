﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Web.Services;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.Permission
{
    public partial class List_Action : System.Web.UI.Page
    {
        protected Paging PAGING = new Paging();
        private string PageID = "PQ_ACTION";
        private bool Perm_Access = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                //Bind_Paging();
                Bind();
                RemoveLoading();
                
            }     
        }
        #region[Bind]
        private void Bind()
        {
            using (var db = new Solution_30shineEntities())
            {
                var list = db.Tbl_Permission_Action.OrderBy(w => w.aID).ToList()/*.Skip(PAGING._Offset).Take(PAGING._Segment).ToList()*/;
                if (list.Count > 0)
                {
                    rptAction.DataSource = list;
                    rptAction.DataBind();
                }
            }
        }
        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }
        //protected void Bind_Paging()
        //{
        //    // init Paging value            
        //    PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
        //    PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
        //    PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
        //    PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage() : PAGING.TotalPage;
        //    PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
        //    PAGING._Paging = PAGING.Make_Paging();

        //    RptPaging.DataSource = PAGING._Paging.ListPage;
        //    RptPaging.DataBind();
        //}


        //protected int Get_TotalPage()
        //{
        //    using (var db = new Solution_30shineEntities())
        //    {
               
        //        var lstAction = db.Tbl_Permission_Action.ToList();
        //        var Count = lstAction.Count();
        //        int TotalRow = Count - PAGING._TopNewsNum;
        //        int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
        //        return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        //    }
        //}
        #endregion
        #region[Status]
        [WebMethod]
        public static void Publish(int Id, int isPublish)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var _obj = db.Tbl_Permission_Action.FirstOrDefault(w => w.aID == Id);
                    _obj.aPublish = Convert.ToBoolean(isPublish);
                    db.Tbl_Permission_Action.AddOrUpdate(_obj);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
              
                IPermissionModel permissionModel = new PermissionModel();
                var permission = Session["User_Permission"].ToString();
                Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_ShowSalon = permissionModel.GetActionByActionNameAndPageId("Perm_ShowSalon", PageID, permission);
                //Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //Perm_ViewAllDate = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllDate", PageID, permission);
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
    }
}