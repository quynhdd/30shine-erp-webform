﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;

namespace _30shine.GUI.BackEnd.Permission
{
    public partial class PermissionSalonArea_Add : System.Web.UI.Page
    {
        protected bool Perm_Access = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {

            }
        }

        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        /// <summary>
        /// Get list nhân viên thuộc bộ phận ASM với Type = 17
        /// author: QUYNHDD
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public static object GetStaff(int id)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            Staff data = new Staff();
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    data = (from c in db.Staffs
                            where c.Id == id && c.IsDelete == 0 && c.Active == 1
                            select c
                            ).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return serializer.Serialize(data);
        }

        /// <summary>
        ///  Get list Salon
        ///  author: QUYNHDD
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public static object GetListSalon()
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            List<Tbl_Salon> data = new List<Tbl_Salon>();
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    data = (from c in db.Tbl_Salon.AsNoTracking()
                            where c.IsDelete == 0 && c.Publish == true
                            select c
                            ).ToList();
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

            return serializer.Serialize(data);
        }

        /// <summary>
        /// AddPermissionSalonArea 
        /// author: QUYNHDD
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public static object Add(int staffId, List<Input_Salon> Salon)
        {
            try
            {
                var message = new Library.Class.cls_message();
                var exc = 0;
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                if (staffId < 0 || Salon == null)
                {
                    throw new Exception();
                }
                // Xử lý chuỗi string Salon truyền vào
                //var inputSalonId = serializer.Deserialize<List<Input_Salon>>(Salon);
                using (var db = new Solution_30shineEntities())
                {
                    if (staffId > 0 && Salon.Count > 0)
                    {
                        foreach (var v in Salon)
                        {
                            // check xem salon da ton tai chua.
                            var data = db.PermissionSalonAreas.Where(r => r.StaffId == staffId && r.SalonId == v.salonId).FirstOrDefault();
                            if (data != null)
                            {
                                data.IsDelete = false;
                                data.IsActive = true;
                            }
                            else
                            {
                                var obj = new PermissionSalonArea();
                                obj.StaffId = staffId;
                                obj.SalonId = v.salonId;
                                obj.IsActive = true;
                                obj.IsDelete = false;
                                obj.CreatedTime = DateTime.Now;
                                db.PermissionSalonAreas.Add(obj);
                            }
                        }

                        exc = db.SaveChanges();
                    }
                    if (exc < 0)
                        message.success = false;
                    else
                        message.success = true;
                }
                return message;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Update PermissionSalonArea
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public static object Update(int staffId, List<Input_Salon> Salon)
        {
            try
            {
                var message = new Library.Class.cls_message();
                if (Salon.Count > 0)
                {
                    using (var db = new Solution_30shineEntities())
                    {
                        var list = db.PermissionSalonAreas.Where(w => w.StaffId == staffId).ToList();
                        var index = -1;
                        var item = new PermissionSalonArea();
                        // kiểm tra xóa Item
                        if (list.Count > 0)
                        {
                            foreach (var v in list)
                            {
                                index = Salon.FindIndex(w => w.salonId == v.SalonId);
                                if (index == -1)
                                {
                                    v.IsDelete = true;
                                    v.ModifiedTime = DateTime.Now;
                                    db.SaveChanges();
                                    message.success = true;
                                }
                                else
                                {
                                    v.ModifiedTime = DateTime.Now;
                                    v.IsDelete = false;
                                    db.SaveChanges();
                                    message.success = true;
                                }
                            }
                        }
                        // kiểm tra thêm mới Item
                        if (Salon.Count > 0)
                        {
                            foreach (var v in Salon)
                            {
                                index = list.FindIndex(w => w.SalonId == v.salonId);
                                if (index == -1)
                                {
                                    item = new PermissionSalonArea();
                                    item.StaffId = staffId;
                                    item.SalonId = v.salonId;
                                    item.IsActive = true;
                                    item.IsDelete = false;
                                    item.CreatedTime = DateTime.Now;
                                    db.PermissionSalonAreas.Add(item);
                                }

                                db.SaveChanges();
                                message.success = true;
                            }

                        }

                    }
                }

                return message;
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Input SalonID
        /// </summary>
        public class Input_Salon
        {
            public int salonId { get; set; }
        }
    }
}