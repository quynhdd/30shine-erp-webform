﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Web.Services;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.Permission
{
    public partial class List_Url : System.Web.UI.Page
    {
        private string PageID = "PQ_MENU";
        private bool Perm_Access = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
        }
        #region[Publish]
        [WebMethod]
        public static void Publish(int Id, int isPublish)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var _obj = db.Tbl_Permission_Menu.FirstOrDefault(w => w.mID == Id);
                    _obj.mPublish = Convert.ToBoolean(isPublish);
                    db.Tbl_Permission_Menu.AddOrUpdate(_obj);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        #region[ShowMenu]
        /// <summary>
        /// show hide on Menu
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="showMenu"></param>
        [WebMethod]
        public static void ShowMenu(int Id, int showMenu)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var _obj = db.Tbl_Permission_Menu.FirstOrDefault(w => w.mID == Id);
                    _obj.MenuShow = Convert.ToBoolean(showMenu);
                    db.Tbl_Permission_Menu.AddOrUpdate(_obj);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion

        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                IPermissionModel permissionModel = new PermissionModel();
                var permission = Session["User_Permission"].ToString();
                Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_ShowSalon = permissionModel.GetActionByActionNameAndPageId("Perm_ShowSalon", PageID, permission);
                //Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //Perm_ViewAllDate = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllDate", PageID, permission);
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
    }
}