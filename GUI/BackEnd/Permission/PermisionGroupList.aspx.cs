﻿using _30shine.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.Interface;
using _30shine.MODEL.ENTITY.EDMX;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Data.Entity.Migrations;

namespace _30shine.GUI.BackEnd.Permission
{
    public partial class PermisionGroupList : System.Web.UI.Page
    {
        IPermisionGroup permisionModel = new PermisionGroupModel();
        protected Paging PAGING = new Paging();
        protected bool Perm_Access = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                BindData();
            }
        }

        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        public void BindData()
        {
            var list = new List<MODEL.ENTITY.EDMX.PermissionErp>();
            var result = permisionModel.ListGroupPermision();
            if (result.success)
            {
                list = (List<MODEL.ENTITY.EDMX.PermissionErp>)result.data;
                Bind_Paging(list.Count());
                Rpt.DataSource = list.Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                Rpt.DataBind();
            }
            else
            {
                WaringClient(result.msg);
            }

        }
        /// <summary>
        /// button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void _BtnClick(object sender, EventArgs e)
        {
            BindData();
            RemoveLoading();
        }
        /// <summary>
        /// Paging
        /// </summary>
        /// <param name="TotalRow"></param>
        protected void Bind_Paging(int TotalRow)
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(TotalRow) : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        /// <summary>
        /// Get_TotalPage
        /// </summary>
        /// <param name="TotalRow"></param>
        /// <returns></returns>
        protected int Get_TotalPage(int TotalRow)
        {
            TotalRow -= PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        }
        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        /// <summary>
        /// Gửi thông tin cảnh báo về client
        /// </summary>
        /// <param name="message"></param>
        public void WaringClient(string message)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Click", "ShowWarning('" + message + "');", true);
        }

        /// <summary>
        /// Get link default page 
        /// author: dungnm.
        /// </summary>
        /// <param name="permId"></param>
        /// <returns></returns>
        [WebMethod]
        public static object GetLinkForChangeDefaultPage(int permId)
        {
            try
            {
                List<Menu> lstMenu = new List<Menu>();
                using (Solution_30shineEntities db = new Solution_30shineEntities())
                {
                    //get menu parents
                    List<PermissionMenu> permMenu = (
                                                        from m in db.PermissionMenus.AsNoTracking()
                                                        where
                                                            m.Pid == 0
                                                            && m.Pid != null
                                                            && m.IsDelete == false
                                                            && m.IsActive == true
                                                        select m
                                                    ).ToList();
                    foreach (var item in permMenu)
                    {
                        Menu menuRecord = new Menu();
                        menuRecord.menuID = item.Id;
                        menuRecord.menuName = item.Name;
                        //get menu children c2
                        var lstChildMenu = (
                                                            from m in db.PermissionMenus.AsNoTracking()
                                                            join p in db.PermissionMenuActions.AsNoTracking() on m.Id equals p.PageId
                                                            where
                                                                p.PermissionId == permId
                                                                && m.Pid == item.Id
                                                                && m.IsDelete == false
                                                                && m.IsActive == true
                                                            select new
                                                            {
                                                                m.Id,
                                                                m.Name,
                                                                m.PageId
                                                            } into d
                                                            group d by new
                                                            {
                                                                d.Id,
                                                                d.Name,
                                                                d.PageId
                                                            } into g
                                                            select new
                                                            {
                                                                Id = g.Key.Id,
                                                                Name = g.Key.Name,
                                                                PageId = g.Key.PageId
                                                            }).ToList();
                        // Menu cap 3
                        var listIdC2 = lstChildMenu.Select(r => r.Id).ToList();
                        var lstMenuC3 = (
                                                             from m in db.PermissionMenus.AsNoTracking()
                                                             join p in db.PermissionMenuActions.AsNoTracking() on m.Id equals p.PageId
                                                             where
                                                                 p.PermissionId == permId
                                                                 && listIdC2.Contains(m.Pid ?? 0)
                                                                 && m.IsDelete == false
                                                                 && m.IsActive == true
                                                             select new
                                                             {
                                                                 m.Id,
                                                                 m.Name,
                                                                 m.PageId,
                                                                 m.Pid
                                                             } into d
                                                             group d by new
                                                             {
                                                                 d.Id,
                                                                 d.Name,
                                                                 d.PageId,
                                                                 d.Pid
                                                             } into g
                                                             select new
                                                             {
                                                                 Id = g.Key.Id,
                                                                 Name = g.Key.Name,
                                                                 PageId = g.Key.PageId,
                                                                 Pid = g.Key.Pid
                                                             }).ToList();


                        menuRecord.lstMenu = new List<Menu>();
                        foreach (var content in lstChildMenu)
                        {
                            //bind data
                            Menu menuChild = new Menu();
                            PermissionDefaultPage record = db.PermissionDefaultPages.FirstOrDefault(w => w.PermissionId == permId && w.PageId == content.Id);
                            if (record != null)
                            {
                                menuChild.selected = "selected='selected'";
                            }
                            else
                            {
                                menuChild.selected = "";
                            }
                            menuChild.menuID = content.Id;
                            menuChild.menuName = content.Name;
                            menuChild.menuLink = content.PageId;
                            menuRecord.lstMenu.Add(menuChild);
                        }
                        foreach (var itemC2 in menuRecord.lstMenu)
                        {
                            itemC2.lstMenu = new List<Menu>();
                            foreach (var c3 in lstMenuC3)
                            {
                                if (c3.Pid == itemC2.menuID)
                                {
                                    var itemC3 = new Menu();
                                    PermissionDefaultPage recordC3 = db.PermissionDefaultPages.FirstOrDefault(w => w.PermissionId == permId && w.PageId == c3.Id);
                                    if (recordC3 != null)
                                    {
                                        itemC3.selected = "selected='selected'";
                                    }
                                    else
                                    {
                                        itemC3.selected = "";
                                    }
                                    itemC3.menuID = c3.Id;
                                    itemC3.menuName = c3.Name;
                                    itemC3.menuLink = c3.PageId;
                                    itemC2.lstMenu.Add(itemC3);
                                }
                            }
                        }
                        lstMenu.Add(menuRecord);
                    }
                }
                return new JavaScriptSerializer().Serialize(lstMenu);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Kiểm tra để add or update default page
        /// author: dungnm.
        /// </summary>
        /// <param name="menuId"></param>
        /// <param name="permId"></param>
        /// <returns></returns>
        [WebMethod]
        public static string ChangeDefaultPage(int menuId, int permId)
        {
            try
            {
                Solution_30shineEntities db = new Solution_30shineEntities();
                List<PermissionMenuAction> lstCheckExist = db.PermissionMenuActions.Where(map => map.Id == menuId).ToList();
                if (lstCheckExist.Count > 0)
                {
                    AddOrUpdateDefaultPage(menuId, permId);
                    return "<span>Thay đổi Page mặc định cho nhóm quyền thành công</span>";
                }
                return "<span>Vui lòng set quyền cho nhóm để set defaulPage</span>";
            }
            catch (Exception ex)
            {

                return "<span>Có lỗi xảy ra. Vui lòng liên hệ nhóm phát triển</span>";
            }

        }


        /// <summary>
        /// add or update default page
        /// author : dungnm
        /// </summary>
        /// <param name="menuId"></param>
        /// <param name="permId"></param>
        private static void AddOrUpdateDefaultPage(int menuId, int permId)
        {
            try
            {
                Solution_30shineEntities db = new Solution_30shineEntities();
                //tồn tại thì update ngược lại thì add
                PermissionDefaultPage record = db.PermissionDefaultPages.FirstOrDefault(w => w.PermissionId == permId);
                if (record != null)
                {
                    record.PageId = menuId;
                    record.ModifiedTime = DateTime.Now;
                    db.PermissionDefaultPages.AddOrUpdate(record);
                    db.SaveChanges();
                }
                else
                {
                    PermissionDefaultPage defaultPage = new PermissionDefaultPage();
                    defaultPage.PageId = menuId;
                    defaultPage.PermissionId = permId;
                    defaultPage.CreatedTime = DateTime.Now;
                    defaultPage.IsDelete = false;
                    db.PermissionDefaultPages.Add(defaultPage);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
    }

    public class Menu
    {
        public int menuID { get; set; }
        public string menuName { get; set; }
        public string menuLink { get; set; }
        public string selected { get; set; }
        public List<Menu> lstMenu { get; set; }
    }
}