﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Action_Add.aspx.cs" Inherits="_30shine.GUI.BackEnd.Permission.Action_Add" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="ActionAdd" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <div class="wp sub-menu">
            <div class="wp960">

                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý Action &nbsp;&#187; </li>
                        <li class="li-listing"><a href="/admin/phan-quyen/danh-sach-action.html">Danh sách</a></li>
                        <li class="li-add active"><a href="/admin/phan-quyen/them-moi-action.html">Thêm mới</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="wp customer-add admin-product-add fe-service">
            <div class="wp960 content-wp">
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <div class="table-wp">
                    <table class="table-add admin-product-table-add fe-service-table-add">
                        <tbody>
                            <tr class="title-head">
                                <td>
                                    <strong>Thông tin Action</strong>
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="col-xs-2-left">
                                    <span>Tên action</span>
                                </td>
                                <td class="col-xs-9-right">
                                    <asp:TextBox ID="Name" placeholder="Ví dụ: Hiển thị hoặc(Thêm, sửa, xóa)" runat="server" ClientIDMode="Static" CssClass="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator runat="server" ID="Required" ForeColor="Red" Font-Italic="true" CssClass="fb-cover-error" Text="Bạn chưa nhập tên action!" ControlToValidate="Name"></asp:RequiredFieldValidator>
                                </td>

                            </tr>
                            <tr>
                                <td class="col-xs-2-left">
                                    <span>Mô tả</span>
                                </td>
                                <td class="col-xs-9-right">
                                    <asp:TextBox ID="txtDes" placeholder="Mô tả..." TextMode="MultiLine" Rows="3" runat="server" ClientIDMode="Static" CssClass="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ForeColor="Red" Font-Italic="true" CssClass="fb-cover-error" Text="Vui lòng nhập mô tả cho action này!" ControlToValidate="txtDes"></asp:RequiredFieldValidator>
                                </td>

                            </tr>
                            <tr class="tr-product-category">
                                <td class="col-xs-2 left"><span>Publish</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:CheckBox ID="Publish" Checked="true" runat="server" />
                                    </span>
                                </td>
                            </tr>
                            <tr class="tr-send">
                                <td class="col-xs-2 left"></td>
                                <td class="col-xs-9 right no-border">
                                    <span class="field-wp">
                                        <asp:Button ID="Send" CssClass="btn-send" runat="server" Text="Hoàn tất" ClientIDMode="Static" OnClick="ExcAddOrUpdate"></asp:Button>
                                        <%--<span class="btn-send btn-cancel" id="CalcelAddFbcv" style="margin-left: 12px;" >Hủy</span>--%>
                                    </span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </asp:Panel>
</asp:Content>

