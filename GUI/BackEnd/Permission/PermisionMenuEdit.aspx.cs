﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.Permission
{
    public partial class PermisionMenuEdit : System.Web.UI.Page
    {
        IPermisionMenu menuModel = new PermisionMenuModel();
        protected bool Perm_Access = false;
        protected int code = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            txtPageId.Enabled = false;
            txtId.Enabled = false;
            txtId.Visible = false;
            code = getMenuId();
            if (!IsPostBack)
            {
                BindDdlMenuName();
                BindData();
            }
        }

        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                pageId = permissionModel.GetRegexPageIdSpecial().Replace(pageId, ".html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        /// <summary>
        /// Thêm mới Menu phân quyền
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EditMenu(object sender, EventArgs e)
        {
            int interger = 0;
            int? parentId = int.TryParse(ddlParentMenu.SelectedValue, out interger) == false ? 0 : interger;
            string name = txtName.Text;
            string link = txtLink.Text;
            string path = txtPath.Text;
            string pageId = txtPageId.Text;
            bool isActive = Publish.Checked;
            int id = int.TryParse(txtId.Text, out interger) == false ? 0 : interger; ;

            if (link == "" ||path == "")
            {
                WaringClient("Bạn chưa nhập đủ trường");
            }
            else
            {
                //Check xem PageId đã bị sửa chưa
                Msg recordMenu = menuModel.GetById(id);
                if (recordMenu.success)
                {
                    var record = (MODEL.ENTITY.EDMX.PermissionMenu)recordMenu.data;
                    //PageId đã bị sửa thì check trùng PageId
                    if (record.PageId != pageId)
                    {
                        if (pageId != "")
                        {
                            var CheckResult = menuModel.CheckDuplicatePageId(pageId);
                            if (CheckResult)
                            {
                                WaringClient("PageId đã có trong cơ sở dữ liệu");
                                return;
                            }
                        }
                    }
                }
                PermissionMenu obj = new PermissionMenu();
                if (name == "")
                {
                    name = null;
                    parentId = null;
                }
                obj.Link = link;
                obj.Path = path;
                obj.PageId = pageId;
                obj.Name = name;
                obj.Pid = parentId;
                obj.IsActive = isActive;
                obj.Id = id;
                obj.ModifiedTime = DateTime.Now;

                Msg result = menuModel.Update(obj);
                if (result.success)
                {
                    var MsgParam = new List<KeyValuePair<string, string>>();
                    MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                    MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Thêm mới thành công!"));
                    UIHelpers.Redirect("/admin/phan-quyen/danh-sach-menu-v1.html", MsgParam);
                }
                else
                {
                    WaringClient(result.msg);
                }
            }
        }
        public void BindData ()
        {
            try
            {
                Msg result = menuModel.GetById(code);
                if (result.success)
                {
                    MODEL.ENTITY.EDMX.PermissionMenu record = (MODEL.ENTITY.EDMX.PermissionMenu)result.data;
                    //Bind du lieu vao droppdow list;
                    ListItem selectedListItem = ddlParentMenu.Items.FindByValue(record.Pid.ToString());
                    if (selectedListItem != null)
                    {
                        selectedListItem.Selected = true;
                    }
                    txtLink.Text = record.Link ?? "";
                    txtName.Text = record.Name ?? "";
                    txtPageId.Text =record.PageId ?? "";
                    txtPath.Text = record.Path ?? "";
                    Publish.Checked = record.IsActive ?? false ;
                    txtId.Text = record.Id.ToString();
                }
                else
                {
                    WaringClient("Đã có lỗi xảy ra, xin vui lòng liên hệ nhà phát triển");
                }
            }
            catch (Exception ex)
            {
                WaringClient("đã có lỗi xảy ra, xin vui lòng liên hệ nhà phát triển");

            }
            
        }
        /// <summary>
        /// Gửi thông tin cảnh báo về client
        /// </summary>
        /// <param name="message"></param>
        public void WaringClient(string message)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Click", "ShowWarning('" + message + "');", true);
        }
        /// <summary>
        /// Bind dữ liệu vào dropplist menu
        /// </summary>
        private void BindDdlMenuName()
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var _list = db.PermissionMenus.OrderBy(o => o.Id).ToList();
                    ddlParentMenu.Items.Clear();
                    ddlParentMenu.Items.Add(new ListItem("Chọn menu", "0"));
                    if (_list.Count > 0)
                    {
                        for (int i = 0; i < _list.Count; i++)
                        {
                            ddlParentMenu.Items.Add(new ListItem(_list[i].Name, _list[i].Id.ToString()));
                        }
                        ddlParentMenu.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                ddlParentMenu.Items.Clear();
                ddlParentMenu.Items.Add(new ListItem("--- Không tìm thấy menu ---", "0"));
                //throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// Get menu id
        /// </summary>
        /// <returns></returns>
        public int getMenuId()
        {
            var id = 0;
            try
            {
                var MatchInt = Regex.Match(Request.QueryString["Code"], @"[^0-9]+");
                if (!MatchInt.Success)
                {
                    id = Convert.ToInt32(Request.QueryString["Code"]);
                }
            }
            catch (Exception ex)
            {
                //WaringClient("Đã có lỗi xảy ra");
            }
            return id;
        }
    }
}