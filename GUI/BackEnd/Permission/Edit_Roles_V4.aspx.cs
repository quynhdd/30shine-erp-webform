﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Web.Services;
using System.Data.Entity.Migrations;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;
using _30shine.Helpers;
using System.IO;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;
using System.Data.Objects.SqlClient;
using System.Linq.Expressions;

namespace _30shine.GUI.BackEnd.Permission
{
    public partial class Edit_Roles_V4 : System.Web.UI.Page
    {
        private string PageID = "PQ_EDIT_ROLES";
        protected bool Perm_Access = false;
        protected List<Menu> menu = new List<Menu>();
        protected void Page_Load(object sender, EventArgs e)
        {
            var _Code = Request.QueryString["Code"];
            SetPermission();
            Session["Code"] = _Code;
            if (!IsPostBack)
            {
                GetMenu();
                //
                bindApiList(new List<DropDownList> { apiList }, true);
                bindActionList(new List<DropDownList> { actionList }, true);
            }

        }

        public void bindApiList(List<DropDownList> ddls, bool isListForm)
        {
            using (var db = new Solution_30shineEntities())
            {
                //var listApi = new List<Tbl_Config>();
                //listMethodType = db.Tbl_Config.Where(w => w.IsDelete == 0 & w.Key == "api_method_type").OrderBy(o => o.Id).ToList();
                List<OutputPermissionApi> listApi = (
                                                  from p in db.PermissionApis
                                                  join f in db.Tbl_Config on SqlFunctions.StringConvert((double)p.ApiMethod).Trim() equals f.Value
                                                  where p.IsDelete == false
                                                  && f.Key == "api_method_type" && f.IsDelete == 0
                                                  select new OutputPermissionApi
                                                  {
                                                      Id = p.Id,
                                                      ApiPath = p.ApiPath + ":" + f.Label
                                                  }
                                              ).OrderBy(w => w.ApiPath).ToList();
                //dung cho form danh sach
                if (isListForm)
                {
                    var item = new OutputPermissionApi();
                    item.ApiPath = "Chọn api";
                    item.Id = 0;
                    listApi.Insert(0, item);
                }

                for (var i = 0; i < ddls.Count; i++)
                {
                    ddls[i].DataTextField = "ApiPath";
                    ddls[i].DataValueField = "Id";
                    ddls[i].DataSource = listApi;
                    ddls[i].DataBind();
                }
            }
        }

        public void bindActionList(List<DropDownList> ddls, bool isListForm)
        {
            using (var db = new Solution_30shineEntities())
            {
                var listAction = new List<PermissionAction>();
                listAction = db.PermissionActions.Where(w => w.IsDelete == false).OrderBy(o => o.Id).ToList();

                if (isListForm)
                {
                    var item = new PermissionAction();
                    item.Name = "Chọn Action";
                    item.Id = 0;
                    listAction.Insert(0, item);
                }

                for (var i = 0; i < ddls.Count; i++)
                {
                    ddls[i].DataTextField = "Name";
                    ddls[i].DataValueField = "Id";
                    ddls[i].DataSource = listAction;
                    ddls[i].DataBind();
                }
            }
        }
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        /// <summary>
        /// get menu children where : Pid == null and Name == null
        /// author: dungnm
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public static object GetChildMenuWithoutModule()
        {
            try
            {

                //List<clsChildMenu> lstChild = new List<clsChildMenu>();
                using (var db = new Solution_30shineEntities())
                {
                    ////Get all menu
                    //List<MODEL.ENTITY.EDMX.PermissionMenu> listMenu = db.PermissionMenus.Where(r =>
                    //                                                 r.IsDelete == false &&
                    //                                                 r.IsActive == true).OrderByDescending(r => r.Name).ToList();
                    ////Get menuParent
                    //var listMenuParent = listMenu.Where(r => r.Pid == 0).ToList();
                    ////Get menu Child1
                    //int[] listMenuChild1 = (from a in listMenu
                    //                        join b in listMenuParent on a.Pid equals b.Id
                    //                        select a
                    //                         ).ToList().Select(r => r.Id).ToArray();
                    ////var listMenu = (
                    ////                    from m in db.PermissionMenus.AsNoTracking()
                    ////                    where
                    ////                        ((m.Pid == null) || (m.Pid == 0))
                    ////                        && m.IsDelete == false 
                    ////                        && m.IsActive == true
                    ////                    select m
                    ////                 ).OrderByDescending(w => w.Name).ToList();

                    //foreach (var item in listMenu)
                    //{
                    //    if (Array.IndexOf(listMenuChild1, item.Id) == -1)
                    //    {
                    //        clsChildMenu childMenu = new clsChildMenu();
                    //        childMenu.childId = item.Id;
                    //        childMenu.childName = item.Name;
                    //        childMenu.childLink = item.Link;
                    //        lstChild.Add(childMenu);
                    //    }
                    //}
                    //return new JavaScriptSerializer().Serialize(lstChild);
                    string sql = @"
                                BEGIN
                                    WITH a AS
                                        (
	                                        SELECT * FROM dbo.PermissionMenu  AS menu
	                                        WHERE menu.Pid = 0
                                        ),
                                    b AS 
                                        (
	                                        SELECT menu1.* FROM dbo.PermissionMenu AS menu1
	                                        INNER JOIN a ON a.Id = menu1.Pid
                                        ),
                                    tmp AS 
                                        (
                                            SELECT menu2.* FROM dbo.PermissionMenu AS menu2
	                                        INNER JOIN b ON menu2.Pid = b.Id 
	                                        WHERE ((menu2.Pid IS NULL) OR (menu2.Pid > 0))
                                        )
                                        SELECT tmp.Id as childId, ISNULL(tmp.Name,'') as childName, tmp.Link as childLink FROM tmp where tmp.IsDelete = 0 and tmp.IsActive = 1
                                        UNION 
                                        SELECT menu.Id as childId, ISNULL(menu.Name,'') as childName, menu.Link as childLink FROM dbo.PermissionMenu  AS menu
	                                    WHERE ((menu.Pid = 0) or (menu.Pid is null)) and menu.IsDelete = 0 and menu.IsActive = 1
                                        ORDER BY childName DESC
	                             END";
                    List<clsChildMenu> lstChild = db.Database.SqlQuery<clsChildMenu>(sql).ToList();
                    return new JavaScriptSerializer().Serialize(lstChild);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Get menu 
        /// author:dungnm
        /// </summary>
        protected void GetMenu()
        {
            try
            {
                var _Code = Convert.ToInt32(Request.QueryString["Code"]);
                using (Solution_30shineEntities db = new Solution_30shineEntities())
                {
                    //get menu parents
                    List<PermissionMenu> permMenu = (
                                                        from m in db.PermissionMenus.AsNoTracking()
                                                        where
                                                            m.Pid == 0
                                                            && m.Pid != null
                                                            && m.IsDelete == false
                                                            && m.IsActive == true
                                                        select m
                                                    ).ToList();
                    foreach (var item in permMenu)
                    {
                        Menu menuRecord = new Menu();
                        menuRecord.menuID = item.Id;
                        menuRecord.menuName = item.Name;
                        //get menu children
                        List<PermissionMenu> childMenu = (
                                                            from m in db.PermissionMenus.AsNoTracking()
                                                            where
                                                                m.Pid == item.Id
                                                                && m.IsDelete == false
                                                                && m.IsActive == true
                                                            select m
                                                         ).ToList();
                        menuRecord.lstMenu = new List<clsChildMenu>();
                        foreach (var content in childMenu)
                        {
                            clsChildMenu menuChild = new clsChildMenu();
                            menuChild.childId = content.Id;
                            menuChild.childName = content.Name;
                            menuChild.childLink = content.Link;
                            menuRecord.lstMenu.Add(menuChild);
                        }
                        menu.Add(menuRecord);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Update PermissionMenuAction
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public static object Update(int menuId, List<InputAction> Action)
        {
            try
            {
                var message = new Library.Class.cls_message();
                if (menuId >= 0)
                {
                    using (var db = new Solution_30shineEntities())
                    {
                        var code = Convert.ToInt32(HttpContext.Current.Session["Code"]);
                        var list = db.PermissionMenuActions.Where(w => w.PageId == menuId && w.PermissionId == code).ToList();
                        var index = -1;
                        var item = new PermissionMenuAction();
                        // kiểm tra xóa Item
                        if (list.Count > 0)
                        {
                            foreach (var v in list)
                            {
                                index = Action.FindIndex(w => w.actionId == v.ActionId);
                                if (index == -1)
                                {
                                    v.IsDelete = true;
                                    v.ModifiedTime = DateTime.Now;
                                    db.SaveChanges();
                                    message.success = true;
                                }
                                else
                                {
                                    v.ModifiedTime = DateTime.Now;
                                    v.IsDelete = false;
                                    db.SaveChanges();
                                    message.success = true;
                                }
                            }
                        }
                        // kiểm tra thêm mới Item
                        if (Action.Count > 0)
                        {
                            foreach (var v in Action)
                            {
                                index = list.FindIndex(w => w.ActionId == v.actionId);
                                if (index == -1)
                                {
                                    item = new PermissionMenuAction();
                                    item.PermissionId = code;
                                    item.PageId = menuId;
                                    item.ActionId = v.actionId;
                                    item.IsActive = true;
                                    item.IsDelete = false;
                                    item.CreatedTime = DateTime.Now;
                                    db.PermissionMenuActions.Add(item);
                                }
                                db.SaveChanges();
                                message.success = true;
                            }

                        }

                    }
                }
                return message;
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// get menu name by id
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public static object getNameById(int id)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            var data = new object();
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    data = (from c in db.PermissionMenus.AsNoTracking()
                            where c.IsDelete == false && c.IsActive == true && c.Id == id
                            select new
                            {
                                Id = c.Id,
                                Name = c.Name,
                                Link = c.Link
                            }
                            ).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

            return serializer.Serialize(data);
        }

        /// <summary>
        ///  Get list permission action
        ///  author: dungnm
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public static object GetListPermAction()
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            var data = new object();
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    data = (
                                from c in db.PermissionActions.AsNoTracking()
                                where c.IsDelete == false && c.IsActive == true
                                select new
                                {
                                    Id = c.Id,
                                    Name = c.Name
                                }
                            ).ToList();
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

            return serializer.Serialize(data);
        }

        /// <summary>
        /// Get list salonId
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public static object GetListActionId(int menuId, int permissionId)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var data = (from c in db.PermissionMenuActions
                                join d in db.PermissionActions on c.ActionId equals d.Id
                                where c.IsDelete == false
                                  && c.PageId == menuId
                                  && c.PermissionId == permissionId
                                select new
                                {
                                    ActionId = c.ActionId,
                                    Name = d.Name
                                }).ToList();

                    return serializer.Serialize(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [WebMethod]
        public static object GetListMapApi(int pageId,int permissionId)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            var data = new object();
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    data = (
                                from c in db.PermissionActionMapApis.AsNoTracking()
                                join a in db.PermissionActions on c.ActionId equals a.Id
                                join p in db.PermissionApis on c.ApiId equals p.Id
                                join f in db.Tbl_Config on SqlFunctions.StringConvert((double)p.ApiMethod).Trim() equals f.Value
                                where p.IsDelete == false
                                && f.Key == "api_method_type" && f.IsDelete == 0
                                && c.IsDelete == false && a.IsDelete == false
                                && c.PageId == pageId
                                && c.PermissionId == permissionId
                                select new
                                {
                                    Id = c.Id,
                                    Action = a.Name,
                                    ApiPath = p.ApiPath,
                                    ApiMethod = f.Label
                                }
                            ).ToList();
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

            return serializer.Serialize(data);
        }

        [WebMethod]
        public static object AddApi(int menuId, int actionId, int apiId, int permissionId)
        {
            try
            {
                var message = new Library.Class.cls_message();
                if (menuId >= 0)
                {
                    //kiem tra trung
                    var permActionMapApi = Load(w => w.PageId == menuId && w.PermissionId == permissionId && w.ApiId == apiId && w.ActionId == actionId && w.IsDelete == false);

                    if (permActionMapApi != null)
                    {
                        //da bi trung
                        message.message = "duplicate";
                        return message;
                    }
                    using (var db = new Solution_30shineEntities())
                    {
                        var item = new PermissionActionMapApi();
                        item.PermissionId = permissionId;
                        item.PageId = menuId;
                        item.ActionId = actionId;
                        item.ApiId = apiId;
                        item.IsDelete = false;
                        item.CreatedTime = DateTime.Now;
                        db.PermissionActionMapApis.Add(item);

                        db.SaveChanges();
                        message.success = true;
                    }
                }
                return message;
            }
            catch (Exception)
            {

                throw;
            }
        }
        [WebMethod]
        public static object DeleteApi(int mapId)
        {
            try
            {
                var message = new Library.Class.cls_message();
                if (mapId >= 0)
                {
                    //kiem tra trung
                    var permActionMapApi = Load(w => w.Id == mapId && w.IsDelete == false);

                    if (permActionMapApi != null)
                    {
                        using (var db = new Solution_30shineEntities())
                        {
                            permActionMapApi.IsDelete = true;
                            permActionMapApi.ModifiedTime = DateTime.Now;
                            db.PermissionActionMapApis.AddOrUpdate(permActionMapApi);
                            db.SaveChanges();
                            message.success = true;
                        }
                    }
                }
                return message;
            }
            catch (Exception)
            {

                throw;
            }
        }
        

        public static PermissionActionMapApi Load(Expression<Func<PermissionActionMapApi, bool>> expression)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    PermissionActionMapApi record = (
                                                  from p in db.PermissionActionMapApis
                                                  where p.IsDelete == false
                                                  select p
                                              ).Where(expression)
                                              .OrderByDescending(w => w.Id)
                                              .FirstOrDefault();
                    return record;
                }
            }
            catch
            {
                throw new Exception();
            }
        }

        public class Menu
        {
            public int menuID { get; set; }
            public string menuName { get; set; }
            public string menuLink { get; set; }
            public List<clsChildMenu> lstMenu { get; set; }
        }
        public class clsChildMenu
        {
            public int childId { get; set; }
            public string childName { get; set; }
            public string childLink { get; set; }
            public int childIsPublish { get; set; }
        }
        public class clsAdd_Menu : Tbl_Permission_Map
        {
            public int permId { get; set; }
            public string permName { get; set; }
            public int menuId { get; set; }
            public string menuName { get; set; }
            public string menuLink { get; set; }
            public string action { get; set; }
            public bool menuPublish { get; set; }
        }
        /// <summary>
        /// Input =actionid
        /// </summary>
        public class InputAction
        {
            public int actionId { get; set; }
        }

        public class cls_Url
        {
            public int Id { get; set; }
            public string FrontName { get; set; }
            public string RewriteUrl { get; set; }
            public string MainUrl { get; set; }
            public int ParentId { get; set; }
            public int ShowMenu { get; set; }
            public int Publish { get; set; }
            public string showHideMenu { get; set; }
            public string checkBox { get; set; }
            public string Action { get; set; }
            public List<cls_Url> _listUrl { get; set; }
        }

        public class OutputPermissionApi
        {
            public int Id { get; set; }
            public string ApiPath { get; set; }
        }
    }
}
