﻿<%@ Page Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="PermisionGroupList.aspx.cs" Inherits="_30shine.GUI.BackEnd.Permission.PermisionGroupList" %>

<asp:Content ID="ProductAdd" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <link href="/Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="/Assets/js/select2/select2.min.js"></script>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý nhóm quyền&nbsp;&#187; </li>
                        <li class="li-listing"><a href="/admin/phan-quyen/danh-sach-nhom-quyen.html">Danh sách nhóm quyền</a></li>
                        <li class="li-add active"><a href="/admin/phan-quyen/them-moi-nhom-quyen.html">Thêm mới</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing be-report">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <!-- Row Table Filter -->
                <div class="table-func-panel">
                    <div class="table-func-elm">
                        <span>Số hàng / Page : </span>
                        <div class="table-func-input-wp">
                            <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                            <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                            <ul class="ul-opt-segment">
                                <li data-value="10">10</li>
                                <li data-value="20">20</li>
                                <li data-value="30">30</li>
                                <li data-value="40">40</li>
                                <li data-value="50">50</li>
                                <li data-value="1000000">Tất cả</li>
                            </ul>
                            <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                        </div>
                    </div>
                </div>
                <!-- End Row Table Filter -->
                <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
                <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                    <ContentTemplate>
                        <div class="table-wp">
                            <table class="table-add table-listing">
                                <thead>
                                    <tr>
                                        <th class="col-xs-1">STT</th>
                                        <th>Tên nhóm quyền</th>
                                        <th class="col-xs-2">Mô tả</th>
                                        <th class="col-xs-1">Ẩn hiện</th>
                                        <th class="col-xs-3">Chức năng</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="Rpt" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# Container.ItemIndex + 1 + PAGING._Offset %></td>
                                                <td><%# Eval("Name") %></td>
                                                <td><%# Eval("Description") %></td>
                                                <td>
                                                    <input disabled="disabled" type="checkbox" <%#Convert.ToBoolean(Eval("IsActive"))? "checked" : "" %> /></td>
                                                <td>
                                                    <button class="btn btn-success btn-xs btn-Edit-Default" permid="<%#Eval("Id")%>" permidname="<%# Eval("Name") %>" style="margin-right: 5px;">
                                                        <i class="fa fa-pencil-square" aria-hidden="true"></i>&nbsp;&nbsp;Sửa DefaultPage
                                                    </button>
                                                    <a class="btn btn-warning btn-xs btn-Edit-Menu" href="/admin/phan-quyen/chinh-sua-phan-quyen-V3/<%#Eval("Id")%>.html" style="margin-left: 5px;">
                                                        <i class="fa fa-pencil-square" aria-hidden="true"></i>
                                                        &nbsp;&nbsp;Sửa phân quyền
                                                    </a>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>

                        <!-- Paging -->
                        <div class="site-paging-wp">
                            <% if (PAGING.TotalPage > 1)
                                { %>
                            <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                                <% if (PAGING._Paging.Prev != 0)
                                    { %>
                                <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                                <% } %>
                                <asp:Repeater ID="RptPaging" runat="server">
                                    <ItemTemplate>
                                        <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                            <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>>
                                            <%# Eval("PageNum") %>
                                        </a>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                                    { %>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                                <% } %>
                            </asp:Panel>
                            <% } %>
                        </div>
                        <!-- End Paging -->
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
            </div>
        </div>
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"></h4>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="permId" />
                        <select class="form-control" id="slMenu">
                        </select>
                        <div class="show-error" style="display: none"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" onclick="addDefaulPage()" id="btnConfirm">Đồng ý</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Hủy bỏ</button>
                    </div>
                </div>
            </div>
        </div>
        <style>
            #slMenu {
                font-family: Roboto Condensed Regular;
            }

                #slMenu option.prUrl {
                    font-weight: bold;
                }

                #slMenu option.childUrl {
                    font-style: italic;
                }

            .modal-backdrop.in {
                opacity: 0.9 !important;
            }

            .modal-content {
                margin-top: 80px !important;
            }
        </style>
        <script>
            //bill data to dropdownlist slMenu
            $(document).on("click", ".btn-Edit-Default", function (e) {
                $("#myModal").modal("show");
                var permId = $(this).attr("permId");
                var permName = $(this).attr("permIdName");
                $("#permId").val(permId);
                $(".modal-title").html("Thay đổi trang mặc định cho nhóm quyền <b>" + permName + "</b>");
                $.ajax({
                    type: "POST",
                    contentType: "application/json;charset=UTF-8",
                    data: '{permId:' + permId + '}',
                    datatype: "JSON",
                    url: '/GUI/BackEnd/Permission/PermisionGroupList.aspx/GetLinkForChangeDefaultPage',
                    success: function (data) {
                        jsonData = JSON.parse(data.d);
                        $("#slMenu").empty();
                        var option = "<option value='' selected='selected'>--- Chọn Page mặc định ---</option>";
                        for (var i = 0; i < jsonData.length; i++) {
                            $("#slMenu").empty();
                            option += "<option value='" + jsonData[i].menuID + "' class='prUrl' disabled='disabled'>" + jsonData[i].menuName + "</option>";
                            for (var j = 0; j < jsonData[i].lstMenu.length; j++) {
                                $("#slMenu").empty();
                                option += "<option url='" + jsonData[i].lstMenu[j].menuLink + "' value='" + jsonData[i].lstMenu[j].menuID + "' " + jsonData[i].lstMenu[j].selected + " class='childUrl'";
                                if (jsonData[i].lstMenu[j].menuLink == "javascript:void(0)") {
                                    option += "disabled='disabled'";
                                }
                                option += ">------&nbsp" + jsonData[i].lstMenu[j].menuName + "&nbsp(" + jsonData[i].lstMenu[j].menuLink + ")</option>";

                                for (var k = 0; k < jsonData[i].lstMenu[j].lstMenu.length; k++) {
                                    option += "<option url='" + jsonData[i].lstMenu[j].lstMenu[k].menuLink + "' value='" + jsonData[i].lstMenu[j].lstMenu[k].menuID + "' " + jsonData[i].lstMenu[j].lstMenu[k].selected + " class='childUrl'";
                                    if (jsonData[i].lstMenu[j].lstMenu[k].menuLink == "javascript:void(0)") {
                                        option += "disabled='disabled'";
                                    }
                                    option += ">---------------&nbsp" + jsonData[i].lstMenu[j].lstMenu[k].menuName + "&nbsp(" + jsonData[i].lstMenu[j].lstMenu[k].menuLink + ")</option>";
                                }
                                
                            }
                            $("#slMenu").append(option);
                        }
                    }

                });
            });

            //event change default page for permission group
            function addDefaulPage() {
                if ($("#slMenu").val() == "") {
                    var msg = "<span>Xin vui lòng chọn 1 trong các đường dẫn trên</span>";
                    $(".show-error").html(msg);
                    $(".show-error").show();
                    $(".show-error span").css({ "color": "red", "font-style": "italic" });
                    $(this).focus();
                }
                else {
                    var menuId = $("#slMenu").val();
                    var permId = $("#permId").val();
                    $.ajax({
                        type: "POST",
                        contentType: "application/json;charset=UTF-8",
                        data: '{menuId:' + menuId + ', permId: ' + permId + '}',
                        datatype: "JSON",
                        url: '/GUI/BackEnd/Permission/PermisionGroupList.aspx/ChangeDefaultPage',
                        success: function (data) {
                            $(".show-error").html(data.d);
                            $(".show-error").show();
                            $(".show-error span").css({ "color": "green", "font-style": "italic" });
                            location.reload();
                        }
                    })
                }
            }
        </script>
    </asp:Panel>
</asp:Content>
