﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.Permission
{
    public partial class Permission_Add : System.Web.UI.Page
    {
        private string PageID = "";
        private bool Perm_Access = false;
        protected string _Code;
        protected bool _IsUpdate = false;
        protected Tbl_Permission OBJ;
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                if (IsUpdate())
                {
                    Bind_OBJ();
                }
            }
        }
        #region[IsUPdate]
        private bool IsUpdate()
        {
            _Code = Request.QueryString["Code"];
            if (_Code != null)
            {
                _IsUpdate = true;
                return true;
            }
            else
            {
                _IsUpdate = false;
                return false;
            }
        }
        #endregion
        #region[AddPermission]
        /// <summary>
        /// AddPermission
        /// </summary>
        private void Add()
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var _obj = new Tbl_Permission();
                    _obj.pName = Name.Text;
                    _obj.pTitle = Title.Text;
                    _obj.pCreatedDate = DateTime.Now;
                    if (Publish.Checked == true)
                        _obj.pPublish = true;
                    else
                        _obj.pPublish = false;
                    var Error = false;
                    if (!Error)
                    {
                        db.Tbl_Permission.Add(_obj);
                        db.SaveChanges();
                        var MsgParam = new List<KeyValuePair<string, string>>();
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Thêm mới thành công!"));
                        UIHelpers.Redirect("/admin/phan-quyen/danh-sach-phan-quyen.html", MsgParam);
                    }
                    else
                    {
                        var msg = "Thêm mới không thành công. Vui lòng liên hệ với nhóm phát triển!";
                        var status = "msg-system warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 1000);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
        #region[Update]
        /// <summary>
        /// Update
        /// </summary>
        private void Update()
        {
            using (var db = new Solution_30shineEntities())
            {
                int Id;
                if (int.TryParse(_Code, out Id))
                {
                    OBJ = db.Tbl_Permission.FirstOrDefault(w => w.pID == Id);
                    if (!OBJ.Equals(null))
                    {
                        OBJ.pName = Name.Text;
                        OBJ.pTitle = Title.Text;
                        if (Publish.Checked)
                            OBJ.pPublish = true;
                        else
                            OBJ.pPublish = false;

                        db.Tbl_Permission.AddOrUpdate(OBJ);
                        var exc = db.SaveChanges();
                        if (exc > 0)
                        {
                            var MsgParam = new List<KeyValuePair<string, string>>();
                            MsgParam.Add(new KeyValuePair<string, string>("msg_add_status", "success"));
                            MsgParam.Add(new KeyValuePair<string, string>("msg_add_message", "Cập nhật thành công!"));
                            UIHelpers.Redirect("/admin/phan-quyen/danh-sach-phan-quyen.html", MsgParam);
                        }
                        else
                        {
                            var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển!";
                            var status = "warning";
                            UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                        }
                    }
                }
                else
                {
                    var msg = "Lỗi!Không tìm thấy tên nhóm quyền!";
                    var status = "warning";
                    UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                }
            }
        }
        #endregion
        #region[BindOBJ]
        /// <summary>
        /// BindOBJ
        /// </summary>
        private bool Bind_OBJ()
        {
            using (var db = new Solution_30shineEntities())
            {
                var ExistOBJ = true;
                // int _Code = Convert.ToInt32(Request.QueryString["Code"]);
                int Id;
                if (int.TryParse(_Code, out Id))
                {
                    OBJ = db.Tbl_Permission.Where(w => w.pID == Id).FirstOrDefault();
                    if (!OBJ.Equals(null))
                    {
                        Name.Text = OBJ.pName;
                        Title.Text = OBJ.pTitle;
                        if (OBJ.pPublish == true)
                            Publish.Checked = true;
                        else
                            Publish.Checked = false;
                    }
                    else
                    {
                        ExistOBJ = false;
                        var msg = "Lỗi! Tên nhóm quyền không tồn tại!";
                        var status = "error warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                    }
                }
                else
                {
                    ExistOBJ = false;
                    var msg = "Lỗi! Tên nhóm quyền không tồn tại!";
                    var status = "error warning";
                    UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                }
                return ExistOBJ;
            }
        }
        #endregion
        #region[ExcAddOrUpdate]
        protected void ExcAddOrUpdate(object sender, EventArgs e)
        {
            if (IsUpdate())
                Update();
            else
                Add();
        }
        #endregion
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                if (Request.QueryString["Code"] != null)
                {
                    PageID = "PQ_EDIT";
                }
                else
                {
                    PageID = "PQ_ADD";
                }
                IPermissionModel permissionModel = new PermissionModel();
                var permission = Session["User_Permission"].ToString();
                Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_ShowSalon = permissionModel.GetActionByActionNameAndPageId("Perm_ShowSalon", PageID, permission);
                //Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //Perm_ViewAllDate = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllDate", PageID, permission);
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
    }
}
