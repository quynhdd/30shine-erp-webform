﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Globalization;
using _30shine.Helpers.Http;
using System.Data;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;
using Project.Helpers;
using System.Linq.Expressions;
using System.Data.Objects.SqlClient;

namespace _30shine.GUI.BackEnd.Permission
{
    public partial class PermissionApiList : System.Web.UI.Page
    {

        private string PageID = "DV_KTH_DSDH";
        protected Paging PAGING = new Paging();
        protected string THead = "";
        protected string StrProductPrice = "";
        public CultureInfo culture = new CultureInfo("vi-VN");
        private DateTime timeFrom;
        private DateTime timeTo;
        protected int salonId;
        private int integer;
        protected string FirstDayOfMonth = string.Format("{0:dd/MM/yyyy}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1));
        protected string Today = String.Format("{0:dd/MM/yyyy}", DateTime.Now);

        //protected string Day1 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-2));
        protected string yesterday = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-1));
        //protected string Day3 = string.Format("{0:dd/MM/yyyy}", DateTime.Now);

        protected bool Perm_ViewAllData = false;
        private bool Perm_Access = false;
        protected bool Perm_Delete = false;
        protected bool Perm_ShowElement = false;

        protected string Permission = "";
        protected string visib = "hidden";
        protected QLKhoSalonModel dbQLkhoSalon = new QLKhoSalonModel();
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (!IsPostBack)
            {
                salonId = Convert.ToInt32(Session["SalonId"]);
                Library.Function.bindApiMethod(new List<DropDownList> { apiMethod }, true);
                //bindData();
                PassDataFromServerToClient();
            }
            else
            {
                //Exc_Filter();
            }
            RemoveLoading();
        }
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                //string pageId = permissionModel.GetRegexPageIdSpecial().Replace(Request.RawUrl, "/edit.html");
                string pageId = permissionModel.GetRegexPageIdSpecial().Replace(Request.RawUrl, ".html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_ShowElement = permissionModel.CheckPermisionByAction("Perm_ShowElement", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                HDF_UserId.Value = staffId.ToString();
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        private void bindData()
        {
            //using (var db = new Solution_30shineEntities())
            //{
            try
            {

                var apiPathTmp = apiPath.Text;
                var apiMethodId = int.TryParse(apiMethod.SelectedValue, out integer) ? integer : 0;
                var data = LoadList(e => (apiPathTmp == "" || apiPathTmp == null || e.ApiPath.Contains(apiPathTmp))
                            && (apiMethodId == 0 || e.ApiMethod == apiMethodId));
                //
                if (data != null)
                {
                    Bind_Paging(data.Count());
                    rptDanhsach.DataSource = data.Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                    rptDanhsach.DataBind();
                }


            }
            catch (Exception ex)
            {
                new _30shine.Helpers.PushNotiErrorToSlack().PushDefault(ex.Message + ", MAIN: " + ex.StackTrace, this.ToString() + ".bindData", "");
                throw ex;
            }
            //}
        }

        [WebMethod]
        public static string Delele_Api(int Id, int UserId)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            var Msg = new Msg();
            try
            {
                Msg.success = false;
                Msg.msg = "fail";
                //
                //         var response = new Request().RunDeleteAsyncV1(
                //Libraries.AppConstants.URL_API_INVENTORY + "/api/order?OrderId=" + Id + "&UserId=" + UserId
                // ).Result;

                //check thong tin api dang su dung => ko dc phep xoa

                //
                PermissionApi record = Load(w => w.Id == Id);

                using (var db = new Solution_30shineEntities())
                {
                    record.ModifiedTime = DateTime.Now;
                    record.IsDelete = true;
                    db.PermissionApis.AddOrUpdate(record);
                    db.SaveChanges();
                }

                Msg.success = true;
                Msg.msg = "success";
                //return data.Status;

            }
            catch (Exception ex)
            {
                new _30shine.Helpers.PushNotiErrorToSlack().PushDefault(ex.Message + ", MAIN: " + ex.StackTrace, "Delele_Api", "");
                //return "fail";
                Msg.success = false;
                Msg.msg = "fail";
            }
            return serializer.Serialize(Msg);
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            bindData();
            RemoveLoading();
        }

        protected void Bind_Paging(int TotalRow)
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(TotalRow) : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage(int TotalRow)
        {
            TotalRow -= PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }


        private void PassDataFromServerToClient()
        {
            List<ElementEnable> list = listElement();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jSon = serializer.Serialize(list);
            string script = String.Format("<script type=\"text/javascript\">var listElement={0}</script>", jSon);
            if (!this.ClientScript.IsClientScriptBlockRegistered("clientScript"))
            {
                this.ClientScript.RegisterClientScriptBlock(this.GetType(), "clientScript", script, false);
            }
        }


        #region Bind chi tiết đơn đặt hàng 

        [WebMethod]
        public static object getAllGoodsById(int orderId)
        {
            using (var db = new Solution_30shineEntities())
            {
                return db.Store_QLKho_DetailById(orderId).ToList();
            }
        }
        #endregion


        #region Update chi tiết đơn hàng
        [WebMethod]
        public static bool UpdateOrderById(string productIds, int orderId)
        {
            var serialize = new JavaScriptSerializer();
            var objectList = serialize.Deserialize<List<QLKho_SalonOrder_Flow>>(productIds).ToList();
            var error = 0;
            using (var db = new Solution_30shineEntities())
            {
                var obj = new ProductBasic();
                var DS1 = Library.Function.genListFromJson(productIds);
                var DS2 = db.QLKho_SalonOrder_Flow.Where(w => w.OrderId == orderId && w.IsDelete != true).ToList();
                var index = -1;
                var product = new Product();
                var item = new QLKho_SalonOrder_Flow();
                // Kiểm tra cập nhật Item
                if (DS2.Count > 0)
                {
                    foreach (var v in DS2)
                    {
                        product = db.Products.FirstOrDefault(w => w.Id == v.ProductId);
                        index = DS1.FindIndex(w => w.Id == v.ProductId);
                        if (index != -1)
                        {
                            obj = DS1[index];
                            v.QuantityExport = obj.Quantity;
                            v.ModifiedTime = DateTime.Now;
                            if (product != null)
                            {
                                v.Cost = product.Cost;
                                v.Price = product.Price;
                            }
                            db.QLKho_SalonOrder_Flow.AddOrUpdate(v);
                            error += db.SaveChanges() > 0 ? 0 : 1;
                        }
                    }
                }
            }
            return true;
        }
        [WebMethod]
        public static bool UpdateQLKho_SalonOrder(int orderId, string noteKho, string noteSalon)
        {
            QLKho_SalonOrder OBJ = new QLKho_SalonOrder();

            if (orderId > 0)
            {
                using (var db = new Solution_30shineEntities())
                {
                    var error = 0;
                    OBJ = db.QLKho_SalonOrder.FirstOrDefault(w => w.Id == orderId);
                    if (OBJ != null)
                    {
                        OBJ.StatusId = 2;
                        OBJ.NoteKho = noteKho;
                        OBJ.NoteSalon = noteSalon;
                        OBJ.ExportTime = DateTime.Now;
                        OBJ.ModifiedTime = DateTime.Now;
                        db.QLKho_SalonOrder.AddOrUpdate(OBJ);
                        error += db.SaveChanges() > 0 ? 0 : 1;
                    }
                }
            }
            return true;
        }
        #endregion

        #region Bind danh sách hàng trả thiếu

        [WebMethod]
        public static object ProductsOweList(int salonId, DateTime currentOrderDate)
        {
            using (var db = new Solution_30shineEntities())
            {
                return db.Store_QLKho_DanhSachTraThieu(null, currentOrderDate, salonId).ToList();
            }
        }
        #endregion

        protected List<ElementEnable> listElement()
        {
            List<ElementEnable> list = new List<ElementEnable>();
            list.Add(new ElementEnable() { ElementName = "NoteKho", Enable = !Perm_Access, Type = "disable" });
            list.Add(new ElementEnable() { ElementName = "NoteSalon", Enable = !Perm_ShowElement, Type = "disable" });
            list.Add(new ElementEnable() { ElementName = "tdOrderID", Enable = !Perm_ShowElement, Type = "hidden" });
            list.Add(new ElementEnable() { ElementName = "del_Bill", Enable = !Perm_ShowElement, Type = "hidden" });

            list.Add(new ElementEnable() { ElementName = "td-product-cost", Enable = !Perm_ShowElement, Type = "visible" });
            list.Add(new ElementEnable() { ElementName = "td-product-TotalPrice", Enable = !Perm_ShowElement, Type = "visible" });
            list.Add(new ElementEnable() { ElementName = "tr-totalPrice", Enable = !Perm_ShowElement, Type = "visible" });
            return list;
        }
        public static List<OutputPermissionApi> LoadList(Expression<Func<OutputPermissionApi, bool>> expression)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    List<OutputPermissionApi> record = (
                                                  from p in db.PermissionApis
                                                  join f in db.Tbl_Config on SqlFunctions.StringConvert((double)p.ApiMethod).Trim() equals f.Value
                                                  where p.IsDelete == false
                                                  && f.Key == "api_method_type" && f.IsDelete == 0
                                                  select new OutputPermissionApi
                                                  {
                                                      Id = p.Id,
                                                      ApiPath = p.ApiPath,
                                                      ApiMethodName = f.Label,
                                                      ApiMethod = p.ApiMethod
                                                  }
                                              ).Where(expression)
                                              .OrderBy(w => w.ApiPath).ThenBy(w => w.ApiMethod).ToList();
                    return record;
                }
            }
            catch (Exception ex)
            {
                throw new Exception();
            }
        }

        public static PermissionApi Load(Expression<Func<PermissionApi, bool>> expression)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    PermissionApi record = (
                                                  from p in db.PermissionApis
                                                  where p.IsDelete == false
                                                  select p
                                              ).Where(expression)
                                              .OrderByDescending(w => w.Id).FirstOrDefault();
                    return record;
                }
            }
            catch
            {
                throw new Exception();
            }
        }
    }

    public class ResponseDelete
    {
        public String Status { get; set; }

    }

    public class OutputPermissionApi
    {
        public int Id { get; set; }
        public string ApiPath { get; set; }
        public string ApiMethodName { get; set; }
        public int ApiMethod { get; set; }
    }



}