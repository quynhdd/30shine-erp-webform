﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.Permission
{
    public partial class PermisionMenuAdd : System.Web.UI.Page
    {
        IPermisionMenu menuModel = new PermisionMenuModel();
        protected bool Perm_Access = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            txtPageId.Enabled = false;
            if (!IsPostBack)
            {
                BindDdlMenuName();
            }
        }

        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        /// <summary>
        /// Thêm mới Menu phân quyền
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AddMenu(object sender, EventArgs e)
        {
            int interger = 0;
            int? parentId = int.TryParse(ddlParentMenu.SelectedValue, out interger) == false ? 0 : interger ;
            string name = txtName.Text;
            string link = txtLink.Text;
            string path = txtPath.Text;
            string pageId = txtPageId.Text;
            bool isActive = Publish.Checked;
            if (link == "" || path == "")
            {
                WaringClient("Bạn chưa nhập đủ trường");
            }
            else
            {
                if (pageId != "")
                {
                    var CheckResult = menuModel.CheckDuplicatePageId(pageId);
                    if (CheckResult)
                    {
                        WaringClient("PageId đã có trong cơ sở dữ liệu");
                        return;
                    }
                }
                if (name == "")
                {
                    name = null;
                    parentId = null;
                }
                PermissionMenu obj = new PermissionMenu();
                obj.Link = link;
                obj.Path = path;
                obj.PageId = pageId;
                obj.Name = name;
                obj.Pid = parentId;
                obj.IsDelete = false;
                obj.CreatedTime = DateTime.Now;
                obj.IsActive = isActive;
                Msg result = menuModel.Add(obj);
                if (result.success)
                {
                    var MsgParam = new List<KeyValuePair<string, string>>();
                    MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                    MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Thêm mới thành công!"));
                    UIHelpers.Redirect("/admin/phan-quyen/danh-sach-menu-v1.html", MsgParam);
                }
                else
                {
                    WaringClient(result.msg);
                }
            }
        }
        /// <summary>
        /// Gửi thông tin cảnh báo về client
        /// </summary>
        /// <param name="message"></param>
        public void WaringClient(string message)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Click", "ShowWarning('" + message + "');", true);
        }
        /// <summary>
        /// Bind dữ liệu vào dropplist menu
        /// </summary>
        private void BindDdlMenuName()
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var _list = db.PermissionMenus.OrderBy(o => o.Id).ToList();
                    ddlParentMenu.Items.Clear();
                    ddlParentMenu.Items.Add(new ListItem("--- Chọn danh mục menu ---", "0"));
                    if (_list.Count > 0)
                    {
                        for (int i = 0; i < _list.Count; i++)
                        {
                            ddlParentMenu.Items.Add(new ListItem(_list[i].Name, _list[i].Id.ToString()));
                        }
                        ddlParentMenu.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                ddlParentMenu.Items.Clear();
                ddlParentMenu.Items.Add(new ListItem("--- Không tìm thấy menu ---", "0"));
                //throw new Exception(ex.Message);
            }
        }
    }
}