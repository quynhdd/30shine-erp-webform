﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.Interface;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.Permission
{
    public partial class PermissionActionAdd : System.Web.UI.Page
    {
        private string PageID = "";
        protected bool CheckUpdate = false;
        private bool Perm_Access = false;
        protected bool Perm_ViewAllData = false;
        protected IPermissionActionModel iPermissionModel = new PermissionActionModel();
        private static PermissionActionAdd instance;
        protected string permissionId;
        protected _30shine.MODEL.ENTITY.EDMX.PermissionAction OBJ;
        CultureInfo culture = new CultureInfo("vi-VN");
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                if (IsUpdate())
                {
                    BindOBJ();
                }
            }
        }

        /// <summary>
        /// get instance, phục vụ trường hợp có hàm statistic, sử dụng instance có thẻ gọi các method không phải static
        /// </summary>
        /// <returns></returns>
        public static PermissionActionAdd getInstance()
        {
            if (!(PermissionActionAdd.instance is PermissionActionAdd))
            {
                PermissionActionAdd.instance = new PermissionActionAdd();
            }

            return PermissionActionAdd.instance;
        }

        /// <summary>
        ///  set phân quyền 
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //if (Request.QueryString["Code"] != null)
                //{
                //    PageID = "PermissonActionEdit";
                //}
                //else
                //{
                //    PageID = "PermissonActionAdd";
                //}
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                pageId = permissionModel.GetRegexPageIdSpecial().Replace(pageId, ".html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        /// <summary>
        /// Bind Data 
        /// </summary>
        private void BindOBJ()
        {

            OBJ = iPermissionModel.GetById(Convert.ToInt32(permissionId));
            if (OBJ != null)
            {
                NameAction.Text = OBJ.Name;
                DescriptionAction.Text = OBJ.Description;
            }
        }

        private bool IsUpdate()
        {
            if (Request.QueryString["Code"] == null) { CheckUpdate = false; return false; }

            permissionId = Request.QueryString["Code"];
            if (permissionId != "")
            {
                CheckUpdate = true;
                return true;
            }
            else
            {
                CheckUpdate = false;
                return false;

            }

        }
        
        protected void AddOrUpdate(object sender, EventArgs e)
        {
            if (IsUpdate())
            {
                Update();

            }
            else
            {
                Add();
            }
        }

        /// <summary>
        /// Insert permisson action
        /// </summary>
        protected void Add()
        {
            try
            {
                _30shine.MODEL.ENTITY.EDMX.PermissionAction record = new _30shine.MODEL.ENTITY.EDMX.PermissionAction();
                record.Name = NameAction.Text.Trim();
                record.Description = DescriptionAction.Text;
                iPermissionModel.Add(record);
                var MsgParam = new List<KeyValuePair<string, string>>();
                MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Theem m thành công!"));
                UIHelpers.Redirect("/admin/phan-quyen/danh-sach-action-v2.html", MsgParam);
            }
            catch
            {
                throw new Exception("Có lỗi xảy ra xin vui lòng liên hệ nhà phát triển!!!");
            }
        }

        /// <summary>
        /// Update permisson action
        /// </summary>
        protected void Update()
        {
            try
            {
                _30shine.MODEL.ENTITY.EDMX.PermissionAction record = new _30shine.MODEL.ENTITY.EDMX.PermissionAction();
                record = iPermissionModel.GetById(Convert.ToInt32(permissionId));
                record.Name = NameAction.Text.Trim();
                record.Description = DescriptionAction.Text;
                var exc = iPermissionModel.Update(record);
                var MsgParam = new List<KeyValuePair<string, string>>();
                MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thành công!"));
                UIHelpers.Redirect("/admin/phan-quyen/danh-sach-action-v2.html", MsgParam);
            }
            catch
            {
                throw new Exception("Có lỗi xảy ra xin vui lòng liên hệ nhà phát triển!!!");
            }
        }

        /// <summary>
        /// Check duplicate name action
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [WebMethod]
        public static object checkDuplicate(string name,int id)
        {
            var message = new Library.Class.cls_message();
            try
            {
                _30shine.MODEL.ENTITY.EDMX.PermissionAction record = new MODEL.ENTITY.EDMX.PermissionAction();
                if (id == 0)
                {
                    record = getInstance().iPermissionModel.Load(w => w.Name == name);
                }
                else
                {
                    record = getInstance().iPermissionModel.Load(w => w.Name == name && w.Id != id);
                }
                if (record != null)
                {
                    message.success = true;
                }
                else
                {
                    message.success = false;
                }
            }
            catch (Exception ex)
            {

            }
            return message;
        }
    }
}