﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SocialThread_Add.aspx.cs" Inherits="_30shine.GUI.BackEnd.Mess.UISocialThread.SocialThread_Add"  MasterPageFile="~/TemplateMaster/SiteMaster.Master"%>

<asp:Content ID="SocialThreadAdd" ContentPlaceHolderID="CtMain" runat="server">
<asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
<asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

<div class="wp sub-menu">
    <div class="wp960">
        <div class="wp content-wp">
            <ul class="ul-sub-menu" id="subMenu">
                <li>Quản lý Nguồn thông tin &nbsp;&#187; </li>
                <li class="li-listing"><a href="/admin/nguon-thong-tin/danh-sach.html">Danh sách</a></li>
                <li class="li-add active"><a href="/admin/nguon-thong-tin/them-moi.html">Thêm mới</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="wp customer-add admin-product-add admin-service-add">
    <%-- Add --%>                
    <div class="wp960 content-wp">
        <!-- System Message -->
        <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
        <!-- END System Message -->

        <div class="table-wp">
            <table class="table-add admin-product-table-add admin-service-table-add">
                <tbody>
                    <tr class="title-head">
                        <td><strong>Nguồn thông tin biết đến cửa hàng</strong></td>
                        <td></td>
                    </tr>
                    <tr class="tr-margin" style="height: 20px;"></tr>
                    <tr>
                        <td class="col-xs-2 left"><span>Tên nguồn</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:TextBox ID="Name" runat="server" ClientIDMode="Static"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="NameValidate" ControlToValidate="Name" runat="server" CssClass="fb-cover-error" Text="Bạn chưa nhập tên nguồn!"></asp:RequiredFieldValidator>
                            </span>
                                            
                        </td>
                    </tr>

                    <tr>
                        <td class="col-xs-2 left"><span>Thể loại</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:DropDownList ID="stType" runat="server">
                                    <asp:ListItem Text="Nguồn thông tin biết đến cửa hàng" Value="1" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="Nguồn khảo sát nhân viên" Value="2"></asp:ListItem>
                                </asp:DropDownList>
                            </span>
                                            
                        </td>
                    </tr>

                    <tr class="tr-description">
                        <td class="col-xs-2 left"><span>Mô tả</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:TextBox TextMode="MultiLine" Rows="5" ID="Description" runat="server" ClientIDMode="Static"></asp:TextBox>
                            </span>
                        </td>
                    </tr>

                    <tr class="tr-product-category">
                        <td class="col-xs-2 left"><span>Publish</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:CheckBox ID="Publish" Checked="true" runat="server" />
                            </span>
                        </td>
                    </tr>
                    
                    <tr class="tr-send">
                        <td class="col-xs-2 left"></td>
                        <td class="col-xs-9 right no-border">
                            <span class="field-wp">
                                <asp:Button ID="Send" CssClass="btn-send" runat="server" Text="Hoàn tất" ClientIDMode="Static" OnClick="AddSocialThread"></asp:Button>
                                <%--<span class="btn-send btn-cancel" id="CalcelAddFbcv" style="margin-left: 12px;" >Hủy</span>--%>
                            </span>
                        </td>
                    </tr>
                    <asp:HiddenField runat="server" ID="UrlKeyField"  ClientIDMode="Static"/>
                    <asp:HiddenField runat="server" ID="ImgCoverField"  ClientIDMode="Static"/>
                </tbody>
            </table>
        </div>
    </div>
    <%-- end Add --%>
</div>

<script>
    jQuery(document).ready(function () {
        $("#glbAdminContent").addClass("active");
        $("#glbAdminSocialThread").addClass("active");
    });
</script>

</asp:Panel>
</asp:Content>