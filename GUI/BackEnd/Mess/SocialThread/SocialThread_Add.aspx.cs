﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.Mess.UISocialThread
{
    public partial class SocialThread_Add : System.Web.UI.Page
    {
        private string PageID = "NTT_ADD";
        private bool Perm_Access = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
        }

        protected void AddSocialThread(object sender, EventArgs e)
        {
            using (var db = new Solution_30shineEntities())
            {
                var obj = new SocialThread();
                obj.Name = Name.Text.Trim();
                obj.stType = Convert.ToInt32(stType.SelectedValue);
                obj.CreatedDate = DateTime.Now;
                obj.IsDelete = 0;
                if (Publish.Checked)
                {
                    obj.Publish = 1;
                }
                else
                {
                    obj.Publish = 0;
                }

                // Validate
                // Check trùng tên dịch vụ
                var check = db.SocialThreads.Where(w => w.Name == obj.Name).ToList();
                var Error = false;
                if (check.Count > 0)
                {
                    MsgSystem.Text = "Tên nguồn đã tồn tại. Bạn vui lòng nhập tên nguồn khác.";
                    MsgSystem.CssClass = "msg-system warning";
                    Error = true;

                }

                if (!Error)
                {
                    db.SocialThreads.Add(obj);
                    db.SaveChanges();

                    MsgSystem.Text = "Cập nhật thành công!";
                    MsgSystem.CssClass = "msg-system success";
                }
            }
        }
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                ////Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                ////Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                ////Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                ////Perm_ViewAllDate = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllDate", PageID, permission);
                //ExecuteByPermission();

                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        
    }
}