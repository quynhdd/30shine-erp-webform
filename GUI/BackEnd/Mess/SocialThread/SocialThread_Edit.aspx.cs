﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.Mess.UISocialThread
{
    public partial class SocialThread_Edit : System.Web.UI.Page
    {
        private string PageID = "NTT_EDIT";
        protected SocialThread OBJ;
        private bool Perm_Access = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (!IsPostBack)
            {
                Bind_OBJ();
            }
        }
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                ////Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                ////Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                ////Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                ////Perm_ViewAllDate = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllDate", PageID, permission);
                //ExecuteByPermission();

                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        private bool Bind_OBJ()
        {
            var Code = Convert.ToInt32(Request.QueryString["Code"]);
            var ExitsOBJ = true;
            using (var db = new Solution_30shineEntities())
            {
                OBJ = db.SocialThreads.FirstOrDefault(w => w.Id == Code && w.IsDelete != 1);
                if (OBJ != null)
                {
                    Name.Text = OBJ.Name;
                    Description.Text = OBJ.Description;
                    if (OBJ.Publish == 1)
                    {
                        Publish.Checked = true;
                    }
                    else
                    {
                        Publish.Checked = false;
                    }
                    var stTypeItem = stType.Items.FindByValue(OBJ.stType.ToString());
                    if (stTypeItem != null)
                    {
                        stTypeItem.Selected = true;
                    }
                    else
                    {
                        stType.SelectedIndex = 0;
                    }
                }
                else
                {
                    ExitsOBJ = false;
                    MsgSystem.Text = "Nguồn thông tin không tồn tại!";
                    MsgSystem.CssClass = "msg-system warning";
                }
                return ExitsOBJ;
            }
        }

        protected void Update_OBJ(object sender, EventArgs e)
        {
            using (var db = new Solution_30shineEntities())
            {
                var Code = Convert.ToInt32(Request.QueryString["Code"]);
                var obj = db.SocialThreads.Where(w => w.Id == Code).FirstOrDefault();

                if (!obj.Equals(null))
                {
                    obj.Name = Name.Text.Trim();
                    obj.stType = Convert.ToInt32(stType.SelectedValue);
                    obj.ModifiedDate = DateTime.Now;
                    if (Publish.Checked)
                    {
                        obj.Publish = 1;
                    }
                    else
                    {
                        obj.Publish = 0;
                    }

                    db.SocialThreads.AddOrUpdate(obj);
                    var exc = db.SaveChanges();
                    if (exc > 0)
                    {
                        MsgSystem.Text = "Cập nhật thành công!";
                        MsgSystem.CssClass = "msg-system success";
                    }
                    else
                    {
                        MsgSystem.Text = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển!";
                        MsgSystem.CssClass = "msg-system warning";
                    }
                }
                else
                {
                    MsgSystem.Text = "Lỗi! Nguồn thông tin không tồn tại.";
                    MsgSystem.CssClass = "msg-system warning";
                }
            }
        }

        
    }
}