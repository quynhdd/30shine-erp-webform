﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Web.Script.Serialization;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;
using System.Globalization;

namespace _30shine.GUI.BackEnd.Mess.Salon
{
    public partial class Salon_Add : System.Web.UI.Page
    {
        protected Tbl_Salon OBJ;
        protected string _Code;
        protected bool _IsUpdate = false;
        private bool Perm_Access = false;
        protected List<cls_media> listImg = new List<cls_media>();
        CultureInfo culture = new CultureInfo("vi-VN");
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (!IsPostBack)
            {
                Library.Function.bindSalon(new List<DropDownList>() { Salon }, Perm_Access);

                if (IsUpdate())
                {
                    if (Bind_OBJ())
                    {
                        Bind_City();
                        Bind_District();
                        Bind_SalonBackUp();
                        Bind_macIp();
                        BindSalonType();
                    }
                }
                else
                {
                    Bind_City();
                    Bind_District();
                    BindSalonType();
                    CreatedDate.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
                    ModifiedDate.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
                }

            }
        }

        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        protected void ExcAddOrUpdate(object sender, EventArgs e)
        {
            if (IsUpdate())
            {
                Update();
            }
            else
            {
                Add();
            }
        }

        /// <summary>
        /// addd
        /// </summary>
        private void Add()
        {
            using (var db = new Solution_30shineEntities())
            {
                var obj = new Tbl_Salon();
                var objKQKD = new KetQuaKinhDoanh_Salon();
                int salonBackUpId = Convert.ToInt32(Salon.SelectedValue);
                string macIp = MacIP.Text;
                obj.Name = Name.Text;
                obj.ShortName = ShortName.Text;
                obj.Description = Description.Text;
                obj.SMS_Signature = SMS_Signature.Text.Trim();
                obj.Email = Email.Text;
                obj.Phone = Phone.Text;
                obj.CityId = Convert.ToInt32(City.SelectedValue);
                obj.DistrictId = Convert.ToInt32(District.SelectedValue);
                obj.Address = Address.Text;
                obj.Images = Array.IndexOf(new string[] { "", "[]" }, HDF_Images.Value) == -1 ? HDF_Images.Value : "";
                obj.ManagerName = SalonManagerName.Text.Trim();
                obj.Fanpage = Fanpage.Text.Trim();
                obj.FanpageId = FanpageId.Text.Trim();
                obj.Order = _Order.Text != "" ? Convert.ToInt32(_Order.Text) : 0;
                obj.Publish = ckbPublish.Checked;
                obj.SalonOTP = ckbSalonOTP.Checked;
                obj.IsSalonHoiQuan = ckbHoiQuan.Checked;
                obj.CreatedDate = Convert.ToDateTime(CreatedDate.Text, culture);
                obj.IsDelete = 0;
                obj.Longitude = Longitude.Text != "" ? Convert.ToDouble(Longitude.Text) : 0;
                obj.Latitude = Latitude.Text != "" ? Convert.ToDouble(Latitude.Text) : 0;
                obj.Type = Convert.ToInt32(ddlSalonType.SelectedValue);
                // Validate
                var Error = false;

                if (!Error)
                {
                    db.Tbl_Salon.Add(obj);
                    var exc = db.SaveChanges();

                    var countAll = db.Salon_Service.Where(p => p.SalonId == obj.Id).ToList();
                    if (countAll.Count() == 0)
                    {
                        var listService = db.Services.Where(p => p.IsDelete != 1 && p.Publish == 1).ToList();
                        foreach (var item in listService)
                        {
                            var checkS = db.Salon_Service.Where(p => p.ServiceId == obj.Id && p.SalonId == item.Id).ToList();
                            if (checkS.Count() == 0)
                            {
                                var salonser = new Salon_Service();
                                salonser.SalonId = obj.Id;
                                salonser.ServiceId = item.Id;
                                salonser.Price = item.Price;
                                salonser.HeSoHL = item.CoefficientRating;
                                salonser.IsCheck = false;
                                salonser.IsDelete = false;
                                db.Salon_Service.Add(salonser);
                            }
                        }
                        db.SaveChanges();
                    }

                    if (exc > 0)
                    {
                        var newSalon = db.Database.SqlQuery<Tbl_Salon>("select top 1 * from tbl_Salon order by Id DESC").SingleOrDefault();
                        objKQKD.SalonId = newSalon.Id;
                        objKQKD.SalonName = newSalon.Name;
                        objKQKD.CreatedTime = DateTime.Now;
                        objKQKD.IsDelete = false;
                        objKQKD.Meta = "salon";
                        objKQKD.Order = newSalon.Id - 1;
                        objKQKD.Publish = newSalon.Publish;
                        objKQKD.IsSalonHoiQuan = newSalon.IsSalonHoiQuan;
                        objKQKD.ShortName = newSalon.ShortName;
                        db.KetQuaKinhDoanh_Salon.Add(objKQKD);
                        db.SaveChanges();
                        //add salon backup
                        if (newSalon.Id > 0 && salonBackUpId > 0)
                        {
                            AddSalonBackUp(newSalon.Id, salonBackUpId);
                        }
                        //add macip
                        if (newSalon.Id > 0 && macIp != null)
                        {
                            AddMacIp(newSalon.Id, newSalon.Name, macIp);
                        }
                        // Thông báo thành công
                        var MsgParam = new List<KeyValuePair<string, string>>();
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thành công!"));
                        UIHelpers.Redirect("/admin/salon/" + obj.Id + ".html", MsgParam);
                    }
                    else
                    {
                        var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển.";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                    }
                }
            }
        }

        /// <summary>
        /// Update
        /// </summary>
        private void Update()
        {
            using (var db = new Solution_30shineEntities())
            {
                int Id;
                var objKQKD = new KetQuaKinhDoanh_Salon();
                if (int.TryParse(_Code, out Id))
                {
                    OBJ = db.Tbl_Salon.FirstOrDefault(w => w.Id == Id);

                    if (!OBJ.Equals(null))
                    {
                        int salonBackUpId = Convert.ToInt32(Salon.SelectedValue);
                        string macIp = MacIP.Text;
                        OBJ.Name = Name.Text;
                        OBJ.ShortName = ShortName.Text;
                        OBJ.Description = Description.Text;
                        OBJ.SMS_Signature = SMS_Signature.Text.Trim();
                        OBJ.Email = Email.Text;
                        OBJ.Phone = Phone.Text;
                        OBJ.CityId = Convert.ToInt32(City.SelectedValue);
                        OBJ.DistrictId = Convert.ToInt32(District.SelectedValue);
                        OBJ.Address = Address.Text;
                        OBJ.Images = Array.IndexOf(new string[] { "", "[]" }, HDF_Images.Value) == -1 ? HDF_Images.Value : "";
                        OBJ.ManagerName = SalonManagerName.Text.Trim();
                        OBJ.Fanpage = Fanpage.Text.Trim();
                        OBJ.FanpageId = FanpageId.Text.Trim();
                        OBJ.Order = _Order.Text != "" ? Convert.ToInt32(_Order.Text) : 0;
                        OBJ.Publish = ckbPublish.Checked;
                        OBJ.IsSalonHoiQuan = ckbHoiQuan.Checked;
                        OBJ.SalonOTP = ckbSalonOTP.Checked;
                        OBJ.CreatedDate = Convert.ToDateTime(CreatedDate.Text, culture);
                        OBJ.ModifiedDate = DateTime.Now;
                        OBJ.IsDelete = 0;
                        OBJ.Longitude = Longitude.Text != "" ? Convert.ToDouble(Longitude.Text) : 0;
                        OBJ.Latitude = Longitude.Text != "" ? Convert.ToDouble(Latitude.Text) : 0;
                        OBJ.Type = Convert.ToInt32(ddlSalonType.SelectedValue);
                        db.Tbl_Salon.AddOrUpdate(OBJ);
                        var exc = db.SaveChanges();
                        //update salonbackup
                        if (OBJ.Id > 0 && salonBackUpId > 0)
                        {
                            UpdateSalonBackUp(OBJ.Id, salonBackUpId);
                        }
                        if (OBJ.Id > 0 && macIp != null)
                        {
                            UpdateMacIp(OBJ.Id, OBJ.Name, macIp);
                        }
                        if (OBJ.Id > 0 && salonBackUpId == 0)
                        {
                            UpdateSalonBackUpNotContent(OBJ.Id, salonBackUpId);
                        }
                        if (exc > 0)
                        {
                            //Update salon-service
                            var countAll = db.Salon_Service.Where(p => p.SalonId == OBJ.Id).ToList();
                            if (countAll.Count() == 0)
                            {
                                var listService = db.Services.Where(p => p.IsDelete != 1 && p.Publish == 1).ToList();
                                foreach (var item in listService)
                                {
                                    var salonser = db.Salon_Service.FirstOrDefault(p => p.ServiceId == item.Id && p.SalonId == OBJ.Id);
                                    if (salonser != null)
                                    {
                                        salonser.SalonId = OBJ.Id;
                                        salonser.ServiceId = item.Id;
                                        salonser.Price = item.Price;
                                        salonser.HeSoHL = item.CoefficientRating;
                                        if (salonser.IsCheck != null && salonser.IsCheck == false)
                                            salonser.IsCheck = false;
                                        salonser.IsDelete = false;
                                        db.Salon_Service.AddOrUpdate(salonser);
                                    }
                                }
                                db.SaveChanges();
                            }

                            objKQKD = db.KetQuaKinhDoanh_Salon.FirstOrDefault(w => w.SalonId == Id);
                            if (objKQKD != null)
                            {
                                objKQKD.SalonId = OBJ.Id;
                                objKQKD.SalonName = OBJ.Name;
                                objKQKD.ModifiedTime = DateTime.Now;
                                objKQKD.Meta = "salon";
                                objKQKD.Order = OBJ.Order;
                                objKQKD.Publish = OBJ.Publish;
                                objKQKD.IsSalonHoiQuan = OBJ.IsSalonHoiQuan;
                                objKQKD.ShortName = OBJ.ShortName;
                                db.KetQuaKinhDoanh_Salon.AddOrUpdate(objKQKD);
                                db.SaveChanges();
                            }
                            else
                            {
                                objKQKD = new KetQuaKinhDoanh_Salon();
                                objKQKD.SalonId = OBJ.Id;
                                objKQKD.SalonName = OBJ.Name;
                                objKQKD.CreatedTime = DateTime.Now;
                                objKQKD.IsDelete = false;
                                objKQKD.Meta = "salon";
                                objKQKD.Order = OBJ.Order;
                                objKQKD.Publish = OBJ.Publish;
                                objKQKD.ShortName = OBJ.ShortName;
                                db.KetQuaKinhDoanh_Salon.AddOrUpdate(objKQKD);
                                db.SaveChanges();
                            }

                            // Thông báo thành công
                            var MsgParam = new List<KeyValuePair<string, string>>();
                            MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                            MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thành công!"));
                            UIHelpers.Redirect("/admin/salon/" + OBJ.Id + ".html", MsgParam);
                        }
                        else
                        {
                            var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển!";
                            var status = "warning";
                            UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                        }
                    }
                    else
                    {
                        var msg = "Lỗi! Không tìm thấy danh mục.";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);

                    }
                }
                else
                {
                    MsgSystem.Text = "Lỗi! Không tìm thấy danh mục.";
                    MsgSystem.CssClass = "msg-system warning";
                }

                Context.RewritePath("+", "", "");
            }
        }

        private bool Bind_OBJ()
        {
            using (var db = new Solution_30shineEntities())
            {
                var ExistOBJ = true;
                int Id;
                if (int.TryParse(_Code, out Id))
                {
                    OBJ = db.Tbl_Salon.Where(w => w.Id == Id).FirstOrDefault();
                    if (!OBJ.Equals(null))
                    {
                        Name.Text = OBJ.Name;
                        ShortName.Text = OBJ.ShortName;
                        Description.Text = OBJ.Description;
                        SMS_Signature.Text = OBJ.SMS_Signature;
                        Phone.Text = OBJ.Phone;
                        Email.Text = OBJ.Email;
                        Address.Text = OBJ.Address;
                        SalonManagerName.Text = OBJ.ManagerName;
                        Fanpage.Text = OBJ.Fanpage;
                        FanpageId.Text = OBJ.FanpageId;
                        CreatedDate.Text = string.Format("{0:dd/MM/yyyy}", OBJ.CreatedDate);
                        ModifiedDate.Text = string.Format("{0:dd/MM/yyyy}", OBJ.ModifiedDate);
                        _Order.Text = Convert.ToString(OBJ.Order);
                        Longitude.Text = OBJ.Longitude.ToString();
                        Latitude.Text = OBJ.Latitude.ToString();
                        if (OBJ.Images != null && OBJ.Images != "")
                        {
                            var serialize = new JavaScriptSerializer();
                            listImg = serialize.Deserialize<List<cls_media>>(OBJ.Images).ToList();
                        }

                        if (OBJ.Publish == true)
                        {
                            ckbPublish.Checked = true;
                        }
                        else
                        {
                            ckbPublish.Checked = false;
                        }

                        if (OBJ.IsSalonHoiQuan == true)
                        {
                            ckbHoiQuan.Checked = true;
                        }
                        else
                        {
                            ckbHoiQuan.Checked = false;
                        }
                        if (OBJ.SalonOTP == true)
                        {
                            ckbSalonOTP.Checked = true;
                        }
                        else
                        {
                            ckbSalonOTP.Checked = false;
                        }
                    }
                    else
                    {
                        ExistOBJ = false;
                        var msg = "Lỗi! Kiểu nhân viên không tồn tại";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                    }
                }
                else
                {
                    ExistOBJ = false;
                    var msg = "Lỗi! Kiểu nhân viên không tồn tại";
                    var status = "warning";
                    UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                }

                return ExistOBJ;
            }
        }

        private void Bind_SalonBackUp()
        {
            var _Code = Convert.ToInt32(Request.QueryString["Code"]);
            using (var db = new Solution_30shineEntities())
            {
                var data = db.Booking_SalonBackup.FirstOrDefault(a => a.SalonId == _Code && a.IsDelete == false);
                if (data != null)
                {
                    Salon.SelectedValue = Convert.ToString(data.SalonBackupId);
                }
            }
        }

        private void Bind_macIp()
        {
            var _Code = Convert.ToInt32(Request.QueryString["Code"]);
            using (var db = new Solution_30shineEntities())
            {
                var data = db.Devices.FirstOrDefault(a => a.OwnerId == _Code && a.IsDelete == false);
                if (data != null)
                {
                    MacIP.Text = Convert.ToString(data.ImeiOrMacIP);
                }
            }
        }

        protected void Bind_City()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.TinhThanhs.OrderBy(p => p.ThuTu).ToList();

                City.DataTextField = "TenTinhThanh";
                City.DataValueField = "Id";

                City.DataSource = lst;
                City.DataBind();

                if (OBJ != null && OBJ.CityId != null)
                {
                    var ItemSelected = City.Items.FindByValue(OBJ.CityId.ToString());
                    if (ItemSelected != null)
                        ItemSelected.Selected = true;
                }
            }
        }

        protected void Bind_District()
        {
            using (var db = new Solution_30shineEntities())
            {
                int Id = Convert.ToInt32(City.SelectedValue);
                if (Id != 0)
                {
                    var lst = db.QuanHuyens.Where(w => w.TinhThanhID == Id).OrderBy(o => o.ThuTu).ToList();

                    District.DataTextField = "TenQuanHuyen";
                    District.DataValueField = "Id";

                    District.DataSource = lst;
                    District.DataBind();
                }

                if (!IsPostBack && OBJ != null && OBJ.DistrictId != null)
                {
                    var ItemSelected = District.Items.FindByValue(OBJ.DistrictId.ToString());
                    if (ItemSelected != null)
                    {
                        ItemSelected.Selected = true;
                    }
                    else
                    {
                        District.SelectedIndex = 0;
                    }
                }
                else
                {
                    District.SelectedIndex = 0;
                }
            }
        }

        /// <summary>
        /// Get salon type
        /// </summary>
        protected void BindSalonType()
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var data = db.Tbl_Config.Where(a => a.Key == "salon_type" && a.IsDelete == 0).ToList();
                    ddlSalonType.DataTextField = "Label";
                    ddlSalonType.DataValueField = "Value";
                    ListItem item = new ListItem("Chọn loại salon", "0");
                    ddlSalonType.DataSource = data;
                    ddlSalonType.DataBind();
                    ddlSalonType.Items.Insert(0, item);
                    if (OBJ != null && OBJ.Type != null)
                    {
                        var itemSelected = ddlSalonType.Items.FindByValue(OBJ.Type.ToString());
                        if (itemSelected != null)
                        {
                            itemSelected.Selected = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void Reload_District(object sender, EventArgs e)
        {
            Bind_District();
        }

        public void ReloadPage()
        {
            HttpRequest Request = HttpContext.Current.Request;
            string host = "http://" + Request.Url.Host.ToString();
            string rawUrl = Request.RawUrl.ToString();
            string Url = host + rawUrl;
            Response.Redirect(Url);
        }

        private bool IsUpdate()
        {
            _Code = Request.QueryString["Code"];
            if (_Code != null)
            {
                _IsUpdate = true;
                return true;
            }
            else
            {
                _IsUpdate = false;
                return false;
            }
        }

        /// <summary>
        /// add SalonBackUpId
        /// </summary>
        /// <param name="salonId"></param>
        /// <param name="salonBackUpId"></param>
        private void AddSalonBackUp(int salonId, int salonBackUpId)
        {
            Booking_SalonBackup obj = new Booking_SalonBackup();
            using (var db = new Solution_30shineEntities())
            {
                if (salonId > 0 && salonBackUpId > 0)
                {
                    obj = new Booking_SalonBackup();
                    obj.SalonId = salonId;
                    obj.SalonBackupId = salonBackUpId;
                    obj.IsDelete = false;
                    db.Booking_SalonBackup.Add(obj);
                    db.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Update salon backup
        /// </summary>
        /// <param name="salonId"></param>
        /// <param name="salonBackUpId"></param>
        private void UpdateSalonBackUp(int salonId, int salonBackUpId)
        {
            using (var db = new Solution_30shineEntities())
            {

                if (salonId > 0 && salonBackUpId > 0)
                {
                    // check exist
                    var data = db.Booking_SalonBackup.FirstOrDefault(a => a.SalonId == salonId);
                    if (data != null)
                    {
                        data.SalonId = salonId;
                        data.SalonBackupId = salonBackUpId;
                        data.IsDelete = false;
                        db.SaveChanges();
                    }
                    else
                    {
                        Booking_SalonBackup obj = new Booking_SalonBackup();
                        obj.SalonId = salonId;
                        obj.SalonBackupId = salonBackUpId;
                        obj.IsDelete = false;
                        db.Booking_SalonBackup.Add(obj);
                        db.SaveChanges();
                    }
                }

            }
        }

        /// <summary>
        /// Update salon backup not content
        /// </summary>
        /// <param name="salonId"></param>
        /// <param name="salonBackUpId"></param>
        private void UpdateSalonBackUpNotContent(int salonId, int salonBackUpId)
        {
            using (var db = new Solution_30shineEntities())
            {
                var data = db.Booking_SalonBackup.FirstOrDefault(a => a.SalonId == salonId && a.IsDelete == false);
                if (data != null)
                {
                    data.SalonId = salonId;
                    data.SalonBackupId = salonBackUpId;
                    data.IsDelete = true;
                    db.SaveChanges();
                }
            }
        }

        /// <summary>
        /// add macip
        /// </summary>
        /// <param name="salonId"></param>
        /// <param name="macIp"></param>
        private void AddMacIp(int salonId, string salonName, string macIp)
        {
            Device obj = new Device();
            using (var db = new Solution_30shineEntities())
            {
                if (salonId > 0 && macIp != null)
                {
                    obj = new Device();
                    obj.OwnerId = salonId;
                    obj.DeviceName = salonName;
                    obj.ImeiOrMacIP = macIp;
                    obj.IsDelete = false;
                    obj.OwnerType = 0;
                    obj.CreatedDate = DateTime.Now;
                    db.Devices.Add(obj);
                    db.SaveChanges();
                }
            }
        }

        /// <summary>
        /// update macip
        /// </summary>
        /// <param name="salonId"></param>
        /// <param name="salonName"></param>
        /// <param name="macIp"></param>
        private void UpdateMacIp(int salonId, string salonName, string macIp)
        {
            using (var db = new Solution_30shineEntities())
            {
                if (salonId > 0 && macIp != null)
                {
                    var data = db.Devices.FirstOrDefault(a => a.OwnerId == salonId && a.IsDelete == false);
                    if (data != null)
                    {
                        data.OwnerId = salonId;
                        data.DeviceName = salonName;
                        data.ImeiOrMacIP = macIp;
                        data.ModifiedDate = DateTime.Now;
                        data.IsDelete = false;
                        data.OwnerType = 0;
                        db.SaveChanges();
                    }
                    else
                    {
                        Device obj = new Device();
                        obj.OwnerId = salonId;
                        obj.DeviceName = salonName;
                        obj.ImeiOrMacIP = macIp;
                        obj.IsDelete = false;
                        obj.OwnerType = 0;
                        obj.CreatedDate = DateTime.Now;
                        db.Devices.Add(obj);
                        db.SaveChanges();
                    }
                }
            }
        }

        public class cls_media
        {
            public string url { get; set; }
            public string title { get; set; }
            public string description { get; set; }
        }
    }
}