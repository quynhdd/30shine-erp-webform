﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;
using System.Web.Services;

namespace _30shine.GUI.BackEnd.Mess.Salon
{
    public partial class Salon_Listing : System.Web.UI.Page
    {
        protected Paging PAGING = new Paging();
        private bool Perm_Access = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (!IsPostBack)
            {
                Bind_Paging();
                Bind_Rpt();
                RemoveLoading();
            }
        }
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        /// <summary>
        ///  get data
        /// </summary>
        private void Bind_Rpt()
        {
            using (var db = new Solution_30shineEntities())
            {
                //var LST = db.Tbl_Salon.Where(w => w.IsDelete == 0).OrderBy(o => o.Order).Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                var LST = (from a in db.Tbl_Salon
                           join b in db.Booking_SalonBackup on a.Id equals b.SalonId into c
                           from d in c.DefaultIfEmpty()
                           where a.IsDelete == 0 //&& d.IsDelete == false
                           select new
                           {
                               Id = a.Id,
                               Name = a.Name,
                               ShortName = a.ShortName,
                               Address = a.Address,
                               Description = a.Description,
                               Phone = a.Phone,
                               Publish = a.Publish,
                               SalonOTP = a.SalonOTP,
                               Order = a.Order,
                               IsSalonHoiQuan = a.IsSalonHoiQuan,
                               SalonBackupId = d.SalonBackupId ?? 0,
                               Type = a.Type ?? 0
                           })
                           .OrderBy(o => o.Order)
                           .Skip(PAGING._Offset)
                           .Take(PAGING._Segment)
                           .ToList();
                Rpt.DataSource = LST;
                Rpt.DataBind();
            }

        }

        /// <summary>
        /// Get name Salon BackUp
        /// </summary>
        /// <param name="SalonBackupId"></param>
        /// <returns></returns>
        public string GetName(int SalonBackupId)
        {
            var data = new List<Tbl_Salon>();
            try
            {
                if (SalonBackupId > 0)
                {
                    using (var db = new Solution_30shineEntities())
                    {
                        data = db.Tbl_Salon.Where(a => a.Id == SalonBackupId).ToList();
                        foreach (var item in data)
                        {
                            return item.Name.ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return "";
        }

        /// <summary>
        /// get name salon type
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public string GetSalonType(int type)
        {
            try
            {
                if (type > 0)
                {
                    using (var db = new Solution_30shineEntities())
                    {
                        var data = db.Tbl_Config.Where(a => a.Key == "salon_type" && a.IsDelete == 0).ToList();
                        foreach (var item in data.Where(a => a.Value == type.ToString()))
                        {
                            return item.Label.ToString();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }

            return "";
        }

        [WebMethod]
        public static object checkPublish(int Id, bool isCheckedPulish)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var data = new Tbl_Salon();
                    if (Id > 0)
                    {
                        data = (from c in db.Tbl_Salon
                                where c.Id == Id
                                select c).FirstOrDefault();
                        data.Publish = isCheckedPulish;
                        db.SaveChanges();

                    }
                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [WebMethod]
        public static object checkHoiQuan(int Id, bool isCheckedSalonHoiQuan)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var data = new Tbl_Salon();
                    if (Id > 0)
                    {
                        data = (from c in db.Tbl_Salon
                                where c.Id == Id
                                select c).FirstOrDefault();
                        data.IsSalonHoiQuan = isCheckedSalonHoiQuan;
                        db.SaveChanges();

                    }
                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [WebMethod]
        public static object checkOTP(int Id, bool isCheckedSalonOTP)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var data = new Tbl_Salon();
                    if (Id > 0)
                    {
                        data = (from c in db.Tbl_Salon
                                where c.Id == Id
                                select c).FirstOrDefault();
                        data.SalonOTP = isCheckedSalonOTP;
                        db.SaveChanges();

                    }
                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            Bind_Paging();
            Bind_Rpt();
            RemoveLoading();
        }

        protected void Bind_Paging()
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage() : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage()
        {
            using (var db = new Solution_30shineEntities())
            {
                var Count = db.Tbl_Salon.Count(w => w.IsDelete != 1);
                int TotalRow = Count - PAGING._TopNewsNum;
                int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
                return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
            }
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }
    }
}