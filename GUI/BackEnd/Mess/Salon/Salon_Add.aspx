﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Salon_Add.aspx.cs" Inherits="_30shine.GUI.BackEnd.Mess.Salon.Salon_Add" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="ProductAdd" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý Salon &nbsp;&#187; </li>
                        <li class="li-listing"><a href="/admin/salon.html">Danh sách</a></li>
                        <% if (_IsUpdate)
                            { %>
                        <li class="li-add"><a href="/admin/salon/them-moi.html">Thêm mới</a></li>
                        <li class="li-edit active"><a href="/admin/salon/<%= _Code %>.html">Cập nhật</a></li>
                        <% }
                            else
                            { %>
                        <li class="li-add active"><a href="/admin/salon/them-moi.html">Thêm mới</a></li>
                        <% } %>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add">
            <%-- Add --%>
            <div class="wp960 content-wp">
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <div class="table-wp">
                    <table class="table-add admin-product-table-add">
                        <tbody>
                            <tr class="title-head">
                                <td><strong>Thông tin salon</strong></td>
                                <td></td>
                            </tr>
                            <tr class="tr-field-ahalf">
                                <td class="col-xs-3 left"><span>Tên salon /Tên viết tắt</span></td>
                                <td class="col-xs-9 right">
                                    <div class="field-wp">
                                        <div class="div_col">
                                            <asp:TextBox ID="Name" runat="server" ClientIDMode="Static" Style="width: 49%" placeholder="Nhập tên Salon"></asp:TextBox>
                                        </div>
                                        <div class="div_col">
                                            <asp:TextBox ID="ShortName" runat="server" ClientIDMode="Static" placeholder="Nhập tên viết tắt" Style="margin-left: 10px"></asp:TextBox>
                                        </div>
                                    </div>

                                </td>
                            </tr>
                            <tr class="tr-field-ahalf">
                                <td class="col-xs-3 left"><span>SalonBackup /MacIP</span></td>
                                <td class="col-xs-9 right">
                                    <div class="field-wp">
                                        <div class="div_col">
                                            <asp:DropDownList ID="Salon" CssClass="form-control select" runat="server" ClientIDMode="Static" Style="width: 49%">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="div_col">
                                            <asp:TextBox ID="MacIP" Style="margin-left: 10px" runat="server" ClientIDMode="Static" placeholder="Nhập Imei hoặc MacIP" onkeypress="return ValidateKeypress(/\d/,event);"></asp:TextBox>

                                        </div>
                                    </div>

                                </td>
                            </tr>
                            <tr class="tr-field-ahalf">
                                <td class="col-xs-3 left"><span>Tọa độ</span></td>
                                <td class="col-xs-9 right">
                                    <div class="field-wp">
                                        <div class="div_col">
                                            <asp:TextBox ID="Longitude" Style="width: 49%" runat="server" ClientIDMode="Static" placeholder="Nhập kinh độ"></asp:TextBox>
                                        </div>
                                        <div class="div_col">
                                            <asp:TextBox ID="Latitude" Style="margin-left: 10px" runat="server" ClientIDMode="Static" placeholder="Nhập vĩ độ"></asp:TextBox>

                                        </div>
                                    </div>

                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-2 left"><span>Địa chỉ</span></td>
                                <td class="col-xs-9 right">
                                    <asp:ScriptManager ID="SM01" runat="server"></asp:ScriptManager>
                                    <asp:UpdatePanel runat="server" ID="UP01">
                                        <ContentTemplate>
                                            <div class="city">
                                                <asp:DropDownList AutoPostBack="true" ID="City" runat="server" CssClass="select"
                                                    OnSelectedIndexChanged="Reload_District" ClientIDMode="Static">
                                                </asp:DropDownList>
                                            </div>
                                            <div class="district">
                                                <asp:DropDownList ID="District" runat="server" CssClass="select" ClientIDMode="Static"></asp:DropDownList>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <%--<asp:AsyncPostBackTrigger ControlID="OrderCloseBtn" EventName="Click"/>--%>
                                        </Triggers>
                                    </asp:UpdatePanel>
                                    <asp:TextBox ID="Address" runat="server" placeholder="Địa chỉ" ClientIDMode="Static"></asp:TextBox>
                                </td>
                            </tr>
                            <tr class="tr-field-ahalf">
                                <td class="col-xs-3 left"><span>Số điện thoại /Email</span></td>
                                <td class="col-xs-9 right">
                                    <div class="field-wp">
                                        <div class="div_col">
                                            <asp:TextBox ID="Phone" runat="server" ClientIDMode="Static"
                                                placeholder="Nhập số điện thoại" Style="width: 49%"></asp:TextBox>
                                        </div>
                                        <div class="div_col">
                                            <asp:TextBox ID="Email" Style="margin-left: 10px" runat="server" ClientIDMode="Static" placeholder="Nhập email"></asp:TextBox>
                                        </div>
                                    </div>

                                </td>
                            </tr>
                            <tr class="tr-description">
                                <td class="col-xs-2 left"><span>Mô tả</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox TextMode="MultiLine" Rows="5" ID="Description" Style="line-height: 18px; padding-top: 3px;" runat="server" ClientIDMode="Static"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-2 left"><span>Chữ ký SMS</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="SMS_Signature" runat="server" ClientIDMode="Static"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>
                            <tr class="tr-field-ahalf">
                                <td class="col-xs-3 left"><span>Thứ tự/Loại salon</span></td>
                                <td class="col-xs-9 right">
                                    <div class="field-wp">
                                        <div class="div_col">
                                            <asp:TextBox ID="_Order" runat="server" ClientIDMode="Static"
                                                placeholder="Ví dụ : 1" Style="width: 49%"></asp:TextBox>
                                        </div>
                                        <div class="div_col">
                                            <asp:DropDownList ID="ddlSalonType" runat="server" CssClass="select" ClientIDMode="Static" Style="margin-left: 10px"></asp:DropDownList>
                                        </div>
                                    </div>

                                </td>
                            </tr>
                            <tr class="tr-field-ahalf">
                                <td class="col-xs-2 left"><span>Thời gian khởi tạo</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox CssClass="txtDateTime st-head" ID="CreatedDate" runat="server" ClientIDMode="Static"
                                            placeholder=""></asp:TextBox>
                                    </span>
                                </td>
                            </tr>
                            <tr class="tr-field-ahalf">
                                <td class="col-xs-2 left"><span>Thời gian chỉnh sửa cuối cùng</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox CssClass="txtDateTime st-head" ID="ModifiedDate" runat="server" ClientIDMode="Static"
                                            placeholder=""></asp:TextBox>
                                    </span>
                                </td>
                            </tr>
                            <%--Checked="true"--%>
                            <tr>
                                <td class="col-xs-2 left"><span>Lựa chọn salon</span></td>
                                <td class="col-xs-9 right">
                                    <span class="text_type_salon">Publish</span>
                                    <div class="check_salon">
                                        <label class="switch">
                                            <asp:CheckBox ID="ckbPublish" runat="server" Checked="true" ClientIDMode="Static" />
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                    <span class="text_type_salon">Salon hội quán</span>
                                    <div class="check_salon">
                                        <label class="switch">
                                            <asp:CheckBox ID="ckbHoiQuan" runat="server" Checked="false" ClientIDMode="Static" />
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                    <span class="text_type_salon">Salon OTP</span>
                                    <div class="check_salon">
                                        <label class="switch">
                                            <asp:CheckBox ID="ckbSalonOTP" runat="server" Checked="false" ClientIDMode="Static" />
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="text-transform: uppercase;">Cấu hình cho mobile app</td>
                            </tr>
                            <tr class="tr-field-ahalf">
                                <td class="col-xs-2 left"><span>Quản lý salon</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="SalonManagerName" runat="server" ClientIDMode="Static" placeholder="Tên quản lý salon"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>
                            <tr class="tr-field-ahalf">
                                <td class="col-xs-2 left"><span>Fanpage</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="Fanpage" runat="server" ClientIDMode="Static" placeholder="Link fanpage"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>
                            <tr class="tr-field-ahalf">
                                <td class="col-xs-2 left"><span>Fanpage ID</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="FanpageId" runat="server" ClientIDMode="Static" placeholder="VD : 378910098941520"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>
                            <tr class="tr-upload">
                                <td class="col-xs-3 left"><span>Ảnh salon</span></td>
                                <td class="col-xs-7 right">
                                    <div class="wrap btn-upload-wp HDF_Images">
                                        <div id="Btn_UploadImg" class="btn-upload-img" onclick="popupImageIframe('HDF_Images')">Thêm ảnh</div>
                                        <asp:FileUpload ID="UploadFile" ClientIDMode="Static" Style="display: none;"
                                            CssClass="input-file-upload" runat="server" AllowMultiple="true" />
                                        <div class="wrap listing-img-upload title-version">
                                            <% if (listImg.Count > 0)
                                                { %>
                                            <% foreach (var v in listImg)
                                                { %>
                                            <div class="thumb-wp">
                                                <div class="left">
                                                    <img class="thumb" alt="" title="" src="<%= v.url %>" data-img="" style="width: 120px; height: 80px; left: 0px;" />
                                                </div>
                                                <div class="right">
                                                    <input class="thumb-title" type="text" placeholder="Tiêu đề ảnh" value="<%=v.title %>" />
                                                    <textarea class="thumb-des" placeholder="Mô tả"><%=v.description %></textarea>
                                                    <div class="action">
                                                        <div class="action-item action-add" onclick="popupImageIframe('HDF_Images')"><i class="fa fa-plus-square" aria-hidden="true"></i>Thêm ảnh</div>
                                                        <div class="action-item action-delete" onclick="deleteThumbnail($(this), 'HDF_Images')"><i class="fa fa-trash-o" aria-hidden="true"></i>Xóa</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <% } %>
                                            <% } %>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr class="tr-send">
                                <td class="col-xs-2 left"></td>
                                <td class="col-xs-9 right no-border">
                                    <span class="field-wp">
                                        <asp:Panel ID="BtnSend" CssClass="btn-send" runat="server" ClientIDMode="Static">Hoàn Tất</asp:Panel>
                                        <asp:Button ID="Send" CssClass="btn-send" runat="server" Text="Hoàn tất" ClientIDMode="Static" OnClick="ExcAddOrUpdate" OnClientClick="initImgStore('HDF_Images')" Style="display: none;"></asp:Button>
                                    </span>
                                </td>
                            </tr>
                            <asp:HiddenField runat="server" ID="UrlKeyField" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="ImgCoverField" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="HDF_Images" ClientIDMode="Static" />
                        </tbody>
                    </table>
                </div>
            </div>
            <%-- end Add --%>
        </div>

        <script>
            jQuery(document).ready(function () {
                $("#glbAdminContent").addClass("active");
                $("#glbAdminSalon").addClass("active");
                //============================
                // Show System Message
                //============================ 
                var qs = getQueryStrings();
                showMsgSystem(qs["msg_update_message"], qs["msg_update_status"]);
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) { },
                    scrollMonth: false,
                    scrollTime: false,
                    scrollInput: false
                });
            });

            $("input").on("click", function () {
                $("input:checked").val();
            });

        </script>

        <!-- Popup plugin image -->
        <script type="text/javascript">
            function popupImageIframe(StoreImgField) {
                var Width = 960;
                var Height = 560;
                var ImgList = [];
                var iframe = $("<div class='iframe-image-upload' style='width:" + Width + "px; height:" + Height + "px; float:left; display:none;'><iframe src='/GUI/BackEnd/UploadImage/Upload_V2.aspx?Code=img&StoreImgField=" + StoreImgField + "&callback=getThumbFromIframe&imgWidth=1334&imgHeight=750&multi_img=false&Folder=Common' style='width:100%; height: 100%;'></iframe></div");

                $("body").find(".iframe-image-upload").remove().end().append(iframe).find(".iframe-image-upload").openEBPopup();
            }

            window.getThumbFromIframe = function (Imgs, StoreImgField) {
                var imgs = "";
                var thumb = "";
                if (Imgs.length > 0) {
                    for (var i = 0; i < Imgs.length; i++) {
                        thumb += '<div class="thumb-wp">' +
                            '<div class="left">' +
                            '<img class="thumb" alt="" title="" src="' + Imgs[i] + '" data-img=""/>' +
                            '</div>' +
                            '<div class="right">' +
                            '<input class="thumb-title" type="text" placeholder="Tiêu đề ảnh" />' +
                            '<textarea class="thumb-des" placeholder="Mô tả"></textarea>' +
                            '<div class="action">        ' +
                            '<div class="action-item action-add" onclick="popupImageIframe(\'HDF_Images\')"><i class="fa fa-plus-square" aria-hidden="true"></i>Thêm ảnh</div>' +
                            '<div class="action-item action-delete" onclick="deleteThumbnail($(this), \'' + StoreImgField + '\')"><i class="fa fa-trash-o" aria-hidden="true"></i>Xóa</div>' +
                            '</div>' +
                            '</div>' +
                            '</div>';
                    }
                    $("." + StoreImgField).find(".listing-img-upload").append($(thumb));
                    excThumbWidth(120, 120);
                }
                autoCloseEBPopup();
            }

            function imgLink(StoreImgField) {
                var imgs = [], src, title, description;
                $("." + StoreImgField).find(".thumb-wp").each(function () {
                    src = $(this).find("img.thumb").attr("src");
                    title = $(this).find("input.thumb-title").val();
                    description = $(this).find("textarea.thumb-des").val();
                    console.log(title + " - " + description);
                    if (src.trim() != "") {
                        imgs.push({ url: src, thumb: executeThumPath(src), title: title, description: description });
                    }
                });
                return imgs;
            }

            function initImgStore(StoreImgField) {
                $("#" + StoreImgField).val(JSON.stringify(imgLink(StoreImgField)))
            }

            function deleteThumbnail(This, StoreImgField) {
                This.parent().parent().parent().remove();
            }

            function executeThumPath(src) {
                var str = "";
                var fileExtension = "";
                var strLeft = "";
                var strPush = "x350";
                if (src != null && src != "") {
                    var loop = src.length - 1;
                    for (var i = loop; i >= 0; i--) {
                        if (src[i] == ".") {
                            str = src.substring(0, i) + strPush + "." + fileExtension;
                            break;
                        } else {
                            fileExtension = src[i] + fileExtension;
                        }
                    }
                }
                return str;
            }

            // validate
            $("#BtnSend").bind("click", function () {
                var kt = true;
                var error = '';
                var name = $("#Name").val();
                var salonType = $("#ddlSalonType").val();
                if (name === "") {
                    kt = false;
                    error = "Bạn phải nhập tên Salon!";
                    $("#Name").css("border-color", "red");
                }
                else if (salonType === "" || salonType === "0") {
                    kt = false;
                    error = "Bạn phải chọn loại Salon!!";
                    $("#ddlSalonType").css("border-color", "red");
                }
                if (!kt) {
                    ShowMessage('', error, 3);
                }
                else {
                    $("#Send").click();
                }
            });
            // Validate keypress
            function ValidateKeypress(numcheck, e) {
                var keynum, keychar, numcheck;
                if (window.event) {
                    keynum = e.keyCode;
                }
                else if (e.which) {
                    keynum = e.which;
                }
                if (keynum == 8 || keynum == 127 || keynum == null || keynum == 9 || keynum == 0 || keynum == 13) return true;
                keychar = String.fromCharCode(keynum);
                var result = numcheck.test(keychar);
                return result;
            }

        </script>
    </asp:Panel>

    <style>
        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

            .switch input {
                display: none;
            }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

            .slider:before {
                position: absolute;
                content: "";
                height: 26px;
                width: 26px;
                left: 4px;
                bottom: 4px;
                background-color: white;
                -webkit-transition: .4s;
                transition: .4s;
            }

        input:checked + .slider {
            background-color: #ead414;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

            .slider.round:before {
                border-radius: 50%;
            }

        .customer-add .table-add td span.slider.round {
            height: 34px;
        }

        .check_salon {
            float: left;
            margin-right: 5%;
        }

        .customer-add .table-add td span.text_type_salon {
            float: left;
            width: auto;
            margin-right: 15px;
        }
    </style>
</asp:Content>

