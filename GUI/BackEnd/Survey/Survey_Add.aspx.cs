﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.Survey
{
    public partial class Survey_Add : System.Web.UI.Page
    {
        private string PageID = "";
        protected SurveyContent OBJ;
        protected string _Code;
        protected bool _IsUpdate = false;
        private bool Perm_Access = false;
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                ////Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                ////Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //////Perm_Add = permissionModel.GetActionByActionNameAndPageId("Perm_Add", PageID, permission);
                ////Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (!IsPostBack)
            {
                bindSurveyCategory();
                if (IsUpdate())
                {
                    if (Bind_OBJ())
                    {
                        //
                    }
                }
                else
                {
                    // 
                }

            }
        }

        protected void ExcAddOrUpdate(object sender, EventArgs e)
        {
            if (IsUpdate())
            {
                Update();
            }
            else
            {
                Add();
            }
        }

        private void Add()
        {
            using (var db = new Solution_30shineEntities())
            {
                int integer;
                bool _bool;
                var obj = new SurveyContent();
                obj.Content = Content.Text;
                obj.Description = Description.Text;
                obj.Order = _Order.Text != "" ? Convert.ToInt32(_Order.Text) : 0;
                obj.CategoryId = int.TryParse(surveyCategory.SelectedValue, out integer) ? integer : 0;
                obj.IsQuestion = bool.TryParse(QorA.SelectedItem.Value.ToString(), out _bool) ? _bool : false;
                obj.Publish = Publish.Checked;
                obj.CreatedDate = DateTime.Now;
                obj.IsDelete = false;

                // Validate
                var Error = false;

                if (!Error)
                {
                    db.SurveyContents.Add(obj);
                    var exc = db.SaveChanges();

                    if (exc > 0)
                    {
                        var MsgParam = new List<KeyValuePair<string, string>>();
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thành công!"));
                        UIHelpers.Redirect("/admin/khao-sat/them-moi.html", MsgParam);
                    }
                    else
                    {
                        var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển.";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                    }
                }
            }
        }

        private void Update()
        {
            using (var db = new Solution_30shineEntities())
            {
                int Id;
                int integer;
                bool _bool;
                if (int.TryParse(_Code, out Id))
                {
                    OBJ = db.SurveyContents.FirstOrDefault(w => w.Id == Id);

                    if (!OBJ.Equals(null))
                    {
                        OBJ.Content = Content.Text;
                        OBJ.Description = Description.Text;
                        OBJ.Order = _Order.Text != "" ? Convert.ToInt32(_Order.Text) : 0;
                        OBJ.CategoryId = int.TryParse(surveyCategory.SelectedValue, out integer) ? integer : 0;
                        OBJ.IsQuestion = bool.TryParse(QorA.SelectedItem.Value.ToString(), out _bool) ? _bool : false;
                        OBJ.Publish = Publish.Checked;
                        OBJ.ModifiedDate = DateTime.Now;

                        db.SurveyContents.AddOrUpdate(OBJ);
                        var exc = db.SaveChanges();
                        if (exc > 0)
                        {
                            var MsgParam = new List<KeyValuePair<string, string>>();
                            MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                            MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thành công!"));
                            UIHelpers.Redirect("/admin/khao-sat/" + OBJ.Id + ".html", MsgParam);
                        }
                        else
                        {
                            var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển!";
                            var status = "warning";
                            UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                        }
                    }
                    else
                    {
                        var msg = "Lỗi! Không tìm thấy danh mục.";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);

                    }
                }
                else
                {
                    MsgSystem.Text = "Lỗi! Không tìm thấy danh mục.";
                    MsgSystem.CssClass = "msg-system warning";
                }

                Context.RewritePath("+", "", "");
            }
        }

        private bool Bind_OBJ()
        {
            using (var db = new Solution_30shineEntities())
            {
                var ExistOBJ = true;
                int Id;
                if (int.TryParse(_Code, out Id))
                {
                    OBJ = db.SurveyContents.Where(w => w.Id == Id).FirstOrDefault();

                    if (!OBJ.Equals(null))
                    {
                        Content.Text = OBJ.Content;
                        Description.Text = OBJ.Description;
                        _Order.Text = Convert.ToString(OBJ.Order);

                        if (OBJ.Publish == true)
                        {
                            Publish.Checked = true;
                        }
                        else
                        {
                            Publish.Checked = false;
                        }
                        
                        var itemSelected = surveyCategory.Items.FindByValue(OBJ.CategoryId.ToString());
                        if (itemSelected != null)
                        {
                            itemSelected.Selected = true;
                        }

                        itemSelected = QorA.Items.FindByValue(OBJ.IsQuestion.ToString());
                        if (itemSelected != null)
                        {
                            itemSelected.Selected = true;
                        }
                    }
                    else
                    {
                        ExistOBJ = false;
                        var msg = "Lỗi! Nội dung khảo sát này không tồn tại";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                    }
                }
                else
                {
                    ExistOBJ = false;
                    var msg = "Lỗi! Kiểu nhân viên không tồn tại";
                    var status = "warning";
                    UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                }

                return ExistOBJ;
            }
        }

        private void bindSurveyCategory()
        {
            using (var db = new Solution_30shineEntities())
            {
                surveyCategory.DataTextField = "Name";
                surveyCategory.DataValueField = "Id";
                //ListItem item = new ListItem("Chọn danh mục", "0");
                var lst = db.Tbl_Category.Where(w => w.IsDelete != 1 && w.Publish == true && w.Pid == 24).ToList();
                surveyCategory.DataSource = lst;
                surveyCategory.DataBind();
                //surveyCategory.Items.Insert(0, item);
            }
        }

        public void ReloadPage()
        {
            HttpRequest Request = HttpContext.Current.Request;
            string host = "http://" + Request.Url.Host.ToString();
            string rawUrl = Request.RawUrl.ToString();
            string Url = host + rawUrl;
            Response.Redirect(Url);
        }

        private bool IsUpdate()
        {
            _Code = Request.QueryString["Code"];
            if (_Code != null)
            {
                _IsUpdate = true;
                return true;
            }
            else
            {
                _IsUpdate = false;
                return false;
            }
        }

    }
}