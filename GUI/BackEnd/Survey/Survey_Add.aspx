﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Survey_Add.aspx.cs" Inherits="_30shine.GUI.BackEnd.Survey.Survey_Add" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="ProductAdd" ContentPlaceHolderID="CtMain" runat="server">
<asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
<asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

<div class="wp sub-menu">
    <div class="wp960">
        <div class="wp content-wp">
            <ul class="ul-sub-menu" id="subMenu">      
                <li>Quản lý khảo sát &nbsp;&#187; </li>
                <li class="li-listing"><a href="/admin/khao-sat/danh-sach.html">Danh sách</a></li>
                <% if(_IsUpdate){ %>
                    <li class="li-add"><a href="/admin/khao-sat/them-moi.html">Thêm mới</a></li>
                    <li class="li-edit active"><a href="/admin/khao-sat/<%= _Code %>.html">Cập nhật</a></li>
                <% }else{ %>
                    <li class="li-add active"><a href="/admin/khao-sat/them-moi.html">Thêm mới</a></li>
                <% } %>
            </ul>
        </div>
    </div>
</div>

<div class="wp customer-add">
    <%-- Add --%>                
    <div class="wp960 content-wp">
        <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
            <div class="table-wp">
                <table class="table-add admin-product-table-add">
                    <tbody>
                        <tr class="title-head">
                            <td><strong>Thông tin khảo sát</strong></td>
                            <td></td>
                        </tr>
                        <tr class="tr-margin" style="height: 20px;"></tr>
                        <tr>
                            <td class="col-xs-2 left"><span>Nội dung</span></td>
                            <td class="col-xs-9 right">
                                <span class="field-wp">
                                    <asp:TextBox ID="Content" runat="server" ClientIDMode="Static"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="ContentValidate" ControlToValidate="Content" runat="server" CssClass="fb-cover-error" Text="Bạn chưa nhập nội dung!"></asp:RequiredFieldValidator>
                                </span>
                                            
                            </td>
                        </tr>                   

                        <tr class="tr-description">
                            <td class="col-xs-2 left"><span>Mô tả</span></td>
                            <td class="col-xs-9 right">
                                <span class="field-wp">
                                    <asp:TextBox TextMode="MultiLine" Rows="5" ID="Description" runat="server" ClientIDMode="Static"></asp:TextBox>
                                </span>
                            </td>
                        </tr>

                        <tr class="tr-product-category">
                            <td class="col-xs-2 left"><span>Danh mục khảo sát</span></td>
                            <td class="col-xs-9 right">
                                <span class="field-wp">
                                    <asp:DropDownList ID="surveyCategory" runat="server"></asp:DropDownList>
                                </span>
                            </td>
                        </tr>

                        <tr>
                            <td class="col-xs-3 left" style="padding-bottom: 0;"><span style="height: 28px; line-height: 28px;">Câu hỏi hay trả lời ?</span></td>
                            <td class="col-xs-9 right" style="padding-bottom: 0;">
                                <span class="field-wp" style="height: auto; line-height: 28px;">
                                    <asp:RadioButtonList ID="QorA" runat="server" CssClass="staff-servey-mark" ClientIDMode="Static" name="qora">
                                        <asp:ListItem Text="Câu hỏi" Value="True" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="Trả lời" Value="False"></asp:ListItem>
                                    </asp:RadioButtonList>
                                    <asp:RequiredFieldValidator ID="ValidateQorA" ControlToValidate="QorA" runat="server"
                                        CssClass="fb-cover-error" Text="Bạn chưa chọn kiểu câu hỏi hay trả lời!" ClientIDMode="Static"></asp:RequiredFieldValidator>
                                </span>
                            </td>
                        </tr>

                        <tr class="tr-field-ahalf">
                            <td class="col-xs-2 left"><span>Thứ tự sắp xếp</span></td>
                            <td class="col-xs-9 right">
                                <span class="field-wp">
                                    <asp:TextBox ID="_Order" runat="server" ClientIDMode="Static"
                                        placeholder="Ví dụ : 1"></asp:TextBox>
                                </span>
                            </td>
                        </tr>

                        <tr class="tr-product-category">
                            <td class="col-xs-2 left"><span>Publish</span></td>
                            <td class="col-xs-9 right">
                                <span class="field-wp">
                                    <asp:CheckBox ID="Publish" Checked="true" runat="server" />
                                </span>
                            </td>
                        </tr>
                    
                        <tr class="tr-send">
                            <td class="col-xs-2 left"></td>
                            <td class="col-xs-9 right no-border">
                                <span class="field-wp">
                                    <asp:Button ID="Send" CssClass="btn-send" runat="server" Text="Hoàn tất" ClientIDMode="Static" OnClick="ExcAddOrUpdate"></asp:Button>
                                    <%--<span class="btn-send btn-cancel" id="CalcelAddFbcv" style="margin-left: 12px;" >Hủy</span>--%>
                                </span>
                            </td>
                        </tr>
                        <asp:HiddenField runat="server" ID="UrlKeyField"  ClientIDMode="Static"/>
                        <asp:HiddenField runat="server" ID="ImgCoverField"  ClientIDMode="Static"/>
                    </tbody>
                </table>
            </div>
        </div>
    <%-- end Add --%>
</div>

<script>
    jQuery(document).ready(function () {
        $("#glbAdminContent").addClass("active");
        $("#glbAdminSkillLevel").addClass("active");
        //============================
        // Show System Message
        //============================ 
        var qs = getQueryStrings();
        showMsgSystem(qs["msg_update_message"], qs["msg_update_status"]);

    });
</script>

</asp:Panel>
</asp:Content>


