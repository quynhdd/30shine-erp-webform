﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Globalization;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace _30shine.GUI.BackEnd.UIDevice
{
    public partial class Device_Add : System.Web.UI.Page
    {
        private string PageID = "";
        protected Device OBJ;
        protected string _Code;
        protected bool _IsUpdate = false;
        private bool Perm_Access = false;
        protected bool Perm_ViewAllData = false;
        protected bool Perm_Delete = false;
        protected bool Perm_Edit = false;

        private CultureInfo culture = new CultureInfo("vi-VN");
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["User_Id"] == null)
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                if (Request.QueryString["Code"] != null)
                {
                    PageID = "DEVICE_ADD";
                }
                else
                {
                    PageID = "DEVICE_ADD";
                }
                IPermissionModel permissionModel = new PermissionModel();
                var permission = Session["User_Permission"].ToString();
                Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //Perm_Add = permissionModel.GetActionByActionNameAndPageId("Perm_Add", PageID, permission);
                Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                RemoveLoading();
                Library.Function.bindSalon(new List<DropDownList>() { ddlSalon }, Perm_ViewAllData);
                BindStaff();
                if (IsUpdate())
                {
                    if (Bind_OBJ())
                    {
                        DeviceType.Enabled = false;
                        ddlSalon.Enabled = false;
                        ddlStaff.Enabled = false;
                    }
                }
                else
                {
                }

            }
        }

        protected void ExcAddOrUpdate(object sender, EventArgs e)
        {
            if (IsUpdate())
            {
                Update();
            }
            else
            {
                Add();
            }
        }
        private void BindStaff()
        {
            using (var db = new Solution_30shineEntities())
            {
                int integer;
                var salon = int.TryParse(ddlSalon.SelectedValue, out integer) ? integer : 0;
                var staff = new List<Staff>();
                var whereSalon = "";
                var sql = "";
                if (salon > 0)
                {
                    whereSalon = " and SalonId = " + salon;
                }
                sql = @"select * from Staff 
                        where IsDelete = 0 and Active = 1
                        and (isAccountLogin = 0 or isAccountLogin is null)" + whereSalon;
                staff = db.Staffs.SqlQuery(sql).ToList();

                var first = new Staff();
                first.Id = 0;
                first.Fullname = "Chọn nhân viên";
                staff.Insert(0, first);
                ddlStaff.DataTextField = "Fullname";
                ddlStaff.DataValueField = "Id";
                ddlStaff.DataSource = staff;
                ddlStaff.DataBind();
            }
        }
        protected void bindStaffBySalon(object sender, EventArgs e)
        {
            BindStaff();
        }
        private void Add()
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    int integer;
                    var obj = new Device();
                    obj.DeviceName = DeviceName.Text;
                    obj.Description = Note.Text;
                    obj.OwnerType = int.TryParse(DeviceType.SelectedValue, out integer) ? integer : 0;
                    try
                    {
                        switch (obj.OwnerType)
                        {
                            case 0:
                                obj.OwnerId = int.TryParse(ddlSalon.SelectedValue, out integer) ? integer : 0;
                                break;
                            case 1:
                                obj.OwnerId = int.TryParse(ddlStaff.SelectedValue, out integer) ? integer : 0;
                                break;
                        }
                    }
                    catch { }
                    obj.CreatedDate = DateTime.Now;
                    obj.ImeiOrMacIP = IMEIORMAC.Text;
                    Guid keyGuid = new Guid();
                    keyGuid = Guid.NewGuid();
                    obj.UniqueKey = keyGuid.ToString();
                    obj.IsDelete = false;
                    var Error = 0;
                    if (obj.ImeiOrMacIP != "")
                    {
                        var checkIMEi = db.Devices.FirstOrDefault(f => f.ImeiOrMacIP == obj.ImeiOrMacIP);
                        if (checkIMEi != null)
                        {
                            var msg = "IMEI hoặc MACIP đã tồn tại. Bạn vui lòng nhập mã khác.";
                            var status = "msg-system warning";
                            UIHelpers.TriggerJsMsgSystem(Page, msg, status, 500);
                            Error = 1;
                        }
                    }
                    if (Error == 0)
                    {
                        IDeviceModel modelDevice = new DeviceModel();
                        modelDevice.Add(obj);
                        var MsgParam = new List<KeyValuePair<string, string>>();
                        var msg = "Thêm mới thành công!!";
                        var status = "msg-system warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 500);
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thành công!"));
                        UIHelpers.Redirect("/admin/danh-sach/thiet-bi.html", MsgParam);
                    }
                }
            }
            catch
            {
                var msg = "Thêm mới thất bại! Vui lòng liên hệ nhóm phát triển!";
                var status = "warning";
                UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
            }
        }

        private void Update()
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    int Id = 0;
                    int integer;
                    if (int.TryParse(_Code, out Id))
                    {
                        OBJ = db.Devices.FirstOrDefault(w => w.Id == Id);
                        if (OBJ != null)
                        {
                            OBJ.DeviceName = DeviceName.Text;
                            OBJ.Description = Note.Text;
                            try
                            {
                                switch (int.TryParse(DeviceType.SelectedValue, out integer) ? integer : 0)
                                {
                                    case 0:
                                        OBJ.OwnerId = int.TryParse(ddlSalon.SelectedValue, out integer) ? integer : 0;
                                        break;
                                    case 1:
                                        OBJ.OwnerId = int.TryParse(ddlStaff.SelectedValue, out integer) ? integer : 0;
                                        break;
                                }
                            }
                            catch { }
                            OBJ.OwnerType = int.TryParse(DeviceType.SelectedValue, out integer) ? integer : 0;
                            OBJ.ModifiedDate = DateTime.Now;
                            OBJ.ImeiOrMacIP = IMEIORMAC.Text;
                            OBJ.IsDelete = false;
                            var Error = 0;
                            if (OBJ.ImeiOrMacIP != "")
                            {
                                var checkIMEi = db.Devices.FirstOrDefault(f => f.ImeiOrMacIP == OBJ.ImeiOrMacIP && f.Id != OBJ.Id);
                                if (checkIMEi != null)
                                {
                                    var msg = "IMEI hoặc MACIP đã tồn tại. Bạn vui lòng nhập mã khác.";
                                    var status = "msg-system warning";
                                    UIHelpers.TriggerJsMsgSystem(Page, msg, status, 500);
                                    Error = 1;
                                }
                            }

                            if (Error == 0)
                            {
                                IDeviceModel modelDevice = new DeviceModel();
                                modelDevice.Update(OBJ);
                                var MsgParam = new List<KeyValuePair<string, string>>();
                                var msg = "Cập nhật thành công!!";
                                var status = "msg-system warning";
                                UIHelpers.TriggerJsMsgSystem(Page, msg, status, 1000);
                                MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                                MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thành công!"));
                                UIHelpers.Redirect("/admin/danh-sach/thiet-bi.html", MsgParam);
                            }
                            else
                            {
                                var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển!";
                                var status = "warning";
                                UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                            }
                        }
                        else
                        {
                            var msg = "Lỗi! Bản ghi không tồn tại.";
                            var status = "warning";
                            UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                        }
                    }
                    else
                    {
                        MsgSystem.Text = "Lỗi! Bản ghi không tồn tại.";
                        MsgSystem.CssClass = "msg-system warning";
                    }
                    Context.RewritePath("+", "", "");
                }
            }
            catch
            {
                var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển!";
                var status = "warning";
                UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
            }
        }
        /// <summary>
        /// Add Loading Status
        /// </summary>
        public void AddLoading()
        {
            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "loading", "addLoading()", true);
        }
        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }
        private bool Bind_OBJ()
        {
            using (var db = new Solution_30shineEntities())
            {
                var ExistOBJ = true;
                int Id;
                int integer;
                if (int.TryParse(_Code, out Id))
                {
                    OBJ = db.Devices.Where(w => w.Id == Id).FirstOrDefault();
                    if (OBJ != null)
                    {
                        DeviceName.Text = OBJ.DeviceName;
                        Note.Text = OBJ.Description;
                        IMEIORMAC.Text = OBJ.ImeiOrMacIP;
                        DeviceType.SelectedValue = OBJ.OwnerType.ToString();
                        if (OBJ.OwnerType == 0)
                        {
                            ddlSalon.SelectedValue = OBJ.OwnerId.ToString();
                        }
                        else if (OBJ.OwnerType == 1)
                        {
                            var objStaff = db.Staffs.FirstOrDefault(f => f.Id == OBJ.OwnerId);
                            ddlSalon.SelectedValue = (int.TryParse(objStaff.SalonId.ToString(), out integer) ? integer : 0).ToString();
                            ddlStaff.SelectedValue = OBJ.OwnerId.ToString();
                        }
                    }
                    else
                    {
                        ExistOBJ = false;
                        var msg = "Lỗi! Bản ghi không tồn tại";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                    }
                }
                else
                {
                    ExistOBJ = false;
                    var msg = "Lỗi! Bản ghi không tồn tại";
                    var status = "warning";
                    UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                }

                return ExistOBJ;
            }
        }
        private bool IsUpdate()
        {
            _Code = Request.QueryString["Code"];
            if (_Code != null)
            {
                _IsUpdate = true;
                return true;
            }
            else
            {
                _IsUpdate = false;
                return false;
            }
        }
    }
}