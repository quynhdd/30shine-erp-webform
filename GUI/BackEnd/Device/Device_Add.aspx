﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Device_Add.aspx.cs" Inherits="_30shine.GUI.BackEnd.UIDevice.Device_Add" %>


<asp:Content ID="DeviceAdd" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

        <style>
            #success-alert {
                top: 100px;
                right: 10px;
                position: fixed;
                width: 20% !important;
                z-index: 2000;
            }
        </style>
        <div class="alert alert-danger" id="success-alert">
            <span id="msg-alert"></span>
        </div>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý thiết bị &nbsp;&#187; Thiết bị &nbsp;&#187;</li>
                        <li class="li-listing"><a href="/admin/danh-sach/thiet-bi.html">Danh sách</a></li>
                        <li class="li-add active"><a href="/admin/thiet-bi/them-moi.html">Thêm mới</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add">
            <%-- Add --%>
            <div class="wp960 content-wp">
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <div class="table-wp">
                    <table class="table-add admin-product-table-add">
                        <tbody>
                            <tr class="title-head">
                                <td><strong>Thông tin thiết bị</strong></td>
                                <td></td>
                            </tr>
                            <tr class="tr-margin" style="height: 20px;"></tr>
                            <tr>
                                <td class="col-xs-3 left"><span>Tên thiết bị<strong style="color: red"> &nbsp*</strong></span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="DeviceName" runat="server" ClientIDMode="Static"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-3 left"><span>IMEI hoặc MAC-IP<strong style="color: red"> &nbsp*</strong></span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="IMEIORMAC" runat="server" ClientIDMode="Static"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-3 left"><span>Kiểu thiết bị<strong style="color: red"> &nbsp*</strong></span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:DropDownList ID="DeviceType" runat="server" ClientIDMode="Static">
                                            <asp:ListItem Selected="True" Value="2"> Chọn kiểu thiết bị </asp:ListItem>
                                            <asp:ListItem Value="0"> Salon (tablet) </asp:ListItem>
                                            <asp:ListItem Value="1"> Nhân viên (cellphone) </asp:ListItem>
                                        </asp:DropDownList>
                                    </span>
                                </td>
                            </tr>
                            <tr id="DropdownSalon">
                                <td class="col-xs-3 left"><span>Salon<strong style="color: red"> &nbsp*</strong></span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:DropDownList ID="ddlSalon" OnSelectedIndexChanged="bindStaffBySalon" AutoPostBack="true" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                    </span>
                                </td>
                            </tr>
                            <tr id="DropdownStaff">
                                <td class="col-xs-3 left"><span>Staff<strong style="color: red"> &nbsp*</strong></span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:DropDownList ID="ddlStaff" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                    </span>
                                </td>
                            </tr>
                            <tr class="tr-description">
                                <td class="col-xs-3 left"><span>Ghi chú</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox TextMode="MultiLine" Rows="5" ID="Note" runat="server" ClientIDMode="Static" Style="padding: 10px;"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>
                            <tr class="tr-send">
                                <td class="col-xs-3 left"></td>
                                <td class="col-xs-9 right no-border">
                                    <span class="field-wp">
                                         <%--  <asp:Panel ID="btnSend" CssClass="btn-send" runat="server" ClientIDMode="Static" OnClick="ExcAddOrUpdate();">Hoàn Tất</asp:Panel>--%>
                                           <asp:Button ID="btnSend" CssClass="btn-send"  runat="server" Text="Hoàn tất" ClientIDMode="Static" OnClick="ExcAddOrUpdate" OnClientClick="if(!ExcAddOrUpdate()) return false;"></asp:Button>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="map-edit">
                                    <div class="edit-wp">
                                        <asp:Panel CssClass="edit-action-wp action-edit" runat="server" ID="EAedit" Visible="false">
                                            <a class="elm edit-btn" href="/admin/thiet-bi/<%# Eval("Id") %>.html" title="Sửa"></a>
                                        </asp:Panel>
                                        <asp:Panel CssClass="edit-action-wp action-delete" runat="server" ID="EAdelete" Visible="false">
                                            <a class="elm del-btn" onclick="del(this.parentNode.parentNode.parentNode.parentNode,'<%# Eval("Id") %>', '<%# Eval("DeviceName") %>')" href="javascript://" title="Xóa"></a>
                                        </asp:Panel>
                                    </div>
                                </td>
                            </tr>
                            <asp:HiddenField runat="server" ID="UrlKeyField" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="ImgCoverField" ClientIDMode="Static" />
                        </tbody>
                    </table>
                </div>
            </div>
            <%-- end Add --%>
        </div>
        <script>

            jQuery(document).ready(function () {
                $("#glbS4M").addClass("active");
                $("#glbS4MClass").addClass("active");
                //============================
                // Show System Message
                //============================ 
                var qs = getQueryStrings();
                showMsgSystem(qs["msg_update_message"], qs["msg_update_status"]);
            });
            $("#success-alert").hide();
            function ExcAddOrUpdate(This) {
                debugger;
                addLoading();
                var kt = true;
                var error = 'Vui lòng nhập đầy đủ thông tin!';
                var device = $('#DeviceName').val();
                var imei = $('#IMEIORMAC').val();
                var note = $('#Note').val();
                var salon = $('#ddlSalon :selected').val();
                var staff = $('#ddlStaff :selected').val();
                var deviceType = $('#DeviceType :selected').val();
                if (device == "" || device == null) {
                    kt = false;
                    error += " [Tên thiết bị] ";
                    $("#DeviceName").css("border-color", "red");
                    //$("#success-alert").show();
                }
                if (imei == "" || imei == null) {
                    kt = false;
                    error += "[IMEI hoặc MAC-IP] ";
                    $("#IMEIORMAC").css("border-color", "red");
                    $("#success-alert").show();
                }
                if (deviceType == "2" || deviceType == null) {
                    kt = false;
                    error += "[Chọn kiểu thiết bị!], ";
                    $("#DeviceType").css("border-color", "red");
                }
                if (deviceType == "0") {
                    if (salon == "0" || salon == null) {
                        kt = false;
                        error += "[Chọn Salon!], ";
                        $("#ddlSalon").css("border-color", "red");
                    }
                }
                if (deviceType == "1") {
                    if (staff == "0" || staff == null) {
                        kt = false;
                        error += "[Chọn nhân viên!], ";
                        $("#ddlStaff").css("border-color", "red");
                    }
                }
                if (!kt) {
                    $("#success-alert").fadeTo(2000, 2000).slideUp(2000, function () {
                        $("#success-alert").slideUp(2000);
                    });
                    $("#msg-alert").text(error);
                }
                removeLoading();
                return kt;
            }
            $(document).on('ready', function () {
                $("#DropdownSalon").hide();
                $("#DropdownStaff").hide();
                var value = $("#DeviceType option:selected").val();
                if (value == "0") {
                    $("#DropdownSalon").show();
                } else if (value == "1") {
                    removeLoading();
                    $("#DropdownSalon").show();
                    $("#DropdownStaff").show();
                }
                $(function () {
                    $("#DeviceType").click(function () {
                        var value = $("#DeviceType option:selected").val();
                        if (value == "0") {
                            $("#DropdownSalon").show();
                        } else if (value == "1") {
                            removeLoading();
                            $("#DropdownSalon").show();
                            $("#DropdownStaff").show();
                        }
                        else {
                            $("#DropdownSalon").hide();
                            $("#DropdownStaff").hide();
                        }
                    });

                    //============================
                    // Event delete
                    //============================
                    function del(This, code, name) {
                        var code = code || null,
                            name = name || null,
                            Row = This;
                        if (!code) return false;

                        // show EBPopup
                        $(".confirm-yn").openEBPopup();
                        $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn xóa [ " + name + " ] ?");

                        $("#EBPopup .yn-yes").bind("click", function () {
                            $.ajax({
                                type: "POST",
                                url: "/GUI/BackEnd/UIDevice/Device_Add.aspx/Delele",
                                data: '{Code : "' + code + '"}',
                                contentType: "application/json; charset=utf-8",
                                dataType: "json", success: function (response) {
                                    var mission = JSON.parse(response.d);
                                    if (mission.success) {
                                        delSuccess();
                                        Row.remove();
                                    } else {
                                        delFailed();
                                    }
                                },
                                failure: function (response) { alert(response.d); }
                            });
                        });
                        $("#EBPopup .yn-no").bind("click", function () {
                            autoCloseEBPopup(0);
                        });
                    }
                });
            });
        </script>
    </asp:Panel>
</asp:Content>



