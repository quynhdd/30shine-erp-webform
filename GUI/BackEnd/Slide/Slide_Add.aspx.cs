﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Web.Script.Serialization;

namespace _30shine.GUI.BackEnd.Slide
{
    public partial class Slide_Add : System.Web.UI.Page
    {
        protected Api_Slide OBJ;
        protected string _Code;
        protected bool _IsUpdate = false;
        public UIHelpers _UIHelpers = new UIHelpers();
        protected cls_media Img = new cls_media();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (IsUpdate())
                {
                    if (Bind_OBJ())
                    {
                        Bind_Pid();
                    }
                }
                else
                {
                    Bind_Pid();  
                }   
                
            }            
        }

        protected void ExcAddOrUpdate(object sender, EventArgs e)
        {
            if (IsUpdate())
            {
                Update();
            }
            else
            {
                Add();
            }
        }

        private void Add()
        {
            using (var db = new Solution_30shineEntities())
            {
                var obj = new Api_Slide();
                obj.Title = Title.Text;
                obj.Des = Description.Text;
                obj.Img = HDF_Images.Value;
                obj.CategoryId = Convert.ToInt32(Pid.SelectedValue);
                obj.Order = _Order.Text != "" ? Convert.ToInt32(_Order.Text) : 0;
                obj.Publish = Publish.Checked;
                obj.CreatedTime = DateTime.Now;
                obj.IsDelete = false;

                // Validate
                var Error = false;

                if (!Error)
                {
                    db.Api_Slide.Add(obj);
                    var exc = db.SaveChanges();

                    if (exc > 0)
                    {
                        var MsgParam = new List<KeyValuePair<string, string>>();
                        MsgParam.Add(new KeyValuePair<string, string>("msg_add_status", "success"));
                        MsgParam.Add(new KeyValuePair<string, string>("msg_add_message", "Cập nhật thành công!"));
                        UIHelpers.Redirect("/admin/slide/" + obj.Id + ".html", MsgParam);
                    }
                    else
                    {
                        var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển.";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);  
                    }
                }
            }
        }

        private void Update()
        {
            using (var db = new Solution_30shineEntities())
            {
                int Id;
                if (int.TryParse(_Code, out Id))
                {
                    OBJ = db.Api_Slide.FirstOrDefault(w => w.Id == Id);

                    if (!OBJ.Equals(null))
                    {
                        OBJ.Title = Title.Text;
                        OBJ.Des = Description.Text;
                        OBJ.Img = HDF_Images.Value;
                        OBJ.CategoryId = Convert.ToInt32(Pid.SelectedValue);
                        OBJ.Order = _Order.Text != "" ? Convert.ToInt32(_Order.Text) : 0;
                        OBJ.Publish = Publish.Checked;
                        OBJ.ModifiedTime = DateTime.Now;

                        db.Api_Slide.AddOrUpdate(OBJ);
                        var exc = db.SaveChanges();
                        if (exc > 0)
                        {
                            var MsgParam = new List<KeyValuePair<string, string>>();
                            MsgParam.Add(new KeyValuePair<string, string>("msg_add_status", "success"));
                            MsgParam.Add(new KeyValuePair<string, string>("msg_add_message", "Cập nhật thành công!"));
                            UIHelpers.Redirect("/admin/slide/" + OBJ.Id + ".html", MsgParam);
                        }
                        else
                        {
                            var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển!";
                            var status = "warning";
                            UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                        }
                    }
                    else
                    {
                        var msg = "Lỗi! Không tìm thấy danh mục.";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);

                    }
                }
                else
                {
                    MsgSystem.Text = "Lỗi! Không tìm thấy danh mục.";
                    MsgSystem.CssClass = "msg-system warning";
                }

                Context.RewritePath("+", "", "");
            }
        }

        private bool Bind_OBJ()
        {
            using (var db = new Solution_30shineEntities())
            {
                var ExistOBJ = true;
                int Id;
                if (int.TryParse(_Code, out Id))
                {
                    OBJ = db.Api_Slide.Where(w => w.Id == Id).FirstOrDefault();

                    if (!OBJ.Equals(null))
                    {
                        Title.Text = OBJ.Title;
                        Description.Text = OBJ.Des;
                        _Order.Text = Convert.ToString(OBJ.Order);

                        if(OBJ.Img != null && OBJ.Img != "")
                        {
                            var serialize = new JavaScriptSerializer();
                            Img = serialize.Deserialize<cls_media>(OBJ.Img);
                        }

                        if (OBJ.Publish == true)
                        {
                            Publish.Checked = true;
                        }
                        else
                        {
                            Publish.Checked = false;
                        }

                    }
                    else
                    {
                        ExistOBJ = false;
                        var msg = "Lỗi! Danh mục không tồn tại";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                    }
                }
                else
                {
                    ExistOBJ = false;
                    var msg = "Lỗi! Danh mục không tồn tại";
                    var status = "warning";
                    UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                }

                return ExistOBJ;
            }
        }     
   
        public void Bind_Pid()
        {
            using (var db = new Solution_30shineEntities())
            {
                var LST = db.Tbl_Category.Where(w => w.IsDelete != 1 && w.Publish == true && w.Pid == 45).ToList();
                var Key = 0;
                var Count = LST.Count;
                Pid.DataTextField = "CategoryName";
                Pid.DataValueField = "Id";
                ListItem item = new ListItem("Chọn Danh mục", "0");
                Pid.Items.Insert(Key, item);
                Key++;

                if (Count > 0)
                {
                    foreach (var v in LST)
                    {
                        item = new ListItem(v.Name, v.Id.ToString());
                        Pid.Items.Insert(Key, item);
                        Key++;
                    }
                }

                if (_IsUpdate)
                {
                    var ItemSelected = Pid.Items.FindByValue(OBJ.CategoryId.ToString());
                    if (ItemSelected != null)
                        ItemSelected.Selected = true;
                }
                else
                {
                    Pid.SelectedIndex = 0;
                } 
            }
        }

        public void ReloadPage()
        {
            HttpRequest Request = HttpContext.Current.Request;
            string host = "http://" + Request.Url.Host.ToString();
            string rawUrl = Request.RawUrl.ToString();
            string Url = host + rawUrl;
            Response.Redirect(Url);
        }

        private bool IsUpdate()
        {
            _Code = Request.QueryString["Code"];
            if (_Code != null)
            {
                _IsUpdate = true;
                return true;
            }
            else
            {
                _IsUpdate = false;
                return false;
            }
        }

        public class cls_media
        {
            public string url { get; set; }
            public string title { get; set; }
            public string description { get; set; }
        }
    }
}