﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Web.Script.Serialization;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;
using System.Globalization;

namespace _30shine.GUI.BackEnd.NetworkManagerment.HomeNetwork
{
    public partial class HomeNetwork_Add : System.Web.UI.Page
    {
        private string PageID = "";
        protected NetworkOperator OBJ;
        protected string _Code;
        protected bool _IsUpdate = false;
        private bool Perm_Access = false;
        //protected List<cls_media> listImg = new List<cls_media>();
        CultureInfo culture = new CultureInfo("vi-VN");

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (!IsPostBack)
            {

                if (IsUpdate())
                {
                    if (Bind_OBJ())
                    {

                    }
                }
                else
                {

                    CreatedTime.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
                    ModifiedTime.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
                }

            }
        }
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["User_Id"] == null)
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                if (Request.QueryString["Code"] != null)
                {
                    PageID = "SALON_EDIT";
                }
                else
                {
                    PageID = "SALON_ADD";
                }
                IPermissionModel permissionModel = new PermissionModel();
                var permission = Session["User_Permission"];
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        protected void ExcAddOrUpdate(object sender, EventArgs e)
        {
            if (IsUpdate())
            {
                Update();
            }
            else
            {
                Add();
            }
        }

        private void Add()
        {
            using (var db = new Solution_30shineEntities())
            {
                var obj = new NetworkOperator();
                obj.NetworkName = NetworkName.Text;
                obj.ShortName = ShortName.Text;
                obj.Url = Url.Text;
                obj.Description = Description.Text;
                obj.Email = Email.Text;
                obj.Phone = Phone.Text;
                obj.Address = Address.Text;
                obj.IsResend = CkbResend.Checked;
                obj.IsDefault = CkbDefault.Checked;
                obj.CreatedTime = Convert.ToDateTime(CreatedTime.Text, culture);
                obj.IsDelete = false;
                db.NetworkOperators.Add(obj);
                db.SaveChanges();
            }
        }

        private void Update()
        {
            using (var db = new Solution_30shineEntities())
            {
                int Id;
                if (int.TryParse(_Code, out Id))
                {
                    OBJ = db.NetworkOperators.FirstOrDefault(w => w.Id == Id);

                    if (!OBJ.Equals(null))
                    {
                        OBJ.NetworkName = NetworkName.Text;
                        OBJ.ShortName = ShortName.Text;
                        OBJ.Url = Url.Text;
                        OBJ.Description = Description.Text;
                        OBJ.Email = Email.Text;
                        OBJ.Phone = Phone.Text;
                        OBJ.Address = Address.Text;
                        OBJ.IsResend = CkbResend.Checked;
                        OBJ.IsDefault = CkbDefault.Checked;
                        OBJ.CreatedTime = DateTime.ParseExact(CreatedTime.Text, "dd/MM/yyyy", null);
                        OBJ.ModifiedTime = DateTime.Now;
                        OBJ.IsDelete = false;
                        db.NetworkOperators.AddOrUpdate(OBJ);
                        db.SaveChanges();
                        // Thông báo thành công
                        var MsgParam = new List<KeyValuePair<string, string>>();
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thành công!"));
                        UIHelpers.Redirect("/admin/homenetwork/" + OBJ.Id + ".html", MsgParam);
                    }
                    else
                    {
                        var msg = "Lỗi! Không tìm thấy danh mục.";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);

                    }
                }
                else
                {
                    MsgSystem.Text = "Lỗi! Không tìm thấy danh mục.";
                    MsgSystem.CssClass = "msg-system warning";
                }

                Context.RewritePath("+", "", "");
            }
        }

        private bool Bind_OBJ()
        {
            using (var db = new Solution_30shineEntities())
            {
                var ExistOBJ = true;
                int Id;
                if (int.TryParse(_Code, out Id))
                {
                    OBJ = db.NetworkOperators.Where(w => w.Id == Id).FirstOrDefault();

                    if (!OBJ.Equals(null))
                    {
                        NetworkName.Text = OBJ.NetworkName;
                        ShortName.Text = OBJ.ShortName;
                        Url.Text = OBJ.Url;
                        Description.Text = OBJ.Description;
                        Phone.Text = OBJ.Phone;
                        Email.Text = OBJ.Email;
                        Address.Text = OBJ.Address;
                        CreatedTime.Text = string.Format("{0:dd/MM/yyyy}", OBJ.CreatedTime);
                        ModifiedTime.Text = string.Format("{0:dd/MM/yyyy}", OBJ.ModifiedTime);
                        if (OBJ.IsResend == true)
                        {
                            CkbResend.Checked = true;
                        }
                        else
                        {
                            CkbResend.Checked = false;
                        }

                        if (OBJ.IsDefault == true)
                        {
                            CkbDefault.Checked = true;
                        }
                        else
                        {
                            CkbDefault.Checked = false;
                        }
                    }
                    else
                    {
                        ExistOBJ = false;
                        var msg = "Lỗi! Kiểu nhân viên không tồn tại";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                    }
                }
                else
                {
                    ExistOBJ = false;
                    var msg = "Lỗi! Kiểu nhân viên không tồn tại";
                    var status = "warning";
                    UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                }

                return ExistOBJ;
            }
        }


        public void ReloadPage()
        {
            HttpRequest Request = HttpContext.Current.Request;
            string host = "http://" + Request.Url.Host.ToString();
            string rawUrl = Request.RawUrl.ToString();
            string Url = host + rawUrl;
            Response.Redirect(Url);
        }

        private bool IsUpdate()
        {
            _Code = Request.QueryString["Code"];
            if (_Code != null)
            {
                _IsUpdate = true;
                return true;
            }
            else
            {
                _IsUpdate = false;
                return false;
            }
        }

        public class cls_media
        {
            public string url { get; set; }
            public string title { get; set; }
            public string description { get; set; }
        }
    }
}