﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="HomeNetwork_Add.aspx.cs" Inherits="_30shine.GUI.BackEnd.NetworkManagerment.HomeNetwork.HomeNetwork_Add" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý đối tác nhà mạng &nbsp;&#187; </li>
                        <li class="li-listing"><a href="/admin/homenetwork.html">Danh sách</a></li>
                        <% if (_IsUpdate)
                            { %>
                        <li class="li-add"><a href="/admin/homenetwork/them-moi.html">Thêm mới</a></li>
                        <li class="li-edit active"><a href="/admin/homenetwork/<%= _Code %>.html">Cập nhật</a></li>
                        <% }
                            else
                            { %>
                        <li class="li-add active"><a href="/admin/homenetwork/them-moi.html">Thêm mới</a></li>
                        <% } %>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add">
            <%-- Add --%>
            <div class="wp960 content-wp">
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <div class="table-wp">
                    <table class="table-add admin-product-table-add">
                        <tbody>
                            <tr class="title-head">
                                <td><strong>Thông tin nhà mạng</strong></td>
                                <td></td>
                            </tr>
                            <tr class="tr-margin" style="height: 20px;"></tr>
                            <tr>
                                <td class="col-xs-2 left"><span>Tên nhà mạng</span></td>
                                <td class="col-xs-9 right">
                                    <div class="col-xs-7" style="padding-left: 0px;">
                                        <span class="field-wp">
                                            <asp:TextBox ID="NetworkName" runat="server" ClientIDMode="Static"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="NameValidate" ControlToValidate="NetworkName" runat="server" CssClass="fb-cover-error" Text="Bạn chưa nhập tên danh mục!"></asp:RequiredFieldValidator>
                                        </span>
                                    </div>
                                    <div class="col-xs-2"><span style="text-align: right;">Tên tắt</span></div>
                                    <div class="col-xs-3" style="padding-right: 0px;">
                                        <span class="field-wp">
                                            <asp:TextBox ID="ShortName" runat="server" ClientIDMode="Static"></asp:TextBox>
                                        </span>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td class="col-xs-2 left"><span>Địa chỉ</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="Address" runat="server" ClientIDMode="Static"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>

                            <tr>
                                <td class="col-xs-2 left"><span>Url</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="Url" runat="server" ClientIDMode="Static"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>

                            <tr class="tr-field-ahalf">
                                <td class="col-xs-2 left"><span>Số điện thoại</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="Phone" runat="server" ClientIDMode="Static"
                                            placeholder=""></asp:TextBox>
                                    </span>
                                </td>
                            </tr>

                            <tr class="tr-field-ahalf">
                                <td class="col-xs-2 left"><span>Email</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="Email" runat="server" ClientIDMode="Static"
                                            placeholder=""></asp:TextBox>
                                    </span>
                                </td>
                            </tr>

                            <tr class="tr-description">
                                <td class="col-xs-2 left"><span>Mô tả</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox TextMode="MultiLine" Rows="5" ID="Description" Style="line-height: 18px; padding-top: 3px;" runat="server" ClientIDMode="Static"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>

                            <tr class="tr-field-ahalf">
                                <td class="col-xs-2 left"><span>Thời gian khởi tạo</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox CssClass="txtDateTime st-head" ID="CreatedTime" runat="server" ClientIDMode="Static"
                                            placeholder=""></asp:TextBox>
                                    </span>
                                </td>
                            </tr>

                            <tr class="tr-field-ahalf">
                                <td class="col-xs-2 left"><span>Thời gian sửa cuối</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox CssClass="txtDateTime st-head" ID="ModifiedTime" runat="server" ClientIDMode="Static"
                                            placeholder=""></asp:TextBox>
                                    </span>
                                </td>
                            </tr>


                            <tr>
                                <td class="col-xs-2 left"><span>Lựa chọn</span></td>
                                <td class="col-xs-9 right">
                                    <span class="text_type">IsResend</span>
                                    <div class="check_box">
                                        <label class="switch">
                                            <asp:CheckBox ID="CkbResend" runat="server" Checked="true" ClientIDMode="Static" />
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                    <span class="text_type">IsDefault</span>
                                    <div class="check_box">
                                        <label class="switch">
                                            <asp:CheckBox ID="CkbDefault" runat="server" Checked="false" ClientIDMode="Static" />
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                </td>
                            </tr>

                            <tr class="tr-send">
                                <td class="col-xs-2 left"></td>
                                <td class="col-xs-9 right no-border">
                                    <span class="field-wp">
                                        <asp:Button ID="Send" CssClass="btn-send" runat="server" Text="Hoàn tất" ClientIDMode="Static" OnClick="ExcAddOrUpdate" OnClientClick="initImgStore('HDF_Images')"></asp:Button>
                                    </span>
                                </td>
                            </tr>
                            <asp:HiddenField runat="server" ID="UrlKeyField" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="ImgCoverField" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="HDF_Images" ClientIDMode="Static" />
                        </tbody>
                    </table>
                </div>
            </div>
            <%-- end Add --%>
        </div>

        <script>
            jQuery(document).ready(function () {
                $("#glbAdminContent").addClass("active");
                $("#glbAdminSalon").addClass("active");
                //============================
                // Show System Message
                //============================ 
                var qs = getQueryStrings();
                showMsgSystem(qs["msg_update_message"], qs["msg_update_status"]);
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) { },
                    scrollMonth: false,
                    scrollTime: false,
                    scrollInput: false
                });
            });

            $("input").on("click", function () {
                $("input:checked").val();
            });

        </script>

        <!-- Popup plugin image -->
        <script type="text/javascript">

            // Validate keypress
            function ValidateKeypress(numcheck, e) {
                var keynum, keychar, numcheck;
                if (window.event) {
                    keynum = e.keyCode;
                }
                else if (e.which) {
                    keynum = e.which;
                }
                if (keynum == 8 || keynum == 127 || keynum == null || keynum == 9 || keynum == 0 || keynum == 13) return true;
                keychar = String.fromCharCode(keynum);
                var result = numcheck.test(keychar);
                return result;
            }

        </script>
    </asp:Panel>

    <style>
        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

            .switch input {
                display: none;
            }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

            .slider:before {
                position: absolute;
                content: "";
                height: 26px;
                width: 26px;
                left: 4px;
                bottom: 4px;
                background-color: white;
                -webkit-transition: .4s;
                transition: .4s;
            }

        input:checked + .slider {
            background-color: #ead414;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

            .slider.round:before {
                border-radius: 50%;
            }

        .customer-add .table-add td span.slider.round {
            height: 34px;
        }

        .check_box {
            float: left;
            margin-right: 5%;
        }

        .customer-add .table-add td span.text_type {
            float: left;
            width: auto;
            margin-right: 15px;
        }
        .customer-add #Address{
            margin-top:0;
        }
    </style>
</asp:Content>
