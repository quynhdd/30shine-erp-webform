﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="HomeNetwork_Listing.aspx.cs" Inherits="_30shine.GUI.BackEnd.NetworkManagerment.HomeNetwork.HomeNetwork_Listing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý đối tác nhà mạng &nbsp;&#187; </li>
                        <li class="li-listing active"><a href="/admin/homenetwork.html">Danh sách</a></li>
                        <li class="li-add"><a href="/admin/homenetwork/them-moi.html">Thêm mới</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <!-- Row Table Filter -->
                <div class="table-func-panel">
                    <div class="table-func-elm">
                        <span>Số hàng / Page : </span>
                        <div class="table-func-input-wp">
                            <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                            <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                            <ul class="ul-opt-segment">
                                <li data-value="10">10</li>
                                <li data-value="20">20</li>
                                <li data-value="30">30</li>
                                <li data-value="40">40</li>
                                <li data-value="50">50</li>
                                <li data-value="1000000">Tất cả</li>
                            </ul>
                            <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                        </div>
                    </div>
                </div>
                <!-- End Row Table Filter -->
                <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
                <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                    <ContentTemplate>
                        <div class="table-wp">
                            <table class="table-add table-listing">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Nhà mạng</th>
                                        <th>Tên tắt</th>
                                        <th>Url</th>
                                        <th>Địa chỉ</th>
                                        <th>Mô tả</th>
                                        <th>Số điện thoại</th>
                                        <th>Mặc định</th>
                                        <th>Gửi lại tin nhắn</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="Rpt" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# Container.ItemIndex + 1 + PAGING._Offset %></td>
                                                <td>
                                                    <a href="/admin/homenetwork/<%# Eval("Id") %>.html"><%# Eval("NetworkName") %></a>
                                                </td>
                                                <td><%# Eval("ShortName") %></td>
                                                <td><%# Eval("Url") %></td>
                                                <td><%# Eval("Address") %></td>
                                                <td><%# Eval("Description") %></td>
                                                <td><%# Eval("Phone") %></td>
                                                <td class="map-edit">
                                                    <div class="check_box">
                                                        <label class="switch">
                                                            <input id="CbxDefault" type="checkbox"
                                                                <%# Convert.ToInt32(Eval("IsDefault")) == 1 ? "Checked=\"True\"" : "" %>
                                                                onclick="CheckDefault($(this), <%# Eval("Id") %>)" />
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </td>
                                                <td class="map-edit">
                                                    <div class="check_box">
                                                        <label class="switch">
                                                            <input id="CbxResend" type="checkbox"
                                                                <%# Convert.ToInt32(Eval("IsResend")) == 1 ? "Checked=\"True\"" : "" %>
                                                                onclick="CheckResend($(this), <%# Eval("Id") %>)" />
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                    <div class="edit-wp">
                                                        <a class="elm edit-btn" href="/admin/homenetwork/<%# Eval("Id") %>.html" title="Sửa"></a>
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>
                        <!-- Paging -->
                        <div class="site-paging-wp">
                            <% if (PAGING.TotalPage > 1)
                                { %>
                            <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                                <% if (PAGING._Paging.Prev != 0)
                                    { %>
                                <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                                <% } %>
                                <asp:Repeater ID="RptPaging" runat="server">
                                    <ItemTemplate>
                                        <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                            <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>>
                                            <%# Eval("PageNum") %>
                                        </a>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                                    { %>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                                <% } %>
                            </asp:Panel>
                            <% } %>
                        </div>
                        <!-- End Paging -->
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
            </div>
            <%-- END Listing --%>
            <style>
                <style > .switch {
                    position: relative;
                    display: inline-block;
                    width: 60px;
                    height: 34px;
                }

                .switch input {
                    display: none;
                }

                .slider {
                    position: absolute;
                    cursor: pointer;
                    top: 0;
                    left: 0;
                    right: 0;
                    bottom: 0;
                    background-color: #ccc;
                    -webkit-transition: .4s;
                    transition: .4s;
                }

                    .slider:before {
                        position: absolute;
                        content: "";
                        height: 19px;
                        width: 19px;
                        left: 1px;
                        bottom: 2px;
                        background-color: white;
                        transition: .4s;
                    }

                input:checked + .slider {
                    background-color: #ead414;
                }

                input:focus + .slider {
                    box-shadow: 0 0 1px #2196F3;
                }

                input:checked + .slider:before {
                    -webkit-transform: translateX(26px);
                    -ms-transform: translateX(26px);
                    transform: translateX(26px);
                }

                /* Rounded sliders */
                .slider.round {
                    border-radius: 34px;
                }

                    .slider.round:before {
                        border-radius: 50%;
                    }

                .customer-add .table-add td span.slider.round {
                    height: 22px;
                    margin: 0 auto;
                    margin: auto;
                    width: 47px;
                }

                .check_box {
                    float: left;
                    margin-right: 5%;
                }

                .customer-add .table-add td span.text_type {
                    float: left;
                    width: auto;
                    margin-right: 15px;
                }
            </style>
        </div>

        <script>
            jQuery(document).ready(function () {
                $("#glbAdminContent").addClass("active");
                $("#glbAdminSalon").addClass("active");
            });

            //============================
            // Event delete
            //============================
            function del(This, code, name) {
                var code = code || null,
                    name = name || null,
                    Row = This;
                if (!code) return false;

                // show EBPopup
                $(".confirm-yn").openEBPopup();
                $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn xóa [ " + name + " ] ?");

                $("#EBPopup .yn-yes").bind("click", function () {
                    $.ajax({
                        type: "POST",
                        url: "/GUI/SystemService/Ajax/Del.aspx/Delele_Category",
                        data: '{Code : "' + code + '"}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json", success: function (response) {
                            var mission = JSON.parse(response.d);
                            if (mission.success) {
                                delSuccess();
                                Row.remove();
                            } else {
                                delFailed();
                            }
                        },
                        failure: function (response) { alert(response.d); }
                    });
                });
                $("#EBPopup .yn-no").bind("click", function () {
                    autoCloseEBPopup(0);
                });
            }

            var paramClass = function () {
                this.Id = 0;
                this.IsResend = false;
                this.IsDefault = false;
            }
            var paramObject = new paramClass();

        </script>

    </asp:Panel>
</asp:Content>
