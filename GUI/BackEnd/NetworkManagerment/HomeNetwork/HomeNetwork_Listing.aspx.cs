﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;
using System.Web.Services;

namespace _30shine.GUI.BackEnd.NetworkManagerment.HomeNetwork
{
    public partial class HomeNetwork_Listing : System.Web.UI.Page
    {
        private string PageID = "QL_SL";
        protected Paging PAGING = new Paging();
        private bool Perm_Access = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (!IsPostBack)
            {
                Bind_Paging();
                Bind_Rpt();
                RemoveLoading();
            }
        }
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["User_Id"] == null)
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                var permission = Session["User_Permission"];
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        private void Bind_Rpt()
        {
            using (var db = new Solution_30shineEntities())
            {
                var LST = db.NetworkOperators.Where(w => w.IsDelete == false).OrderByDescending(o => o.Id).Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                Rpt.DataSource = LST;
                Rpt.DataBind();
            }

        }

        [WebMethod]
        public static object CheckResend(int Id, bool isCheckedResend)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var data = new NetworkOperator();
                    if (Id > 0)
                    {
                        data = (from c in db.NetworkOperators
                                where c.Id == Id
                                select c).FirstOrDefault();
                        data.IsResend = isCheckedResend;
                        db.SaveChanges();

                    }
                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [WebMethod]
        public static object CheckDefault(int Id, bool isCheckedDefault)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var data = new NetworkOperator();
                    if (Id > 0)
                    {
                        data = (from c in db.NetworkOperators
                                where c.Id == Id
                                select c).FirstOrDefault();
                        data.IsDefault = isCheckedDefault;
                        db.SaveChanges();

                    }
                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            Bind_Paging();
            Bind_Rpt();
            RemoveLoading();
        }

        protected void Bind_Paging()
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage() : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage()
        {
            using (var db = new Solution_30shineEntities())
            {
                var Count = db.NetworkOperators.Count(w => w.IsDelete != true);
                int TotalRow = Count - PAGING._TopNewsNum;
                int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
                return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
            }
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }


    }
}