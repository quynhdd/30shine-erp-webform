﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Globalization;
using System.Web.Script.Serialization;
using LinqKit;
using System.Data.Entity.Migrations;

namespace _30shine.GUI.BackEnd.UpdateDB
{
    public partial class Update_Bill_Familiar : System.Web.UI.Page
    {
        protected Paging PAGING = new Paging();
        protected List<List<ProductBasic>> ProductList;
        private Expression<Func<BillService, bool>> Where = PredicateBuilder.True<BillService>();
        CultureInfo culture = new CultureInfo("vi-VN");

        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        protected bool Perm_ViewAllData = false;
        protected bool Perm_ViewAllDate = false;
        private bool Perm_Access = false;
        protected bool IsAccountant = false;
        List<_30shine.MODEL.ENTITY.EDMX.Staff> _LstNhanVien;
        protected string Day1 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-2));
        protected string Day2 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-1));
        protected string Day3 = string.Format("{0:dd/MM/yyyy}", DateTime.Now);

        protected string table = "";

        private string sql = "";
        private string where = "";
        private DateTime timeFrom = new DateTime();
        private DateTime timeTo = new DateTime();
        private int salonId = 0;
        /// <summary>
        /// check permission
        /// </summary>
        protected void checkPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                string perName = Session["User_Permission"].ToString();
                string url = Request.Url.AbsolutePath.ToString();
                Solution_30shineEntities db = new Solution_30shineEntities();
                var pem = (from m in db.Tbl_Permission_Map
                           join mn in db.Tbl_Permission_Menu on m.mID equals mn.mID
                           where m.pName == perName && mn.Url_Main.EndsWith(url) || mn.Url_Rewrite.EndsWith(url)
                           select new { m.aID }
                           ).FirstOrDefault();
                if (pem != null)
                {
                    var pemArray = pem.aID.Split(',');
                    if (Array.IndexOf(pemArray, "1") > -1)
                    {
                        Perm_Access = true;
                        if (Array.IndexOf(pemArray, "2") > -1)
                        {
                            Perm_ViewAllData = true;
                        }
                        if (Array.IndexOf(pemArray, "7") > -1)
                        {
                            Perm_ViewAllDate = true;
                        }
                        if (Array.IndexOf(pemArray, "4") > -1)
                        {
                            Perm_Edit = true;
                        }
                        if (Array.IndexOf(pemArray, "5") > -1)
                        {
                            Perm_Delete = true;
                        }
                    }
                    else
                    {
                        //ContentWrap.Visible = false;
                        //NotAllowAccess.Visible = true;
                    }

                }


            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            Bind_Data();
        }

        private void Bind_Data()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lstStaff = new List<cls_quanlity>();
                var staffQ = new cls_quanlity();
                double point;
                int index = -1;
                int integer;
                int chemistry;
                int timesUsedService = 0;
                var familiar = new List<BillService>();
                var billCus = new List<BillService>();
                bool isSpecial = false;
                int loop = 0;

                sql = @"select *
                        from BillService
                        where IsDelete != 1 and Pending != 1
                        and CreatedDate between '2016/3/11' and '2016/4/1' ";

                var bills = db.BillServices.SqlQuery(sql).ToList();
                if (bills.Count > 0)
                {
                    foreach (var v in bills)
                    {
                        isSpecial = false;
                        /// 1. Tính số lần đã sử dụng dịch vụ
                        sql = @"select COUNT(*) as times
                                from BillService
                                where IsDelete != 1 and Pending != 1
                                and ServiceIds != '' and ServiceIds is not null
                                and CreatedDate < '" + v.CreatedDate + "'" +
                                @"and CustomerCode = '" + v.CustomerCode + "'" +
                                @"group by CustomerCode";

                        timesUsedService = db.Database.SqlQuery<int>(sql).FirstOrDefault();
                        bills[loop].TimesUsedService = timesUsedService;

                        /// 2. Kiểm tra có phải khách quen (là khách đã dùng dịch vụ bởi stylist này ít nhất 2 lần, hoặc 1 lần nhưng là lần gần đây nhất)
                        sql = @"select *
                                from BillService
                                where IsDelete != 1 and Pending != 1
                                and CustomerCode = '" + v.CustomerCode + @"' 
                                and CustomerCode != '' and CustomerCode is not null
                                and CreatedDate < '" + String.Format("{0:yyyy/MM/dd}", v.CreatedDate) + "'";
                        billCus = db.BillServices.SqlQuery(sql).ToList();
                        familiar = billCus.Where(w => w.Staff_Hairdresser_Id == v.Staff_Hairdresser_Id).ToList();
                        // Nếu khách đã sử dụng dịch vụ bởi stylist này ít nhất 2 lần
                        if (familiar.Count > 1)
                        {
                            isSpecial = true;
                        }
                        // hoặc dùng 1 lần nhưng là lần gần đây nhất
                        else if (familiar.Count == 1)
                        {
                            if (billCus[billCus.Count - 1].Staff_Hairdresser_Id == v.Staff_Hairdresser_Id)
                            {
                                isSpecial = true;
                            }
                        }

                        // Cập nhật bill
                        bills[loop].IsCusFamiliar = isSpecial;
                        db.BillServices.AddOrUpdate(bills[loop]);
                        db.SaveChanges();

                        loop++;
                    }
                }
            }
        }
    }

    public class cls_Servey : Tbl_Staff_Survey
    {
        public string salonName { get; set; }
        public string stylistName { get; set; }
        public string skinnerName { get; set; }
        public string threadname { get; set; }
        public string ratingName { get; set; }
    }

    public class cls_quanlity
    {
        public int staffId { get; set; }
        public string staffName { get; set; }
        public int normalBills { get; set; }
        public int specialBills { get; set; }
        public double totalPoint { get; set; }
    }

    public class cls_billservice : BillService
    {
        public string stylistName { get; set; }
        public int ratingPoint { get; set; }
    }
}