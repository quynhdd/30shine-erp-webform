﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Update_Flow_Salary.aspx.cs" Inherits="_30shine.GUI.BackEnd.UpdateDB.Update_Flow_Salary" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Cập nhật bảng store lương theo ngày</title>
    <link href="/Assets/css/jquery.datetimepicker.css" rel="stylesheet" />
    <script src="/Assets/js/jquery.v1.11.1.js"></script>
    <script src="/Assets/js/jquery.datetimepicker.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
        <h3>Cập nhật dữ liệu store lương theo ngày</h3>
        <div>
            <p>Thời gian update : <span runat="server" id="updateStatus"></span></p>
            Chọn ngày:
            <br />
            <asp:TextBox runat="server" ID="txtTimeFrom" CssClass="txtDateTime" placeholder="Từ ngày"></asp:TextBox>
            <asp:TextBox runat="server" ID="txtTimeTo" CssClass="txtDateTime" placeholder="Đến ngày"></asp:TextBox>
            <br />
            <br />
            <div>
                <asp:UpdatePanel runat="server" ID="UpdatePanel1" style="float: left; margin-right: 10px;">
                    <ContentTemplate>
                        <asp:DropDownList runat="server" ID="ddlSalon" CssClass="form-control select" AutoPostBack="true" OnSelectedIndexChanged="updateDdlStaffBySalon"></asp:DropDownList>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:Button runat="server" Text="Cập nhật theo salon" OnClick="updateBySalon" />
            </div>
            <br />
            <div>
                <asp:UpdatePanel runat="server" ID="UpdatePanel2" style="float: left; margin-right: 10px;">
                    <ContentTemplate>
                        <asp:DropDownList runat="server" ID="ddlStaff">
                            <asp:ListItem Text="Chọn nhân viên"></asp:ListItem>
                        </asp:DropDownList>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:Button runat="server" Text="Cập nhật theo nhân viên" OnClick="updateByStaff" />
            </div>
            <br />
            <div>
                <asp:UpdatePanel runat="server" ID="UpdatePanel3" style="float: left; margin-right: 10px;">
                    <ContentTemplate>
                        <asp:DropDownList runat="server" ID="ddlDepartment">
                            <asp:ListItem Text="Chọn bộ phận"></asp:ListItem>
                        </asp:DropDownList>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:Button runat="server" Text="Cập nhật theo bộ phận" OnClick="updateByDepartment" />
            </div>

            <br />
            <p>Cập nhật lương cứng</p>
            <div>
                <asp:Button runat="server" Text="Cập nhật lương cứng" OnClick="updateFixSalary" />
            </div>

            <br />
            <p>Cập nhật lương sự kiện tăng ca x1.5</p>
            <div>
                <asp:Button runat="server" Text="Cập nhật lương" OnClick="UpdateX15" />
            </div>
        </div>
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 190px !important;
                margin-top: 5px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }
        </style>
        <script>
            //============================
            // Datepicker
            //============================
            $('.txtDateTime').datetimepicker({
                dayOfWeekStart: 1,
                lang: 'vi',
                startDate: '2014/10/10',
                format: 'd/m/Y',
                dateonly: true,
                showHour: false,
                showMinute: false,
                timepicker: false,
                onChangeDateTime: function (dp, $input) {
                    //$input.val($input.val().split(' ')[0]);
                }
            });
        </script>
    </form>
</body>
</html>
