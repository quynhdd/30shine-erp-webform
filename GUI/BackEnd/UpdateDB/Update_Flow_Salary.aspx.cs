﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Web.Helpers;
using System.Text.RegularExpressions;
using System.Globalization;
using Project.Helpers;
using Helpers;

namespace _30shine.GUI.BackEnd.UpdateDB
{
    public partial class Update_Flow_Salary : System.Web.UI.Page
    {
        private CultureInfo culture = new CultureInfo("vi-VN");
        public Solution_30shineEntities db = new Solution_30shineEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Library.Function.bindSalon(new List<DropDownList>() { ddlSalon }, true);
                bindDepartment();
            }
        }

        protected void updateDdlStaffBySalon(object sender, EventArgs e)
        {
            using (var db = new Solution_30shineEntities())
            {
                var salonId = 0;
                try
                {
                    salonId = Convert.ToInt32(ddlSalon.SelectedValue);
                }
                catch { }
                var listStaff = db.Staffs.Where(w => w.SalonId == salonId && w.IsDelete != 1 && w.Active == 1).ToList();
                ddlStaff.DataTextField = "Fullname";
                ddlStaff.DataValueField = "Id";
                ddlStaff.DataSource = listStaff;
                ddlStaff.DataBind();
                var item = new ListItem("Chọn nhân viên", "0");
                ddlStaff.Items.Insert(0, item);
            }
        }

        protected void updateFlowSalaryByBill(object sender, EventArgs e)
        {
            using (var db = new Solution_30shineEntities())
            {
                var timeFrom = new DateTime();
                var timeTo = new DateTime();
                var timeTemp = new DateTime();
                if (txtTimeFrom.Text != "")
                {
                    timeFrom = Convert.ToDateTime(txtTimeFrom.Text, culture);
                    if (txtTimeTo.Text != "")
                    {
                        timeTo = Convert.ToDateTime(txtTimeTo.Text, culture).AddDays(1);
                    }
                    else
                    {
                        timeTo = timeFrom.AddDays(1);
                    }

                    /// Init dữ liệu flow salary
                    timeTemp = timeFrom;
                    while (timeTemp < timeTo)
                    {
                        SalaryLib.Instance.initSalaryToday(timeTemp);
                        timeTemp = timeTemp.AddDays(1);
                    }
                    /// Cập nhật dữ liệu theo bill
                    var bills = db.BillServices.Where(w => w.IsDelete != 1 && w.Pending != 1 && w.CompleteBillTime >= timeFrom && w.CompleteBillTime < timeTo).ToList();
                    if (bills.Count > 0)
                    {
                        foreach (var v in bills)
                        {
                            SalaryLib.Instance.updateFlowSalaryByBill(v);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Cập nhật bổ sung
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void updateFlowSalaryByFTK(object sender, EventArgs e)
        {
            using (var db = new Solution_30shineEntities())
            {
                var timeFrom = new DateTime();
                var timeTo = new DateTime();
                if (txtTimeFrom.Text != "")
                {
                    timeFrom = Convert.ToDateTime(txtTimeFrom.Text, culture);
                    if (txtTimeTo.Text != "")
                    {
                        timeTo = Convert.ToDateTime(txtTimeTo.Text, culture);
                    }
                    else
                    {
                        timeTo = timeFrom;
                    }

                    /// Cập nhật dữ liệu theo flow timekeeping
                    var ftk = db.FlowTimeKeepings.Where(w => w.IsDelete != 1 && w.WorkDate >= timeFrom && w.WorkDate <= timeTo).ToList();
                    if (ftk.Count > 0)
                    {
                        foreach (var v in ftk)
                        {
                            SalaryLib.Instance.updateFlowSalaryByTimeKeeping(v);
                        }
                    }
                }
            }
        }

        protected void updateFlowSalaryByBillFix(object sender, EventArgs e)
        {
            using (var db = new Solution_30shineEntities())
            {
                var timeFrom = new DateTime();
                var timeTo = new DateTime();
                if (txtTimeFrom.Text != "")
                {
                    timeFrom = Convert.ToDateTime(txtTimeFrom.Text, culture);
                    if (txtTimeTo.Text != "")
                    {
                        timeTo = Convert.ToDateTime(txtTimeTo.Text, culture).AddDays(1);
                    }
                    else
                    {
                        timeTo = timeFrom.AddDays(1);
                    }

                    /// Cập nhật dữ liệu theo bill
                    var sql = @"select bill.*
                                from BillService as bill
                                inner join FlowService as fs
                                on bill.Id = fs.BillId and fs.IsDelete = 1
                                where bill.IsDelete != 1 and bill.Pending != 1
	                                and bill.CreatedDate between '" + string.Format("{0:yyyy/MM/dd}", timeFrom) + "' and '" + string.Format("{0:yyyy/MM/dd}", timeTo) + @"'";
                    var bills = db.BillServices.SqlQuery(sql).ToList();
                    if (bills.Count > 0)
                    {
                        foreach (var v in bills)
                        {
                            SalaryLib.Instance.updateFlowSalaryByBill(v);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Cập nhật bản ghi FlowSalary theo nhân viên
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void updateByStaff(object sender, EventArgs e)
        {
            using (var db = new Solution_30shineEntities())
            {
                var timeFrom = new DateTime();
                var timeTo = new DateTime();
                int staffId = 0;
                Staff staff = new Staff();
                try
                {
                    staffId = Convert.ToInt32(ddlStaff.SelectedValue);
                    staff = db.Staffs.FirstOrDefault(w => w.Id == staffId);
                }
                catch { }

                if (!staff.Id.Equals(null))
                {
                    if (txtTimeFrom.Text != "")
                    {
                        timeFrom = Convert.ToDateTime(txtTimeFrom.Text, culture);
                        if (txtTimeTo.Text != "")
                        {
                            timeTo = Convert.ToDateTime(txtTimeTo.Text, culture);
                        }
                        else
                        {
                            timeTo = timeFrom;
                        }
                        updateStatus.InnerText = "Đang cập nhật...";
                        string timeUpdate = String.Format("{0:HH:mm:ss}", DateTime.Now);
                        SalaryLib.Instance.forUpdate_updateSalaryByStaff(staff, timeFrom, timeTo);
                        timeUpdate += " - " + String.Format("{0:HH:mm:ss}", DateTime.Now);
                        updateStatus.InnerText = timeUpdate;
                    }
                }
            }
        }

        /// <summary>
        /// Cập nhật bản ghi FlowSalary theo salon
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void updateBySalon(object sender, EventArgs e)
        {
            using (var db = new Solution_30shineEntities())
            {
                var timeFrom = new DateTime();
                var timeTo = new DateTime();
                int salonId = 0;
                var list_staff = new List<Staff>();
                try
                {
                    salonId = Convert.ToInt32(ddlSalon.SelectedValue);
                    list_staff = db.Staffs.Where(w => w.SalonId == salonId && w.IsDelete != 1 && w.Active == 1).ToList();
                }
                catch { }

                if (list_staff.Count > 0)
                {
                    if (txtTimeFrom.Text != "")
                    {
                        timeFrom = Convert.ToDateTime(txtTimeFrom.Text, culture);
                        if (txtTimeTo.Text != "")
                        {
                            timeTo = Convert.ToDateTime(txtTimeTo.Text, culture);
                        }
                        else
                        {
                            timeTo = timeFrom;
                        }

                        updateStatus.InnerText = "Đang cập nhật...";
                        string timeUpdate = String.Format("{0:HH:mm:ss}", DateTime.Now);
                        foreach (var v in list_staff)
                        {
                            SalaryLib.Instance.forUpdate_updateSalaryByStaff(v, timeFrom, timeTo);
                        }
                        timeUpdate += " - " + String.Format("{0:HH:mm:ss}", DateTime.Now);
                        updateStatus.InnerText = timeUpdate;
                    }
                }
            }
        }

        protected void updateByDepartment(object sender, EventArgs e)
        {
            using (var db = new Solution_30shineEntities())
            {
                var timeFrom = new DateTime();
                var timeTo = new DateTime();
                int departmentId = 0;
                var list_staff = new List<Staff>();
                try
                {
                    departmentId = Convert.ToInt32(ddlDepartment.SelectedValue);
                    list_staff = db.Staffs.Where(w => w.Type == departmentId && w.IsDelete != 1 && w.Active == 1).ToList();
                }
                catch { }

                if (list_staff.Count > 0)
                {
                    if (txtTimeFrom.Text != "")
                    {
                        timeFrom = Convert.ToDateTime(txtTimeFrom.Text, culture);
                        if (txtTimeTo.Text != "")
                        {
                            timeTo = Convert.ToDateTime(txtTimeTo.Text, culture);
                        }
                        else
                        {
                            timeTo = timeFrom;
                        }

                        updateStatus.InnerText = "Đang cập nhật...";
                        string timeUpdate = String.Format("{0:HH:mm:ss}", DateTime.Now);
                        foreach (var v in list_staff)
                        {
                            SalaryLib.Instance.forUpdate_updateSalaryByStaff(v, timeFrom, timeTo);
                        }
                        timeUpdate += " - " + String.Format("{0:HH:mm:ss}", DateTime.Now);
                        updateStatus.InnerText = timeUpdate;
                    }
                }
            }
        }

        public void bindDepartment()
        {
            using (var db = new Solution_30shineEntities())
            {
                ddlDepartment.DataTextField = "Name";
                ddlDepartment.DataValueField = "Id";
                ddlDepartment.DataSource = db.Staff_Type.Where(w => w.IsDelete != 1 && w.Publish == true).ToList();
                ddlDepartment.DataBind();
                ddlDepartment.Items.Insert(0, new ListItem("Chọn bộ phận", "0"));
            }
        }

        /////////////////////////////////////
        // Cập nhật lương cứng tháng 7/2017
        /////////////////////////////////////

        /// <summary>
        /// Lấy salonId
        /// </summary>
        /// <returns></returns>
        public int getSalonId()
        {
            try
            {
                return Convert.ToInt32(ddlSalon.SelectedValue);
            }
            catch
            {
                throw new Exception("Lỗi lấy salon");
            }
        }

        public int getStaffId()
        {
            try
            {
                return Convert.ToInt32(ddlStaff.SelectedValue);
            }
            catch
            {
                throw new Exception("Lỗi lấy nhân viên");
            }
        }

        public DateTime? getDateTimeFromString(string dateTimeString)
        {
            try
            {
                DateTime? dateTime;
                if (txtTimeFrom.Text != "")
                {
                    dateTime = Convert.ToDateTime(dateTimeString, Locale.getCultureInfo());
                }
                else
                {
                    dateTime = null;
                }
                return dateTime;
            }
            catch
            {
                throw new Exception("Lỗi Convert DateTime");
            }
        }

        /// <summary>
        /// Get hệ số lương dịch vụ
        /// </summary>
        /// <param name="department"></param>
        /// <param name="skillLevel"></param>
        /// <param name="payon"></param>
        /// <returns></returns>
        public int getPayonValue(Staff staff, int paybytime, Solution_30shineEntities db)
        {
            int value = 0;
            if (staff != null)
            {
                var record = new Tbl_Payon();
                if (staff.SalaryByPerson == true)
                {
                    record = this.db.Tbl_Payon.FirstOrDefault(w => w.StaffId == staff.Id && w.PayByTime == paybytime && w.Hint == "base_salary");
                }
                else
                {
                    record = this.db.Tbl_Payon.FirstOrDefault(w => w.TypeStaffId == staff.Type && w.ForeignId == staff.SkillLevel && w.PayByTime == paybytime && w.Hint == "base_salary");
                }
                if (record != null && record.Value != null)
                {
                    value = record.Value.Value;
                }
            }
            return value;
        }

        /// <summary>
        /// Kiểm tra nhân viên đã được chấm công hay chưa
        /// - Riêng đối với stylist và skinner, công được tính khi 1 ngày tham gia đc ít nhất 3 bill
        /// </summary>
        /// <param name="staff"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public bool isEnroll(Staff staff, DateTime date)
        {
            bool enroll = false;
            if (staff != null)
            {
                var ftk = new FlowTimeKeeping();
                ftk = this.db.FlowTimeKeepings.FirstOrDefault(w => w.StaffId == staff.Id && w.WorkDate == date && w.IsDelete != 1);
                if (ftk != null && ftk.IsEnroll == true)
                {
                    enroll = true;
                }
            }
            return enroll;
        }

        /// <summary>
        /// Cập nhật lương cứng
        /// Có 2 trường hợp
        /// 1. Nhân viên được trả lương theo công thức Nhóm (Bộ phận)
        /// 2. Nhân viên được trả lương theo công thức cá nhân (Áp dụng cho 1 số cá nhân đặc biệt)
        /// </summary>
        /// <param name="staff"></param>
        /// <param name="payon"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public float getFixSalaryByDate(Staff staff, DateTime date)
        {
            float salary = 0;
            if (staff != null)
            {
                int dayInMonth = DateTime.DaysInMonth(date.Year, date.Month) - 2;
                if (staff.SalaryByPerson == true || staff.RequireEnroll == false)
                {
                    int payonValue = getPayonValue(staff, 1, db);
                    if (payonValue > 0 && dayInMonth > 0)
                    {
                        salary = (float)payonValue / DateTime.DaysInMonth(date.Year, date.Month);
                    }
                }
                else
                {
                    int payonValue = getPayonValue(staff, 1, db);
                    if (payonValue > 0 && dayInMonth > 0)
                    {
                        salary = (float)payonValue / dayInMonth;
                    }
                }
            }
            return salary;
        }

        /// <summary>
        /// Cập nhật lương cứng
        /// 1. Lấy toàn bộ bản ghi FlowSalary trong tháng 7
        /// 2. Kiểm tra chấm công của nhân viên đó trong ngày
        /// 3. Truy vấn lấy giá trị cấu hình lương cứng của nhân viên
        /// 4. Tính và update lương cứng
        /// 12h21
        /// </summary>
        public void updateFixSalary(object sender, EventArgs e)
        {
            try
            {
                var salonId = this.getSalonId();
                var staffId = this.getStaffId();
                var timeFrom = this.getDateTimeFromString(txtTimeFrom.Text).Value;
                var timeTo = this.getDateTimeFromString(txtTimeTo.Text).Value;
                timeTo = timeTo != null ? timeTo : timeFrom;
                
                var listTimeKeeping = new List<FlowTimeKeeping>();
                var listFlowSalary = new List<FlowSalary>();
                if (staffId > 0)
                {
                    listTimeKeeping = db.FlowTimeKeepings.Where(
                        w => w.StaffId == staffId
                        && w.IsDelete == 0
                        && w.IsEnroll == true
                        && w.WorkDate >= timeFrom
                        && w.WorkDate <= timeTo).ToList();
                    listFlowSalary = db.FlowSalaries.Where(
                        w => w.staffId == staffId
                        && w.IsDelete == 0
                        && w.sDate >= timeFrom
                        && w.sDate <= timeTo).ToList();
                }
                else
                {
                    listTimeKeeping = db.FlowTimeKeepings.Where(
                        w => (w.SalonId == salonId || (salonId == 0 && w.SalonId > 0))
                        && w.IsDelete == 0
                        && w.IsEnroll == true
                        && w.WorkDate >= timeFrom
                        && w.WorkDate <= timeTo).ToList();
                    listFlowSalary = db.FlowSalaries.Where(
                        w => (w.SalonId == salonId || (salonId == 0 && w.SalonId > 0))
                        && w.IsDelete == 0
                        && w.sDate >= timeFrom
                        && w.sDate <= timeTo).ToList();
                }
                if (listTimeKeeping.Count > 0)
                {
                    foreach (var v in listTimeKeeping)
                    {
                        var flowSalary = listFlowSalary.FirstOrDefault(w => w.staffId == v.StaffId && w.sDate == v.WorkDate && w.IsDelete == 0);
                        if (flowSalary != null)
                        {
                            var staff = db.Staffs.FirstOrDefault(w => w.Id == v.StaffId);
                            flowSalary.SalonId = v.SalonId;
                            if (flowSalary.fixSalary == 0)
                            {
                                flowSalary.fixSalary = getFixSalaryByDate(staff, flowSalary.sDate);
                            }
                            db.FlowSalaries.AddOrUpdate(flowSalary);
                            db.SaveChanges();
                        }
                    }
                }
            }
            catch
            {
                throw new Exception("Lỗi cập nhật lương cứng");
            }
        }

        public void UpdateX15(Object sender, EventArgs e)
        {
            try
            {
                var timeFrom = new DateTime();
                var timeTo = new DateTime();

                timeFrom = Convert.ToDateTime(txtTimeFrom.Text, culture);
                if (txtTimeTo.Text != "")
                {
                    timeTo = Convert.ToDateTime(txtTimeTo.Text, culture);
                }
                else
                {
                    timeTo = timeFrom;
                }

                var listStaff = db.Database.SqlQuery<Staff>(
                        @"with billx1_5 as 
                            (
                            select 
                            bill.Staff_Hairdresser_Id, bill.Staff_HairMassage_Id, bill.ReceptionId
                            from BillService as bill
                            where bill.IsDelete = 0 and bill.Pending = 0
	                            and bill.CreatedDate between '"+Library.Format.getDateTimeString(timeFrom)+@"' and '"+Library.Format.getDateTimeString(timeTo.AddDays(1))+@"'
	                            and bill.IsX2 = 1
                            ),
                            staffs as 
                            (
	                            select Staff_Hairdresser_Id as StaffId from billx1_5 
	                            group by Staff_Hairdresser_Id
	                            union 
	                            select Staff_HairMassage_Id as StaffId from billx1_5 
	                            group by Staff_HairMassage_Id
	                            union 
	                            select ReceptionId as StaffId from billx1_5 
	                            group by ReceptionId
                            )
                            select Staff.*
                            from staffs
                            inner join Staff on Staff.Id = staffs.StaffId
                            order by Staff.Id"
                    ).ToList();
                foreach (var v in listStaff)
                {
                    Response.Write(v.Fullname + "</br>");
                    updateStatus.InnerText = "Đang cập nhật...";
                    string timeUpdate = String.Format("{0:HH:mm:ss}", DateTime.Now);
                    SalaryLib.Instance.forUpdate_updateSalaryByStaff(v, timeFrom, timeTo);
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}