﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Web.Helpers;
using System.Text.RegularExpressions;
using System.Globalization;

namespace _30shine.GUI.BackEnd.UpdateDB
{
    public partial class Update_FlowSalary_1_6_2016 : System.Web.UI.Page
    {
        private CultureInfo culture = new CultureInfo("vi-VN");
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void updateFlowSalaryByBill(object sender, EventArgs e)
        {
            using (var db = new Solution_30shineEntities())
            {
                var timeFrom = new DateTime();
                var timeTo = new DateTime();
                var timeTemp = new DateTime();
                if (txtTimeFrom.Text != "")
                {
                    timeFrom = Convert.ToDateTime(txtTimeFrom.Text, culture);
                    if (txtTimeTo.Text != "")
                    {
                        timeTo = Convert.ToDateTime(txtTimeTo.Text, culture).AddDays(1);
                    }
                    else
                    {
                        timeTo = timeFrom.AddDays(1);
                    }

                    /// Init dữ liệu flow salary
                    timeTemp = timeFrom;
                    while (timeTemp < timeTo)
                    {
                        SalaryLib.initSalaryToday(timeTemp);
                        timeTemp = timeTemp.AddDays(1);
                    }
                    /// Cập nhật dữ liệu theo bill
                    var bills = db.BillServices.Where(w=>w.IsDelete != 1 && w.Pending != 1 && w.CompleteBillTime >= timeFrom && w.CompleteBillTime < timeTo).ToList();
                    if (bills.Count > 0)
                    {
                        foreach (var v in bills)
                        {
                            SalaryLib.updateFlowSalaryByBill(v, db);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Cập nhật bổ sung
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void updateFlowSalaryByFTK(object sender, EventArgs e)
        {
            using (var db = new Solution_30shineEntities())
            {
                var timeFrom = new DateTime();
                var timeTo = new DateTime();
                if (txtTimeFrom.Text != "")
                {
                    timeFrom = Convert.ToDateTime(txtTimeFrom.Text, culture);
                    if (txtTimeTo.Text != "")
                    {
                        timeTo = Convert.ToDateTime(txtTimeTo.Text, culture);
                    }
                    else
                    {
                        timeTo = timeFrom;
                    }
                    
                    /// Cập nhật dữ liệu theo flow timekeeping
                    var ftk = db.FlowTimeKeepings.Where(w => w.IsDelete != 1 && w.WorkDate >= timeFrom && w.WorkDate <= timeTo).ToList();
                    if (ftk.Count > 0)
                    {
                        foreach (var v in ftk)
                        {
                            SalaryLib.updateFlowSalaryByTimeKeeping(v, db);
                        }
                    }
                }
            }
        }

        protected void updateFlowSalaryByBillFix(object sender, EventArgs e)
        {
            using (var db = new Solution_30shineEntities())
            {
                var timeFrom = new DateTime();
                var timeTo = new DateTime();
                if (txtTimeFrom.Text != "")
                {
                    timeFrom = Convert.ToDateTime(txtTimeFrom.Text, culture);
                    if (txtTimeTo.Text != "")
                    {
                        timeTo = Convert.ToDateTime(txtTimeTo.Text, culture).AddDays(1);
                    }
                    else
                    {
                        timeTo = timeFrom.AddDays(1);
                    }
                    
                    /// Cập nhật dữ liệu theo bill
                    var sql = @"select bill.*
                                from BillService as bill
                                inner join FlowService as fs
                                on bill.Id = fs.BillId and fs.IsDelete = 1
                                where bill.IsDelete != 1 and bill.Pending != 1
	                                and bill.CreatedDate between '" + string.Format("{0:yyyy/MM/dd}", timeFrom) + "' and '" + string.Format("{0:yyyy/MM/dd}", timeTo) + @"'";
                    var bills = db.BillServices.SqlQuery(sql).ToList();
                    if (bills.Count > 0)
                    {
                        foreach (var v in bills)
                        {
                            SalaryLib.updateFlowSalaryByBill(v, db);
                        }
                    }
                }
            }
        }
    }
}