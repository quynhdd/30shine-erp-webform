﻿<%@ Page Async="true" Language="C#" AutoEventWireup="true" CodeBehind="Update_Bill_CustomerId.aspx.cs" Inherits="_30shine.GUI.BackEnd.UpdateDB.Update_Bill_CustomerId" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <h3>Cập nhật CustomerId - Bảng BillService</h3>
    <div>
        <asp:TextBox runat="server" ID="startId" placeholder="ID bắt đầu"></asp:TextBox>
        <asp:TextBox runat="server" ID="endId" placeholder="ID kết thúc"></asp:TextBox>
        <asp:TextBox runat="server" ID="txtSegment" placeholder="Segment" Text="1000" title="Segment"></asp:TextBox>
        <asp:TextBox runat="server" ID="txtDelay" placeholder="Delay" Text="3000" title="Delay"></asp:TextBox>
        <asp:Button runat="server" Text="Cập nhật" ID="btnUpdate" OnClick="update" />
    </div>
    <script>
        function check() {
            console.log("ok");
        }
    </script>
    </form>    
</body>
</html>
