﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Threading.Tasks;
using _30shine.MODEL.ENTITY.EDMX;

namespace _30shine.GUI.BackEnd.UpdateDB
{
    public partial class Update_Bill_CustomerId : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected async void update(object sender, EventArgs e)
        {
            using (var db = new Solution_30shineEntities())
            {
                bool flag = true;
                string sql = "";                
                int integer;
                int delay = int.TryParse(txtDelay.Text, out integer) ? integer : 0;
                int end = int.TryParse(endId.Text, out integer) ? integer : 0;
                int offset = int.TryParse(startId.Text, out integer) ? integer : 0;                
                int segment = int.TryParse(txtSegment.Text, out integer) ? integer : 0;
                int to = offset + segment;
                while (flag)
                {
                    if (to == end)
                    {
                        flag = false;
                    }
                    sql = @"update BillService set CustomerId = (select top 1 Id from Customer 
where (Customer_Code = BillService.CustomerCode or Customer_Code1 = BillService.CustomerCode) and IsDelete != 1) where Id >= "+offset+" and Id < " + to;
                    var exc = db.Database.ExecuteSqlCommand(sql);
                    if (exc > 0)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "callback", "check();", true);
                        offset = to;
                        to += segment;
                        if (to > end)
                        {
                            to = end;
                        }
                    }
                    else
                    {
                        flag = false;
                    }
                    await Task.Delay(delay);
                }
            }
        }
    }
}