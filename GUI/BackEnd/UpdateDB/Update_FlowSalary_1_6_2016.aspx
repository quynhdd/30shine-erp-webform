﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Update_FlowSalary_1_6_2016.aspx.cs" Inherits="_30shine.GUI.BackEnd.UpdateDB.Update_FlowSalary_1_6_2016" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Cập nhật bảng store lương theo ngày</title>
    <link href="/Assets/css/jquery.datetimepicker.css" rel="stylesheet" />
    <script src="/Assets/js/jquery.v1.11.1.js"></script>
    <script src="/Assets/js/jquery.datetimepicker.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <h3>Cập nhật dữ liệu store lương theo ngày</h3>
        <div>
        Chọn ngày: <br />
        <asp:TextBox runat="server" ID="txtTimeFrom" CssClass="txtDateTime" placeholder="Từ ngày"></asp:TextBox>
        <asp:TextBox runat="server" ID="txtTimeTo" CssClass="txtDateTime" placeholder="Đến ngày"></asp:TextBox>
        <asp:Button runat="server" ID="btnSubmitByBill" Text="Cập nhật theo bill" OnClick="updateFlowSalaryByBill" />
        <%--<asp:Button runat="server" ID="Button1" Text="Cập nhật theo bill (fix bổ sung)" OnClick="updateFlowSalaryByBillFix" />--%>
        <asp:Button runat="server" ID="btnSubmitByFTK" Text="Cập nhật theo chấm công" OnClick="updateFlowSalaryByFTK" />
        </div>
        <script>
            //============================
            // Datepicker
            //============================
            $('.txtDateTime').datetimepicker({
                dayOfWeekStart: 1,
                lang: 'vi',
                startDate: '2014/10/10',
                format: 'd/m/Y',
                dateonly: true,
                showHour: false,
                showMinute: false,
                timepicker: false,
                onChangeDateTime: function (dp, $input) {
                    //$input.val($input.val().split(' ')[0]);
                }
            });
        </script>
    </form>
</body>
</html>
