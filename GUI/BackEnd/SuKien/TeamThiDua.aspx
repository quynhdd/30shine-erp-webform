﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TeamThiDua.aspx.cs" Inherits="_30shine.GUI.BackEnd.SuKien.TeamThiDua" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<%--<%@ Register Src="~/GUI/BackEnd/Report/ReportMenu.ascx" TagName="ReportMenu" TagPrefix="uc1" %>--%>

<asp:Content ID="pageHead" ContentPlaceHolderID="head" runat="server">
    <title>Sự kiện - Thêm team</title>
    <script src="/Assets/libs/magicsuggest-master/magicsuggest.js"></script>
    <link href="/Assets/libs/magicsuggest-master/magicsuggest.css" rel="stylesheet" />
    <style>
        .event-team-add { width: 600px; float: left; padding-bottom: 10px; display: none; }
        .event-team-add .container { min-width: 600px; }
        .event-team-add .team-add-head { padding: 10px; font-weight: normal; text-transform: uppercase; font-family: Roboto Condensed Bold; float: left; width: 100%; margin-bottom: 10px; border-bottom: 1px solid #e7e7e7; }
        .event-team-add label { line-height: 34px; font-weight: normal; }
        .event-team-add .form-group { float: left; width: 100%; }

        /*.suggestion-wrap { width: 420px; float:left; }*/
        .ms-sel-ctn input[type='text'] { height: auto !important; line-height: 24px !important; border: none !important; }
        .ms-ctn .ms-sel-item { line-height: 22px; color: #000000; font-size: 13px; }
        .ms-helper { display: none !important; }
        .ms-ctn .dropdown-menu { height: 220px; }
    </style>
    <script>
        var ms;
        var listStaff;
        var teamID;
        function addTeam() {
            teamID = 0;
            $("#eventTeamAdd").openEBPopup();
            ms = $('#EBPopup .ms-suggest').magicSuggest({
                maxSelection: 10,
                //data: response.d,
                valueField: 'Id',
                displayField: 'Fullname'
            });
            $(ms).on('selectionchange', function (e, m) {
                listStaff = this.getSelection();
            });
        }

        function updateTeam(teamId) {
            // set TeamID
             teamID = teamId;          
            $.ajax({
                type: "POST",
                url: "/GUI/BackEnd/SuKien/TeamThiDua.aspx/Load",
                data: '{TeamId : ' + teamID + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    console.log(response);
                    // 1 : mo popup
                    $("#eventTeamAdd").openEBPopup();
                    // 2. mo magic suggest
                    ms = $('#EBPopup .ms-suggest').magicSuggest({
                        maxSelection: 10,
                        valueField: 'Id',
                        displayField: 'Fullname'
                    });
                    $(ms).on('selectionchange', function (e, m) {
                        listStaff = this.getSelection();
                        console.log(listStaff);
                    });
                    // set teamId
                    teamID = response.d.TeamID;
                    //set team-Name
                    $("#EBPopup .team-name").val(response.d.TeamName);
                    //set teamID                   
                    // set gia tri cho salon
                    $("#EBPopup .team-salon").val(response.d.SalonID);
                    loadStaffbySalon($("#EBPopup .team-salon"));

                    // set gia tri nhan vien
                    if (response.d.ListStaff.length > 0) {
                        var arrNV = [];
                        $.each(response.d.ListStaff, function (i, v) {
                            arrNV.push({ Fullname: v.Fullname, Id: v.Id });
                        });
                        ms.setSelection(arrNV);
                    }

                },
                failure: function (response) { alert(response.d); }
            });           
        }

        function saveTeam() {
            teamID;
            var teamName = $("#EBPopup input.team-name").val();
            var salonID = $("#EBPopup .team-salon").val();
            console.log(teamID, teamName, salonID);
            startLoading();

            $.ajax({
                type: "POST",
                url: "/GUI/BackEnd/SuKien/TeamThiDua.aspx/editTeam",
                data: '{teamID : ' + teamID + ', teamName : "' + teamName + '", salonID : ' + salonID + ', listJSONStaff : \'' + JSON.stringify(listStaff) + '\'}',
                contentType: "application/json; charset=utf-8",
                dataType: "json", success: function (response) {
                    console.log(response);
                    finishLoading();            
                    $("#ViewData").click();
                    $("#EBCloseBtn").click();
                },
                failure: function (response) { alert(response.d); }
            });
        }

        function loadStaffbySalon(This) {
            $(".ms-sel-item").each(function (index) {
                $(".ms-close-btn").trigger("click");
            });
            //console.log("afd");
            //$("#CtMain_Staff").removeClass(".ms-sel-item");
            //$('.ms-sel-ctn').removeClass('.ms-sel-item ');
        
            
            startLoading();
            $.ajax({
                type: "POST",
                url: "/GUI/BackEnd/SuKien/TeamThiDua.aspx/loadStaffBySalon",
                data: '{salonID : ' + This.val() + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json", success: function (response) {
                    console.log(response);
                    ms.setData(response.d);
                    finishLoading();
                   
                },
                failure: function (response) { alert(response.d); }
            });
        }

    </script>
</asp:Content>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

        <%--<uc1:ReportMenu runat="server" id="ReportMenu" />--%>

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Sự kiện &nbsp;&#187; </li>
                        <li class="be-report-li"><a href="javascript:void(0);"><i class="fa fa-th-large"></i>Team thi đua</a></li>
                        <% if (Perm_AddTeam)
                            { %>
                        <li class="li-add" onclick="addTeam()"><a href="javascript:void(0);">Thêm team</a></li>
                        <% } %>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing be-report be-report-timekeeping">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <asp:ScriptManager ID="SM01" runat="server"></asp:ScriptManager>
                <div class="row">
                    <div class="filter-item">
                        <strong class="st-head"><i class="fa fa-clock-o"></i>Thời gian</strong>
                        <div class="time-wp">
                            <asp:TextBox CssClass="txtDateTime st-head" ID="TxtDateTimeFrom" placeholder="Từ ngày" Style="margin-left: 10px!important"
                                ClientIDMode="Static" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="ValidateTime" ControlToValidate="TxtDateTimeFrom" runat="server" CssClass="fb-cover-error" Text="Vui lòng chọn thời gian!"></asp:RequiredFieldValidator>
                        </div>
                        <strong class="st-head" style="margin-left: 10px;">
                            <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                        </strong>
                        <asp:TextBox CssClass="txtDateTime st-head" ID="TxtDateTimeTo" placeholder="Đến ngày" Style="margin-left: 10px!important"
                            ClientIDMode="Static" runat="server"></asp:TextBox>
                      
                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="Salon" runat="server" ClientIDMode="Static" Style="width: 190px;"></asp:DropDownList>
                    </div>
                  
                    <asp:Panel ID="ViewData" CssClass="st-head btn-viewdata" ClientIDMode="Static"
                        onclick="excPaging(1)" runat="server">
                        Xem dữ liệu
                    </asp:Panel>
                    <a href="javascript:void(0);" onclick="location.href=location.href" class="st-head btn-viewdata">Reset Filter</a>
                  
                </div>
                <div class="row">
                    <strong class="st-head"><i class="fa fa-file-text"></i>Bảng xếp hạng</strong>
                </div>
                <!-- Row Table Filter -->
                <div class="table-func-panel" style="margin-top: -33px;">
                    <div class="table-func-elm">
                        <span>Số hàng / Page : </span>
                        <div class="table-func-input-wp">
                            <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                            <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                            <ul class="ul-opt-segment">
                                <li data-value="10">10</li>
                                <li data-value="20">20</li>
                                <li data-value="30">30</li>
                                <li data-value="40">40</li>
                                <li data-value="50">50</li>
                                <li data-value="1000000">Tất cả</li>
                            </ul>
                            <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                        </div>
                    </div>
                </div>
                <!-- End Row Table Filter -->
                <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                    <ContentTemplate>
                        <div class="row table-wp">
                            <table class="table-add table-listing">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Team</th>
                                        <th>Nhân viên</th>
                                        <th>Tổng điểm</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="rptDataTeam" runat="server" OnItemDataBound="Repeater_ItemDataBound">
                                        <ItemTemplate>
                                            <tr onclick="updateTeam(<%# Eval("TeamID") %>)">
                                                <td><%# Container.ItemIndex + 1 + PAGING._Offset %></td>
                                                <td><%# Eval("TeamName") %></td>
                                                <td><%# Eval("TeamStaffs") %></td>
                                                <td class="map-edit">
                                                    <%# Eval("TeamPoint") %>
                                                    <div class="edit-wp">
                                                        <asp:Panel CssClass="edit-action-wp action-edit" runat="server" ID="EAedit" Visible="false">
                                                            <a class="elm edit-btn" href="javascript:void(0);"  title="Sửa"></a>
                                                        </asp:Panel>
                                                        <%--<asp:Panel CssClass="edit-action-wp action-delete" runat="server" ID="EAdelete" Visible="false">
                                                <a class="elm del-btn" onclick="del(this.parentNode.parentNode.parentNode.parentNode,'<%# Eval("Id") %>', '<%# Eval("BillCode") %>')" href="javascript://" title="Xóa"></a>
                                            </asp:Panel>--%>
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>
                        <!-- Paging -->
                        <div class="site-paging-wp">
                            <% if (PAGING.TotalPage > 1)
                                { %>
                            <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                                <% if (PAGING._Paging.Prev != 0)
                                    { %>
                                <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                                <% } %>
                                <asp:Repeater ID="RptPaging" runat="server">
                                    <ItemTemplate>
                                        <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                            <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>>
                                            <%# Eval("PageNum") %>
                                        </a>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                                    { %>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                                <% } %>
                            </asp:Panel>
                            <% } %>
                        </div>
                        <!-- End Paging -->
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                <%--<asp:Button ID="BtnFakeExcel" ClientIDMode="Static" runat="server" OnClick="Exc_ExportExcel" style="display:none;" />--%>
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />

                <!-- Hidden Field-->
                <asp:HiddenField ID="HDF_TypeStaff" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="HDF_Staff" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="HDF_ExcelType" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="HDF_ExportType" ClientIDMode="Static" runat="server" />
                <!-- END Hidden Field-->
            </div>
            <%-- END Listing --%>
        </div>

        <!-- Popup thêm mới team -->
        <div class="event-team-add" id="eventTeamAdd">
            <strong class="team-add-head">Thêm mới team</strong>
            <div class="container">
                <div class="row">
                    <div class="form-group form-group-lg">
                        <label class="col-xs-3 control-label" for="formGroupInputLarge">Tên team</label>
                        <div class="col-xs-9">
                            <input class="form-control team-name" runat="server" id="TeamName" type="text" placeholder="Tên team" />
                        </div>
                    </div>
                    <div class="form-group form-group-lg">
                        <label class="col-xs-3 control-label" for="formGroupInputLarge">Salon</label>
                        <div class="col-xs-9">
                            <select class="form-control team-salon" onchange="loadStaffbySalon($(this))">
                                <option runat="server"  id="slectSalon" value="1">Chọn salon</option>
                                <% if (listSalon.Count > 0)
                                    { %>
                                <% foreach (var v in listSalon)
                                    { %>
                                <option value="<%=v.Id %>"><%=v.Name %></option>
                                <%} %>
                                <%} %>
                            </select>
                        </div>
                    </div>
                    <div class="form-group form-group-lg">
                        <label class="col-xs-3 control-label" for="formGroupInputLarge">Chọn nhân viên</label>
                        <div class="col-xs-9 suggestion-wrap">
                            <input class="form-control ms-suggest" runat="server" id="Staff" type="text" placeholder="Search theo tên nhân viên" />
                        </div>
                    </div>
                    <div class="form-group form-group-lg">
                        <div class="col-xs-3"></div>
                        <div class="col-xs-9 suggestion-wrap">
                            <div class="btn btn-success" onclick="saveTeam()">Hoàn tất</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/ Popup thêm mới team -->

        <!-- Page loading -->
        <div class="page-loading">
            <p>Vui lòng đợi trong giây lát...</p>
        </div>
        <!--/ Page loading -->

        <link href="/Assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" />
        <script src="/Assets/js/jquery.mCustomScrollbar.js"></script>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                // Add active menu
                $("#glbAdminSales").addClass("active");
                $("#glbAdminReportService").addClass("active");
                $("li.be-report-li").addClass("active");

                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) { }
                });

                //============================
                // Bind Staff
                //============================
                BindStaffFilter();

                // View data yesterday
                $(".tag-date-yesterday").click();

            });

            function showTrPrice() {
                $(".tr-price").show();
            }

            function BindStaffFilter() {
                $(".eb-select").bind("focus", function () {
                    EBSelect_ShowBox($(this).parent().find(".eb-select-data"));
                });
                $(window).bind("click", function (e) {
                    console.log(e);
                    if ((!e.target.className.match("eb-select") && e.target.parentElement.className.match("ul-listing-staff")) ||
                        (!e.target.className.match("eb-select") && !e.target.parentElement.className.match("ul-listing-staff"))
                    && !e.target.className.match("mCSB_dragger_bar")) {
                        EBSelect_HideBox();
                    }
                });
                //============================
                // Scroll Staff Filter
                //============================
                $('.eb-select-data').each(function () {
                    if ($(this).height() > 230) {
                        $(this).mCustomScrollbar({
                            theme: "dark-2",
                            scrollInertia: 100
                        });
                    }
                });
            }

            function BindIdToHDF(THIS, id, HDF_DomId, Input_DomId) {
                var text = THIS.innerText.trim(),
                    HDF_Dom = document.getElementById(HDF_DomId),
                    InputText = document.getElementById(Input_DomId);
                HDF_Dom.value = id;
                InputText.value = text;
                InputText.setAttribute("data-value", id);
                ajaxGetStaffsByType(id);
                if (HDF_DomId == "HDF_TypeStaff") {
                    $("#StaffName").val("");
                    $("#HDF_Staff").val("");
                }
            }

            function ajaxGetStaffsByType(type) {
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/Report/TimeKeeping.aspx/Load_Staff_ByType",
                    data: '{type : "' + type + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        var mission = JSON.parse(response.d);
                        if (mission.success) {
                            var staffs = JSON.parse(mission.msg);
                            if (staffs.length > 0) {
                                var tmp = "";
                                $.each(staffs, function (i, v) {
                                    tmp += '<li data-code="' + v.Code + '" data-id="' + v.Id + '"' +
                                            'onclick="BindIdToHDF(this,' + v.Id + ',\'HDF_Staff\',\'StaffName\')">' +
                                            v.Fullname + '</li>';
                                });
                                $("#UlListStaff").empty().append(tmp);
                            }

                        } else {
                            var msg = "Không tìm thấy mã khách hàng trong hệ thống.";
                            showMsgSystem(msg, "warning");
                        }
                    },
                    failure: function (response) { alert(response.d); }
                });
            }

            function EBSelect_ShowBox(Dom) {
                EBSelect_HideBox();
                Dom.show();
            }

            function EBSelect_HideBox() {
                $(".eb-select-data").hide();
            }

            function EBSelect_BindData() {

            }

            function ReplaceZezoValue() {
                $("table.table-listing td").each(function () {
                    if ($(this).text().trim() == "0") {
                        $(this).text("-");
                    }
                });
            }

            function viewDataByDate(This, time) {
                $(".tag-date.active").removeClass("active");
                This.addClass("active");
                $("#TxtDateTimeTo").val("");
                $("#TxtDateTimeFrom").val(time);
                $("#ViewData").click();
            }


        </script>

    </asp:Panel>
</asp:Content>

