﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Globalization;
using LinqKit;
using System.Linq.Expressions;
using ExportToExcel;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.SuKien
{
    public partial class TeamThiDua : System.Web.UI.Page
    {
        protected Paging PAGING = new Paging();

        private int _TypeStaff;
        private int _StaffId;
        private bool IsProductFilter = false;

        protected bool Staff_All = false;
        protected bool Staff_Hairdresser = false;
        protected bool Staff_HairMassage = false;
        protected bool Staff_Singer = false;
        protected string THead = "";
        protected string StrProductPrice = "";
        public CultureInfo culture = new CultureInfo("vi-VN");
        public string timeFrom;
        public string timeTo;
        private Expression<Func<Staff, bool>> WhereStaff = PredicateBuilder.True<Staff>();
        private List<Service> _Service = new List<Service>();
        private List<Product> _Product = new List<Product>();
        protected string FirstDayOfMonth = string.Format("{0:dd/MM/yyyy}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1));
        protected string Today = String.Format("{0:dd/MM/yyyy}", DateTime.Now);
        public string sqlWhereStaff = "";
        protected int SalonId;

        protected string Day1 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-2));
        protected string Day2 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-1));
        protected string Day3 = string.Format("{0:dd/MM/yyyy}", DateTime.Now);

        private string PageID = "SK_TEAM";
        protected bool Perm_ViewAllData = false;
        private bool Perm_Access = false;
        private bool Perm_Edit = false;
        protected bool Perm_AddTeam = false;
        protected string Permission = "";

        protected string listNhanVien;
        protected List<Tbl_Salon> listSalon = new List<Tbl_Salon>();
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                ////Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //Perm_AddTeam = permissionModel.GetActionByActionNameAndPageId("Perm_AddTeam", PageID, permission);
                //Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_AddTeam = permissionModel.CheckPermisionByAction("Perm_AddTeam", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (!IsPostBack)
            {
                TxtDateTimeFrom.Text = FirstDayOfMonth;
                TxtDateTimeTo.Text = Today;
                Library.Function.bindSalon(new List<DropDownList> { Salon }, Perm_ViewAllData);
                listNhanVien = getListStaff();
                listSalon = getListSalon();
            }
            else
            {
                //Exc_Filter();
            }
            RemoveLoading();
        }

        /// <summary>
        /// Lấy danh sách nhân viên
        /// </summary>
        /// <returns></returns>
        private string getListStaff()
        {
            using (var db = new Solution_30shineEntities())
            {
                var serialize = new JavaScriptSerializer();
                return serialize.Serialize(db.Staffs.Where(w => w.IsDelete != 1 && w.Active == 1 && (w.isAccountLogin == null || w.isAccountLogin != 1) && (w.isAppLogin == null || w.isAppLogin != 1)).ToList());
            }
        }

        /// <summary>
        /// Lấy danh sách salon
        /// </summary>
        /// <returns></returns>
        private List<Tbl_Salon> getListSalon()
        {
            using (var db = new Solution_30shineEntities())
            {
                return db.Tbl_Salon.Where(w => w.IsDelete != 1 && w.Publish == true).ToList();
            }
        }

        /// <summary>
        /// Load danh sách nhân viên theo salon ID
        /// </summary>
        /// <param name="salonID"></param>
        /// <returns></returns>
        [WebMethod]
        public static object loadStaffBySalon(int salonID)
        {
            using (var db = new Solution_30shineEntities())
            {
                return db.Staffs.Where(w => w.SalonId == salonID && w.IsDelete != 1 && w.Active == 1 && (w.isAccountLogin == null || w.isAccountLogin != 1) && (w.isAppLogin == null || w.isAppLogin != 1)).Select(s => new { s.Id, s.Fullname }).ToList();
            }
        }
        /// <summary>
        /// Load
        /// </summary>
        /// <param name="TeamId"></param>
        /// <returns></returns>
        [WebMethod]
        public static object Load(int TeamId)
        {
           
            using (var db = new Solution_30shineEntities())
            {
                //var serialize = new JavaScriptSerializer();
                var list2 = new List<cls_team>();
                var data = new cls_team_update();
                var itemStaff = new cls_staff();
                var item = new cls_team();
                var dataStore = db.Store_SuKien_TeamThiDua_Load(TeamId).ToList();
                var index = -1;

                foreach (var v in dataStore)
                {
                    if (data.TeamID == 0)
                    {
                        data.TeamID = v.Id;
                        data.TeamName = v.TeamName;
                        data.SalonID = v.salonId;
                        data.ListStaff = new List<cls_staff>();
                    }

                    itemStaff = new cls_staff();
                    itemStaff.Id = v.staffId;
                    itemStaff.Fullname = v.StaffName;
                    data.ListStaff.Add(itemStaff);

                   
                }
                return data;
            }
        }
       

        /// <summary>
        /// Insert/Update team
        /// </summary>
        /// <param name="salonID"></param>
        /// <param name="teamName"></param>
        /// <param name="listJSONStaff"></param>
        /// <returns></returns>
        [WebMethod]
        public static object editTeam(int teamID, int salonID, string teamName, string listJSONStaff)
        {
            var msg = new Library.Class.cls_message();
            if (teamID == 0)
            {
                msg = insertTeam(teamID, salonID, teamName, listJSONStaff);
            }
            else
            {
                msg = updateTeam(teamID, salonID, teamName, listJSONStaff);
            }
            return msg;
        }

        /// <summary>
        /// Insert Team
        /// </summary>
        /// <param name="teamID"></param>
        /// <param name="salonID"></param>
        /// <param name="teamName"></param>
        /// <param name="listJSONStaff"></param>
        /// <returns></returns>
        private static Library.Class.cls_message insertTeam(int teamID, int salonID, string teamName, string listJSONStaff)
        {
            using (var db = new Solution_30shineEntities())
            {
                var msg = new Library.Class.cls_message();
                var serialize = new JavaScriptSerializer();
                var listStaff = serialize.Deserialize<List<cls_staff>>(listJSONStaff).ToList();
                if (listStaff.Count > 0)
                {
                    var teamRecord = new SuKien_Team();
                    teamRecord.TeamName = teamName;
                    teamRecord.SalonId = salonID;
                    teamRecord.CreatedTime = DateTime.Now;
                    teamRecord.Publish = true;
                    teamRecord.IsDelete = false;
                    db.SuKien_Team.AddOrUpdate(teamRecord);
                    db.SaveChanges();

                    foreach (var v in listStaff)
                    {
                        var teamStaffRecord = new SuKien_Team_Staff();
                        teamStaffRecord.TeamId = teamRecord.Id;
                        teamStaffRecord.StaffId = v.Id;
                        teamStaffRecord.CreatedTime = DateTime.Now;
                        teamStaffRecord.IsDelete = false;
                        db.SuKien_Team_Staff.AddOrUpdate(teamStaffRecord);
                        db.SaveChanges();
                    }

                    msg.success = true;
                    msg.message = "Cập nhật thành công!";
                }
                else
                {
                    msg.success = false;
                    msg.message = "Vui lòng chọn nhân viên vào team.";
                }
                return msg;
            }
        }

        /// <summary>
        /// Update Team
        /// </summary>
        /// <param name="teamID"></param>
        /// <param name="salonID"></param>
        /// <param name="teamName"></param>
        /// <param name="listJSONStaff"></param>
        /// <returns></returns>
        private static Library.Class.cls_message updateTeam(int teamID, int salonID, string teamName, string listJSONStaff)
        {
            using (var db = new Solution_30shineEntities())
            {
                var msg = new Library.Class.cls_message();
                var serialize = new JavaScriptSerializer();
                var listStaff = serialize.Deserialize<List<cls_staff>>(listJSONStaff).ToList();
                if (listStaff.Count > 0)
                {
                    var teamRecord = db.SuKien_Team.FirstOrDefault(w => w.Id == teamID);
                    if (teamRecord != null)
                    {
                        teamRecord.TeamName = teamName;
                        teamRecord.SalonId = salonID;
                        teamRecord.ModifiedTime = DateTime.Now;
                        db.SuKien_Team.AddOrUpdate(teamRecord);
                        db.SaveChanges();

                        var listStaffDB = db.SuKien_Team_Staff.Where(w => w.TeamId == teamID).ToList();
                        var index = -1;

                        //1. Kiểm tra trong listStaff truyền lên, nếu có item mới => insert
                        foreach (var v in listStaff)
                        {
                            index = listStaffDB.FindIndex(w => w.StaffId == v.Id);
                            if (index == -1)
                            {
                                var teamStaffRecord = new SuKien_Team_Staff();
                                teamStaffRecord.TeamId = teamRecord.Id;
                                teamStaffRecord.StaffId = v.Id;
                                teamStaffRecord.CreatedTime = DateTime.Now;
                                teamStaffRecord.IsDelete = false;
                                db.SuKien_Team_Staff.AddOrUpdate(teamStaffRecord);
                                db.SaveChanges();
                            }
                        }

                        //1. Kiểm tra trong listStaffDB, nếu item nào ko có trong list listStaff truyền lên => xóa
                        if (listStaffDB.Count > 0)
                        {
                            foreach (var v in listStaffDB)
                            {
                                index = listStaff.FindIndex(w => w.Id == v.StaffId);
                                if (index == -1)
                                {
                                    v.IsDelete = true;
                                    db.SuKien_Team_Staff.AddOrUpdate(v);
                                    db.SaveChanges();
                                }
                            }
                        }

                        msg.success = true;
                        msg.message = "Cập nhật thành công!";
                    }
                    else
                    {
                        msg.success = false;
                        msg.message = "Lỗi. Không tồn tại bản ghi!";
                    }

                }
                else
                {
                    msg.success = false;
                    msg.message = "Vui lòng chọn nhân viên vào team.";
                }
                return msg;
            }
        }

        /// <summary>
        /// Xử lý lấy dữ liệu Team
        /// </summary>
        /// <returns></returns>
        private List<cls_team> getDataTeam()
        {
            using (var db = new Solution_30shineEntities())
            {
                var data = new List<cls_team>();
                var item = new cls_team();
                var index = -1;
                int integer;
                int salonID = int.TryParse(Salon.SelectedValue, out integer) ? integer : 0;
                try
                {
                    DateTime timeFrom;
                    DateTime timeTo;
                    if (TxtDateTimeFrom.Text != "")
                    {
                        timeFrom = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                        if (TxtDateTimeTo.Text != "")
                        {
                            timeTo = Convert.ToDateTime(TxtDateTimeTo.Text, culture);
                        }
                        else
                        {
                            timeTo = timeFrom;
                        }

                        var dataStore = db.Store_SuKien_TeamThiDua_Diem(salonID, String.Format("{0:yyyy/MM/dd}", timeFrom), String.Format("{0:yyyy/MM/dd}", timeTo)).ToList();
                        if (dataStore.Count > 0)
                        {
                            foreach (var v in dataStore)
                            {
                                index = data.FindIndex(w => w.TeamID == v.TeamId);
                                if (index == -1)
                                {
                                    item = new cls_team();
                                    item.TeamID = v.TeamId.Value;
                                    item.TeamName = v.TeamName;
                                    item.TeamPoint = v.TeamPoint.Value;
                                    item.TeamStaffs += v.StaffName + ",";
                                    data.Add(item);
                                }
                                else
                                {
                                    item = data[index];
                                    item.TeamStaffs += v.StaffName + ",";
                                    data[index] = item;
                                }
                            }

                            var dataTemp = new List<cls_team>();
                            foreach (var v in data)
                            {
                                item = v;
                                item.TeamStaffs = item.TeamStaffs.TrimEnd(',');
                                dataTemp.Add(item);
                            }
                            data = dataTemp;
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                return data;
            }
        }

        /// <summary>
        /// Bind dữ liệu team ra view
        /// </summary>
        private void bindDataTeam()
        {
            var data = getDataTeam();
            Bind_Paging(data.Count);
            rptDataTeam.DataSource = data.Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
            rptDataTeam.DataBind();
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            bindDataTeam();
            RemoveLoading();
        }

    

        /// <summary>
        /// PAGING - Binding phân trang
        /// </summary>
        /// <param name="TotalRow"></param>
        protected void Bind_Paging(int TotalRow)
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(TotalRow) : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        /// <summary>
        /// PAGING - Tính tổng số bản ghi
        /// </summary>
        /// <param name="TotalRow"></param>
        /// <returns></returns>
        protected int Get_TotalPage(int TotalRow)
        {
            TotalRow -= PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        

        public void Repeater_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
              || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (Perm_Edit == true)
                    ((Panel)(e.Item.FindControl("EAedit"))).Visible = true;
                //if (Perm_Delete == true)
                //    ((Panel)(e.Item.FindControl("EAdelete"))).Visible = true;
            }
        }

        /// <summary>
        /// Class Nhân viên tối giản
        /// </summary>
        private class cls_staff
        {
            public int Id { get; set; }
            public string Fullname { get; set; }
        }

        /// <summary>
        /// Class dữ liệu team
        /// </summary>
        private class cls_team
        {
            public int TeamID { get; set; }
            public int SalonID { get; set; }
            public string TeamName { get; set; }
            public string TeamStaffs { get; set; }
          
            public double TeamPoint { get; set; }
        }

        private class cls_team_update
        {
            public int TeamID { get; set; }
            public int SalonID { get; set; }
            public string TeamName { get; set; }
            public List<cls_staff> ListStaff { get; set; }
        }
    }
    public class load
    {
        public string status { get; set; }
        public string message { get; set; }
        public bool success { get; set; }
    }

    public class cls_reportService : Service
    {
        public int times { get; set; }
        public long totalMoney { get; set; }
    }
}