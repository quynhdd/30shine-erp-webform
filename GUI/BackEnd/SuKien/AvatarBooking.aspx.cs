﻿using _30shine.Helpers;
using _30shine.Helpers.Http;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.SuKien
{
    public partial class AvatarBooking : System.Web.UI.Page
    {
        protected Paging PAGING = new Paging();
        public CultureInfo culture = new CultureInfo("vi-VN");
        public bool Perm_ViewAllData = false;
        private bool Perm_Access = false;
        protected bool Perm_Approve = false;
        protected string FirstDayOfMonth = string.Format("{0:dd/MM/yyyy}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1));
        protected string Today = String.Format("{0:dd/MM/yyyy}", DateTime.Now);
        IStyleMasterModel styleMaster = new StyleMasterModel();

        /// <summary>
        /// set permission
        /// </summary>
        public void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_Approve = permissionModel.CheckPermisionByAction("Perm_Approve", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        /// <summary>
        /// ExecuteByPermission
        /// </summary>
        private void ExecuteByPermission()
        {

            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        /// <summary>
        /// page load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                TxtDateTimeFrom.Text = FirstDayOfMonth;
                TxtDateTimeTo.Text = Today;
                Bind_Status();
            }
        }

        /// <summary>
        /// Bind status
        /// </summary>
        private void Bind_Status()
        {
            //    var Key = 0;
            //    ddlStatus.DataTextField = "StatusName";
            //    ddlStatus.DataValueField = "StatusId";
            //    ListItem item = new ListItem();
            //    ddlStatus.Items.Insert(0, item);

            //    foreach (var v in styleMaster.ListStatus())
            //    {
            //        Key++;
            //        item = new ListItem(v.Name, v.Id.ToString());
            //        ddlStatus.Items.Insert(Key, item);
            //    }
            //    ddlStatus.SelectedIndex = 0;
            ddlStatus.Items.Clear();
            var list = styleMaster.ListStatus();
            for (var i = 0; i < list.Count(); i++)
            {
                ddlStatus.Items.Add(new ListItem(list[i].Name, list[i].Id.ToString()));
            }
            ddlStatus.DataBind();
        }

        /// <summary>
        /// Bind data
        /// </summary>
        private List<Origin> BindData()
        {
            var list = new List<Origin>();
            var input = new cls_Search();
            int interger;
            input.timeFrom = new DateTime();
            input.timeTo = new DateTime();
            input.statusId = int.TryParse(ddlStatus.SelectedValue, out interger) ? interger : 0;
            if (TxtDateTimeFrom.Text != "")
            {
                input.timeFrom = Convert.ToDateTime(TxtDateTimeFrom.Text);
                if (TxtDateTimeTo.Text != "")
                {
                    if (TxtDateTimeFrom.Text == TxtDateTimeTo.Text)
                    {
                        input.timeTo = input.timeFrom;
                    }
                    else
                    {
                        input.timeTo = Convert.ToDateTime(TxtDateTimeTo.Text, culture);
                    }
                }
            }
            else
            {
                input.timeTo = input.timeFrom;
            }
            using (var client = new HttpClient())
            {
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                client.BaseAddress = new Uri(Libraries.AppConstants.URL_API_STAFF);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                var response = client.GetAsync("/api/staff-avatar/get-list?statusId=" + input.statusId + "&timeFrom=" + input.timeFrom + "&timeTo=" + input.timeTo + "").Result;
                var serializer = new JavaScriptSerializer();
                if (response.IsSuccessStatusCode)
                {
                    var result = response.Content.ReadAsStringAsync().Result;
                    var data = response.Content.ReadAsAsync<clsAvatar>().Result.data.ToList();
                    if (data != null)
                    {
                        Bind_Paging(data.Count());
                        RptAvatarBooking.DataSource = data.Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                        RptAvatarBooking.DataBind();
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// btn click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void _BtnClick(object sender, EventArgs e)
        {
            BindData();
            RemoveLoading();
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        /// <summary>
        ///Get Full name staff 
        /// </summary>
        public string GetName(int Id)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var list = db.Staffs.Where(w => w.Id == Id && w.Active == 1 && w.IsDelete == 0).ToList();
                    foreach (var v in list)
                    {
                        return v.Fullname.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return "";
        }
        /// <summary>
        /// Phân trang
        /// </summary>
        /// <param name="TotalRow"></param>
        protected void Bind_Paging(int TotalRow)
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(TotalRow) : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();
            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage(int TotalRow)
        {
            TotalRow -= PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        }
        public class clsAvatar
        {
            public int status { get; set; }
            public string message { get; set; }
            public List<Origin> data { get; set; }
        }
        public class Origin
        {
            public int id { get; set; }
            public int staffId { get; set; }
            public string images { get; set; }
            public int statusId { get; set; }
            public int approveUserId { get; set; }
        }
        //search
        public class cls_Search
        {
            public DateTime timeFrom { get; set; }
            public DateTime timeTo { get; set; }
            public int statusId { get; set; }
        }
    }
}