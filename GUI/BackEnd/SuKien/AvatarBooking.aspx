﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="AvatarBooking.aspx.cs" Inherits="_30shine.GUI.BackEnd.SuKien.AvatarBooking" %>

<asp:Content ID="Content2" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <style>
            .customer-listing table.table-listing tbody tr:hover .edit-wp.display_permission {
                display: none !important;
            }

            .display_permission {
                display: none;
            }

            .edit-wp {
                position: absolute;
                right: 3px;
                top: 24%;
                background: #e7e7e7;
                display: none;
            }

                .edit-wp .elm {
                    width: 17px;
                    height: 20px;
                    float: left;
                    display: block;
                    background: url(/Assets/images/icon.delete.small.active.png?v=2);
                    margin-right: 30px;
                }

                    .edit-wp .elm:hover {
                        background: url(/Assets/images/icon.delete.small.png?v=2);
                    }

            .wp_time_booking {
                width: 100%;
                float: left;
                padding-left: 55px;
                margin: 5px 0px 15px;
            }

                .wp_time_booking .a_time_booking {
                    float: left;
                    height: 26px;
                    line-height: 26px;
                    padding: 0px 15px;
                    background: #dfdfdf;
                    margin-right: 10px;
                    cursor: pointer;
                    position: relative;
                    font-size: 13px;
                    -webkit-border-radius: 15px;
                    -moz-border-radius: 15px;
                    border-radius: 15px;
                }

                    .wp_time_booking .a_time_booking:hover, .wp_time_booking .a_time_booking.active {
                        background: #fcd344;
                        color: #000;
                    }

                    .wp_time_booking .a_time_booking .span_time_booking {
                        position: absolute;
                        top: 24px;
                        left: 0;
                        width: 100%;
                        float: left;
                        text-align: center;
                        font-size: 13px;
                    }

            .edit-wp .elm {
                margin-right: 0;
            }

            table.table-total-report-1 {
                border-collapse: collapse;
            }

                table.table-total-report-1,
                table.table-total-report-1 th,
                table.table-total-report-1 td {
                    border: 1px solid black;
                }

                    table.table-total-report-1 td {
                        padding: 5px 15px;
                        text-align: right;
                    }

                        table.table-total-report-1 td:first-child {
                            text-align: left;
                        }

            .btn-action {
                padding: 0px 17px;
                margin: 0;
                background: none;
                border: none;
                font-size: 18px !important;
            }

                .btn-action span:hover {
                    color: green;
                }

            .edit-action-wp {
                float: left;
            }


            /*css modal*/
            /* Style the Image Used to Trigger the Modal */
            #myImg {
                border-radius: 5px;
                cursor: pointer;
                transition: 0.3s;
            }

                #myImg:hover {
                    opacity: 0.7;
                }

            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 11111111; /* Sit on top */
                padding-top: 100px; /* Location of the box */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
            }

            /* Modal Content (Image) */
            .modal-content {
                margin: auto;
                display: block;
                width: 80%;
                max-width: 700px;
            }

            /* Caption of Modal Image (Image Text) - Same Width as the Image */
            #caption {
                margin: auto;
                display: block;
                width: 80%;
                max-width: 700px;
                text-align: center;
                color: #ccc;
                padding: 10px 0;
                height: 150px;
            }

            /* Add Animation - Zoom in the Modal */
            /*.modal-content, #caption { 
                -webkit-animation-name: zoom;
                -webkit-animation-duration: 0.6s;
                animation-name: zoom;
                animation-duration: 0.6s;
            }*/

            @-webkit-keyframes zoom {
                from {
                    -webkit-transform: scale(0)
                }

                to {
                    -webkit-transform: scale(1)
                }
            }

            @keyframes zoom {
                from {
                    transform: scale(0)
                }

                to {
                    transform: scale(1)
                }
            }

            /* The Close Button */
            .close {
                position: absolute;
                top: 15px;
                right: 35px;
                color: #f1f1f1;
                font-size: 40px;
                font-weight: bold;
                transition: 0.3s;
            }

                .close:hover,
                .close:focus {
                    color: #bbb;
                    text-decoration: none;
                    cursor: pointer;
                }

            /* 100% Image Width on Smaller Screens */
            @media only screen and (max-width: 700px) {
                .modal-content {
                    width: 100%;
                }
            }

            /*end css modal*/

            .show-error {
                top: 120px;
                right: 40px;
                position: absolute;
                z-index: 10000;
                border: 1px solid darkcyan;
                border-radius: 4px;
                padding: 7px 20px;
                background: antiquewhite;
                display: none;
            }
        </style>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Danh sách &nbsp;&#187; </li>
                        <li class="be-report-li" style="background: none !important"><a href="javascript:void(0);"><i class="fa fa-th-large"></i>Avatar Booking</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="wp customer-add customer-listing be-report be-report-timekeeping">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <div class="row">
                    <strong class="st-head"><i class="fa fa-clock-o"></i>Thời gian</strong>
                    <div class="time-wp">
                        <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeFrom" placeholder="Từ ngày"
                            ClientIDMode="Static" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="ValidateTime" ControlToValidate="TxtDateTimeFrom" runat="server" CssClass="fb-cover-error" Text="Vui lòng chọn thời gian!"></asp:RequiredFieldValidator>
                    </div>
                    <strong class="st-head" style="margin-left: 10px;">
                        <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                    </strong>
                    <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeTo" placeholder="Đến ngày"
                        ClientIDMode="Static" runat="server"></asp:TextBox>
                    <div class="filter-item">
                        <strong class="st-head" style="margin-right: 5px;">Trạng thái: </strong>
                        <asp:DropDownList ID="ddlStatus" runat="server" ClientIDMode="Static" Style="width: 190px;">
                        </asp:DropDownList>
                    </div>
                    <a id="ViewData" onclick="excPaging(1)" class="st-head btn-viewdata" href="javascript:void(0)">Xem dữ liệu
                    </a>
                </div>
                <div class="row">
                    <strong class="st-head"><i class="fa fa-bars"></i>Danh sách Menu</strong>

                </div>
                <div class="wp customer-add customer-listing be-report">
                    <%-- Listing --%>
                    <div class="wp960 content-wp">
                        <!-- Filter -->
                        <!-- End Filter -->
                        <!-- Row Table Filter -->
                        <div class="table-func-panel" style="margin-top: -45px;">
                            <div class="table-func-elm">
                                <span>Số hàng / Page : </span>
                                <div class="table-func-input-wp">
                                    <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                                    <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                                    <ul class="ul-opt-segment">
                                        <li data-value="10">10</li>
                                        <li data-value="20">20</li>
                                        <li data-value="30">30</li>
                                        <li data-value="40">40</li>
                                        <li data-value="50">50</li>
                                        <li data-value="1000000">Tất cả</li>
                                    </ul>
                                    <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                                </div>
                            </div>
                        </div>
                        <!-- End Row Table Filter -->
                        <asp:ScriptManager runat="server" ID="ScriptManager2"></asp:ScriptManager>
                        <asp:UpdatePanel ID="UPTableListing" runat="server" OnItemDataBound="Repeater_ItemDataBound" ClientIDMode="Static">
                            <ContentTemplate>
                                <div class="table-wp">
                                    <table class="table-add table-listing">
                                        <thead>
                                            <tr>
                                                <th>STT</th>
                                                <th>Mã NV</th>
                                                <th>Hình ảnh</th>
                                                <th>Trạng thái</th>
                                                <th>Người duyệt</th>
                                                <th style="width: 10%; min-width: 150px;">Hành động</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <asp:Repeater ID="RptAvatarBooking" runat="server">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td><%# Container.ItemIndex + 1 + PAGING._Offset %></td>
                                                        <td><%# GetName(Convert.ToInt32(Eval("staffId"))) %></td>
                                                        <td>
                                                        <img src="<%#Eval("images").ToString()  %>" style="width:119px;height:119px;"/>
                                                            <%--<button type="button" class="btn btn-link" onclick="ViewImage($(this),<%#Eval("staffId") %>)">Xem ảnh</button>--%>
                                                        </td>
                                                        <td class="image-status"><%#Eval("statusId").ToString() == "1" ? "Chưa duyệt" : Eval("statusId").ToString() == "2" ? "Không duyệt" : "Đã duyệt"%>   </td>
                                                        <td class="approve-user"><%# Eval("approveUserId")== null? "0" :GetName(Convert.ToInt32(Eval("approveUserId"))) %></td>
                                                        <td style="width: 10%; min-width: 150px;">
                                                            <asp:Panel CssClass="edit-action-wp action-edit" runat="server" ID="EAedit" Visible="False">
                                                                <a class="btn btn-action btn-action-edit details1" href="#" title="Sửa"><span class="fa fa-pencil-square-o" aria-hidden="true"></span></a>
                                                            </asp:Panel>
                                                            <asp:Panel CssClass="edit-action-wp action-confirm" runat="server" ID="EAconfirm" Visible="true">
                                                                <button type="button" class="btn btn-action btn-action-confirm" data-id="<%#Eval("staffId")%>" onclick="ApproveClick($(this))" <%#Eval("statusId").ToString() != "1" ? "disabled='disabled'" : "" %> title="Duyệt/Không duyệt">
                                                                    <span><i class="fa fa-check" aria-hidden="true"></i>/<i class="fa fa-ban" aria-hidden="true"></i>
                                                                    </span>
                                                                </button>
                                                            </asp:Panel>
                                                            <asp:Panel CssClass="edit-action-wp action-del" runat="server" ID="EAdel" Visible="true">
                                                                <button type="button" data-id="<%#Eval("staffId")%>" class="btn btn-action btn-action-del" onclick="DeleteData($(this))" title="Xóa"><span class="fa fa-trash" aria-hidden="true"></span></button>
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </tbody>
                                    </table>
                                </div>

                                <!-- Paging -->
                                <div class="site-paging-wp">
                                    <% if (PAGING.TotalPage > 1)
                                        { %>
                                    <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                                        <% if (PAGING._Paging.Prev != 0)
                                            { %>
                                        <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                                        <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                                        <% } %>
                                        <asp:Repeater ID="RptPaging" runat="server">
                                            <ItemTemplate>
                                                <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                                    <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>>
                                                    <%# Eval("PageNum") %>
                                                </a>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                                            { %>
                                        <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                                        <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                                        <% } %>
                                    </asp:Panel>
                                    <% } %>
                                </div>
                                <!-- End Paging -->
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                        <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
                    </div>
                    <%-- END Listing --%>
                </div>

            </div>
            <%-- END Listing --%>
        </div>
        <div class="show-error"></div>
        <!-- The Modal -->
        <div id="modal-image" class="modal">
            <!-- The Close Button -->
            <span class="close" onclick="CloseModal()">&times;</span>
            <!-- Modal Content (The Image) -->
            <div class="modal-content">
            </div>
        </div>

        <script type="text/javascript">

            jQuery(document).ready(function () {
                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) { }
                });
            });
            // call url
            var URL_API_STAFF = "<%= Libraries.AppConstants.URL_API_STAFF %>";
            //modal show image
            function CloseModal() {
                $("#modal-image").css("display", "none");
            }
            function ViewImage(This, id) {
                $("#modal-image").css("display", "block");
                GetImage(id);
            }
            // get data show image
            function GetImage(id) {
                $.ajax({
                    type: "GET",
                    url: URL_API_STAFF + "/api/staff-avatar/get-data-by-app?staffId=" + id + "",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var status = data.statusId;
                        console.log(status);
                        var result = "";
                        //slide show image
                        result += '<div class="slideshow-container">';

                        result += '<div class="mySlides">';
                        result += '<img src="' + data.images + '" style="width:100%;height:100%;"/>';
                        result += '</div>';
                        result += '</div>';
                        $(".modal-content").empty().append(result);
                        $(".slideshow-container").slick();

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(textStatus);
                        console.log(jqXHR);
                        console.log(errorThrown);
                    },
                    failure: function (response) { alert(response.d); }
                })
            }

            function ApproveClick(This) {
                var staffId = This.attr("data-id");
                var date = new Date();

                var c = confirm("Duyệt ảnh? Yes:Duyệt. Cancel: Không duyệt");
                if (c == true) {
                    var approveUserId = <%=Session["User_Id"].ToString()%>;
                    var statusId = 3;
                    ApproveImage(staffId, approveUserId, statusId);
                    This.attr("disabled", "disabled");
                } else {
                    var approveUserId = <%=Session["User_Id"].ToString()%>;
                    var statusId = 2;
                    NonApproveImage(staffId, approveUserId, statusId);
                    This.attr("disabled", "disabled");
                }
            }
            //Không duyệt ảnh
            function NonApproveImage(Id, ApproveUserId, StatusId) {
                var staffId = Id;
                var approveUserId = ApproveUserId;
                var statusId = StatusId;
                var data = JSON.stringify({ staffId, approveUserId, statusId });
                $.ajax({
                    type: "PUT",
                    data: data,
                    url: URL_API_STAFF + "/api/staff-avatar/update",
                    dataType: "JSON",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        console.log(data.d);
                        var result = "";
                        if (data.d === 0) {
                            result = "<span style='color:red'>Có lỗi xảy ra </span>";
                        }
                        else { result = "<span style='color:blue'>Không duyệt ảnh</span>"; }
                        $(".show-error").show();
                        $(".show-error").empty().append(result);

                        setTimeout(function () {
                            $('.show-error').fadeOut('fast');
                        }, 2000);

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(textStatus);
                        console.log(jqXHR);
                        console.log(errorThrown);
                    },
                    failure: function (response) { alert(response.d); },
                })
            }
            //Duyệt ảnh
            function ApproveImage(Id, ApproveUserId, StatusId) {
                var staffId = Id;
                var approveUserId = ApproveUserId;
                var statusId = StatusId;
                var data = JSON.stringify({ staffId, approveUserId, statusId });
                $.ajax({
                    type: "PUT",
                    data: data,
                    url: URL_API_STAFF + "/api/staff-avatar/update",
                    dataType: "JSON",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        console.log(data.d);
                        var result = "";
                        if (data.d === 0) {
                            result = "<span style='color:red'>Có lỗi xảy ra </span>";
                        }
                        else { result = "<span style='color:blue'>Đã duyệt ảnh</span>"; }
                        $(".show-error").show();
                        $(".show-error").empty().append(result);

                        setTimeout(function () {
                            $('.show-error').fadeOut('fast');
                        }, 2000);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(textStatus);
                        console.log(jqXHR);
                        console.log(errorThrown);
                    },
                    failure: function (response) { alert(response.d); },
                })
            }

            //delete 
            function DeleteData(This) {
                var id = This.attr("data-id");
                var c = confirm("Bạn chắc chắn muốn xóa ảnh này?");
                if (c == true) {
                    $.ajax({
                        type: "DELETE",
                        url: URL_API_STAFF + "/api/staff-avatar?staffId=" + id + "",
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            console.log(data.statusId);
                            var result = "";
                            if (data.status == 1) {
                                This.closest('tr').remove();
                                result = "<span style='color:blue'>Xóa thành công</span>";
                            }
                            else { result = "<span style='color:red'>Xóa thất bại</span>"; }
                            $(".show-error").show();
                            $(".show-error").empty().append(result);

                            setTimeout(function () {
                                $('.show-error').fadeOut('fast');
                            }, 2000);
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log(textStatus);
                            console.log(jqXHR);
                            console.log(errorThrown);
                        },
                        failure: function (response) { alert(response.d); }
                    })
                }
                else return false;
            }
        </script>
    </asp:Panel>
</asp:Content>
