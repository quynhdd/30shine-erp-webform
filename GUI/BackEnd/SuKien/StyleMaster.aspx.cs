﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;

namespace _30shine.GUI.BackEnd.SuKien
{
    public partial class StyleMaster : System.Web.UI.Page
    {
        private string PageID = "STYLEMASTER";
        private DateTime time;
        protected bool Perm_Access = false;
        protected bool Perm_Edit = false;
        protected bool Perm_Delete = false;
        protected bool Perm_Approve = false;
        protected Paging PAGING = new Paging();
        IStyleMasterModel styleMaster = new StyleMasterModel();
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            //txtStartDate.Text = DateTime.Now.ToString("MM/yyyy",culture);
            if (!IsPostBack)
            {
                Bind_Status();
                //Bind_Paging();
                //Bind_Data();
                //RemoveLoading();
            }
        }

        readonly CultureInfo culture = new CultureInfo("vi-VN");

        private void SetPermission()
        {
            
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                Perm_Approve = permissionModel.CheckPermisionByAction("Perm_Approve", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        public void Repeater_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
                || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (Perm_Edit == true)
                    ((Panel)(e.Item.FindControl("EAedit"))).Visible = true;
                if (Perm_Delete == true)
                    ((Panel)(e.Item.FindControl("EAdelete"))).Visible = true;
            }
        }
        private void Bind_Status()
        {
            var Key = 0;
            ddlStatus.DataTextField = "StatusName";
            ddlStatus.DataValueField = "StatusId";
            ListItem item = new ListItem("Chọn tất cả", "0");
            ddlStatus.Items.Insert(0, item);

            foreach (var v in styleMaster.ListStatus())
            {
                Key++;
                item = new ListItem(v.Name, v.Id.ToString());
                ddlStatus.Items.Insert(Key, item);
            }
            ddlStatus.SelectedIndex = 0;
        }
        private void Bind_Data()
        {
            var data = styleMaster.ListStyleMaster(Convert.ToDateTime(txtStartDate.Text, culture),
                Convert.ToInt32(ddlStatus.SelectedValue)).Skip(PAGING._Offset).Take(PAGING._Segment);
            rptStyleMaster.DataSource = data;
            rptStyleMaster.DataBind();
        }
        private void Bind_Paging()
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? (HDF_Page.Value != "" ? Convert.ToInt32(HDF_Page.Value) : 1) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage() : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();
            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }
        private int Get_TotalPage()
        {
            using (var db = new Solution_30shineEntities())
            {
                this.time = Convert.ToDateTime(txtStartDate.Text, culture);
                int Count = styleMaster.ListStyleMaster(time,Convert.ToInt32(ddlStatus.SelectedValue)).Count;
                int TotalRow = Count - PAGING._TopNewsNum;
                int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((float)TotalRow / PAGING._Segment));
                return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
            }
        }
        protected void _BtnClick(object sender,EventArgs e)
        {
            Bind_Paging();
            Bind_Data();
            RemoveLoading();
        }
        [WebMethod(EnableSession = true)]
        public static object ApproveImage(int id,int status)
        {
            Solution_30shineEntities db = new Solution_30shineEntities();
            var approveId = Convert.ToInt32(HttpContext.Current.Session["User_Id"].ToString());
            IStyleMasterModel masterModel = new StyleMasterModel();
            var r = masterModel.Approve(id, approveId, status);
            if (r)
            {
                var getResult = masterModel.StyleMaster(id);
                return new JavaScriptSerializer().Serialize(getResult);
            }
            else
                return new JavaScriptSerializer().Serialize(0);
        }

        [WebMethod]
        public static object GetImage(int id)
        {
            IStyleMasterModel masterModel = new StyleMasterModel();
            return new JavaScriptSerializer().Serialize(masterModel.StyleMaster(id));
        }
        [WebMethod]
        public static object Del(int id)
        {
            IStyleMasterModel masterModel = new StyleMasterModel();
            var r = masterModel.Delete(id);
            if (r)
            {
                return new JavaScriptSerializer().Serialize(1);
            }
            else
            {
                return new JavaScriptSerializer().Serialize(0);
            }
        }
        private void AddLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "addLoading();", true);
        }

        private void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

    }
}