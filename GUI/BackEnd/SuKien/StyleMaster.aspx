﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StyleMaster.aspx.cs" MasterPageFile="~/TemplateMaster/SiteMaster.Master" Inherits="_30shine.GUI.BackEnd.SuKien.StyleMaster" %>

<asp:Content ID="Content2" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <style>
            .customer-listing table.table-listing tbody tr:hover .edit-wp.display_permission {
                display: none !important;
            }

            .display_permission {
                display: none;
            }

            .edit-wp {
                position: absolute;
                right: 3px;
                top: 24%;
                background: #e7e7e7;
                display: none;
            }

                .edit-wp .elm {
                    width: 17px;
                    height: 20px;
                    float: left;
                    display: block;
                    background: url(/Assets/images/icon.delete.small.active.png?v=2);
                    margin-right: 30px;
                }

                    .edit-wp .elm:hover {
                        background: url(/Assets/images/icon.delete.small.png?v=2);
                    }

            .wp_time_booking {
                width: 100%;
                float: left;
                padding-left: 55px;
                margin: 5px 0px 15px;
            }

                .wp_time_booking .a_time_booking {
                    float: left;
                    height: 26px;
                    line-height: 26px;
                    padding: 0px 15px;
                    background: #dfdfdf;
                    margin-right: 10px;
                    cursor: pointer;
                    position: relative;
                    font-size: 13px;
                    -webkit-border-radius: 15px;
                    -moz-border-radius: 15px;
                    border-radius: 15px;
                }

                    .wp_time_booking .a_time_booking:hover, .wp_time_booking .a_time_booking.active {
                        background: #fcd344;
                        color: #000;
                    }

                    .wp_time_booking .a_time_booking .span_time_booking {
                        position: absolute;
                        top: 24px;
                        left: 0;
                        width: 100%;
                        float: left;
                        text-align: center;
                        font-size: 13px;
                    }

            .edit-wp .elm {
                margin-right: 0;
            }

            table.table-total-report-1 {
                border-collapse: collapse;
            }

                table.table-total-report-1,
                table.table-total-report-1 th,
                table.table-total-report-1 td {
                    border: 1px solid black;
                }

                    table.table-total-report-1 td {
                        padding: 5px 15px;
                        text-align: right;
                    }

                        table.table-total-report-1 td:first-child {
                            text-align: left;
                        }
                        .btn-action {
                            padding: 0px 17px;
                            margin: 0;
                            background: none;
                            border: none;
                            font-size: 18px !important;
                        }
                        .btn-action span:hover {
                            color: green;
                        }
                        .edit-action-wp {
                            float: left;
                        }


            /*css modal*/
               /* Style the Image Used to Trigger the Modal */
            #myImg {
                border-radius: 5px;
                cursor: pointer;
                transition: 0.3s;
            }

            #myImg:hover {opacity: 0.7;}

            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 11111111; /* Sit on top */
                padding-top: 100px; /* Location of the box */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
            }

            /* Modal Content (Image) */
            .modal-content {
                margin: auto;
                display: block;
                width: 80%;
                max-width: 700px;
            }

            /* Caption of Modal Image (Image Text) - Same Width as the Image */
            #caption {
                margin: auto;
                display: block;
                width: 80%;
                max-width: 700px;
                text-align: center;
                color: #ccc;
                padding: 10px 0;
                height: 150px;
            }

            /* Add Animation - Zoom in the Modal */
            /*.modal-content, #caption { 
                -webkit-animation-name: zoom;
                -webkit-animation-duration: 0.6s;
                animation-name: zoom;
                animation-duration: 0.6s;
            }*/

            @-webkit-keyframes zoom {
                from {-webkit-transform:scale(0)} 
                to {-webkit-transform:scale(1)}
            }

            @keyframes zoom {
                from {transform:scale(0)} 
                to {transform:scale(1)}
            }

            /* The Close Button */
            .close {
                position: absolute;
                top: 15px;
                right: 35px;
                color: #f1f1f1;
                font-size: 40px;
                font-weight: bold;
                transition: 0.3s;
            }

            .close:hover,
            .close:focus {
                color: #bbb;
                text-decoration: none;
                cursor: pointer;
            }

            /* 100% Image Width on Smaller Screens */
            @media only screen and (max-width: 700px){
                .modal-content {
                    width: 100%;
                }
            }

            /*end css modal*/

            .show-error {
                top: 120px;
                right: 40px;
                position: absolute;
                z-index: 10000;
                border: 1px solid darkcyan;
                border-radius: 4px;
                padding: 7px 20px;
                background: antiquewhite;
                display: none;
            }
        </style>
        <%--<uc1:ReportMenu runat="server" id="ReportMenu" />--%>

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Danh sách &nbsp;&#187; </li>
                        <li class="be-report-li" style="background: none !important"><a href="javascript:void(0);" ><i class="fa fa-th-large"></i>Style Master</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing be-report be-report-timekeeping">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <div class="row">
                    <div class="filter-item">
                        <strong class="st-head"><i class="fa fa-clock-o"></i>Thời gian</strong>
                        <div class="time-wp">
                            <asp:TextBox CssClass="txtDateTime st-head" ID="txtStartDate" placeholder="Chọn tháng" Style="margin-left: 10px!important" ClientIDMode="Static" runat="server"></asp:TextBox>
                            <%--<asp:RequiredFieldValidator ID="ValidateTime" ControlToValidate="txtStartDate" runat="server" CssClass="fb-cover-error" Text="Vui lòng chọn thời gian!"></asp:RequiredFieldValidator>--%>
                        </div>
                    </div>
                    <%--<div class="filter-item">
                        <strong class="st-head" style="margin-right: 5px;">Nguồn dự thi: </strong>
                        <asp:DropDownList ID="ddlSource" runat="server" ClientIDMode="Static" Style="width: 190px;">
                            <asp:ListItem Selected="True" Value="0">Chọn tất cả</asp:ListItem>
                            <asp:ListItem Value="1">Top SCSC</asp:ListItem>
                            <asp:ListItem Value="2">Stylist</asp:ListItem>
                        </asp:DropDownList>
                    </div>--%>
                    <div class="filter-item">
                        <strong class="st-head" style="margin-right: 5px;">Trạng thái: </strong>
                        <asp:DropDownList ID="ddlStatus" runat="server" ClientIDMode="Static" Style="width: 190px;">
                        </asp:DropDownList>
                    </div>
                    <a id="ViewData" onclick="excPaging(1)" class="st-head btn-viewdata" href="javascript:void(0)">Xem dữ liệu
                    </a>
                </div>
                <div class="row">
                    <strong class="st-head"><i class="fa fa-bars"></i>Danh sách Menu</strong>
                
                </div>
                <div class="wp customer-add customer-listing be-report">
                    <%-- Listing --%>
                    <div class="wp960 content-wp">
                        <!-- Filter -->
                        <!-- End Filter -->
                        <!-- Row Table Filter -->
                        <div class="table-func-panel" style="margin-top: -45px;">
                            <div class="table-func-elm">
                                <span>Số hàng / Page : </span>
                                <div class="table-func-input-wp">
                                    <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                                    <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                                    <ul class="ul-opt-segment">
                                        <li data-value="10">10</li>
                                        <li data-value="20">20</li>
                                        <li data-value="30">30</li>
                                        <li data-value="40">40</li>
                                        <li data-value="50">50</li>
                                        <li data-value="1000000">Tất cả</li>
                                    </ul>
                                    <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                                </div>
                            </div>
                        </div>
                        <!-- End Row Table Filter -->
                        <asp:ScriptManager runat="server" ID="ScriptManager2"></asp:ScriptManager>
                        <asp:UpdatePanel ID="UPTableListing" runat="server" OnItemDataBound="Repeater_ItemDataBound" ClientIDMode="Static">
                            <ContentTemplate>
                                <div class="table-wp">
                                    <table class="table-add table-listing">
                                        <thead>
                                        <tr>
                                            <th>STT</th>
                                            <th>Mã số</th>
                                            <th>Stylist</th>
                                            <th>Hình ảnh</th>
                                            <%--<th>Hình ảnh 2</th>
                                            <th>Hình ảnh 3</th>
                                            <th>Hình ảnh 4</th>--%>
                                            <th>Nguồn ảnh</th>
                                            <th>Trạng thái</th>
                                            <th>Số lượt thích</th>
                                            <th>Ngày duyệt</th>
                                            <th>Người duyệt</th>
                                            <th style="width: 10%; min-width: 150px;">Hành động</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            <asp:Repeater ID="rptStyleMaster" runat="server">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td><%# Container.ItemIndex + 1 + PAGING._Offset %></td>
                                                        <td><%#Eval("PostNumber") %></td>
                                                        <td><%#Eval("StylistName")%></td>
                                                        <td><button type="button" class="btn btn-link" onclick="ViewImage($(this),<%#Eval("Id") %>)">Xem ảnh</button></td>
                                                        <%--<td><img src="<%#Eval("Image1")%>" onclick="GetImage($(this),'<%#Eval("Image1")%>')" id="image-<%#Eval("Id")%>" class="image" alt="" title="" data-img="<%#Eval("Image1")%>" width="50px" height="50px"></td>
                                                        <td><img src="<%#Eval("Image2")%>" onclick="GetImage($(this),'<%#Eval("Image2")%>')" class="image" alt="" title="" data-img="<%#Eval("Image2")%>" width="50px" height="50px"></td>
                                                        <td><img src="<%#Eval("Image3")%>" onclick="GetImage($(this),'<%#Eval("Image3")%>')" class="image" alt="" title="" data-img="<%#Eval("Image3")%>" width="50px" height="50px"></td>
                                                        <td><img src="<%#Eval("Image4")%>" onclick="GetImage($(this),'<%#Eval("Image4")%>')" class="image" alt="" title="" data-img="<%#Eval("Image4")%>" width="50px" height="50px"></td>--%>
                                                        <td><%#Eval("ImageSource").ToString() == "1" ? "TOP SCSC" : "Stylist"%></td>
                                                        <td class="image-status"><%#Eval("StyleMasterStatusId").ToString() == "1" ? "Chưa duyệt" : Eval("StyleMasterStatusId").ToString() == "2" ? "Không duyệt" : "Đã duyệt"%>   </td>
                                                        <td><%#Eval("TotalLike") ?? 0  %></td>
                                                        <td class="approve-time"><%#Eval("ApproveTime") %></td>
                                                        <td class="approve-user"><%#Eval("ApproveUserId") %></td>
                                                        <td style="width: 10%; min-width: 150px;">
                                                            <asp:Panel CssClass="edit-action-wp action-edit" runat="server" ID="EAedit" Visible="False">
                                                                <a class="btn btn-action btn-action-edit details1" href="#" title="Sửa"><span class="fa fa-pencil-square-o" aria-hidden="true"></span></a>
                                                            </asp:Panel>
                                                            <asp:Panel CssClass="edit-action-wp action-confirm" runat="server" ID="EAconfirm" Visible="true">
                                                                <button type="button" class="btn btn-action btn-action-confirm" data-id="<%#Eval("Id")%>" onclick="ApproveClick($(this))" <%#Eval("StyleMasterStatusId").ToString() != "1" ? "disabled='disabled'" : "" %>  title="Duyệt/Không duyệt"><span><i class="fa fa-check" aria-hidden="true"></i>/<i class="fa fa-ban" aria-hidden="true"></i></span></button>
                                                                <%--<button type="button" class="btn btn-action btn-action-cancel" title="Không duyệt"></button>--%>
                                                            </asp:Panel>
                                                            <asp:Panel CssClass="edit-action-wp action-del" runat="server" ID="EAdel" Visible="true">
                                                                <button type="button" data-id="<%#Eval("Id")%>" class="btn btn-action btn-action-del" onclick="DeleteData($(this))" title="Xóa"><span class="fa fa-trash" aria-hidden="true"></span></button>
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </tbody>
                                    </table>
                                </div>

                                <!-- Paging -->
                                <div class="site-paging-wp">
                                    <% if (PAGING.TotalPage > 1)
                                        { %>
                                    <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                                        <% if (PAGING._Paging.Prev != 0)
                                            { %>
                                        <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                                        <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                                        <% } %>
                                        <asp:Repeater ID="RptPaging" runat="server">
                                            <ItemTemplate>
                                                <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                                    <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>>
                                                    <%# Eval("PageNum") %>
                                                </a>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                                            { %>
                                        <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                                        <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                                        <% } %>
                                    </asp:Panel>
                                    <% } %>
                                </div>
                                <!-- End Paging -->
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                        <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
                    </div>
                    <%-- END Listing --%>
                </div>

            </div>
            <%-- END Listing --%>
        </div>
        <div class="show-error"></div>
        <!-- The Modal -->
        <div id="modal-image" class="modal">
            <!-- The Close Button -->
            <span class="close" onclick="CloseModal()">&times;</span>
            <!-- Modal Content (The Image) -->
            <div class="modal-content">
                
            </div>
        </div>
        <script type="text/javascript">

           
            jQuery(document).ready(function () {
                $(document).keydown(function (event) {
                    if (event.keyCode == 27) {
                        $('#modal-image').hide();
                    }
                });
                // Add active menu
                $("#glbBooking").addClass("active");
                $("#glbAdminBooking_Test").addClass("active");
                $("li.be-report-li").addClass("active");
                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datepicker({
                    //dayOfWeekStart: 1,
                    language: 'vi',
                    format: "mm/yyyy",
                    //showWeekDays:true,
                    startViewMode: 1,
                    minViewMode: 1,
                    autoclose: true,
                    onChangeDateTime: function (dp, $input) {
                    }
                });

            });
            function DeleteData(This) {
                var id = This.attr("data-id");
                var c = confirm("Bạn chắc chắn muốn xóa bài dự thi này?");
                if (c == true) {
                    $.ajax({
                        type: "POST",
                        url: "/GUI/BackEnd/SuKien/StyleMaster.aspx/Del",
                        data: "{id:" + id + "}",
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            console.log(data.d);
                            var result = "";
                            if (data.d == 1) {
                                This.closest('tr').remove();
                                result = "<span style='color:blue'>Xóa thành công</span>";
                            }
                            else { result = "<span style='color:red'>Xóa thất bại</span>"; }
                            $(".show-error").show();
                            $(".show-error").empty().append(result);

                            setTimeout(function () {
                                $('.show-error').fadeOut('fast');
                            }, 2000);
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log(textStatus);
                            console.log(jqXHR);
                            console.log(errorThrown);
                        },
                        failure: function (response) { alert(response.d); }
                    })
                }
                else return false;
            }
            function ApproveClick(This) {
                var id = This.attr("data-id");
                var date = new Date();
               
                var c = confirm("Duyệt bài thi? Yes:Duyệt. Cancel: Không duyệt");
                if (c === true) {
                    ApproveImage(id);
                    This.closest("tr").find("td.image-status").empty().html("Đã Duyệt");
                    This.closest("tr").find("td.approve-time").empty().html(formatDate(date));
                    This.closest("tr").find("td.approve-user").empty().html(<%=Session["User_Id"].ToString()%>);
                    This.attr("disabled", "disabled");
                } else {
                    NonApproveImage(id);
                    This.closest("tr").find("td.image-status").empty().html("Không Duyệt");
                    This.closest("tr").find("td.approve-time").empty().html(formatDate(date));
                    This.closest("tr").find("td.approve-user").empty().html(<%=Session["User_Id"].ToString()%>);
                    This.attr("disabled", "disabled");
                }
            }
            //Không duyệt bài thi
            function NonApproveImage(id) {
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/SuKien/StyleMaster.aspx/ApproveImage",
                    data: "{id:" + id + ",status:2}",
                    dataType: "JSON",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        console.log(data.d);
                        var result = "";
                        if (data.d === 0) {
                            result = "<span style='color:red'>Có lỗi xảy ra </span>";
                        }
                        else { result = "<span style='color:blue'>Không duyệt bài thi</span>"; }
                        $(".show-error").show();
                        $(".show-error").empty().append(result);

                        setTimeout(function () {
                            $('.show-error').fadeOut('fast');
                        }, 2000);

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(textStatus);
                        console.log(jqXHR);
                        console.log(errorThrown);
                    },
                    failure: function (response) { alert(response.d); },
                })
            }
            //Duyệt bài thi
            function ApproveImage(id) {
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/SuKien/StyleMaster.aspx/ApproveImage",
                    data: "{id:" + id + ",status:3}",
                    dataType: "JSON",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        console.log(data.d);
                        var result = "";
                        if (data.d === 0) {
                            result = "<span style='color:red'>Có lỗi xảy ra </span>";
                        }
                        else { result = "<span style='color:blue'>Đã duyệt bài thi</span>"; }
                        $(".show-error").show();
                        $(".show-error").empty().append(result);

                        setTimeout(function () {
                            $('.show-error').fadeOut('fast');
                        }, 2000);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(textStatus);
                        console.log(jqXHR);
                        console.log(errorThrown);
                    },
                    failure: function (response) { alert(response.d); },
                })
            }
            //modal show image
            function CloseModal() {
                $("#modal-image").css("display", "none");
            }
            function ViewImage(This, id) {
                $("#modal-image").css("display", "block");
                GetImage(id);
            }
        
            function GetImage(id) {
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/SuKien/StyleMaster.aspx/GetImage",
                    data: "{id:" + id + "}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var jsData = JSON.parse(data.d);
                        console.log(jsData);
                        var result = "";
                        //slide show image
                        result += '<div class="slideshow-container">';

                        result += '<div class="mySlides">';
                        result += '<img src="' + jsData.Image1 + '" style="width:100%;height:100%;"/>';
                        result += '</div>';

                        result += '<div class="mySlides">';
                        result += '<img src="' + jsData.Image2 + '" style="width:100%;height:100%;"/>';
                        result += '</div>';

                        result += '<div class="mySlides">';
                        result += '<img src="' + jsData.Image3 + '" style="width:100%;height:100%;"/>';
                        result += '</div>';

                        result += '<div class="mySlides">';
                        result += '<img src="' + jsData.Image4 + '" style="width:100%;height:100%;"/>';
                        result += '</div>';

                        result += '</div>';
                        $(".modal-content").empty().append(result);
                        $(".slideshow-container").slick();

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(textStatus);
                        console.log(jqXHR);
                        console.log(errorThrown);
                    },
                    failure: function (response) { alert(response.d); }
                })
            }
            //format date
            function formatDate(date) {
                var hours = date.getHours();
                var minutes = date.getMinutes();
                var seconds = date.getSeconds();
                var ampm = hours >= 12 ? 'PM' : 'AM';
                hours = hours % 12;
                hours = hours ? hours : 12; // the hour '0' should be '12'
                minutes = minutes < 10 ? '0' + minutes : minutes;
                seconds = seconds < 10 ? '0' + seconds : seconds; 
                var strTime = hours + ':' + minutes + ':'+seconds+ ' ' + ampm;
                return date.getMonth() + 1 + "/" + date.getDate() + "/" + date.getFullYear() + "  " + strTime;
            }

        </script>

    </asp:Panel>
</asp:Content>
