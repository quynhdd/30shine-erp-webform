﻿using _30shine.Helpers;
using _30shine.MODEL.ENTITY.EDMX;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.SkinAttribute
{
    public partial class SkinAtt_A : System.Web.UI.Page
    {
        protected Skin_Attribute OBJ;
        protected string _Code;
        protected bool _IsUpdate = false;
        public UIHelpers _UIHelpers = new UIHelpers();
        protected cls_media Thumb = new cls_media();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (IsUpdate())
                {
                    if (Bind_OBJ())
                    {
                        Bind_Pid();
                    }
                }
                else
                {
                    Bind_Pid();
                }

            }
        }

        protected void ExcAddOrUpdate(object sender, EventArgs e)
        {
            if (IsUpdate())
            {
                Update();
            }
            else
            {
                Add();
            }
        }

        private void Add()
        {
            using (var db = new Solution_30shineEntities())
            {
                var obj = new Skin_Attribute();
                //int integer;
                obj.SkinTypeName = Name.Text;
                //obj.Description = Description.Text;
                //obj.Thumb = HDF_Images.Value;
                obj.ParentId = Convert.ToInt32(Pid.SelectedValue);
                //---------
                //obj.OnWebId = int.TryParse(OnWebId.Text, out integer) ? integer : 0;
                //---------
                obj.Order = _Order.Text != "" ? Convert.ToInt32(_Order.Text) : 0;
                obj.Status = Publish.Checked;
                //obj.CreatedTime = DateTime.Now;
                obj.IsDelete = false;

                // Validate
                var Error = false;

                if (!Error)
                {
                    db.Skin_Attribute.Add(obj);
                    var exc = db.SaveChanges();

                    if (exc > 0)
                    {
                        var MsgParam = new List<KeyValuePair<string, string>>();
                        MsgParam.Add(new KeyValuePair<string, string>("msg_add_status", "success"));
                        MsgParam.Add(new KeyValuePair<string, string>("msg_add_message", "Cập nhật thành công!"));
                        UIHelpers.Redirect("/admin/loai-da/danh-sach.html", MsgParam);
                    }
                    else
                    {
                        var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển.";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                    }
                }
            }
        }

        private void Update()
        {
            using (var db = new Solution_30shineEntities())
            {
                int Id;
                //int integer;
                if (int.TryParse(_Code, out Id))
                {
                    OBJ = db.Skin_Attribute.FirstOrDefault(w => w.Id == Id);

                    if (!OBJ.Equals(null))
                    {
                        OBJ.SkinTypeName = Name.Text;
                        //OBJ.Description = Description.Text;
                        //OBJ.Thumb = HDF_Images.Value;
                        OBJ.ParentId = Convert.ToInt32(Pid.SelectedValue);
                        //---------
                        //OBJ.OnWebId = int.TryParse(OnWebId.Text, out integer) ? integer : 0;
                        //---------
                        OBJ.Order = _Order.Text != "" ? Convert.ToInt32(_Order.Text) : 0;
                        OBJ.Status = Publish.Checked;
                        //OBJ.CreatedTime = DateTime.Now;
                        OBJ.IsDelete = false;

                        db.Skin_Attribute.AddOrUpdate(OBJ);
                        var exc = db.SaveChanges();
                        if (exc > 0)
                        {
                            var MsgParam = new List<KeyValuePair<string, string>>();
                            MsgParam.Add(new KeyValuePair<string, string>("msg_add_status", "success"));
                            MsgParam.Add(new KeyValuePair<string, string>("msg_add_message", "Cập nhật thành công!"));
                            UIHelpers.Redirect("/admin/loai-da/" + OBJ.Id + ".html", MsgParam);
                        }
                        else
                        {
                            var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển!";
                            var status = "warning";
                            UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                        }
                    }
                    else
                    {
                        var msg = "Lỗi! Không tìm thấy danh mục.";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);

                    }
                }
                else
                {
                    MsgSystem.Text = "Lỗi! Không tìm thấy danh mục.";
                    MsgSystem.CssClass = "msg-system warning";
                }

                Context.RewritePath("+", "", "");
            }
        }

        private bool Bind_OBJ()
        {
            using (var db = new Solution_30shineEntities())
            {
                var ExistOBJ = true;
                int Id;
                if (int.TryParse(_Code, out Id))
                {
                    OBJ = db.Skin_Attribute.Where(w => w.Id == Id).FirstOrDefault();

                    if (!OBJ.Equals(null))
                    {
                        Name.Text = OBJ.SkinTypeName;
                        //Description.Text = OBJ.Description;
                        _Order.Text = Convert.ToString(OBJ.Order);
                        //---------
                        //OnWebId.Text = OBJ.OnWebId.ToString();
                        //---------

                        //if (OBJ.Thumb != null && OBJ.Thumb != "")
                        //{
                        //    var serialize = new JavaScriptSerializer();
                        //    Thumb = serialize.Deserialize<cls_media>(OBJ.Thumb);
                        //}

                        if (OBJ.Status == true)
                        {
                            Publish.Checked = true;
                        }
                        else
                        {
                            Publish.Checked = false;
                        }

                    }
                    else
                    {
                        ExistOBJ = false;
                        var msg = "Lỗi! Danh mục không tồn tại";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                    }
                }
                else
                {
                    ExistOBJ = false;
                    var msg = "Lỗi! Danh mục không tồn tại";
                    var status = "warning";
                    UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                }

                return ExistOBJ;
            }
        }

        public void Bind_Pid()
        {
            using (var db = new Solution_30shineEntities())
            {
                var LST = db.Skin_Attribute.Where(w => w.IsDelete != true).ToList();
                var Key = 0;
                var Count = LST.Count;
                Pid.DataTextField = "CategoryName";
                Pid.DataValueField = "Id";
                ListItem item = new ListItem("Chọn Danh mục", "0");
                Pid.Items.Insert(Key, item);
                Key++;

                if (Count > 0)
                {
                    foreach (var v in LST)
                    {
                        item = new ListItem(v.SkinTypeName, v.Id.ToString());
                        Pid.Items.Insert(Key, item);
                        Key++;
                    }
                }

                if (_IsUpdate)
                {
                    var ItemSelected = Pid.Items.FindByValue(OBJ.ParentId.ToString());
                    if (ItemSelected != null)
                        ItemSelected.Selected = true;
                }
                else
                {
                    Pid.SelectedIndex = 0;
                }
            }
        }

        public void ReloadPage()
        {
            HttpRequest Request = HttpContext.Current.Request;
            string host = "http://" + Request.Url.Host.ToString();
            string rawUrl = Request.RawUrl.ToString();
            string Url = host + rawUrl;
            Response.Redirect(Url);
        }

        private bool IsUpdate()
        {
            _Code = Request.QueryString["Code"];
            if (_Code != null)
            {
                _IsUpdate = true;
                return true;
            }
            else
            {
                _IsUpdate = false;
                return false;
            }
        }

        public class cls_media
        {
            public string url { get; set; }
            public string title { get; set; }
            public string description { get; set; }
        }
    }
}