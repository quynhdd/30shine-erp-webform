﻿using _30shine.Helpers;
using _30shine.MODEL.ENTITY.EDMX;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.SkinAttribute
{
    public partial class Skin_Att_Product_A : System.Web.UI.Page
    {
        protected Skin_Attribute_Product OBJ;
        protected string _Code;
        protected bool _IsUpdate = false;
        public UIHelpers _UIHelpers = new UIHelpers();
        protected cls_media Thumb = new cls_media();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BillProduct();
                if (IsUpdate())
                {
                    if (Bind_OBJ())
                    {
                        Bind_Pid();
                    }
                }
                else
                {
                    Bind_Pid();
                }

            }
        }

        protected void BillProduct()
        {
            using (var db = new Solution_30shineEntities())
            {
                var _Lst = db.Products.Where(a => a.IsDelete == 0 && a.Publish == 1).ToList();

                lsbSanPham.DataTextField = "Name";
                lsbSanPham.DataValueField = "Id";
                lsbSanPham.DataSource = _Lst;
                lsbSanPham.DataBind();

            }
        }

        protected void ExcAddOrUpdate(object sender, EventArgs e)
        {
            if (IsUpdate())
            {
                Update();
            }
            else
            {
                Add();
            }
        }

        private void Add()
        {
            using (var db = new Solution_30shineEntities())
            {
                var obj = new Skin_Attribute_Product();
                obj.Skin_AttributeId = Convert.ToInt32(Pid.SelectedValue);

                string _ProductIds = "";
                foreach (ListItem item in lsbSanPham.Items)
                {
                    if (item.Selected == true)
                    {
                        _ProductIds += item.Value + ",";
                    }
                }
                if (_ProductIds.EndsWith(","))
                    _ProductIds = _ProductIds.Remove(_ProductIds.Length - 1, 1);

                obj.ProductId = _ProductIds;
                obj.IsDelete = false;
                obj.Description = txtMota.Text;
                obj.Title = txtLoiKhuyen.Text;

                // Validate
                var Error = false;

                if (!Error)
                {
                    db.Skin_Attribute_Product.Add(obj);
                    var exc = db.SaveChanges();

                    if (exc > 0)
                    {
                        var MsgParam = new List<KeyValuePair<string, string>>();
                        MsgParam.Add(new KeyValuePair<string, string>("msg_add_status", "success"));
                        MsgParam.Add(new KeyValuePair<string, string>("msg_add_message", "Cập nhật thành công!"));
                        UIHelpers.Redirect("/admin/to-hop-da/danh-sach.html", MsgParam);
                    }
                    else
                    {
                        var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển.";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                    }
                }
            }
        }

        private void Update()
        {
            using (var db = new Solution_30shineEntities())
            {
                int Id;
                //int integer;
                if (int.TryParse(_Code, out Id))
                {
                    OBJ = db.Skin_Attribute_Product.FirstOrDefault(w => w.Id == Id);

                    if (!OBJ.Equals(null))
                    {

                        OBJ.Skin_AttributeId = Convert.ToInt32(Pid.SelectedValue);

                        string _ProductIds = "";
                        foreach (ListItem item in lsbSanPham.Items)
                        {
                            if (item.Selected == true)
                            {
                                _ProductIds += item.Value + ",";
                            }
                        }
                        if (_ProductIds.EndsWith(","))
                            _ProductIds = _ProductIds.Remove(_ProductIds.Length - 1, 1);
                        OBJ.ProductId = _ProductIds;
                        OBJ.IsDelete = false;
                        OBJ.Description = txtMota.Text;
                        OBJ.Title = txtLoiKhuyen.Text;

                        db.Skin_Attribute_Product.AddOrUpdate(OBJ);
                        var exc = db.SaveChanges();
                        if (exc > 0)
                        {
                            var MsgParam = new List<KeyValuePair<string, string>>();
                            MsgParam.Add(new KeyValuePair<string, string>("msg_add_status", "success"));
                            MsgParam.Add(new KeyValuePair<string, string>("msg_add_message", "Cập nhật thành công!"));
                            UIHelpers.Redirect("/admin/to-hop-da/" + OBJ.Id + ".html", MsgParam);
                        }
                        else
                        {
                            var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển!";
                            var status = "warning";
                            UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                        }
                    }
                    else
                    {
                        var msg = "Lỗi! Không tìm thấy danh mục.";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);

                    }
                }
                else
                {
                    MsgSystem.Text = "Lỗi! Không tìm thấy danh mục.";
                    MsgSystem.CssClass = "msg-system warning";
                }

                Context.RewritePath("+", "", "");
            }
        }

        private bool Bind_OBJ()
        {
            using (var db = new Solution_30shineEntities())
            {
                var ExistOBJ = true;
                int Id;
                if (int.TryParse(_Code, out Id))
                {
                    OBJ = db.Skin_Attribute_Product.Where(w => w.Id == Id).FirstOrDefault();

                    if (!OBJ.Equals(null))
                    {

                        //Set active product
                        string _ProductIds = OBJ.ProductId;
                        if (_ProductIds != "")
                        {

                            string[] a = _ProductIds.Split(',');

                            for (int i = 0; i < a.Length; i++)
                            {
                                if (a[i].ToString() != "" && a[i].ToString() != "")
                                    lsbSanPham.Items.FindByValue(a[i].ToString()).Selected = true;
                            }
                        }

                        txtMota.Text = OBJ.Description;
                        txtLoiKhuyen.Text = OBJ.Title;

                    }
                    else
                    {
                        ExistOBJ = false;
                        var msg = "Lỗi! Danh mục không tồn tại";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                    }
                }
                else
                {
                    ExistOBJ = false;
                    var msg = "Lỗi! Danh mục không tồn tại";
                    var status = "warning";
                    UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                }

                return ExistOBJ;
            }
        }

        public void Bind_Pid()
        {
            using (var db = new Solution_30shineEntities())
            {
                var LST = db.Skin_Attribute.Where(w => w.IsDelete != true && w.ParentId == 0).ToList();
                var Key = 0;
                var Count = LST.Count;
                Pid.DataTextField = "CategoryName";
                Pid.DataValueField = "Id";
                ListItem item = new ListItem("Chọn loại thuộc tính", "0");
                Pid.Items.Insert(Key, item);
                Key++;

                if (Count > 0)
                {
                    foreach (var v in LST)
                    {
                        item = new ListItem(v.SkinTypeName, v.Id.ToString());
                        Pid.Items.Insert(Key, item);
                        Key++;
                    }
                }

                if (_IsUpdate)
                {
                    int _Skin_Att_Id = Convert.ToInt32(OBJ.Skin_AttributeId);
                    var ItemSelected = Pid.Items.FindByValue(_Skin_Att_Id.ToString());
                    if (ItemSelected != null)
                        ItemSelected.Selected = true;
                }
                else
                {
                    Pid.SelectedIndex = 0;
                }
            }
        }

        public void ReloadPage()
        {
            HttpRequest Request = HttpContext.Current.Request;
            string host = "http://" + Request.Url.Host.ToString();
            string rawUrl = Request.RawUrl.ToString();
            string Url = host + rawUrl;
            Response.Redirect(Url);
        }

        private bool IsUpdate()
        {
            _Code = Request.QueryString["Code"];
            if (_Code != null)
            {
                _IsUpdate = true;
                return true;
            }
            else
            {
                _IsUpdate = false;
                return false;
            }
        }

        public class cls_media
        {
            public string url { get; set; }
            public string title { get; set; }
            public string description { get; set; }
        }
    }
}