﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Skin_Att_Product_L.aspx.cs" Inherits="_30shine.GUI.BackEnd.SkinAttribute.Skin_Att_Product_L" %>


<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý tổ hợp da &nbsp;&#187; </li>
                        <li class="li-listing"><a href="/admin/loai-da/danh-sach.html">DS thuộc tính da</a></li>
                        <li class="li-add"><a href="/admin/loai-da/them-moi.html">Thêm mới thuộc tính da</a></li>
                        <li class="li-listing active"><a href="/admin/to-hop-da/danh-sach.html">Danh sách tổ hợp da</a></li>
                        <li class="li-add"><a href="/admin/to-hop-da/them-moi.html">Thêm mới tổ hợp da</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <!-- Row Table Filter -->
                <div class="table-func-panel">
                    <div class="table-func-elm">
                        <span>Số hàng / Page : </span>
                        <div class="table-func-input-wp">
                            <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                            <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                            <ul class="ul-opt-segment">
                                <li data-value="10">10</li>
                                <li data-value="20">20</li>
                                <li data-value="30">30</li>
                                <li data-value="40">40</li>
                                <li data-value="50">50</li>
                                <li data-value="1000000">Tất cả</li>
                            </ul>
                            <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                        </div>
                    </div>
                </div>
                <!-- End Row Table Filter -->
                <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
                <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                    <ContentTemplate>
                        <div class="table-wp">
                            <table class="table-add table-listing">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>ID</th>
                                        <th>Loại tóc</th>
                                        <th>Sản phẩm phù hợp</th>
                                        <th>Phân loại</th>
                                        <th>Mô tả</th>
                                        <th>Publish</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="Rpt" runat="server" OnItemDataBound="Repeater_ItemDataBound">
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# Container.ItemIndex + 1 + PAGING._Offset %></td>
                                                <td><%# Eval("Id") %></td>
                                                <td>
                                                    <a href="/admin/to-hop-da/<%# Eval("Id") %>.html"><%# Eval("Pid_Name") %></a>
                                                </td>                                                                                <td><%# ReturnProductName(Eval("ProductId").ToString()) %></td>
                                                <td><%# Eval("Title") %></td>
                                                <td><%# Eval("Description") %></td>
                                                <td class="map-edit">
                                                    <div class="edit-wp">
                                                        <asp:Panel CssClass="edit-action-wp action-edit" runat="server" ID="EAedit" Visible="false">
                                                    <a class="elm edit-btn" href="/admin/to-hop-da/<%# Eval("Id") %>.html" title="Sửa" target="_blank"></a>
                                                </asp:Panel>
                                                <asp:Panel CssClass="edit-action-wp action-delete" runat="server" ID="EAdelete" Visible="false">
                                                    <a class="elm del-btn" onclick="del(this.parentNode.parentNode.parentNode.parentNode,'<%# Eval("Id") %>', '<%# Eval("Pid_Name") %>')" href="javascript://" title="Xóa"></a></asp:Panel>
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>                    
                                </tbody>
                            </table>
                        </div>

                        <!-- Paging -->
                        <div class="site-paging-wp">
                            <% if (PAGING.TotalPage > 1)
                            { %>
                            <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                                <% if (PAGING._Paging.Prev != 0)
                                    { %>
                                <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                                <% } %>
                                <asp:Repeater ID="RptPaging" runat="server">
                                    <ItemTemplate>
                                        <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                            <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>>
                                            <%# Eval("PageNum") %>
                                        </a>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                                    { %>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                                <% } %>
                            </asp:Panel>
                            <% } %>
                        </div>
                        <!-- End Paging -->
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
            </div>
            <%-- END Listing --%>
        </div>

        <script>
            jQuery(document).ready(function () {
                $("#glbAdminContent").addClass("active");
                $("#glbAdminSkinAtt").addClass("active");
                //$("#subMenu .li-listing").addClass("active");
            });

            //============================
            // Event delete
            //============================
            function del(This, code, name) {
                var code = code || null,
                    name = name || null,
                    Row = This;
                if (!code) return false;

                // show EBPopup
                $(".confirm-yn").openEBPopup();
                $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn xóa [ " + name + " ] ?");

                $("#EBPopup .yn-yes").bind("click", function () {
                    $.ajax({
                        type: "POST",
                        url: "/GUI/SystemService/Ajax/Del.aspx/Delele_SkinAtt_Product",
                        data: '{Code : "' + code + '"}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json", success: function (response) {
                            var mission = JSON.parse(response.d);
                            if (mission.success) {
                                delSuccess();
                                Row.remove();
                            } else {
                                delFailed();
                            }
                        },
                        failure: function (response) { alert(response.d); }
                    });
                });
                $("#EBPopup .yn-no").bind("click", function () {
                    autoCloseEBPopup(0);
                });
            }

            //============================
            // Event publish, featured
            //============================
            function checkFeatured(This, table) {
                var Id = This.attr("data-id");
                var isChecked = This.is(":checked");
                $.ajax({
                    type: "POST",
                    url: "/GUI/SystemService/Ajax/Publish_Featured.aspx/Featured",
                    data: '{Id : "' + Id + '", Table : "' + table + '", isChecked : "' + isChecked + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        var mission = JSON.parse(response.d);
                        if (mission.success) {
                            alert("Chuyển trạng thái thành công.");
                        } else {
                        }
                    },
                    failure: function (response) { alert(response.d); }
                });
            }

            function checkPublish(This, table) {
                var Id = This.attr("data-id");
                var isChecked = This.is(":checked");
                $.ajax({
                    type: "POST",
                    url: "/GUI/SystemService/Ajax/Publish_Featured.aspx/Publish",
                    data: '{Id : "' + Id + '", Table : "' + table + '", isChecked : "' + isChecked + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        var mission = JSON.parse(response.d);
                        if (mission.success) {
                            alert("Chuyển trạng thái thành công.");
                        } else {
                        }
                    },
                    failure: function (response) { alert(response.d); }
                });
            }
        </script>

    </asp:Panel>
</asp:Content>