﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Skin_Att_Product_A.aspx.cs" Inherits="_30shine.GUI.BackEnd.SkinAttribute.Skin_Att_Product_A" %>



<asp:Content ID="ProductAdd" ContentPlaceHolderID="CtMain" runat="server">
<asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
<asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
<div class="wp sub-menu">
    <div class="wp960">
        <div class="wp content-wp">
            <ul class="ul-sub-menu" id="subMenu">
                <li>Quản lý tổ hợp da &nbsp;&#187; </li>
                <li class="li-listing"><a href="/admin/loai-da/danh-sach.html">DS thuộc tính da</a></li>
                <li class="li-add"><a href="/admin/loai-da/them-moi.html">Thêm mới thuộc tính da</a></li>
                <li class="li-listing"><a href="/admin/to-hop-da/danh-sach.html">Danh sách tổ hợp da</a></li>
                <li class="li-add active"><a href="/admin/to-hop-da/them-moi.html">Thêm mới tổ hợp da</a></li>
            </ul>
        </div>
    </div>
</div>
    
<div class="wp customer-add admin-product-add">
    <%-- Add --%>                
    <div class="wp960 content-wp">
        <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
        <div class="table-wp">
            
            <table class="table-add admin-product-table-add">
                <tbody>
                    <tr class="title-head">
                        <td><strong>Thông tin tổ hợp da</strong></td>
                        <td></td>
                    </tr>
                    <tr class="tr-margin" style="height: 20px;"></tr>

                    <tr class="tr-field-ahalf">
                        <td class="col-xs-3 left"><span>Loại da</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                
                                <asp:DropDownList ID="Pid" runat="server" CssClass="select" ClientIDMode="Static" AutoPostBack="true"></asp:DropDownList>
                            </span>   
                          <asp:RequiredFieldValidator InitialValue="0" ID="Req_ID" Display="Dynamic" runat="server" ControlToValidate="Pid" Text="Chọn loại thuộc tính (*)" ErrorMessage="ErrorMessage" CssClass="error-msg"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="col-xs-3 left"><span>Sản phẩm</span></td>
                        <td class="col-xs-9 right">
                            <div>
                                <asp:ListBox ID="lsbSanPham" runat="server" SelectionMode="Multiple" Width="290" data-placeholder="Chọn sản phẩm" class="chosen-select"></asp:ListBox>
                            </div>
                        </td>
                    </tr>

                    <tr class="tr-field-ahalf">
                        <td class="col-xs-3 left"><span>Phân loại</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:TextBox ID="txtLoiKhuyen" runat="server" ClientIDMode="Static" placeholder="Bạn có da..."></asp:TextBox>
                            </span>
                        </td>
                    </tr>

                    <tr class="tr-field-ahalf">
                        <td class="col-xs-3 left"><span>Mô tả</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:TextBox ID="txtMota" runat="server" ClientIDMode="Static" TextMode="MultiLine"
                                    placeholder=""></asp:TextBox>
                            </span>
                        </td>
                    </tr>

                    


                    
                    <tr class="tr-send">
                        <td class="col-xs-3 left"></td>
                        <td class="col-xs-9 right no-border">
                            <span class="field-wp">
                                <asp:Button ID="Send" CssClass="btn-send" runat="server" Text="Hoàn tất" ClientIDMode="Static" OnClick="ExcAddOrUpdate" OnClientClick="initImgStore('HDF_Images')"></asp:Button>
                                <%--<span class="btn-send btn-cancel" id="CalcelAddFbcv" style="margin-left: 12px;" >Hủy</span>--%>
                            </span>
                        </td>
                    </tr>
                    <asp:HiddenField runat="server" ID="UrlKeyField"  ClientIDMode="Static"/>
                    <asp:HiddenField runat="server" ID="ImgCoverField"  ClientIDMode="Static"/>
                    <asp:HiddenField runat="server" ID="HDF_Images"  ClientIDMode="Static"/>
                </tbody>
            </table>
        </div>
    </div>
    <%-- end Add --%>
</div>

<script>
    jQuery(document).ready(function () {
        $("#glbAdminContent").addClass("active");
        $("#glbAdminSkinAtt").addClass("active");

        var test = "http://solution.30shine.net/Public/Media/Upload/Images/Common/2016/7/22/1-iuTivF.png";
        console.log(executeThumPath(test));
    });
</script>

<!-- Popup plugin image -->
<script type="text/javascript">
    function popupImageIframe(StoreImgField) {
        var Width = 960;
        var Height = 560;
        var ImgList = [];
        var iframe = $("<div class='iframe-image-upload' style='width:" + Width + "px; height:" + Height + "px; float:left; display:none;'><iframe src='/GUI/BackEnd/UploadImage/Upload_V2.aspx?Code=img&StoreImgField=" + StoreImgField + "&callback=getThumbFromIframe&imgWidth=350&imgHeight=350&multi_img=false&Folder=Common' style='width:100%; height: 100%;'></iframe></div");

        $("body").find(".iframe-image-upload").remove().end().append(iframe).find(".iframe-image-upload").openEBPopup();
    }

    window.getThumbFromIframe = function (Imgs, StoreImgField) {
        var imgs = "";
        var thumb = "";
        if (Imgs.length > 0) {
            for (var i = 0; i < Imgs.length; i++) {
                thumb += '<div class="thumb-wp">' +
                            '<div class="left">' +
                                    '<img class="thumb" alt="" title="" src="' + Imgs[i] + '" data-img=""/>' +
                            '</div>' +
                            '<div class="right">' +
                                '<input class="thumb-title" type="text" placeholder="Tiêu đề ảnh" />' +
                                '<textarea class="thumb-des" placeholder="Mô tả"></textarea>' +
                                '<div class="action">        ' +
                                    '<div class="action-item action-add" onclick="popupImageIframe(\'HDF_Images\')"><i class="fa fa-plus-square" aria-hidden="true"></i>Thêm ảnh</div>' +
                                    '<div class="action-item action-delete" onclick="deleteThumbnail($(this), \'' + StoreImgField + '\')"><i class="fa fa-trash-o" aria-hidden="true"></i>Xóa</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>';
            }
            $("." + StoreImgField).find(".listing-img-upload").append($(thumb));
            excThumbWidth(120, 120);
        }
        autoCloseEBPopup();
    }

    function imgLink(StoreImgField) {
        var imgs = [], img, src, title, description;
        $("." + StoreImgField).find(".thumb-wp").each(function () {
            src = $(this).find("img.thumb").attr("src");
            title = $(this).find("input.thumb-title").val();
            description = $(this).find("textarea.thumb-des").val();
            img = { url: src, thumb: executeThumPath(src), title: title, description: description };
        });
        return img;
    }

    function initImgStore(StoreImgField) {
        $("#" + StoreImgField).val(JSON.stringify(imgLink(StoreImgField)));
    }

    function deleteThumbnail(This, StoreImgField) {
        This.parent().parent().parent().remove();
    }

    function executeThumPath(src) {
        var str = "";
        var fileExtension = "";
        var strLeft = "";
        var strPush = "x350";
        if (src != null && src != "") {
            var loop = src.length - 1;
            for (var i = loop; i >= 0; i--) {
                if (src[i] == ".") {
                    str = src.substring(0, i) + strPush + "." + fileExtension;
                    break;
                } else {
                    fileExtension = src[i] + fileExtension;
                }
            }
        }
        return str;
    }
</script>
<!-- Popup plugin image -->

    <link href="/Assets/css/chosen.css" rel="stylesheet" />
    <script src="/Assets/js/chosen.jquery.js"></script>
    <script type="text/javascript">
        var config = {
            '.chosen-select': {},
            '.chosen-select-deselect': { allow_single_deselect: true },
            '.chosen-select-no-single': { disable_search_threshold: 10 },
            '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' },
            '.chosen-select-width': { width: "95%" }
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }
  </script>
    <script>
        function ResetSelect() {
            var config = {
                '.chosen-select': {},
                '.chosen-select-deselect': { allow_single_deselect: true },
                '.chosen-select-no-single': { disable_search_threshold: 10 },
                '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' },
                '.chosen-select-width': { width: "95%" }
            }
            for (var selector in config) {
                $(selector).chosen(config[selector]);
            }
        }
    </script>
    <style>
        .customer-add .table-add .search-choice span { height: auto; line-height: 14px;}
        .chosen-container-multi .chosen-choices li.search-field input[type="text"] { font-family: Roboto Condensed Regular;}
        .error-msg { color: red;}
    </style>

</asp:Panel>
</asp:Content>
