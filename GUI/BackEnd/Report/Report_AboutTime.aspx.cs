﻿using _30shine.Helpers;
using _30shine.Helpers.Http;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.Interface;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.Report
{
    public partial class Report_AboutTime : System.Web.UI.Page
    {

        protected Paging PAGING = new Paging();
        public CultureInfo culture = new CultureInfo("vi-VN");
        private string PageID = "_30shine.GUI.BackEnd.Report.Report_AboutTime";
        private bool Perm_ViewAllData = false;
        private bool Perm_Access = false;
        protected bool Perm_ViewAllDataByDate = false;
        protected bool Perm_ViewAllDataBySalon = false;
        protected bool isRoot = false;
        protected string FirstDayOfMonth = string.Format("{0:dd/MM/yyyy}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1));
        protected string Today = String.Format("{0:dd/MM/yyyy}", DateTime.Now);

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                Library.Function.bindSalon_NotHoiQuan(new List<DropDownList> { ddlSalon }, Perm_ViewAllData);
                Library.Function.Bind_Department(new List<DropDownList> { ddlDepartment });
                txtStartDate.Text = FirstDayOfMonth;
                txtEndDate.Text = Today;
                RemoveLoading();
            }
        }

        protected void SetPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_ViewAllDataByDate = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllDataByDate", PageID, permission);
                //Perm_ViewAllDataBySalon = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllDataBySalon", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllDataByDate = permissionModel.CheckPermisionByAction("Perm_ViewAllDataByDate", pageId, staffId);
                Perm_ViewAllDataBySalon = permissionModel.CheckPermisionByAction("Perm_ViewAllDataBySalon", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        /// <summary>
        /// Get data
        /// </summary>
        private void BindData()
        {
            try
            {
                int integer;
                var filter = new cls_Search();
                filter.salonId = int.TryParse(ddlSalon.SelectedValue, out integer) ? integer : 0;
                filter.departmentId = int.TryParse(ddlDepartment.SelectedValue, out integer) ? integer : 0;
                filter.fromDate = String.Format("{0:yyyy/MM/dd}", Convert.ToDateTime(txtStartDate.Text,new CultureInfo("vi-VN")));
                filter.toDate = String.Format("{0:yyyy/MM/dd}", Convert.ToDateTime(txtEndDate.Text, new CultureInfo("vi-VN")));
                var request = new Request();
                var response = request.RunGetAsyncV1(Libraries.AppConstants.URL_API_CHECKIN + "/api/staff/report-about-time?fromDate=" + filter.fromDate + "&toDate=" + filter.toDate + "&salonId=" + filter.salonId + "&departmentId=" + filter.departmentId + "").Result;
                if (response.IsSuccessStatusCode)
                {
                    var data = response.Content.ReadAsAsync<ResponseData>().Result.data.ToList();
                    if (data != null)
                    {
                        Bind_Paging(data.Count);
                        rptListTimekeeping.DataSource = data.Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                        rptListTimekeeping.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                rptListTimekeeping.DataSource = "";
                rptListTimekeeping.DataBind();
            }
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            BindData();
            RemoveLoading();
        }
        protected void Bind_Paging(int TotalRow)
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(TotalRow) : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();
            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage(int TotalRow)
        {
            TotalRow -= PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        }

        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        /// <summary>
        /// ResponseData
        /// </summary>
        public class ResponseData
        {
            public int status { get; set; }
            public string message { get; set; }
            public List<Origin> data { get; set; }
            public ResponseData()
            {
                data = null;
                status = 0;
                message = "";
            }
        }

        /// <summary>
        /// origin
        /// </summary>
        public class Origin
        {
            public string staffName { get; set; }
            public int staffId { get; set; }
            public string department { get; set; }
            public int totalKeeping { get; set; }
            public int totalInTime { get; set; }
            public int totalComeLater { get; set; }
            public int totalLeaveEarly { get; set; }
            public int totalComeLaterLeaveEarly { get; set; }
            public int totalConfirmed { get; set; }
            public int totalUnConfirm { get; set; }
        }

        /// <summary>
        /// cls 
        /// </summary>
        public class cls_Search
        {
            public string fromDate { get; set; }
            public string toDate { get; set; }
            public int salonId { get; set; }
            public int departmentId { get; set; }
        }
    }
}