﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.CustomClass;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;

namespace _30shine.GUI.BackEnd.Report
{
    public partial class Report_Sales_V2 : System.Web.UI.Page
    {
        private string PageID = "BC_VH_V2";
        CultureInfo culture = new CultureInfo("vi-VN");
        IReport_Sale_V2 sale_V2 = new Report_Sales_V2Model();
        protected bool Perm_ViewAllData = false;
        protected bool Perm_ViewAllDate = false;
        private bool Perm_Access = false;
        protected bool IsAccountant = false;
        private string sql = "";

        protected string Day1 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-2));
        protected string Day2 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-1));
        protected string Day3 = string.Format("{0:dd/MM/yyyy}", DateTime.Now);

        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_ViewAllDate = true;
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        //public class houir
        //{
        //    public int id { get; set; }
        //}

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                TxtDateTimeFrom.Text = String.Format("{0:dd/MM/yyyy}", DateTime.Now);
                var staffId = Convert.ToInt32(Session["User_Id"].ToString());
                Library.Function.bindSalon_NotHoiQuan(new List<DropDownList> { Salon }, Perm_ViewAllData);
            }
            else
            {
                RemoveLoading();
            }
        }



        //protected void GetDataToday(object sender, EventArgs e)
        //{
        //    var dateNow = DateTime.Now.Date;
        //    var fromDate = Convert.ToDateTime(dateNow, culture).AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
        //    var toDate = Convert.ToDateTime(dateNow, culture).AddDays(1).AddHours(00).AddMinutes(00).AddSeconds(00).AddMilliseconds(000);
        //    var data = sale_V2.GetDataToday_BCVH_v2(Convert.ToInt32(Salon.SelectedValue), fromDate, toDate).OrderBy(s => s.SalonId).ToList();
        //    rptData.DataSource = data;
        //    rptData.DataBind();
        //    RemoveLoading();
        //}
        //protected void GetData()
        //{
        //    if (TxtDateTimeFrom.Text != "")
        //    {
        //        DateTime _FromDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture).Date;
        //        DateTime _ToDate;
        //        if (TxtDateTimeTo.Text != "")
        //        {
        //            _ToDate = Convert.ToDateTime(TxtDateTimeTo.Text, culture).AddDays(1).Date;
        //        }
        //        else
        //        {
        //            _ToDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture).AddDays(1).Date;
        //        }
        //        int SalonId = Convert.ToInt32(Salon.SelectedValue);
        //        var lsData = sale_V2.BCVH(_FromDate, _ToDate, SalonId).OrderBy(s => s.SalonId).ToList();
        //        rptData.DataSource = lsData;
        //        rptData.DataBind();
        //    }
        //}

        protected void _BtnClick(Object sender, EventArgs e)
        {
            //GetData();
            RemoveLoading();
        }
        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

    }
}