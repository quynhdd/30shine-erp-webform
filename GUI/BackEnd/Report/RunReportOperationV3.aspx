﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/TemplateMaster/SiteMasterVersion1.Master" CodeBehind="RunReportOperationV3.aspx.cs" Inherits="_30shine.GUI.BackEnd.Report.RunReportOperationV3" %>

<asp:Content ID="Content2" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel ID="ContentWrap" runat="server" ClientIDMode="Static">
        <form runat="server">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center mt-5 form-group">
                        <h2>Chạy lại báo cáo vận hành</h2>
                    </div>
                    <div class="col-12 text-center d-flex justify-content-center">
                        <input type="text" placeholder="Từ ngày" name="fromDate" class="datetime-picker form-control float-left w-auto" />
                        <i class="fa fa-arrow-circle-right float-left mt-2 mr-2 ml-2" style="color: #D0D6D8;"></i>
                        <input type="text" placeholder="Đến ngày" name="toDate" class="datetime-picker form-control float-left w-auto mr-4" />
                        <input type="button" value="Chạy BCVH" class="btn btn-success" onclick="RunBCVH()" style="width: 120px;" />
                    </div>
                    
                </div>
            </div>
        </form>
        <script>
            var domain = '<%=Libraries.AppConstants.URL_API_REPORTS%>';
            function RunBCVH() {
                 startLoading();
                ///api/operation-report-v3/statistic-rerun;
                var fromDate = $('input[name=fromDate]').val();
                var toDate = $('input[name=toDate]').val();
                if (fromDate == "" || toDate == "") {
                    ShowMessage("","Vui lòng chọn ngày",4,7000);
                    return;
                }
                var querry = "?from=" + encodeURIComponent(fromDate) + "&to=" + encodeURIComponent(toDate);
                $.ajax({
                    type: "POST",
                    //async: false,
                    url: domain + "/api/operation-report-v3/statistic-rerun" + querry,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data, textStatus, jqXHR) {
                       ShowMessage("","Thành công!",2);
                        finishLoading();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        if (jqXHR.status === 400) {
                            return;
                        }
                        if (jqXHR.status != 200) {
                            let strError = 'Đã có lỗi xảy ra vui lòng liên hệ nhóm phát triển';
                            let message = "";
                            if (typeof jqXHR.responseJSON != "undefined" && typeof jqXHR.responseJSON.Message != "undefined") {
                                message = jqXHR.responseJSON.Message
                            }
                            else {
                                message = jqXHR.responseText;
                                message = message == "" ? jqXHR.statusText : message;
                            }
                            ShowMessage("Cảnh báo", strError + " <br>- status:" + jqXHR.status + ',' + textStatus + '<br>- message: ' + message + '<br>- ' + errorThrown, 3, 7000);
                        }
                        finishLoading();
                    }
                });
            }
        </script>
    </asp:Panel>
</asp:Content>

