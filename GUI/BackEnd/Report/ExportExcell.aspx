﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeBehind="ExportExcell.aspx.cs" MasterPageFile="~/TemplateMaster/SiteMaster.Master" Inherits="_30shine.GUI.BackEnd.Report.ExportExcell" %>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">

    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>

    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <%--<uc1:ReportMenu runat="server" id="ReportMenu" />--%>
        <link href="/Assets/js/select2/select2.min.css" rel="stylesheet" />
        <style>
            .table-wp table th { font-family: Roboto Condensed Bold; font-weight: normal; }
        </style>

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Báo cáo &nbsp;&#187; </li>
                        <li class="be-report-li"><a href="javascript:void(0);"><i class="fa fa-th-large"></i>Xuất excel báo cáo</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing be-report">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <!-- Filter -->
                <div class="row">
                    <div class="filter-item" style="margin-left: 0;">
                        <%--<strong class="st-head" style="margin-right: 10px;"><i class="fa fa-clock-o"></i>Thời gian</strong>
                        <div class="datepicker-wp">
                            <asp:TextBox CssClass="txtDateTime st-head" ID="TxtDateTimeFrom" placeholder="Từ ngày"
                                ClientIDMode="Static" runat="server"></asp:TextBox>

                            <strong class="st-head" style="margin-left: 10px; margin-right: 10px;">
                                <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                            </strong>
                            <asp:TextBox CssClass="txtDateTime st-head" ID="TxtDateTimeTo" placeholder="Đến ngày"
                                ClientIDMode="Static" runat="server"></asp:TextBox>
                        </div>
                        <div class="filter-item" style="margin-top: 6px;">
                            <asp:DropDownList ID="Salon"  CssClass="select form-control" runat="server" ClientIDMode="Static" Style="width: 190px;"></asp:DropDownList>
                        </div>--%>
                    </div>
                    
                </div>
                
                <div class="row">
                    <strong class="st-head"><i class="fa fa-file-text"></i>EXPORT EXCELL THEO YÊU CẦU</strong>
                </div>
                <br />
                <asp:Panel ID="PanelAdmin" runat="server">
                    <div class="row form-group">
                        <div class="col-xs-2">File Upload: </div>
                        <div class="col-xs-9">
                            <asp:FileUpload ID="FileUpload" runat="server" AllowMultiple="true" CssClass="form-control" Width="30%"/>
                            <%--<input type="file" id="FileUpload" class="form-control" style="width:30%;margin-left:10px" />--%>
                            <asp:Button Text="Upload" style="margin-top:20px;margin-left:10px;" ClientIDMode="Static"   CssClass="btn btn-send" OnClick="UploadFile" ID="btn_Upload" runat="server"/><br>
                            <asp:Label ID="lblMessage" ForeColor="Green" runat="server" />
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="PanelUser" runat="server">
                    <div class="row form-group">
                        <div class="col-xs-2">Chọn : </div>
                        <div class="col-xs-9">
                            <asp:DropDownList ID="Script" CssClass="select form-control" runat="server" ClientIDMode="Static" Style="width: 30%;"></asp:DropDownList>
                            <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" style="margin-left:20px;" OnClick="ReadFile" CssClass="btn btn-send" Text="Đọc file"/>
                        </div>
                    </div>
                </asp:Panel>
                <asp:TextBox id="FileName" style="width:100%;margin:20px auto !important" CssClass="form-control" runat="server" placeholder ="Tên file"/>
                <asp:TextBox id="SqlQuery" TextMode="multiline" style="margin-top:20px !important" Columns="50" Rows="20" CssClass="form-control" runat="server" />
                <asp:Button ID="ButtonExport" ClientIDMode="Static" runat="server" style="margin-top:20px;" OnClick="_BtnClick" CssClass="btn btn-send" Text="Xuất Excell"/>
                <asp:Label ID="Label1" ForeColor="Green" runat="server" />
               <%-- <div class="row form-group">
                    <div class="col-xs-1">Tên file: </div>
                    <div class="col-xs-4">
                        <asp:TextBox id="FileName" style="width:100%;margin:0 !important" CssClass="form-control" runat="server" />
                        <asp:Label ID="Label1" runat="server" Visible="false"></asp:Label>
                    </div>
                </div>
                <div class="row form-group">
                    
                    <div class="col-xs-1">SQL Query: </div>
                    <div class="col-xs-4">
                        <asp:TextBox id="SqlQuery" TextMode="multiline" Columns="50" Rows="20" CssClass="form-control" runat="server" />
                        <asp:Panel ID="ExportPanel" runat="server">
                        
                        </asp:Panel>
                    </div>
                </div>--%>
            </div>
        </div>
        
        <script src="/Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <script type="text/javascript">

            $("#CtMain_SqlQuery").keyup(function () {
                if ($(this).val().length === 0) {
                     $("#CtMain_SqlQuery").attr("disabled", "disabled");
                };
            });
            jQuery(document).ready(function () {

                if ($("#CtMain_SqlQuery").val() == null || $("#CtMain_SqlQuery").val() == '') {
                    $("#CtMain_SqlQuery").attr("disabled", "disabled");
                }
                // Add active menu
                $("#glbAdminSales").addClass("active");
                $("#glbAdminKetQuaKinhDoanh").addClass("active");
                $("li.be-report-li").addClass("active");
                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) {
                        //$input.val($input.val().split(' ')[0]);
                    }
                });

                // View data today
                //$(".tag-date-today").click();
            });
            function viewDataByDate(This, time) {
                $(".tag-date.active").removeClass("active");
                This.addClass("active");
                $("#TxtDateTimeTo").val("");
                $("#TxtDateTimeFrom").val(time);
                $("#ViewData").click();
            }
          
        </script>

    </asp:Panel>
</asp:Content>
