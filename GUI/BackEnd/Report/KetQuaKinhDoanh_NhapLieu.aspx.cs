﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Globalization;
using System.Web.Script.Serialization;
using ExportToExcel;
using Excel;
using LinqKit;
using System.Web.Services;
using System.Data.Entity.Migrations;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.Report
{
    public partial class KetQuaKinhDoanh_NhapLieu : System.Web.UI.Page
    {
        private Solution_30shineEntities db;
        protected Paging PAGING = new Paging();
        protected SingleReportData _SingleReportData = new SingleReportData();
        protected SingleReportData _SingleReportData_CL = new SingleReportData();
        protected SingleReportData _SingleReportData_KT = new SingleReportData();
        protected SingleReportData _SingleReportData_TDN = new SingleReportData();
        protected List<List<ProductBasic>> ProductList;
        private Expression<Func<BillService, bool>> Where = PredicateBuilder.True<BillService>();
        CultureInfo culture = new CultureInfo("vi-VN");

        private string PageID = "BC_KQKD_NL";
        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        protected bool Perm_ViewAllData = false;
        protected bool Perm_ViewAllDate = false;
        private bool Perm_Access = false;
        protected bool IsAccountant = false;
        List<_30shine.MODEL.ENTITY.EDMX.Staff> _LstNhanVien;

        protected string Day1 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-2));
        protected string Day2 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-1));
        protected string Day3 = string.Format("{0:dd/MM/yyyy}", DateTime.Now);

        protected string table = "";

        protected List<KetQuaKinhDoanh_ItemImport> listItem;
        protected List<cls_ketquakinhdoanh_nhaplieu> listItemNhapLieu;
        protected string textNote;
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //Perm_ViewAllDate = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllDate", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_ViewAllDate = permissionModel.CheckPermisionByAction("Perm_ViewAllDate", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }

        }
        public KetQuaKinhDoanh_NhapLieu()
        {
            db = new Solution_30shineEntities();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (!IsPostBack)
            {
                TxtImportTime.Text = String.Format("{0:dd/MM/yyyy}", DateTime.Now);
                listItem = getData_ItemImport();
                listItemNhapLieu = getData_NhapLieu();
            }
            else
            {
                RemoveLoading();
            }
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            listItem = getData_ItemImport();
            listItemNhapLieu = getData_NhapLieu();
        }        
        
        /// <summary>
        /// Lấy danh sách Item nhập liệu
        /// </summary>
        /// <returns></returns>
        private List<cls_ketquakinhdoanh_nhaplieu> getData_NhapLieu()
        {
            var data = new List<cls_ketquakinhdoanh_nhaplieu>();
            var item = new cls_ketquakinhdoanh_nhaplieu();
            DateTime importTime;

            try
            {
                if (TxtImportTime.Text != "")
                {
                    importTime = Convert.ToDateTime(TxtImportTime.Text, culture);
                    var listSalon = db.KetQuaKinhDoanh_Salon.Where(w => w.IsDelete != true && w.Publish == true).OrderBy(w=>w.Order).ToList();
                    if (listSalon.Count > 0)
                    {
                        foreach (var v in listSalon)
                        {
                            item = new cls_ketquakinhdoanh_nhaplieu();
                            item.SalonId = v.Id;
                            item.SalonName = v.SalonName;
                            item.NhapLieu = getData_NhapLieuBySalonId(String.Format("{0:yyyy/MM/dd}", importTime), v.Id);
                            data.Add(item);
                        }
                    }

                    // Lấy dữ liệu ghi chú
                    textNote = getData_NoteByDate(importTime);
                }             
            }
            catch { }

            return data;          
        }

        /// <summary>
        /// Lấy dữ liệu ghi chú
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        private string getData_NoteByDate(DateTime date)
        {
            var note = db.KetQuaKinhDoanh_FlowImport.FirstOrDefault(w => w.IsDelete != true && w.ImportDate == date && w.KQKDSalonId == null && w.KQKDItemId == null);
            if (note != null)
            {
                return note.Note;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Lấy dữ liệu item theo Salon
        /// </summary>
        /// <param name="importTime"></param>
        /// <param name="salonId"></param>
        /// <returns></returns>
        private List<Store_KetQuaKinhDoanh_NhapLieu_Result> getData_NhapLieuBySalonId(string importTime, int salonId)
        {
            return db.Store_KetQuaKinhDoanh_NhapLieu(importTime, salonId).ToList();
        }

        /// <summary>
        /// Lấy danh sách item
        /// </summary>
        /// <returns></returns>
        private List<KetQuaKinhDoanh_ItemImport> getData_ItemImport()
        {
            return db.KetQuaKinhDoanh_ItemImport.Where(w => w.IsDelete != true && w.Publish == true).OrderBy(o=>o.Order).ToList();
        }

        [WebMethod]
        public static object updateItemValue(int salonId, int itemId, string importTime, int value)
        {
            using (var db = new Solution_30shineEntities())
            {
                var msg = new Msg();
                var error = 0;
                try
                {
                    var ImportTime = Convert.ToDateTime(importTime, new CultureInfo("vi-VN"));
                    var record = db.KetQuaKinhDoanh_FlowImport.FirstOrDefault(w => w.KQKDSalonId == salonId && w.KQKDItemId == itemId && w.ImportDate == ImportTime);
                    if (record != null)
                    {
                        record.Value = value;
                        record.ModifiedTime = DateTime.Now;
                    }
                    else
                    {
                        record = new KetQuaKinhDoanh_FlowImport();
                        record.KQKDSalonId = salonId;
                        record.KQKDItemId = itemId;
                        record.ImportDate = ImportTime;
                        record.Value = value;
                        record.IsDelete = false;
                        record.CreatedTime = DateTime.Now;
                    }
                    db.KetQuaKinhDoanh_FlowImport.AddOrUpdate(record);
                    error += db.SaveChanges() > 0 ? 0 : 1;
                }
                catch {
                    error++;
                }

                if (error == 0)
                {
                    msg.success = true;
                    msg.msg = "Cập nhật thành công!";
                }
                else
                {
                    msg.success = false;
                    msg.msg = "Cập nhật thất bại!";
                }
                return msg;
            }            
        }

        [WebMethod]
        public static object updateNote(string importTime, string textNote)
        {
            using (var db = new Solution_30shineEntities())
            {
                var msg = new Msg();
                var error = 0;
                try
                {
                    var ImportTime = Convert.ToDateTime(importTime, new CultureInfo("vi-VN"));
                    var note = db.KetQuaKinhDoanh_FlowImport.FirstOrDefault(w => w.IsDelete != true && w.ImportDate == ImportTime && w.KQKDSalonId == null && w.KQKDItemId == null);
                    if (note == null)
                    {
                        note = new KetQuaKinhDoanh_FlowImport();
                        note.ImportDate = ImportTime;
                        note.CreatedTime = DateTime.Now;
                        note.IsDelete = false;
                        note.Note = textNote;
                    }
                    else
                    {
                        note.Note = textNote;
                        note.ModifiedTime = DateTime.Now;
                    }

                    db.KetQuaKinhDoanh_FlowImport.AddOrUpdate(note);
                    error += db.SaveChanges() > 0 ? 0 : 1;
                }
                catch
                {
                    error++;
                }

                if (error == 0)
                {
                    msg.success = true;
                    msg.msg = "Cập nhật thành công!";
                }
                else
                {
                    msg.success = false;
                    msg.msg = "Cập nhật thất bại!";
                }
                return msg;
            }
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

       

        public class cls_ketquakinhdoanh_nhaplieu
        {
            public int SalonId { get; set; }
            public string SalonName { get; set; }
            public List<Store_KetQuaKinhDoanh_NhapLieu_Result> NhapLieu { get; set; }
        }
    }
}