﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Globalization;
using System.Web.Script.Serialization;
using ExportToExcel;
using Excel;
using LinqKit;
using System.Data.SqlClient;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.Report
{
    public partial class KetQuaKinhDoanh : System.Web.UI.Page
    {
        private Solution_30shineEntities db;
        private Expression<Func<BillService, bool>> Where = PredicateBuilder.True<BillService>();
        CultureInfo culture = new CultureInfo("vi-VN");

        private string PageID = "";

        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        protected bool Perm_ViewAllData = false;
        protected bool Perm_ViewAllDate = false;
        private bool Perm_Access = false;
        protected bool IsAccountant = false;

        protected string Day1 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-2));
        protected string Day2 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-1));
        protected string Day3 = string.Format("{0:dd/MM/yyyy}", DateTime.Now);

        protected string table = "";
        private DateTime timeFrom;
        private DateTime timeTo;
        private int salonId;
        protected string textNote;
        protected void checkPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //string perName = Session["User_Permission"].ToString();
                //string url = Request.Url.AbsolutePath.ToString();
                //Solution_30shineEntities db = new Solution_30shineEntities();
                //var pem = (from m in db.Tbl_Permission_Map
                //           join mn in db.Tbl_Permission_Menu on m.mID equals mn.mID
                //           where m.pName == perName && mn.Url_Main.EndsWith(url) || mn.Url_Rewrite.EndsWith(url)
                //           select new { m.aID }
                //           ).FirstOrDefault();
                //if (pem != null)
                //{
                //    var pemArray = pem.aID.Split(',');
                //    if (Array.IndexOf(pemArray, "1") > -1)
                //    {
                //        Perm_Access = true;
                //        if (Array.IndexOf(pemArray, "2") > -1)
                //        {
                //            Perm_ViewAllData = true;
                //        }
                //        if (Array.IndexOf(pemArray, "7") > -1)
                //        {
                //            Perm_ViewAllDate = true;
                //        }
                //        if (Array.IndexOf(pemArray, "4") > -1)
                //        {
                //            Perm_Edit = true;
                //        }
                //        if (Array.IndexOf(pemArray, "5") > -1)
                //        {
                //            Perm_Delete = true;
                //        }
                //    }
                //    else
                //    {
                //        ContentWrap.Visible = false;
                //        NotAllowAccess.Visible = true;
                //    }

                //}
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_ViewAllDate = permissionModel.CheckPermisionByAction("Perm_ViewAllDate", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();

            }
        }
        public KetQuaKinhDoanh()
        {
            db = new Solution_30shineEntities();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            checkPermission();

            if (!IsPostBack)
            {
                TxtDateTimeFrom.Text = String.Format("{0:dd/MM/yyyy}", DateTime.Now);
                //genReportTable();
                divNote.Visible = false;
            }
            else
            {
                RemoveLoading();
            }
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            genReportTable();
        }

        /// <summary>
        /// Lấy full dữ liệu báo cáo kết quả kinh doanh
        /// </summary>
        /// <returns></returns>
        private List<cls_ketquakinhdoanh_itemsalon> getData_KetQuaKinhDoanh()
        {
            var data = new List<cls_ketquakinhdoanh_itemsalon>();
            var item = new cls_ketquakinhdoanh_itemsalon();
            var itemTotal = new cls_ketquakinhdoanh_itemsalon();
            //try
            //{
            if (TxtDateTimeFrom.Text != "")
            {
                timeFrom = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                if (!timeFrom.Equals(null) && TxtDateTimeTo.Text != "")
                {
                    timeTo = Convert.ToDateTime(TxtDateTimeTo.Text, culture).AddDays(1);
                }
                else
                {
                    timeTo = timeFrom.AddDays(1);
                }
            }
            else
            {
                timeFrom = DateTime.Now;
                timeTo = timeFrom.AddDays(1);
            }

            var listSalon = getData_ListSalon();
            if (listSalon.Count > 0)
            {
                foreach (var v in listSalon)
                {
                    item = new cls_ketquakinhdoanh_itemsalon();
                    salonId = v.Id;
                    item.SalonId = v.Id;
                    item.SalonName = v.ShortName.ToUpper();
                    item.LuotKhach = getData_LuotKhach(false);
                    item.DV_ShineCombo100k = getData_DoanhSo_DichVu(53);
                    item.DV_KidCombo = getData_DoanhSo_DichVu(74);
                    item.DV_DuongProtein = getData_DoanhSo_DichVu(69);
                    item.DV_Uon250k = getData_DoanhSo_DichVu(16);
                    item.DV_Nhuom180k = getData_DoanhSo_DichVu(14);
                    item.DV_Tay100k = getData_DoanhSo_DichVu(17);
                    item.DV_Tay2Lan200k = getData_DoanhSo_DichVu(24);

                    item.DT_Salon = getData_DoanhThu_DichVu();
                    item.DT_MyPham = getData_DoanhThu_SanPham(false);
                    item.DT_Salon_MyPham = item.DT_Salon + item.DT_MyPham;

                    item.GV_NhanVien_HoaHong = getData_GV_LuongMyPhamNhanVienSalon();
                    item.GV_MyPhamXuatBan = getData_GV_MyPhamXuatBan(false);
                    item.GV_TienLuongNhanVien = getData_GV_LuongNhanVienSalon();
                    item.GV_MyPhamVatTu = getData_GV_MyPhamVatTu();

                    item.CP_BH_QL_Khac = getData_FlowImport(2);
                    item.CP_Marketing = getData_FlowImport(3);
                    //item.CP_Marketing = Convert.ToInt64(getData_MarketingCostBySalon(salonId));
                    item.CP_IT = getData_FlowImport(9);
                    item.CP_PhatSinh = getData_FlowImport(4);

                    item.LNG = item.DT_Salon_MyPham - (item.GV_MyPhamVatTu + item.GV_MyPhamXuatBan + item.GV_NhanVien_HoaHong + item.GV_TienLuongNhanVien + item.CP_BH_QL_Khac + item.CP_Marketing + item.CP_PhatSinh + item.CP_IT);

                    // Tổng
                    itemTotal.LuotKhach += item.LuotKhach;
                    itemTotal.DV_ShineCombo100k += item.DV_ShineCombo100k;
                    itemTotal.DV_KidCombo += item.DV_KidCombo;
                    itemTotal.DV_DuongProtein += item.DV_DuongProtein;
                    itemTotal.DV_Uon250k += item.DV_Uon250k;
                    itemTotal.DV_Nhuom180k += item.DV_Nhuom180k;
                    itemTotal.DV_Tay100k += item.DV_Tay100k;
                    itemTotal.DV_Tay2Lan200k += item.DV_Tay2Lan200k;

                    itemTotal.DT_Salon += item.DT_Salon;
                    itemTotal.DT_MyPham += item.DT_MyPham;
                    itemTotal.DT_Salon_MyPham += item.DT_Salon_MyPham;

                    itemTotal.GV_NhanVien_HoaHong += item.GV_NhanVien_HoaHong;
                    itemTotal.GV_MyPhamXuatBan += item.GV_MyPhamXuatBan;
                    itemTotal.GV_TienLuongNhanVien += item.GV_TienLuongNhanVien;
                    itemTotal.GV_MyPhamVatTu += item.GV_MyPhamVatTu;

                    itemTotal.CP_BH_QL_Khac += item.CP_BH_QL_Khac;
                    itemTotal.CP_Marketing += item.CP_Marketing;
                    //itemTotal.CP_Marketing = Convert.ToInt64(getData_MarketingCostBySalon(0));
                    itemTotal.CP_IT += item.CP_IT;
                    itemTotal.CP_PhatSinh += item.CP_PhatSinh;

                    itemTotal.LNG += item.LNG;

                    data.Add(item);
                }

                // Store online
                //item = new cls_ketquakinhdoanh_itemsalon();
                //salonId = 7; //  ID trong bảng KetQuaKinhDoanh_Salon
                //item.SalonName = "Store Online";
                //item.LuotKhach = getData_FlowImport(5, "store-online");
                //item.DT_MyPham = getData_FlowImport(6, "store-online");
                //item.DT_Salon_MyPham = item.DT_MyPham;
                //item.GV_MyPhamXuatBan = getData_FlowImport(8, "store-online");
                //item.GV_NhanVien_HoaHong = getData_FlowImport(1, "store-online");
                //item.LNG = item.DT_Salon_MyPham - (item.GV_MyPhamVatTu + item.GV_MyPhamXuatBan + item.GV_NhanVien_HoaHong + item.GV_TienLuongNhanVien + item.CP_BH_QL_Khac + item.CP_Marketing + item.CP_PhatSinh);

                // Tổng + Store online
                //itemTotal.LuotKhach += item.LuotKhach;
                //itemTotal.DV_ShineCombo100k += item.DV_ShineCombo100k;
                //itemTotal.DV_Uon250k += item.DV_Uon250k;
                //itemTotal.DV_Nhuom180k += item.DV_Nhuom180k;
                //itemTotal.DV_Tay100k += item.DV_Tay100k;
                //itemTotal.DV_Tay2Lan200k += item.DV_Tay2Lan200k;

                //itemTotal.DT_Salon += item.DT_Salon;
                //itemTotal.DT_MyPham += item.DT_MyPham;
                //itemTotal.DT_Salon_MyPham += item.DT_Salon_MyPham;

                //itemTotal.GV_NhanVien_HoaHong += item.GV_NhanVien_HoaHong;
                //itemTotal.GV_MyPhamXuatBan += item.GV_MyPhamXuatBan;
                //itemTotal.GV_TienLuongNhanVien += item.GV_TienLuongNhanVien;
                //itemTotal.GV_MyPhamVatTu += item.GV_MyPhamVatTu;

                //itemTotal.CP_BH_QL_Khac += item.CP_BH_QL_Khac;
                //itemTotal.CP_Marketing += item.CP_Marketing;
                //itemTotal.CP_PhatSinh += item.CP_PhatSinh;

                //itemTotal.LNG += item.LNG;

                //data.Add(item);

                // Bộ phận S4M
                item = new cls_ketquakinhdoanh_itemsalon();
                salonId = 8; //  ID trong bảng KetQuaKinhDoanh_Salon
                item.SalonName = "S4M";
                item.GV_TienLuongNhanVien = getData_GV_LuongNhanVienS4M();
                item.DT_MyPham = getData_FlowImport(6, "s4m");
                item.CP_BH_QL_Khac = getData_FlowImport(2, "s4m");
                item.CP_Marketing = getData_FlowImport(3, "s4m");
                item.CP_IT = getData_FlowImport(9, "s4m");
                item.CP_PhatSinh = getData_FlowImport(4, "s4m");
                item.LNG = item.DT_Salon_MyPham - (item.GV_MyPhamVatTu + item.GV_MyPhamXuatBan + item.GV_NhanVien_HoaHong + item.GV_TienLuongNhanVien + item.CP_BH_QL_Khac + item.CP_Marketing + item.CP_PhatSinh + item.CP_IT);

                // Tổng + Bộ phận S4M
                itemTotal.LuotKhach += item.LuotKhach;
                itemTotal.DV_ShineCombo100k += item.DV_ShineCombo100k;
                itemTotal.DV_KidCombo += item.DV_KidCombo;
                itemTotal.DV_DuongProtein += item.DV_DuongProtein;
                itemTotal.DV_Uon250k += item.DV_Uon250k;
                itemTotal.DV_Nhuom180k += item.DV_Nhuom180k;
                itemTotal.DV_Tay100k += item.DV_Tay100k;
                itemTotal.DV_Tay2Lan200k += item.DV_Tay2Lan200k;

                itemTotal.DT_Salon += item.DT_Salon;
                itemTotal.DT_MyPham += item.DT_MyPham;
                itemTotal.DT_Salon_MyPham += item.DT_Salon_MyPham;

                itemTotal.GV_NhanVien_HoaHong += item.GV_NhanVien_HoaHong;
                itemTotal.GV_MyPhamXuatBan += item.GV_MyPhamXuatBan;
                itemTotal.GV_TienLuongNhanVien += item.GV_TienLuongNhanVien;
                itemTotal.GV_MyPhamVatTu += item.GV_MyPhamVatTu;

                itemTotal.CP_BH_QL_Khac += item.CP_BH_QL_Khac;
                itemTotal.CP_Marketing += item.CP_Marketing;
                //itemTotal.CP_Marketing = Convert.ToInt64(getData_MarketingCostBySalon(0));
                itemTotal.CP_IT += item.CP_IT;
                itemTotal.CP_PhatSinh += item.CP_PhatSinh;

                itemTotal.LNG += item.LNG;

                data.Add(item);

                itemTotal.SalonName = "TOÀN HỆ THỐNG";
                data.Insert(0, itemTotal);
            }
            //}
            //catch { }

            return data;
        }

        private List<Tbl_Salon> getData_ListSalon()
        {
            return db.Tbl_Salon.Where(w => w.IsDelete != 1 && w.Publish == true && w.IsSalonHoiQuan != true).OrderBy(w => w.Order).ToList();
        }

        /// <summary>
        /// Lượt khách/Đơn hàng
        /// </summary>
        /// <param name="salonId"></param>
        /// <param name="timeFrom"></param>
        /// <param name="timeTo"></param>
        /// <returns></returns>
        private long getData_LuotKhach(bool isOnline)
        {
            if (isOnline)
            {
                return db.BillServices.Count(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.IsDelete != 1 && w.IsOnline == true);
            }
            else
            {
                return db.BillServices.Count(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.IsDelete != 1 && w.Pending != 1 && w.SalonId == salonId && (w.ServiceIds != null && w.ServiceIds != ""));
            }
        }

        /// <summary>
        /// Doanh số dịch vụ
        /// Shine Combo 100k : ID 53
        /// Uốn 250k : ID 16
        /// Nhuộm 180k : ID 14
        /// Tẩy 100k : ID 17
        /// </summary>
        /// <param name="serviceId"></param>
        /// <returns></returns>
        private long getData_DoanhSo_DichVu(int serviceId)
        {
            object[] paras =
            {
                new SqlParameter("@timeFrom",String.Format("{0:yyyy/MM/dd}", timeFrom)),
                new SqlParameter("@timeTo",String.Format("{0:yyyy/MM/dd}", timeTo)),
                new SqlParameter("@salonId",salonId),
                new SqlParameter("@serviceId",serviceId)
            };
            long result = db.Database.SqlQuery<long>("sp_BC_getData_DoanhSo_DichVu @timeFrom, @timeTo, @salonId, @serviceId", paras).SingleOrDefault();
            return result;
        }

        /// <summary>
        /// DT - dịch vụ SALON
        /// Doanh thu dịch vụ
        /// </summary>
        /// <returns></returns>
        private long getData_DoanhThu_DichVu()
        {
            object[] paras =
            {
                new SqlParameter("@timeFrom",String.Format("{0:yyyy/MM/dd}", timeFrom)),
                new SqlParameter("@timeTo",String.Format("{0:yyyy/MM/dd}", timeTo)),
                new SqlParameter("@salonId",salonId)
            };
            long result = db.Database.SqlQuery<long>("sp_BC_getData_DoanhThu_DichVu @timeFrom, @timeTo, @salonId", paras).SingleOrDefault();
            return result;
        }

        /// <summary>
        /// DT - dịch vụ MỸ PHẨM
        /// Doanh thu sản phẩm (bao gồm cả đơn bán hàng online)
        /// Với hóa đơn online, cần làm rõ sẽ lấy ngày tạo bill (CreatedDate) hay ngày thanh toán (CompletedTime) làm mốc thời gian hiển thị
        /// </summary>
        /// <returns></returns>
        private long getData_DoanhThu_SanPham(bool isOnline)
        {
            object[] paras =
            {
                new SqlParameter("@timeFrom",String.Format("{0:yyyy/MM/dd}", timeFrom)),
                new SqlParameter("@timeTo",String.Format("{0:yyyy/MM/dd}", timeTo)),
                new SqlParameter("@salonId",salonId),
                new SqlParameter("@isOnline", isOnline)
            };
            long result = db.Database.SqlQuery<long>("sp_BC_getData_DoanhThu_SanPham @timeFrom, @timeTo, @salonId, @isOnline", paras).SingleOrDefault();
            return result;
        }

        /// <summary>
        /// GV - Mỹ phẩm xuất bán
        /// </summary>
        /// <returns></returns>
        private long getData_GV_MyPhamXuatBan(bool isOnline)
        {
            object[] paras =
            {
                new SqlParameter("@timeFrom",String.Format("{0:yyyy/MM/dd}", timeFrom)),
                new SqlParameter("@timeTo",String.Format("{0:yyyy/MM/dd}", timeTo)),
                new SqlParameter("@salonId",salonId),
                new SqlParameter("@isOnline", isOnline)
            };
            long result = db.Database.SqlQuery<long>("sp_BC_getData_GV_MyPhamXuatBan @timeFrom, @timeTo, @salonId, @isOnline", paras).SingleOrDefault();
            return result;
        }

        /// <summary>
        /// GV - Mỹ phẩm xuất dùng
        /// </summary>
        /// <returns></returns>
        private long getData_GV_MyPhamVatTu()
        {
            object[] paras =
           {
                new SqlParameter("@timeFrom",String.Format("{0:yyyy/MM/dd}", timeFrom)),
                new SqlParameter("@timeTo",String.Format("{0:yyyy/MM/dd}", timeTo)),
                new SqlParameter("@salonId",salonId)
            };
            long result = db.Database.SqlQuery<long>("sp_BC_getData_GV_MyPhamVatTu @timeFrom, @timeTo, @salonId", paras).SingleOrDefault();
            return result;
        }

        /// <summary>
        /// GV - Tiền lương nhân viên ( stylist, skinner, lễ tân, bảo vê, TSL) 
        /// </summary>
        /// <returns></returns>
        private long getData_GV_LuongNhanVienSalon()
        {
            object[] paras =
           {
                new SqlParameter("@timeFrom",String.Format("{0:yyyy/MM/dd}", timeFrom)),
                new SqlParameter("@timeTo",String.Format("{0:yyyy/MM/dd}", timeTo)),
                new SqlParameter("@salonId",salonId)
            };
            long result = db.Database.SqlQuery<long>("sp_BC_getData_GV_LuongNhanVienSalon @timeFrom, @timeTo, @salonId", paras).SingleOrDefault();
            return result;
        }

        /// <summary>
        /// GV - % nhân viên, % hoa hồng LAZADA, tiền ship hàng nội thành
        /// </summary>
        /// <returns></returns>
        private long getData_GV_LuongMyPhamNhanVienSalon()
        {
            object[] paras =
            {
                new SqlParameter("@timeFrom",String.Format("{0:yyyy/MM/dd}", timeFrom)),
                new SqlParameter("@timeTo",String.Format("{0:yyyy/MM/dd}", timeTo)),
                new SqlParameter("@salonId",salonId)
            };
            long result = db.Database.SqlQuery<long>("sp_BC_getData_GV_LuongMyPhamNhanVienSalon @timeFrom, @timeTo, @salonId", paras).SingleOrDefault();
            return result;
        }

        /// <summary>
        /// GV - Lương nhân viên Stylist4Men
        /// Bộ phận S4M, Type = 11
        /// </summary>
        /// <returns></returns>
        private long getData_GV_LuongNhanVienS4M()
        {
            object[] paras =
          {
                new SqlParameter("@timeFrom",String.Format("{0:yyyy/MM/dd}", timeFrom)),
                new SqlParameter("@timeTo",String.Format("{0:yyyy/MM/dd}", timeTo))
            };
            long result = db.Database.SqlQuery<long>("sp_BC_getData_GV_LuongNhanVienS4M @timeFrom, @timeTo", paras).SingleOrDefault();
            return result;
        }

        /// <summary>
        /// Chi phí Marketing 
        /// </summary>
        /// <returns></returns>
        private double getData_MarketingCostBySalon(int idSalon)
        {
            object[] paras =
          {
                new SqlParameter("@TuNgay",String.Format("{0:yyyy/MM/dd}", timeFrom)),
                new SqlParameter("@DenNgay",String.Format("{0:yyyy/MM/dd}", timeTo.AddDays(-1))),
                new SqlParameter("@salonId",idSalon)
            };
            double result = db.Database.SqlQuery<double>("sp_BC_getData_MarketingCostBySalon @TuNgay, @DenNgay, @salonId", paras).SingleOrDefault();
            return result;
        }

        /// <summary>
        /// Lấy dữ liệu nhập
        /// </summary>
        /// <param name="itemId"></param>
        /// <returns></returns>
        private long getData_FlowImport(int itemId, string meta = null)
        {
            object[] paras =
            {
                new SqlParameter("@timeFrom",String.Format("{0:yyyy/MM/dd}", timeFrom)),
                new SqlParameter("@timeTo",String.Format("{0:yyyy/MM/dd}", timeTo)),
                new SqlParameter("@salonId",salonId),
                new SqlParameter("@meta",meta ?? Convert.DBNull),
                new SqlParameter("@KQKDItemId",itemId)

            };
            long result = db.Database.SqlQuery<long>("sp_BC_getData_FlowImport @timeFrom, @timeTo, @salonId, @meta, @KQKDItemId", paras).SingleOrDefault();
            return result;
        }

        private void genReportTable()
        {
            var data = getData_KetQuaKinhDoanh();
            /// Generate table
            if (data != null)
            {
                var tr = "<tr><th style='border: 1px solid black; border-collapse: collapse; padding: 5px;' rowspan='2'></th><th style='border: 1px solid black; border-collapse: collapse; padding: 5px;' rowspan='2'>LNG - Toàn 30Shine</th><th style='border: 1px solid black; border-collapse: collapse; padding: 5px; font-family: Roboto Condensed Bold; font-weight: normal; text-align: center;' colspan='8'>DOANH SỐ</th><th colspan='3' style='border: 1px solid black; border-collapse: collapse; padding: 5px; font-family: Roboto Condensed Bold; font-weight: normal; text-align: center;'>DOANH THU</th><th colspan='8' style='border: 1px solid black; border-collapse: collapse; padding: 5px; font-family: Roboto Condensed Bold; font-weight: normal; text-align: center;'>CHI PHÍ</th>";
                var tr1 = "<tr>";
                var tr2 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>Tổng hóa đơn</th>";
                var tr3 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>Shine Combo</th>";
                var tr19 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>Kid Combo</th>";
                var tr20 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>Dưỡng Protein</th>";
                var tr4 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>Uốn</th>";
                var tr5 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>Nhuộm</th>";
                var tr6 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>Tẩy</th>";
                var tr7 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>Tẩy 2 lần</th>";
                var tr8 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>Tổng doanh thu</th>";
                var tr9 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>Dịch vụ</th>";
                var tr10 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>Mỹ phẩm</th>";
                var tr11 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>Giá vốn - % lương MP</th>";
                var tr12 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>GV - Mỹ phẩm xuất bán</th>";
                var tr13 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>GV - Tiền lương nhân viên (ST, SK, LT, BV, TSL) </th>";
                var tr14 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>GV - Mỹ phẩm, vật tư xuất dùng DV</th>";
                var tr15 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>BH + QL + khác phân bổ </th>";
                var tr16 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>Marketing - FB hàng ngày (Số liệu tạm tính)</th>";
                var tr17 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>IT hàng ngày </th>";
                var tr18 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>Chi phí phát sinh trong ngày (theo chi tiết *)</th>";
                //var tr19 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>LNG - Toàn 30 Shine</th>";

                tr18 += "</tr>";

                var result = "";
                foreach (var v in data)
                {
                    var row1 = "<tr><th style='border: 1px solid black; border-collapse: collapse; padding: 5px; font-family: Roboto Condensed Bold; font-weight: normal;'>" + v.SalonName + "</th>";
                    row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:#,###}", v.LNG).Replace(',', '.') + "</td>";
                    row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:#,###}", v.LuotKhach).Replace(',', '.') + "</td>";
                    row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:#,###}", v.DV_ShineCombo100k).Replace(',', '.') + "</td>";
                    row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:#,###}", v.DV_KidCombo).Replace(',', '.') + "</td>";
                    row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:#,###}", v.DV_DuongProtein).Replace(',', '.') + "</td>";
                    row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:#,###}", v.DV_Uon250k).Replace(',', '.') + "</td>";
                    row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:#,###}", v.DV_Nhuom180k).Replace(',', '.') + "</td>";
                    row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:#,###}", v.DV_Tay100k).Replace(',', '.') + "</td>";
                    row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:#,###}", v.DV_Tay2Lan200k).Replace(',', '.') + "</td>";
                    row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:#,###}", v.DT_Salon_MyPham).Replace(',', '.') + "</td>";
                    row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:#,###}", v.DT_Salon).Replace(',', '.') + "</td>";
                    row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:#,###}", v.DT_MyPham).Replace(',', '.') + "</td>";
                    row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:#,###}", v.GV_NhanVien_HoaHong).Replace(',', '.') + "</td>";
                    row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:#,###}", v.GV_MyPhamXuatBan).Replace(',', '.') + "</td>";
                    row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:#,###}", v.GV_TienLuongNhanVien).Replace(',', '.') + "</td>";
                    row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:#,###}", v.GV_MyPhamVatTu).Replace(',', '.') + "</td>";
                    row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:#,###}", v.CP_BH_QL_Khac).Replace(',', '.') + "</td>";
                    row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:#,###}", v.CP_Marketing).Replace(',', '.') + "</td>";
                    row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:#,###}", v.CP_IT).Replace(',', '.') + "</td>";
                    row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:#,###}", v.CP_PhatSinh).Replace(',', '.') + "</td>";
                    row1 += "</tr>";
                    result += row1;
                }
                //tr1 += "</tr>";
                //tr2 += "</tr>";
                //tr3 += "</tr>";
                //tr4 += "</tr>";
                //tr5 += "</tr>";
                //tr6 += "</tr>";
                //tr7 += "</tr>";
                //tr8 += "</tr>";
                //tr9 += "</tr>";
                //tr10 += "</tr>";
                //tr11 += "</tr>";
                //tr12 += "</tr>";
                //tr13 += "</tr>";
                //tr14 += "</tr>";
                //tr15 += "</tr>";
                //tr16 += "</tr>";
                //tr19 += "</tr>";
                //tr18 += "</tr>";

                table += @"<table style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>
                                    <thead>" + tr + tr1 + tr2 + tr3 + tr19 + tr20 + tr4 + tr5 + tr6 + tr7 + tr8 + tr9 + tr10 + tr11 + tr12 + tr13 + tr14 + tr15 + tr16 + tr17 + tr18 + @"</thead>
                                    <tbody>" + result + @"</tbody>
                                </table>";
                if (TxtDateTimeFrom.Text == TxtDateTimeTo.Text || (TxtDateTimeFrom.Text != "" && TxtDateTimeTo.Text == ""))
                {
                    textNote = getData_NoteByDate(Convert.ToDateTime(TxtDateTimeFrom.Text, culture));
                    if (textNote == null)
                    {
                        divNote.Visible = false;
                    }
                    else if (textNote == "")
                    {
                        divNote.Visible = false;
                    }
                    else
                    {
                        divNote.Visible = true;
                    }
                }
                else
                {
                    divNote.Visible = false;
                }
            }
            else
            {
                //
            }
        }

        /// <summary>
        /// Lấy dữ liệu ghi chú
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        private string getData_NoteByDate(DateTime date)
        {
            var note = db.KetQuaKinhDoanh_FlowImport.FirstOrDefault(w => w.IsDelete != true && w.ImportDate == date && w.KQKDSalonId == null && w.KQKDItemId == null);
            if (note != null)
            {
                return note.Note;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        /// <summary>
        /// Check permission
        /// </summary>
        private void CheckPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                string[] Allow = new string[] { "root", "admin", "salonmanager", "accountant" };
                string[] AllowViewAllData = new string[] { "root", "admin", "accountant" };
                string[] AllowViewAllDate = new string[] { "root", "admin", "accountant", "salonmanager" };
                string[] Accountant = new string[] { "accountant" };
                string[] AllowDelete = new string[] { "root", "admin", "salonmanager" };
                var Permission = Session["User_Permission"].ToString().Trim();
                if (Array.IndexOf(Allow, Permission) != -1)
                {
                    Perm_Access = true;
                }
                Perm_Edit = true;
                if (Array.IndexOf(AllowDelete, Permission) != -1)
                {
                    Perm_Delete = true;
                }

                if (Array.IndexOf(AllowViewAllDate, Permission) != -1)
                {
                    Perm_ViewAllDate = true;
                }

                if (Array.IndexOf(AllowViewAllData, Permission) != -1)
                {
                    Perm_ViewAllData = true;
                }

                if (Array.IndexOf(Accountant, Permission) != -1)
                {
                    IsAccountant = true;
                }
            }

            // Call execute function
            ExecuteByPermission();
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        private class cls_ketquakinhdoanh_itemsalon
        {
            public int SalonId { get; set; }
            public string SalonName { get; set; }
            public long LuotKhach { get; set; }
            public long DV_ShineCombo100k { get; set; }
            public long DV_KidCombo { get; set; }
            public long DV_DuongProtein { get; set; }
            public long DV_Uon250k { get; set; }
            public long DV_Nhuom180k { get; set; }
            public long DV_Tay100k { get; set; }
            public long DV_Tay2Lan200k { get; set; }
            public long DT_Salon_MyPham { get; set; }
            public long DT_Salon { get; set; }
            public long DT_MyPham { get; set; }
            public long GV_NhanVien_HoaHong { get; set; }
            public long GV_MyPhamXuatBan { get; set; }
            public long GV_TienLuongNhanVien { get; set; }
            public long GV_MyPhamVatTu { get; set; }
            public long CP_BH_QL_Khac { get; set; }
            public long CP_Marketing { get; set; }
            public long CP_IT { get; set; }
            public long CP_PhatSinh { get; set; }
            public long Note_1 { get; set; }
            public long Note_2 { get; set; }
            public long LNG { get; set; }
        }
    }
}
