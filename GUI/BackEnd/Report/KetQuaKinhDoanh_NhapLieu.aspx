﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="KetQuaKinhDoanh_NhapLieu.aspx.cs" Inherits="_30shine.GUI.BackEnd.Report.KetQuaKinhDoanh_NhapLieu" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<%--<%@ Register Src="~/GUI/BackEnd/Report/ReportMenu.ascx" TagName="ReportMenu" TagPrefix="uc1" %>--%>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">

    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>

    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <%--<uc1:ReportMenu runat="server" id="ReportMenu" />--%>

        <style>
            .text-note-wp label { margin-top: 10px; margin-bottom: 10px; width: 100%; float: left; }
            .text-note-wp textarea { width: 60%; padding: 10px; border: 1px solid #ddd; }
        </style>

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Báo cáo &nbsp;&#187; </li>
                        <li class="be-report-li"><a href="javascript:void(0);"><i class="fa fa-th-large"></i>Kết quả kinh doanh &nbsp;&#187; Nhập liệu</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing be-report" onload="abc()">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <!-- Filter -->
                <div class="row">
                    <div class="filter-item" style="margin-left: 0;">
                        <strong class="st-head" style="margin-right: 10px;"><i class="fa fa-clock-o"></i>Ngày nhập</strong>
                        <div class="datepicker-wp">
                            <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtImportTime" placeholder="Từ ngày"
                                ClientIDMode="Static" runat="server"></asp:TextBox>
                        </div>
                        <% if (Perm_ViewAllDate)
                        { %>
                        <br />
                        <% }
                        else
                        { %>
                        <style>
                            .datepicker-wp { display: none; }
                            .customer-listing .tag-wp { padding-left: 0; padding-top: 15px; }
                        </style>
        <% } %>
        <div class="tag-wp">
            <p class="tag tag-date" onclick="viewDataByDate($(this), '<%=Day1 %>')">Hôm kia <span class="txt-date"><%=Day1 %></span></p>
            <p class="tag tag-date" onclick="viewDataByDate($(this), '<%=Day2 %>')">Hôm qua<span class="txt-date"><%=Day2 %></span></p>
            <p class="tag tag-date tag-date-today" onclick="viewDataByDate($(this), '<%=Day3 %>')">Hôm nay<span class="txt-date"><%=Day3 %></span></p>
        </div>
        </div>

            <asp:Panel ID="ViewData" CssClass="st-head btn-viewdata" Text="Xem dữ liệu"
                ClientIDMode="Static" onclick="excPaging(1)" runat="server" Style="margin-left: -67px; position: relative; z-index: 1000;">
                Xem dữ liệu
        </asp:Panel>
        <div class="export-wp drop-down-list-wp" style="display: none;">
            <asp:Panel ID="fff" CssClass="st-head btn-viewdata"
                ClientIDMode="Static" runat="server" onclick="EntryExportExcel('all')">
                Xuất File Excel
            </asp:Panel>
            <%--OnClick="Exc_ExportExcel" --%>
            <ul class="ul-drop-down-list" style="display: none;">
                <li onclick="EntryExportExcel('all')">Tất cả dữ liệu</li>
                <li onclick="EntryExportExcel('current page')">Trang hiện tại</li>
            </ul>
        </div>
        <%--<asp:Panel ID="Btn_ExportExcel" CssClass="st-head btn-viewdata"
                ClientIDMode="Static" onclick="excExcel('Export')" runat="server">Xuất File Excel</asp:Panel>--%>
        <%--<asp:Panel ID="Btn_ImportExcel" CssClass="st-head btn-viewdata"
                ClientIDMode="Static" onclick="excExcel('Import')" runat="server">Nhập File Excel</asp:Panel>--%>
        </div> 
        <!-- End Filter -->
        <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
        <asp:UpdatePanel ID="UPTotal" runat="server" ClientIDMode="Static" style="margin-top: 15px;">
            <ContentTemplate>
                <div class="row">
                    <strong class="st-head"><i class="fa fa-file-text"></i>NHẬP DỮ LIỆU</strong>

                </div>
                <div class="row">
                    <div class="col-xs-12 table-wp">
                        <% if (listItem.Count > 0 && listItemNhapLieu.Count > 0)
                            { %>
                        <table>
                            <thead>
                                <tr>
                                    <th class="th-salon">Salon</th>
                                    <% foreach (var v in listItem)
                                        { %>
                                    <th><%=v.Title %></th>
                                    <% } %>
                                </tr>
                            </thead>
                            <tbody>
                                <% foreach (var v in listItemNhapLieu)
                                    { %>
                                <tr>
                                    <td><%=v.SalonName %></td>
                                    <% if (v.NhapLieu.Count > 0)
                                        { %>
                                    <% foreach (var v2 in v.NhapLieu)
                                        { %>
                                    <td>
                                        <input type="text" onchange="updateValue(<%=v.SalonId%>,<%=v2.Id%>, $(this).val())" value="<%=v2.Value %>" onkeyup="FormatCurrency($(this));" onkeypress="return isNumber(event)" /></td>
                                    <%} %>
                                    <%} %>
                                </tr>
                                <%} %>
                            </tbody>
                        </table>
                        <% }%>
                    </div>
                    <div class="col-xs-12 text-note-wp">
                        <label style="">Ghi chú:</label>
                        <textarea class="text-note" id="Note" placeholder="Nhập ghi chú" onchange="updateNote($(this))"><%=textNote %></textarea>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>

        <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
        <asp:HiddenField runat="server" ClientIDMode="Static" ID="IsPaging" />
        <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
        <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_ExcelType" />
        <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_EXCELPage" />
        <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_EXCELSegment" />
        </div> 
    <%-- END Listing --%>
</div>

        <script type="text/javascript">
            jQuery(document).ready(function () {
                // Add active menu
                $("#glbAdminSales").addClass("active");
                $("#glbAdminKetQuaKinhDoanh").addClass("active");
                $("li.be-report-li").addClass("active");

                // Price format
                UP_FormatPrice('.be-report-price');

                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) {
                        //$input.val($input.val().split(' ')[0]);
                    }
                });

                // View data today
                $(".tag-date-today").click();
            });

            function SubTableFixHeight() {
                $("tbody table.sub-table").each(function () {
                    $(this).height($(this).parent().height() + 1);
                });
            }

            function viewDataByDate(This, time) {
                $(".tag-date.active").removeClass("active");
                This.addClass("active");
                $("#TxtImportTime").val(time);
                $("#ViewData").click();
            }

            //============================
            // Event delete
            //============================
            function del(This, code, name) {
                var code = code || null,
                    name = name || null,
                    Row = This;
                if (!code) return false;

                // show EBPopup
                $(".confirm-yn").openEBPopup();
                $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn xóa [ " + name + " ] ?");

                $("#EBPopup .yn-yes").bind("click", function () {
                    $.ajax({
                        type: "POST",
                        url: "/GUI/SystemService/Ajax/Del.aspx/Delele_Bill",
                        data: '{Code : "' + code + '"}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json", success: function (response) {
                            var mission = JSON.parse(response.d);
                            if (mission.success) {
                                delSuccess();
                                Row.remove();
                            } else {
                                delFailed();
                            }
                        },
                        failure: function (response) { alert(response.d); }
                    });
                });
                $("#EBPopup .yn-no").bind("click", function () {
                    autoCloseEBPopup(0);
                });
            }

            /**
            * Cập nhật giá trị theo SalonID và ItemID
            */
            function updateValue(salonId, itemId, value) {
                $.ajax({
                    type: "POST",
                    url: "/admin/bao-cao/ket-qua-kinh-doanh/nhap-lieu/update",
                    data: '{salonId : ' + salonId + ', itemId : ' + itemId + ', importTime : "' + $("#TxtImportTime").val() + '", value : ' + value.replace(/\D/g, '') + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        alert(response.d.msg);
                    },
                    failure: function (response) { alert(response.d); }
                });
            }

            /**
            * Cập nhật ghi chú theo ngày nhập
            */
            function updateNote(This) {
                console.log('asdf');
                $.ajax({
                    type: "POST",
                    url: "/admin/bao-cao/ket-qua-kinh-doanh/nhap-lieu/note-update",
                    data: '{ importTime : "' + $("#TxtImportTime").val() + '", textNote : "' + This.val() + '" }',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        alert(response.d.msg);
                    },
                    failure: function (response) { alert(response.d); }
                });
            }

            function FormatCurrency(THIS) {
                    var val = $(THIS).val();
                    val = val.replace(/,/g, "")
                    $(THIS).val("");
                    val += '';
                    x = val.split('.');
                    x1 = x[0];
                    x2 = x.length > 1 ? '.' + x[1] : '';

                    var rgx = /(\d+)(\d{3})/;

                    while (rgx.test(x1)) {
                        x1 = x1.replace(rgx, '$1' + ',' + '$2');
                    }
                    $(THIS).val(x1 + x2);
                
            }

        </script>

        <style>
            .table-wp { padding-left: 0 !important; padding-right: 0 !important; overflow: auto; }
            .table-wp table { border-collapse: collapse; width: 100%; float: left; }
            .table-wp table, .table-wp th, .table-wp td { border: 1px solid black; }
            .table-wp table th { font-family: Roboto Condensed Bold; font-weight: normal; padding: 5px; width: 140px; text-align: center; }
            .table-wp table th:first-child { min-width: 180px !important; padding-left: 5px; padding-right: 5px; }
            .table-wp table td:first-child { min-width: 180px !important; padding-left: 5px; padding-right: 5px; }
            .table-wp table td:not(:first-child) { text-align: center; padding: 3px; }
            .table-wp table input[type="text"] { text-align: center; width: 100%; margin-left: 0 !important; }
        </style>

    </asp:Panel>
</asp:Content>

