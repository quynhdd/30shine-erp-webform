﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Globalization;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.Report
{
    public partial class KetQuaKinhDoanh_ImportItem : System.Web.UI.Page
    {
        protected KetQuaKinhDoanh_FlowImport RECORD;
        protected string _Code;
        protected bool _IsUpdate = false;
        private bool Perm_Access = false;
        private Solution_30shineEntities db;
        private int integer;
        private long _long;
        CultureInfo culture = new CultureInfo("vi-VN");
        protected void checkPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //string perName = Session["User_Permission"].ToString();
                //string url = Request.Url.AbsolutePath.ToString();
                //Solution_30shineEntities db = new Solution_30shineEntities();
                //var pem = (from m in db.Tbl_Permission_Map
                //           join mn in db.Tbl_Permission_Menu on m.mID equals mn.mID
                //           where m.pName == perName && mn.Url_Main.EndsWith(url) || mn.Url_Rewrite.EndsWith(url)
                //           select new { m.aID }
                //           ).FirstOrDefault();
                //if (pem != null)
                //{
                //    var pemArray = pem.aID.Split(',');
                //    if (Array.IndexOf(pemArray, "1") > -1)
                //    {
                //        Perm_Access = true;

                //    }
                //    else
                //    {
                //        ContentWrap.Visible = false;
                //        NotAllowAccess.Visible = true;
                //    }

                //}
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();


            }
        }
        public KetQuaKinhDoanh_ImportItem()
        {
            db = new Solution_30shineEntities();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            checkPermission();

            if (!IsPostBack)
            {
                bindKQKDItemImport();
                bindKQKDSalon();

                if (IsUpdate())
                {
                    if (Bind_OBJ())
                    {
                        //
                    }
                }
                else
                {
                    TxtDateTimeFrom.Text = String.Format("{0:dd/MM/yyyy}", DateTime.Now);
                }

            }
        }

        protected void ExcAddOrUpdate(object sender, EventArgs e)
        {
            if (IsUpdate())
            {
                Update();
            }
            else
            {
                Add();
            }
        }

        private void Add()
        {
            var record = new KetQuaKinhDoanh_FlowImport();
            try
            {
                record.ImportDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
            }
            catch { }            
            record.KQKDItemId = int.TryParse(ddlItemId.SelectedValue, out integer) ? integer : 0;
            record.KQKDSalonId = int.TryParse(ddlSalon.SelectedValue, out integer) ? integer : 0;
            record.Value = long.TryParse(Money.Text, out _long) ? _long : 0;
            record.Note = Note.Text;
            record.CreatedTime = DateTime.Now;
            record.IsDelete = false;

            // Validate
            var Error = false;

            if (!Error)
            {
                db.KetQuaKinhDoanh_FlowImport.Add(record);
                var exc = db.SaveChanges();

                if (exc > 0)
                {
                    var MsgParam = new List<KeyValuePair<string, string>>();
                    MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                    MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thành công!"));
                    UIHelpers.Redirect("/admin/bao-cao/ket-qua-kinh-doanh/" + record.Id + ".html", MsgParam);
                }
                else
                {
                    var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển.";
                    var status = "warning";
                    UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                }
            }
        }

        private void Update()
        {
            int Id;
            if (int.TryParse(_Code, out Id))
            {
                RECORD = db.KetQuaKinhDoanh_FlowImport.FirstOrDefault(w => w.Id == Id);

                if (!RECORD.Equals(null))
                {
                    try
                    {
                        RECORD.ImportDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                    }
                    catch { }
                    RECORD.KQKDItemId = int.TryParse(ddlItemId.SelectedValue, out integer) ? integer : 0;
                    RECORD.KQKDSalonId = int.TryParse(ddlSalon.SelectedValue, out integer) ? integer : 0;
                    RECORD.Value = long.TryParse(Money.Text, out _long) ? _long : 0;
                    RECORD.Note = Note.Text;
                    RECORD.ModifiedTime = DateTime.Now;

                    db.KetQuaKinhDoanh_FlowImport.AddOrUpdate(RECORD);
                    var exc = db.SaveChanges();
                    if (exc > 0)
                    {
                        var MsgParam = new List<KeyValuePair<string, string>>();
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thành công!"));
                        UIHelpers.Redirect("/admin/bao-cao/ket-qua-kinh-doanh/" + RECORD.Id + ".html", MsgParam);
                    }
                    else
                    {
                        var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển!";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                    }
                }
                else
                {
                    var msg = "Lỗi! Bản ghi không tồn tại.";
                    var status = "warning";
                    UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);

                }
            }
            else
            {
                MsgSystem.Text = "Lỗi! Bản ghi không tồn tại.";
                MsgSystem.CssClass = "msg-system warning";
            }

            Context.RewritePath("+", "", "");
        }

        private bool Bind_OBJ()
        {
            var ExistOBJ = true;
            int Id;
            if (int.TryParse(_Code, out Id))
            {
                RECORD = db.KetQuaKinhDoanh_FlowImport.Where(w => w.Id == Id).FirstOrDefault();

                if (!RECORD.Equals(null))
                {
                    TxtDateTimeFrom.Text = String.Format("{dd/MM/yyyy}", RECORD.ImportDate);
                    Money.Text = RECORD.Value.ToString();
                    Note.Text = RECORD.Note;

                    ddlItemId.SelectedItem.Selected = false;
                    var itemSelected = ddlItemId.Items.FindByValue(RECORD.KQKDItemId.ToString());
                    if (itemSelected != null)
                    {
                        itemSelected.Selected = true;
                    }

                    ddlSalon.SelectedItem.Selected = false;
                    itemSelected = ddlItemId.Items.FindByValue(RECORD.KQKDSalonId.ToString());
                    if (itemSelected != null)
                    {
                        itemSelected.Selected = true;
                    }
                }
                else
                {
                    ExistOBJ = false;
                    var msg = "Lỗi! Bản ghi không tồn tại";
                    var status = "warning";
                    UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                }
            }
            else
            {
                ExistOBJ = false;
                var msg = "Lỗi! Bản ghi không tồn tại";
                var status = "warning";
                UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
            }

            return ExistOBJ;
        }

        /// <summary>
        /// Hiển thị danh mục item
        /// </summary>
        private void bindKQKDItemImport()
        {
            var data = db.KetQuaKinhDoanh_ItemImport.Where(w => w.IsDelete != true && w.Publish == true).ToList();
            var item = new KetQuaKinhDoanh_ItemImport();
            item.Id = 0;
            item.Title = "Chọn danh mục báo cáo";
            data.Insert(0, item);
            ddlItemId.DataTextField = "Title";
            ddlItemId.DataValueField = "Id";
            ddlItemId.DataSource = data;
            ddlItemId.DataBind();
        }

        /// <summary>
        /// Hiển thị dữ liệu Salon/Bộ phận
        /// </summary>
        private void bindKQKDSalon()
        {
            var data = db.KetQuaKinhDoanh_Salon.Where(w=>w.IsDelete != true && w.Publish == true).ToList();
            var item = new KetQuaKinhDoanh_Salon();
            item.Id = 0;
            item.SalonName = "Chọn Salon/Bộ phận";
            data.Insert(0, item);
            ddlSalon.DataTextField = "SalonName";
            ddlSalon.DataValueField = "Id";
            ddlSalon.DataSource = data;
            ddlSalon.DataBind();
        }

        public void ReloadPage()
        {
            HttpRequest Request = HttpContext.Current.Request;
            string host = "http://" + Request.Url.Host.ToString();
            string rawUrl = Request.RawUrl.ToString();
            string Url = host + rawUrl;
            Response.Redirect(Url);
        }

        private bool IsUpdate()
        {
            _Code = Request.QueryString["Code"];
            if (_Code != null)
            {
                _IsUpdate = true;
                return true;
            }
            else
            {
                _IsUpdate = false;
                return false;
            }
        }

        /// <summary>
        /// Check permission
        /// </summary>
        private void CheckPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                string[] Allow = new string[] { "root", "admin" };
                var Permission = Session["User_Permission"].ToString().Trim();
                if (Array.IndexOf(Allow, Permission) != -1)
                {
                    Perm_Access = true;
                }
            }

            // Call execute function
            ExecuteByPermission();
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
    }
}