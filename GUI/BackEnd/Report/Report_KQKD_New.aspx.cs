﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.Report
{
    public partial class Report_KQKD_New : System.Web.UI.Page
    {
        private string PageID = "BETA_BC_KQKD_V2";
        private Solution_30shineEntities db;
        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        protected bool Perm_ViewAllData = false;
        protected bool Perm_ViewAllDate = false;
        private bool Perm_Access = false;
        protected bool IsAccountant = false;
        CultureInfo culture = new CultureInfo("vi-VN");
        protected string Day1 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-2));
        protected string Day2 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-1));
        protected string Day3 = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
        IReport_Sale_V2 sale_V2 = new Report_Sales_V2Model();
        public Report_KQKD_New()
        {
            db = new Solution_30shineEntities();
        }

        private DateTime timeFrom;
        private DateTime timeTo;
        protected string textNote;

        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {                
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_ViewAllDate = permissionModel.CheckPermisionByAction("Perm_ViewAllDate", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                TxtDateTimeFrom.Text = String.Format("{0:dd/MM/yyyy}", DateTime.Now);
                //genReportTable();
                divNote.Visible = false;
            }
            else
            {
                RemoveLoading();
            }
        }

        /// <summary>
        /// _BtnClick
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void _BtnClick(object sender, EventArgs e)
        {
            BindData();
            RemoveLoading();
        }
        protected void GetDataToday(object sender,EventArgs e) {
            var dateNow = DateTime.Now.Date;
            var toDate = Convert.ToDateTime(dateNow, culture).AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
            var fromDate = Convert.ToDateTime(dateNow, culture).AddDays(1).AddHours(00).AddMinutes(00).AddSeconds(00).AddMilliseconds(000);
            var data = sale_V2.GetDataToday_BCKQKD(toDate, fromDate).OrderBy(s => s.SalonId).ToList();
            RemoveLoading();
        }
        protected void BindData()
        {
            if (TxtDateTimeFrom.Text != "")
            {
                timeFrom = Convert.ToDateTime(TxtDateTimeFrom.Text, culture).AddDays(-1).Date;
                if (!timeFrom.Equals(null) && TxtDateTimeTo.Text != "")
                {
                    timeTo = Convert.ToDateTime(TxtDateTimeTo.Text, culture).AddDays(1).Date;
                }
                else
                {
                    timeTo = timeFrom.AddDays(2).Date;
                }
            }
            else
            {
                timeFrom = DateTime.Now.Date.AddDays(-1);
                timeTo = timeFrom.AddDays(2).Date;
            }
            
            var listData = sale_V2.BC_KQKD(timeFrom,timeTo).OrderBy(s=>s.SalonId).ToList();
        }
        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }
    }
}