﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMasterVersion1.Master" AutoEventWireup="true" CodeBehind="ReportSalesV3.aspx.cs" Inherits="_30shine.GUI.BackEnd.Report.ReportSalesV3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel ID="ContentWrap" runat="server" ClientIDMode="Static">

        <div class="container-fluid sub-menu">
            <div class="row">
                <div class="col-md-12 pt-3 pb-2">
                    <ul>
                        <li><i class="fas fa-th-large"></i><a href="javascript:void(0);">Báo cáo - Vận hành</a></li>
                    </ul>
                </div>
                <div class="col-md-12">
                    <div class="sub-menu-boder"></div>
                </div>
            </div>
        </div>
        <form runat="server">
            <div class="container-fluid pt-2 pb-2">
                <div class="row">
                    <div class="form-group col-auto">
                        <strong><i class="fa fa-filter"></i>Lọc kết quả &nbsp;&#187; </strong>
                    </div>
                    <div class="col-auto form-group text-center">
                        <div class="row">
                            <div class="col-auto form-grouptext-center">
                                <div class="float-left menu-time text-center" onclick="ClickBeforeYesterday()">
                                    <label class="label">Hôm Kia</label><br />
                                    <span class="beforeYesterday"></span>
                                </div>
                                <div class="float-left menu-time text-center" onclick="ClickYesterday()">
                                    <label class="label">Hôm qua</label><br />
                                    <span class="yesterday"></span>
                                </div>
                                <div class="float-left menu-time text-center" onclick="BindDataRevenueToday()">
                                    <label class="label">Hôm nay</label><br />
                                    <span class="today"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-auto">
                        <div class="form-group search-content">
                            <input type="text" placeholder="Từ ngày" name="fromDate" class="datetime-picker form-control float-left w-auto" />
                            <i class="fa fa-arrow-circle-right float-left mt-2 mr-2 ml-2" style="color: #D0D6D8;"></i>
                            <input type="text" placeholder="Đến ngày" name="toDate" class="datetime-picker form-control float-left w-auto" />
                        </div>
                        <div class="form-group search-content">
                            <input type="text" placeholder="Từ ngày so sánh" name="fromDateCondition" class="datetime-picker form-control float-left w-auto" />
                            <i class="fa fa-arrow-circle-right float-left mt-2 mr-2 ml-2" style="color: #D0D6D8;"></i>
                            <input type="text" placeholder="Đến ngày so sánh" name="toDateCondition" class="datetime-picker form-control float-left w-auto" />
                        </div>
                    </div>
                    <div class="form-group col-auto">
                        <asp:DropDownList ID="ddlSalon" data-name="salonId" ClientIDMode="Static" CssClass="select form-control" runat="server" onchange="ChangeSalon(event)"></asp:DropDownList>
                    </div>
                    <div class="form-group col-auto region">
                        <select class="select form-control" data-name="regionId" onchange="ChangeRegion(event)"></select>
                    </div>
                    <div class="form-group col-auto">
                        <div id="ViewDataFilter" class="form-group col-auto pl-0" onclick="GetData()">
                            <div class="btn-viewdata">Xem dữ liệu</div>
                        </div>
                    </div>
                    <div class="col-md-12 col-xl-12">
                        <div class="sub-menu-boder-low"></div>
                    </div>
                </div>
            </div>
            <div class="container content">
            </div>
            <div class="container content-temp d-none">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-12 col-12 form-group">
                        <div class="border border-secondary h-100 w-100 revenue" style="border-color: black; position: relative">
                            <table class="table table-sm table-bordered mb-0 head" data-id="revenue">
                                <tbody>
                                    <tr>
                                        <td class="td-1 text-center" style="background-color: yellow">Doanh thu</td>
                                        <td class="td-2"></td>
                                        <td class="td-3"></td>
                                        <td class="td-4"></td>
                                        <td class="td-5"></td>
                                        <td class="td-6"></td>
                                    </tr>
                                    <tr>
                                        <td class="td-1 text-center font-weight-bold">Tổng DT</td>
                                        <td class="td-2"></td>
                                        <td class="td-3"></td>
                                        <td class="td-4"></td>
                                        <td class="td-5"></td>
                                        <td class="td-6"></td>
                                    </tr>
                                    <tr>
                                        <td rowspan="3" class="td-1 text-center position-relative" data-toggle="tooltip" title="Tổng doanh thu">
                                            <h4 class=""></h4>
                                        </td>
                                        <td class="td-2"></td>
                                        <td class="td-3"></td>
                                        <td class="td-4"></td>
                                        <td class="td-5"></td>
                                        <td class="td-6"></td>
                                    </tr>
                                    <tr>
                                        <td class="td-2"></td>
                                        <td class="td-3"></td>
                                        <td class="td-4"></td>
                                        <td class="td-5"></td>
                                        <td class="td-6"></td>
                                    </tr>
                                    <tr>
                                        <td class="td-2"></td>
                                        <td class="td-3"></td>
                                        <td class="td-4"></td>
                                        <td class="td-5"></td>
                                        <td class="td-6"></td>
                                    </tr>
                                    <tr>
                                        <td class="td-1 text-center" data-toggle="tooltip" title="Đơn vị tính">triệu đồng</td>
                                        <td class="td-2"></td>
                                        <td class="td-3"></td>
                                        <td class="td-4"></td>
                                        <td class="td-5"></td>
                                        <td class="td-6"></td>
                                    </tr>
                                    <tr>
                                        <td class="td-1 text-center"></td>
                                        <td class="td-2"></td>
                                        <td class="td-3"></td>
                                        <td class="td-4"></td>
                                        <td class="td-5"></td>
                                        <td class="td-6"></td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="overlay" onclick="BindDataRevenueToday()">
                                <div class="view-addition">
                                    <button class="btn btn-info">Xem thêm...</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-12 col-12">
                        <div class="row">
                            <div class="col-12 form-group">
                                <div class="border border-secondary h-100 w-100" style="border-color: black; position: relative">
                                    <table class="table table-sm table-bordered mb-0 head" data-id="lng">
                                        <tbody>
                                            <tr>
                                                <td class="td-1 text-center" style="background-color: #FEF2CD">LNG</td>
                                                <td class="td-2"></td>
                                                <td class="td-3"></td>
                                                <td class="td-4"></td>
                                                <td class="td-6"></td>
                                            </tr>
                                            <tr>
                                                <td class="td-1 text-center font-weight-bold">Tỷ lệ LNG</td>
                                                <td class="td-2">CP trực tiếp</td>
                                                <td class="td-3"></td>
                                                <td class="td-4"></td>
                                                <td class="td-6"></td>
                                            </tr>
                                            <tr>
                                                <td class="td-1 text-center" rowspan="3">
                                                    <h4></h4>
                                                </td>
                                                <td class="td-2">CP bán hàng</td>
                                                <td class="td-3"></td>
                                                <td class="td-4"></td>
                                                <td class="td-6"></td>
                                            </tr>
                                            <tr>
                                                <td class="td-2">CP QLDN</td>
                                                <td class="td-3"></td>
                                                <td class="td-4"></td>
                                                <td class="td-6"></td>
                                            </tr>
                                            <tr>
                                                <td class="td-2"></td>
                                                <td class="td-3"></td>
                                                <td class="td-4"></td>
                                                <td class="td-6"></td>
                                            </tr>
                                            <tr>
                                                <td class="td-1 text-center"></td>
                                                <td class="td-2"></td>
                                                <td class="td-3"></td>
                                                <td class="td-4"></td>
                                                <td class="td-6"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="overlay" onclick="BindLngToday()">
                                        <div class="view-addition">
                                            <button class="btn btn-info">Xem thêm...</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 form-group">
                                <div class="border border-secondary h-100 w-100" style="border-color: black; position: relative">
                                    <table class="table table-sm table-bordered mb-0 head" data-id="customer">
                                        <tbody>
                                            <tr>
                                                <td class="td-1 text-center" style="background-color: #5E9CD3">Khách hàng</td>
                                                <td class="td-2"></td>
                                                <td class="td-3"></td>
                                                <td class="td-4"></td>
                                                <td class="td-6"></td>
                                            </tr>
                                            <tr>
                                                <td class="td-1 text-center font-weight-bold">Lượt khách</td>
                                                <td class="td-2">Lượt khách cũ</td>
                                                <td class="td-3"></td>
                                                <td class="td-4"></td>
                                                <td class="td-6"></td>
                                            </tr>
                                            <tr>
                                                <td class="td-1 text-center" rowspan="3">
                                                    <h4></h4>
                                                </td>
                                                <td class="td-2">Lượt khách mới</td>
                                                <td class="td-3"></td>
                                                <td class="td-4"></td>
                                                <td class="td-6"></td>
                                            </tr>
                                            <tr>
                                                <td class="td-2">Book trước</td>
                                                <td class="td-3"></td>
                                                <td class="td-4"></td>
                                                <td class="td-6"></td>
                                            </tr>
                                            <tr>
                                                <td class="td-2">Hủy book</td>
                                                <td class="td-3"></td>
                                                <td class="td-4"></td>
                                                <td class="td-6"></td>
                                            </tr>
                                            <tr>
                                                <td class="td-1 text-center"></td>
                                                <td class="td-2">Book kín slot</td>
                                                <td class="td-3"></td>
                                                <td class="td-4"></td>
                                                <td class="td-6"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="overlay" onclick="BindCustomerToday()">
                                        <div class="view-addition">
                                            <button class="btn btn-info">Xem thêm...</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-12 col-12">
                        <div class="row">
                            <div class="col-12 form-group">
                                <div class="border border-secondary h-100 w-100" style="border-color: black; position: relative">
                                    <table class="table table-sm table-bordered mb-0 head" data-id="productivity">
                                        <tr>
                                            <td class="td-1 text-center" style="background-color: #F5B16F">Năng suất</td>
                                            <td class="td-2"></td>
                                            <td class="td-3"></td>

                                            <td class="td-6"></td>
                                        </tr>
                                        <tr>
                                            <td class="td-1 text-center font-weight-bold">Kinh tế</td>
                                            <td class="td-2">Khách/Stlist/h</td>
                                            <td class="td-3"></td>
                                            <td class="td-6"></td>
                                        </tr>
                                        <tr>
                                            <td class="td-1 text-center" rowspan="2">
                                                <h4></h4>
                                            </td>
                                            <td class="td-2">DT/Salon/ngày</td>
                                            <td class="td-3"></td>
                                            <td class="td-6"></td>
                                        </tr>
                                        <tr>
                                            <td class="td-2">Khách/Salon/ngày</td>
                                            <td class="td-3"></td>
                                            <td class="td-6"></td>
                                        </tr>
                                        <tr>
                                            <td class="td-1 text-center">nghìn/stylist/h</td>
                                            <td class="td-2">Giờ công stylist</td>
                                            <td class="td-3"></td>
                                            <td class="td-6"></td>
                                        </tr>
                                        <tr>
                                            <td class="td-1 text-center"></td>
                                            <td class="td-2">Giờ công skinner</td>
                                            <td class="td-3"></td>
                                            <td class="td-6"></td>
                                        </tr>
                                    </table>
                                    <div class="overlay" onclick="BindProductivityToday()">
                                        <div class="view-addition">
                                            <button class="btn btn-info">Xem thêm...</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 form-group">
                                <div class="border border-secondary h-100 w-100" style="border-color: black; position: relative">
                                    <table class="table table-sm table-bordered mb-0 head" data-id="quality">
                                        <tr>
                                            <td class="td-1 text-center" style="background-color: #BED6ED">Chất lượng</td>
                                            <td class="td-2"></td>
                                            <td class="td-3"></td>
                                            <td class="td-4"></td>
                                            <td class="td-6"></td>
                                        </tr>
                                        <tr>
                                            <td class="td-1 text-center font-weight-bold">Điểm TNDV</td>
                                            <td class="td-2">Rating TB</td>
                                            <td class="td-3"></td>
                                            <td class="td-4"></td>
                                            <td class="td-6"></td>
                                        </tr>
                                        <tr>
                                            <td class="td-1 text-center" rowspan="3">
                                                <h4></h4>
                                            </td>
                                            <td class="td-2">Lỗi GS</td>
                                            <td class="td-3"></td>
                                            <td class="td-4"></td>
                                            <td class="td-6"></td>
                                        </tr>
                                        <tr>
                                            <td class="td-2">Lỗi SCSC</td>
                                            <td class="td-3"></td>
                                            <td class="td-4"></td>
                                            <td class="td-6"></td>
                                        </tr>
                                        <tr>
                                            <td class="td-2">Lỗi Uốn</td>
                                            <td class="td-3"></td>
                                            <td class="td-4"></td>
                                            <td class="td-6"></td>
                                        </tr>
                                        <tr>
                                            <td class="td-1"></td>
                                            <td class="td-2">Thiếu ảnh</td>
                                            <td class="td-3"></td>
                                            <td class="td-4"></td>
                                            <td class="td-6"></td>
                                        </tr>
                                        <tr>
                                            <td class="td-1"></td>
                                            <td class="td-2">FeedBack</td>
                                            <td class="td-3"></td>
                                            <td class="td-4"></td>
                                            <td class="td-6"></td>
                                        </tr>
                                        <tr>
                                            <td class="td-1"></td>
                                            <td class="td-2">Chờ lâu</td>
                                            <td class="td-3"></td>
                                            <td class="td-4"></td>
                                            <td class="td-6"></td>
                                        </tr>
                                    </table>
                                    <div class="overlay" onclick="BindQualityToday()">
                                        <div class="view-addition">
                                            <button class="btn btn-info">Xem thêm...</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-12 col-12">
                        <div class="row">
                            <div class="col-12 form-group">
                                <div class="border border-secondary h-100 w-100" style="border-color: black; position: relative">
                                    <table class="table table-sm table-bordered mb-0 head" data-id="shineMember">
                                        <tr>
                                            <td class="td-1 text-center" style="background-color: #71AD4C">Shine Member</td>
                                            <td class="td-2"></td>
                                            <td class="td-3"></td>
                                            <td class="td-4"></td>
                                            <td class="td-6"></td>
                                        </tr>
                                        <tr>
                                            <td class="td-1 text-center font-weight-bold">Tổng SM mới</td>
                                            <td class="td-2">Khách mới mua</td>
                                            <td class="td-3"></td>
                                            <td class="td-4"></td>
                                            <td class="td-6"></td>
                                        </tr>
                                        <tr>
                                            <td class="td-1 text-center" rowspan="3">
                                                <h4></h4>
                                            </td>
                                            <td class="td-2">Khách cũ mua</td>
                                            <td class="td-3"></td>
                                            <td class="td-4"></td>
                                            <td class="td-6"></td>
                                        </tr>
                                        <tr>
                                            <td class="td-2">Quay lại</td>
                                            <td class="td-3"></td>
                                            <td class="td-4"></td>
                                            <td class="td-6"></td>
                                        </tr>
                                        <tr>
                                            <td class="td-2">DT TB/khách SM</td>
                                            <td class="td-3"></td>
                                            <td class="td-4"></td>
                                            <td class="td-6"></td>
                                        </tr>
                                        <tr>
                                            <td class="td-1 text-center"></td>
                                            <td class="td-2"></td>
                                            <td class="td-3"></td>
                                            <td class="td-4"></td>
                                            <td class="td-6"></td>
                                        </tr>
                                    </table>
                                    <div class="overlay" onclick="BindShineMemberToday()">
                                        <div class="view-addition">
                                            <button class="btn btn-info">Xem thêm...</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 d-none form-group">
                                <div class="border border-secondary h-100 w-100" style="border-color: black; position: relative">
                                    <table class="table table-sm table-bordered mb-0 head">
                                        <tr>
                                            <td class="td-1 text-center" style="background-color: #EBDCA3">Lỗi VH</td>
                                            <td class="td-2"></td>
                                            <td class="td-3"></td>
                                            <td class="td-4"></td>
                                            <td class="td-5"></td>
                                            <td class="td-6"></td>
                                        </tr>
                                        <tr>
                                            <td class="td-1 text-center font-weight-bold">Tỷ lệ</td>
                                            <td class="td-2"></td>
                                            <td class="td-3"></td>
                                            <td class="td-4"></td>
                                            <td class="td-5"></td>
                                            <td class="td-6"></td>
                                        </tr>
                                        <tr>
                                            <td class="td-1 text-center" rowspan="3">
                                                <h4></h4>
                                            </td>
                                            <td class="td-2">Lỗi chấm công</td>
                                            <td class="position-relative td-3">
                                                <h6 class="position-absolute"></h6>
                                            </td>
                                            <td class="td-4"></td>
                                            <td class="td-5"></td>
                                            <td class="td-6"></td>
                                        </tr>
                                        <tr>
                                            <td class="td-2">Lỗi Cung ứng</td>
                                            <td class="position-relative td-3">
                                                <h6 class="position-absolute"></h6>
                                            </td>
                                            <td class="td-4"></td>
                                            <td class="td-5"></td>
                                            <td class="td-6"></td>
                                        </tr>
                                        <tr>
                                            <td class="td-2">Lỗi CSVC</td>
                                            <td class="position-relative td-3">
                                                <h6 class="position-absolute"></h6>
                                            </td>
                                            <td class="td-4"></td>
                                            <td class="td-5"></td>
                                            <td class="td-6"></td>
                                        </tr>
                                        <tr>
                                            <td class="td-1"></td>
                                            <td class="td-2"></td>
                                            <td class="td-3"></td>
                                            <td class="td-4"></td>
                                            <td class="td-5"></td>
                                            <td class="td-6"></td>
                                        </tr>
                                        <tr>
                                            <td class="td-1"></td>
                                            <td class="td-2"></td>
                                            <td class="td-3"></td>
                                            <td class="td-4"></td>
                                            <td class="td-5"></td>
                                            <td class="td-6"></td>
                                        </tr>
                                    </table>
                                    <div class="overlay">
                                        <div class="view-addition">
                                            <button class="btn btn-info">Xem thêm...</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 text-center">
                        <button class="btn btn-secondary view-detail" onclick="OpenDetail($(this))">Hiện chi tiết</button>
                    </div>
                </div>
                <div class="table-detail d-none">
                    <table id="table-listing" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Salon</th>
                                <th>Khách</th>
                                <th>ShineCB</th>
                                <th>KidCB</th>
                                <th>Dưỡng</th>
                                <th>Uốn</th>
                                <th>Nhuộm</th>
                                <th>Skin Shine</th>
                                <th>Cool Shine</th>
                                <th>Lấy mụn</th>
                                <th>Uốn ATS</th>
                                <th>Nhuộm Echosline</th>
                                <th>Nhuộm-Dưỡng</th>
                                <th>Tổng DT</th>
                                <th>Doanh thu DV</th>
                                <th>Doanh thu mỹ phẩm</th>
                                <th>Doanh thu SM</th>
                                <th>Doanh thu DV/Khách</th>
                                <th>Doanh thu MP/Khách</th>
                                <th>Doanh thu SM/Khách</th>
                                <th>Stylist</th>
                                <th>Skinner</th>
                                <th>Điểm TNDV</th>
                                <th>Rating TB</th>
                                <th>% Khách cũ</th>
                                <th>% SM quay lại</th>
                                <th>% Khách chờ</th>
                                <th>% Không ảnh</th>
                                <th>% Đặt trước</th>
                                <th>% Hủy lịch</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </form>
        <style>
            table.head tr td[data-toggle=tooltip]:hover {
                border-color: darkgray;
                box-shadow: 0px 1px 5px 0px;
            }

            .overlay {
                position: absolute;
                /*display: none;*/
                width: 100%;
                height: 100%;
                top: 0;
                left: 0;
                right: 0;
                bottom: 0;
                background-color: rgba(0,0,0,0.5);
                z-index: 2;
                cursor: pointer;
            }

                .overlay .view-addition {
                    position: absolute;
                    top: 50%;
                    left: 50%;
                    font-size: 50px;
                    color: white;
                    transform: translate(-50%,-50%);
                    -ms-transform: translate(-50%,-50%);
                }

            .menu-time {
                margin-right: 7px;
                font-size: 13px;
            }

                .menu-time .label {
                    padding: 0px 15px;
                    background: #000000;
                    color: #ffffff;
                    cursor: pointer;
                    border-radius: 15px;
                    line-height: 26px;
                    width: 94px;
                }

            .form-control {
                height: 31px !important;
                width: 150px !important;
            }

            .search-content {
                height: 31px;
            }

            table.head th, table.head td {
                height: 30px;
            }

            table.head .td-1 {
                width: 142px;
            }

            table.head .td-3 {
                width: 70px;
            }

            table.head .td-4 {
                width: 56px;
            }

            table.head .td-5 {
                width: 56px;
            }

            table.head .td-6 {
                width: 90px;
            }

            table.head tr td h4 {
                font-size: 36px;
            }

            @media only screen and (max-width: 500px) {

                .container.content {
                    font-size: 11px;
                }

                table.head tr td h4 {
                    font-size: 22px;
                }

                table.head .td-1 {
                    width: 87px;
                }

                table.head .td-6 {
                    width: 52px;
                }

                table.head .td-5 {
                    width: 47px;
                }

                table.head .td-4 {
                    width: 47px;
                }

                table.head .td-3 {
                    width: 47px;
                }
            }
        </style>
        <script>
            var perm_ViewAllData = <%=Perm_ViewAllData.ToString().ToLower()%>;
            var domain = '<%=Libraries.AppConstants.URL_API_REPORTS%>';
            var staffId = <%=StaffId%>;
            var dataRevenueToday = null;
            var dvtTy, dvtTrieu, dvtNghin;
            var table = null;
            var listObjDetail = [];
            // set don vi tinh
            dvtTy = 1000000000;
            dvtTrieu = 1000000;
            dvtNghin = 1000;
            $(document).ready(function () {
                startLoading();

                var beforeYesterday = new Date();
                beforeYesterday.setDate(beforeYesterday.getDate() - 2);
                var yesterday = new Date();
                yesterday.setDate(yesterday.getDate() - 1);
                var today = new Date();
                today.setDate(today.getDate());
                $('span.beforeYesterday').text(beforeYesterday.getDate() + "/" + (beforeYesterday.getMonth() + 1) + "/" + beforeYesterday.getFullYear());
                $('span.yesterday').text(yesterday.getDate() + "/" + (yesterday.getMonth() + 1) + "/" + yesterday.getFullYear());
                $('span.today').text(today.getDate() + "/" + (today.getMonth() + 1) + "/" + today.getFullYear());
                BindRegion();
                ResetData();
                finishLoading();
            });

            function GetData(date) {

                var fromDate, toDate, fromDateCondition, toDateCondition, salonId, salonRegionId;
                fromDate = $('input[name=fromDate]').val();
                toDate = $('input[name=toDate]').val();
                if (date != '' && date !== undefined) {
                    $('input[name=fromDate]').val(date);
                    $('input[name=toDate]').val(date);
                    $('input[name=fromDateCondition]').val("");
                    $('input[name=toDateCondition]').val("");
                    fromDate = date;
                    toDate = date;
                }
                // check ngày hiện tại.
                if (fromDate === '' || fromDate === undefined || toDate === '' || toDate === undefined) {
                    ShowMessage("", "Vui lòng chọn ngày", 3, 5000);
                    return;
                }
                else {
                    moment.locale('vi');
                    var today = moment().startOf('day');
                    var fromdate = moment(fromDate, 'DD/MM/YYYY');
                    var todate = moment(toDate, 'DD/MM/YYYY');
                    if (today.isSame(fromdate) || today.isSame(todate)) {
                        BindDataRevenueToday();
                        return;
                    }
                }
                // Chuyen html temp sang chinh
                // Reset data
                ResetData();
                // mở overlay
                $('.overlay').removeClass('d-none');
                $('.overlay').addClass('d-none');

                fromDateCondition = $('input[name=fromDateCondition]').val();
                toDateCondition = $('input[name=toDateCondition]').val();
                salonId = ~~$('select[data-name=salonId]').val();
                salonRegionId = ~~$('select[data-name=regionId]').val();
                startLoading();
                // get querry
                var querry = '?salonId=' + salonId + '&regionId=' + salonRegionId + '&dateFrom=' + encodeURIComponent(fromDate) + '&dateTo=' + encodeURIComponent(toDate);
                var querryCondition = '?salonId=' + salonId + '&regionId=' + salonRegionId + '&dateFrom=' + encodeURIComponent(fromDateCondition) + '&dateTo=' + encodeURIComponent(toDateCondition);
                // Lay data chuan
                $.ajax({
                    type: "GET",
                    //async: false,
                    url: domain + "/api/operation-report-v3" + querry,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data, textStatus, jqXHR) {
                        if (fromDateCondition != "" && toDateCondition != "") {
                            GetDataCondition(data, querryCondition);
                        }
                        else {
                            BindData(data, null, null);

                        }

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        if (jqXHR.status === 400) {
                            return;
                        }
                        if (jqXHR.status != 200) {
                            let strError = 'Đã có lỗi xảy ra vui lòng liên hệ nhóm phát triển';
                            let message = "";
                            if (typeof jqXHR.responseJSON != "undefined" && typeof jqXHR.responseJSON.Message != "undefined") {
                                message = jqXHR.responseJSON.Message
                            }
                            else {
                                message = jqXHR.responseText;
                                message = message == "" ? jqXHR.statusText : message;
                            }
                            ShowMessage("Cảnh báo", strError + " <br>- status:" + jqXHR.status + ',' + textStatus + '<br>- message: ' + message + '<br>- ' + errorThrown, 3, 7000);
                        }
                        finishLoading();
                    }
                });


            }
            function GetDataCondition(dataStandal, querryCondition) {
                // Lay data cau hinh
                var dataConfig = null;
                $.ajax({
                    type: "GET",
                    async: false,
                    url: domain + "/api/operation-report-v3/sale-target" + querryCondition,
                    dataType: "json",
                    contentType: "applicate/json; charset=utf-8",
                    success: function (data, textStatus, jqXHR) {
                        dataConfig = data;
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        if (jqXHR.status === 400) {
                            finishLoading();
                            return;
                        }
                        if (jqXHR.status != 200) {
                            let strError = 'Đã có lỗi xảy ra vui lòng liên hệ nhóm phát triển';
                            let message = "";
                            if (typeof jqXHR.responseJSON != "undefined" && typeof jqXHR.responseJSON.Message != "undefined") {
                                message = jqXHR.responseJSON.Message
                            }
                            else {
                                message = jqXHR.responseText;
                                message = message == "" ? jqXHR.statusText : message;
                            }
                            ShowMessage("Cảnh báo", strError + " <br>- status:" + jqXHR.status + ',' + textStatus + '<br>- message: ' + message + '<br>- ' + errorThrown, 3, 7000);
                        }
                        finishLoading();
                    }
                });
                // Lay data dieu kien
                $.ajax({
                    type: "GET",
                    url: domain + "/api/operation-report-v3" + querryCondition,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (dataCondition, textStatus, jqXHR) {
                        BindData(dataStandal, dataConfig, dataCondition);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        if (jqXHR.status === 400) {
                            BindData(dataStandal, dataConfig, null);
                            finishLoading();
                            return;
                        }
                        if (jqXHR.status !== 200) {
                            let strError = 'Đã có lỗi xảy ra vui lòng liên hệ nhóm phát triển';
                            ShowMessage("Cảnh báo", strError + " <br>- status:" + jqXHR.status + ',' + textStatus + '<br>- ' + errorThrown, 3, 7000);
                        }
                        finishLoading();
                    }
                });
            }

            function BindData(dataStandal, dataConfig, dataCondition) {
                let salonId = ~~$('#ddlSalon').val();
                let regionId = ~~$('select[data-name=regionId]').val();
                if (!perm_ViewAllData && salonId === 0 && regionId === 0) {
                    ShowMessage('Lưu ý', 'Vui lòng chọn salon hoặc vùng', 3, 7000);
                    ResetData();
                    finishLoading();
                    return;
                }
                // Ẩn chi tiết;
                if ($('button.view-detail').text() === "Ẩn chi tiết") {
                    $('table.table-detail').removeClass('d-none');
                    $('button.view-detail').click(OpenDetail($('button.view-detail')));
                }
                //Bind data
                if (dataStandal) {
                    // Clone object
                    var objCloneDetail = JSON.parse(JSON.stringify(dataStandal));
                    dataStandalGlobal = objCloneDetail;
                    var objClone = JSON.parse(JSON.stringify(dataStandal));
                    let data = GetDataCommom(objClone, dataConfig, dataCondition);
                    // bind doanh thu
                    BindDataRevenue(data.Standal.Revenue,
                        data.Condition.RevenueCondition,
                        data.Standal.Customer,
                        data.Condition.CustomerCondition,
                        data.dataConfig);
                    // bind chi phi
                    BindDataLng(data.Standal.Lng,
                        data.Condition.LngCondition,
                        data.Standal.Revenue,
                        data.Condition.RevenueCondition,
                        data.dataConfig);
                    // bind khach hang
                    BindDataCustomer(data.Standal.Customer,
                        data.Condition.CustomerCondition,
                        data.dataConfig);
                    // bind nang suat
                    BindDataProductivity(data.Standal.Productivity,
                        data.Condition.ProductivityCondition,
                        data.Standal.Customer,
                        data.Condition.CustomerCondition,
                        data.Standal.Revenue,
                        data.Condition.RevenueCondition);
                    // bind chat luong
                    BindQuality(data.Standal.Quality,
                        data.Condition.QualityCondition,
                        data.Standal.Revenue,
                        data.Condition.RevenueCondition,
                        data.dataConfig);
                    // bind shineMember.
                    BindShineMember(data.Standal.ShineMember,
                        data.Condition.ShineMemberCondition,
                        data.Standal.Revenue,
                        data.Condition.RevenueCondition,
                        data.dataConfig);


                }
                $('[data-toggle="tooltip"]').tooltip();
                finishLoading();
            }
            function GetDataCommom(dataStandal, dataConfig, dataCondition) {
                //var Customer, ShineMember, Productivity, Quality, Revenue, Lng;
                var obj = {
                    Standal: {},
                    Condition: {},
                    dataConfig: {}
                }
                // Set object standal
                obj.Standal.Customer = {
                    totalBill: 0,
                    oldCustomer: 0,
                    newCustomer: 0,
                    bookInAdvance: 0,
                    cancelBook: 0,
                    bookAll: 0
                }
                obj.Standal.ShineMember = {
                    totalBuyShineMember: 0,
                    billNewCustomer: 0,
                    billOldCustomer: 0,
                    shineMemberComeBack: 0,
                    totalIncomePerShineMember: 0,
                    bill: 0,
                }

                obj.Standal.Productivity = {
                    totalSalon: dataStandal[0].productivity ? (~~dataStandal[0].productivity.totalSalon) : 0,
                    totalDay: dataStandal[0].productivity ? (~~dataStandal[0].productivity.totalDay) : 0,
                    totalStylistWorkTime: 0,
                    totalSkinnerWorkTime: 0
                }
                obj.Standal.Quality = {
                    tndv: 0,
                    avgRating: 0,
                    mark1and2: 0,
                    specialCustomer: 0,
                    staffError: 0,
                    wait: 0,
                    billWithoutImage: 0,
                    errorSCSC: 0,
                    errorCurling: 0,
                    errorMissImage: 0,
                    feedback: 0
                }

                obj.Standal.Revenue = {
                    TotalRevenue: 0,
                    listService: [],
                    totalBill: 0,
                    totalService: 0,
                    totalProduct: 0
                }

                obj.Standal.Lng = {
                    directFee: 0,
                    salonFee: 0,
                    manageFee: 0,
                }
                // Set Object Condition
                obj.Condition.CustomerCondition = {
                    totalBill: 0,
                    oldCustomer: 0,
                    newCustomer: 0,
                    bookInAdvance: 0,
                    cancelBook: 0,
                    bookAll: 0
                }
                obj.Condition.ShineMemberCondition = {
                    totalBuyShineMember: 0,
                    billNewCustomer: 0,
                    billOldCustomer: 0,
                    shineMemberComeBack: 0,
                    totalIncomePerShineMember: 0,
                    bill: 0,
                }
                obj.Condition.ProductivityCondition = {
                    totalSalon: 0,
                    totalDay: 0,
                    totalStylistWorkTime: 0.00,
                    totalSkinnerWorkTime: 0.00
                }
                obj.Condition.QualityCondition = {
                    tndv: 0,
                    avgRating: 0,
                    mark1and2: 0,
                    specialCustomer: 0,
                    staffError: 0,
                    wait: 0,
                    billWithoutImage: 0,
                    errorSCSC: 0,
                    errorCurling: 0,
                    errorMissImage: 0,
                    feedback: 0
                }

                obj.Condition.RevenueCondition = {
                    TotalRevenue: 0,
                    listService: [],
                    totalBill: 0,
                    totalService: 0,
                    totalProduct: 0
                }

                obj.Condition.LngCondition = {
                    directFee: 0,
                    salonFee: 0,
                    manageFee: 0,
                }
                listObjDetail = [];


                // Tinh tong data Standal
                $.each(dataStandal, function (index, value) {
                    // Tinh tong Customer
                    if (!value.customer) {
                        value.customer = {};
                    }
                    obj.Standal.Customer.totalBill += ~~value.customer.totalBill;
                    obj.Standal.Customer.oldCustomer += ~~value.customer.oldCustomer;
                    obj.Standal.Customer.newCustomer += ~~value.customer.newCustomer;
                    obj.Standal.Customer.bookInAdvance += ~~value.customer.bookInAdvance;
                    obj.Standal.Customer.cancelBook += ~~value.customer.cancelBook;
                    obj.Standal.Customer.bookAll += ~~value.customer.bookAll;
                    // Tinh tong shinemember
                    if (!value.shineMember) {
                        value.shineMember = {};
                    }
                    obj.Standal.ShineMember.totalBuyShineMember += ~~value.shineMember.totalBuyShineMember;
                    obj.Standal.ShineMember.billNewCustomer += ~~value.shineMember.billNewCustomer;
                    obj.Standal.ShineMember.billOldCustomer += ~~value.shineMember.billOldCustomer;
                    obj.Standal.ShineMember.shineMemberComeBack += ~~value.shineMember.shineMemberComeBack;
                    obj.Standal.ShineMember.totalIncomePerShineMember += ~~value.shineMember.totalIncomePerShineMember;
                    obj.Standal.ShineMember.bill += ~~value.shineMember.bill;
                    // Tinh tong nang suat
                    if (!value.productivity) {
                        value.productivity = {};
                    }
                    obj.Standal.Productivity.totalStylistWorkTime += value.productivity.totalStylistWorkTime;
                    obj.Standal.Productivity.totalSkinnerWorkTime += value.productivity.totalSkinnerWorkTime;
                    // Tinh tong chat luong
                    if (!value.quality) {
                        value.quality = {};
                    }
                    obj.Standal.Quality.tndv += ~~value.quality.tndv;
                    obj.Standal.Quality.avgRating += ~~value.quality.avgRating;
                    obj.Standal.Quality.mark1and2 += ~~value.quality.mark1and2;
                    obj.Standal.Quality.specialCustomer += ~~value.quality.specialCustomer;
                    obj.Standal.Quality.staffError += ~~value.quality.staffError;
                    obj.Standal.Quality.wait += ~~value.quality.wait;
                    obj.Standal.Quality.billWithoutImage += ~~value.quality.billWithoutImage;
                    obj.Standal.Quality.errorSCSC += ~~value.quality.errorSCSC;
                    obj.Standal.Quality.errorCurling += ~~value.quality.errorCurling;
                    obj.Standal.Quality.errorMissImage += ~~value.quality.errorMissImage;
                    obj.Standal.Quality.feedback += ~~value.quality.feedback;
                    // tinh tong Lng
                    if (!value.lng) {
                        value.lng = {};
                    }
                    obj.Standal.Lng.directFee += ~~value.lng.directFee;
                    obj.Standal.Lng.salonFee += ~~value.lng.salonFee;
                    obj.Standal.Lng.manageFee += ~~value.lng.manageFee;
                    // Tinh tong Revenue
                    let totalServiceDetail = 0;
                    let totalProductDetail = 0;
                    let shineCBDetail = 0;
                    let kidComboDetail = 0;
                    let duongDetail = 0;
                    let uonDetail = 0;
                    let dmnDetail = 0;
                    let tdcDetail = 0;
                    let nhuomTayDetail = 0;
                    let layMun = 0;
                    let shineMember = 0;
                    let uonATS = 0;
                    let nhuomEchosline = 0;
                    let nhuomDuong = 0;
                    $(value.revenue.listService).each(function (indexRevenue, valueRevenue) {
                        var isSum = false;
                        $(obj.Standal.Revenue.listService).each(function (index1, value1) {
                            if (value1.categoryId === valueRevenue.categoryId) {
                                value1.income += valueRevenue.income;
                                value1.billCount += valueRevenue.billCount;
                                isSum = true;
                            }
                        });
                        if (!isSum) {
                            obj.Standal.Revenue.listService.push(valueRevenue);
                        }
                        // Tính tổng dịch vụ và mỹ phẩm.
                        // dịch vụ

                        if (valueRevenue.type === 1) {
                            obj.Standal.Revenue.totalService += valueRevenue.income;
                            totalServiceDetail += valueRevenue.income;
                        }
                        // mỹ phẩm
                        if (valueRevenue.type === 0) {
                            obj.Standal.Revenue.totalProduct += valueRevenue.income;
                            totalProductDetail += valueRevenue.income;
                        }
                        // Nhom shineCombo 100
                        if (valueRevenue.categoryId === 100) {
                            shineCBDetail += valueRevenue.quantity;
                        }
                        // nhom kidcombo 101
                        if (valueRevenue.categoryId === 101) {
                            kidComboDetail += valueRevenue.quantity;
                        }
                        // nhom duong protein 103
                        if (valueRevenue.categoryId === 103) {
                            duongDetail += valueRevenue.quantity;
                        }
                        // nhom uon 102
                        if (valueRevenue.categoryId === 102) {
                            uonDetail += valueRevenue.quantity;
                        }
                        // nhom skin shine DMN 137
                        if (valueRevenue.categoryId === 137) {
                            dmnDetail += valueRevenue.quantity
                        }
                        // nhom sun shine TDC 136
                        if (valueRevenue.categoryId === 136) {
                            tdcDetail += valueRevenue.quantity;
                        }
                        // nhom nhuom tay 118
                        if (valueRevenue.categoryId === 118) {
                            nhuomTayDetail += valueRevenue.quantity;
                        }
                        // nhom lay mun 140
                        if (valueRevenue.categoryId === 140) {
                            layMun += valueRevenue.quantity;
                        }
                        //nhom membership 116
                        if (valueRevenue.categoryId === 116) {
                            shineMember += valueRevenue.quantity;
                        }
                        // nhom uốn ATS 141
                        if (valueRevenue.categoryId === 141) {
                            uonATS += valueRevenue.quantity;
                        }
                        // nhom Nhuộm Echosline 143
                        if (valueRevenue.categoryId === 143) {
                            nhuomEchosline += valueRevenue.quantity;
                        }
                        // nhom Nhuộm-Dưỡng 145
                        if (valueRevenue.categoryId === 145) {
                            nhuomDuong += valueRevenue.quantity;
                        }
                    });

                    obj.Standal.Revenue.TotalRevenue += value.revenue.totalRevenue;
                    obj.Standal.Revenue.totalBill += value.revenue.totalBill;
                    // Tính điểm TNDV detail 
                    var pointQuanlityDetail = ((parseFloat(value.quality.avgRating) / parseFloat(value.revenue.totalBill)) / 5
                        - (
                            parseFloat(value.quality.staffError)
                            + parseFloat(value.quality.wait)
                            + (parseFloat(value.quality.errorSCSC) + parseFloat(value.quality.errorCurling)) * 2
                            + parseFloat(value.quality.errorMissImage) * 0.5
                            + parseFloat(value.quality.feedback) * 20
                        )
                        / value.revenue.totalBill) * 100;
                    // Set object Detail

                    let objDetail = {
                        shortName: value.shortName,
                        salonId: value.salonId,
                        khach: addCommas(value.revenue.totalBill),
                        shineCommbo: addCommas(shineCBDetail),
                        kidCombo: addCommas(kidComboDetail),
                        duong: addCommas(duongDetail),
                        uon: addCommas(uonDetail),
                        nhuom: addCommas(nhuomTayDetail),
                        skinShine: addCommas(tdcDetail),
                        coolShine: addCommas(dmnDetail),
                        layMun: addCommas(layMun),
                        uonATS: addCommas(uonATS),
                        nhuomEchosline: addCommas(nhuomEchosline),
                        nhuomDuong: addCommas(nhuomDuong),
                        tongDT: addCommas(value.revenue.totalRevenue),
                        dtDV: addCommas(totalServiceDetail),
                        dtMP: addCommas(totalProductDetail),
                        dtSM: addCommas(shineMember),
                        dvKhach: addCommas(round(parseFloat(totalServiceDetail) / parseFloat(value.revenue.totalBill), 2)),
                        mpKhach: addCommas(round(parseFloat(totalProductDetail) / parseFloat(value.revenue.totalBill), 2)),
                        smKhach: addCommas(round(parseFloat(value.shineMember.totalIncomePerShineMember) / parseFloat(value.revenue.totalBill), 2)),
                        stylist: addCommas(value.productivity.totalStylistWorkTime),
                        skinner: addCommas(value.productivity.totalSkinnerWorkTime),
                        diemTNDV: addCommas(round(pointQuanlityDetail, 2)),
                        ratingTB: addCommas(round(parseFloat(value.quality.avgRating) / parseFloat(value.revenue.totalBill), 2)),
                        percentKhachCu: addCommas(round(parseFloat(value.customer.oldCustomer) / parseFloat(value.revenue.totalBill) * 100, 2)),
                        PecentSMQuayLai: addCommas(round(parseFloat(value.shineMember.shineMemberComeBack) / parseFloat(value.revenue.totalBill) * 100, 2)),
                        percentKhachCho: addCommas(round(parseFloat(value.quality.wait) / parseFloat(value.revenue.totalBill) * 100, 2)),
                        percentKhongAnh: addCommas(round(parseFloat(value.quality.errorMissImage) / parseFloat(value.revenue.totalBill) * 100, 2)),
                        percentDatTruoc: addCommas(round(parseFloat(value.customer.bookInAdvance) / parseFloat(value.revenue.totalBill) * 100, 2)),
                        percentHuyLich: addCommas(round(parseFloat(value.customer.cancelBook) / parseFloat(value.revenue.totalBill) * 100, 2)),
                    }
                    listObjDetail.push(objDetail);
                });
                // Tinh tong data Condition.
                if (dataCondition) {

                    obj.Condition.ProductivityCondition.totalSalon = dataCondition[0].productivity.totalSalon;
                    obj.Condition.ProductivityCondition.totalDay = dataCondition[0].productivity.totalDay;

                    $(dataCondition).each(function (index, value) {
                        // Tinh tong Customer 
                        obj.Condition.CustomerCondition.totalBill += ~~value.customer.totalBill;
                        obj.Condition.CustomerCondition.oldCustomer += ~~value.customer.oldCustomer;
                        obj.Condition.CustomerCondition.newCustomer += ~~value.customer.newCustomer;
                        obj.Condition.CustomerCondition.bookInAdvance += ~~value.customer.bookInAdvance;
                        obj.Condition.CustomerCondition.cancelBook += ~~value.customer.cancelBook;
                        obj.Condition.CustomerCondition.bookAll += ~~value.customer.bookAll;
                        // Tinh tong shinemember
                        obj.Condition.ShineMemberCondition.totalBuyShineMember += ~~value.shineMember.totalBuyShineMember;
                        obj.Condition.ShineMemberCondition.billNewCustomer += ~~value.shineMember.billNewCustomer;
                        obj.Condition.ShineMemberCondition.billOldCustomer += ~~value.shineMember.billOldCustomer;
                        obj.Condition.ShineMemberCondition.shineMemberComeBack += ~~value.shineMember.shineMemberComeBack;
                        obj.Condition.ShineMemberCondition.totalIncomePerShineMember += ~~value.shineMember.totalIncomePerShineMember;
                        obj.Condition.ShineMemberCondition.bill += ~~value.shineMember.bill;
                        // Tinh tong nang suat
                        obj.Condition.ProductivityCondition.totalStylistWorkTime += value.productivity.totalStylistWorkTime;
                        obj.Condition.ProductivityCondition.totalSkinnerWorkTime += value.productivity.totalSkinnerWorkTime;
                        // Tinh tong chat luong
                        obj.Condition.QualityCondition.tndv += ~~value.quality.tndv;
                        obj.Condition.QualityCondition.avgRating += ~~value.quality.avgRating;
                        obj.Condition.QualityCondition.mark1and2 += ~~value.quality.mark1and2;
                        obj.Condition.QualityCondition.specialCustomer += ~~value.quality.specialCustomer;
                        obj.Condition.QualityCondition.staffError += ~~value.quality.staffError;
                        obj.Condition.QualityCondition.wait += ~~value.quality.wait;
                        obj.Condition.QualityCondition.billWithoutImage += ~~value.quality.billWithoutImage;
                        obj.Condition.QualityCondition.errorSCSC += ~~value.quality.errorSCSC;
                        obj.Condition.QualityCondition.errorCurling += ~~value.quality.errorCurling;
                        obj.Condition.QualityCondition.errorMissImage += ~~value.quality.errorMissImage;
                        obj.Condition.QualityCondition.feedback += ~~value.quality.feedback;
                        // tinh tong Lng
                        obj.Condition.LngCondition.directFee += ~~value.lng.directFee;
                        obj.Condition.LngCondition.salonFee += ~~value.lng.salonFee;
                        obj.Condition.LngCondition.manageFee += ~~value.lng.manageFee;
                        // Tinh tong Revenue
                        $(value.revenue.listService).each(function (indexRevenue, valueRevenue) {
                            var isSum = false;
                            $(obj.Condition.RevenueCondition.listService).each(function (index1, value1) {
                                if (value1.categoryId === valueRevenue.categoryId) {
                                    value1.income += valueRevenue.income;
                                    value1.billCount += valueRevenue.billCount;
                                    isSum = true;
                                }
                            });
                            if (!isSum) {
                                obj.Condition.RevenueCondition.listService.push(valueRevenue);
                            }
                            // tính tổng dịch vụ, mỹ phẩm
                            // dịch vụ
                            if (valueRevenue.type === 1) {
                                obj.Standal.Revenue.totalService += valueRevenue.income;
                            }
                            // mỹ phẩm
                            if (valueRevenue.type === 0) {
                                obj.Condition.RevenueCondition.totalProduct += valueRevenue.income;
                            }
                        });

                        obj.Condition.RevenueCondition.TotalRevenue += value.revenue.totalRevenue;
                        obj.Condition.RevenueCondition.totalBill += value.revenue.totalBill;
                    });
                }

                if (!dataConfig) {

                    dataConfig = {};
                    dataConfig.saleConfig = {};
                }
                obj.dataConfig = dataConfig;
                return obj;
            }
            function BindDataRevenue(Revenue, RevenueCondition, Customer, CustomerCondition, dataConfig) {
                // ResetData Revenue;
                var tableRevenueTemp = $('.content-temp table[data-id=revenue]');
                $('.content .revenue').empty().append(tableRevenueTemp.clone());
                /* Bind html Doanh thu */
                var revenueTable = $('.content table[data-id=revenue] tr');
                var revenueConfig = dataConfig ? dataConfig.saleConfig.revenue : undefined;
                // Lay 7 ban ghi mac dinh.
                var tr1Revenue = $(revenueTable).slice(0, 1); // Doanh thu
                var tr2Revenue = $(revenueTable).slice(1, 2); // Tong doanh thu
                var tr3Revenue = $(revenueTable).slice(2, 3); // chi so "Tong doanh thu"
                var tr4Revenue = $(revenueTable).slice(3, 4);
                var tr5Revenue = $(revenueTable).slice(4, 5);
                var tr6Revenue = $(revenueTable).slice(5, 6); // Trieu dong
                var tr7Revenue = $(revenueTable).slice(6, 7); // Chi so tang giam
                //object service
                var service4 = $(Revenue.listService).slice(0, 1);
                var service5 = $(Revenue.listService).slice(1, 2);
                var service6 = $(Revenue.listService).slice(2, 3);
                var service7 = $(Revenue.listService).slice(3, 4);
                // object cau hinh 

                $(tr1Revenue).find('.td-4').text(round(Revenue.TotalRevenue / Revenue.totalBill / dvtNghin, 2) + "k");
                SetToolTip($(tr1Revenue).find('.td-4'), 'Tổng DT/Lượt khách');
                $(tr1Revenue).find('.td-6').text(ConfigInDays(Revenue.TotalRevenue / Revenue.totalBill, RevenueCondition.TotalRevenue / RevenueCondition.totalBill));
                $(tr1Revenue).find('.td-6').css('background-color', ColorConfigInDays(Revenue.TotalRevenue / Revenue.totalBill, RevenueCondition.TotalRevenue / RevenueCondition.totalBill));
                $(tr1Revenue).find('.td-6').prepend(IconCondition(Revenue.TotalRevenue / Revenue.totalBill, RevenueCondition.TotalRevenue / RevenueCondition.totalBill));

                $(tr2Revenue).find('.td-2').text('Tổng doanh thu DV');
                $(tr2Revenue).find('.td-3').text(round(Revenue.totalService / dvtTrieu, 2) + "tr");
                $(tr2Revenue).find('.td-4').text(round(Revenue.totalService / Revenue.totalBill / dvtNghin, 2) + "k");
                SetToolTip($(tr2Revenue).find('.td-4'), 'DT dịch vụ / Lượt khách');
                //$(tr2Revenue).find('.td-5').text(round(service2[0].billCount / Revenue.totalBill * 100, 2) + "%");
                //var configValue2 = ConfigRevenue(revenueConfig, service2[0].categoryId);
                //$(tr2Revenue).find('.td-5').css('background-color', ColorTarget(service2[0].billCount / Revenue.totalBill, configValue2))
                //var revenue2Condition = ValueRevenueCondition(service2[0].categoryId, RevenueCondition.listService);
                // $(tr2Revenue).find('.td-6').text(ConfigInDays(service2[0].billCount / Revenue.totalBill, revenue2Condition / RevenueCondition.totalBill));
                //$(tr2Revenue).find('.td-6').css('background-color', ColorConfigInDays(service2[0].billCount / Revenue.totalBill, revenue2Condition / RevenueCondition.totalBill))
                //$(tr2Revenue).find('.td-6').prepend(IconCondition(service2[0].billCount / Revenue.totalBill, revenue2Condition / RevenueCondition.totalBill));

                $(tr3Revenue).find('.td-1 h4').text(addCommas(round(Revenue.TotalRevenue / dvtTrieu, 2)));
                $(tr3Revenue).find('.td-2').text('Tổng doanh thu MP');
                $(tr3Revenue).find('.td-3').text(round(Revenue.totalProduct / dvtTrieu, 2) + "tr");
                $(tr3Revenue).find('.td-4').text(round(Revenue.totalProduct / Revenue.totalBill / dvtNghin, 2) + "k");
                SetToolTip($(tr3Revenue).find('.td-4'), 'DT Mỹ phẩm / Lượt khách');
                //$(tr3Revenue).find('.td-5').text(round(service3[0].billCount / Revenue.totalBill * 100, 2) + "%");
                //var configValue3 = ConfigRevenue(revenueConfig, service3[0].categoryId);
                //$(tr3Revenue).find('.td-5').css('background-color', ColorTarget(service3[0].billCount / Revenue.totalBill, configValue3))
                //var revenue3Condition = ValueRevenueCondition(service3[0].categoryId, RevenueCondition.listService);
                //$(tr3Revenue).find('.td-6').text(ConfigInDays(service3[0].billCount / Revenue.totalBill, revenue3Condition / RevenueCondition.totalBill));
                //$(tr3Revenue).find('.td-6').css('background-color', ColorConfigInDays(service3[0].billCount / Revenue.totalBill, revenue3Condition / RevenueCondition.totalBill));
                //$(tr3Revenue).find('.td-6').prepend(IconCondition(service3[0].billCount / Revenue.totalBill, revenue3Condition / RevenueCondition.totalBill));

                $(tr4Revenue).find('.td-2').text(service4[0].name);
                $(tr4Revenue).find('.td-3').text(round(service4[0].income / dvtTrieu, 2) + "tr");
                $(tr4Revenue).find('.td-4').text(round(service4[0].income / Revenue.totalBill / dvtNghin, 2) + "k");
                SetToolTip($(tr4Revenue).find('.td-4'), 'DT ' + service4[0].name + ' / Lượt khách');
                $(tr4Revenue).find('.td-5').text(round(service4[0].billCount.toFixed(2) / Revenue.totalBill * 100, 2) + "%");
                SetToolTip($(tr4Revenue).find('.td-5'), 'Tổng bill ' + service4[0].name + ' / Lượt khách * 100');
                var configValue4 = ConfigRevenue(revenueConfig, service4[0].categoryId);
                $(tr4Revenue).find('.td-5').css('background-color', ColorTarget(service4[0].billCount / Revenue.totalBill, configValue4))
                var revenue4Condition = ValueRevenueCondition(service4[0].categoryId, RevenueCondition.listService);
                $(tr4Revenue).find('.td-6').text(ConfigInDays(service4[0].billCount / Revenue.totalBill, revenue4Condition / RevenueCondition.totalBill));
                $(tr4Revenue).find('.td-6').css('background-color', ColorConfigInDays(service4[0].billCount / Revenue.totalBill, revenue4Condition / RevenueCondition.totalBill))
                $(tr4Revenue).find('.td-6').prepend(IconCondition(service4[0].billCount / Revenue.totalBill, revenue4Condition / RevenueCondition.totalBill));

                $(tr5Revenue).find('.td-2').text(service5[0].name);
                $(tr5Revenue).find('.td-3').text(round(service5[0].income / dvtTrieu, 2) + "tr");
                $(tr5Revenue).find('.td-4').text(round(service5[0].income / Revenue.totalBill / dvtNghin, 2) + "k");
                SetToolTip($(tr5Revenue).find('.td-4'), 'DT ' + service5[0].name + ' / Lượt khách');
                $(tr5Revenue).find('.td-5').text(round(service5[0].billCount / Revenue.totalBill * 100, 2) + "%");
                SetToolTip($(tr5Revenue).find('.td-5'), 'Tổng bill ' + service5[0].name + ' / Lượt khách * 100');
                var configValue5 = ConfigRevenue(revenueConfig, service5[0].categoryId);
                $(tr5Revenue).find('.td-5').css('background-color', ColorTarget(service5[0].billCount / Revenue.totalBill, configValue5))
                var revenue5Condition = ValueRevenueCondition(service5[0].categoryId, RevenueCondition.listService);
                $(tr5Revenue).find('.td-6').text(ConfigInDays(service5[0].billCount / Revenue.totalBill, revenue5Condition / RevenueCondition.totalBill));
                $(tr5Revenue).find('.td-6').css('background-color', ColorConfigInDays(service5[0].billCount / Revenue.totalBill, revenue5Condition / RevenueCondition.totalBill))
                $(tr5Revenue).find('.td-6').prepend(IconCondition(service5[0].billCount / Revenue.totalBill, revenue5Condition / RevenueCondition.totalBill));

                $(tr6Revenue).find('.td-2').text(service6[0].name);
                $(tr6Revenue).find('.td-3').text(round(service6[0].income / dvtTrieu, 2) + "tr");
                $(tr6Revenue).find('.td-4').text(round(service6[0].income / Revenue.totalBill / dvtNghin, 2) + "k");
                SetToolTip($(tr6Revenue).find('.td-4'), 'DT ' + service6[0].name + ' / Lượt khách');
                $(tr6Revenue).find('.td-5').text(round(service6[0].billCount / Revenue.totalBill * 100, 2) + "%");
                SetToolTip($(tr6Revenue).find('.td-5'), 'Tổng bill ' + service6[0].name + ' / Lượt khách * 100');
                var configValue6 = ConfigRevenue(revenueConfig, service6[0].categoryId);
                $(tr6Revenue).find('.td-5').css('background-color', ColorTarget(service6[0].billCount / Revenue.totalBill, configValue6))
                var revenue6Condition = ValueRevenueCondition(service6[0].categoryId, RevenueCondition.listService);
                $(tr6Revenue).find('.td-6').text(ConfigInDays(service6[0].billCount / Revenue.totalBill, revenue6Condition / RevenueCondition.totalBill));
                $(tr6Revenue).find('.td-6').css('background-color', ColorConfigInDays(service6[0].billCount / Revenue.totalBill, revenue6Condition / RevenueCondition.totalBill))
                $(tr6Revenue).find('.td-6').prepend(IconCondition(service6[0].billCount / Revenue.totalBill, revenue6Condition / RevenueCondition.totalBill));

                $(tr7Revenue).find('.td-1').text(ConfigInDays(Revenue.TotalRevenue, RevenueCondition.TotalRevenue));
                $(tr7Revenue).find('.td-1').css('background-color', ColorConfigInDays(Revenue.TotalRevenue, RevenueCondition.TotalRevenue))
                $(tr7Revenue).find('.td-1').prepend(IconCondition(Revenue.TotalRevenue, RevenueCondition.TotalRevenue));
                $(tr7Revenue).find('.td-2').text(service7[0].name);
                $(tr7Revenue).find('.td-3').text(round(service7[0].income / dvtTrieu, 2) + "tr");
                $(tr7Revenue).find('.td-4').text(round(service7[0].income / Revenue.totalBill / dvtNghin, 2) + "k");
                SetToolTip($(tr7Revenue).find('.td-4'), 'DT ' + service7[0].name + ' / Lượt khách');
                $(tr7Revenue).find('.td-5').text(round(service7[0].billCount / Revenue.totalBill * 100, 2) + "%");
                SetToolTip($(tr7Revenue).find('.td-5'), 'Tổng bill ' + service7[0].name + ' / Lượt khách * 100');
                var configValue7 = ConfigRevenue(revenueConfig, service7[0].categoryId);
                $(tr7Revenue).find('.td-5').css('background-color', ColorTarget(service7[0].billCount / Revenue.totalBill, configValue7))
                var revenue7Condition = ValueRevenueCondition(service7[0].categoryId, RevenueCondition.listService);
                $(tr7Revenue).find('.td-6').text(ConfigInDays(service7[0].billCount / Revenue.totalBill, revenue7Condition / RevenueCondition.totalBill));
                $(tr7Revenue).find('.td-6').css('background-color', ColorConfigInDays(service7[0].billCount / Revenue.totalBill, revenue7Condition / RevenueCondition.totalBill))
                $(tr7Revenue).find('.td-6').prepend(IconCondition(service7[0].billCount / Revenue.totalBill, revenue7Condition / RevenueCondition.totalBill));

                // bind ban ghi con lai
                var serviceOrther = $(Revenue.listService).slice(4, Revenue.listService.length);
                var listTrRevenue = '';
                $(serviceOrther).each(function (index, value) {
                    var configValue = ConfigRevenue(revenueConfig, value.categoryId);
                    var color = ColorTarget(value.billCount / Revenue.totalBill, configValue);
                    var revenueCondition = ValueRevenueCondition(value.categoryId, RevenueCondition.listService);
                    var colorCondition = ColorConfigInDays(value.billCount / Revenue.totalBill, revenueCondition / RevenueCondition.totalBill);
                    var iconCondition = IconCondition(value.billCount / Revenue.totalBill, revenueCondition / RevenueCondition.totalBill);
                    var PecentCondition = ConfigInDays(value.billCount / Revenue.totalBill, revenueCondition / RevenueCondition.totalBill);
                    listTrRevenue += `<tr> 
                                        <td class="td-1"></td><td class="td-2">${value.name}</td> 
                                        <td class="td-3">${round(value.income / dvtTrieu, 2)}tr</td> 
                                        <td class="td-4" data-toggle="tooltip" title="DT ${value.name} / Lượt khách" style="background-color:  color" style="cursor:cell">${round(value.income / Revenue.totalBill / dvtNghin, 2)}k </td> 
                                        <td class="td-5" data-toggle="tooltip" title="Tổng bill ${value.name} / Lượt khách * 100" style="cursor:cell"> ${round(value.billCount / Revenue.totalBill * 100, 2)}% </td> 
                                        <td style="background-color:${colorCondition}" class="td-6" style="cursor:cell">  ${iconCondition}  ${PecentCondition}  </td>
                                    </tr>`;
                });
                $('.content table[data-id=revenue] tbody').append(listTrRevenue);
                ///* Ket thuc bind Doanh thu */
            }
            function BindDataLng(Lng, LngCondition, Revenue, RevenueCondition, dataConfig) {
                /*Bind LNG*/
                var lngTable = $('.content table[data-id=lng] tr');
                var tr1LNG = $(lngTable).slice(0, 1); // LNG
                var tr2LNG = $(lngTable).slice(1, 2); // Tỉ lệ LNG
                var tr3LNG = $(lngTable).slice(2, 3); // chi so "Tỷ lệ"
                var tr4LNG = $(lngTable).slice(3, 4);
                var tr5LNG = $(lngTable).slice(4, 5);
                var tr6LNG = $(lngTable).slice(5, 6); // % tăng giảm
                $(tr2LNG).find('.td-3').text(round(Lng.directFee / dvtTrieu, 2) + "tr");
                var percentLNGCPTRuctiep = Lng.directFee / Revenue.TotalRevenue * 100;
                var percentLNGCPTRuctiepCD = LngCondition.directFee / RevenueCondition.TotalRevenue * 100;
                $(tr2LNG).find('.td-4').text(round(percentLNGCPTRuctiep, 2) + "%")
                // Hiển thị công thức
                SetToolTip($(tr2LNG).find('.td-4'), 'CP trực tiếp / Tổng DT * 100');

                $(tr2LNG).find('.td-4').css('background-color', ColorTarget(percentLNGCPTRuctiep, dataConfig.saleConfig.DirectFee))
                $(tr2LNG).find('.td-6').text(ConfigInDays(percentLNGCPTRuctiep, percentLNGCPTRuctiepCD));
                $(tr2LNG).find('.td-6').css('background-color', ColorConfigInDays(percentLNGCPTRuctiep, percentLNGCPTRuctiepCD))
                $(tr2LNG).find('.td-6').prepend(IconCondition(percentLNGCPTRuctiep, percentLNGCPTRuctiepCD));

                $(tr3LNG).find('.td-1 h4').text(round(100 - percentLNGCPTRuctiep, 2) + "%");
                // Hiển thị công thức
                SetToolTip($(tr3LNG).find('.td-1'), '100 - (CP trực tiếp / Tổng DT * 100)');
                $(tr3LNG).find('.td-3').text(round(Lng.salonFee / dvtTrieu, 2) + "tr");

                var pcSalonFee = Lng.salonFee / Revenue.TotalRevenue * 100;
                var pcSalonFeeCondition = LngCondition.salonFee / RevenueCondition.TotalRevenue * 100;
                $(tr3LNG).find('.td-4').text(round(pcSalonFee, 2) + "%")
                // Hiển thị công thức
                SetToolTip($(tr3LNG).find('.td-4'), 'CP bán hàng / Tổng DT * 100');
                $(tr3LNG).find('.td-4').css('background-color', ColorTarget(pcSalonFee, dataConfig.saleConfig.SalonFee))
                $(tr3LNG).find('.td-6').text(ConfigInDays(pcSalonFee, pcSalonFeeCondition));
                $(tr3LNG).find('.td-6').css('background-color', ColorConfigInDays(pcSalonFee, pcSalonFeeCondition))
                $(tr3LNG).find('.td-6').prepend(IconCondition(pcSalonFee, pcSalonFeeCondition));


                $(tr4LNG).find('.td-3').text(round(Lng.manageFee / dvtTrieu, 2) + "tr");
                var pcManageFee = Lng.manageFee / Revenue.TotalRevenue * 100;
                var pcManageFeeCondition = LngCondition.manageFee / RevenueCondition.TotalRevenue * 100;
                $(tr4LNG).find('.td-4').text(round(pcManageFee, 2) + "%")
                // Hiển thị công thức
                SetToolTip($(tr4LNG).find('.td-4'), 'CP QLDN / Tổng DT * 100');
                $(tr4LNG).find('.td-4').css('background-color', ColorTarget(pcManageFee, dataConfig.saleConfig.ManageFee))
                $(tr4LNG).find('.td-6').text(ConfigInDays(pcManageFee, pcManageFeeCondition));
                $(tr4LNG).find('.td-6').css('background-color', ColorConfigInDays(pcManageFee, pcManageFeeCondition))
                $(tr4LNG).find('.td-6').prepend(IconCondition(pcManageFee, pcManageFeeCondition));

                $(tr6LNG).find('.td-1').text(ConfigInDays(percentLNGCPTRuctiep, percentLNGCPTRuctiepCD));
                $(tr6LNG).find('.td-1').css('background-color', ColorConfigInDays(percentLNGCPTRuctiep, percentLNGCPTRuctiepCD))
                $(tr6LNG).find('.td-1').prepend(IconCondition(percentLNGCPTRuctiep, percentLNGCPTRuctiepCD));
                /*Ket thuc bind LNG*/
            }
            function BindDataCustomer(Customer, CustomerCondition, dataConfig) {
                /*Bind Customer*/
                var customerTable = $('.content table[data-id=customer] tr');
                var tr1Customer = $(customerTable).slice(0, 1); // LNG
                var tr2Customer = $(customerTable).slice(1, 2); // Tỉ lệ LNG
                var tr3Customer = $(customerTable).slice(2, 3); // chi so "Tỷ lệ"
                var tr4Customer = $(customerTable).slice(3, 4);
                var tr5Customer = $(customerTable).slice(4, 5);
                var tr6Customer = $(customerTable).slice(5, 6); // % tăng giảm
                $(tr2Customer).find('.td-3').text(addCommas(Customer.oldCustomer));
                var pcOldCustomer = Customer.oldCustomer / Customer.totalBill * 100;
                var pcOldCustomerCondition = CustomerCondition.oldCustomer / CustomerCondition.totalBill * 100;

                $(tr2Customer).find('.td-4').text(round(pcOldCustomer, 2) + "%");
                SetToolTip($(tr2Customer).find('.td-4'), 'Lượt khách cũ / Lượt khách * 100');
                $(tr2Customer).find('.td-4').css('background-color', ColorTarget(pcOldCustomer, dataConfig.saleConfig.OldCustomerPercent))
                $(tr2Customer).find('.td-6').text(ConfigInDays(pcOldCustomer, pcOldCustomerCondition));
                $(tr2Customer).find('.td-6').css('background-color', ColorConfigInDays(pcOldCustomer, pcOldCustomerCondition))
                $(tr2Customer).find('.td-6').prepend(IconCondition(pcOldCustomer, pcOldCustomerCondition));

                $(tr3Customer).find('.td-3').text(addCommas(Customer.newCustomer));
                $(tr3Customer).find('.td-1 h4').text(addCommas(Customer.totalBill));
                SetToolTip($(tr3Customer).find('.td-1'), 'Lượt khách');
                var pcNewCustomer = Customer.newCustomer / Customer.totalBill * 100;
                var pcNewCustomerCondition = CustomerCondition.newCustomer / CustomerCondition.totalBill * 100;
                $(tr3Customer).find('.td-4').text(round(pcNewCustomer, 2) + "%");
                SetToolTip($(tr3Customer).find('.td-4'), 'Lượt khách mới / Lượt khách * 100');
                $(tr3Customer).find('.td-6').text(ConfigInDays(pcNewCustomer, pcNewCustomerCondition));
                $(tr3Customer).find('.td-6').css('background-color', ColorConfigInDays(pcNewCustomer, pcNewCustomerCondition))
                $(tr3Customer).find('.td-6').prepend(IconCondition(pcNewCustomer, pcNewCustomerCondition));

                $(tr4Customer).find('.td-3').text(addCommas(Customer.bookInAdvance));
                var pcBookInAdvance = Customer.bookInAdvance / Customer.totalBill * 100;
                var pcBookInAdvanceCondition = CustomerCondition.bookInAdvance / CustomerCondition.totalBill * 100;
                $(tr4Customer).find('.td-4').text(round(pcBookInAdvance, 2) + "%");
                SetToolTip($(tr4Customer).find('.td-4'), 'Book trước / Lượt khách * 100');
                $(tr4Customer).find('.td-4').css('background-color', ColorTarget(pcBookInAdvance, dataConfig.saleConfig.BookInAdvancePercent))
                $(tr4Customer).find('.td-6').text(ConfigInDays(pcBookInAdvance, pcBookInAdvanceCondition));
                $(tr4Customer).find('.td-6').css('background-color', ColorConfigInDays(pcBookInAdvance, pcBookInAdvanceCondition))
                $(tr4Customer).find('.td-6').prepend(IconCondition(pcBookInAdvance, pcBookInAdvanceCondition));

                $(tr5Customer).find('.td-3').text(addCommas(Customer.cancelBook));
                var pcCancelBook = Customer.cancelBook / Customer.totalBill * 100;
                var pcCancelBookCondition = CustomerCondition.cancelBook / CustomerCondition.totalBill * 100;
                $(tr5Customer).find('.td-4').text(round(pcCancelBook, 2) + "%");
                SetToolTip($(tr5Customer).find('.td-4'), 'Hủy book / Lượt khách * 100');
                $(tr5Customer).find('.td-6').text(ConfigInDays(pcCancelBook, pcCancelBookCondition));
                $(tr5Customer).find('.td-6').css('background-color', ColorConfigInDays(pcCancelBook, pcCancelBookCondition))
                $(tr5Customer).find('.td-6').prepend(IconCondition(pcCancelBook, pcCancelBookCondition));

                $(tr6Customer).find('.td-1').text(ConfigInDays(Customer.totalBill, CustomerCondition.totalBill));
                $(tr6Customer).find('.td-1').css('background-color', ColorConfigInDays(Customer.totalBill, CustomerCondition.totalBill))
                $(tr6Customer).find('.td-1').prepend(IconCondition(Customer.totalBill, CustomerCondition.totalBill));
                $(tr6Customer).find('.td-3').text(addCommas(Customer.bookAll));
                var pcBookAll = Customer.bookAll / Customer.totalBill * 100;
                var pcBookAllCondition = CustomerCondition.bookAll / CustomerCondition.totalBill * 100;
                $(tr6Customer).find('.td-4').text(round(pcBookAll, 2) + "%");
                SetToolTip($(tr6Customer).find('.td-4'), 'Book kín slot / Lượt khách * 100');
                $(tr6Customer).find('.td-4').css('background-color', ColorTarget(pcBookAll, dataConfig.saleConfig.CancelBookPercent))
                $(tr6Customer).find('.td-6').text(ConfigInDays(pcBookAll, pcBookAllCondition));
                $(tr6Customer).find('.td-6').css('background-color', ColorConfigInDays(pcBookAll, pcBookAllCondition))
                $(tr6Customer).find('.td-6').prepend(IconCondition(pcBookAll, pcBookAllCondition));

                /*Ket thuc bind customer*/
            }
            function BindDataProductivity(Productivity, ProductivityCondition, Customer, CustomerCondition, Revenue, RevenueCondition) {
                /*Bind nang suat*/
                var productivityTable = $('.content table[data-id=productivity] tr');
                var tr1Productivity = $(productivityTable).slice(0, 1); // Năng suất
                var tr2Productivity = $(productivityTable).slice(1, 2); // Kinh tế
                var tr3Productivity = $(productivityTable).slice(2, 3); // chi so "Tỷ lệ"
                var tr4Productivity = $(productivityTable).slice(3, 4);
                var tr5Productivity = $(productivityTable).slice(4, 5); // Nghìn/stylist/h
                var tr6Productivity = $(productivityTable).slice(5, 6); // % tăng giảm

                var productivity2 = Customer.totalBill / Productivity.totalStylistWorkTime;
                var productivity2Condition = CustomerCondition.totalBill / ProductivityCondition.totalStylistWorkTime;
                $(tr2Productivity).find('.td-3').text(round(productivity2, 2));
                SetToolTip($(tr2Productivity).find('.td-3'), 'Lượt khách / Tổng giờ stylist');
                $(tr2Productivity).find('.td-6').text(ConfigInDays(productivity2, productivity2Condition));
                $(tr2Productivity).find('.td-6').css('background-color', ColorConfigInDays(productivity2, productivity2Condition));
                $(tr2Productivity).find('.td-6').prepend(IconCondition(productivity2, productivity2Condition));

                var percentTotal = Revenue.TotalRevenue / Productivity.totalStylistWorkTime;
                var percentTotalCondition = RevenueCondition.TotalRevenue / ProductivityCondition.totalStylistWorkTime;
                var productivity3 = Revenue.TotalRevenue / (Productivity.totalDay * Productivity.totalSalon);
                var productivity3Condition = RevenueCondition.TotalRevenue / (ProductivityCondition.totalDay * ProductivityCondition.totalSalon);
                $(tr3Productivity).find('.td-1 h4').text(round(percentTotal / dvtNghin, 2))
                SetToolTip($(tr3Productivity).find('.td-1'), 'Tổng DT / Tổng giờ stylist');
                $(tr3Productivity).find('.td-3').text(round(productivity3 / dvtTrieu, 2) + "tr");
                SetToolTip($(tr3Productivity).find('.td-3'), 'Tổng DT / (số ngày trong kỳ * số salon trong kỳ )');
                $(tr3Productivity).find('.td-6').text(ConfigInDays(productivity3, productivity3Condition));
                $(tr3Productivity).find('.td-6').css('background-color', ColorConfigInDays(productivity3, productivity3Condition));
                $(tr3Productivity).find('.td-6').prepend(IconCondition(productivity3, productivity3Condition));

                var productivity4 = Customer.totalBill / (Productivity.totalDay * Productivity.totalSalon);
                var productivity4Condition = CustomerCondition.totalBill / (ProductivityCondition.totalDay * ProductivityCondition.totalSalon);
                $(tr4Productivity).find('.td-3').text(round(productivity4, 2));
                SetToolTip($(tr4Productivity).find('.td-3'), 'Lượt khách / (số ngày trong kỳ * số salon trong kỳ)');
                $(tr4Productivity).find('.td-6').text(ConfigInDays(productivity4, productivity4Condition));
                $(tr4Productivity).find('.td-6').css('background-color', ColorConfigInDays(productivity4, productivity4Condition));
                $(tr4Productivity).find('.td-6').prepend(IconCondition(productivity4, productivity4Condition));

                var productivity5 = Productivity.totalStylistWorkTime;
                var productivity5Condition = ProductivityCondition.totalStylistWorkTime;
                $(tr5Productivity).find('.td-3').text(productivity5);
                SetToolTip($(tr5Productivity).find('.td-3'), 'Giờ công stylist');


                $(tr6Productivity).find('.td-1').text(ConfigInDays(percentTotal, percentTotalCondition));
                $(tr6Productivity).find('.td-1').css('background-color', ColorConfigInDays(percentTotal, percentTotalCondition))
                $(tr6Productivity).find('.td-1').prepend(IconCondition(percentTotal, percentTotalCondition));
                var productivity6 = Productivity.totalSkinnerWorkTime;
                var productivity6Condition = ProductivityCondition.totalSkinnerWorkTime;
                $(tr6Productivity).find('.td-3').text(productivity6);
                SetToolTip($(tr6Productivity).find('.td-3'), 'Giờ công skinner');

                /*ket thuc bind nang suat*/
            }
            function BindQuality(Quality, QualityCondition, Revenue, RevenueCondition, dataConfig) {
                /*Bind Chat luong*/
                var qualityTable = $('.content table[data-id=quality] tr');
                var tr1Quality = $(qualityTable).slice(0, 1); // Chất lượng
                var tr2Quality = $(qualityTable).slice(1, 2); // Điểm TNDV
                var tr3Quality = $(qualityTable).slice(2, 3); // "Điểm TNDV"
                var tr4Quality = $(qualityTable).slice(3, 4);
                var tr5Quality = $(qualityTable).slice(4, 5);
                var tr6Quality = $(qualityTable).slice(5, 6); // % tăng giảm
                var tr7Quality = $(qualityTable).slice(6, 7);
                var tr8Quality = $(qualityTable).slice(7, 8);

                var quality6 = Quality.avgRating / Revenue.totalBill;
                var quality6Condition = QualityCondition.avgRating / RevenueCondition.totalBill;
                $(tr2Quality).find('.td-3').text(round(quality6, 2));
                SetToolTip($(tr2Quality).find('.td-3'), 'Tổng Rating TB / Lượt khách');
                $(tr2Quality).find('.td-6').text(ConfigInDays(quality6, quality6Condition));
                $(tr2Quality).find('.td-6').css('background-color', ColorConfigInDays(quality6, quality6Condition));
                $(tr2Quality).find('.td-6').prepend(IconCondition(quality6, quality6Condition));

                var pointQuanlity = (Quality.avgRating / Revenue.totalBill) / 5 - (Quality.staffError + Quality.wait + (Quality.errorSCSC + Quality.errorCurling) * 2 + Quality.errorMissImage * 0.5 + Quality.feedback * 20) / Revenue.totalBill;
                var qualityStaffError = Quality.staffError / Revenue.totalBill * 100;
                var qualityStaffErrorCondition = QualityCondition.staffError / RevenueCondition.totalBill * 100;

                $(tr3Quality).find('.td-1 h4').text(round(pointQuanlity * 100, 2) + '%');
                SetToolTip($(tr3Quality).find('.td-1'), 'Rating TB/5 - (Lỗi GS + Chờ lâu + (Lỗi SCSC + lỗi uốn)*2 + Thiếu ảnh*0.5 + feedback*20)/Lượt khách');
                $(tr3Quality).find('.td-3').text(round(Quality.staffError, 2));
                $(tr3Quality).find('.td-4').text(round(qualityStaffError, 2) + "%");
                SetToolTip($(tr3Quality).find('.td-4'), 'Lỗi GS / Lượt khách * 100');
                $(tr3Quality).find('.td-6').text(ConfigInDays(qualityStaffError, qualityStaffErrorCondition));
                $(tr3Quality).find('.td-6').css('background-color', ColorConfigInDays(qualityStaffError, qualityStaffErrorCondition));
                $(tr3Quality).find('.td-6').prepend(IconCondition(qualityStaffError, qualityStaffErrorCondition));

                var qualityErrorScsc = Quality.errorSCSC / Revenue.totalBill * 100;
                var qualityErrorScscCondition = QualityCondition.errorSCSC / RevenueCondition.totalBill * 100;
                $(tr4Quality).find('.td-3').text(round(Quality.errorSCSC, 2));
                $(tr4Quality).find('.td-4').text(round(qualityErrorScsc, 2) + "%");
                SetToolTip($(tr4Quality).find('.td-4'), 'Lỗi SCSC / Lượt khách * 100');
                $(tr4Quality).find('.td-6').text(ConfigInDays(qualityErrorScsc, qualityErrorScscCondition));
                $(tr4Quality).find('.td-6').css('background-color', ColorConfigInDays(qualityErrorScsc, qualityErrorScscCondition));
                $(tr4Quality).find('.td-6').prepend(IconCondition(qualityErrorScsc, qualityErrorScscCondition));

                var quanlityErrorCurling = Quality.errorCurling / Revenue.totalBill * 100;
                var quanlityErrorCurlingCondition = QualityCondition.staffError / RevenueCondition.totalBill * 100;

                $(tr5Quality).find('.td-3').text(round(Quality.errorCurling, 2));
                $(tr5Quality).find('.td-4').text(round(quanlityErrorCurling, 2) + "%");
                SetToolTip($(tr5Quality).find('.td-4'), 'Lỗi Uốn / Lượt khách * 100');
                $(tr5Quality).find('.td-6').text(ConfigInDays(quanlityErrorCurling, quanlityErrorCurlingCondition));
                $(tr5Quality).find('.td-6').css('background-color', ColorConfigInDays(quanlityErrorCurling, quanlityErrorCurlingCondition));
                $(tr5Quality).find('.td-6').prepend(IconCondition(quanlityErrorCurling, quanlityErrorCurlingCondition));

                var qualityErrorMissImage = Quality.errorMissImage / Revenue.totalBill * 100;
                var qualityErrorMissImageCondition = QualityCondition.errorMissImage / RevenueCondition.totalBill * 100;

                $(tr6Quality).find('.td-3').text(round(Quality.errorMissImage, 2));
                $(tr6Quality).find('.td-4').text(round(qualityErrorMissImage, 2) + "%");
                SetToolTip($(tr6Quality).find('.td-4'), 'Lỗi thiếu ảnh / Lượt khách * 100');
                $(tr6Quality).find('.td-6').text(ConfigInDays(qualityErrorMissImage, qualityErrorMissImageCondition));
                $(tr6Quality).find('.td-6').css('background-color', ColorConfigInDays(qualityErrorMissImage, qualityErrorMissImageCondition));
                $(tr6Quality).find('.td-6').prepend(IconCondition(qualityErrorMissImage, qualityErrorMissImageCondition));

                var qualityFeedback = Quality.feedback / Revenue.totalBill * 100;
                var qualityFeedbackCondition = QualityCondition.feedback / RevenueCondition.totalBill * 100;

                $(tr7Quality).find('.td-3').text(round(Quality.feedback, 2));
                $(tr7Quality).find('.td-4').text(round(qualityFeedback, 2) + "%");
                SetToolTip($(tr7Quality).find('.td-4'), 'Feedback / Lượt khách * 100');
                $(tr7Quality).find('.td-6').text(ConfigInDays(qualityFeedback, qualityFeedbackCondition));
                $(tr7Quality).find('.td-6').css('background-color', ColorConfigInDays(qualityFeedback, qualityFeedbackCondition));
                $(tr7Quality).find('.td-6').prepend(IconCondition(qualityFeedback, qualityFeedbackCondition));

                var qualityWait = Quality.wait / Revenue.totalBill * 100;
                var qualityWaitCondition = QualityCondition.wait / RevenueCondition.totalBill * 100;

                $(tr8Quality).find('.td-3').text(round(Quality.wait, 2));
                $(tr8Quality).find('.td-4').text(round(qualityWait, 2) + "%");
                SetToolTip($(tr8Quality).find('.td-4'), 'Chờ lâu / Lượt khách * 100');
                $(tr8Quality).find('.td-6').text(ConfigInDays(qualityWait, qualityWaitCondition));
                $(tr8Quality).find('.td-6').css('background-color', ColorConfigInDays(qualityWait, qualityWaitCondition));
                $(tr8Quality).find('.td-6').prepend(IconCondition(qualityWait, qualityWaitCondition));

                /*Ket thuc bind chat luong*/
            }
            function BindShineMember(ShineMember, ShineMemberCondition, Revenue, RevenueCondition, dataConfig) {
                /*Bind shinmember*/
                var shineMemberTable = $('.content table[data-id=shineMember] tr');
                var tr1ShineMember = $(shineMemberTable).slice(0, 1); // Shine member
                var tr2ShineMember = $(shineMemberTable).slice(1, 2); // Tổng SM Mới
                var tr3ShineMember = $(shineMemberTable).slice(2, 3); // "Tổng Shine member Mới"
                var tr4ShineMember = $(shineMemberTable).slice(3, 4);
                var tr5ShineMember = $(shineMemberTable).slice(4, 5);
                var tr6ShineMember = $(shineMemberTable).slice(5, 6); // % tăng giảm

                $(tr2ShineMember).find('.td-3').text(round(ShineMember.billNewCustomer, 2));
                var pcBillNewCustomer = ShineMember.billNewCustomer / Revenue.totalBill * 100;
                var pcBillNewCustomerCondition = ShineMemberCondition.billNewCustomer / RevenueCondition.totalBill * 100;
                $(tr2ShineMember).find('.td-4').text(round(pcBillNewCustomer, 2) + "%");
                SetToolTip($(tr2ShineMember).find('.td-4'), 'Khách mới mua / Lượt khách * 100');
                $(tr2ShineMember).find('.td-4').css('background-color', ColorTarget(pcBillNewCustomer, dataConfig.saleConfig.ShineNewCustomerPercent));
                $(tr2ShineMember).find('.td-6').text(ConfigInDays(pcBillNewCustomer, pcBillNewCustomerCondition));
                $(tr2ShineMember).find('.td-6').css('background-color', ColorConfigInDays(pcBillNewCustomer, pcBillNewCustomerCondition));
                $(tr2ShineMember).find('.td-6').prepend(IconCondition(pcBillNewCustomer, pcBillNewCustomerCondition));

                var totalShine = ShineMember.billNewCustomer + ShineMember.billOldCustomer;
                var totalShineCondition = ShineMemberCondition.billNewCustomer + ShineMemberCondition.billOldCustomer;

                $(tr3ShineMember).find('.td-1 h4').text(addCommas(totalShine));
                $(tr3ShineMember).find('.td-3').text(round(ShineMember.billOldCustomer, 2));
                var pcBillOldCustomer = ShineMember.billOldCustomer / Revenue.totalBill * 100;
                var pcBillOldCustomerCondition = ShineMemberCondition.billOldCustomer / RevenueCondition.totalBill * 100;
                SetToolTip($(tr3ShineMember).find('.td-4'), 'Khách cũ mua / Lượt khách * 100');
                $(tr3ShineMember).find('.td-4').text(round(pcBillOldCustomer, 2) + "%");
                $(tr3ShineMember).find('.td-4').css('background-color', ColorTarget(pcBillOldCustomer, dataConfig.saleConfig.ShineOldCustomerPercent))
                $(tr3ShineMember).find('.td-6').text(ConfigInDays(pcBillOldCustomer, pcBillOldCustomerCondition));
                $(tr3ShineMember).find('.td-6').css('background-color', ColorConfigInDays(pcBillOldCustomer, pcBillOldCustomerCondition));
                $(tr3ShineMember).find('.td-6').prepend(IconCondition(pcBillOldCustomer, pcBillOldCustomerCondition));

                $(tr4ShineMember).find('.td-3').text(round(ShineMember.shineMemberComeBack, 2));
                var pcShineMemberComeBack = ShineMember.shineMemberComeBack / Revenue.totalBill * 100;
                var pcShineMemberComeBackCondition = ShineMemberCondition.shineMemberComeBack / RevenueCondition.totalBill * 100;
                $(tr4ShineMember).find('.td-4').text(round(pcShineMemberComeBack, 2) + "%");
                SetToolTip($(tr4ShineMember).find('.td-4'), 'Khách quay lại / Lượt khách * 100');
                $(tr4ShineMember).find('.td-6').text(ConfigInDays(pcShineMemberComeBack, pcShineMemberComeBackCondition));
                $(tr4ShineMember).find('.td-6').css('background-color', ColorConfigInDays(pcShineMemberComeBack, pcShineMemberComeBackCondition));
                $(tr4ShineMember).find('.td-6').prepend(IconCondition(pcShineMemberComeBack, pcShineMemberComeBackCondition));

                var DTTBKhachSM = ShineMember.totalIncomePerShineMember / (ShineMember.billNewCustomer + ShineMember.billOldCustomer + ShineMember.shineMemberComeBack);
                var DTTBKhachSMCondition = ShineMemberCondition.totalIncomePerShineMember / (ShineMemberCondition.billNewCustomer + ShineMemberCondition.billOldCustomer + ShineMemberCondition.shineMemberComeBack);
                $(tr5ShineMember).find('.td-3').text(addCommas(round(DTTBKhachSM, 2)));
                SetToolTip($(tr5ShineMember).find('.td-3'), 'Tổng DT shinemember / (Tổng SM mới + Tổng SM cũ + tổng SM quay lại)');
                $(tr5ShineMember).find('.td-6').text(ConfigInDays(DTTBKhachSM, DTTBKhachSMCondition));
                $(tr5ShineMember).find('.td-6').css('background-color', ColorConfigInDays(DTTBKhachSM, DTTBKhachSMCondition));
                $(tr5ShineMember).find('.td-6').prepend(IconCondition(DTTBKhachSM, DTTBKhachSMCondition));

                $(tr6ShineMember).find('.td-1').text(ConfigInDays(totalShine, totalShineCondition));
                $(tr6ShineMember).find('.td-1').css('background-color', ColorConfigInDays(totalShine, totalShineCondition))
                $(tr6ShineMember).find('.td-1').prepend(IconCondition(totalShine, totalShineCondition));
                /*Ket thuc bind shinemember*/
            }
            function BindDataRevenueToday() {
                event.preventDefault();
                startLoading();
                ResetData();
                $('.overlay').removeClass('d-none');
                var table = $('.content table[data-id=revenue]');
                SetDefaultDate();
                //fromDateCondition = $('input[name=fromDateCondition]').val();
                //toDateCondition = $('input[name=toDateCondition]').val();
                var toDay = moment().format('DD/MM/YYYY');
                salonId = ~~$('select[data-name=salonId]').val();
                salonRegionId = ~~$('select[data-name=regionId]').val();
                startLoading();
                var api = '/api/operation-report-v3/revenue';
                // get querry
                var querry = '?salonId=' + salonId + '&regionId=' + salonRegionId + '&dateFrom=' + encodeURIComponent(toDay) + '&dateTo=' + encodeURIComponent(toDay);
                //var querryCondition = '?salonId=' + salonId + '&regionId=' + salonRegionId + '&dateFrom=' + encodeURIComponent(fromDateCondition) + '&dateTo=' + encodeURIComponent(toDateCondition);

                $.ajax({
                    type: "GET",
                    async: true,
                    url: domain + api + querry,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (response, textStatus, jqXHR) {
                        if (response) {
                            // off overlay.
                            dataRevenueToday = response;
                            console.log(dataRevenueToday);
                            BindData(dataRevenueToday, null, null);

                            table.parent().find('.overlay').removeClass('d-none');
                            table.parent().find('.overlay').addClass('d-none');

                        }
                        finishLoading();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        if (jqXHR.status === 400) { return; }
                        if (jqXHR.status != 200) { ShowMessage("Cảnh báo", 'Đã có lỗi xảy ra' + " <br>- status:" + jqXHR.status + ',' + textStatus + '<br>- ' + errorThrown, 3, 7000); }
                        finishLoading();
                    }
                });

            }
            function BindLngToday() {
                event.preventDefault();
                //startLoading();
                ShowMessage('', 'Chưa có dữ liệu', 3, 5000);
                //if (dataRevenueToday == null) {
                //    ShowMessage('', 'Vui lòng xem doanh thu trước',3,5000);
                //    return;
                //}
                //var table = $('.content table[data-id=lng]');
                //SetDefaultDate();
                ////fromDateCondition = $('input[name=fromDateCondition]').val();
                ////toDateCondition = $('input[name=toDateCondition]').val();
                //var toDay = moment().format('DD/MM/YYYY');
                //salonId = ~~$('select[data-name=salonId]').val();
                //salonRegionId = ~~$('select[data-name=regionId]').val();
                //startLoading();
                //var api = '/api/operation-report-v3';
                //// get querry
                //var querry = '?salonId=' + salonId + '&regionId=' + salonRegionId + '&dateFrom=' + encodeURIComponent(toDay) + '&dateTo=' + encodeURIComponent(toDay);
                ////var querryCondition = '?salonId=' + salonId + '&regionId=' + salonRegionId + '&dateFrom=' + encodeURIComponent(fromDateCondition) + '&dateTo=' + encodeURIComponent(toDateCondition);
                //$.ajax({
                //    type: "GET",
                //    async: true,
                //    url: domain + api + querry,
                //    dataType: "json",
                //    contentType: "application/json; charset=utf-8",
                //    success: function (data, textStatus, jqXHR) {
                //        if (data) {
                //            debugger;
                //            // off overlay.
                //            table.parent().find('.overlay').removeClass('d-none');
                //            table.parent().find('.overlay').addClass('d-none');
                //        }
                //        finishLoading();
                //    },
                //    error: function (jqXHR, textStatus, errorThrown) {
                //        if (jqXHR.status === 400) { return; }
                //        if (jqXHR.status != 200) { ShowMessage("Cảnh báo", 'Đã có lỗi xảy ra' + " <br>- status:" + jqXHR.status + ',' + textStatus + '<br>- ' + errorThrown, 3, 7000); }
                //        finishLoading();
                //    }
                //});
            }
            function BindCustomerToday() {
                event.preventDefault();
                startLoading();
                if (dataRevenueToday == null) {
                    ShowMessage('', 'Vui lòng xem doanh thu trước', 3, 5000);
                    return;
                }
                var table = $('.content table[data-id=customer]');
                SetDefaultDate();
                var api = '/api/operation-report-v3/customer-info';
                //fromDateCondition = $('input[name=fromDateCondition]').val();
                //toDateCondition = $('input[name=toDateCondition]').val();
                var toDay = moment().format('DD/MM/YYYY');
                salonId = ~~$('select[data-name=salonId]').val();
                salonRegionId = ~~$('select[data-name=regionId]').val();
                // get querry
                var querry = '?salonId=' + salonId + '&regionId=' + salonRegionId + '&dateFrom=' + encodeURIComponent(toDay) + '&dateTo=' + encodeURIComponent(toDay);
                //var querryCondition = '?salonId=' + salonId + '&regionId=' + salonRegionId + '&dateFrom=' + encodeURIComponent(fromDateCondition) + '&dateTo=' + encodeURIComponent(toDateCondition);
                $.ajax({
                    type: "GET",
                    async: true,
                    url: domain + api + querry,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data, textStatus, jqXHR) {
                        if (data) {
                            // off overlay.

                            $(dataRevenueToday).each(function (index, value) {
                                var salon = null;
                                $(data).each(function (index1, value1) {
                                    if (value1.salonId === value.salonId) {
                                        salon = value1;
                                    }
                                });
                                if (salon) {
                                    value.customer = salon.customer;
                                }
                                return value;
                            });
                            console.log(dataRevenueToday);
                            BindData(dataRevenueToday, null, null);

                            table.parent().find('.overlay').removeClass('d-none');
                            table.parent().find('.overlay').addClass('d-none');
                        }
                        finishLoading();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        if (jqXHR.status === 400) { return; }
                        if (jqXHR.status != 200) { ShowMessage("Cảnh báo", 'Đã có lỗi xảy ra' + " <br>- status:" + jqXHR.status + ',' + textStatus + '<br>- ' + errorThrown, 3, 7000); }
                        finishLoading();
                    }
                });
            }
            function BindProductivityToday() {
                event.preventDefault();
                startLoading();
                if (dataRevenueToday == null) {
                    ShowMessage('', 'Vui lòng xem doanh thu trước', 3, 5000);
                    return;
                }
                var table = $('.content table[data-id=productivity]');
                SetDefaultDate();
                var api = '/api/operation-report-v3/productivity';
                //fromDateCondition = $('input[name=fromDateCondition]').val();
                //toDateCondition = $('input[name=toDateCondition]').val();
                var toDay = moment().format('DD/MM/YYYY');
                salonId = ~~$('select[data-name=salonId]').val();
                salonRegionId = ~~$('select[data-name=regionId]').val();
                // get querry
                var querry = '?salonId=' + salonId + '&regionId=' + salonRegionId + '&dateFrom=' + encodeURIComponent(toDay) + '&dateTo=' + encodeURIComponent(toDay);
                //var querryCondition = '?salonId=' + salonId + '&regionId=' + salonRegionId + '&dateFrom=' + encodeURIComponent(fromDateCondition) + '&dateTo=' + encodeURIComponent(toDateCondition);
                $.ajax({
                    type: "GET",
                    async: true,
                    url: domain + api + querry,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data, textStatus, jqXHR) {
                        if (data) {
                            // off overlay.
                            $(dataRevenueToday).each(function (index, value) {
                                var salon = null;
                                $(data).each(function (index1, value1) {
                                    if (value1.salonId === value.salonId) {
                                        salon = value1;
                                    }
                                });
                                if (salon) {
                                    value.productivity = salon.productivity;
                                }
                                return value;
                            });
                            console.log(dataRevenueToday);
                            BindData(dataRevenueToday, null, null);
                            table.parent().find('.overlay').removeClass('d-none');
                            table.parent().find('.overlay').addClass('d-none');
                        }
                        finishLoading();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        if (jqXHR.status === 400) { return; }
                        if (jqXHR.status != 200) { ShowMessage("Cảnh báo", 'Đã có lỗi xảy ra' + " <br>- status:" + jqXHR.status + ',' + textStatus + '<br>- ' + errorThrown, 3, 7000); }
                        finishLoading();
                    }
                });
            }
            function BindQualityToday() {
                event.preventDefault();
                startLoading();
                if (dataRevenueToday == null) {
                    ShowMessage('', 'Vui lòng xem doanh thu trước', 3, 5000);
                    return;
                }
                var table = $('.content table[data-id=quality]');
                SetDefaultDate();
                var api = '/api/operation-report-v3/quality';
                //fromDateCondition = $('input[name=fromDateCondition]').val();
                //toDateCondition = $('input[name=toDateCondition]').val();
                var toDay = moment().format('DD/MM/YYYY');
                salonId = ~~$('select[data-name=salonId]').val();
                salonRegionId = ~~$('select[data-name=regionId]').val();
                // get querry
                var querry = '?salonId=' + salonId + '&regionId=' + salonRegionId + '&dateFrom=' + encodeURIComponent(toDay) + '&dateTo=' + encodeURIComponent(toDay);
                //var querryCondition = '?salonId=' + salonId + '&regionId=' + salonRegionId + '&dateFrom=' + encodeURIComponent(fromDateCondition) + '&dateTo=' + encodeURIComponent(toDateCondition);
                $.ajax({
                    type: "GET",
                    async: true,
                    url: domain + api + querry,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data, textStatus, jqXHR) {
                        if (data) {
                            $(dataRevenueToday).each(function (index, value) {
                                var salon = null;
                                $(data).each(function (index1, value1) {
                                    if (value1.salonId === value.salonId) {
                                        salon = value1;
                                    }
                                });
                                if (salon) {
                                    value.quality = salon.quality;
                                }
                                return value;
                            });
                            console.log(dataRevenueToday);
                            BindData(dataRevenueToday, null, null);

                            table.parent().find('.overlay').removeClass('d-none');
                            table.parent().find('.overlay').addClass('d-none');
                        }
                        finishLoading();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        if (jqXHR.status === 400) { return; }
                        if (jqXHR.status != 200) { ShowMessage("Cảnh báo", 'Đã có lỗi xảy ra' + " <br>- status:" + jqXHR.status + ',' + textStatus + '<br>- ' + errorThrown, 3, 7000); }
                        finishLoading();
                    }
                });
            }
            function BindShineMemberToday() {
                event.preventDefault();
                startLoading();
                if (dataRevenueToday == null) {
                    ShowMessage('', 'Vui lòng xem doanh thu trước', 3, 5000);
                    return;
                }
                var table = $('.content table[data-id=shineMember]');
                SetDefaultDate();
                var api = '/api/operation-report-v3/shine-member';
                //fromDateCondition = $('input[name=fromDateCondition]').val();
                //toDateCondition = $('input[name=toDateCondition]').val();
                var toDay = moment().format('DD/MM/YYYY');
                salonId = ~~$('select[data-name=salonId]').val();
                salonRegionId = ~~$('select[data-name=regionId]').val();
                // get querry
                var querry = '?salonId=' + salonId + '&regionId=' + salonRegionId + '&dateFrom=' + encodeURIComponent(toDay) + '&dateTo=' + encodeURIComponent(toDay);
                //var querryCondition = '?salonId=' + salonId + '&regionId=' + salonRegionId + '&dateFrom=' + encodeURIComponent(fromDateCondition) + '&dateTo=' + encodeURIComponent(toDateCondition);
                $.ajax({
                    type: "GET",
                    async: true,
                    url: domain + api + querry,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data, textStatus, jqXHR) {
                        if (data) {
                            // off overlay.
                            $(dataRevenueToday).each(function (index, value) {
                                var salon = null;
                                $(data).each(function (index1, value1) {
                                    if (value1.salonId === value.salonId) {
                                        salon = value1;
                                    }
                                });
                                if (salon) {
                                    value.shineMember = salon.shineMember;
                                }
                                return value;
                            });
                            console.log(dataRevenueToday);
                            BindData(dataRevenueToday, null, null);
                            table.parent().find('.overlay').removeClass('d-none');
                            table.parent().find('.overlay').addClass('d-none');
                        }
                        finishLoading();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        if (jqXHR.status === 400) { return; }
                        if (jqXHR.status != 200) { ShowMessage("Cảnh báo", 'Đã có lỗi xảy ra' + " <br>- status:" + jqXHR.status + ',' + textStatus + '<br>- ' + errorThrown, 3, 7000); }
                        finishLoading();
                    }
                });
            }
            function BindRegion() {

                $.ajax({
                    type: "GET",
                    url: domain + "/api/operation-report-v3/list-asm",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data, textStatus, jqXHR) {
                        if (data) {
                            var option = '<option value="0">Chọn vùng asm</option>';
                            if (!perm_ViewAllData) {
                                $(data).each(function (index, value) {
                                    if (value.id === staffId) {
                                        option += '<option value="' + value.id + '">' + value.fullName + '</option>';
                                    }
                                });
                            }
                            else {
                                $(data).each(function (index, value) {
                                    option += '<option value="' + value.id + '">' + value.fullName + '</option>';
                                });
                            }
                            $('select[data-name=regionId]').append(option);
                            $('.select').select2();
                        }

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        if (jqXHR.status === 400) {
                            return;
                        }
                        if (jqXHR.status != 200) {
                            let strError = 'Đã có lỗi xảy ra vui lòng liên hệ nhóm phát triển';
                            ShowMessage("Cảnh báo", strError + " <br>- status:" + jqXHR.status + ',' + textStatus + '<br>- ' + errorThrown, 3, 7000);
                        }
                        finishLoading();
                    }
                });
            }
            function ConfigRevenue(listConfig, IdConfig) {
                if (typeof listConfig === "undefined") {
                    return undefined;
                }
                var valueConfig = null;
                $(listConfig).each(function (index, value) {
                    if (value.id === IdConfig) {
                        valueConfig = value.value;
                    }
                });
                return valueConfig;
            }
            /*
            * A: giá trị kỳ A
            * B: Giá trị kì B
            * Nếu A >= B --> giá trị = (A-B)/B			
            * Nếu A < B --> giá trị = (B-A)/B			
            * Nếu B = 0, để trống
            * So sánh cùng kỳ theo ngày.
            */
            function ColorConfigInDays(valueA, valueB) {
                if (CheckExits(valueA) && CheckExits(valueB)) {
                    if (valueA < valueB && valueB !== 0) {
                        return "#F3AF86"
                    }
                    else if (valueA > valueB && valueB !== 0) {
                        return "#A9D08F";
                    }
                    else {
                        return "";
                    }
                }
                else {
                    return "";
                }
            }
            function ConfigInDays(valueA, valueB) {
                if (CheckExits(valueA) && CheckExits(valueB) && valueB != 0) {
                    if (valueA >= valueB) {
                        var value = round((valueA - valueB) / valueB * 100, 2);
                        return value === 0 ? "" : (value + "%");
                    }
                    else if (valueA < valueB) {
                        var value = round((valueB - valueA) / valueB * 100, 2);
                        return value === 0 ? "" : (value + "%");
                    }
                }
                return "";

            }
            /*
            * So sanh Sale Target; mục tiêu kinh doanh
            */
            function ColorTarget(value, valueConfig) {
                if (CheckExits(value) && CheckExits(valueConfig) && valueConfig != 0) {
                    var pecent = round(value / valueConfig * 100, 2);
                    if (pecent >= 100) {
                        return "#cc66ff";
                    }
                    else if (pecent >= 85 && pecent < 100) {
                        return "#93D057";
                    }
                    else if (pecent >= 70 && pecent < 85) {
                        return "#ffff80";
                    }
                    else if (pecent < 70) {
                        return "#ff4e50";
                    }
                }
                else {
                    return "";
                }
            }
            function ValueRevenueCondition(categoryId, listCondition) {
                var income = null;
                if (listCondition) {
                    $(listCondition).each(function (index, value) {
                        if (value.categoryId == categoryId) {
                            income = value.income
                        }
                    });
                }
                return income;
            }
            function IconCondition(valueA, valueB) {
                if (CheckExits(valueA) && CheckExits(valueB)) {
                    if (valueA < valueB && valueB != 0) {
                        return '<i class="fas fa-arrow-down"></i>';
                    }
                    else if (valueA > valueB && valueB != 0) {
                        return '<i class="fas fa-arrow-up"></i>';
                    }
                    else {
                        return "";
                    }
                }
                else {
                    return "";
                }


            }
            function CheckExits(value) {
                if (value !== undefined &&
                    value !== null &&
                    !isNaN(value)) {
                    return true;
                }
                else {
                    return false;
                }
            }
            function SetDefaultDate() {
                var date = moment().format('DD/MM/YYYY');
                $('input[name=fromDate]').val(date);
                $('input[name=toDate]').val(date);
                $('input[name=fromDateCondition]').val("");
                $('input[name=toDateCondition]').val("");
            }
            function ChangeSalon(event) {
                ResetData();
                if (event.target.value != "0") {
                    $('select[data-name=regionId]').attr('disabled', 'disabled');
                }
                else {
                    $('select[data-name=regionId]').removeAttr('disabled');
                }
                $('select[data-name=regionId]').val(0);
                $('.select').select2();
            }
            function ChangeRegion(event) {
                ResetData();
                if (event.target.value != "0") {
                    $('select[data-name=salonId]').attr('disabled', 'disabled');
                }
                else {
                    $('select[data-name=salonId]').removeAttr('disabled');
                }
                $('select[data-name=salonId]').val(0);
                $('.select').select2();
            }
            function ClickBeforeYesterday() {

                GetData($('span.beforeYesterday').text());
            }
            function ClickYesterday() {
                GetData($('span.yesterday').text());

            }
            function ResetData() {
                dataRevenueToday = null;
                listObjDetail = [];
                $('.container.content').empty();
                var content = $('.container.content-temp >div.row');
                $('.container.content').append(content.clone());
            }
            function SetToolTip(element, title) {
                element.attr('data-toggle', 'tooltip');
                element.attr('title', title);
                element.css('cursor', 'cell');
            }

            function OpenDetail(t) {
                event.preventDefault();
                var elementTable = t.parent().parent().parent().find('.table-detail');
                var isNone = elementTable.hasClass('d-none');
                if (!isNone) {
                    if (table) {
                        $('#table-listing').DataTable().clear();
                        $('#table-listing').DataTable().destroy();
                    }
                    t.text('Hiện chi tiết');
                    elementTable.addClass('d-none');
                }
                else {
                    if (listObjDetail.length > 0) {
                        //Bind vao data table
                        t.text('Ẩn chi tiết');
                        elementTable.removeClass('d-none');
                        table = $('#table-listing').DataTable(
                            {
                                data: listObjDetail,
                                columns: [
                                    { "data": 'shortName' },//Salon Short name
                                    { "data": 'khach' },//khach
                                    { "data": 'shineCommbo' },//ShineCombo
                                    { "data": 'kidCombo' },//KidCombo
                                    { "data": 'duong' },//Duong
                                    { "data": 'uon' },//Uon
                                    { "data": 'nhuom' },//Nhuom
                                    { "data": 'skinShine' },//Skin Shine
                                    { "data": 'coolShine' },//Cool Shine
                                    { "data": 'layMun' },//Lay mun
                                    { "data": 'uonATS' }, // uon ATS
                                    { "data": 'nhuomEchosline' },// nhuom Echosline
                                    { "data":'nhuomDuong'}, // nhuom Duong
                                    { "data": 'tongDT' },//Tong DT
                                    { "data": 'dtDV' },// DT DV
                                    { "data": 'dtMP' },// DT MP
                                    { "data": 'dtSM' },// DT SM
                                    { "data": 'dvKhach' },// DT DV/Khach
                                    { "data": 'mpKhach' },// DT MP/Khach
                                    { "data": 'smKhach' },// DT SM/Khach
                                    { "data": 'stylist' },// stylist
                                    { "data": 'skinner' },// Skinner
                                    { "data": 'diemTNDV' },// Diem TNDV
                                    { "data": 'ratingTB' },// %Rating
                                    { "data": 'percentKhachCu' },// %Khach cu
                                    { "data": 'PecentSMQuayLai' },// %SM quay lai
                                    { "data": 'percentKhachCho' },// %Khach cho
                                    { "data": 'percentKhongAnh' },// %Khong anh
                                    { "data": 'percentDatTruoc' },// %Dat truoc
                                    { "data": 'percentHuyLich' },// %Huy lich
                                ],
                                paging: false,
                                searching: false,
                                info: false,
                                fixedHeader: true,
                                ordering: false,
                                fixedColumns: {
                                    leftColumns: 1
                                },
                                scrollY: "80vh",
                                scrollX: true,
                                scrollCollapse: true,
                            });
                    }
                    $('html,body').animate({
                        scrollTop: $("#table-listing").offset().top
                    },
                        'slow');
                }

            }
        </script>
    </asp:Panel>
</asp:Content>
