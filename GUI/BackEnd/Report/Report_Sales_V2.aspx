﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="~/GUI/BackEnd/Report/Report_Sales_V2.aspx.cs" Inherits="_30shine.GUI.BackEnd.Report.Report_Sales_V2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
    <link href="/Assets/css/erp.30shine.com/fixStyleCommon.css" rel="stylesheet" />
    <script src="/Assets/js/erp.30shine.com/jsReport/jquery.Report.Operation.V2.js?v=29181823"></script>
</asp:Content>
<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
    <link href="/Assets/js/select2/select2.min.css" rel="stylesheet" />
    <script src="/Assets/js/select2/select2.min.js"></script>
    <script>
        $('.select').select2();
    </script>
    <style>
        .p-name-body-report span {
            float: right;
            padding-right: 5px;
        }

        .blue {
            background: #00ff00;
        }

        .warning {
            background: #ffff00;
        }

        .danger {
            background: #db0000;
        }


        #totalReport {
            display: none;
        }

        .wp-content {
            width: 650px;
            max-width: 100%;
            margin: 0 auto;
        }

        .wp-report {
            width: 100%;
            float: left;
        }

        .report {
            width: 50%;
            float: left;
            border: 1px solid #aaa;
            padding-bottom: 20px;
            height: 200px;
        }

        .header-report {
            width: 100%;
            float: left;
        }

        .title-report {
            float: left;
            border: 1px solid #aaa;
            font-family: Roboto Condensed Bold;
            padding: 6px 5px 3px 5px;
            width: 40%;
            /*background-repeat: no-repeat;
                background-size: 23px;
                background-position: 99px 3px;*/
            position: relative;
        }

        .img-mockup {
            position: absolute;
            right: 3px;
            width: 23px;
            top: 3px;
        }

        .title-doanhthu {
            /*background-image: url(/Assets/images/Report/Doanhthu.png);*/
            background-color: #ffff00;
            /*background-position: 123px 3px;*/
        }

        .title-khach {
            /*background-image: url(/Assets/images/Report/Khach.png);*/
            background-color: #5b9bd5;
            /*background-position: 115px 3px;*/
        }

        .title-nangxuat {
            /*background-image: url(/Assets/images/Report/Nangsuat.png);*/
            background-color: #ffd966;
            /*background-position: 115px 3px;*/
        }

        .title-chatluong {
            /*background-image: url(/Assets/images/Report/Chatluong.png);*/
            background-color: #bdd7ee;
            /*background-position: 125px 3px;*/
        }

        .title-nhansu {
            /*background-image: url(/Assets/images/Report/Nhansu.png);*/
            background-color: #c55a11;
        }

        .title-booking {
            /*background-image: url(/Assets/images/Report/Booking.png);*/
            background-color: #50b347;
        }

        .body-report {
            width: 100%;
            float: left;
        }

        .body-lef {
            width: 33%;
            float: left;
            padding: 0px 5px 0px 15px;
            padding-top: 40px;
        }

        .value-hight {
            width: 100%;
            float: left;
            font-family: Roboto Condensed Bold;
            font-size: 26px;
        }

        .des-value {
            width: 100%;
            float: left;
        }

        .p-name-body-report {
            width: 58%;
            float: left;
            text-align: right;
            padding: 0 0 0 5px;
            font-family: Roboto Condensed Regular;
        }

        .body-right {
            width: 67%;
            float: left;
        }

        .row-content {
            width: 100%;
            float: left;
            padding: 0px 5px;
            margin-bottom: 10px;
        }

        .name-body-right {
            width: 60%;
            float: left;
            padding: 0 5px;
        }

        .value-body-right {
            width: 100%;
            float: left;
        }

        .p-value-body-right {
            /*width: 42%;*/
            float: left;
            font-family: Roboto Condensed Regular;
        }

        .select2-container {
            width: 190px !important;
            margin-top: 5px !important;
        }

        .select2-container--default .select2-selection--single {
            height: 32px !important;
            padding-top: 1px !important;
        }

        .fix-fullwidth .p-name-body-report {
            width: 52%;
        }

        .div-more {
            width: 100%;
            float: left;
            text-align: center;
            margin: 30px 0px;
        }

        .st-head.btn-viewdata.viewDetail {
            float: none;
            padding: 8px 20px;
            cursor: pointer;
        }

        #table-detail {
            display: none;
        }

        .wp-table {
            width: 100%;
            margin: 0 auto;
            padding: 0 30px;
        }

        .ctn-table {
            width: 100%;
            float: left;
        }

        @media only screen and (max-width: 768px) {
            .wp-table {
                width: 100%;
                padding: 0;
            }

            .wp-content {
                width: 100%;
            }

            .report {
                height: 165px;
                padding-bottom: 0px;
            }

            .row-content {
                margin-bottom: 5px;
            }

            .body-lef {
                padding-top: 20px;
            }
        }

        @media only screen and (max-width: 600px) {
            .report {
                width: 100%;
                height: inherit;
                padding-bottom: 10px;
            }



            .p-name-body-report {
                width: 55%;
            }

            .p-value-body-right {
                width: 45%;
            }
        }
    </style>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            // Add active menu
            $("#glbAdminSales").addClass("active");
            $("#glbAdminReportSales").addClass("active");
            $("li.be-report-li").addClass("active");

            // Price format
            UP_FormatPrice('.be-report-price');

            //============================
            // Datepicker
            //============================
            $('.txtDateTime').datetimepicker({
                dayOfWeekStart: 1,
                lang: 'vi',
                startDate: '2014/10/10',
                format: 'd/m/Y',
                dateonly: true,
                showHour: false,
                showMinute: false,
                timepicker: false,
                onChangeDateTime: function (dp, $input) {
                    //$input.val($input.val().split(' ')[0]);
                }
            });

            // View data today
            //$(".tag-date-today").click();
        });

        //function SubTableFixHeight() {
        //    $("tbody table.sub-table").each(function () {
        //        $(this).height($(this).parent().height() + 1);
        //    });
        //}

            //function viewDataByDate(This, time) {
            //    $(".tag-date.active").removeClass("active");
            //    This.addClass("active");
            //    $("#TxtDateTimeTo").val("");
            //    $("#TxtDateTimeFrom").val(time);
            //    $("#ViewData").click();
            //}



            //var sDate = "26/5/2016";
            //var date = Date.parse(sDate, "yyyy-MM-dd");
            //console.log(date);
    </script>
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>

    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <%--<uc1:ReportMenu runat="server" id="ReportMenu" />--%>
        <style>
            .table-wp table thead th {
                font-weight: normal;
                font-family: Roboto Condensed Bold;
            }
        </style>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Báo cáo &nbsp;&#187; </li>
                        <li class="be-report-li"><a href="javascript:void(0);"><i class="fa fa-th-large"></i>Báo cáo - Vận hành</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing be-report" onload="abc()">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <!-- Filter -->
                <div class="row">
                    <div class="filter-item" style="margin-left: 0;">
                        <strong class="st-head" style="margin-right: 10px;"><i class="fa fa-clock-o"></i>Thời gian</strong>
                        <div class="datepicker-wp">
                            <asp:TextBox CssClass="txtDateTime st-head" ID="TxtDateTimeFrom" placeholder="Từ ngày"
                                ClientIDMode="Static" runat="server" autocomplete="off"></asp:TextBox>

                            <p class="st-head" style="margin-left: 10px; margin-right: 10px;">
                                <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                            </p>
                            <asp:TextBox CssClass="txtDateTime st-head" ID="TxtDateTimeTo" placeholder="Đến ngày"
                                ClientIDMode="Static" runat="server" autocomplete="off"></asp:TextBox>
                        </div>
                        <% if (Perm_ViewAllDate)
                            { %>
                        <%--<br />--%>
                        <% }
                            else
                            { %>
                        <style>
                            .datepicker-wp {
                                display: none;
                            }

                            .customer-listing .tag-wp {
                                padding-left: 0;
                                padding-top: 15px;
                            }
                        </style>
                        <% } %>
                        <div class="filter-item">
                            <asp:DropDownList ID="Salon" CssClass="select form-control" runat="server" ClientIDMode="Static" Style="width: 190px;"></asp:DropDownList>
                        </div>
                        <p class="st-head btn-viewdata" data-type="2" onclick="viewDataByDate($(this))">Xem dữ liệu</p>
                        <div class="tag-wp">
                            <p class="tag tag-date" style="margin: 5px" data-time="<%=Day1 %>" data-type="1" onclick="viewDataByDate($(this))">Hôm kia</p>
                            <p class="tag tag-date" style="margin: 5px" data-time="<%=Day2 %>" data-type="1" onclick="viewDataByDate($(this))">Hôm qua</p>
                            <p class="tag tag-date" style="margin: 5px" data-time="<%=Day3 %>" data-type="0" onclick="viewDataByDate($(this))">Hôm nay</p>
                        </div>
                    </div>

                    <!-- End Filter -->
                    <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
                    <asp:UpdatePanel ID="UPTotal" runat="server" ClientIDMode="Static" style="margin-top: 15px;">
                        <ContentTemplate>
                            <div class="wp-content">
                                <div class="wp-report">
                                    <div class="content-report" id="totalReport">
                                        <div class="report ">
                                            <div class="header-report">
                                                <p class="title-report title-doanhthu">
                                                    DOANH THU
                                                 <img class="img-mockup" src="/Assets/images/Report/Doanhthu.png" />
                                                </p>
                                            </div>
                                            <div class="body-report">
                                                <div class="body-lef">
                                                    <p class="value-hight" id="totalIncome">
                                                    </p>
                                                    <p class="des-value">
                                                        triệu đồng
                                                    </p>
                                                </div>
                                                <div class="body-right">
                                                    <div class="row-content">
                                                        <p class="p-name-body-report">
                                                            <span>Dịch vụ: </span>
                                                        </p>
                                                        <p class="p-value-body-right" id="totalServiceIncome">
                                                        </p>
                                                    </div>
                                                    <div class="row-content">
                                                        <p class="p-name-body-report">
                                                            <span>Mỹ phẩm: </span>
                                                        </p>
                                                        <p class="p-value-body-right" id="totalProductIncome">
                                                        </p>
                                                    </div>
                                                    <div class="row-content">

                                                        <p class="p-name-body-report">
                                                            <span id="span-totalIncomePercentCus">DT/khách: </span>
                                                        </p>
                                                        <p class="p-value-body-right" id="totalIncomePercentCus">
                                                        </p>
                                                    </div>
                                                    <div class="row-content">
                                                        <p class="p-name-body-report">
                                                            <span id="span-totalServiceIncomePercentCus">DTDV/khách: </span>
                                                        </p>
                                                        <p class="p-value-body-right" id="totalServiceIncomePercentCus">
                                                        </p>
                                                    </div>
                                                    <div class="row-content">
                                                        <p class="p-name-body-report">
                                                            <span id="span-totalProductIncomePercentCus">DTMP/khách:
                                                            </span>
                                                        </p>
                                                        <p class="p-value-body-right" id="totalProductIncomePercentCus">
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="report">
                                            <div class="header-report">
                                                <p class="title-report title-khach">
                                                    <span>KHÁCH</span>
                                                    <img class="img-mockup" src="/Assets/images/Report/Khach.png" />
                                                </p>
                                            </div>
                                            <div class="body-report">
                                                <div class="body-lef">
                                                    <p class="value-hight" id="numberOfTurns">
                                                    </p>
                                                    <p class="des-value">
                                                        <span>Khách </span>
                                                    </p>
                                                </div>
                                                <div class="body-right" id="ServiceAuto">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="report">
                                            <div class="header-report">
                                                <p class="title-report title-nangxuat">
                                                    <span>NĂNG SUẤT </span>
                                                    <img class="img-mockup" src="/Assets/images/Report/Nangsuat.png" />
                                                </p>
                                            </div>
                                            <div class="body-report">
                                                <div class="body-lef">
                                                    <p class="value-hight" id="productivity">
                                                    </p>
                                                    <p class="des-value">
                                                        nghìn/stylist /h
                                                    </p>
                                                </div>
                                                <div class="body-right">
                                                    <div class="row-content">
                                                        <p class="p-name-body-report">
                                                            <span>DT/St/day: </span>
                                                        </p>
                                                        <p class="p-value-body-right" id="DT-St-day">
                                                        </p>
                                                    </div>
                                                    <div class="row-content">
                                                        <p class="p-name-body-report">
                                                            <span>DT/Sl/day: </span>
                                                        </p>
                                                        <p class="p-value-body-right" id="DT-Sl-day">
                                                        </p>
                                                    </div>
                                                    <div class="row-content">
                                                        <p class="p-name-body-report">
                                                            <span>Khách/St/h: </span>
                                                        </p>
                                                        <p class="p-value-body-right" id="Khach-St-h">
                                                        </p>
                                                    </div>
                                                    <div class="row-content">
                                                        <p class="p-name-body-report">
                                                            <span>Khách/St/day: </span>
                                                        </p>
                                                        <p class="p-value-body-right" id="Khach-St-day">
                                                        </p>
                                                    </div>
                                                    <div class="row-content">
                                                        <p class="p-name-body-report">
                                                            <span>Khách/Sl/day: </span>
                                                        </p>
                                                        <p class="p-value-body-right" id="Khach-Sl-day">
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="report">
                                            <div class="header-report">
                                                <p class="title-report title-chatluong">
                                                    CHẤT LƯỢNG
                                                 <img class="img-mockup" src="/Assets/images/Report/Chatluong.png" />
                                                </p>
                                            </div>
                                            <div class="body-report fix-fullwidth">
                                                <div class="body-lef">
                                                    <p class="value-hight" id="sao">
                                                    </p>
                                                    <p class="des-value">
                                                        sao
                                                    </p>
                                                </div>
                                                <div class="body-right">
                                                    <div class="row-content">
                                                        <p class="p-name-body-report">
                                                            <span style="font-weight: bold; font-size: 15px;">Khách cũ:</span>
                                                        </p>
                                                        <p class="p-value-body-right" id="khach-cu" style="font-weight: bold; font-size: 15px;">
                                                        </p>
                                                    </div>
                                                    <div class="row-content">
                                                        <p class="p-name-body-report">
                                                            <span id="span-cho-lau">Chờ lâu: </span>
                                                        </p>
                                                        <p class="p-value-body-right" id="cho-lau">
                                                        </p>
                                                    </div>
                                                    <div class="row-content">
                                                        <p class="p-name-body-report">
                                                            <span id="span-khong-anh">Không ảnh: </span>
                                                        </p>
                                                        <p class="p-value-body-right" id="khong-anh">
                                                        </p>
                                                    </div>
                                                    <div class="row-content">
                                                        <p class="p-name-body-report">
                                                            <span>1-2*: </span>
                                                        </p>
                                                        <p class="p-value-body-right" id="sao1-2">
                                                        </p>
                                                    </div>
                                                    <div class="row-content">
                                                        <p class="p-name-body-report">
                                                            <span>Lỗi GS:  </span>
                                                        </p>
                                                        <p class="p-value-body-right" id="Loi-GS">
                                                        </p>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="report">
                                            <div class="header-report">
                                                <p class="title-report title-nhansu">
                                                    NHÂN SỰ
                                                <img class="img-mockup" src="/Assets/images/Report/Nhansu.png" />
                                                </p>
                                            </div>
                                            <div class="body-report">
                                                <div class="body-lef">
                                                    <p class="value-hight" id="Stylist-di-lam">
                                                    </p>
                                                    <p class="des-value" id="span-Stylist-di-lam">
                                                        Stylist đi làm
                                                    </p>
                                                </div>
                                                <div class="body-right">
                                                    <div class="row-content">
                                                        <p class="p-name-body-report">
                                                            <span>Stylist: </span>
                                                        </p>
                                                        <p class="p-value-body-right" id="Stylist">
                                                        </p>
                                                    </div>
                                                    <div class="row-content">
                                                        <p class="p-name-body-report">
                                                            <span>Skinner:</span>
                                                        </p>
                                                        <p class="p-value-body-right" id="Skinner">
                                                        </p>
                                                    </div>
                                                    <div class="row-content">
                                                        <p class="p-name-body-report">
                                                            <span>Giờ làm: </span>
                                                        </p>
                                                        <p class="p-value-body-right" id="GioLam">
                                                        </p>
                                                    </div>
                                                    <div class="row-content">
                                                        <p class="p-name-body-report">
                                                            <span>Tăng ca: </span>
                                                        </p>
                                                        <p class="p-value-body-right" id="TangCa">
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="report">
                                            <div class="header-report">
                                                <p class="title-report title-booking">
                                                    BOOKING
                                                 <img class="img-mockup" src="/Assets/images/Report/Booking.png" />
                                                </p>
                                            </div>
                                            <div class="body-report">
                                                <div class="body-lef">
                                                    <p class="value-hight" id="BookKinSlot">
                                                    </p>
                                                    <p class="des-value">
                                                        Book kín slot
                                                    </p>
                                                </div>
                                                <div class="body-right">
                                                    <div class="row-content">
                                                        <p class="p-name-body-report">
                                                            <span>Slot:</span>
                                                        </p>
                                                        <p class="p-value-body-right" id="Slot">
                                                        </p>

                                                    </div>
                                                    <div class="row-content">
                                                        <p class="p-name-body-report">
                                                            <span id="span-HuyLich">Hủy lịch:</span>
                                                        </p>
                                                        <p class="p-value-body-right" id="HuyLich">
                                                        </p>
                                                    </div>
                                                    <div class="row-content">
                                                        <p class="p-name-body-report">
                                                            <span id="span-DatTruoc">Đặt trước:</span>
                                                        </p>
                                                        <p class="p-value-body-right" id="DatTruoc">
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="div-more">
                                            <a class="viewDetail st-head btn-viewdata" id="viewDetail">Xem chi tiết</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="wp-table">
                                <div class="ctn-table" id="table-detail">
                                    <div class="row">
                                        <strong class="st-head"><i class="fa fa-file-text"></i>CHI TIẾT BÁO CÁO VẬN HÀNH THEO SALON</strong>
                                        <a href="javascript://" class="st-head btn-viewdata" onclick="ExportExcel($(this))">Xuất File Excel</a>
                                    </div>
                                    <table class="table-report" id="table-report">
                                        <thead class="th-report">
                                            <tr>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 70px; */ text-align: center;"></th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 70px; */ text-align: center;" colspan="8">Khách</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 70px; */ text-align: center;" colspan="5">Doanh thu</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 70px; */ text-align: center;" colspan="3">Nhân sự</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 70px; */ text-align: center;" colspan="4">Chất lượng</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 70px; */ text-align: center;" colspan="2">Booking</th>
                                            </tr>
                                            <tr>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 70px; */ text-align: center;"></th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 70px; */ text-align: center;">Khách
                                                </th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 70px; */ text-align: center;">Shine Combo
                                                </th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 70px; */ text-align: center;">Kid Combo
                                                </th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 70px; */ text-align: center;">Dưỡng
                                                </th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 70px; */ text-align: center;">Uốn
                                                </th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 70px; */ text-align: center;">Nhuộm
                                                </th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 70px; */ text-align: center;">Skin Shine
                                                </th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 70px; */ text-align: center;">Cool Shine
                                                </th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 70px; */ text-align: center;">Doanh thu 
                                                </th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 70px; */ text-align: center;">Dịch vụ
                                                </th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 70px; */ text-align: center;">Mỹ Phẩm
                                                </th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 70px; */ text-align: center;">DV/Khách
                                                </th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 70px; */ text-align: center;">MP/Khách
                                                </th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 70px; */ text-align: center;">Stylist
                                                </th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 70px; */ text-align: center;">Skinner
                                                </th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 70px; */ text-align: center;">Tổng giờ làm
                                                </th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 70px; */ text-align: center;">% Chất lượng
                                                </th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 70px; */ text-align: center;">% Khách cũ
                                                </th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 70px; */ text-align: center;">% Khách chờ
                                                </th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 70px; */ text-align: center;">% Không ảnh
                                                </th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 70px; */ text-align: center;">% Đặt trước
                                                </th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 70px; */ text-align: center;">% Hủy lịch
                                                </th>

                                            </tr>
                                        </thead>

                                        <tbody class="tbody-report" id="tbodyBCVH">
                                        </tbody>
                                    </table>
                                </div>

                            </div>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <%-- END Listing --%>
            </div>
        </div>
        <!-- Page loading -->
        <div class="page-loading">
            <p>Vui lòng đợi trong giây lát...</p>
        </div>

    </asp:Panel>
    <link href="/Assets/js/select2/select2.min.css" rel="stylesheet" />
    <script src="/Assets/js/select2/select2.min.js"></script>
    <script>
        $('.select').select2();
    </script>
    <style>
        .select2-container {
            width: 190px !important;
            margin-top: 5px !important;
        }

        .select2-container--default .select2-selection--single {
            height: 32px !important;
            padding-top: 1px !important;
        }
    </style>
    <script>
        $(document).ready(function () {


            //checkScreen();
            $("tbody.tbody-report").css({ "height": "" + screen.height - 250 + "" });
            $('tbody.tbody-report').scroll(function (e) {
                $('thead.th-report').css("left", -$("tbody").scrollLeft());
                $('thead.th-report th:nth-child(1)').css("left", $("tbody").scrollLeft());
                $('tbody.tbody-report td:nth-child(1)').css({ "left": $("tbody").scrollLeft() }); //,  "display" : "block", "border-bottom" : "none"
            });
        });

        $("#viewDetail").click(function () {
            var Viewdetail = $("#viewDetail");
            if (!Viewdetail.hasClass("active")) {
                console.log("true");
                $("#table-detail").css({ "display": "block" });
                Viewdetail.text("Ẩn chi tiết");
                Viewdetail.addClass("active");
                $('html,body').animate({
                    scrollTop: $("#table-detail").offset().top
                },
                    'slow');
            }
            else {
                console.log("false");
                $("#table-detail").css({ "display": "none" });
                Viewdetail.text("Xem chi tiết");
                Viewdetail.removeClass("active");
                $('html,body').animate({
                    scrollTop: $(".wp.customer-add.customer-listing.be-report").offset().top
                },
                    'slow');
            }


        });

    </script>

</asp:Content>
