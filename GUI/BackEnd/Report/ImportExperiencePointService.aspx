﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/TemplateMaster/SiteMasterVersion1.Master" CodeBehind="ImportExperiencePointService.aspx.cs" Inherits="_30shine.GUI.BackEnd.Report.ImportExperiencePointService" %>

<asp:Content ID="CustomerAdd" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel ID="ContentWrap" runat="server" ClientIDMode="Static">
        <div class="container-fluid sub-menu">
            <div class="row">
                <div class="col-md-12 pt-3 pb-2">
                    <ul class="h-100">
                        <li>
                            <label style="font-size: 16px">Báo cáo </label>
                            &nbsp;&#187;</li>
                        <li><i class="fas fa-list-alt mr-1 align-middle"></i><a class="align-middle" href="/admin/bao-cao/diem-trai-nghiem-dv.html">Trải nghiệm dịch vụ &nbsp;&#187;</a></li>
                        <li><i class="fas fa-list-alt mr-1 align-middle"></i><a class="align-middle" href="/admin/bao-cao/import-feedback-khach-hang.html">Import Feedback khách hàng</a></li>
                    </ul>
                </div>
                <div class="col-md-12">
                    <div class="sub-menu-boder"></div>
                </div>
            </div>
        </div>
        <form runat="server">
            <div class="container">
                <div class="row">
                    <div class="col-12 form-group mt-3">
                        <h2 class="text-center">IMPORT PHẢN HỒI KHÁCH HÀNG</h2>
                    </div>
                    <div class="col-12 form-group">
                        <input type="file" id="myFileInput" />
                    </div>
                    <div class="col-12 form-group">
                        <a href="/TemplateFile/ImportExcelFeedbackService/templeImportFeedback.xlsx" class="cls_tempfile" style="color: blue; text-decoration: underline;">Tải File Excel Import MẪu</a>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-12 form-group">
                        <div class="float-right">
                            - Lưu ý: 1 Salon 1 ngày chỉ có 1 bản ghi.
                            <br />
                            - Những bản ghi <b><i>không hợp lệ</i></b>, sẽ không được lưu vào cơ sở dữ liệu
                            <br />
                            <i class="fas fa-ban text-danger mr-1" title="Không hợp lệ"></i>Không hợp lệ  |  
                            <i class="fas fa-check text-success mr-1" title="Hợp lệ"></i>Hợp lệ
                        </div>
                    </div>
                    <div class="col-12 form-group">
                        <table class="table table-bordered selectable table-listing" id="table-listing">
                            <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>ID Salon</th>
                                    <th>Số lượng feedback</th>
                                    <th>Ngày (ngày/tháng/năm)</th>
                                    <th>Kiểm tra</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>

                    </div>
                    <div class="col-12 form-group">
                        <button class="btn btn-success float-right" id="save" onclick="SaveData(event)">Lưu lại</button>
                        <button class="btn btn-light float-right mr-2 ml-2" id="cancel" onclick="Cancel(event)">Hủy Bỏ</button>
                    </div>
                </div>
            </div>
        </form>
        <style>
        </style>
        <script>
            var table = null;
            var domain = '<%=Libraries.AppConstants.URL_API_REPORTS%>';
            var listDataImportSucess = [];
            $(document).ready(function () {
                $('#myFileInput').on('change', filePicked);
            });
            function filePicked(oEvent) {
                var oFile = oEvent.target.files[0];
                var reader = new FileReader();
                reader.onload = function (e) {
                    var data = e.target.result;
                    var cfb = XLSX.read(data, { type: 'binary' });
                    var XL_row_object = XLSX.utils.sheet_to_row_object_array(cfb.Sheets["Sheet1"]);
                    listDataImportSucess = [];
                    if (XL_row_object.length > 0) {
                        var bodySuccess = '';
                        var bodyError = '';
                        var errorIcon = '<i class="fas fa-ban text-danger" title="Không hợp lệ"></i>';
                        var successIcon = '<i class="fas fa-check text-success" title="Hợp lệ"></i>';
                        if (table) {
                            $('#table-listing').DataTable().clear();
                            $('#table-listing').DataTable().destroy();
                        }
                        $(XL_row_object).each(function (index, value) {
                            var d = moment(value.WorkDate === undefined ? "" : value.WorkDate, 'DD/MM/YYYY', true);
                            var isCheckDate = d.isValid();

                            var isCheckQuantity = value.FeedbackQuantity === undefined || value.FeedbackQuantity === null || value.FeedbackQuantity === "" || IsFloat(value.FeedbackQuantity)? false : true;
                            var isCheckSalon = IsValid(value.SalonId);
                            if (isCheckDate && isCheckQuantity && isCheckSalon) {
                                listDataImportSucess.push(value);
                                bodySuccess += `<tr>
                                                    <td>${index + 1}</td>
                                                    <td>${value.SalonId}</td>
                                                    <td>${value.FeedbackQuantity}</td>
                                                    <td>${value.WorkDate === undefined ? "" : value.WorkDate}</td>
                                                    <td>${successIcon}</d>
                                               </tr>`
                            }
                            else {
                                bodyError += `<tr>
                                                    <td></td>
                                                    <td>${value.SalonId}</td>
                                                    <td>${value.FeedbackQuantity}</td>
                                                    <td>${value.WorkDate === undefined ? "" : value.WorkDate}</td>
                                                    <td>${errorIcon}</d>
                                               </tr>`
                            }

                        });
                        $('#table-listing tbody').empty().append(bodyError + bodySuccess);
                        table = $('#table-listing').DataTable(
                            {
                                paging: false,
                                searching: false,
                                info: false,
                                fixedHeader: true,
                                ordering: false,
                                fixedColumns: {
                                    leftColumns: 1
                                },
                                scrollY: "700px",
                                scrollX: false,
                                scrollCollapse: true,
                            });
                    }
                    else {
                        $("#table-listing tbody").empty();
                        XL_row_object = {};
                        ShowMessage('', 'Kiểm tra lại file excel!', 4, 5000);
                    }
                };
                reader.readAsBinaryString(oFile);
            }
            function IsValid(value) {
                if (value === null ||
                    value === undefined ||
                    value === "" ||
                    value === "0" ||
                    ~~value <= 0  || 
                    IsFloat(value)){
                    return false;
                }
                else {
                    return true;
                }
            }
            function IsFloat(n) {
                n = Number(n);
                return Number(n) === n && n % 1 !== 0;
            }
            function SaveData(event) {
                event.preventDefault();
                startLoading();
                if (listDataImportSucess && listDataImportSucess.length > 0) {
                    // Lưu data;
                    $.ajax({
                        type: "POST",
                        url: domain + "/api/report-point-service",
                        data: JSON.stringify(listDataImportSucess),
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (dataCondition, textStatus, jqXHR) {
                            ShowMessage("", "Thành công", 2);
                            finishLoading();
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            if (jqXHR.status !== 200) {
                                let strError = 'Đã có lỗi xảy ra vui lòng liên hệ nhóm phát triển';
                                let message = jqXHR.responseJSON == undefined ? "" : jqXHR.responseJSON;
                                ShowMessage("Cảnh báo", strError + " <br>- status:" + jqXHR.status + ',' + textStatus + '<br>- message: ' + message + '<br>- ' + errorThrown, 3, 10000);
                            }
                            finishLoading();
                        }
                    });
                }
                else {
                    ShowMessage("", "Vui lòng chọn file excel hợp lệ", 4, 7000);
                    finishLoading();
                }
            }
            function Cancel(event) {
                $("#table-listing tbody").empty();
                listDataImportSucess = [];
                $('#myFileInput').val('');
                $('#myFileInput').text('');
                event.preventDefault();
            }
        </script>
    </asp:Panel>
</asp:Content>
