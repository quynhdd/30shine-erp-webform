﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Globalization;
using System.Web.Script.Serialization;
using ExportToExcel;
using Excel;
using LinqKit;
using System.Net.Mail;
using System.Net;
using System.Data.SqlClient;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.Report
{
    public partial class Sales : System.Web.UI.Page
    {
        protected Paging PAGING = new Paging();
        protected SingleReportData _SingleReportData = new SingleReportData();
        protected SingleReportData _SingleReportData_CL = new SingleReportData();
        protected SingleReportData _SingleReportData_KT = new SingleReportData();
        protected SingleReportData _SingleReportData_TDN = new SingleReportData();
        protected List<List<ProductBasic>> ProductList;
        private Expression<Func<BillService, bool>> Where = PredicateBuilder.True<BillService>();
        CultureInfo culture = new CultureInfo("vi-VN");

        private string PageID = "";
        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        protected bool Perm_ViewAllData = false;
        protected bool Perm_ViewAllDate = false;
        private bool Perm_Access = false;
        protected bool IsAccountant = false;
        List<_30shine.MODEL.ENTITY.EDMX.Staff> _LstNhanVien;

        protected string Day1 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-2));
        protected string Day2 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-1));
        protected string Day3 = string.Format("{0:dd/MM/yyyy}", DateTime.Now);

        private Solution_30shineEntities db;
        private DateTime timeFrom;
        private DateTime timeTo;
        private int salonId;

        protected string table = "";
        protected void checkPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //string perName = Session["User_Permission"].ToString();
                //string url = Request.Url.AbsolutePath.ToString();
                //Solution_30shineEntities db = new Solution_30shineEntities();
                //var pem = (from m in db.Tbl_Permission_Map
                //           join mn in db.Tbl_Permission_Menu on m.mID equals mn.mID
                //           where m.pName == perName && mn.Url_Main.EndsWith(url) || mn.Url_Rewrite.EndsWith(url)
                //           select new { m.aID }
                //           ).FirstOrDefault();
                //if (pem != null)
                //{
                //    var pemArray = pem.aID.Split(',');
                //    if (Array.IndexOf(pemArray, "1") > -1)
                //    {
                //        Perm_Access = true;
                //        if (Array.IndexOf(pemArray, "2") > -1)
                //        {
                //            Perm_ViewAllData = true;
                //        }
                //        if (Array.IndexOf(pemArray, "7") > -1)
                //        {
                //            Perm_ViewAllDate = true;
                //        }
                //        if (Array.IndexOf(pemArray, "4") > -1)
                //        {
                //            Perm_Edit = true;
                //        }
                //        if (Array.IndexOf(pemArray, "5") > -1)
                //        {
                //            Perm_Delete = true;
                //        }
                //    }
                //    else
                //    {
                //        ContentWrap.Visible = false;
                //        NotAllowAccess.Visible = true;
                //    }

                //}
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_ViewAllDate = permissionModel.CheckPermisionByAction("Perm_ViewAllDate", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();


            }
        }
        public Sales()
        {
            db = new Solution_30shineEntities();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            checkPermission();

            if (!IsPostBack)
            {
                Library.Function.bindSalon_NotHoiQuan(new List<DropDownList> { Salon }, Perm_ViewAllData);
            }
            else
            {
                GenWhere();

                RemoveLoading();
            }
        }

        protected void FillLstStaff()
        {
            using (var db = new Solution_30shineEntities())
            {
                _LstNhanVien = db.Staffs.ToList();
            }
        }

        private void GenWhere()
        {
            if (TxtDateTimeFrom.Text != "")
            {
                DateTime _FromDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture).AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
                DateTime _ToDate;
                if (TxtDateTimeTo.Text != "")
                {
                    _ToDate = Convert.ToDateTime(TxtDateTimeTo.Text, culture).AddDays(1);
                }
                else
                {
                    _ToDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture).AddDays(1);
                }
                Where = Where.And(w => w.CreatedDate > _FromDate && w.CreatedDate < _ToDate);
                Where = Where.And(w => w.IsDelete != 1 && w.Pending != 1);
                int SalonId = Convert.ToInt32(Salon.SelectedValue);
                if (!Perm_ViewAllData)
                {
                    Where = Where.And(p => p.SalonId == SalonId);
                }
                else
                {
                    if (SalonId > 0)
                    {
                        Where = Where.And(p => p.SalonId == SalonId);
                    }
                    else
                    {
                        Where = Where.And(p => p.SalonId > 0);
                    }
                }
            }
        }
        

        public void ReportData_Daily()
        {
            using (var db = new Solution_30shineEntities())
            {
                var item = new SingleReportData();
                SingleReportData totalReport = new SingleReportData();
                List<SingleReportData> lst = new List<SingleReportData>();
                cls_sta totalSta = new cls_sta();
                cls_doanhso_dv shinecombo = new cls_doanhso_dv();
                cls_doanhso_dv kidcombo = new cls_doanhso_dv();
                cls_doanhso_dv DuongProtein = new cls_doanhso_dv();
                cls_doanhso_dv Uon = new cls_doanhso_dv();
                cls_doanhso_dv Nhuom = new cls_doanhso_dv();
                cls_doanhso_dv Tay = new cls_doanhso_dv();
                cls_doanhso_dv Tayx2 = new cls_doanhso_dv();

                cls_sta totalStaMon = new cls_sta();
                cls_sta2 totalSta2 = new cls_sta2();
                cls_sta2 totalperBooking = new cls_sta2();
                cls_sta2 totalperImages = new cls_sta2();
                cls_skinner totalSinner = new cls_skinner();
                cls_rating rating = new cls_rating();
                cls_rating ratingTotal = new cls_rating();
                int temp;
                string sql;
                salonId = Convert.ToInt32(Salon.SelectedValue);
                string whereSalon = salonId > 0 ? "and SalonId = " + salonId : "and SalonId > 0";
                int days1 = 0;

                if (TxtDateTimeFrom.Text != "")
                {
                    timeFrom = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                    timeTo = new DateTime();
                    if (TxtDateTimeFrom.Text != "")
                    {
                        timeFrom = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                        if (!timeFrom.Equals(null) && TxtDateTimeTo.Text != "")
                        {
                            timeTo = Convert.ToDateTime(TxtDateTimeTo.Text, culture).AddDays(1);
                        }
                        else
                        {
                            timeTo = timeFrom.AddDays(1);
                        }
                    }
                    else
                    {
                        timeFrom = DateTime.Now;
                        timeTo = timeFrom.AddDays(1);
                    }
                    days1 = int.TryParse((timeTo - timeFrom).Days.ToString(), out temp) ? temp : 0;

                    /// Xử lý dữ liệu
                    /// Báo cáo tổng doanh thu
                    // Lấy salon
                    sql = @"select b.*
                            from 
                            (
                                select SalonId from BillService
                                where IsDelete != 1 and Pending != 1" +
                                whereSalon + @"
                                and CreatedDate between '" + String.Format("{0:yyyy/MM/dd}", timeFrom) + @"' and '" + String.Format("{0:yyyy/MM/dd}", timeTo) + @"'
                                group by SalonId
                            ) as a
                            inner join Tbl_Salon as b on a.SalonId = b.Id
                            join KetQuaKinhDoanh_Salon _kqkd on _kqkd.SalonId = b.Id
                            where b.IsDelete != 1
                            and _kqkd.IsDelete != 1 and b.IsSalonHoiQuan != 1
                            order by _kqkd.[Order]";
                    var Salons = db.Database.SqlQuery<Tbl_Salon>(sql).ToList();
                    if (Salons.Count > 0)
                    {
                        foreach (var salon in Salons)
                        {
                            salonId = salon.Id;
                            item = new SingleReportData();
                            item.SalonName = salon.ShortName;
                            var bills = db.BillServices.Where(w => w.IsDelete != 1 && w.Pending != 1 && w.SalonId == salon.Id && w.CreatedDate >= timeFrom && w.CreatedDate < timeTo).ToList();
                            // Tổng bills
                            //var query = "select * from BillService where IsDelete != 1 and Pending != 1 and SalonId = " + salon.Id + " and (ServiceIds is not null and ServiceIds != '') and CreatedDate >= '" + timeFrom + "' and CreatedDate < '" + timeTo + "'";
                            //var billSer = db.Database.SqlQuery<BillService>(query).ToList();
                            var billSer = db.BillServices.Where(w => w.IsDelete != 1 && w.Pending != 1 && w.SalonId == salon.Id && w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && (w.ServiceIds != null && w.ServiceIds != "")).ToList();
                            totalReport.Total_Bills += billSer.Count;
                            foreach (var total in billSer)
                            {
                                item.Total_Bills++;
                            }

                            foreach (var v in bills)
                            {
                                temp = Convert.ToInt32(v.TotalMoney);
                                item.Total_Money += temp;
                                totalReport.Total_Money += temp;
                                //item.Money_Services += Convert.ToInt32(v.ServicePrice);
                                JavaScriptSerializer serializer = new JavaScriptSerializer();

                                // Product online
                                if (v.ServiceIds == "" || v.ServiceIds == null)
                                {
                                    var ProductListThis = serializer.Deserialize<List<ProductBasic>>(v.ProductIds);
                                    if (ProductListThis != null)
                                    {
                                        foreach (var v2 in ProductListThis)
                                        {
                                            temp = v2.Promotion != 0 ? (v2.Price * v2.Quantity - v2.Promotion) : (Convert.ToInt32(v2.Price * v2.Quantity) * (100 - v2.VoucherPercent) / 100);
                                            item.Money_Products += temp;
                                            item.Money_Products_Online += temp;
                                            totalReport.Money_Products += temp;
                                            totalReport.Money_Products_Online += temp;
                                        }
                                    }
                                    item.Bills_Product++;
                                    totalReport.Bills_Product++;
                                }
                                else
                                {
                                    // Product Salon
                                    var ProductListThis = serializer.Deserialize<List<ProductBasic>>(v.ProductIds);
                                    if (ProductListThis != null)
                                    {
                                        foreach (var v2 in ProductListThis)
                                        {
                                            temp = v2.Promotion != 0 ? (v2.Price * v2.Quantity - v2.Promotion) : (Convert.ToInt32(v2.Price * v2.Quantity) * (100 - v2.VoucherPercent) / 100);
                                            item.Money_Products += temp;
                                            item.Money_Products_Salon += temp;
                                            totalReport.Money_Products += temp;
                                            totalReport.Money_Products_Salon += temp;
                                        }
                                    }

                                    // Service
                                    var ServiceListThis = serializer.Deserialize<List<ProductBasic>>(v.ServiceIds);
                                    if (ServiceListThis != null)
                                    {
                                        foreach (var v2 in ServiceListThis)
                                        {
                                            temp = Convert.ToInt32(v2.Price * v2.Quantity) * (100 - v2.VoucherPercent) / 100;
                                            item.Money_Services += temp;
                                            totalReport.Money_Services += temp;
                                        }
                                    }
                                    item.Bills_Service++;
                                    totalReport.Bills_Service++;
                                }


                                /// Tính số lần rating
                                switch (v.Mark)
                                {
                                    case 1:
                                        item.rating_bad++;
                                        totalReport.rating_bad++;
                                        item.rating_total++;
                                        totalReport.rating_total++;
                                        break;
                                    case 2:
                                        item.rating_good++;
                                        totalReport.rating_good++;
                                        item.rating_total++;
                                        totalReport.rating_total++;
                                        break;
                                    case 3:
                                        item.rating_great++;
                                        totalReport.rating_great++;
                                        item.rating_total++;
                                        totalReport.rating_total++;
                                        break;
                                    default: break;
                                }
                            }

                            /// Chỉ số thống kê
                            /// 1. sta1 : Doanh thu dịch vụ từ stylist / số stylist (đối với mỗi salon và toàn bộ salon)
                            ///     số stylist = số stylist ngày 1 + số stylist ngày 2 + ... (theo khoảng thời gian)
                            /// 2. sta2 : Số bill dịch vụ / số stylist
                            /// 3. sta3 : % khách quay lại, bill khách cũ / tổng số bill
                            /// 4. sta4 : Doanh số DV TB/ngày trong kỳ (time2 - time1)
                            /// 5. sta5 : Doanh thu DV TB/ngày trong kỳ (time2 - time1)


                            /// Tính sta1, sta2
                            var staData = getData_DoanhSoDoanhThuDV_Stylist();
                            object[] parasSta2 =
                           {
                                new SqlParameter("@timeFrom",String.Format("{0:yyyy/MM/dd}", timeFrom)),
                                new SqlParameter("@timeTo",String.Format("{0:yyyy/MM/dd}", timeTo)),
                                new SqlParameter("@salonId",salonId)
                            };
                            var skinData = db.Database.SqlQuery<cls_skinner>("sp_BC_getData_DoanhSoDoanhThuDV_Stylist @timeFrom, @timeTo, @salonId", parasSta2).FirstOrDefault();

                            // Tính số stylist
                            //sql = @"select a.Staff_Hairdresser_Id
                            //        from
                            //        (
                            //            select Staff_Hairdresser_Id
                            //                    , COUNT(*) as bills, 
                            //              DATEPART(YEAR, CreatedDate) as [year], DATEPART(MONTH, CreatedDate) as mon,DATEPART(DAY, CreatedDate) as [day]
                            //            from BillService
                            //            where IsDelete != 1 and Pending != 1
                            //            and CreatedDate between '" + String.Format("{0:yyyy/MM/dd}", timeFrom) + @"' and '" + String.Format("{0:yyyy/MM/dd}", timeTo) + @"'
                            //            and ServiceIds != '' and ServiceIds is not null
                            //            and Staff_Hairdresser_Id > 0
                            //            and SalonId = " + salon.Id + @"
                            //            group by Staff_Hairdresser_Id, DATEPART(YEAR, CreatedDate), DATEPART(MONTH, CreatedDate),DATEPART(DAY, CreatedDate)
                            //        ) as a
                            //        where a.bills >= 3";
                            //var stylist = db.Database.SqlQuery<int>(sql).ToList();
                            //staData.stylist = stylist.Count;

                            //if (staData != null)
                            //{
                            //    totalSta.bills += staData.bills;
                            //    totalSta.money += staData.money;
                            //    totalSta.stylist += staData.stylist;

                            //    if (staData.stylist > 0)
                            //    {
                            //        item.sta1 = (float)staData.money / staData.stylist;
                            //        item.sta2 = (float)staData.bills / staData.stylist;
                            //        if(days1 > 0)
                            //            item.sta6 = (float)staData.stylist / days1;
                            //    }
                            //}

                            // Stylist đi làm (theo chấm công)
                            var stylistWork = getData_Staff_Work(1);
                            staData.stylist = stylistWork.Count;

                            if (staData != null)
                            {
                                totalSta.bills += staData.bills;
                                totalSta.money += staData.money;
                                totalSta.stylist += staData.stylist;

                                if (staData.stylist > 0)
                                {
                                    item.sta1 = (float)staData.money / staData.stylist;
                                    item.sta2 = (float)staData.bills / staData.stylist;
                                    if (days1 > 0)
                                        item.stylistWork = (float)staData.stylist / days1;
                                }
                            }

                            // Skinner đi làm (theo chấm công)
                            var skinnerWork = getData_Staff_Work(2);
                            skinData.skinner = skinnerWork.Count;

                            if (skinData != null)
                            {
                                totalSinner.skinner += skinData.skinner;

                                if (skinData.skinner > 0)
                                {
                                    if (days1 > 0)
                                        item.skinnerWork = (float)skinData.skinner / days1;
                                }
                            }

                            /// Tính sta3 : tỷ lệ khách quay lại
                            var lstCustomerIds = getData_OlderCustomerByTime();
                            if (lstCustomerIds.Count > 0)
                            {
                                var strCustomerIds = "";
                                foreach (var ccode in lstCustomerIds)
                                {
                                    strCustomerIds += ccode.CustomerId.ToString() + ",";
                                }
                                strCustomerIds = strCustomerIds.TrimEnd(',');

                                var staData2 = getData_OlderCustomerTotal(strCustomerIds);
                                if (staData2 != null)
                                {
                                    totalSta2.old_bills += staData2.old_bills;
                                    totalSta2.all_bills += staData2.all_bills;

                                    if (staData2.all_bills > 0)
                                    {
                                        item.sta3 = (float)staData2.old_bills / staData2.all_bills * 100;
                                    }
                                }
                            }

                            //Tỷ lệ khách đặt lịch trước
                            var bookingData = getData_BookingPercent();
                            if (bookingData != null)
                            {
                                totalperBooking.old_bills += bookingData.old_bills;
                                totalperBooking.all_bills += bookingData.all_bills;

                                if (bookingData.all_bills > 0)
                                {
                                    item.perBooking = (float)bookingData.old_bills / bookingData.all_bills * 100;
                                }
                            }



                            //Tỷ lệ lưu lại ảnh
                            var imagesData = getData_SaveImagePercent();
                            if (imagesData != null)
                            {
                                totalperImages.old_bills += imagesData.old_bills;
                                totalperImages.all_bills += imagesData.all_bills;

                                if (imagesData.all_bills > 0)
                                {
                                    item.perImage = (float)imagesData.old_bills / imagesData.all_bills * 100;
                                }
                            }

                            // dịch vụ Shine Combo
                            var combo = getData_DoanhSo_DichVu(53);
                            if (combo >= 0)
                            {
                                shinecombo.doanhso += combo;
                                item.DV_ShineCombo100k = combo;
                            }

                            //dịch vụ Kid Combo
                            var kid = getData_DoanhSo_DichVu(74);
                            if (kid >= 0)
                            {
                                kidcombo.doanhso += kid;
                                item.DV_KidCombo = kid;
                            }

                            var protein = getData_DoanhSo_DichVu(69);
                            if (protein >= 0)
                            {
                                DuongProtein.doanhso += protein;
                                item.DV_DuongProtein = protein;
                            }

                            // dịch vụ Uốn
                            var uon = getData_DoanhSo_DichVu(16);
                            if (uon >= 0)
                            {
                                Uon.doanhso += uon;
                                item.DV_Uon250k = uon;
                            }

                            // dịch vụ Nhuộm
                            var nhuom = getData_DoanhSo_DichVu(14);
                            if (nhuom >= 0)
                            {
                                Nhuom.doanhso += nhuom;
                                item.DV_Nhuom180k = nhuom;
                            }

                            // dịch vụ Tẩy
                            var tay = getData_DoanhSo_DichVu(17);
                            if (tay >= 0)
                            {
                                Tay.doanhso += tay;
                                item.DV_Tay100k = tay;
                            }

                            // dịch vụ Tẩy 2 lần
                            var tayx2 = getData_DoanhSo_DichVu(24);
                            if (tayx2 >= 0)
                            {
                                Tayx2.doanhso += tayx2;
                                item.DV_Tay2Lan200k = tayx2;
                            }

                            /// Tính sta4, sta5
                            // tổng doanh số dịch vụ trong kỳ (tính trên tất cả bill )
                            var staData3 = getData_DoanhSoDoanhThuDV_TB();
                            if (staData3 != null)
                            {
                                totalStaMon.bills += staData3.bills;
                                totalStaMon.money += staData3.money;

                                if (days1 > 0)
                                {
                                    item.sta4 = (float)item.Bills_Service / days1;
                                    item.sta5 = (float)item.Money_Services / days1;
                                }
                            }

                            /// Add item to lst                        
                            lst.Add(item);
                        }

                        if (totalSta.stylist > 0)
                        {
                            totalReport.sta1 = (float)totalSta.money / totalSta.stylist;
                            totalReport.sta2 = (float)totalSta.bills / totalSta.stylist;
                            if (days1 > 0)
                                totalReport.stylistWork = (float)totalSta.stylist / days1;
                        }
                        if (totalSinner.skinner > 0)
                        {
                            if (days1 > 0)
                                totalReport.skinnerWork = (float)totalSinner.skinner / days1;
                        }
                        if (totalSta2.all_bills > 0)
                        {
                            totalReport.sta3 = (float)totalSta2.old_bills / totalSta2.all_bills * 100;
                        }
                        if (totalperBooking.all_bills > 0)
                        {
                            totalReport.perBooking = (float)totalperBooking.old_bills / totalperBooking.all_bills * 100;
                        }
                        if (totalperImages.all_bills > 0)
                        {
                            totalReport.perImage = (float)totalperImages.old_bills / totalperImages.all_bills * 100;
                        }
                        if (shinecombo.doanhso > 0)
                        {
                            totalReport.DV_ShineCombo100k = shinecombo.doanhso;
                        }
                        if (kidcombo.doanhso > 0)
                        {
                            totalReport.DV_KidCombo = kidcombo.doanhso;
                        }
                        if (DuongProtein.doanhso > 0)
                        {
                            totalReport.DV_DuongProtein = DuongProtein.doanhso;
                        }
                        if (Uon.doanhso > 0)
                        {
                            totalReport.DV_Uon250k = Uon.doanhso;
                        }
                        if (Nhuom.doanhso > 0)
                        {
                            totalReport.DV_Nhuom180k = Nhuom.doanhso;
                        }
                        if (Tay.doanhso > 0)
                        {
                            totalReport.DV_Tay100k = Tay.doanhso;
                        }
                        if (Tayx2.doanhso > 0)
                        {
                            totalReport.DV_Tay2Lan200k = Tayx2.doanhso;
                        }
                        if (days1 > 0)
                        {
                            totalReport.sta4 = (float)totalReport.Bills_Service / days1;
                            totalReport.sta5 = (float)totalReport.Money_Services / days1;
                        }

                        totalReport.SalonName = "Toàn hệ thống";
                        if (Salons.Count > 1)
                        {
                            //lst.Add(totalReport);
                            lst.Insert(0, totalReport);
                        }
                    }

                    /// Generate table
                    if (lst != null)
                    {
                        var tr = "<tr><th style='border: 1px solid black; border-collapse: collapse; padding: 5px; font-family: Roboto Condensed Bold; font-weight: normal; text-align: center;' rowspan='2'></th>";
                        var tr1 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px; font-family: Roboto Condensed Bold; font-weight: normal; text-align: center;' colspan='8'>DOANH SỐ</th>";
                        var tr2 = "<th colspan='3' style='border: 1px solid black; border-collapse: collapse; padding: 5px; font-family: Roboto Condensed Bold; font-weight: normal; text-align: center;'>DOANH THU</th>";
                        var tr3 = "<th colspan='10' style='border: 1px solid black; border-collapse: collapse; padding: 5px; font-family: Roboto Condensed Bold; font-weight: normal; text-align: center;'>VẬN HÀNH</th></tr>";
                        var tr4 = "<tr>";
                        var tr5 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>Tổng hóa đơn</th>";
                        var tr6 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>Shine Combo</th>";
                        var tr24 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>Kid Combo</th>";
                        var tr25 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>Dưỡng Protein</th>";
                        var tr7 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>Uốn</th>";
                        var tr8 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>Nhuộm</th>";
                        var tr9 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>Tẩy</th>";
                        var tr10 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>Tẩy 2 lần</th>";
                        var tr11 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>Tổng doanh thu</th>";
                        var tr12 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>Dịch vụ</th>";
                        var tr13 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>Mỹ phẩm</th>";
                        var tr14 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>Stylist đi làm</th>";
                        var tr15 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>Skinner đi làm</th>";
                        var tr16 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>Đánh giá chất lượng (%)</th>";
                        var tr17 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>Tỷ lệ khách cũ (%)</th>";
                        var tr18 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>Tỷ lệ khách đặt trước (%)</th>";
                        var tr19 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>Tỷ lệ lưu lại ảnh (%)</th>";
                        var tr20 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>Doanh số DV theo Stylist / Số Stylist</th>";
                        var tr21 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>Doanh thu DV theo Stylist / Số Stylist</th>";
                        var tr22 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>Doanh số DV TB/ngày trong kỳ</th>";
                        var tr23 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>Doanh thu DV TB/ngày trong kỳ</th>";

                        tr23 += "</tr>";

                        var result = "";
                        foreach (var v in lst)
                        {
                            /// Tỷ lệ % số bill theo các mức đánh giá của khách hàng
                            float great = 0;
                            float good = 0;
                            float bad = 0;
                            if (v.rating_total > 0)
                            {
                                great = (float)v.rating_great / v.rating_total * 100;
                                good = (float)v.rating_good / v.rating_total * 100;
                                bad = 100 - (great + good);
                            }

                            var row1 = "<tr><th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + v.SalonName + "</th>";
                            row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:#,###}", v.Total_Bills).Replace(',', '.') + "</td>";
                            row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:#,###}", v.DV_ShineCombo100k).Replace(',', '.') + "</td>";
                            row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:#,###}", v.DV_KidCombo).Replace(',', '.') + "</td>";
                            row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:#,###}", v.DV_DuongProtein).Replace(',', '.') + "</td>";
                            row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:#,###}", v.DV_Uon250k).Replace(',', '.') + "</td>";
                            row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:#,###}", v.DV_Nhuom180k).Replace(',', '.') + "</td>";
                            row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:#,###}", v.DV_Tay100k).Replace(',', '.') + "</td>";
                            row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:#,###}", v.DV_Tay2Lan200k).Replace(',', '.') + "</td>";
                            row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:#,###}", v.Total_Money / 1000).Replace(',', '.') + "</td>";
                            row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:#,###}", v.Money_Services / 1000).Replace(',', '.') + "</td>";
                            row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:#,###}", v.Money_Products / 1000).Replace(',', '.') + "</td>";
                            row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:0.00}", v.stylistWork).Replace('.', ',') + "</td>";
                            row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:0.00}", v.skinnerWork).Replace('.', ',') + "</td>";
                            row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:00.00}", great).Replace('.', ',') + " - " + String.Format("{0:00.00}", good).Replace('.', ',') + " - " + String.Format("{0:00.00}", bad).Replace('.', ',') + "</td>";
                            row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:00.00}", v.sta3).Replace('.', ',') + "</td>";
                            row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:00.00}", v.perBooking).Replace(',', '.') + "</td>";
                            row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:00.00}", v.perImage).Replace(',', '.') + "</td>";
                            row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:00.00}", v.sta2).Replace('.', ',') + "</td>";
                            row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:#,###}", v.sta1).Replace(',', '.') + "</td>";
                            row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:0.00}", v.sta4).Replace('.', ',') + "</td>";
                            row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:#,###}", v.sta5).Replace(',', '.') + "</td>";

                            row1 += "</tr>";
                            result += row1;

                        }

                        table += @"<table style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>
                                    <thead>" + tr + tr1 + tr2 + tr3 + tr4 + tr5 + tr6 + tr24 + tr25 + tr7 + tr8 + tr9 + tr10 + tr11 + tr12 + tr13 + tr14 + tr15 + tr16 + tr17 + tr18 + tr19 + tr20 + tr21 + tr22 + tr23 + @"</thead>
                                    <tbody>" + result + @"</tbody>
                                </table>";

                    }
                    else
                    {
                        //
                    }
                }
            }
        }


        /// <summary>
        /// Doanh thu doanh số theo Stylist
        /// </summary>
        /// <returns></returns>
        private cls_sta getData_DoanhSoDoanhThuDV_Stylist()
        {
            object[] paras =
                    {
                        new SqlParameter("@timeFrom",String.Format("{0:yyyy/MM/dd}", timeFrom)),
                        new SqlParameter("@timeTo",String.Format("{0:yyyy/MM/dd}", timeTo)),
                        new SqlParameter("@salonId",salonId)
                    };
            return db.Database.SqlQuery<cls_sta>("sp_BC_getData_DoanhSoDoanhThuDV_Stylist @timeFrom, @timeTo, @salonId", paras).FirstOrDefault();
        }

        /// <summary>
        /// nhân viên đi làm theo chấm công
        /// </summary>
        /// <returns></returns>
        private List<int> getData_Staff_Work(int staff_type)
        {
            object[] parasSt =
                    {
                        new SqlParameter("@timeFrom",String.Format("{0:yyyy/MM/dd}", timeFrom)),
                        new SqlParameter("@timeTo",String.Format("{0:yyyy/MM/dd}", timeTo.AddDays(-1))),
                        new SqlParameter("@salonId",salonId),
                        new SqlParameter("@staff_type", staff_type)
                    };
            return db.Database.SqlQuery<int>("sp_BC_getData_Staff_Work @timeFrom, @timeTo, @salonId, @staff_type", parasSt).ToList();
        }

        /// <summary>
        /// list khách cũ
        /// </summary>
        /// <returns></returns>
        private List<cls_sta3> getData_OlderCustomerByTime()
        {
            object[] parasSta3 =
                     {
                        new SqlParameter("@timeFrom", String.Format("{0:yyyy/MM/dd}", timeFrom)),
                        new SqlParameter("@timeTo", String.Format("{0:yyyy/MM/dd}", timeTo)),
                        new SqlParameter("@salonId", salonId)
                    };
            return db.Database.SqlQuery<cls_sta3>("sp_BC_getData_OlderCustomerByTime @timeFrom, @timeTo, @salonId", parasSta3).ToList();
        }

        /// <summary>
        /// Tổng số lượng khách cũ
        /// </summary>
        /// <returns></returns>
        private cls_sta2 getData_OlderCustomerTotal(string strCustomerIds)
        {
            object[] parasOlderCus =
                        {
                            new SqlParameter("@timeFrom",String.Format("{0:yyyy/MM/dd}", timeFrom)),
                            new SqlParameter("@timeTo",String.Format("{0:yyyy/MM/dd}", timeTo)),
                            new SqlParameter("@salonId",salonId),
                            new SqlParameter("@CustomerIds", strCustomerIds)
                        };
            return db.Database.SqlQuery<cls_sta2>("sp_BC_getData_OlderCustomerTotal @timeFrom, @timeTo, @salonId, @CustomerIds", parasOlderCus).FirstOrDefault();
        }

        /// <summary>
        /// bill có booking 
        /// </summary>
        /// <returns></returns>
        private cls_sta2 getData_BookingPercent()
        {
            object[] parasBooking =
                    {
                        new SqlParameter("@timeFrom",String.Format("{0:yyyy/MM/dd}", timeFrom)),
                        new SqlParameter("@timeTo",String.Format("{0:yyyy/MM/dd}", timeTo)),
                        new SqlParameter("@salonId",salonId)
                    };
            return db.Database.SqlQuery<cls_sta2>("sp_BC_getData_BookingPercent @timeFrom, @timeTo, @salonId", parasBooking).FirstOrDefault();
        }

        /// <summary>
        /// bill có ảnh
        /// </summary>
        /// <returns></returns>
        private cls_sta2 getData_SaveImagePercent()
        {
            object[] parasImage =
                   {
                        new SqlParameter("@timeFrom",String.Format("{0:yyyy/MM/dd}", timeFrom)),
                        new SqlParameter("@timeTo",String.Format("{0:yyyy/MM/dd}", timeTo)),
                        new SqlParameter("@salonId",salonId)
                    };
            return db.Database.SqlQuery<cls_sta2>("sp_BC_getData_SaveImagePercent @timeFrom, @timeTo, @salonId", parasImage).FirstOrDefault();
        }

        /// <summary>
        /// Doanh số - doanh thu DV TB
        /// </summary>
        /// <returns></returns>
        private cls_sta getData_DoanhSoDoanhThuDV_TB()
        {
            object[] parasSta =
                    {
                        new SqlParameter("@timeFrom",String.Format("{0:yyyy/MM/dd}", timeFrom)),
                        new SqlParameter("@timeTo",String.Format("{0:yyyy/MM/dd}", timeTo)),
                        new SqlParameter("@salonId",salonId)
                    };
            return db.Database.SqlQuery<cls_sta>("sp_BC_getData_DoanhSoDoanhThuDV_TB @timeFrom, @timeTo, @salonId", parasSta).FirstOrDefault();
        }

        /// <summary>
        /// Doanh số dịch vụ
        /// Shine Combo 100k : ID 53
        /// Uốn 250k : ID 16
        /// Nhuộm 180k : ID 14
        /// Tẩy 100k : ID 17
        /// Kid Combo : ID 74
        /// Dưỡng protein: ID 69
        /// </summary>
        /// <param name="serviceId"></param>
        /// <returns></returns>
        private long getData_DoanhSo_DichVu(int serviceId)
        {
            object[] paras =
            {
                new SqlParameter("@timeFrom",String.Format("{0:yyyy/MM/dd}", timeFrom)),
                new SqlParameter("@timeTo",String.Format("{0:yyyy/MM/dd}", timeTo)),
                new SqlParameter("@salonId",salonId),
                new SqlParameter("@serviceId",serviceId)
            };
            long result = db.Database.SqlQuery<long>("sp_BC_getData_DoanhSo_DichVu @timeFrom, @timeTo, @salonId, @serviceId", paras).SingleOrDefault();
            return result;
        }

        private int Compute_DaysMon(DateTime timeFrom, DateTime timeTo)
        {
            int day = 0;
            if (timeTo.Month > timeFrom.Month)
            {
                day = (timeTo - timeFrom).Days;
            }
            else
            {
                if (timeFrom.Month == DateTime.Now.Month)
                {
                    day = DateTime.Now.Day;
                }
                else
                {
                    day = timeTo.Day;
                }
            }
            return day;
        }

        protected string ReturnStaffName(int _Id)
        {
            string _Name = "";
            _Name = _LstNhanVien.FirstOrDefault(a => a.Id == _Id).Fullname;
            return _Name;
        }

        protected void _BtnClick(Object sender, EventArgs e)
        {
            ReportData_Daily();
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        /// <summary>
        /// Check permission
        /// </summary>
        private void CheckPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                string[] Allow = new string[] { "root", "admin", "salonmanager", "accountant" };
                string[] AllowViewAllData = new string[] { "root", "admin", "accountant" };
                string[] AllowViewAllDate = new string[] { "root", "admin", "accountant", "salonmanager" };
                string[] Accountant = new string[] { "accountant" };
                string[] AllowDelete = new string[] { "root", "admin", "salonmanager" };
                var Permission = Session["User_Permission"].ToString().Trim();
                if (Array.IndexOf(Allow, Permission) != -1)
                {
                    Perm_Access = true;
                }
                Perm_Edit = true;
                if (Array.IndexOf(AllowDelete, Permission) != -1)
                {
                    Perm_Delete = true;
                }

                if (Array.IndexOf(AllowViewAllDate, Permission) != -1)
                {
                    Perm_ViewAllDate = true;
                }

                if (Array.IndexOf(AllowViewAllData, Permission) != -1)
                {
                    Perm_ViewAllData = true;
                }

                if (Array.IndexOf(Accountant, Permission) != -1)
                {
                    IsAccountant = true;
                }
            }

            // Call execute function
            ExecuteByPermission();
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        public void Repeater_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
              || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (Perm_Edit == true)
                    ((Panel)(e.Item.FindControl("EAedit"))).Visible = true;
                if (Perm_Delete == true)
                    ((Panel)(e.Item.FindControl("EAdelete"))).Visible = true;
            }
        }

    }

    public struct SingleReportData
    {
        public long Total_Bills { get; set; }
        public long Bills_Service { get; set; }
        public long Bills_Product { get; set; }
        public long Total_Money { get; set; }
        public long Money_Services { get; set; }
        public long Money_Products { get; set; }
        public long Money_Products_Online { get; set; }
        public long Money_Products_Salon { get; set; }
        public string ProductNames { get; set; }
        public int Customer_JustService { get; set; }
        public int Customer_JustProduct { get; set; }
        public string SalonName { get; set; }
        /// <summary>
        /// sta1 : Doanh thu dịch vụ từ stylist / số stylist (đối với mỗi salon và toàn bộ salon)
        /// </summary>
        public float sta1 { get; set; }
        /// <summary>
        /// sta2 : Số bill dịch vụ / số stylist
        /// </summary>
        public float sta2 { get; set; }
        /// <summary>
        /// sta3 : % khách quay lại, bill khách cũ / tổng số bill
        /// </summary>
        public float sta3 { get; set; }
        /// <summary>
        /// Doanh số DV TB/ngày trong kỳ (time2 - time1)
        /// </summary>
        public float sta4 { get; set; }
        /// <summary>
        /// Doanh thu DV TB/ngày trong kỳ (time2 - time1)
        /// </summary>
        public float sta5 { get; set; }
        /// <summary>
        /// Số stylist trong ngày
        /// </summary>
        public float sta6 { get; set; }
        /// <summary>
        /// Stylist đi làm (theo chấm công)
        /// </summary>
        public float stylistWork { get; set; }
        /// <summary>
        /// Skinner đi làm (theo chấm công)
        /// </summary>
        public float skinnerWork { get; set; }
        /// <summary>
        /// tỷ lệ khách đặt trước
        /// </summary>
        public float perBooking { get; set; }
        /// <summary>
        /// tỷ lệ lưu lại ảnh
        /// </summary>
        public float perImage { get; set; }
        public int rating_great { get; set; }
        public int rating_good { get; set; }
        public int rating_bad { get; set; }
        public int rating_total { get; set; }
        public long DV_ShineCombo100k { get; set; }
        public long DV_KidCombo { get; set; }
        public long DV_DuongProtein { get; set; }
        public long DV_Uon250k { get; set; }
        public long DV_Nhuom180k { get; set; }
        public long DV_Tay100k { get; set; }
        public long DV_Tay2Lan200k { get; set; }
    }

    public struct BillStruct
    {
        public int Id { get; set; }
        public string Date { get; set; }
        public string CustomerName { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerPhone { get; set; }
        public int CustomerReturn { get; set; }
        public string ProductNames { get; set; }
        public string ServiceNames { get; set; }
        public List<string> ServiceName { get; set; }
        public List<string> ProductName { get; set; }
        public List<int> ServiceValue { get; set; }
        public List<int> ProductValue { get; set; }
        public int TotalMoneyService { get; set; }
        public int TotalMoneyProduct { get; set; }
        public int TotalMoney { get; set; }
        public int Staff_Hairdresser_Id { get; set; }
        public int Staff_HairMassage_Id { get; set; }
        public int SellerId { get; set; }
        public int SalonId { get; set; }
        public int Mark { get; set; }
        public int IsDelete { get; set; }
        public string Images { get; set; }
        public string Note { get; set; }
        public bool CustomerNoInfo { get; set; }
        public string HairdresserName { get; set; }
        public string HairMassageName { get; set; }
        public string CosmeticName { get; set; }
        public string ProductIds { get; set; }
        public string ServiceIds { get; set; }
        public DateTime ModifiedDate { get; set; }
        public DateTime CreatedDate { get; set; }
    }

    public class cls_sta
    {
        public int bills { get; set; }
        public double money { get; set; }
        public int stylist { get; set; }
    }
    public class cls_skinner
    {
        public int bills { get; set; }
        public double money { get; set; }
        public int skinner { get; set; }
    }

    public class cls_sta2
    {
        public int old_bills { get; set; }
        public int all_bills { get; set; }
    }

    public class cls_sta3
    {
        public int CustomerId { get; set; }
        public int num { get; set; }
    }

    public class cls_rating
    {
        public int great { get; set; }
        public int good { get; set; }
        public int bad { get; set; }
        public int total { get; set; }
    }
    public class cls_doanhso_dv
    {
        public long doanhso { get; set; }
    }
}