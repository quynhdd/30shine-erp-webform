﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Globalization;
using System.Web.Script.Serialization;
using ExportToExcel;
using Excel;
using LinqKit;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.Report
{
    public partial class SaleReportChart : System.Web.UI.Page
    {
        protected Paging PAGING = new Paging();
        protected SingleReportData _SingleReportData = new SingleReportData();
        protected SingleReportData _SingleReportData_CL = new SingleReportData();
        protected SingleReportData _SingleReportData_KT = new SingleReportData();
        protected SingleReportData _SingleReportData_TDN = new SingleReportData();

        protected List<List<ProductBasic>> ProductList;
        private Expression<Func<BillService, bool>> Where = PredicateBuilder.True<BillService>();

        private string PageID = "BC_CHART";
        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        protected bool Perm_ViewAllData = false;
        private bool Perm_Access = false;
        protected bool IsAccountant = false;
        List<_30shine.MODEL.ENTITY.EDMX.Staff> _LstNhanVien;

        //protected string Today = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
        //protected string ThisWeek = string.Format("{0:dd/MM/yyyy}", DateTime.Now.DayOfWeek);
        //protected string ThisMonth = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                //if (permission == "salonmanager")
                //{
                //    btnReportSale.Visible = false;
                //    btnReportBill.Visible = false;
                //    btnReportBillByHours.Visible = false;
                //    btnReportSaleAvg.Visible = false;
                //    btnReportCustomer.Visible = false;
                //    btnCustomerFamiliar.Visible = false;
                //}

                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                ExecuteByPermission();

                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                FillLstStaff();
                Library.Function.bindSalon(new List<DropDownList> { Salon }, Perm_ViewAllData);
            }
          
        }

        protected void FillLstStaff()
        {
            using (var db = new Solution_30shineEntities())
            {
                _LstNhanVien = db.Staffs.ToList();
            }
        }

        public void Repeater_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
              || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (Perm_Edit == true)
                    ((Panel)(e.Item.FindControl("EAedit"))).Visible = true;
                if (Perm_Delete == true)
                    ((Panel)(e.Item.FindControl("EAdelete"))).Visible = true;
            }
        }

    }

 
    
}