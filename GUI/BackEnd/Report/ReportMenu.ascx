﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReportMenu.ascx.cs" Inherits="_30shine.GUI.BackEnd.Report.ReportMenu" %>

<div class="wp sub-menu">
    <div class="wp960">
        <div class="wp content-wp">
            <ul class="ul-sub-menu" id="subMenu">
                <li>Báo cáo &nbsp;&#187; </li>
                <% if(Permission != "reception"){ %>
                <li class="be-report-li">
                    <a href="/admin/bao-cao/hoa-don.html"><i class="fa fa-th-large"></i>Tổng hợp</a>
                </li>
                <%--<li class="be-report-li">                    
                    <a href="/admin/bao-cao/dich-vu.html"><i class="fa fa-th-large"></i>Dịch vụ</a>
                </li>--%>
                <li class="be-report-li">
                    <a href="/admin/bao-cao/doanh-so-my-pham.html"><i class="fa fa-th-large"></i>Mỹ phẩm</a>
                </li>
                <li class="be-report-li">
                    <a href="/admin/bao-cao/doanh-so-dich-vu.html"><i class="fa fa-th-large"></i>Dịch vụ</a>
                </li>
                 <li class="be-report-li li-pending"><a href="/dich-vu/pending-cu.html"><i class="pending-old"></i>Pending cũ</a></li>
                <li class="be-report-li">
                    <a href="/admin/bao-cao/nhan-vien.html"><i class="fa fa-th-large"></i>Nhân viên</a>
                </li>
                <li class="be-report-li">
                    <a href="/admin/bao-cao/salary-v2.html"><i class="fa fa-th-large"></i>Tính lương</a>
                </li>

                <%--<li class="be-report-li">
                    <a href="/admin/bao-cao/bill-theo-gio.html"><i class="fa fa-th-large"></i>Bill theo khung giờ</a>
                </li>--%>
                  <li class="be-report-li">
                    <a href="/admin/bao-cao/bieu-do.html"><i class="fa fa-bar-chart"></i>Biểu đồ</a>
                </li>
                <% }else if(Permission == "reception"){ %>
                    <li class="be-report-li">
                        <a href="/admin/bao-cao/doanh-so.html"><i class="fa fa-th-large"></i>Doanh số SP</a>
                    </li>
                <% } %>
            </ul>
        </div>
    </div>
</div>
