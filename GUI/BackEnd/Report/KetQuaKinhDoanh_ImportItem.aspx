﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="KetQuaKinhDoanh_ImportItem.aspx.cs" Inherits="_30shine.GUI.BackEnd.Report.KetQuaKinhDoanh_ImportItem" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="ProductAdd" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý bậc kỹ năng &nbsp;&#187; </li>
                        <li class="li-listing"><a href="/admin/bao-cao/ket-qua-kinh-doanh/nhap-lieu/danh-sach.html">Danh sách</a></li>
                        <% if (_IsUpdate)
                        { %>
                        <li class="li-add"><a href="/admin/bao-cao/ket-qua-kinh-doanh/nhap-lieu/nhap-moi.html">Thêm mới</a></li>
                        <li class="li-edit active"><a href="/admin/bao-cao/ket-qua-kinh-doanh/nhap-lieu/<%= _Code %>.html">Cập nhật</a></li>
                        <% }
                        else
                        { %>
                        <li class="li-add active"><a href="/admin/bao-cao/ket-qua-kinh-doanh/nhap-lieu/nhap-moi.html">Thêm mới</a></li>
                        <% } %>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add">
            <%-- Add --%>
            <div class="wp960 content-wp">
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <div class="table-wp">
                    <table class="table-add admin-product-table-add">
                        <tbody>
                            <tr class="title-head">
                                <td><strong>Nhập dữ liệu báo cáo kinh doanh</strong></td>
                                <td></td>
                            </tr>
                            <tr class="tr-margin" style="height: 20px;"></tr>

                            <tr class="tr-field-ahalf">
                                <td class="col-xs-2 left"><span>Ngày nhập</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeFrom" style="width: 55% !important" placeholder="Chọn ngày"
                                            ClientIDMode="Static" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="TimeValidate" ControlToValidate="TxtDateTimeFrom" runat="server" CssClass="fb-cover-error" Text="Bạn chưa chọn ngày!"></asp:RequiredFieldValidator>
                                    </span>
                                </td>
                            </tr>

                            <tr class="tr-field-ahalf">
                                <td class="col-xs-3 left"><span>Danh mục báo cáo</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:DropDownList ID="ddlItemId" runat="server" CssClass="form-control" style="width: 55% !important" ClientIDMode="Static"></asp:DropDownList>
                                    </span>
                                </td>
                            </tr>

                            <tr class="tr-field-ahalf">
                                <td class="col-xs-3 left"><span>Salon/Bộ phận</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:DropDownList ID="ddlSalon" runat="server" CssClass="select form-control" style="width: 55% !important" ClientIDMode="Static"></asp:DropDownList>
                                    </span>
                                </td>
                            </tr>

                            <tr class="tr-field-ahalf">
                                <td class="col-xs-3 left"><span>Số tiền</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:Label style="margin-left: -36px;border-left: 1px solid silver;padding-left: 5px;">VNĐ</asp:Label>
                                        <asp:TextBox ID="Money" runat="server" CssClass="form-control" style="width: 55% !important" ClientIDMode="Static"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>

                            <tr class="tr-description">
                                <td class="col-xs-2 left"><span>Ghi chú</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox TextMode="MultiLine" CssClass="form-control" style="width: 55% !important" Rows="5" ID="Note" runat="server" ClientIDMode="Static"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>

                            <tr class="tr-send">
                                <td class="col-xs-2 left"></td>
                                <td class="col-xs-9 right no-border">
                                    <span class="field-wp">
                                        <asp:Button ID="Send" CssClass="btn-send" runat="server" Text="Hoàn tất" ClientIDMode="Static" OnClick="ExcAddOrUpdate"></asp:Button>
                                        <%--<span class="btn-send btn-cancel" id="CalcelAddFbcv" style="margin-left: 12px;" >Hủy</span>--%>
                                    </span>
                                </td>
                            </tr>
                            <asp:HiddenField runat="server" ID="UrlKeyField" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="ImgCoverField" ClientIDMode="Static" />
                        </tbody>
                    </table>
                </div>
            </div>
            <%-- end Add --%>
        </div>
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 55% !important;
                margin-top: 5px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }
        </style>
        <script>
            jQuery(document).ready(function () {
                $("#glbAdminSales").addClass("active");
                $("#glbAdminKetQuaKinhDoanh").addClass("active");
                //============================
                // Show System Message
                //============================ 
                var qs = getQueryStrings();
                showMsgSystem(qs["msg_update_message"], qs["msg_update_status"]);
            });
        </script>

    </asp:Panel>
</asp:Content>

