﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Report_AboutTime.aspx.cs" Inherits="_30shine.GUI.BackEnd.Report.Report_AboutTime" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title></title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <style>
            .customer-listing table.table-listing tbody tr:hover .edit-wp.display_permission { display: none !important; }
            .display_permission { display: none; }
            .edit-wp { position: absolute; right: 3px; top: 24%; background: #e7e7e7; display: none; }
                .edit-wp .elm { width: 17px; height: 20px; float: left; display: block; background: url(/Assets/images/icon.delete.small.active.png?v=2); margin-right: 30px; }
                    .edit-wp .elm:hover { background: url(/Assets/images/icon.delete.small.png?v=2); }
            .wp_time_booking { width: 100%; float: left; padding-left: 55px; margin: 5px 0px 15px; }
                .wp_time_booking .a_time_booking { float: left; height: 26px; line-height: 26px; padding: 0px 15px; background: #dfdfdf; margin-right: 10px; cursor: pointer; position: relative; font-size: 13px; -webkit-border-radius: 15px; -moz-border-radius: 15px; border-radius: 15px; }
                    .wp_time_booking .a_time_booking:hover, .wp_time_booking .a_time_booking.active { background: #fcd344; color: #000; }
                    .wp_time_booking .a_time_booking .span_time_booking { position: absolute; top: 24px; left: 0; width: 100%; float: left; text-align: center; font-size: 13px; }
            .edit-wp .elm { margin-right: 0; }
            table.table-total-report-1 { border-collapse: collapse; }
                table.table-total-report-1,
                table.table-total-report-1 th,
                table.table-total-report-1 td { border: 1px solid black; }
                    table.table-total-report-1 td { padding: 5px 15px; text-align: right; }
                        table.table-total-report-1 td:first-child { text-align: left; }
        </style>
        <%--<uc1:ReportMenu runat="server" id="ReportMenu" />--%>

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                     <ul class="ul-sub-menu" id="subMenu">
                        <li>QRCode &nbsp;&#187; </li>
                        <li class="be-report-li" id="ReportDaily"><a href="/admin/staff-checkin/report-by-time.html"><i class="fa fa-th-large"></i>Báo cáo chấm công hàng ngày</a></li>
                        <li class="be-report-li" id="ReportMonth"><a href="/admin/staff-checkin/daily-report.html"><i class="fa fa-th-large"></i>Thống kê chấm công</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing be-report be-report-timekeeping">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <div class="row">
                    <div class="filter-item">
                        <strong class="st-head"><i class="fa fa-clock-o"></i>Thời gian</strong>
                        <div class="time-wp">
                            <asp:TextBox CssClass="txtDateTime st-head form-control" ID="txtStartDate" placeholder="Từ ngày" Style="margin-left: 10px!important; width: 143px;"
                                ClientIDMode="Static" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="ValidateTime" ControlToValidate="txtStartDate" runat="server" CssClass="fb-cover-error" Text="Vui lòng chọn thời gian!"></asp:RequiredFieldValidator>
                        </div>
                        <strong class="st-head" style="margin-left: 10px;">
                            <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                        </strong>
                        <asp:TextBox CssClass="txtDateTime st-head form-control" ID="txtEndDate" placeholder="Đến ngày" Style="margin-left: 10px!important; width: 143px;"
                            ClientIDMode="Static" runat="server"></asp:TextBox>
                        <br />
                    </div>
                    <div class="filter-item">
                        <strong class="st-head" style="margin-right: 5px;">Salon : </strong>
                        <asp:DropDownList ID="ddlSalon" CssClass="form-control select" runat="server" ClientIDMode="Static"></asp:DropDownList>
                    </div>
                    <div class="filter-item">
                        <strong class="st-head" style="margin-right: 5px;">Bộ phận : </strong>
                        <asp:DropDownList ID="ddlDepartment" CssClass="form-control" runat="server" ClientIDMode="Static" Style="width: 190px;"></asp:DropDownList>
                    </div>
                    <a id="ViewData" onclick="excPaging(1)" class="st-head btn-viewdata " href="javascript:void(0)">Xem dữ liệu
                    </a>
                </div>
                <div class="row">
                    <strong class="st-head"><i class="fa fa-file-text"></i>Thống kê theo dõi chấm công</strong>
                </div>
                <div class="wp customer-add customer-listing be-report">
                    <%-- Listing --%>
                    <div class="wp960 content-wp">
                        <!-- Filter -->
                        <!-- End Filter -->
                        <!-- Row Table Filter -->
                        <div class="table-func-panel" style="margin-top: -20px;">
                            <div class="table-func-elm">
                                <span>Số hàng / Page : </span>
                                <div class="table-func-input-wp">
                                    <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                                    <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                                    <ul class="ul-opt-segment">
                                        <li data-value="10">10</li>
                                        <li data-value="20">20</li>
                                        <li data-value="30">30</li>
                                        <li data-value="40">40</li>
                                        <li data-value="50">50</li>
                                        <li data-value="1000000">Tất cả</li>
                                    </ul>
                                    <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                                </div>
                            </div>
                        </div>
                        <!-- End Row Table Filter -->
                        <asp:ScriptManager runat="server" ID="ScriptManager2"></asp:ScriptManager>
                        <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                            <ContentTemplate>
                                <div class="table-wp">
                                    <table class="table-add table-listing">
                                        <thead>
                                            <tr>
                                                <th>STT</th>
                                                <th>ID</th>
                                                <th>Tên nhân viên</th>
                                                <th>Bộ phận</th>
                                                <th>Tổng công</th>
                                                <th>Đúng giờ</th>
                                                <th>Đến muộn</th>
                                                <th>Về sớm</th>
                                                <th>Đi muộn, về sớm</th>
                                                <th>Đã xin phép</th>
                                                <th>Không xin phép</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <asp:Repeater ID="rptListTimekeeping" runat="server">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td><%# Container.ItemIndex + 1 + PAGING._Offset %></td>
                                                        <td><%# Eval("staffId") %></td>
                                                        <td><%# Eval("staffName") %></td>
                                                        <td><%# Eval("department")%></td>
                                                        <td><%# Eval("totalKeeping") %></td>
                                                        <td><%# Eval("totalInTime") %></td>
                                                        <td><%# Eval("totalComeLater") %></td>
                                                        <td><%# Eval("totalLeaveEarly") %></td>
                                                        <td><%# Eval("totalComeLaterLeaveEarly") %></td>
                                                        <td><%# Eval("totalConfirmed") %></td>
                                                        <td><%# Eval("totalUnConfirm") %></td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </tbody>
                                    </table>
                                </div>

                                <!-- Paging -->
                                <div class="site-paging-wp">
                                    <% if (PAGING.TotalPage > 1)
                                        { %>
                                    <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                                        <% if (PAGING._Paging.Prev != 0)
                                            { %>
                                        <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                                        <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                                        <% } %>
                                        <asp:Repeater ID="RptPaging" runat="server">
                                            <ItemTemplate>
                                                <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                                    <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>>
                                                    <%# Eval("PageNum") %>
                                                </a>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                                            { %>
                                        <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                                        <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                                        <% } %>
                                    </asp:Panel>
                                    <% } %>
                                </div>
                                <!-- End Paging -->
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                        <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
                    </div>
                    <%-- END Listing --%>
                </div>

            </div>
            <%-- END Listing --%>
        </div>
           <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 190px !important;
                margin-top: 5px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }
        </style>
        <script type="text/ecmascript">
            jQuery(document).ready(function () {
                // Add active menu
                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) { }
                });

            });
            //======================================================
            function viewDataByDate(This, time) {
                $(".a_time_booking.active").removeClass("active");
                This.addClass("active");
                $("#txtStartDate").val(time);
                $("#txtEndDate").val(time);
                $("#ViewData").click();
            }

        </script>
    </asp:Panel>
</asp:Content>
