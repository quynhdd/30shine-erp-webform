﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Web.Script.Serialization;
using System.Globalization;
using LinqKit;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.UIService
{
    public partial class Service_OldPending_Listing : System.Web.UI.Page
    {
        private string PageID = "DV_PENDING_CU";
        protected Paging PAGING = new Paging();
        protected string Permission = "";
        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        private DateTime _FromDate;
        private DateTime _ToDate;
        protected bool Perm_ViewAllData = false;
        private bool Perm_Access = false;
        protected int SalonId;
        protected List<TeamService> lstTeamService = new List<TeamService>();
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            Bind_TeamWork();
            // SalonId = Convert.ToInt32(Session["SalonId"]);
            //TxtDateTimeFrom.Text = String.Format("{0:dd/MM/yyyy}", DateTime.Today);
            if (!IsPostBack)
            {
                //Perm_ViewAllData = false;
                if (!Perm_ViewAllData)
                {
                    Salon.Visible = false;
                }
                else
                {

                    Salon.Visible = true;
                    Library.Function.bindSalon(new List<DropDownList> { Salon }, Perm_ViewAllData);
                }

                Bind_Paging();
                Bind_RptBillService();
            }
        }



        private void Bind_TeamWork()
        {
            using (var db = new Solution_30shineEntities())
            {
                lstTeamService = db.TeamServices.Where(w => w.IsDelete != 1 && w.Publish == true && w.Status == 1).ToList();
            }
        }
        

        private void Bind_RptBillService()
        {
            using (var db = new Solution_30shineEntities())
            {
                CultureInfo culture = new CultureInfo("vi-VN");
                var _BillService = new List<BillService>();
                var Where = PredicateBuilder.True<BillService>();
                string SgtValue;
                string sql = "";
                if (TxtDateTimeFrom.Text.Trim() != "")
                {
                    _FromDate = Convert.ToDateTime(TxtDateTimeFrom.Text.Trim(), culture);
                    //_FromDate = Convert.ToDateTime(TxtDateTimeFrom.Text.Trim(), culture).AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);

                    if (TxtDateTimeTo.Text != "")
                    {
                        _ToDate = Convert.ToDateTime(TxtDateTimeTo.Text.Trim(), culture).AddDays(1);
                    }
                    else
                    {
                        _ToDate = Convert.ToDateTime(TxtDateTimeFrom.Text.Trim(), culture).AddDays(1);
                    }
                }else
                {
                    _FromDate =new DateTime(2015, 8, 1, 0, 0, 0, 0);
                    _ToDate = DateTime.Today;
                }
                switch (HDF_Suggestion_Field.Value)
                {
                    case "customer.code":
                        SgtValue = HDF_Suggestion_Code.Value;
                        Where = Where.And(w => w.CustomerCode.Contains(SgtValue));
                        break;
                    case "customer.name":
                        SgtValue = HDF_Suggestion_Code.Value;
                        Where = Where.And(w => w.CustomerCode.Contains(SgtValue));
                        break;
                    case "customer.phone":
                        SgtValue = HDF_Suggestion_Code.Value;
                        Where = Where.And(w => w.CustomerCode.Contains(SgtValue));
                        break;
                    case "bill.code": break;
                }

                Where = Where.And(p => p.IsDelete != 1 && p.Pending == 1);

                int integer;
                if (!Perm_ViewAllData)
                {
                    SalonId = Session["SalonId"] != null ? (int.TryParse(Session["SalonId"].ToString(), out integer) ? integer : 0) : 0;
                }
                else
                {
                    SalonId = (int.TryParse(Salon.SelectedValue, out integer) ? integer : 0);
                }

                //where
                if (SalonId > 0)
                {
                    var salonName = db.Tbl_Salon.Where(w => w.Id == SalonId).Select(s => s.Name).FirstOrDefault();
                    if (string.IsNullOrEmpty(salonName))
                    {
                        Label_Salon.InnerText = "Danh sách chờ hoàn tất của Salon " + salonName;
                    }

                    Where = Where.And(p => p.SalonId == SalonId);
                }
                else
                {
                    Label_Salon.InnerText = "Danh sách chờ hoàn tất";
                    Where = Where.And(p => p.SalonId >= 0);
                }



                // Điều kiện chỉ lấy pending trước ngày
                Where = Where.And(w => w.CreatedDate <= _FromDate);

                sql = @"select a.*, s.Fullname as HairdresserName, s2.Fullname as HairMassageName, s3.Fullname as CosmeticName, 
                            c.Fullname as CustomerName, c.Phone as CustomerPhone, t.Color as TeamColor 
                        from BillService as a
                        left join Staff as s
                        on a.Staff_Hairdresser_Id = s.Id
                        left join Staff as s2
                        on a.Staff_HairMassage_Id = s2.Id
                        left join Staff as s3
                        on a.SellerId = s3.Id
                        left join Customer as c
                        on a.CustomerCode = c.Customer_Code
                        left join TeamService as t
                        on a.TeamId = t.Id
                        where a.IsDelete != 1 and a.Pending = 1
                        and a.CreatedDate between '" + String.Format("{0:yyyy/MM/dd}", _FromDate) + "' and '" + String.Format("{0:yyyy/MM/dd}", _ToDate) + "'";
                if (SalonId > 0)
                {
                    sql += " and a.SalonId = " + SalonId;
                }

                sql += " order by a.CreatedDate";
                //var totalBill= db.Database.SqlQuery<BillService2>(sql).ToList().Count;
                //if (totalBill != null)
                //    Total_Bill.InnerText = "(" + totalBill + " hóa đơn)";
                //else Total_Bill.InnerText = "(0 hóa đơn)";


                var lst = db.Database.SqlQuery<BillService2>(sql).Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                if (lst.Count > 0)
                {
                    for (var i = 0; i < lst.Count; i++)
                    {
                        lst[i].billOrder = UIHelpers.getBillOrder(lst[i].BillCode, 4);
                        lst[i].completeTime = lst[i].CreatedDate.Value.Add(TimeSpan.Parse("00:50:00"));
                    }
                }

                if (lst.Count > 0)
                {

                    var Count = 0;
                    foreach (var v in lst)
                    {
                        JavaScriptSerializer serializer = new JavaScriptSerializer();
                        if (v.ProductIds != null)
                        {
                            var ProductListThis = serializer.Deserialize<List<ProductBasic>>(v.ProductIds);
                            var _ProductName = "";
                            if (ProductListThis != null)
                            {
                                foreach (var v2 in ProductListThis)
                                {
                                    _ProductName += "<a href=\"/admin/san-pham/" + v2.Code + ".html\" target=\"_blank\">" + v2.Name + "</a>, ";
                                }
                                _ProductName = _ProductName.TrimEnd(',', ' ');
                                lst[Count].ProductNames = _ProductName;
                            }
                        }

                        if (v.ServiceIds != null)
                        {
                            var ServiceListThis = serializer.Deserialize<List<ProductBasic>>(v.ServiceIds);
                            var _ServiceName = "";
                            if (ServiceListThis != null)
                            {
                                foreach (var v2 in ServiceListThis)
                                {
                                    _ServiceName += "<a href=\"/admin/dich-vu/" + v2.Code + ".html\" target=\"_blank\">" + v2.Name + "</a>, ";
                                }
                                _ServiceName = _ServiceName.TrimEnd(',', ' ');
                                lst[Count].ServiceNames = _ServiceName;
                            }
                        }

                        Count++;
                    }
                }

                RptBillService.DataSource = lst.OrderBy(o => o.CreatedDate);
                RptBillService.DataBind();
            }
        }


        protected void _BtnClick(object sender, EventArgs e)
        {

            Bind_Paging();
            Bind_RptBillService();
            RemoveLoading();

        }
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }



        protected void Bind_Paging()
        {
            // init Paging value
            PAGING._Segment = 10;
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? (HDF_Page.Value != "" ? Convert.ToInt32(HDF_Page.Value) : 1) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage() : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage()
        {
            using (var db = new Solution_30shineEntities())
            {
                CultureInfo culture = new CultureInfo("vi-VN");
                int Count;
                var Where = PredicateBuilder.True<BillService>();
                string SgtValue;

                if (TxtDateTimeFrom.Text.Trim() != "")
                {
                    _FromDate = Convert.ToDateTime(TxtDateTimeFrom.Text.Trim(), culture);
                    //_FromDate = Convert.ToDateTime(TxtDateTimeFrom.Text.Trim(), culture).AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);


                    if (TxtDateTimeTo.Text.Trim() != "")
                    {
                        _ToDate = Convert.ToDateTime(TxtDateTimeTo.Text.Trim(), culture).AddDays(1);
                    }
                    else
                    {
                        _ToDate = Convert.ToDateTime(TxtDateTimeFrom.Text.Trim(), culture).AddDays(1);
                    }
                }
                else
                {
                    _FromDate = new DateTime(2015, 8, 1, 0, 0, 0, 0);
                    _ToDate = DateTime.Today;
                }
                switch (HDF_Suggestion_Field.Value)
                {
                    case "customer.code":
                        SgtValue = HDF_Suggestion_Code.Value;
                        Where = Where.And(w => w.CustomerCode.Contains(SgtValue));
                        break;
                    case "customer.name":
                        SgtValue = HDF_Suggestion_Code.Value;
                        Where = Where.And(w => w.CustomerCode.Contains(SgtValue));
                        break;
                    case "customer.phone":
                        SgtValue = HDF_Suggestion_Code.Value;
                        Where = Where.And(w => w.CustomerCode.Contains(SgtValue));
                        break;
                    case "bill.code": break;
                }

                Where = Where.And(p => p.IsDelete != 1 && p.Pending == 1);
                int integer;

                if (!Perm_ViewAllData)
                {
                    SalonId = Session["SalonId"] != null ? (int.TryParse(Session["SalonId"].ToString(), out integer) ? integer : 0) : 0;

                }
                else
                {

                    SalonId = (int.TryParse(Salon.SelectedValue, out integer) ? integer : 0);

                }

                if (SalonId > 0)
                {

                    Where = Where.And(p => p.SalonId == SalonId);
                }
                else
                {

                    Where = Where.And(p => p.SalonId > 0);
                }


                var sql = @"select a.*, s.Fullname as HairdresserName, s2.Fullname as HairMassageName, s3.Fullname as CosmeticName, 
                            c.Fullname as CustomerName, c.Phone as CustomerPhone, t.Color as TeamColor 
                        from BillService as a
                        left join Staff as s
                        on a.Staff_Hairdresser_Id = s.Id
                        left join Staff as s2
                        on a.Staff_HairMassage_Id = s2.Id
                        left join Staff as s3
                        on a.SellerId = s3.Id
                        left join Customer as c
                        on a.CustomerCode = c.Customer_Code
                        left join TeamService as t
                        on a.TeamId = t.Id
                        where a.IsDelete != 1 and a.Pending = 1
                        and a.CreatedDate between '" + String.Format("{0:yyyy/MM/dd}", _FromDate) + "' and '" + String.Format("{0:yyyy/MM/dd}", _ToDate) + "'";
                if (SalonId > 0)
                {
                    sql += " and a.SalonId = " + SalonId;
                }
                sql += " order by a.CreatedDate";
                Count = db.Database.SqlQuery<BillService2>(sql).ToList().Count();
                //var totalBill = db.Database.SqlQuery<BillService2>(sql).ToList().Count;
                //if (totalBill != null)
                //    Total_Bill.InnerText = "(" + totalBill + " hóa đơn)";
                //else Total_Bill.InnerText = "(0 hóa đơn)";

                // Count = db.BillServices.AsExpandable().OrderBy(o=>o.CreatedDate).Count(Where);

                int TotalRow = Count - PAGING._TopNewsNum;
                int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
                return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
            }
        }
        public void Repeater_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
              || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //if (Perm_Edit == true)
                //    ((Panel)(e.Item.FindControl("EAedit"))).Visible = true;
                //if (Perm_Delete == true)
                //    ((Panel)(e.Item.FindControl("EAdelete"))).Visible = true;
            }
        }
    }

    public class BillService2 : BillService
    {
        public int billOrder { get; set; }
        public DateTime completeTime { get; set; }
        public string TeamColor { get; set; }
    }
}