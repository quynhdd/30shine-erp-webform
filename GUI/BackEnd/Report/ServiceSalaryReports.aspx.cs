﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.Interface;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;

namespace _30shine.GUI.BackEnd.Report
{
    public partial class ServiceSalaryReports : System.Web.UI.Page
    {
        private bool Perm_Access;
        private bool Perm_ViewAllData;
        private bool Perm_ShowSalon;

        /// <summary>
        /// check permission
        /// </summary>
        private void SetPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                int integer;
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_ShowSalon = permissionModel.CheckPermisionByAction("Perm_ShowSalon", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        /// <summary>
        /// Executeby permission
        /// </summary>
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        /// <summary>
        ///  Page load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                var date = DateTime.Now;
                date = date.AddMonths(-1);
                beforeMonth.Text = string.Format("{0:dd/MM/yyyy}", new DateTime(date.Year, date.Month, 1));
                beforeMonth.Attributes.Add("data-firstDay", string.Format("{0:dd/MM/yyyy}", new DateTime(date.Year, date.Month, 1)));
                beforeMonth.Attributes.Add("data-endDay", string.Format("{0:dd/MM/yyyy}", new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month))));
                sevenDayBefore.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-7));
                sevenDayBefore.Attributes.Add("data-firstDay", string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-7)));
                sevenDayBefore.Attributes.Add("data-endDay", string.Format("{0:dd/MM/yyyy}", DateTime.Now));
                toDay.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
                toDay.Attributes.Add("data-firstDay", string.Format("{0:dd/MM/yyyy}", DateTime.Now));
                toDay.Attributes.Add("data-endDay", string.Format("{0:dd/MM/yyyy}", DateTime.Now));
                //BindSalon();
                Library.Function.bindSalon(new List<DropDownList> { ddlSalon }, Perm_ViewAllData);
            }
        }

        //private void BindSalon()
        //{
        //    try
        //    {
        //        var SalonId = Convert.ToInt32(HttpContext.Current.Session["SalonId"]);
        //        var UserId = Convert.ToInt32(HttpContext.Current.Session["User_Id"]);
        //        using (var db = new Solution_30shineEntities())
        //        {
        //            var salonCurrent = db.Staffs.Where(r => r.Id == UserId).Select(r => new { Id = r.SalonId ?? 0 });

        //            var salonArea = db.PermissionSalonAreas.Where(r => ((r.StaffId == UserId && UserId > 0) || UserId == 0) &&
        //                                                               r.IsDelete == false &&
        //                                                               r.IsActive == true)
        //                                                               .Select(r => new { Id = r.SalonId ?? 0 });
        //            var listSalon = new List<Tbl_Salon>();
        //            if (Perm_ShowSalon)
        //            {
        //                listSalon = (from a in db.Tbl_Salon
        //                             where a.IsDelete == 0 && a.Publish == true && a.IsSalonHoiQuan == false
        //                             select a).OrderBy(r=>r.Order).ToList();
        //            }
        //            else
        //            {
        //                var listSalonId = salonCurrent.Union(salonArea).Where(r => r.Id != 0).Select(r => r.Id).ToList();

        //                listSalon = (from a in db.Tbl_Salon
        //                             where a.IsDelete == 0 && a.Publish == true && a.IsSalonHoiQuan == false && listSalonId.Contains(a.Id)
        //                             select a).OrderBy(r=>r.Order).ToList();
        //            }
        //            var ddls = new List<DropDownList>() { ddlSalon };
        //            for (var i = 0; i < ddls.Count; i++)
        //            {
        //                ddls[i].DataTextField = "Name";
        //                ddls[i].DataValueField = "Id";
        //                ddls[i].DataSource = listSalon;
        //                ddls[i].DataBind();
        //            }

        //            ddlSalon.Items.Insert(0, new ListItem("Chọn tất cả", "0"));

        //        }

        //    }
        //    catch (Exception e)
        //    {
        //        throw e;
        //    }
        //}

        /// <summary>
        /// bind data
        /// </summary>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="salonId"></param>
        /// <returns></returns>
        [WebMethod]
        public static object BindData(string fromDate, string toDate, int salonId)
        {
            try
            {
                if (string.IsNullOrEmpty(fromDate) || string.IsNullOrEmpty(toDate)) return null;
                var dateFrom = Convert.ToDateTime(fromDate, new CultureInfo("vi-VN"));
                var dateTo = Convert.ToDateTime(toDate, new CultureInfo("vi-VN"));
                string sql = $@"
                           DECLARE 
	                            @timeFrom DATE = '{string.Format("{0:yyyy-MM-dd}", dateFrom)}',
	                            @timeTo DATE  = '{string.Format("{0:yyyy-MM-dd}", dateTo)}',
	                            @salonId INT = {salonId};

	                            -- get total service salary 
                                WITH a AS (
		                            SELECT 
				                            si.SalonId
				                            ,s.Type
				                            ,MAX(st.Name) AS Department
				                            ,SUM(si.ServiceSalary) AS luong_dv
				                            ,SUM(si.FixedSalary) AS luong_cung
				                            ,SUM(si.BehaveSalary) AS luong_vhpv
				                            ,SUM(si.OvertimeSalary) AS luong_partime
		                            FROM dbo.SalaryIncome si 
		                            JOIN dbo.FlowTimeKeeping AS timekeeping ON si.StaffId = timekeeping.StaffId AND si.WorkDate = timekeeping.WorkDate
		                            INNER JOIN dbo.Staff s ON si.StaffId = s.Id
		                            INNER JOIN dbo.Staff_Type st ON s.Type = st.Id
		                            INNER JOIN dbo.Tbl_Salon sl ON si.SalonId = sl.Id
		                            WHERE si.WorkDate >= @timeFrom AND si.WorkDate <= @timeTo 
				                            AND ((si.SalonId = @salonId) OR (@salonId = 0))
				                            AND s.Type IN (1,2,4,5,6)
				                            AND si.IsDeleted = 0 
				                            AND sl.IsSalonHoiQuan = 0 
				                            AND si.SalonId NOT IN (24)
				                            AND timekeeping.IsEnroll = 1 
				                            AND timekeeping.IsDelete = 0
				                            AND si.FixedSalary >= 0
		                            GROUP BY si.SalonId,s.Type
		                            )
		                            --SELECT * FROM a ORDER BY a.SalonId
		                            ,
		                            -- salary income change
		                            o AS (
		                            SELECT sc.SalonId,s.Type
				                            ,SUM(CASE WHEN sc.ChangedType = 'luong_5c' THEN sc.Point ELSE 0 END) AS luong_5c
				                            ,SUM(CASE WHEN sc.ChangedType = 'bu_luong' THEN sc.Point ELSE 0 END) AS bu_luong
		                            FROM dbo.SalaryIncomeChange sc 
		                            INNER JOIN dbo.Staff s ON s.Id = sc.StaffId 
		                            WHERE sc.WorkDate >= @timeFrom AND sc.WorkDate <= @timeTo 
				                             AND sc.SalonId IS NOT NULL 
				                             AND ((sc.SalonId = @salonId) OR (@salonId = 0))
				                             AND sc.IsDeleted = 0
				                             AND s.Type IN (1,2,5,6,4)
		                             GROUP BY sc.SalonId,s.Type
		                            )
		                            --SELECT * FROM o ORDER BY o.SalonId
		                            ,
		                            -- 30shine care stylist
		                            stylist AS (
		                            SELECT sl.Id
				                            ,CASE WHEN s.Type IS NULL THEN 1 ELSE s.Type END AS Type 
				                            ,SUM(crm.StylistMoney) AS care_money  
		                            FROM dbo.CRM_VoucherWaitTime crm 
		                            LEFT JOIN dbo.Staff s ON s.Id = crm.StylistID 
		                            LEFT JOIN dbo.Tbl_Salon sl ON sl.Id = crm.SalonID
		                            WHERE   crm.CreatedTime >= @timeFrom AND crm.CreatedTime <= DATEADD(DAY,1,@timeTo) 
				                            AND ((crm.SalonID = @salonId) OR (@salonId = 0))
				                            AND crm.IsDelete = 0 
			                            GROUP BY sl.Id,s.Type
		                            )
		                            --SELECT * FROM stylist
		                            ,
		                            -- Get total service money
		                            service_money AS (
		                            SELECT 
			                             a.SalonId
			                            ,a.Type
										,a.luong_partime + a.luong_dv + o.bu_luong + (o.luong_5c) AS serviceMoney
		                            FROM a 
		                            LEFT JOIN o ON a.SalonId = o.SalonId AND a.Type = o.Type
		                            ),
		                            --SELECT * FROM service_money
		                            -- get total product salary
		                            b AS (
		                            SELECT 
				                            si.SalonId
				                            ,s.Type
				                            ,MAX(st.Name) AS Department
				                            ,SUM(si.ProductSalary) AS productSalary
		                            FROM dbo.SalaryIncome si 
		                            INNER JOIN dbo.Staff s ON si.StaffId = s.Id
		                            INNER JOIN dbo.Staff_Type st ON s.Type = st.Id
		                            INNER JOIN dbo.Tbl_Salon sl ON si.SalonId = sl.Id
		                            WHERE si.WorkDate >= @timeFrom AND si.WorkDate <= @timeTo
				                            AND ((si.SalonId = @salonId) OR (@salonId = 0))
				                            AND s.Type IN (1,2,5,6)
				                            AND si.IsDeleted = 0
				                            AND sl.IsSalonHoiQuan = 0
		                            GROUP BY si.SalonId,s.Type
		                            )
		                            ,
		                            -- switch row to column table a
		                            c AS (
		                            SELECT SalonId, [1] AS Stylist, [2] AS Skinner, [5] AS Checkin, [6] AS Checkout,[4] AS Sercurity_LSP   FROM (
			                            SELECT a.SalonId,a.serviceMoney,a.Type FROM service_money a
				                            ) AS sourcetable
				                            PIVOT(
				                            AVG(serviceMoney)
				                            FOR Type IN ([1],[2],[5],[6],[4])  
				                            ) AS PivotTable
		                            )
									--SELECT * FROM c
		                            ,
									-- fix salary
									u AS (
										SELECT SalonId, [1] AS Stylist, [2] AS Skinner, [5] AS Checkin, [6] AS Checkout,[4] AS Sercurity_LSP   FROM (
			                            SELECT a.SalonId,a.luong_cung,a.Type FROM a
				                            ) AS sourcetable
				                            PIVOT(
				                            AVG(luong_cung)
				                            FOR Type IN ([1],[2],[5],[6],[4])  
				                            ) AS PivotTable
									)
									--SELECT * FROM u
									,
									-- behavesalary
									h AS (
										SELECT SalonId, [1] AS Stylist, [2] AS Skinner, [5] AS Checkin, [6] AS Checkout,[4] AS Sercurity_LSP   FROM (
			                            SELECT a.SalonId,a.luong_vhpv,a.Type FROM a
				                            ) AS sourcetable
				                            PIVOT(
				                            AVG(luong_vhpv)
				                            FOR Type IN ([1],[2],[5],[6],[4])  
				                            ) AS PivotTable
									),
		                            -- switch row to column table b
		                            d AS (
			                            SELECT SalonId, [1] AS Stylist_LSP, [2] AS Skinner_LSP, [5] AS Checkin_LSP, [6] AS Checkout_LSP FROM (
			                            SELECT b.SalonId,b.productSalary,b.Type FROM b
			                            ) AS sourcetable
			                            PIVOT(
			                            AVG(productSalary)
			                            FOR Type IN ([1],[2],[5],[6])  
			                            ) AS PivotTable
		                            ),
		                             --GET product salary and service salary
		                            e AS (
		                            SELECT c.*
				                            ,d.Stylist_LSP
				                            ,d.Skinner_LSP
				                            ,d.Checkin_LSP
				                            ,d.Checkout_LSP
		                             FROM c
		                             INNER JOIN d ON c.SalonId = d.SalonId
		                            ),
		                             --GET total money of services not in ServiceId 69
		                            f AS (
		                            SELECT fs.SalonId
			                               ,SUM(CAST(fs.Price * (100-fs.VoucherPercent) * fs.Quantity/100 AS bigint)) AS totalServiceMoney 
		                            FROM dbo.FlowService fs 
		                            INNER JOIN dbo.BillServiceHis bs ON bs.Id = fs.BillId
		                            WHERE fs.CreatedDate >= @timeFrom AND fs.CreatedDate < DATEADD(DAY,1,@timeTo) 
				                            AND ((fs.SalonId = @salonId) OR (@salonId = 0))
				                            AND fs.IsDelete = 0
				                            AND bs.Pending = 0 
				                            AND bs.IsDelete = 0
				                            AND fs.ServiceId NOT IN (69)
		                            GROUP BY fs.SalonId
		                            ),
		                            -- Get total money of Product not in ProductId 689 category 98
		                            p AS (
		                            SELECT fs.SalonId
			                               ,SUM(CAST(fs.Price * (100-fs.VoucherPercent) * fs.Quantity/100 AS BIGINT)) AS totalProductMoney 
		                            FROM dbo.FlowProduct fs 
		                            INNER JOIN dbo.BillServiceHis bs ON bs.Id = fs.BillId
		                            INNER JOIN dbo.Product AS pro ON fs.ProductId = pro.Id
		                            WHERE fs.CreatedDate >= @timeFrom AND fs.CreatedDate < DATEADD(DAY,1,@timeTo) 
				                            AND ((fs.SalonId = @salonId) OR (@salonId = 0))
				                            AND fs.IsDelete = 0
				                            AND fs.ComboId IS  NULL
				                            AND fs.ProductId NOT IN (689)
				                            AND bs.Pending = 0 
				                            AND bs.IsDelete = 0
				                            AND pro.CategoryId NOT IN (98)
		                            GROUP BY fs.SalonId
		                            )
		                            ,
		                            -- get overtime salary
		                            g AS (
		                            SELECT 
				                            bs.SalonId
				                            ,SUM(CASE WHEN sb.OvertimeStatusValue = 1 THEN sb.ServiceIncomeBonus ELSE 0 END) AS OT_Salary
				                            ,SUM(CASE WHEN sb.OvertimeStatusValue = 2 THEN sb.ServiceIncomeBonus ELSE 0 END) AS OT_Allday_Salary
			                            FROM dbo.StaffBillServiceDetail sb 
			                            INNER JOIN dbo.BillServiceHis bs ON bs.Id = sb.BillId
			                            WHERE bs.CreatedDate >= @timeFrom AND bs.CreatedDate < DATEADD(DAY,1,@timeTo)
				                            AND ((bs.SalonId = @salonId) OR (@salonId = 0))
				                            AND sb.IsDelete = 0
			                            GROUP BY bs.SalonId
		                            ),
		                            shine_member AS(
			                            SELECT flow.SalonId
				                               ,SUM(flow.Quantity) AS total_shine_member 
				                               ,SUM(flow.Price * (100-flow.VoucherPercent) * flow.Quantity/100) AS total_price_shine_member 
			                            FROM dbo.FlowProduct AS flow
			                            JOIN dbo.Product AS product ON flow.ProductId = product.Id
			                             WHERE flow.CreatedDate >= @timeFrom AND flow.CreatedDate < DATEADD(DAY,1,@timeTo) 
					                             AND ((flow.SalonId = @salonId) OR (@salonId = 0))
					                             AND flow.IsDelete = 0 
					                             AND product.CategoryId = 98
			                            GROUP BY flow.SalonId
		                            ),
		                            protein_69 AS (
			                            SELECT SalonId
					                            ,SUM(Quantity) AS protein_69
					                            ,SUM(Price * (100-VoucherPercent) * Quantity/100) AS price_protein_69
			                             FROM dbo.FlowService
			                             WHERE CreatedDate >= @timeFrom AND CreatedDate < DATEADD(DAY,1,@timeTo) 
					                             AND ((SalonId = @salonId) OR (@salonId = 0))
					                             AND IsDelete = 0
					                             AND ServiceId = 69
			                               GROUP BY SalonId		
		                            ),
		                            protein_689 AS (
		                            SELECT SalonId
				                            ,SUM(Quantity) AS protein_689 
				                            ,SUM(Price * (100-VoucherPercent) * Quantity/100) AS price_protein_689
		                            FROM dbo.FlowProduct 
		                            WHERE CreatedDate >= @timeFrom AND CreatedDate < DATEADD(DAY,1,@timeTo) 
				                            AND ((SalonId = @salonId) OR (@salonId = 0))
				                            AND IsDelete = 0 
				                            AND ProductId = 689 
				                            GROUP BY SalonId
		                            ),
		                            -- 30shine care
		                            _30_shine_care AS (
			                            SELECT SalonID
				                            ,SUM(StylistMoney) AS stylistMoney
				                            ,SUM(SkinnerMoney) AS skinnerMoney
				                            ,SUM(CheckinMoney) AS checkinMoney
				                            ,0 AS checkoutMoney 
			                            FROM dbo.CRM_VoucherWaitTime 
			                            WHERE CreatedTime >= @timeFrom AND CreatedTime < DATEADD(DAY,1,@timeTo) 
				                            AND ((SalonID = @salonId) OR (@salonId = 0))
				                            AND IsDelete = 0
			                            GROUP BY SalonID
		                            )
		                            --SELECT * FROM dbo.Product WHERE Name LIKE N'%Protein%'
	                            SELECT 
		                            sl.ShortName AS Salon,
				                   -- fix salary
									ISNULL(ISNULL(u.Stylist ,0) + ISNULL(h.Stylist,0),0) AS luong_cung_stylist,
									ISNULL(ISNULL(u.Skinner ,0) + ISNULL(h.Skinner,0),0) AS luong_cung_skinner,
									ISNULL(ISNULL(u.Checkin ,0)+ ISNULL(h.Checkin,0),0) AS luong_cung_checkin,
									ISNULL(ISNULL(u.Checkout,0)+ ISNULL(h.Checkout,0),0) AS luong_cung_checkout,
									ISNULL(ISNULL(u.Sercurity_LSP,0)+ ISNULL(h.Sercurity_LSP,0),0) AS luong_cung_baove,
									-- service salary
		                            ISNULL(e.Stylist - ISNULL(sc.stylistMoney,0),0) AS luong_dv_stylist,
		                            ISNULL(e.Skinner - ISNULL(sc.skinnerMoney,0),0) AS luong_dv_skinner,
		                            ISNULL(e.Checkin - ISNULL(sc.checkinMoney,0),0) AS luong_dv_checkin,
		                            ISNULL(e.Checkout,0) AS luong_dv_checkout,
		                            CAST(ISNULL( e.Sercurity_LSP, 0) AS FLOAT) AS luong_dv_baove,
									-- product salary
		                            ISNULL(e.Stylist_LSP,0) AS luong_sp_stylist,
		                            ISNULL(e.Skinner_LSP,0) AS luong_sp_skinner,
		                            ISNULL(e.Checkin_LSP,0) AS luong_sp_checkin,
		                            ISNULL(e.Checkout_LSP,0) AS luong_sp_checkout,
		                            ISNULL(f.totalServiceMoney,0) AS tong_doanh_thu_dv,
		                            ISNULL(pro.totalProductMoney,0) AS tong_doanh_thu_mp,
		                            ISNULL( g.OT_Salary,0) AS tong_luong_tang_ca_tieng, 
		                            ISNULL(g.OT_Allday_Salary,0) AS tong_luong_tang_ca_ngay,
		                            ISNULL(sm.total_shine_member,0) AS total_shine_member,
		                            CAST(sm.total_price_shine_member AS FLOAT) AS total_price_shine_member ,
		                            ISNULL(p.protein_69,0) AS protein_69_service,
		                            CAST(ISNULL(p.price_protein_69, 0) AS FLOAT)  AS total_price_protein_69,
		                            ISNULL(pt.protein_689,0) AS total_protein_689_product,
		                            CAST(ISNULL(pt.price_protein_689, 0) AS FLOAT) AS total_price_protein_689
	                            FROM e
		                            INNER JOIN f ON f.SalonId = e.SalonId 
		                            INNER JOIN g ON g.SalonId = f.SalonId 
		                            INNER JOIN dbo.Tbl_Salon sl ON sl.Id = f.SalonId
		                            INNER JOIN p AS pro ON pro.SalonId = e.SalonId
		                            INNER JOIN shine_member sm ON sm.SalonId = e.SalonId
		                            LEFT JOIN protein_69 p ON p.SalonId = e.SalonId
		                            LEFT JOIN protein_689 pt ON pt.SalonId = e.SalonId
		                            LEFT JOIN [_30_shine_care] sc ON sc.SalonID = e.SalonId
									INNER JOIN u ON u.SalonId = e.SalonId
									INNER JOIN h ON h.SalonId = e.SalonId
	                            ORDER BY sl.CityId DESC";

                using (var db = new Solution_30shineEntities())
                {
                    // Set lai connection string sang bi
                    db.Database.Connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLConnectionStringBI"].ConnectionString;
                    var list = db.Database.SqlQuery<SalaryReport>(sql).ToList();
                    var recordTotal = new SalaryReport();
                    recordTotal.Salon = list.Count().ToString();

                    recordTotal.luong_cung_baove = list.Sum(r => r.luong_cung_baove);
                    recordTotal.luong_cung_checkin = list.Sum(r => r.luong_cung_checkin);
                    recordTotal.luong_cung_checkout = list.Sum(r => r.luong_cung_checkout);
                    recordTotal.luong_cung_skinner = list.Sum(r => r.luong_cung_skinner);
                    recordTotal.luong_cung_stylist = list.Sum(r => r.luong_cung_stylist);

                    recordTotal.luong_dv_baove = list.Sum(r => r.luong_dv_baove);
                    recordTotal.luong_dv_checkin = list.Sum(r => r.luong_dv_checkin);
                    recordTotal.luong_dv_checkout = list.Sum(r => r.luong_dv_checkout);
                    recordTotal.luong_dv_skinner = list.Sum(r => r.luong_dv_skinner);
                    recordTotal.luong_dv_stylist = list.Sum(r => r.luong_dv_stylist);
                    recordTotal.luong_sp_checkin = list.Sum(r => r.luong_sp_checkin);
                    recordTotal.luong_sp_checkout = list.Sum(r => r.luong_sp_checkout);
                    recordTotal.luong_sp_skinner = list.Sum(r => r.luong_sp_skinner);
                    recordTotal.luong_sp_stylist = list.Sum(r => r.luong_sp_stylist);
                    recordTotal.protein_69_service = list.Sum(r => r.protein_69_service);
                    recordTotal.total_price_protein_69 = list.Sum(r => r.total_price_protein_69);
                    recordTotal.total_protein_689_product = list.Sum(r => r.total_protein_689_product);
                    recordTotal.total_price_protein_689 = list.Sum(r => r.total_price_protein_689);
                    recordTotal.tong_doanh_thu_dv = list.Sum(r => r.tong_doanh_thu_dv);
                    recordTotal.tong_doanh_thu_mp = list.Sum(r => r.tong_doanh_thu_mp);
                    recordTotal.tong_luong_tang_ca_ngay = list.Sum(r => r.tong_luong_tang_ca_ngay);
                    recordTotal.tong_luong_tang_ca_tieng = list.Sum(r => r.tong_luong_tang_ca_tieng);
                    recordTotal.total_price_shine_member = list.Sum(r => r.total_price_shine_member);
                    recordTotal.total_shine_member = list.Sum(r => r.total_shine_member);
                    return new { total = recordTotal, list = list };
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// class custom
        /// </summary>
        public class SalaryReport
        {
            public string Salon { get; set; }
            public double luong_cung_stylist { get; set; }
            public double luong_cung_skinner { get; set; }
            public double luong_cung_checkin { get; set; }
            public double luong_cung_checkout { get; set; }
            public double luong_cung_baove { get; set; }
            public double luong_dv_stylist { get; set; }
            public double luong_dv_skinner { get; set; }
            public double luong_dv_checkin { get; set; }
            public double luong_dv_checkout { get; set; }
            public double luong_dv_baove { get; set; }
            public double luong_sp_stylist { get; set; }
            public double luong_sp_skinner { get; set; }
            public double luong_sp_checkin { get; set; }
            public double luong_sp_checkout { get; set; }
            public long? tong_doanh_thu_dv { get; set; }
            public long? tong_doanh_thu_mp { get; set; }
            public double tong_luong_tang_ca_tieng { get; set; }
            public double tong_luong_tang_ca_ngay { get; set; }
            public int total_shine_member { get; set; }
            public double total_price_shine_member { get; set; }
            public int protein_69_service { get; set; }
            public double total_price_protein_69 { get; set; }
            public int total_protein_689_product { get; set; }
            public double total_price_protein_689 { get; set; }
        }
    }
}