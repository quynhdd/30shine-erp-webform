﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="SaleReportChart.aspx.cs" Inherits="_30shine.GUI.BackEnd.Report.SaleReportChart" %>

<%@ Register Src="~/GUI/BackEnd/Report/ReportMenu.ascx" TagName="ReportMenu" TagPrefix="uc1" %>

<%--<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>--%>

<asp:Content ID="Content2" ContentPlaceHolderID="CtMain" runat="server">

    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>

    <asp:Panel CssClass="wp" ID="Panel1" runat="server">
        <uc1:ReportMenu runat="server" ID="ReportMenu" />
    </asp:Panel>


    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

        <%--sale report chart--%>
        <link href="/Assets/chartist/chartist.min.css" rel="stylesheet" />
        <script src="/Assets/chartist/chartist.js"></script>
        <script src="/Assets/js/chartist/chartist-plugin-pointlabels.js"></script>
        <script src="/Assets/js/chartist/chartist-plugin-legend.js"></script>
        <script src="/Assets/js/chartist/chartist-plugin-zoom.js"></script>
        <script src="/Assets/js/chartist/chartist-bar-labels.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />

        <link href="/Assets/css/report-chart.css" rel="stylesheet" />

        <%--sematic ui--%>
        <link href="/Assets/css/semantic.css" rel="stylesheet" />
        <script src="/Assets/js/semantic.js"></script>

        <div class="wp customer-add customer-listing be-report" onload="abc()">
            <%-- Listing --%>
            <div class="wp960 content-wp" style="border-top: 1px solid #d0d0d0; padding-top: 3px;">
                <div class="container no-padding">
                    <!-- Filter -->
                    <div class="row">
                        <div class="filter-item" style="margin-left: 0;">
                            <strong class="st-head" style="margin-right: 10px;"><i class="fa fa-clock-o"></i>Thời gian</strong>
                            <div class="datepicker-wp">
                                <input type="text" class="txtDateTime st-head form-control" id="TxtDateTimeFrom" placeholder="Từ ngày" />

                                <strong class="st-head" style="margin-left: 10px; margin-right: 10px;">
                                    <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                                </strong>
                                <input type="text" class="txtDateTime st-head form-control" id="TxtDateTimeTo" placeholder="Đến ngày" />

                            </div>

                            <div class="tag-wp" style="margin-top: 8px; padding: 0 10px;">
                                <p class="tag tag-date tag-date-today" onclick="viewDataByDate($(this),'today')">Hôm nay <span class="txt-date"></span></p>
                                <p class="tag tag-date" onclick="viewDataByDate($(this),'thisWeek')">Tuần này<span class="txt-date"></span></p>
                                <p class="tag tag-date " onclick="viewDataByDate($(this),'thisMonth')">Tháng này<span class="txt-date"></span></p>
                                <p class="tag tag-date " style="display: none" onclick="viewDataByDate($(this),'thisQuarter')">Quý này<span class="txt-date"></span></p>
                            </div>
                        </div>
                        <%--view time--%>
                        <div class="filter-item type-view" style="margin-left: 0;">
                            <div class="ui form">
                                <div class="inline fields" style="height: 42px; margin: 0px; padding: 0px;">
                                    <p style="font-family: Roboto Condensed Bold; margin: 0px; padding-right: 15px"><i class="fa fa-calendar" style="padding-right: 10px;"></i>Xem theo</p>
                                    <div class="field">
                                        <div class="ui radio checkbox date">
                                            <input type="radio" name="frequency" checked="checked" />
                                            <label>Ngày </label>
                                        </div>
                                    </div>
                                    <div class="field">
                                        <div class="ui radio checkbox week">
                                            <input type="radio" name="frequency" />
                                            <label>Tuần </label>
                                        </div>
                                    </div>
                                    <div class="field">
                                        <div class="ui radio checkbox month">
                                            <input type="radio" name="frequency" />
                                            <label>Tháng </label>
                                        </div>
                                    </div>
                                    <div class="field" style="display: none">
                                        <div class="ui radio checkbox quarter">
                                            <input type="radio" name="frequency" />
                                            <label>Quý </label>
                                        </div>
                                    </div>
                                    <div class="field" style="display: none">
                                        <div class="ui radio checkbox year">
                                            <input type="radio" name="frequency" />
                                            <label>Năm </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <%--<a href="/admin/bao-cao/bieu-do.html" class="st-head btn-viewdata">Bỏ lọc</a>--%>
                    </div>


                    <div class="col-xs-12 col-lg-12 col-md-12 col-sm-12 col-lg-12 no-padding wrap-office" style="display: block">
                        <div class="container no-padding">
                            <!-- Filter -->
                            <div class="row">
                                <strong class="st-head" style="margin-right: 20px;"><i class="fa fa-fort-awesome"></i>Salon</strong>

                                <div class="filter-item">
                                    <asp:DropDownList ID="Salon" CssClass="form-control select" runat="server" ClientIDMode="Static" Style="width: 190px;"></asp:DropDownList>
                                </div>

                                <strong class="st-head" style="margin-left: 20px;"><i class="fa fa-th-large"></i>Bộ phận</strong>
                                <ul class="filter-item ul-office">
                                    <li>
                                        <div class="ui checkbox skinner">
                                            <input type="checkbox" />
                                            <label>Skinner</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="ui checkbox stylist">
                                            <input type="checkbox" checked="checked" />
                                            <label>Stylist</label>
                                        </div>
                                    </li>
                                </ul>


                                <%--search new--%>
                                <div class="filter-item" style="margin-left: 20px; margin-top: 5px; width: 215px;">

                                    <div class='ui fluid search selection dropdown staff-list' style='min-height: 32px; height: 32px; border-radius: 0px; border: 1px solid #D0D6D8; padding: 8px;'>
                                        <input type='hidden' name='staff' /><i class='dropdown icon'></i><div class='default text'>Chọn nhân viên</div>

                                        <div class='menu'></div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>

                    <%--btn report--%>
                    <div class="col-xs-12 col-md-12 col-lg-12 col-sm-12 no-padding row  btn-report" style="border-top: 1px solid #d0d0d0; margin-bottom: 10px; border-bottom: 1px solid #d0d0d0">

                        <span id="btnReport"><i id="iconStatus" class="fa fa-line-chart" style="width: 18px; font-size: 15px; color: #50b347; margin-right: 5px;"></i>Xem báo cáo </span>


                        <ul class="tree-report">
                            <li class="item total">30Shine</li>
                            <li class="item salon" id="0"><i class="fa fa-chevron-right"></i>Tất cả Salon</li>
                            <li class="item staff"></li>
                        </ul>

                        <%--old report--%>
                        <div style="display: none">
                            <p id="btnReportSale" class="st-head btn-viewdata" runat="server">
                                Doanh thu 
                            </p>

                            <p id="btnReportBill" class="st-head btn-viewdata" runat="server">
                                Hóa đơn
                            </p>

                            <p id="btnReportBillByHours" class="st-head btn-viewdata" runat="server">
                                Hóa đơn theo giờ
                            </p>

                            <p id="btnReportSaleAvg" class="st-head btn-viewdata" runat="server">
                                Trung bình dịch vụ
                            </p>

                            <p id="btnReportService" class="st-head btn-viewdata" runat="server">
                                Doanh số dịch vụ
                            </p>

                            <p id="btnReportCustomer" class="st-head btn-viewdata" runat="server">
                                Khách cũ/mới
                            </p>

                            <p id="btnCustomerFamiliar" class="st-head btn-viewdata" runat="server">
                                Khách quen Stylist
                            </p>

                            <%-- <p id="btnReportStaff" class="st-head btn-viewdata">
                            Năng suất nhân viên
                        </p>--%>
                            <%--   <p id="btnCustomerOfStaff" class="st-head btn-viewdata">
                            Năng suất nhân viên
                        </p>--%>


                            <p id="btnCustomerAvgOfStaff" class="st-head btn-viewdata" runat="server">
                                Năng suất nhân viên
                            </p>

                            <p id="btnReportTotal" class="st-head btn-viewdata">
                                Nhân viên
                            </p>

                        </div>
                    </div>
                    <!-- End Filter -->

                    <div class="row">
                        <div class="profile" style="display: none;">
                            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                <div class="col-lg-3 col-md-3 col-xs-3 col-sm-3 avatar">
                                    <img id="avartar" src="" />
                                </div>
                                <div class="col-lg-8 col-md-8 col-xs-8 col-sm-8 info">
                                    <div class="co-lg-6 col-xs-6 col-md-6 col-sm-6">
                                        <p class="item name"></p>
                                        <p class="item phone"></p>
                                        <p class="item"><span class="level"></span>-<span class="item salon" style="padding-left: 0px;"></span></p>
                                    </div>
                                    <div class="co-lg-6 col-xs-6 col-md-6 col-sm-6">

                                        <p class="item index">TB Doanh thu: <span class="item index sale-avg" style="padding-left: 0px; font-size: 20px;"></span><span class="item unit unit-sale" style="padding-left: 5px"></span></p>
                                        <p class="item index">TB Hóa đơn: <span class="item index bill-avg" style="padding-left: 0px; font-size: 20px;"></span><span class="item unit unit-bill" style="padding-left: 5px"></span></p>
                                    </div>
                                </div>
                            </div>
                            <input id="ipChooseFileReal" type='file' style="display: none" />
                            <p id="uploadAvartar" style="display: none"><i class="fa fa-upload" style="display: none"></i>Up ảnh đại diện</p>
                        </div>
                    </div>

                    <%--Chart--%>
                    <div class="row">
                        <div class="wp-chart chart">
                            <div class="ct-chart line ct-golden-section">
                            </div>
                            <label></label>
                        </div>
                    </div>
                    <%--hien tai dang dung--%>
                    <%--many chart--%>
                    <div class="row wrap-all-chart-staff">

                        <%--report sale--%>
                        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 wp-all-chart-1 no-padding">

                            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 no-padding">
                                <div class="head">
                                    <p class="title"></p>
                                    <p class="caption"></p>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12 no-padding">
                                <div class="wp-chart chart1">
                                    <div class="ct-chart line ct-golden-section">
                                    </div>
                                    <label style="display: none"></label>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
                                <table id="tableStaff2" class="col-lg-12 col-md-12 col-xs-12 no-padding table-report">
                                    <thead class="col-lg-12 col-md-12 col-xs-12 no-padding">

                                        <tr class="col-lg-12 col-md-12 col-xs-12 head" style="padding: 0px;">
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <tr class="col-xs-12 no-padding" id="">
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <%--report bill--%>
                        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 wp-all-chart-2 no-padding">
                            <div class="head">
                                <p class="title"></p>
                                <p class="caption"></p>
                            </div>

                            <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12 no-padding">
                                <div class="wp-chart chart2">
                                    <div class="ct-chart line ct-golden-section">
                                    </div>
                                    <label style="display: none"></label>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12 no-padding">

                                <table id="tableStaff3" class="col-xs-12 no-padding table-report" style="padding: 0px;">
                                    <thead class="col-xs-12">
                                        <tr class="col-xs-12 head" style="padding: 0px;">
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <tr class="col-lg-12 col-md-12 col-xs-12" id="">
                                        </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>



                        <div class="col-lg-12 col-md-12 col-xs-12 wp-all-chart34 no-padding">
                            <%--report bill by hours--%>
                            <div class="col-lg-12 col-md-12 col-xs-12 no-padding">
                                <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12 no-padding wp-all-chart-3">
                                    <div class="head">
                                        <p class="title"></p>
                                        <p class="caption"></p>
                                    </div>
                                    <div class="wp-chart chart3">
                                        <div class="ct-chart line ct-golden-section">
                                        </div>
                                        <label style="display: none"></label>
                                    </div>
                                </div>
                                <%--report customer--%>
                                <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12 no-padding wp-all-chart-4">
                                    <div class="head">
                                        <p class="title"></p>
                                        <p class="caption"></p>
                                    </div>
                                    <div class="wp-chart chart4">
                                        <div class="ct-chart line ct-golden-section">
                                        </div>
                                        <label style="display: none"></label>
                                    </div>
                                </div>
                            </div>
                            <%--report service--%>
                        </div>

                        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 wp-all-chart-5 no-padding">
                            <div class="head">
                                <p class="title"></p>
                                <p class="caption"></p>
                            </div>
                            <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12 no-padding">
                                <div class="wp-chart chart5">
                                    <div class="ct-chart line ct-golden-section">
                                    </div>
                                    <label style="display: none"></label>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12 no-padding">
                                <table id="tableStaff5" class="col-xs-12 no-padding table-report" style="padding: 0px;">
                                    <thead class="col-xs-12 no-padding">

                                        <tr class="col-xs-12 no-padding head" style="padding: 0px;">
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <tr class="col-lg-12 col-md-12 col-xs-12 no-padding" id="">
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <%--chart 6 - danh gia --%>
                        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 wp-all-chart-6 no-padding" style="display: none">
                            <div class="head">
                                <p class="title"></p>
                                <p class="caption"></p>
                            </div>
                            <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12 no-padding">
                                <div class="wp-chart chart6">
                                    <div class="ct-chart line ct-golden-section">
                                    </div>
                                    <label style="display: none"></label>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12 no-padding">
                                <table id="tableStaff6" class="col-xs-12 no-padding table-report" style="padding: 0px;">
                                    <thead class="col-xs-12">
                                        <tr class="col-xs-12 no-padding head" style="padding: 0px;">
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <tr class="col-lg-12 col-md-12 col-xs-12 no-padding" id="">
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>


                        <%--chart 7 - danh gia rating --%>
                        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 wp-all-chart-7 no-padding">
                            <div class="head">
                                <p class="title"></p>
                                <p class="caption"></p>
                            </div>
                            <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12 no-padding">
                                <div class="wp-chart chart7">
                                    <div class="ct-chart line ct-golden-section">
                                    </div>
                                    <label style="display: none"></label>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12 no-padding">
                                <table id="tableStaff7" class="col-xs-12 no-padding table-report" style="padding: 0px;">
                                    <thead class="col-xs-12 no-padding">
                                        <tr class="col-xs-12 no-padding head" style="padding: 0px;">
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <tr class="col-lg-12 col-md-12 col-xs-12 no-padding" id="">
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>




                    </div>

                </div>

                <%--end hien tai dang dung--%>

                <%--old report staff--%>
                <div class="row">
                    <table id="tableStaff" class="col-xs-12 table-report">
                        <thead class="col-xs-12">
                            <tr class="col-xs-12 title" style="display: none">
                                <td></td>
                            </tr>

                            <tr class="col-lg-12 col-md-12 col-xs-12 head">
                            </tr>
                        </thead>

                        <tbody class="col-xs-12">
                        </tbody>
                    </table>
                </div>

                <div class="row">
                    <table id="tableStaffk" class="col-xs-12 table-report" style="display: none; padding: 0px;">
                        <thead class="col-xs-12">
                            <tr class="col-xs-12 title" style="display: none">
                                <td></td>
                            </tr>

                            <tr class="col-xs-12 head" style="padding: 0px;">
                                <td class="col-xs-2 col-first">Stylist</td>
                                <td class="col-xs-2">Ngày công</td>
                                <td class="col-xs-2 ">Tổng doanh thu</td>
                                <td class="col-xs-2 ">Doanh thu trung bình</td>
                                <td class="col-xs-2 ">Tổng doanh số</td>
                                <td class="col-xs-2 billAvg col-end">Doanh số trung bình</td>
                            </tr>

                        </thead>

                        <tbody class="col-xs-12">
                        </tbody>
                    </table>

                </div>


            </div>
        </div>

    </asp:Panel>
    <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_SingleReportData" />
    <asp:HiddenField runat="server" ClientIDMode="Static" ID="TypeViewTime" Value="day" />
    <asp:HiddenField runat="server" ClientIDMode="Static" ID="TypeViewChart" Value="line" />
    <asp:HiddenField runat="server" ClientIDMode="Static" ID="Report" Value="" />
    <asp:HiddenField runat="server" ClientIDMode="Static" ID="staffType" Value="1" />
    <asp:HiddenField runat="server" ClientIDMode="Static" ID="ResetStatus" Value="" />

    <!-- Hidden Field-->
    <asp:HiddenField ID="HDF_Suggestion_Code" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="HDF_Suggestion_Field" ClientIDMode="Static" runat="server" />
    <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
    <script src="../../../Assets/js/select2/select2.min.js"></script>
    <script>
        $('.select').select2();
    </script>
    <style>
        .select2-container {
            width: 190px !important;
            margin-top: 5px !important;
        }

        .select2-container--default .select2-selection--single {
            height: 32px !important;
            padding-top: 1px !important;
        }
    </style>
    <script>
        //vidible
        var $CtMain_Panel1 = $('#CtMain_Panel1');
        var $headTop = $('.wp.header');
        var $menuLeft = $('#MenurLeftHeade');
        var $gbMenu = $('#glbMenu');
        var $profile = $('.profile');
        var indexObj = { "saleAvg": 0, "billAvg": 0 };
        // new , only btn report for many charts
        var $btnReport = $('#btnReport');
        var $btn30Shine = $('.tree-report .total');
        var $btnSalon = $('.tree-report .salon');
        var $btnStaff = $('.tree-report .staff');
        var $searchBox = $('.filter-item .search');


        var $reSetStatus = $('#ResetStatus');
        var $staffType = $('#staffType');
        var $report = $('#Report');
        var $txtDateTime = $('.txtDateTime');
        var $iconStatus = $('#iconStatus');

        var $btnReportBill = $('#btnReportBill');
        var $btnReportBillByHours = $('#btnReportBillByHours');
        var $btnReportSale = $('#btnReportSale');
        var $btnReportSaleAvg = $('#btnReportSaleAvg');
        var $btnReportCustomer = $('#btnReportCustomer');
        var $btnReportStaff = $('#btnReportStaff');
        var $btnReportCustomerOfStaff = $('#btnCustomerOfStaff');
        var $btnReportCustomerAvgOfStaff = $('#btnCustomerAvgOfStaff');
        var $btnReportCustomerFamiliar = $('#btnCustomerFamiliar');
        var $btnReportService = $('#btnReportService');
        var $btnReportTotal = $('#btnReportTotal');


        var $infoChart = $('.wp-chart .info ul');
        var $tagDate = $('.tag-date');

        var $typeCheckBok = $('.type-view');
        var $typeViewTime = $('#TypeViewTime');
        var $typeDate = $typeCheckBok.find('.date');
        var $typeWeek = $typeCheckBok.find('.week');
        var $typeMonth = $typeCheckBok.find('.month');
        var $typeQuarter = $typeCheckBok.find('.quarter');
        var $typeYear = $typeCheckBok.find('.year');


        var $tableStaff = $('#tableStaff');
        var $tableStaff2 = $('#tableStaff2');
        var $tableStaff3 = $('#tableStaff3');
        var $tableStaff4 = $('#tableStaff4');
        var $tableStaff5 = $('#tableStaff5');


        var $salon = $('#Salon');

        var $wrapOffice = $('.wrap-office');
        var $stylist = $('ul-office .stylist');
        var $skinner = $('ul-office .skinner');
        var $ipSearchStaff = $('.be-report .filter-item .ip-short');
        var $customerName = $('#CustomerName');

        var $wpAllChart2 = $('.wp-all-chart-1');
        var $wrapAllChartStaff = $('.wrap-all-chart-staff');
        var $chart = $('.wp-chart.chart');
        var $chart1 = $('.wp-chart.chart1');
        var $chart2 = $('.wp-chart.chart2');
        //console.log($salon.find('option').attr('value'));

        jQuery(document).ready(function () {
            //visible
            $CtMain_Panel1.hide();
            $headTop.hide();
            $gbMenu.css("margin-left", "85px");


            // Add active menu
            $("#glbAdminReport").addClass("active");

            // Price format
            UP_FormatPrice('.be-report-price');

            //============================
            // Datepicker
            //============================
            $('.txtDateTime').datetimepicker({
                dayOfWeekStart: 1,
                lang: 'vi',
                startDate: '2014/10/10',
                format: 'd/m/Y',
                dateonly: true,
                showHour: false,
                showMinute: false,
                timepicker: false,

                onChangeDateTime: function (dp, $input) {
                    //$input.val($input.val().split(' ')[0]);
                }
            });

            // View data today
            $(".tag-date-today").click();

            //set 
            var $ctLegend = $('.ct-legend');
            $ctLegend.css({ "top": "100px", "left": "0px" })

            //dropdown list semantic 
            $searchBox.dropdown({
                onShow: function () {
                    //  $addNewAuthor.show();
                },
                onHide: function () {
                    setTimeout(function () {
                        //  $addNewAuthor.hide();
                    }, 300)

                }
            });

        });

        //display content
        function setDisplayContent() {
            var report = $report.val();
            switch (report) {
                case "bill":
                    $tableStaff.hide();
                    $chart.show();
                    $wrapAllChartStaff.hide();
                    break;
                case "billByHours":
                    $tableStaff.hide();
                    $chart.show();
                    $wrapAllChartStaff.hide();
                    break;
                case "saleAvg":
                    $tableStaff.hide();

                    $chart.show();
                    $wrapAllChartStaff.hide();
                    break;
                case "sale":
                    $tableStaff.hide();

                    $chart.show();
                    $wrapAllChartStaff.hide();
                    break;
                case "customer":
                    $tableStaff.hide();

                    $chart.show();
                    $wrapAllChartStaff.hide();
                    break;
                case "staff":
                    $tableStaff.hide();
                    $chart.hide();
                    $wrapAllChartStaff.show();
                    break;
                case "customerOfStaff":
                    $tableStaff.show();
                    $chart.show();
                    $wrapAllChartStaff.hide();
                    break;
                case "customerAvgOfStaff":
                    $tableStaff.show();
                    $chart.show();
                    $wrapAllChartStaff.hide();

                    break;
                case "customerFamiliar":
                    $tableStaff.show();
                    $chart.show();
                    $wrapAllChartStaff.hide();
                    break;
                case "service":
                    $tableStaff.hide();
                    $chart.show();
                    $wrapAllChartStaff.hide();
                    break;
            }
        }


        /*-----------CÁC LỰA CHỌN LỌC------------*/

        $txtDateTime.click(function () {
            $('.btn-viewdata').find('p').removeClass('active');
            $(".tag-date.active").removeClass("active");
        });

        $('.btn-report').find('p').click(function () {
            console.log("btn click");
            $('.btn-report').find('p').removeClass('active');
            $(this).addClass('active');
        });

        $txtDateTime.bind("change", function () {
            //console.log(" time change ");
            $reSetStatus.val("1");
        });

        $tagDate.bind("click", function () {
            //console.log(" time change ");
            $reSetStatus.val("1");
        });


        $typeDate.checkbox().first().checkbox({
            onChecked: function () {
                console.log("view type date");
                $typeViewTime.val("day");
                $('.btn-report').find('p').removeClass('active');
            }
        });


        $typeWeek.checkbox().first().checkbox({
            onChecked: function () {
                console.log("view type ");
                $typeViewTime.val("week");
                $('.btn-report').find('p').removeClass('active');
            }
        });



        $typeMonth.checkbox().first().checkbox({
            onChecked: function () {
                console.log("view type ");
                $typeViewTime.val("month");
                $('.btn-report').find('p').removeClass('active');
            }
        });



        $typeQuarter.checkbox().first().checkbox({
            onChecked: function () {
                console.log("view type");
                $typeViewTime.val("quarter");
                $('.btn-report').find('p').removeClass('active');
            }
        });


        $typeYear.checkbox().first().checkbox({
            onChecked: function () {
                console.log("view type ");
                $typeViewTime.val("year");
                $('.btn-report').find('p').removeClass('active');
            }
        });



        $('.stylist').checkbox().first().checkbox({
            onChecked: function () {
                $staffType.val("1");
                $reSetStatus.val("1");
                $('.skinner').checkbox('uncheck');

                var salonId = $('#Salon').find('option:selected').attr('value');
                var staffType = $('#staffType').val();
                setStaffToList(salonId, staffType);
            },
            onUnchecked: function () {
                $('.skinner').checkbox('check');
            }
        });

        $('.skinner').checkbox().first().checkbox({
            onChecked: function () {
                $staffType.val("2");
                $reSetStatus.val("1");
                $('.stylist').checkbox('uncheck');

                var salonId = $('#Salon').find('option:selected').attr('value');
                var staffType = $('#staffType').val();
                setStaffToList(salonId, staffType);
            },
            onUnchecked: function () {
                $('.stylist').checkbox('check');
            }
        });

        $salon.bind("change", function () {
            $reSetStatus.val("1");

            var salonId = $('#Salon').find('option:selected').attr('value');
            var staffType = $('#staffType').val();
            // console.log(salonId+"|"+ staffType);
            setStaffToList(salonId, staffType);
            //reset code 
            var $staffId = $('#HDF_Suggestion_Code');
            var $staffName = $('.staff-list .text span');
            $staffName.text("");
            $staffId.val("");
        })


        jQuery(document).ready(function () {
            //var salonId = $('#Salon').find('option:selected').attr('value');

            //mặc định: tất cả các salon và nhân viên toàn salon.
            var salonId = 0;
            var staffType = $('#staffType').val();
            setStaffToList(salonId, staffType);
        })




        /*
        *Hàm bind danh sách nhân viên

        *Tham số: 
        - salonId: id salon
        - staffType: kiểu nhân viên, stylist hoặc skinner

        *Hàm void
        */
        function setStaffToList(salonId, staffType) {
            var $authorList = $('.staff-list');

            $.ajax({
                type: 'POST',
                url: '/GUI/SystemService/Ajax/ReportService.aspx/GetStaffBySalon',
                data: '{salonId:"' + salonId + '",staffType:"' + staffType + '"}',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (response) {
                    var mission = JSON.parse(response.d);
                    if (mission.success) {
                        //console.log(mission.msg);
                        var data = JSON.parse(mission.msg);
                        var $lis = "";

                        $.each(data, function (i, v) {
                            var id = v.Id;
                            var name = v.Name;

                            $lis += "<div class='item' data-id='" + id + "' data-type='" + staffType + "' onClick=\"chooseStaff($(this))\"><i class='fa fa-user' style='padding-right:5px'></i><span>" + name + "</span></div>";

                        })
                        var $menu = $authorList.find('.menu');
                        $menu.empty().append($lis);

                    }

                }, failure: function (response) { alert(response.d); }
            });
        }

        /*Hàm lựa chọn nhân viên

        *Tham số:
        - This: đối tượng thực hiện hành động

        *Hàm void
        */
        function chooseStaff(This) {
            var id = This.attr("data-id");
            var $staffId = $('#HDF_Suggestion_Code');
            $staffId.val(id);
        }


        function setTreeReport(note) {
            var staffType = $('#staffType').val();
            var $staffId = $('#HDF_Suggestion_Code');
            var $salon = $('#Salon');
            //var $customerName = $('#CustomerName');
            var $customerName = $('.staff-list .text span');
            var staffId = $staffId.val();
            if (note == 2) {
                $staffId.val("");
                $customerName.text("");
                var salonId = $btnSalon.attr("id");
                $salon.val(salonId);

            }

            else if (note == 1) {
                $staffId.val("");
                $customerName.text("");
                $salon.val(0);
            }

            //var salonId = $salon.attr('value');
            var salonName = $salon.find('option:selected').text();
            var staffName = $customerName.text();
            if (salonName != "")
                $btnSalon.empty().append("<i class='fa fa-chevron-right'></i>" + salonName);
            else
                $btnSalon.empty();


            if (staffName != "") {
                $btnStaff.empty().append("<i class='fa fa-chevron-right'></i>" + staffName);
                $.ajax({
                    type: 'POST',
                    url: '/GUI/SystemService/Ajax/ReportService.aspx/GetSalonOfStaff',
                    data: '{ staffId: "' + staffId + '"}',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        var salon = JSON.parse(response.d);
                        $btnSalon.attr("id", salon.Id);
                        $btnSalon.empty().append("<i class='fa fa-chevron-right'></i>" + salon.Name);
                    }, failure: function (response) { alert(response.d); }
                });
            }
            else
                $btnStaff.empty();

            var salonId = $salon.val();
            var staffType = $('#staffType').val();

            setStaffToList(salonId, staffType);
        }

        $btn30Shine.click(function () {
            setTreeReport(1);
            $btnReport.click();
            $(this).parent().find('li').removeClass('active');
            $(this).addClass('active');

        })

        $btnSalon.click(function () {
            setTreeReport(2);

            $(this).parent().find('li').removeClass('active');
            $(this).addClass('active');
            $btnReport.click();

        })

        $btnStaff.click(function () {
            setTreeReport(0);
            $btnReport.click();
            $(this).parent().find('li').removeClass('active');
            $(this).addClass('active');
        })

        $btnReport.click(function () {
            console.log('click');
            $report.val("staff");
            setDisplayContent();
            setTreeReport(0);

            $typeDate.click();
            var from = $('#TxtDateTimeFrom').val();
            var to = $('#TxtDateTimeTo').val();
            var viewTime = $typeViewTime.val();
            var staffType = $('#staffType').val();
            var staffId = $('#HDF_Suggestion_Code').val();
            var salonId = $('#Salon').find('option:selected').attr('value');


            $btnStaff.parent().find('li').removeClass('active');
            if (salonId > 0) {
                $btnSalon.addClass('active');
            }

            if (to == "")
                to = from;
            if (from != "") {
                getDataTotal(salonId, staffType, staffId, from, to, viewTime);
            }
            if (staffId != "") {
                $btnStaff.parent().find('li').removeClass('active');
                $btnStaff.addClass('active');
            }
        })


        /*--------------------/ CÁC LỰA CHỌN LỌC -----------------------*/



        /*-------------CÁC HÀM XỬ LÝ DỮ LIỆU BÁO CÁO-------------
        ---------------CẬP NHẬT 3/2016 ----------------------*/
        /*
         *Hàm lấy dữ liệu báo cáo tổng

         Hiện tại báo cáo tổng chỉ xem theo ngày
         *Tham số:
         - salonId: id salon
         - from: thời gian từ 
         - to: thời gian đến
         - viewTime: kiểu xem (ngày, tháng, quý, năm)
     
         *Hàm void
         *Chú ý, dữ liệu dùng lại nhiều lần, nên load tất cả các dữ liệu rồi mới hiển thị báo cáo tổng để tiết kiệm vòng lặp xử lý.
         */
        function getDataTotal(salonId, staffType, staffId, from, to, viewTime) {
            $iconStatus.attr('class', 'fa fa-spinner fa-pulse');

            //kiểm tra lấy dữ liệu:
            //1. mã nhân viên khác rỗng thì lấy dữ liệu theo nhân viên trên toàn 30shine
            //2. mã nhân viên rỗng thì lấy dữ liệu theo salon
            if (staffId != null && staffId != "")
                salonId = 0;

            $.ajax({
                type: 'POST',
                url: '/GUI/SystemService/Ajax/ReportService.aspx/ReportTotal',
                data: '{ salonId: "' + salonId + '",from:"' + from + '",to:"' + to + '",viewTime:"' + viewTime + '",staffType:"' + staffType + '",staffId:"' + staffId + '"}',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (response) {
                    var data = JSON.parse(response.d);
                    //xem cấu trúc dữ liệu tổng
                    //console.log(data);

                    var number = data.Number;
                    //bind tiêu đề biểu đồ tổng
                    bindTitleAndCaption(data);
                    //dữ liệu doanh thu
                    var listSale = JSON.parse(data.Sale);
                    //dữ liệu hóa đơn
                    var listBill = JSON.parse(data.Bill);
                    //dữ liệu dịch vụ
                    var dataService = JSON.parse(data.Service);
                    //dữ liệu hóa đơn theo giờ
                    var dataBillByHour = JSON.parse(data.BillByHour);
                    //dữ liệu hóa đơn theo giờ
                    var dataCustomer = JSON.parse(data.Customer);
                    //dữ liệu nhân viên làm viêc (ngày làm, ngày nghỉ)
                    var dataStaffWork = JSON.parse(data.StaffWork);
                    //dữ liệu đánh giá dịch vụ
                    var dataRatingTotal = JSON.parse(data.RatingTotal);

                    var date = getSeries(listSale, "date");
                    var data1 = getSeries(listSale, "sum");
                    var data2 = getSeries(listBill, "count");
                    var data3 = getSeries(dataStaffWork, "count");
                    var data7 = getSeries(dataRatingTotal, "count");

                    labels1 = date;
                    series1 = [data1];
                    lines1 = ["Doanh thu"];

                    labels2 = date;
                    series2 = [data2];
                    lines2 = ["Doanh số"];

                    var arr3 = getDataChart(dataBillByHour, "billByHours");
                    var arr4 = getDataChart(dataCustomer, "customer");
                    var arr5 = getDataChart(dataService, "service");

                    console.log(arr4);

                    labels3 = arr3[0];
                    series3 = arr3[1];
                    lines3 = arr3[2];

                    labels4 = arr4[0];
                    series4 = arr4[1];
                    lines4 = arr4[2];

                    labels5 = ["", "", "", "", "", "", ""];
                    series5 = arr5[1];
                    lines5 = arr5[2];

                    labels7 = date;
                    series7 = [data7];
                    lines7 = ["Điểm"];

                    var chart1 = '.wp-chart.chart1 .ct-chart';
                    var chart2 = '.wp-chart.chart2 .ct-chart';
                    var chart3 = '.wp-chart.chart3 .ct-chart';
                    var chart4 = '.wp-chart.chart4 .ct-chart';
                    var chart5 = '.wp-chart.chart5 .ct-chart';
                    var chart6 = '.wp-chart.chart6 .ct-chart';
                    var chart7 = '.wp-chart.chart7 .ct-chart';

                    $iconStatus.attr('class', 'fa fa-line-chart');
                    //kiểm tra số lượng thống kê để bind biểu đồ:
                    //1. 1 mốc thời gian thống kê thì bind dữ liệu dạng cột
                    //2. nhiều hơn 1 mốc thì bìn dữ liệu dạng đường
                    var number = data.Number;
                    if (number > 1) {
                        bindChart(data.Title1, labels1, series1, lines1, "line", chart1);
                        bindChart(data.Title2, labels2, series2, lines2, "line", chart2);
                        bindChart(data.Title3, labels3, series3, lines3, "line", chart3);
                        bindChart(data.Title4, labels4, series4, lines4, "line", chart4);
                        //bindChart(data.Title6, labels6, series6, lines6, "line", chart6);
                        bindChart(data.Title7, labels7, series7, lines7, "line", chart7);
                    }
                    else if (number == 1) {
                        bindChart(data.Title1, labels1, series1, lines1, "bar", chart1);
                        bindChart(data.Title2, labels2, series2, lines2, "bar", chart2);
                        bindChart(data.Title3, labels3, series3, lines3, "bar", chart3);
                        bindChart(data.Title4, labels4, series4, lines4, "bar", chart4);
                        //bindChart(data.Title6, labels6, series6, lines6, "bar", chart6);
                        bindChart(data.Title7, labels7, series7, lines7, "bar", chart7);
                    }
                    bindChart(data.Title5, labels5, series5, lines5, "pice", chart5);

                    var title = data.Title;
                    var name = data.Name;
                    var dayWork = data.DayWork;

                    bindTable2(data, data1, data2, data3);
                    bindTable3(data, data1, data2, data3);
                    bindTable5(data, arr5);
                    bindTable7(data);

                    setProfile(data, "base");
                }, failure: function (response) { alert(response.d); }
            });
        }

        /*
        *Hàm đặt profile cho salon hoặc nhân viên

        *Tham số: 
        - data: dữ liệu thống kê
        - option: kiểu profile là salon hoặc nhân viên
        
        *Hàm void
        */
        function setProfile(data, option) {
            var $profile = $('.profile');
            $profile.show();
            console.log("set profile " + option);
            if (option == "base") {
                var $avartar = $profile.find('#avartar');
                var $name = $profile.find('.info .name');
                var $phone = $profile.find('.info .phone');
                var $level = $profile.find('.info .level');
                var $salon = $profile.find('.info .salon');

                if (data.Name != null && data.Name != "")
                    $name.text("Họ tên: " + data.Name);
                else $name.text("");
                if (data.Phone != null && data.Phone != "")
                    $phone.text("ĐT: " + data.Phone);
                else $phone.text("");
                if (data.Level != null && data.Level != "")
                    $level.text("Bậc: " + data.Level);
                else $level.text("");

                if (data.Salon != null && data.Salon != "")
                    $salon.text("Salon: " + data.Salon);

                else $salon.text("");
                if (data.Avartar != null && data.Avartar != "")
                    $avartar.attr("src", data.Avartar);
                else {
                    if (data.Name == null)
                        $avartar.attr("src", "/Assets/images/salon.png");
                    else
                        $avartar.attr("src", "/Assets/images/user.png");
                }

            } else if (option == "index") {
                var $saleAvg = $profile.find('.info .sale-avg');
                var $billAvg = $profile.find('.info .bill-avg');
                var $unitSale = $profile.find('.unit-sale');
                var $unitBill = $profile.find('.unit-bill');
                //$saleAvg.text("TB Doanh thu/Stylist/ngày: " + indexObj["saleAvg"]);
                var saleAvg = indexObj["saleAvg"];
                $unitSale.text(data.Unit1);
                $saleAvg.text(saleAvg);

                var billAvg = indexObj["billAvg"];
                $unitBill.text(data.Unit2);
                $billAvg.text(billAvg);

                // $billAvg.text("TB Hóa đơn/Stylist/ngày: " + indexObj["billAvg"] + " " + data.Unit2);
            }
        }

        /*
       *Hàm bind tiêu đề cho biểu đồ tổng

       *Tham số: 
       - data: dữ liệu thống kê
     
       *Hàm void
       */
        function bindTitleAndCaption(data) {
            var $title1 = $('.wp-all-chart-1 .head .title');
            var $title2 = $('.wp-all-chart-2 .head .title');
            var $title3 = $('.wp-all-chart-3 .head .title');
            var $title4 = $('.wp-all-chart-4 .head .title');
            var $title5 = $('.wp-all-chart-5 .head .title');
            var $title6 = $('.wp-all-chart-6 .head .title');
            var $title7 = $('.wp-all-chart-7 .head .title');

            var $caption1 = $('.wp-all-chart-1 .head .caption');
            var $caption2 = $('.wp-all-chart-2 .head .caption');
            var $caption3 = $('.wp-all-chart-3 .head .caption');
            var $caption4 = $('.wp-all-chart-4 .head .caption');
            var $caption5 = $('.wp-all-chart-5 .head .caption');
            var $caption6 = $('.wp-all-chart-6 .head .caption');
            var $caption7 = $('.wp-all-chart-7 .head .caption');

            $title1.text(data.Title1);
            $title2.text(data.Title2);
            $title3.text(data.Title3);
            $title4.text(data.Title4);
            $title5.text(data.Title5);
            $title6.text(data.Title6);
            $title7.text(data.Title7);

            $caption1.text("(đơn vị: " + data.Unit1 + ")");
            $caption2.text("(đơn vị: " + data.Unit2 + ")");
            $caption3.text("(đơn vị: " + data.Unit3 + ")");
            $caption4.text("(đơn vị: " + data.Unit4 + ")");
            $caption5.text("(đơn vị Biểu đồ: " + data.Unit5 + ", đơn vị Bảng: số lượng bill)");
            $caption6.text("(đơn vị: " + data.Unit6 + ")");
            $caption7.text("(đơn vị Biểu đồ: " + data.Unit7 + ", đơn vị Bảng: Bill, Tuyệt vời 3đ, Hài lòng 1đ, Chưa hài lòng 0đ)");
        }

        /*
        *Hàm bind bảng doanh thu dịch vụ  
        *Tham số:
        - data: thông tin báo cáo tổng
        - data1: thông tin doanh thu
        - data2: thông tin hóa đơn
        - data3: thông tin số ngày làm
        *Hàm void
        */
        function bindTable2(data, data1, data2, data3) {

            var $tableStaff2 = $('#tableStaff2');
            var $tbody = $tableStaff2.find('tbody');
            var $title = $tableStaff2.find('thead .title td');
            var $head = $tableStaff2.find('thead .head');

            $title.text(data.Title);

            var totalStaffWork = 0;
            var staffWork = 0;
            $.each(data3, function (index, value) {
                totalStaffWork += value;
            });

            var totalSale = 0;
            $.each(data1, function (index, value) {
                totalSale += value;
            });

            var unit1 = data.Unit1;
            //var unit2 = data.Unit2;

            var billAvg = 0;
            var saleAvg = 0;
            var dayWork = data.DayWork;



            var staffId = $('#HDF_Suggestion_Code').val();
            var salonName = $salon.find('option:selected').text();
            var staffName = $customerName.val();
            var col1Head = "";
            var col1Name = "";



            if (staffId == "") {
                col1Head = "Salon";
                col1Name = salonName;
                if (dayWork != 0 && totalStaffWork != 0) {
                    saleAvg = (totalSale / totalStaffWork).toFixed(2);
                    staffWork = (totalStaffWork / dayWork).toFixed(1);
                }
            } else {
                col1Head = "Stylist";
                col1Name = data.Name;
                if (dayWork != 0) {
                    saleAvg = (totalSale / dayWork).toFixed(2);
                }
                staffWork = 1;
            }

            totalSale = totalSale.toFixed(1);

            var $inTrHead = " <td class='col-lg-4 col-md-4 col-xs-4 col-first'>" + col1Head + "</td>" +
                "<td class='col-lg-2 col-md-2 col-xs-2'>Ngày công</td>" +
                "<td class='col-lg-2 col-md-2 col-xs-2'>Doanh thu</td>" +
                "<td class='col-lg-2 col-md-2 col-xs-2'>Stylist/ngày</td>" +
                "<td class='col-lg-2 col-md-2 col-xs-2 no-padding col-end'>TB DT/Stylist</td>";


            var $tr = "<tr  data-id='" + data.Id + "' class='col-lg-12 col-md-12 col-xs-12 no-padding'>" +
                "<td class='col-lg-4 col-md-4 col-xs-4 col-first'><div class='remove'><i class='fa fa-times' onclick='removeItem($(this)," + data.Id + ")'></i></div>" + col1Name + "</td>" +
                "<td class='col-lg-2 col-md-2 col-xs-2 day-work'>" + dayWork + "</td>" +
                "<td class='col-lg-2 col-md-2 col-xs-2 sales'>" + totalSale + "</td>" +
                "<td class='col-lg-2 col-md-2 col-xs-2 staffs'>" + staffWork + "</td>" +
                "<td class='col-lg-2 col-md-2 col-xs-2 sale-avg col-end'>" + saleAvg + "</td>";
            "</tr>";
            $head.empty().append($inTrHead);

            $tbody.append($tr);

            indexObj = { "saleAvg": saleAvg + "" };
            setProfile(data, "index");
        }

        /*
        *Hàm bind bảng doanh số dịch vụ
           
            *Tham số:
            - data: thông tin báo cáo tổng
            - data1: thông tin doanh thu
            - data2: thông tin hóa đơn
            - data3: thông tin số ngày làm
            *Hàm void
            */
        function bindTable3(data, data1, data2, data3) {

            var $tableStaff2 = $('#tableStaff3');
            var $tbody = $tableStaff2.find('tbody');
            var $title = $tableStaff2.find('thead .title td');
            var $head = $tableStaff2.find('thead .head');

            $title.text(data.Title);

            var totalStaffWork = 0;
            var staffWork = 0;
            console.log("staffWork");
            $.each(data3, function (index, value) {
                console.log(index + "|" + value);
                totalStaffWork += value;
            });

            var totalBill = 0;
            $.each(data2, function (index, value) {
                totalBill += value;
            });

            var unit2 = data.Unit2;
            var billAvg = 0;
            var dayWork = data.DayWork;
            var staffId = $('#HDF_Suggestion_Code').val();
            var salonName = $salon.find('option:selected').text();
            var staffName = $customerName.val();
            var col1Head = "";
            var col1Name = "";

            if (staffId == "") {
                col1Head = "Salon";
                col1Name = salonName;
                if (dayWork != 0 && totalStaffWork != 0) {
                    billAvg = (totalBill / totalStaffWork).toFixed(1);
                    staffWork = (totalStaffWork / dayWork).toFixed(1);
                }
            } else {
                col1Head = "Stylist";
                col1Name = data.Name;
                if (dayWork != 0) {
                    billAvg = (totalBill / dayWork).toFixed(1);
                }
                staffWork = 1;
            }

            var $inTrHead = " <td class='col-lg-4 col-md-4 col-xs-4 col-first'>" + col1Head + "</td>" +
                "<td class='col-lg-2 col-md-2 col-xs-2'>Ngày công</td>" +
                "<td class='col-lg-2 col-md-2 col-xs-2' >Doanh số</td>" +
                "<td class='col-lg-2 col-md-2 col-xs-2'>Stylist/ngày</td>" +
                "<td class='col-lg-2 col-md-2 col-xs-2 no-padding billAvg col-end'>TB DS/Stylist</td>";

            var $tr = "<tr  data-id='" + data.Id + "' class='col-lg-12 col-md-12 col-xs-12'>" +
                "<td class='col-lg-4 col-md-4 col-xs-4 col-first'><div class='remove'><i class='fa fa-times' onclick='removeItem($(this)," + data.Id + ")'></i></div>" + col1Name + "</td>" +
                "<td class='col-lg-2 col-md-2 col-xs-2 day-work'>" + dayWork + "</td>" +
                "<td class='col-lg-2 col-md-2 col-xs-2 bills'>" + totalBill + "</td>" +
                "<td class='col-lg-2 col-md-2 col-xs-2 staffs'>" + staffWork + "</td>" +
                "<td class='col-lg-2 col-md-2 col-xs-2 bill-avg col-end'>" + billAvg + "</td>"
            "</tr>";

            $head.empty().append($inTrHead);

            $tbody.append($tr);

            indexObj = { "billAvg": billAvg };
            setProfile(data, "index");
        }

        /*
       *Hàm bind bảng tỉ lệ dịch vụ
      
       *Tham số:
       - data: thông tin báo cáo tổng
       - arr5: mảng dữ liệu thông tin dịch vụ
       *Hàm void
       */
        function bindTable5(data, arr5) {
            //console.log(arr5);
            var $tableStaff5 = $('#tableStaff5');
            var $tbody = $tableStaff5.find('tbody');
            var $title = $tableStaff5.find('thead .title td');
            var $head = $tableStaff5.find('thead .head');

            var v = arr5[1];
            $title.text(data.Title);


            var staffId = $('#HDF_Suggestion_Code').val();
            var salonName = $salon.find('option:selected').text();
            var staffName = $customerName.val();
            var col1Head = "";
            var col1Name = "";

            if (staffId == "") {
                col1Head = "Salon";
                col1Name = salonName;
            } else {
                col1Head = "Stylist";
                col1Name = data.Name;
            }


            var $inTrHead = " <td class='col-lg-3 col-md-3 col-xs-3 col-first'>" + col1Head + "</td>" +
                "<td class='col-lg-2 col-md-2 col-xs-2'>SMaster</td>" +
                "<td class='col-lg-2 col-md-2 col-xs-2 '>SCombo</td>" +
                "<td class='col-lg-1 col-md-1 col-xs-1 ' style='padding:0 5px;'>SQuick</td>" +
                "<td class='col-lg-1 col-md-1 col-xs-1 ' style='padding:0 5px;'>TKiểu</td>" +
                "<td class='col-lg-1 col-md-1 col-xs-1 '>Tẩy</td>" +
                "<td class='col-lg-1 col-md-1 col-xs-1 '>Uốn</td>" +
                "<td class='col-lg-1 col-md-1 col-xs-1 billAvg col-end' style='padding:0 5px;'>ColorC</td>";

            var $tr = " <tr data-id='" + data.Id + "' class='col-lg-12 col-md-12 col-xs-12 no-padding' id=''>" +
                "<td class='col-xs-3 col-first'><div class='remove'><i class='fa fa-times' onclick='removeItem($(this)," + data.Id + ")'></i></div>" + col1Name + "</td>" +
                "<td class='col-xs-2'>" + v[0] + "</td>" +
                "<td class='col-xs-2 '>" + v[1] + "</td>" +
                "<td class='col-xs-1 '>" + v[2] + "</td>" +
                "<td class='col-xs-1 '>" + v[3] + "</td>" +
                "<td class='col-xs-1 '>" + v[4] + "</td>" +
                "<td class='col-xs-1 '>" + v[5] + "</td>" +
                "<td class='col-xs-1 col-end'>" + v[6] + "</td>" +
                "</tr>";

            $head.empty().append($inTrHead);

            $tbody.append($tr);

        }

        /*
        *Hàm bind bảng chất lượng dịch vụ
        - dùng trước 3/2016

        *Tham số:
        - data: thông tin báo cáo tổng

        *Hàm void
        */
        function bindTable6(data, data6) {
            //console.log(arr5);
            var $tableStaff6 = $('#tableStaff6');
            var $tbody = $tableStaff6.find('tbody');
            var $title = $tableStaff6.find('thead .title td');
            var $head = $tableStaff6.find('thead .head');
            var dataQuantityServey = JSON.parse(data.QuatityServey);
            var data6_0 = getSeries(JSON.parse(dataQuantityServey[0].Val), "count");
            var data6_1 = getSeries(JSON.parse(dataQuantityServey[1].Val), "count");
            // console.log(data6_0+"|"+data6_1);

            var total1 = 0;
            var total2 = 0;
            var total = 0;
            $.each(data6_0, function (index, value) {
                total1 += value;
            });

            $.each(data6_1, function (index, value) {
                total2 += value;
            });

            total = total1 + total2;
            $title.text(data.Title);

            var staffId = $('#HDF_Suggestion_Code').val();
            var salonName = $salon.find('option:selected').text();
            var staffName = $customerName.val();
            var col1Head = "";
            var col1Name = "";

            if (staffId == "") {
                col1Head = "Salon";
                col1Name = salonName;
            } else {
                col1Head = "Stylist";
                col1Name = data.Name;
            }


            var $inTrHead = "<tr class='col-xs-12 head' style='padding: 0px;'>" +
                "<td class='col-lg-4 col-md-4 col-xs-4 col-first'>Đối tượng</td>" +
                "<td class='col-lg-2 col-md-2 col-xs-2'>KHBM</td>" +
                "<td class='col-lg-3 col-md-3 col-xs-3'>FB</td>" +
                "<td class='col-lg-3 col-md-3 col-xs-3 billAvg col-end'>Tổng số</td>" +
                "</tr>"

            var $tr = "<tr data-id='" + data.Id + "' class='col-lg-12 col-md-12 col-xs-12' id='Tr1'></tr>" +
                "<tr id='' class='col-lg-12 col-md-12 col-xs-12'>" +
                "<td class='col-lg-4 col-md-4 col-xs-4 col-first'>" +
                "<div class='remove'><i class='fa fa-times' onclick='removeItem($(this)," + data.Id + ")'></i></div>" +
                col1Name + "</td>" +
                "<td class='col-lg-2 col-md-2 col-xs-2 day-work'>" + total1 + "</td>" +
                "<td class='col-lg-3 col-md-3 col-xs-3 bills'>" + total2 + "</td>" +
                "<td class='col-lg-3 col-md-3 col-xs-3 bill-avg col-end'>" + total + "</td>" +
                "</tr>";

            $head.empty().append($inTrHead);

            $tbody.append($tr);

        }


        /*
        *Hàm bind bảng đánh giá dịch vụ

        *Tham số:
        - data: thông tin báo cáo tổng

        *Hàm void
        */
        function bindTable7(data) {
            var $tableStaff7 = $('#tableStaff7');
            var $tbody = $tableStaff7.find('tbody');
            var $title = $tableStaff7.find('thead .title td');
            var $head = $tableStaff7.find('thead .head');

            var billNormal0 = data.BillNormal0;
            var billNormal1 = data.BillNormal1;
            var billNormal2 = data.BillNormal2;
            var billSpecial0 = data.BillSpecial0;
            var billSpecial1 = data.BillSpecial1;
            var billSpecial2 = data.BillSpecial2;
            var billNotRating = data.BillNotRating;

            var totalBillRating = 0;
            totalBillRating = parseInt(billNormal0) + parseInt(billNormal1) + parseInt(billNormal2) + parseInt(billSpecial0) + parseInt(billSpecial1) + parseInt(billSpecial2);
            console.log("total bill rating " + totalBillRating);
            var percentBillNormal0 = 0;
            var percentBillNormal1 = 0;
            var percentBillNormal2 = 0;
            var percentBillSpecial0 = 0;
            var percentBillSpecial1 = 0;
            var percentBillSpecial2 = 0;

            if (totalBillRating != 0) {
                percentBillNormal0 = ((billNormal0 / totalBillRating) * 100).toFixed(1);
                percentBillNormal1 = ((billNormal1 / totalBillRating) * 100).toFixed(1);
                percentBillNormal2 = ((billNormal2 / totalBillRating) * 100).toFixed(1);
                percentBillSpecial0 = ((billSpecial0 / totalBillRating) * 100).toFixed(1);
                percentBillSpecial1 = ((billSpecial1 / totalBillRating) * 100).toFixed(1);
                percentBillSpecial2 = ((billSpecial2 / totalBillRating) * 100).toFixed(1);
            }

            var staffId = $('#HDF_Suggestion_Code').val();
            var salonName = $salon.find('option:selected').text();
            var staffName = $customerName.val();
            var col1Head = "";
            var col1Name = "";

            if (staffId == "") {
                col1Head = "Salon";
                col1Name = salonName;
            } else {
                col1Head = "Stylist";
                col1Name = data.Name;
            }


            var $inTrHead =
                "<td class='col-7-head'></td>" +
                "<td class='col-2'>Bill bình thường</td>" +
                "<td class='col-2'>Bill khách quen/hóa chất</td>" +
                "<td class='col-7'>Chưa ĐG</td>" +

                "<td class='col-7-head'>" + col1Head + "</td>" +
                "<td class='col-7'><img class='icon-status' src='/assets/images/logo/1.png'></td>" +
                "<td class='col-7'><img class='icon-status' src='/assets/images/logo/2.png'></td>" +
                "<td class='col-7'><img class='icon-status' src='/assets/images/logo/3.png'></td>" +
                "<td class='col-7'><img class='icon-status' src='/assets/images/logo/1.png'></td>" +
                "<td class='col-7'><img class='icon-status' src='/assets/images/logo/2.png'></td>" +
                "<td class='col-7 '><img class='icon-status' src='/assets/images/logo/3.png'></td>" +
                "<td class='col-7'></td>"

            var $tr = " <tr data-id='" + data.Id + "' id=''>" +
                "<td class='col-7-head'><div class='remove'><i class='fa fa-times' onclick='removeItem($(this)," + data.Id + ")'></i></div>" + col1Name + "</td>" +
                "<td class='col-7'>" + billNormal2 + " (" + percentBillNormal2 + "%)</td>" +
                "<td class='col-7'>" + billNormal1 + " (" + percentBillNormal1 + "%)</td>" +
                "<td class='col-7'>" + billNormal0 + " (" + percentBillNormal0 + "%)</td>" +
                "<td class='col-7'>" + billSpecial2 + " (" + percentBillSpecial2 + "%)</td>" +
                "<td class='col-7'>" + billSpecial1 + " (" + percentBillSpecial1 + "%)</td>" +
                "<td class='col-7'>" + billSpecial0 + " (" + percentBillSpecial0 + "%)</td>" +
                "<td class='col-7'>" + billNotRating + "</td>" +
                "</tr>";


            $head.empty().append($inTrHead);

            $tbody.append($tr);

        }
        /*
        *Hàm xóa dòng trong bảng

        *Tham số: 
        - This: control thực hiện action
        - id: id đối tượng cần xóa

        *Hàm void
        */
        function removeItem(This, id) {
            var $tr = This.parent().parent().parent();
            var dataId = $tr.attr('data-id');
            var $tr = $('.table-report tr[data-id="' + dataId + '"]');
            $tr.remove();
        }

        /*
        *Hàm Bind dữ liệu vào biểu đồ

        *các tham số:
        - title: tiêu đề
        - labels: nhãn giá trị trục x
        - series: nhãn giá trị trục y
        - lines: chú thích nhãn các đường 
        - typeChart: kiểu hiển thị biểu đồ (đường, cột, thanh, bánh)
        - controlChart: vùng biểu đồ hiển thị

        *Hàm void
        */
        /*sử dụng thư viện: 
        chartist.js
        plugin lagend (thư viện giúp chú thích các đường)
        */
        function bindChart(title, labels, series, lines, typeChart, controlChart) {
            $(controlChart).empty();
            var $title = $(controlChart).parent().find('label');
            $title.text(title);

            switch (typeChart) {
                case "line":
                    var options = {
                        plugins: [
                            Chartist.plugins.legend({
                                legendNames: lines
                            }),
                            Chartist.plugins.ctPointLabels({
                                textAnchor: 'middle'
                            }),
                        ],

                        fullWidth: true,
                        lineSmooth: false,
                        chartPadding: {
                            right: 20,
                            left: 10
                        },
                    }
                    var data = {
                        labels: labels,
                        series: series
                    };
                    new Chartist.Line(controlChart, data, options);
                    break;

                case "bar":
                    var data = {
                        labels: labels,
                        series: series
                    };
                    var options = {
                        plugins: [
                            Chartist.plugins.legend({
                                legendNames: lines
                            }),
                            Chartist.plugins.ctPointLabels({
                                textAnchor: 'middle'
                            }),
                            Chartist.plugins.ctBarLabels()
                        ],
                        seriesBarDistance: 55
                    };
                    new Chartist.Bar(controlChart, data, options);

                    break;

                case "pice":
                    var data = {
                        labels: labels,
                        series: series
                    };

                    var options = {
                        plugins: [
                            Chartist.plugins.legend({
                                legendNames: lines
                            }),
                            Chartist.plugins.ctPointLabels({
                                textAnchor: 'middle'
                            }),
                        ],

                        fullWidth: true,
                        lineSmooth: false,
                        chartPadding: {
                            right: 20,
                            left: 10
                        },
                    }

                    var responsiveOptions = [
                        ['screen and (min-width: 640px)', {
                            chartPadding: 30,
                            labelOffset: 100,
                            labelDirection: 'explode',
                            labelInterpolationFnc: function (value) {
                                return value;
                            }
                        }],
                        ['screen and (min-width: 1024px)', {
                            labelOffset: 80,
                            chartPadding: 20
                        }]
                    ];
                    new Chartist.Pie(controlChart, data, options, responsiveOptions);
                    break;
            }
        }

        /*
        *Hàm bind dữ liệu vào biểu đồ
        - hàm dùng cho các biểu đồ đơn
        - dùng trước 3/2016
       
       *các tham số:
       - data: thông tin thống kê
       - typeData: kiểu dữ liệu phân theo nghiệp vụ
       - typeChart: kiểu hiển thị biểu đồ (đường, cột, thanh, bánh)

       *hàm void
       */
        /*sử dụng thư viện: 
        chartist.js
        plugin lagend (thư viện giúp chú thích các đường)
        */
        function bindDataChart(data, typeData, typeChart) {
            var title = data.Title;

            var $title = $('.wp-chart.chart label');
            var labels = "";
            var series = "";
            var lines = "";
            var $filterStoreName = $('#Salon').find('option:selected');
            $title.text(title);

            //xử lý dữ liệu đầu vào theo phân loại dữ liệu nghiệp vụ
            //dữ liệu phân theo nghiệp vụ: hóa đơn, hóa đơn theo giờ, doanh thu trung bình, khách hàng, doanh thu, nhân viên, loại khách hàng, khách quen, loại dịch vụ
            switch (typeData) {

                case "bill":
                    var billServiceTotal = JSON.parse(data.BillServiceTotal);
                    var billCosmeticTotal = JSON.parse(data.BillCosmeticTotal);
                    var billCosmeticOnline = JSON.parse(data.BillCosmeticOnline);
                    var billCosmeticSalon = JSON.parse(data.BillCosmeticSalon);

                    var date = getSeries(billServiceTotal, "date");
                    var data1 = getSeries(billServiceTotal, "count");
                    var data4 = getSeries(billCosmeticTotal, "count");
                    var data5 = getSeries(billCosmeticOnline, "count");
                    var data6 = getSeries(billCosmeticSalon, "count");

                    labels = date;
                    series = [data1, data4, data5, data6];
                    console.log(series);
                    lines = ["Tổng hóa đơn dịch vụ", "Tổng hóa đơn mỹ phẩm", "Hóa đơn mỹ phẩm Online", "Hóa đơn mỹ phẩm Salon"]
                    break;

                case "billByHours":
                    var billsByHours = JSON.parse(data.Hours);
                    var hours = ["8h-9h", "9h-10h", "10h-11h", "11h-12h", "12h-13h", "13h-14h", "14h-15h", "15h-16h", "16h-17h", "17h-18h", "18h-19h", "19h-20h", "20h-21h", "21h-22h"];

                    var bills = [billsByHours.h_8, billsByHours.h_9, billsByHours.h_10, billsByHours.h_11, billsByHours.h_12, billsByHours.h_13, billsByHours.h_14, billsByHours.h_15, billsByHours.h_16, billsByHours.h_17, billsByHours.h_18, billsByHours.h_19, billsByHours.h_20, billsByHours.h_21, billsByHours.h_22];

                    labels = hours;
                    series = [bills];
                    lines = ["Hóa đơn theo giờ"]
                    break;

                case "saleAvg":
                    var listAvg = JSON.parse(data.Avg);
                    var date = getSeries(listAvg, "date");
                    var avgs = getSeries(listAvg, "avg");

                    labels = date;
                    series = [avgs];
                    lines = ["Giá trị trung bình của hóa đơn"]
                    break;


                case "customer":
                    var billServiceCustomerOld = JSON.parse(data.BillServiceCustomerOld);
                    var billServiceCustomerNew = JSON.parse(data.BillServiceCustomerNew);
                    var date = getSeries(billServiceCustomerOld, "date");
                    var data1 = getSeries(billServiceCustomerOld, "count");
                    var data2 = getSeries(billServiceCustomerNew, "count");

                    labels = date;

                    series = [data1, data2];
                    lines = ["Khách cũ", "Khách mới",]
                    console.log(series);
                    break;

                case "sale":
                    var saleService = JSON.parse(data.SaleService);
                    var saleCosmetic = JSON.parse(data.SaleCosmetic);
                    var saleCosmeticOnline = JSON.parse(data.SaleCosmeticOnline);
                    var saleCosmeticSalon = JSON.parse(data.SaleCosmeticSalon);

                    var date = getSeries(saleService, "date");
                    var data1 = getSeries(saleService, "sum");
                    var data2 = getSeries(saleCosmetic, "sum");
                    var data3 = getSeries(saleCosmeticOnline, "sum");
                    var data4 = getSeries(saleCosmeticSalon, "sum");


                    labels = date;
                    series = [data1, data2, data3, data4];
                    lines = ["Doanh thu dịch vụ", "Doanh thu mỹ phẩm", "Doanh thu mỹ phẩm online", "Doanh thu mỹ phẩm Salon"]
                    break;


                case "staff":
                    var $title1 = $('.wp-chart.chart1 label');
                    var $title2 = $('.wp-chart.chart2 label');
                    var $title3 = $('.wp-chart.chart3 label');
                    var $title4 = $('.wp-chart.chart4 label');
                    var $title5 = $('.wp-chart.chart5 label');

                    var title1 = data.Title1;
                    var title2 = data.Title2;
                    var title3 = data.Title3;
                    var title4 = data.Title4;
                    var title5 = data.Title5;

                    $title1.text(title1);
                    $title2.text(title2);
                    $title3.text(title3);
                    $title4.text(title4);
                    $title5.text(title5);


                    var listSale = JSON.parse(data.Sale);
                    var listBill = JSON.parse(data.Bill);
                    var dataService = JSON.parse(data.Service);

                    var date = getSeries(listSale, "date");
                    var data1 = getSeries(listSale, "sum");
                    var data2 = getSeries(listBill, "count");
                    var arr5 = getDataChart(dataService, "service");

                    labels1 = date;
                    series1 = [data1];
                    lines1 = ["Doanh thu"];

                    labels2 = date;
                    series2 = [data2];
                    lines2 = ["Doanh số"];

                    labels5 = arr5[0];
                    series5 = arr5[1];
                    lines5 = arr5[2];

                    var title = data.Title;
                    var name = data.Name;
                    var dayWork = data.DayWork;

                    bindTable2(data, data1, data2);

                    break;
                case "customerOfStaff":
                    var list = JSON.parse(data.CustomerOfStaff);
                    var listDataCount = [];
                    var date = "";
                    if (list.length > 0) {
                        $.each(list, function (i, v) {
                            var countByDate = JSON.parse(v.CountByDate);
                            var data = getSeries(countByDate, "count");
                            if (i == 0) {
                                date = getSeries(countByDate, "date");
                            }
                            listDataCount.push(data);
                        })
                    }

                    var names = getSeries(list, "name");
                    var counts = getSeries(list, "avg");

                    labels = date;
                    series = listDataCount;
                    lines = names;
                    break;


                case "customerAvgOfStaff":
                    console.log(data);
                    var list = JSON.parse(data.CustomerAvgOfStaff);
                    var names = getSeries(list, "name");
                    var counts = getSeries(list, "avg");

                    labels = names;
                    series = [counts];
                    lines = ["Stylist"]
                    console.log("line" + lines);
                    break;


                case "customerFamiliar":
                    var list = JSON.parse(data.CustomerOfStaff);
                    var listDataCount = [];
                    var date = "";
                    if (list.length > 0) {
                        $.each(list, function (i, v) {
                            var countByDate = JSON.parse(v.CountByDate);
                            var data = getSeries(countByDate, "count");
                            if (i == 0) {
                                date = getSeries(countByDate, "date");
                            }
                            listDataCount.push(data);
                        })
                    }

                    var names = getSeries(list, "name");
                    var counts = getSeries(list, "count");
                    labels = date;
                    series = listDataCount;
                    lines = names;

                    break;

                case "service":
                    var count1 = JSON.parse(data.ShineMaster);
                    var count2 = JSON.parse(data.ShineCombo);
                    var count3 = JSON.parse(data.ShineQuick);
                    var count4 = JSON.parse(data.CutStyle);
                    var count5 = JSON.parse(data.HairRemove);
                    var count6 = JSON.parse(data.ComboU);
                    var count7 = JSON.parse(data.ComboC);
                    var total = count1 + count2 + count3 + count4 + count5 + count6 + count7;
                    var percent1 = ((count1 / total) * 100).toFixed(1);
                    var percent2 = ((count2 / total) * 100).toFixed(1);
                    var percent3 = ((count3 / total) * 100).toFixed(1);
                    var percent4 = ((count4 / total) * 100).toFixed(1);
                    var percent5 = ((count5 / total) * 100).toFixed(1);
                    var percent6 = ((count6 / total) * 100).toFixed(1);
                    var percent7 = ((count7 / total) * 100).toFixed(1);

                    var label1 = "Shine Master " + count1 + " (" + percent1 + " %)";
                    var label2 = "Shine Combo " + count2 + " (" + percent2 + " %)";
                    var label3 = "Shine Quick " + count3 + " (" + percent3 + " %)";
                    var label4 = "Cắt tóc tạo kiểu " + count4 + " (" + percent4 + " %)";
                    var label5 = "Tẩy tóc " + count5 + " (" + percent5 + " %)";
                    var label6 = "Combo uốn " + count6 + " (" + percent6 + " %)";
                    var label7 = "Color Combo " + count7 + " (" + percent7 + " %)";


                    labels = [percent1, percent2, percent3, percent4, percent5, percent6, percent7];
                    series = [count1, count2, count3, count4, count5, count6, count7];
                    lines = [label1, label2, label3, label4, label5, label6, label7];
            }

            $iconStatus.attr('class', 'fa fa-line-chart');

            //bind dữ liệu cho kiểu biểu đồ
            switch (typeChart) {

                case "line":
                    var options = {
                        //height: '400px',
                        //width: '1000px',
                        plugins: [
                            Chartist.plugins.legend({
                                legendNames: lines
                            }),
                            Chartist.plugins.ctPointLabels({
                                textAnchor: 'middle'
                            }),
                            // Chartist.plugins.zoom({ onZoom: onZoom })
                        ],

                        fullWidth: true,
                        lineSmooth: false,
                        chartPadding: {
                            right: 20,
                            left: 10
                        },
                    }

                    if (typeData == "staff") {
                        var data1 = {
                            labels: labels1,
                            series: series1
                        };

                        var data2 = {
                            labels: labels2,
                            series: series2
                        };

                        var data5 = {
                            labels: labels5,
                            series: series5
                        };

                        $('.wp-chart.chart1 .ct-chart').empty();
                        new Chartist.Line('.wp-chart.chart1 .ct-chart', data1, options);

                        $('.wp-chart.chart2 .ct-chart').empty();
                        new Chartist.Line('.wp-chart.chart2 .ct-chart', data2, options);

                    } else {
                        var data = {
                            labels: labels,
                            series: series
                        };

                        $('.wp-chart.chart .ct-chart').empty();
                        new Chartist.Line('.wp-chart.chart .ct-chart', data, options);
                    }

                    //pice               
                    var responsiveOptions = [
                        ['screen and (min-width: 640px)', {
                            chartPadding: 30,
                            labelOffset: 100,
                            labelDirection: 'explode',
                            labelInterpolationFnc: function (value) {
                                return value;
                            }
                        }],
                        ['screen and (min-width: 1024px)', {
                            labelOffset: 80,
                            chartPadding: 20
                        }]
                    ];

                    console.log(data5);
                    $('.wp-chart.chart5 .ct-chart').empty();
                    new Chartist.Pie('.wp-chart.chart5 .ct-chart', data5, options, responsiveOptions);

                    break;

                case "bar":

                    var options = {
                        plugins: [
                            Chartist.plugins.legend({
                                legendNames: lines
                            }),
                            Chartist.plugins.ctPointLabels({
                                textAnchor: 'middle'
                            }),
                            Chartist.plugins.ctBarLabels()
                        ],
                        seriesBarDistance: 55
                    };

                    if (typeData == "staff") {
                        var data1 = {
                            labels: labels1,
                            series: series1
                        };

                        var data2 = {
                            labels: labels2,
                            series: series2
                        };

                        $('.wp-chart.chart2 .ct-chart').empty();
                        new Chartist.Bar('.wp-chart.chart2 .ct-chart', data1, options);

                        $('.wp-chart.chart3 .ct-chart').empty();
                        new Chartist.Bar('.wp-chart.chart3 .ct-chart', data2, options);
                        break;
                    } else {
                        var data = {
                            labels: labels,
                            series: series
                        };

                        $('.wp-chart.chart .ct-chart').empty();
                        new Chartist.Bar('.wp-chart.chart .ct-chart', data, options);
                    }

                    break;

                case "pice":
                    var data = {
                        labels: labels,
                        series: series
                    };

                    var options = {
                        //height: '400px',
                        //width: '1000px',
                        //labelInterpolationFnc: function (value) {
                        //    return value[0]
                        //},
                        plugins: [
                            Chartist.plugins.legend({
                                legendNames: lines
                            }),
                            Chartist.plugins.ctPointLabels({
                                textAnchor: 'middle'
                            }),
                            // Chartist.plugins.zoom({ onZoom: onZoom })
                        ],

                        fullWidth: true,
                        lineSmooth: false,
                        chartPadding: {
                            right: 20,
                            left: 10
                        },
                    }

                    var responsiveOptions = [
                        ['screen and (min-width: 640px)', {
                            chartPadding: 30,
                            labelOffset: 100,
                            labelDirection: 'explode',
                            labelInterpolationFnc: function (value) {
                                return value;
                            }
                        }],
                        ['screen and (min-width: 1024px)', {
                            labelOffset: 80,
                            chartPadding: 20
                        }]
                    ];

                    $('.ct-chart').empty();
                    new Chartist.Pie('.ct-chart', data, options, responsiveOptions);
                    break;

                case "many":

            }
        }

        /*
        *hàm chuyển đổi dữ liệu lấy từ service sang dạng dữ liệu biểu đồ
        *các tham số:
        - data: dữ liệu thống kê báo cáo tổng
        - typeData: dữ liệu theo chức năng nghiệp vụ(dịch vụ, hóa đơn theo giờ, khách hàng )
        *kết quả: trả ra mảng các giá trị dữ liệu dùng cho biểu đồ
        */
        function getDataChart(data, typeData) {
            var labels = "";
            var series = "";
            var lines = "";
            var arr = [];

            switch (typeData) {

                case "service":
                    //dữ liệu thống kê dịch vụ
                    //xem cấu trúc dữ liệu
                    //console.log(data.ShineMaster);
                    //console.log(data.ShineCombo)
                    //console.log(data.ShineCombo);
                    //console.log(data.ShineQuick);
                    //console.log(data.CutStyle);
                    //console.log(data.HairRemove);
                    //console.log(data.ComboU);
                    //console.log(data.ComboC);

                    var count1 = JSON.parse(data.ShineMaster);
                    var count2 = JSON.parse(data.ShineCombo);
                    var count3 = JSON.parse(data.ShineQuick);
                    var count4 = JSON.parse(data.CutStyle);
                    var count5 = JSON.parse(data.HairRemove);
                    var count6 = JSON.parse(data.ComboU);
                    var count7 = JSON.parse(data.ComboC);
                    //tính tổng các loại dịch vụ và % loại dịch vụ
                    var total = count1 + count2 + count3 + count4 + count5 + count6 + count7;
                    var percent1 = ((count1 / total) * 100).toFixed(1);
                    var percent2 = ((count2 / total) * 100).toFixed(1);
                    var percent3 = ((count3 / total) * 100).toFixed(1);
                    var percent4 = ((count4 / total) * 100).toFixed(1);
                    var percent5 = ((count5 / total) * 100).toFixed(1);
                    var percent6 = ((count6 / total) * 100).toFixed(1);
                    var percent7 = ((count7 / total) * 100).toFixed(1);

                    //ghi nhãn các dịch vụ
                    var label1 = "Shine Master  (" + percent1 + " %)";
                    var label2 = "Shine Combo  (" + percent2 + " %)";
                    var label3 = "Shine Quick  (" + percent3 + " %)";
                    var label4 = "Cắt tóc tạo kiểu  (" + percent4 + " %)";
                    var label5 = "Tẩy tóc  (" + percent5 + " %)";
                    var label6 = "Combo uốn  (" + percent6 + " %)";
                    var label7 = "Color Combo (" + percent7 + " %)";

                    labels = [percent1, percent2, percent3, percent4, percent5, percent6, percent7];
                    series = [count1, count2, count3, count4, count5, count6, count7];
                    lines = [label1, label2, label3, label4, label5, label6, label7];

                    break;
                case "billByHours":
                    //dữ liệu thống kê theo giờ
                    //xem cấu trúc dữ liệu
                    //console.log(data.Hours);
                    var hours = ["8h-9h", "9h-10h", "10h-11h", "11h-12h", "12h-13h", "13h-14h", "14h-15h", "15h-16h", "16h-17h", "17h-18h", "18h-19h", "19h-20h", "20h-21h", "21h-22h"];

                    var billsByHours = JSON.parse(data.Hours);
                    var bills = [billsByHours.h_8, billsByHours.h_9, billsByHours.h_10, billsByHours.h_11, billsByHours.h_12, billsByHours.h_13, billsByHours.h_14, billsByHours.h_15, billsByHours.h_16, billsByHours.h_17, billsByHours.h_18, billsByHours.h_19, billsByHours.h_20, billsByHours.h_21, billsByHours.h_22];
                    labels = hours;
                    series = [bills];
                    lines = ["Hóa đơn theo giờ"]

                    break;



                case "customer":
                    //trường hợp báo cáo phân loại khách hàng
                    //tính tổng các phân loại khách hàng bằng cách cộng phân loại khách hàng theo ngày từ dữ liệu thống kê
                    //Để hình dung dữ liệu thống kê, xem chi tiết cấu trúc đối tượng thống kê CustomerOfStaff trong ReportService
                    //xem console.log(data.CustomerOfStaff)
                    var list = JSON.parse(data.CustomerOfStaff);
                    var data = [];
                    var data2 = [];
                    var data3 = [];

                    var date = "";
                    if (list.length > 0) {
                        $.each(list, function (i, v) {
                            var countByDate = JSON.parse(v.CountByDate);
                            var data0 = getSeries(countByDate, "count");
                            var data02 = getSeries(countByDate, "count2");
                            var data03 = getSeries(countByDate, "count3");
                            if (i == 0) {
                                date = getSeries(countByDate, "date");
                                $.each(date, function (i, v) {
                                    data.push(0);
                                    data2.push(0);
                                    data3.push(0);
                                })
                            }

                            $.each(data0, function (i, v) {
                                data[i] += v;
                            })
                            $.each(data02, function (i, v) {
                                data2[i] += v;
                            })

                            $.each(data03, function (i, v) {
                                data3[i] += v;
                            })

                        });
                    }
                    var counts = getSeries(list, "count");
                    labels = date;
                    series = [data, data2, data3];
                    lines = ["Khách quen", "Khách mới", "Khách cũ"];

                    break;

            }

            arr.push(labels);
            arr.push(series);
            arr.push(lines);

            return arr;

        }

        /*lấy ra mảng nhãn giá trị trục y (series) cho biểu đồ
        *tham số: 
        - list: danh sách dữ liệu thống kê
        - option: lựa chọn trường dữ liệu thống kê
        *kết quả: mảng giá trị nhãn
        */
        function getSeries(list, option) {
            var series = [];
            switch (option) {
                case "count":
                    if (list.length > 0) {
                        $.each(list, function (i, v) {
                            series.push(v.Count);
                        })
                    }
                    break;

                case "count2":
                    if (list.length > 0) {
                        $.each(list, function (i, v) {
                            series.push(v.Count2);
                        })
                    }
                    break;

                case "count3":
                    if (list.length > 0) {
                        $.each(list, function (i, v) {
                            series.push(v.Count3);
                        })
                    }
                    break;

                case "date":
                    if (list.length > 0) {
                        $.each(list, function (i, v) {
                            series.push(v.DateRe);
                        })
                    }
                    break;
                case "sum":
                    if (list.length > 0) {
                        $.each(list, function (i, v) {
                            series.push(v.Sum);
                        })
                    }
                    break;
                case "avg":
                    if (list.length > 0) {
                        $.each(list, function (i, v) {
                            series.push(v.Avg);
                        })
                    }
                    break;
                case "percent":
                    if (list.length > 0) {
                        $.each(list, function (i, v) {
                            series.push(v.Percent);
                        })
                    }
                    break;
                case "id":
                    if (list.length > 0) {
                        $.each(list, function (i, v) {
                            series.push(v.Id);
                        })
                    }
                    break;

                case "code":
                    if (list.length > 0) {
                        $.each(list, function (i, v) {
                            series.push(v.Code);
                        })
                    }
                    break;
                case "name":
                    if (list.length > 0) {
                        $.each(list, function (i, v) {
                            series.push(v.Name);
                        })
                    }
                    break;
                case "dayOff":
                    if (list.length > 0) {
                        $.each(list, function (i, v) {
                            series.push(v.DayOff);
                        })
                    }
                case "dayWork":
                    if (list.length > 0) {
                        $.each(list, function (i, v) {
                            series.push(v.DayWork);
                        })
                    }
                    break;
            }

            return series;
        }

        /*-------------/ CÁC HÀM XỬ LÝ DỮ LIỆU BÁO CÁO------------- */
    </script>

    <script>
        /*
        Các hàm chuyển đổi và định dạng thời gian
        */

        /*xử lý action các lựa chọn nhanh hôm nay, tháng này, quý này, năm này*/
        function viewDataByDate(This, type) {
            var time = getDayOf(type);
            console.log("time " + time);
            $("#TxtDateTimeFrom").val(time);

            var timeNow = fomatDate(new Date(), "dd/MM/yyy");
            $("#TxtDateTimeTo").val(timeNow);
            $(".tag-date.active").removeClass("active");
            This.addClass("active");
        }

        /*
        *Hàm lấy ra ngày đầu tiên của tuần, tháng, quý, hoặc năm
        *Tham số: 
        - type: kiểu tháng này, tuần này, quý này, năm này
        *Trả ra string định dạng ngày dd/MM/yyy
        */
        function getDayOf(type) {
            var date = new Date();
            var dateNow = new Date();
            y = dateNow.getFullYear(), m = dateNow.getMonth();

            switch (type) {
                case "today":
                    date = dateNow;
                    break;
                case "thisWeek":
                    var date = getMonday(date);
                    break;
                case "thisMonth":
                    var firstDay = new Date(y, m, 1);
                    date = firstDay;
                    break;
                case "thisQuarter":
                    var currQuarter = (dateNow.getMonth() - 1) / 3 + 1;
                    date = new Date(dateNow.getFullYear(), 3 * currQuarter - 3 - 1, 1);
                    break;
            }

            var dateStr = fomatDate(date, "dd/MM/yyy");
            return dateStr;
        }

        /*       
        *Hàm lấy ra ngày đầu tuần (thứ 2)
        *Tham số: 
        - d: thời gian
        *Trả ra string định dạng ngày dd/MM/yyy
        */
        function getMonday(d) {
            d = new Date(d);
            var day = d.getDay(),
                diff = d.getDate() - day + (day == 0 ? -6 : 1);
            return new Date(d.setDate(diff));
        }

        /*       
         *Hàm định dạng ngày
         *Tham số: 
         - date: thời gian
         - type: định dạng kiểu thời gian
         *Trả ra string định dạng ngày dd/MM/yyy
         */
        function fomatDate(date, type) {
            var dateStr = "";
            switch (type) {
                case "dd/MM/yyy":
                    dateStr = date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();
                    break;
                case "yyy/MM/dd":
                    dateStr = date.getFullYear() + '/' + (date.getMonth() + 1) + '/' + date.getDate();
            }
            return dateStr;
        }


    </script>

    <script>
        /*
        load js biểu đồ chartist
        Sử dụng thư viện chartist.js
        */
        (function (window, document, Chartist) {
            'use strict';

            var defaultOptions = {
                labelClass: 'ct-label',
                labelOffset: {
                    x: 0,
                    y: -10
                },
                textAnchor: 'middle',
                labelInterpolationFnc: Chartist.noop
            };

            Chartist.plugins = Chartist.plugins || {};
            Chartist.plugins.ctPointLabels = function (options) {

                options = Chartist.extend({}, defaultOptions, options);

                return function ctPointLabels(chart) {
                    if (chart instanceof Chartist.Line) {
                        chart.on('draw', function (data) {
                            if (data.type === 'point') {
                                data.group.elem('text', {
                                    x: data.x + options.labelOffset.x,
                                    y: data.y + options.labelOffset.y,
                                    style: 'text-anchor: ' + options.textAnchor
                                }, options.labelClass).text(options.labelInterpolationFnc(data.value.x === undefined ? data.value.y : data.value.x + ', ' + data.value.y));
                            }
                        });
                    }
                    if (chart instanceof Chartist.Bar) {
                        chart.on('draw', function (data) {
                            if (data.type === 'bar') {
                                data.group.elem('text', {
                                    x: data.x + options.labelOffset.x,
                                    y: data.y + options.labelOffset.y,
                                    style: 'text-anchor: ' + options.textAnchor
                                }, options.labelClass).text(options.labelInterpolationFnc(data.value.x === undefined ? data.value.y : data.value.x + ', ' + data.value.y));
                            }
                        });
                    }
                };
            };

        }(window, document, Chartist));
    </script>

    <%--SCRTIP TRƯỚC 3/2016--%>
    <script>
        /*
        Các hàm dùng cho biểu đồ đơn
        Dùng trước 3/2016*/

        function bindTable(data, checkedIds, staffType) {

            var topIds = [];
            var $tableStaff = $('#tableStaff');
            var $tbody = $tableStaff.find('tbody');
            var $title = $tableStaff.find('thead .title td');
            //console.log(data);
            var title = data.Title;
            $title.text(title);

            var list = JSON.parse(data.CustomerAvgOfStaff);
            var ids = getSeries(list, "id");
            var names = getSeries(list, "name");
            var avgs = getSeries(list, "avg");
            var dayWorks = getSeries(list, "dayWork");
            var checked = "";
            var classTop = "";

            var $staffCol = $tableStaff.find('thead .staff');
            console.log("bind table");
            if (staffType == '1') {
                $staffCol.text("Stylist");
            } else if (staffType == '2') {
                $staffCol.text("Skinner");
            }

            var $trs = "";
            for (i = 0; i < names.length; i++) {
                var id = ids[i];
                var name = names[i];
                var avg = avgs[i];
                var dayWork = dayWorks[i];
                if (checkedIds == null || checkedIds == "") {
                    if (i < 5) {
                        checked = "checked='checked'";
                        topIds.push(id);
                        classTop = "top";
                    } else {
                        checked = "";
                        classTop = "";
                    }

                } else if (checkedIds.length > 0) {
                    for (j = 0; j < checkedIds.length; j++)
                        if (id == checkedIds[j]) {
                            checked = "checked='checked'";
                            topIds.push(id);
                            //classTop = "top";
                        } else if (topIds.indexOf(id) == -1) {
                            checked = "";
                        }
                }
                $trs += "<tr class='col-xs-12 " + classTop + "' id='" + id + "'>" +
                    "<td class='col-xs-1 col-first'>" + (i + 1) + "</td>" +
                    "<td class='col-xs-2'>" +
                    "<input class='cb-staff' type='checkbox' value='' " + checked + " /></td>" +
                    "<td class='col-xs-4'>" + name + "</td>" +
                    "<td class='col-xs-3 avg'>" + avg + "</td>" +
                    "<td class='col-xs-2 col-end'>" + dayWork + "</td>" +
                    "</tr>";
            }
            $tbody.empty().append($trs);

            return topIds;
        }



        function removeAllCheck() {
            var $checkBox = $tableStaff.find('input[type="checkbox"]');
            $checkBox.prop('checked', false);
        }

        function getCheckedId() {
            var Ids = [];
            var $tableStaff = $('#tableStaff');
            var $checkeds = $tableStaff.find("input[type=checkbox]:checked");
            //console.log($checkeds);
            for (var i = 0; i < $checkeds.length; i++) {
                //console.log($checkeds);
                var id = $($checkeds[i]).parent().parent().attr("id");
                Ids.push(id);
            }
            //console.log(Ids);
            return Ids;
        }

        $tableStaff.find('tbody').click(function () {
            console.log("click check box in table");
            $reSetStatus.val("");
            console.log($reSetStatus.val());
        });


        function getDataReportCustomerOfStaff(salonId, from, to, viewTime, staffType, topIds) {
            $iconStatus.attr('class', 'fa fa-spinner fa-pulse');
            $.ajax({
                type: 'POST',
                url: '/GUI/SystemService/Webservice/BookingService.asmx',
                data: '{ salonId: "' + salonId + '",from:"' + from + '",to:"' + to + '",viewTime:"' + viewTime + '",staffType:"' + staffType + '",topIds:"' + topIds + '"}',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (response) {
                    var data = JSON.parse(response.d);
                    var number = data.Number;
                    if (number == 1)
                        bindDataChart(data, "customerOfStaff", "bar");
                    else
                        bindDataChart(data, "customerOfStaff", "line");
                }, failure: function (response) { alert(response.d); }
            });
        }

        $btnReportBill.click(function () {
            console.log('click');
            $report.val("bill");
            setDisplayContent();
            var from = $('#TxtDateTimeFrom').val();
            var to = $('#TxtDateTimeTo').val();
            var viewTime = $typeViewTime.val();
            var salonId = $('#Salon').find('option:selected').attr('value');
            if (to == "")
                to = from;
            if (from != "")
                getDataReportBill(salonId, from, to, viewTime);

        });

        $btnReportBillByHours.click(function () {
            console.log('click');
            $report.val("billByHours");
            setDisplayContent();
            var from = $('#TxtDateTimeFrom').val();
            var to = $('#TxtDateTimeTo').val();
            var viewTime = $typeViewTime.val();
            var salonId = $('#Salon').find('option:selected').attr('value');
            if (to == "")
                to = from;
            if (from != "")
                getDataReportBillByHours(salonId, from, to, viewTime);

        });

        $btnReportSaleAvg.click(function () {
            console.log('click');
            $report.val("saleAvg");
            setDisplayContent();
            var from = $('#TxtDateTimeFrom').val();
            var to = $('#TxtDateTimeTo').val();
            var viewTime = $typeViewTime.val();
            var salonId = $('#Salon').find('option:selected').attr('value');
            if (to == "")
                to = from;
            if (from != "")
                getDataReportSaleAvg(salonId, from, to, viewTime);
        });

        $btnReportSale.click(function () {
            console.log('click');
            $report.val("sale");
            setDisplayContent();
            var from = $('#TxtDateTimeFrom').val();
            var to = $('#TxtDateTimeTo').val();
            var viewTime = $typeViewTime.val();
            var salonId = $('#Salon').find('option:selected').attr('value');
            if (to == "")
                to = from;
            if (from != "")
                getDataReportSale(salonId, from, to, viewTime);
        });

        $btnReportCustomer.click(function () {
            console.log('click');
            $report.val("customer");
            setDisplayContent();
            var from = $('#TxtDateTimeFrom').val();
            var to = $('#TxtDateTimeTo').val();
            //$typeDate.checkbox('check');

            var viewTime = $typeViewTime.val();
            var salonId = $('#Salon').find('option:selected').attr('value');
            if (to == "")
                to = from;
            if (from != "")
                getDataReportCustomer(salonId, from, to, viewTime);
        });

        $btnReportStaff.click(function () {
            console.log('click');
            $report.val("staff");
            setDisplayContent();
            var from = $('#TxtDateTimeFrom').val();
            var to = $('#TxtDateTimeTo').val();
            var viewTime = $typeViewTime.val();
            var salonId = $('#Salon').find('option:selected').attr('value');
            if (to == "")
                to = from;
            if (from != "")
                getDataReportStaff(salonId, from, to, viewTime);
        });

        $btnReportCustomerOfStaff.click(function () {
            console.log('click');
            $report.val("customerOfStaff");
            setDisplayContent();
            var from = $('#TxtDateTimeFrom').val();
            var to = $('#TxtDateTimeTo').val();
            var viewTime = $typeViewTime.val();
            var salonId = $('#Salon').find('option:selected').attr('value');
            if (to == "")
                to = from;
            if (from != "") {

                getDataReportCustomerOfStaff(salonId, from, to, viewTime);

            }

        });

        $btnReportCustomerAvgOfStaff.click(function () {
            console.log('click');
            $report.val("customerAvgOfStaff");
            setDisplayContent();
            var from = $('#TxtDateTimeFrom').val();
            var to = $('#TxtDateTimeTo').val();
            var viewTime = $typeViewTime.val();
            var staffType = $('#staffType').val();

            var salonId = $('#Salon').find('option:selected').attr('value');
            if (to == "")
                to = from;
            if (from != "")
                getDataReportCustomerAvgOfStaff(salonId, from, to, viewTime, staffType);
        });

        $btnReportCustomerFamiliar.click(function () {
            console.log('click');
            $report.val("customerFamiliar");
            setDisplayContent();
            $typeDate.click();
            var from = $('#TxtDateTimeFrom').val();
            var to = $('#TxtDateTimeTo').val();
            var viewTime = $typeViewTime.val();
            var staffType = $('#staffType').val();

            var salonId = $('#Salon').find('option:selected').attr('value');
            if (to == "")
                to = from;
            if (from != "")
                getCustomerFamiliar(salonId, from, to, viewTime, staffType);
        });

        $btnReportService.click(function () {
            console.log('click');
            $report.val("service");
            setDisplayContent();

            $typeDate.click();
            var from = $('#TxtDateTimeFrom').val();
            var to = $('#TxtDateTimeTo').val();
            var viewTime = $typeViewTime.val();
            var staffType = $('#staffType').val();

            var salonId = $('#Salon').find('option:selected').attr('value');
            if (to == "")
                to = from;
            if (from != "")
                getServiceBycategory(salonId, from, to, viewTime);
        })

        $btnReportTotal.click(function () {
            console.log('click');
            $report.val("staff");
            setDisplayContent();

            $typeDate.click();
            var from = $('#TxtDateTimeFrom').val();
            var to = $('#TxtDateTimeTo').val();
            var viewTime = $typeViewTime.val();
            var staffType = $('#staffType').val();

            var salonId = $('#Salon').find('option:selected').attr('value');
            if (to == "")
                to = from;
            if (from != "")
                getDataStaff(salonId, from, to, viewTime);
        })

        function getDataReportBill(salonId, from, to, viewTime) {
            $iconStatus.attr('class', 'fa fa-spinner fa-pulse');
            console.log(salonId + "| " + from + "| " + to + "|" + viewTime);

            $.ajax({
                type: 'POST',
                url: '/GUI/SystemService/Ajax/ReportService.aspx/ReportBill',
                data: '{ salonId: "' + salonId + '",from:"' + from + '",to:"' + to + '",viewTime:"' + viewTime + '"}',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (response) {
                    var data = JSON.parse(response.d);
                    var title = data.Title;
                    var number = data.Number;

                    if (number == 1)
                        bindDataChart(data, "bill", "bar");
                    else
                        bindDataChart(data, "bill", "line");

                }, failure: function (response) { alert(response.d); }
            });
        }

        function getDataReportBillByHours(salonId, from, to, viewTime) {
            $iconStatus.attr('class', 'fa fa-spinner fa-pulse');
            console.log(salonId + "| " + from + "| " + to + "|" + viewTime);
            var staffType = $('#staffType').val();
            var staffId = $('#HDF_Suggestion_Code').val();

            $.ajax({
                type: 'POST',
                url: '/GUI/SystemService/Ajax/ReportService.aspx/ReportBillByHours',
                data: '{ salonId: "' + salonId + '",staffType:"' + staffType + '",staffId:"' + staffId + '",from:"' + from + '",to:"' + to + '",viewTime:"' + viewTime + '"}',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (response) {
                    var data = JSON.parse(response.d);
                    var title = data.Title;
                    //var number = data.Number;
                    //if (number == 1)
                    //    bindDataChart(title, data, "billByHours", "bar");
                    //else
                    bindDataChart(data, "billByHours", "line");
                }, failure: function (response) { alert(response.d); }
            });
        }

        function getDataReportSaleAvg(salonId, from, to, viewTime) {
            $iconStatus.attr('class', 'fa fa-spinner fa-pulse');
            console.log(salonId + "| " + from + "| " + to);
            $.ajax({
                type: 'POST',
                url: '/GUI/SystemService/Ajax/ReportService.aspx/ReportSaleAvg',
                data: '{ salonId: "' + salonId + '",from:"' + from + '",to:"' + to + '",viewTime:"' + viewTime + '"}',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (response) {
                    //console.log(response);
                    var data = JSON.parse(response.d);
                    var number = data.Number;
                    var title = data.Title;
                    if (number == 1)
                        bindDataChart(data, "saleAvg", "bar");
                    else
                        bindDataChart(data, "saleAvg", "line");
                }, failure: function (response) { alert(response.d); }
            });
        }

        function getDataReportSale(salonId, from, to, viewTime) {
            $iconStatus.attr('class', 'fa fa-spinner fa-pulse');
            console.log(salonId + "| " + from + "| " + to);
            $.ajax({
                type: 'POST',
                url: '/GUI/SystemService/Ajax/ReportService.aspx/ReportSale',
                data: '{ salonId: "' + salonId + '",from:"' + from + '",to:"' + to + '",viewTime:"' + viewTime + '"}',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (response) {
                    //console.log(response);
                    var data = JSON.parse(response.d);
                    var number = data.Number;
                    var title = data.Title;
                    if (number == 1)
                        bindDataChart(data, "sale", "bar");
                    else
                        bindDataChart(data, "sale", "line");
                }, failure: function (response) { alert(response.d); }
            });
        }

        function getServiceBycategory(salonId, from, to, viewTime) {
            $iconStatus.attr('class', 'fa fa-spinner fa-pulse');
            console.log(salonId + "| " + from + "| " + to);
            var staffType = $('#staffType').val();
            var staffId = $('#HDF_Suggestion_Code').val();
            $.ajax({
                type: 'POST',
                url: '/GUI/SystemService/Ajax/ReportService.aspx/ReportServiceByCategory',
                data: '{ salonId: "' + salonId + '",staffType:"' + staffType + '",staffId:"' + staffId + '",from:"' + from + '",to:"' + to + '",viewTime:"' + viewTime + '"}',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (response) {
                    //console.log(response);
                    var data = JSON.parse(response.d);
                    var number = data.Number;
                    var title = data.Title;
                    if (number == 1)
                        bindDataChart(data, "service", "pice");
                    else
                        bindDataChart(data, "service", "pice");
                }, failure: function (response) { alert(response.d); }
            });
        }

        function getDataReportCustomer(salonId, from, to, viewTime) {
            $iconStatus.attr('class', 'fa fa-spinner fa-pulse');
            console.log(salonId + "| " + from + "| " + to);
            $.ajax({
                type: 'POST',
                url: '/GUI/SystemService/Ajax/ReportService.aspx/ReportCustomer',
                data: '{ salonId: "' + salonId + '",from:"' + from + '",to:"' + to + '",viewTime:"' + viewTime + '"}',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (response) {
                    var data = JSON.parse(response.d);
                    var title = data.Title;
                    var number = data.Number;
                    if (number == 1)
                        bindDataChart(data, "customer", "bar");
                    else
                        bindDataChart(data, "customer", "line");


                }, failure: function (response) { alert(response.d); }
            });
        }

        function getDataReportStaff(salonId, from, to, viewTime) {
            $iconStatus.attr('class', 'fa fa-spinner fa-pulse');
            console.log(salonId + "| " + from + "| " + to);

            $.ajax({
                type: 'POST',
                url: '/GUI/SystemService/Ajax/ReportService.aspx/ReportStaff',
                data: '{ salonId: "' + salonId + '",from:"' + from + '",to:"' + to + '",viewTime:"' + viewTime + '"}',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (response) {
                    var data = JSON.parse(response.d);
                    //var title = data.Title;
                    var number = data.Number;
                    if (number == 1)
                        bindDataChart(data, "staff", "bar");
                    else
                        bindDataChart(data, "staff", "line");
                }, failure: function (response) { alert(response.d); }
            });
        }

        function getCustomerFamiliar(salonId, from, to, viewTime, staffType) {
            var resetStatus = $reSetStatus.val();
            if (resetStatus == 1) {
                console.log("reset");
                removeAllCheck();
            }
            else
                console.log("no reset");

            console.log(staffType);
            $iconStatus.attr('class', 'fa fa-spinner fa-pulse');
            console.log(salonId + "| " + from + "| " + to + "|" + staffType);
            $.ajax({
                type: 'POST',
                url: '/GUI/SystemService/Ajax/ReportService.aspx/ReportCustomerAvgOfStaff',
                data: '{ salonId: "' + salonId + '",from:"' + from + '",to:"' + to + '",viewTime:"' + viewTime + '",staffType:"' + staffType + '"}',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (response) {

                    var data = JSON.parse(response.d);

                    var number = data.Number;
                    // bind table 

                    var checkedIds = getCheckedId();
                    var topIds = [];
                    // console.log("checked staff " + checkedIds);
                    topIds = bindTable(data, checkedIds, staffType);
                    // console.log("top staff " + topIds);
                    var report = $report.val();
                    if (viewTime != "day") {
                        $tableStaff.find('.avg').hide();
                        $tableStaff.find('.col-end').hide();
                    } else {
                        $tableStaff.find('.avg').show();
                        $tableStaff.find('.col-end').show();
                    }

                    getDataReportCustomerFamiliar(salonId, from, to, viewTime, staffType, topIds)


                }, failure: function (response) { alert(response.d); }
            });
        }

        function getDataReportCustomerFamiliar(salonId, from, to, viewTime, staffType, topIds) {
            $iconStatus.attr('class', 'fa fa-spinner fa-pulse');
            console.log(salonId + "| " + from + "| " + to);

            $.ajax({
                type: 'POST',
                url: '/GUI/SystemService/Ajax/ReportService.aspx/ReportCustomerFamiliar2',
                data: '{ salonId: "' + salonId + '",from:"' + from + '",to:"' + to + '",viewTime:"' + viewTime + '",staffType:"' + staffType + '",topIds:"' + topIds + '"}',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (response) {
                    var data = JSON.parse(response.d);
                    console.log("ReportCustomerFamiliar success !");
                    console.log(data);
                    var title = data.Title;
                    var number = data.Number;
                    if (number == 1)
                        bindDataChart(data, "customerFamiliar", "bar");
                    else
                        bindDataChart(data, "customerFamiliar", "line");
                }, failure: function (response) { alert(response.d); }
            });
        }

        function getDataReportCustomerAvgOfStaff(salonId, from, to, viewTime, staffType) {

            var resetStatus = $reSetStatus.val();
            if (resetStatus == 1) {
                console.log("reset");
                removeAllCheck();
            }
            else
                console.log("no reset");

            console.log(staffType);
            $iconStatus.attr('class', 'fa fa-spinner fa-pulse');
            console.log(salonId + "| " + from + "| " + to + "|" + staffType);
            $.ajax({
                type: 'POST',
                url: '/GUI/SystemService/Ajax/ReportService.aspx/ReportCustomerAvgOfStaff',
                data: '{ salonId: "' + salonId + '",from:"' + from + '",to:"' + to + '",viewTime:"' + viewTime + '",staffType:"' + staffType + '"}',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (response) {

                    var data = JSON.parse(response.d);

                    var number = data.Number;
                    // bind table 

                    var checkedIds = getCheckedId();
                    var topIds = [];
                    // console.log("checked staff " + checkedIds);
                    topIds = bindTable(data, checkedIds, staffType);
                    // console.log("top staff " + topIds);

                    if (viewTime != "day") {
                        $tableStaff.find('.avg').hide();
                        $tableStaff.find('.col-end').hide();
                    } else {
                        $tableStaff.find('.avg').show();
                        $tableStaff.find('.col-end').show();
                    }

                    getDataReportCustomerOfStaff(salonId, from, to, viewTime, staffType, topIds)

                }, failure: function (response) { alert(response.d); }
            });

        }

    </script>
</asp:Content>
