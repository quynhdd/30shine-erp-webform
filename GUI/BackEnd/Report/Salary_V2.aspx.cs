﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Globalization;
using LinqKit;
using System.Linq.Expressions;
using ExportToExcel;

namespace _30shine.GUI.BackEnd.Report
{
    public partial class Salary_V2 : System.Web.UI.Page
    {
        protected Paging PAGING = new Paging();

        private int _TypeStaff;
        private int _StaffId;
        private int _SkillLevel;
        private bool IsProductFilter = false;

        protected bool Staff_All = false;
        protected bool Staff_Hairdresser = false;
        protected bool Staff_HairMassage = false;
        protected bool Staff_Singer = false;
        protected string THead = "";
        protected string StrProductPrice = "";
        public CultureInfo culture = new CultureInfo("vi-VN");
        public string timeFrom;
        public string timeTo;
        private Expression<Func<Staff, bool>> WhereStaff = PredicateBuilder.True<Staff>();
        private List<Service> _Service = new List<Service>();
        private List<Product> _Product = new List<Product>();
        protected string FirstDayOfMonth = string.Format("{0:dd/MM/yyyy}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1));
        protected string Today = String.Format("{0:dd/MM/yyyy}", DateTime.Now);
        public string sqlWhereStaff = "";

        private bool Perm_ViewAllData = false;
        private bool Perm_Access = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            CheckPermission();

            if (!IsPostBack)
            {
                TxtDateTimeFrom.Text = FirstDayOfMonth;
                TxtDateTimeTo.Text = Today;
                Bind_Salon();
                Bind_TypeStaff();
                GenWhere();
                Bind_All_ByDay();
            }
            else
            {
                GenWhere();
                //Exc_Filter();
            }
            RemoveLoading();
        }

        private void GenWhere()
        {
            _TypeStaff = HDF_TypeStaff.Value != "" ? Convert.ToInt32(HDF_TypeStaff.Value) : 0;
            _SkillLevel = HDF_SkillLevel.Value != "" ? Convert.ToInt32(HDF_SkillLevel.Value) : 0;
            _StaffId = HDF_Staff.Value != "" ? Convert.ToInt32(HDF_Staff.Value) : 0;

            if (_StaffId > 0)
            {
                WhereStaff = WhereStaff.And(w => w.Id == _StaffId);
                sqlWhereStaff += "and a.[SellerId] = " + _StaffId;
            }
            else if (_TypeStaff > 0)
            {
                WhereStaff = WhereStaff.And(w => w.Type == _TypeStaff);
                sqlWhereStaff += "and b.[Type] = " + _TypeStaff;
            }
            if (_SkillLevel > 0)
            {
                WhereStaff = WhereStaff.And(w => w.SkillLevel == _SkillLevel);
                sqlWhereStaff += "and b.SkillLevel = " + _SkillLevel;
            }
            WhereStaff = WhereStaff.And(w => w.IsDelete != 1 && w.Active == 1);

            int SalonId = Convert.ToInt32(Salon.SelectedValue);
            if (!Perm_ViewAllData)
            {
                WhereStaff = WhereStaff.And(p => p.SalonId == SalonId);
            }
            else
            {
                if (SalonId > 0)
                {
                    WhereStaff = WhereStaff.And(p => p.SalonId == SalonId);
                }
                else
                {
                    WhereStaff = WhereStaff.And(p => p.SalonId > 0);
                }
            }

            WhereStaff = WhereStaff.And(w => w.Permission != "accountant" && w.Permission != "admin");

            DateTime d1 = Convert.ToDateTime(TxtDateTimeFrom.Text.Trim(), culture);
            DateTime d2;
            if (TxtDateTimeTo.Text.Trim() != "")
            {
                d2 = Convert.ToDateTime(TxtDateTimeTo.Text.Trim(), culture).AddDays(1);
            }
            else
            {
                d2 = d1.AddDays(1);
            }
            timeFrom = String.Format("{0:MM/dd/yyyy}", d1);
            timeTo = String.Format("{0:MM/dd/yyyy}", d2);
        }

        private void Bind_Salon()
        {
            using (var db = new Solution_30shineEntities())
            {
                var _Salons = db.Tbl_Salon.Where(w => (w.IsDelete == 0 || w.IsDelete == null) && w.IsSalonHoiQuan == false).OrderBy(o => o.Order).ToList();
                var Key = 0;
                var SalonId = Convert.ToInt32(Session["SalonId"]);

                Salon.DataTextField = "Salon";
                Salon.DataValueField = "Id";

                if (Perm_ViewAllData)
                {
                    ListItem item = new ListItem("Chọn Salon", "0");
                    Salon.Items.Insert(0, item);

                    foreach (var v in _Salons)
                    {
                        Key++;
                        item = new ListItem(v.Name, v.Id.ToString());
                        Salon.Items.Insert(Key, item);
                    }
                    Salon.SelectedIndex = 0;
                }
                else
                {
                    foreach (var v in _Salons)
                    {
                        if (v.Id == SalonId)
                        {
                            ListItem item = new ListItem(v.Name, v.Id.ToString());
                            Salon.Items.Insert(Key, item);
                            Salon.SelectedIndex = SalonId;
                            Salon.Enabled = false;
                        }

                    }
                }
            }
        }

        public void Exc_Filter()
        {
            var LSTStaff = new List<Staff_TimeKeeping>();
            int kindView = KindView.Value != "" ? Convert.ToInt32(KindView.Value) : 0;
            if (kindView == 1)
            {
                //THead = Get_Services();
                LSTStaff = Get_DataBy_Filter_Service(true);
            }
            else if (kindView == 2)
            {
                //THead = Get_Products();
                LSTStaff = Get_DataBy_Filter_Product(true);
                IsProductFilter = true;
            }

            RptTimeKeeping.DataSource = LSTStaff;
            RptTimeKeeping.DataBind();
        }

        private List<Staff_TimeKeeping> Get_DataBy_Filter_Service(bool paging)
        {
            using (var db = new Solution_30shineEntities())
            {
                _TypeStaff = HDF_TypeStaff.Value != "" ? Convert.ToInt32(HDF_TypeStaff.Value) : 0;
                _SkillLevel = HDF_SkillLevel.Value != "" ? Convert.ToInt32(HDF_SkillLevel.Value) : 0;
                _StaffId = HDF_Staff.Value != "" ? Convert.ToInt32(HDF_Staff.Value) : 0;

                CultureInfo culture = new CultureInfo("vi-VN");
                //var WhereStaff = PredicateBuilder.True<Staff>();
                var LST_Staff = new List<Staff_TimeKeeping>();
                var LST_Staff2 = new List<Staff_TimeKeeping>();
                var OBJ_Staff_TimeKeeping = new Staff_TimeKeeping();
               
                var lstServiceStatistic = new List<ServiceStatistic>();
                var LST0 = new List<Staff>();
                int index = -1;
                int integer;

                // Get staff
                if (paging)
                {
                    LST0 = db.Staffs.AsExpandable().Where(WhereStaff).OrderBy(o => o.Id).Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                }
                else
                {
                    LST0 = db.Staffs.AsExpandable().Where(WhereStaff).OrderBy(o => o.Id).ToList();
                }

                // Time condition
                if (TxtDateTimeFrom.Text != "")
                {
                    // Map data to staff
                    if (LST0.Count > 0)
                    {
                        string sql;
                        string whereStaff = "";
                        sql = @"select b.* 
                                    from
                                    (
                                    select ServiceId from FlowService
                                    where IsDelete != 1
                                    and (CreatedDate between '" + timeFrom + "' and '" + timeTo + "')" +
                                        @"group by ServiceId
                                    ) as a
                                    join Service as b
                                    on a.ServiceId = b.Id";
                        _Service = db.Services.SqlQuery(sql).ToList();
                        if (_Service.Count > 0)
                        {
                            foreach (var v2 in _Service)
                            {
                                THead += "<th>" + v2.Name + "</th>";
                            }
                        }
                        foreach (var v in LST0)
                        {
                            whereStaff = "";
                            switch (v.Type)
                            {
                                case 1:
                                    whereStaff += "and Staff_Hairdresser_Id = " + v.Id;
                                    break;
                                case 2:
                                    whereStaff += "and Staff_HairMassage_Id = " + v.Id;
                                    break;
                                default:
                                    whereStaff += "and Staff_HairMassage_Id = " + v.Id;
                                    break;
                            }
                            sql = @"select a.*
                                    from FlowService as a
                                    inner join
                                    (
	                                    select Id, ServiceIds, TotalMoney, SalonId, CreatedDate
	                                    from BillService
	                                    where IsDelete != 1 and Pending != 1" + whereStaff + @"
	                                    and CreatedDate between '" + timeFrom + "' and '" + timeTo + @"' 
	                                    and (ServiceIds != '' and ServiceIds is not null)
                                    ) as b
                                    on a.BillId = b.Id
                                    where a.IsDelete != 1";
                            var bills = db.FlowServices.SqlQuery(sql).ToList();
                            
                            if (bills.Count > 0)
                            {
                                var lstStatistic = new List<ServiceStatistic>();
                                var item = new ServiceStatistic();
                                var itemTemp = new ServiceStatistic();
                                var strStatistic = "";
                                int totalMoney = 0;
                                int money;
                                int quantity;
                                int times = 0;
                                
                                foreach (var v2 in bills)
                                {
                                    quantity = (int.TryParse(v2.Quantity.ToString(), out integer) ? integer : 0);
                                    item = new ServiceStatistic();
                                    index = lstStatistic.FindIndex(fi => fi.ServiceId == v2.ServiceId);
                                    var payTemp = db.Tbl_Payon.FirstOrDefault(w => w.TypeStaffId == v.Type && w.KeyId == v2.ServiceId && w.ForeignId == v.SkillLevel);
                                    money = payTemp != null ? (int.TryParse(payTemp.Value.ToString(), out integer) ? integer : 0) : 0;

                                    if (index == -1)
                                    {
                                        item.ServiceId = Convert.ToInt32(v2.ServiceId);
                                        item.ServiceTimes = quantity;
                                        item.ServiceMoney = money;
                                        lstStatistic.Add(item);
                                    }
                                    else
                                    {
                                        itemTemp = new ServiceStatistic();
                                        item = lstStatistic.Find(f => f.ServiceId == v2.ServiceId);
                                        itemTemp.ServiceId = item.ServiceId;
                                        itemTemp.ServiceTimes = item.ServiceTimes + quantity;
                                        itemTemp.ServiceMoney = item.ServiceMoney + money;
                                        lstStatistic[index] = itemTemp;
                                    }
                                    totalMoney += money;
                                    times += quantity;
                                }

                                if (_Service.Count > 0)
                                {
                                    foreach (var v3 in _Service)
                                    {
                                        index = lstStatistic.FindIndex(fi => fi.ServiceId == v3.Id);
                                        if (index == -1)
                                        {
                                            strStatistic += "<td>" + "-" + "</td>";
                                        }
                                        else
                                        {
                                            strStatistic += "<td><span class='sp-times'>" + lstStatistic[index].ServiceTimes + "</span><span class='sp-money'>" +
                                                                String.Format("{0:#,####}", lstStatistic[index].ServiceMoney).Replace(',', '.') +
                                                            "</span></td>";
                                        }
                                        
                                    }
                                }

                                sp_BC_getData_StaffReport_Result data = db.sp_BC_getData_StaffReport(timeFrom, timeTo, v.Id).SingleOrDefault();
                                if (data != null)
                                {
                                    OBJ_Staff_TimeKeeping.LuotKhach = data.LuotKhach;
                                    OBJ_Staff_TimeKeeping.DoanhThu = data.DoanhThu;
                                    OBJ_Staff_TimeKeeping.TongGioLam = data.TongGioLam;
                                    OBJ_Staff_TimeKeeping.DoanhThuTB = data.DoanhThuTB;
                                    OBJ_Staff_TimeKeeping.DoanhSoTB = data.DoanhSoTB;
                                }

                                OBJ_Staff_TimeKeeping.Id = v.Id;
                                OBJ_Staff_TimeKeeping.Fullname = v.Fullname;
                                OBJ_Staff_TimeKeeping._ServiceStatistic = lstStatistic;
                                OBJ_Staff_TimeKeeping.StrDataService = strStatistic;
                                OBJ_Staff_TimeKeeping.StrTotalService = "<td><span class='sp-times'>" + times + "</span><span class='sp-money'>" +
                                                                            String.Format("{0:#,####}", totalMoney).Replace(',', '.') +
                                                                        "</span></td>";
                                
                                LST_Staff.Add(OBJ_Staff_TimeKeeping);
                            }
                        }
                    }

                    // Merge staff
                    if (LST0.Count > 0)
                    {
                        var strserviceDefault = "";
                        var loop = 0;
                        if (_Service.Count > 0)
                        {
                            foreach (var v3 in _Service)
                            {
                                strserviceDefault += "<td>" + "-" + "</td>";
                            }
                        }
                        foreach (var v in LST0)
                        {
                            index = LST_Staff.FindIndex(fi => fi.Id == v.Id);
                            if (index == -1)
                            {
                                OBJ_Staff_TimeKeeping = new Staff_TimeKeeping();
                                OBJ_Staff_TimeKeeping.Id = v.Id;
                                OBJ_Staff_TimeKeeping.Fullname = v.Fullname;
                                OBJ_Staff_TimeKeeping.Type = Convert.ToInt32(v.Type);
                                OBJ_Staff_TimeKeeping.IdShowroom = Convert.ToInt32(v.IdShowroom);
                                OBJ_Staff_TimeKeeping.TotalService = 0;
                                OBJ_Staff_TimeKeeping.StrDataService = strserviceDefault;
                                LST_Staff2.Add(OBJ_Staff_TimeKeeping);
                            }
                            else
                            {
                                LST_Staff2.Add(LST_Staff[loop]);
                                loop++;
                            }
                        }
                    }

                }
                return LST_Staff2;
            }
        }

        private List<Staff_TimeKeeping> Get_DataBy_Filter_Product(bool paging)
        {
            using (var db = new Solution_30shineEntities())
            {
                _TypeStaff = HDF_TypeStaff.Value != "" ? Convert.ToInt32(HDF_TypeStaff.Value) : 0;
                _SkillLevel = HDF_SkillLevel.Value != "" ? Convert.ToInt32(HDF_SkillLevel.Value) : 0;
                _StaffId = HDF_Staff.Value != "" ? Convert.ToInt32(HDF_Staff.Value) : 0;

                CultureInfo culture = new CultureInfo("vi-VN");
                //var WhereStaff = PredicateBuilder.True<Staff>();
                var LST_Staff = new List<Staff_TimeKeeping>();
                var LST_Staff2 = new List<Staff_TimeKeeping>();
                var OBJ_Staff_TimeKeeping = new Staff_TimeKeeping();
                var lstProductStatistic = new List<ProductStatistic>();
                var LST0 = new List<Staff>();
                int index = -1;
                int index2 = -2;
                int integer;

                // Get staff
                if (paging)
                {
                    LST0 = db.Staffs.AsExpandable().Where(WhereStaff).OrderBy(o => o.Id).Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                }
                else
                {
                    LST0 = db.Staffs.AsExpandable().Where(WhereStaff).OrderBy(o => o.Id).ToList();
                }

                // Time condition
                if (TxtDateTimeFrom.Text != "")
                {
                    // Map data to staff
                    if (LST0.Count > 0)
                    {
                        int PayPercent = 10;
                        string sql;
                        var product = new ProductStatistic();
                        var _ProductTmp = new ProductStatistic();
                        sql = @"select f.*, e.Quantity
                                from Product as f
                                inner join
                                (
	                                select c.ProductId, Coalesce(SUM(Coalesce(c.Quantity,0)), 0) as Quantity, COUNT(c.Id) as times
	                                from FlowProduct as c
	                                inner join
	                                (
		                                select a.Id, a.CustomerCode, a.SellerId, a.CreatedDate, b.[Type]
		                                from BillService as a
		                                inner join Staff as b
		                                on a.SellerId = b.Id
		                                where a.CreatedDate between '" + timeFrom + "' and '" + timeTo + @"'
		                                and a.IsDelete != 1 and a.Pending != 1" +
                                        sqlWhereStaff +
                                    @") as d
	                                on c.BillId = d.Id
	                                where c.IsDelete != 1
	                                group by c.ProductId
                                ) as e
                                on f.Id = e.ProductId
                                order by e.times asc";
                        _Product = db.Products.SqlQuery(sql).ToList();
                        if (_Product.Count > 0)
                        {
                            foreach (var v2 in _Product)
                            {
                                THead += "<th>" + v2.Name + "</th>";
                                StrProductPrice += "<td>" + String.Format("{0:#,####}", v2.Price).Replace(',', '.') + "</td>";
                            }
                        }

                        foreach (var v in LST0)
                        {
                            if (v.SkillLevel == 2 && v.Type == 2)
                            {
                                PayPercent = 12;
                            }
                            else
                            {
                                PayPercent = 10;
                            }
                            //sql = @"select a.*, Cast(Coalesce(c.ForSalary, 0) as float) as ForSalary
                            //        from FlowProduct as a
                            //        inner join
                            //        (
                            //         select Id, ProductIds, TotalMoney, SalonId, CreatedDate
                            //         from BillService
                            //         where SellerId = " + v.Id + @"
                            //         and IsDelete != 1 and Pending != 1
                            //         and CreatedDate between '" + timeFrom + "' and '" + timeTo + @"' 
                            //         and (ProductIds != '' and ProductIds is not null)
                            //        ) as b
                            //        on a.BillId = b.Id
                            //        where a.IsDelete != 1
                            //        and (a.Price != 0 and a.Price is not null) and (a.VoucherPercent != 100)";
                            //var bills_product = db.FlowProducts.SqlQuery(sql).ToList();
                            sql = @"select a.*
                                    from FlowProduct as a
                                    inner join
                                    (
	                                    select Id, ProductIds, TotalMoney, SalonId, CreatedDate
	                                    from BillService
	                                    where SellerId = " + v.Id + @"
	                                    and IsDelete != 1 and Pending != 1
	                                    and CreatedDate between '" + timeFrom + "' and '" + timeTo + @"' 
	                                    and (ProductIds != '' and ProductIds is not null)
                                    ) as b
                                    on a.BillId = b.Id
                                    left join Product as c
                                    on a.ProductId = c.Id
                                    where a.IsDelete != 1
                                    and (a.ProductId not in (74,82))";
                            var bills_product = db.Database.SqlQuery<cls_flowproduct>(sql).ToList();

                            sql = @"select a.BillId
                                    from FlowProduct as a
                                    inner join
                                    (
	                                    select Id, ProductIds, TotalMoney, SalonId, CreatedDate
	                                    from BillService
	                                    where SellerId = " + v.Id + @"
	                                    and IsDelete != 1 and Pending != 1
	                                    and CreatedDate between '" + timeFrom + "' and '" + timeTo + @"' 
	                                    and (ProductIds != '' and ProductIds is not null)
                                    ) as b
                                    on a.BillId = b.Id
                                    where a.IsDelete != 1
                                    and ( (PromotionMoney is not null and PromotionMoney != 0)
                                    or (ProductId in (74, 82)) )
                                    group by BillId";
                            var bills_promotion = db.Database.SqlQuery<int>(sql).ToList();

                            if (bills_product.Count > 0)
                            {
                                lstProductStatistic = new List<ProductStatistic>();
                                var strproduct = "";
                                int totalMoney = 0;
                                int totalPromotion = 0;
                                int productPromotion = 0;
                                int money;
                                int quantity;
                                int times = 0;
                                foreach (var v2 in bills_product)
                                {
                                    product = new ProductStatistic();
                                    productPromotion = 0;
                                    quantity = int.TryParse(v2.Quantity.ToString(), out integer) ? integer : 0;
                                    index = lstProductStatistic.FindIndex(fi => fi.ProductId == v2.ProductId);
                                    index2 = bills_promotion.FindIndex(fi => fi == v2.BillId);
                                    if (index2 != -1)
                                    {
                                        money = 10000 * quantity;
                                        totalPromotion += quantity;
                                        productPromotion = quantity;
                                    }
                                    else
                                    {
                                        //money = Convert.ToInt32(v2.Price * v2.Quantity * (100 - v2.VoucherPercent)/100 * PayPercent /100);
                                        money = Convert.ToInt32(v2.Price * v2.Quantity * (float)(100 - v2.VoucherPercent) / 100 * (float)v2.ForSalary / 100);
                                    }

                                    if (index == -1)
                                    {
                                        product.ProductId = Convert.ToInt32(v2.ProductId);
                                        product.Price = Convert.ToInt32(v2.Price);
                                        product.ProductTimes = quantity;
                                        product.ProductMoney = money;
                                        product.ProductPromotion = productPromotion;
                                        lstProductStatistic.Add(product);
                                    }
                                    else
                                    {
                                        _ProductTmp = new ProductStatistic();
                                        product = lstProductStatistic.Find(f => f.ProductId == v2.ProductId);
                                        _ProductTmp.ProductId = product.ProductId;
                                        _ProductTmp.Price = product.Price;
                                        _ProductTmp.ProductTimes = product.ProductTimes + quantity;
                                        _ProductTmp.ProductMoney = product.ProductMoney + money;
                                        _ProductTmp.ProductPromotion = product.ProductPromotion + productPromotion;
                                        lstProductStatistic.Add(product);
                                        lstProductStatistic[index] = _ProductTmp;
                                    }
                                    totalMoney += money;
                                    times += quantity;
                                }

                                if (_Product.Count > 0)
                                {
                                    foreach (var v3 in _Product)
                                    {
                                        index = lstProductStatistic.FindIndex(fi => fi.ProductId == v3.Id);
                                        if (index == -1)
                                        {
                                            strproduct += "<td>" + "-" + "</td>";
                                        }
                                        else
                                        {
                                            strproduct += "<td><span class='sp-times'>" + lstProductStatistic[index].ProductTimes + "</span><span class='sp-promotion'>KM : " + lstProductStatistic[index].ProductPromotion + "</span><span class='sp-money'>" +
                                                                String.Format("{0:#,####}", lstProductStatistic[index].ProductMoney).Replace(',', '.') +
                                                            "</span></td>";
                                        }
                                    }
                                }

                                OBJ_Staff_TimeKeeping.Id = v.Id;
                                OBJ_Staff_TimeKeeping.Fullname = v.Fullname;
                                OBJ_Staff_TimeKeeping._ProductStatistic = lstProductStatistic;
                                OBJ_Staff_TimeKeeping.StrDataService = strproduct;
                                OBJ_Staff_TimeKeeping.StrTotalService = "<td><span class='sp-times'>" + times + "</span><span class='sp-promotion'>KM : " + totalPromotion + "</span><span class='sp-money'>" +
                                                                            String.Format("{0:#,####}", totalMoney).Replace(',', '.') +
                                                                        "</span></td>";
                                OBJ_Staff_TimeKeeping.TotalPromotion = totalPromotion;
                                LST_Staff.Add(OBJ_Staff_TimeKeeping);
                            }
                        }
                    }

                    // Merge staff
                    if (LST0.Count > 0)
                    {
                        var strserviceDefault = "";
                        var loop = 0;
                        if (_Product.Count > 0)
                        {
                            foreach (var v3 in _Product)
                            {
                                strserviceDefault += "<td>" + "-" + "</td>";
                            }
                        }
                        foreach (var v in LST0)
                        {
                            index = LST_Staff.FindIndex(fi => fi.Id == v.Id);
                            if (index == -1)
                            {
                                OBJ_Staff_TimeKeeping = new Staff_TimeKeeping();
                                OBJ_Staff_TimeKeeping.Id = v.Id;
                                OBJ_Staff_TimeKeeping.Fullname = v.Fullname;
                                OBJ_Staff_TimeKeeping.Type = Convert.ToInt32(v.Type);
                                OBJ_Staff_TimeKeeping.IdShowroom = Convert.ToInt32(v.IdShowroom);
                                OBJ_Staff_TimeKeeping.TotalService = 0;
                                OBJ_Staff_TimeKeeping.StrDataService = strserviceDefault;
                                OBJ_Staff_TimeKeeping.StrTotalService = "<td>-</td>";
                                LST_Staff2.Add(OBJ_Staff_TimeKeeping);
                            }
                            else
                            {
                                LST_Staff2.Add(LST_Staff[loop]);
                                loop++;
                            }
                        }
                    }
                }

                return LST_Staff2;
            }
        }

        private void Bind_All_ByDay()
        {
            using (var db = new Solution_30shineEntities())
            {
                var LST = db.Staffs.AsExpandable().Where(WhereStaff).OrderBy(o => o.Id).ToList();
                Rpt_Staff.DataSource = LST;
                Rpt_Staff.DataBind();
            }
        }

        public void Get_Services()
        {
            using (var db = new Solution_30shineEntities())
            {
                var str = "";
                DateTime d1 = Convert.ToDateTime(TxtDateTimeFrom.Text.Trim(), culture);
                DateTime d2;
                if (TxtDateTimeTo.Text.Trim() != "")
                {
                    d2 = Convert.ToDateTime(TxtDateTimeTo.Text.Trim(), culture).AddDays(1);
                }
                else
                {
                    d2 = d1.AddDays(1);
                }
                string timeFrom = String.Format("{0:MM/dd/yyyy}", d1);
                string timeTo = String.Format("{0:MM/dd/yyyy}", d2);

                string sql_service = @"select b.* 
                                    from
                                    (
                                    select ServiceId from FlowService
                                    where IsDelete != 1
                                    and (CreatedDate between '" + timeFrom + "' and '" + timeTo + "')" +
                                        @"group by ServiceId
                                    ) as a
                                    join Service as b
                                    on a.ServiceId = b.Id";
                _Service = db.Services.SqlQuery(sql_service).ToList();
                if (_Service.Count > 0)
                {
                    foreach (var v in _Service)
                    {
                        str += "<th>" + v.Name + "</th>";
                    }
                }
                //return str;
            }
        }

        public string Get_Products()
        {
            using (var db = new Solution_30shineEntities())
            {
                var str = "";
                DateTime d1 = Convert.ToDateTime(TxtDateTimeFrom.Text.Trim(), culture);
                DateTime d2;
                if (TxtDateTimeTo.Text.Trim() != "")
                {
                    d2 = Convert.ToDateTime(TxtDateTimeTo.Text.Trim(), culture).AddDays(1);
                }
                else
                {
                    d2 = d1.AddDays(1);
                }
                string timeFrom = String.Format("{0:MM/dd/yyyy}", d1);
                string timeTo = String.Format("{0:MM/dd/yyyy}", d2);

                string sql_product = @"select b.* 
                                    from
                                    (
                                    select ProductId from FlowProduct
                                    where IsDelete != 1
                                    and (CreatedDate between '" + timeFrom + "' and '" + timeTo + "')" +
                                        @"group by ProductId
                                    ) as a
                                    join Product as b
                                    on a.ProductId = b.Id";
                _Product = db.Products.SqlQuery(sql_product).ToList();
                if (_Product.Count > 0)
                {
                    foreach (var v in _Product)
                    {
                        str += "<th>" + v.Name + "</th>";
                    }
                }
                return str;
            }
        }

        public void Bind_TypeStaff()
        {
            using (var db = new Solution_30shineEntities())
            {
                var LST = db.Staff_Type.Where(w => w.IsDelete != 1 && w.Publish == true).OrderBy(o => o.Order).ToList();
                Rpt_StaffTye.DataSource = LST;
                Rpt_StaffTye.DataBind();
            }
        }

        [WebMethod]
        public static string Load_Staff_ByType(string type, string skill, string salonId)
        {
            {
                var _Msg = new Msg();
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                var Type = Convert.ToInt32(type);
                var Skill = 0;
                int integer;
                if (skill != "")
                {
                    Skill = Convert.ToInt32(skill);
                }
                using (var db = new Solution_30shineEntities())
                {
                    var WhereLoadStaff = PredicateBuilder.True<Staff>();
                    WhereLoadStaff = WhereLoadStaff.And(w => w.Type == Type);
                    if (Skill > 0)
                    {
                        WhereLoadStaff = WhereLoadStaff.And(w => w.SkillLevel == Skill);
                    }
                    //if (HttpContext.Current.Session["SalonId"] != null)
                    //{

                    //    int Salonid = int.TryParse(HttpContext.Current.Session["SalonId"].ToString(), out integer) ? integer : 0;
                    //    if (Salonid > 0)
                    //    {
                    //        WhereLoadStaff = WhereLoadStaff.And(w => w.SalonId == Salonid);
                    //    }                        
                    //}
                    if (salonId != "")
                    {
                        int Salonid = int.TryParse(salonId, out integer) ? integer : 0;
                        if (Salonid > 0)
                        {
                            WhereLoadStaff = WhereLoadStaff.And(w => w.SalonId == Salonid);
                        }
                    }
                    var LST = db.Staffs.AsExpandable().Where(WhereLoadStaff).Select(s => new { s.Id, s.Fullname, s.Code }).ToList();

                    if (LST.Count > 0)
                    {
                        _Msg.success = true;
                        _Msg.msg = serializer.Serialize(LST);
                    }
                    else
                    {
                        _Msg.success = false;
                    }
                }
                return serializer.Serialize(_Msg);
            }
        }

        [WebMethod]
        public static string Load_Skill_ByType(string type)
        {
            {
                var _Msg = new Msg();
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                var Type = Convert.ToInt32(type);
                using (var db = new Solution_30shineEntities())
                {
                    string sql = @"select a.* from (
                    SELECT LTRIM(RTRIM(m.n.value('.[1]','varchar(1000)'))) as Skill
                    FROM
                    (
	                    SELECT Id , Name, CAST('<XMLRoot><RowData>' + REPLACE(SkillLevelIds,',','</RowData><RowData>') + '</RowData></XMLRoot>' AS XML) AS x
	                    FROM   Staff_Type 
	                    where (IsDelete = 0 or IsDelete is null) 
	                    and Id = " + Type + @" )t
                    CROSS APPLY x.nodes('/XMLRoot/RowData')m(n)) as b
                    left join Tbl_SkillLevel a on a.Id = b.Skill";

                    var LST = db.Database.SqlQuery<Tbl_SkillLevel>(sql).ToList();

                    if (LST.Count > 0)
                    {
                        _Msg.success = true;
                        _Msg.msg = serializer.Serialize(LST);
                    }
                    else
                    {
                        _Msg.success = false;
                    }
                }
                return serializer.Serialize(_Msg);
            }
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            Bind_Paging();
            Exc_Filter();

            if (IsProductFilter)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Script", "SetActiveTableListing();ReplaceZezoValue();showTrPrice();", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Script", "SetActiveTableListing();ReplaceZezoValue();", true);
            }

            RemoveLoading();
        }

        protected void Bind_Paging()
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? (!HDF_Page.Value.Equals("") ? Convert.ToInt32(HDF_Page.Value) : 1) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage() : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage()
        {
            using (var db = new Solution_30shineEntities())
            {
                _TypeStaff = HDF_TypeStaff.Value != "" ? Convert.ToInt32(HDF_TypeStaff.Value) : 0;
                _SkillLevel = HDF_SkillLevel.Value != "" ? Convert.ToInt32(HDF_SkillLevel.Value) : 0;
                _StaffId = HDF_Staff.Value != "" ? Convert.ToInt32(HDF_Staff.Value) : 0;

                CultureInfo culture = new CultureInfo("vi-VN");
                //var WhereStaff = PredicateBuilder.True<Staff>();
                int Count = 0;

                //if (_StaffId > 0)
                //{
                //    WhereStaff = WhereStaff.And(w => w.Id == _StaffId);
                //}
                //if (_TypeStaff > 0)
                //{
                //    WhereStaff = WhereStaff.And(w => w.Type == _TypeStaff);
                //}
                //WhereStaff = WhereStaff.And(w => w.IsDelete != 1);

                // Get staff
                if (TxtDateTimeFrom.Text != "")
                {
                    Count = db.Staffs.AsExpandable().Count(WhereStaff);
                }

                int TotalRow = Count - PAGING._TopNewsNum;
                int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
                return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
            }
        }

        protected void Exc_ExportExcel(object sender, EventArgs e)
        {
            using (var db = new Solution_30shineEntities())
            {
                var _ExcelHeadRow = new List<string>();
                var serializer = new JavaScriptSerializer();
                var BillRowLST = new List<List<string>>();
                var BillRow = new List<string>();
                _ExcelHeadRow.Add("Tên nhân viên");

                var TotalTimes = 0;
                var TotalPromotion = 0;
                var Index = -1;
                var LSTStaff = new List<Staff_TimeKeeping>();
                var kindView = KindView.Value != "" ? Convert.ToInt32(KindView.Value) : 0;
                if (kindView == 1)
                {
                    // Listing service
                    //var LST_Service = db.Services.Where(w => w.IsDelete != 1 && w.Publish == 1 && (w.IsFreeService != 1 || w.IsFreeService == null)).OrderByDescending(w => w.Id).ToList();
                    LSTStaff = Get_DataBy_Filter_Service(false);

                    if (_Service.Count > 0)
                    {
                        foreach (var v in _Service)
                        {
                            _ExcelHeadRow.Add(v.Name);
                        }
                    }
                    _ExcelHeadRow.Add("Tổng số");

                    if (LSTStaff.Count > 0)
                    {
                        foreach (var v in LSTStaff)
                        {
                            TotalTimes = 0;
                            TotalPromotion = 0;
                            BillRow = new List<string>();
                            BillRow.Add(v.Fullname);
                            foreach (var v2 in _Service)
                            {
                                if (v._ServiceStatistic != null && v._ServiceStatistic.Count > 0)
                                {
                                    Index = v._ServiceStatistic.FindIndex(fi => fi.ServiceId == v2.Id);
                                    if (Index != -1)
                                    {
                                        if (HDF_ExportType.Value == "money")
                                        {
                                            BillRow.Add(v._ServiceStatistic[Index].ServiceMoney.ToString());
                                            TotalTimes += v._ServiceStatistic[Index].ServiceMoney;
                                        }
                                        else if (HDF_ExportType.Value == "quantity")
                                        {
                                            BillRow.Add(v._ServiceStatistic[Index].ServiceTimes.ToString());
                                            TotalTimes += v._ServiceStatistic[Index].ServiceTimes;
                                        }
                                    }
                                    else
                                    {
                                        BillRow.Add("0");
                                    }
                                }
                                else
                                {
                                    BillRow.Add("0");
                                }
                            }

                            BillRow.Add(TotalTimes.ToString());

                            BillRowLST.Add(BillRow);
                        }
                    }
                }
                else if (kindView == 2)
                {
                    // Listing product
                    //var LST_Product = db.Products.Where(w => w.IsDelete != 1 && w.Publish == 1).OrderByDescending(w => w.Id).ToList();
                    LSTStaff = Get_DataBy_Filter_Product(false);

                    if (_Product.Count > 0)
                    {
                        foreach (var v in _Product)
                        {
                            _ExcelHeadRow.Add(v.Name);
                        }
                    }
                    _ExcelHeadRow.Add("Tổng số");
                    _ExcelHeadRow.Add("Số khuyến mại (35k, 50k, Gôm tặng)");

                    if (LSTStaff.Count > 0)
                    {
                        foreach (var v in LSTStaff)
                        {
                            TotalTimes = 0;
                            BillRow = new List<string>();
                            BillRow.Add(v.Fullname);
                            foreach (var v2 in _Product)
                            {
                                if (v._ProductStatistic != null && v._ProductStatistic.Count > 0)
                                {
                                    Index = v._ProductStatistic.FindIndex(fi => fi.ProductId == v2.Id);
                                    if (Index != -1)
                                    {
                                        if (HDF_ExportType.Value == "money")
                                        {
                                            BillRow.Add(v._ProductStatistic[Index].ProductMoney.ToString());
                                            TotalTimes += v._ProductStatistic[Index].ProductMoney;
                                        }
                                        else if (HDF_ExportType.Value == "quantity")
                                        {
                                            BillRow.Add(v._ProductStatistic[Index].ProductTimes.ToString());
                                            TotalTimes += v._ProductStatistic[Index].ProductTimes;
                                        }
                                    }
                                    else
                                    {
                                        BillRow.Add("0");
                                    }
                                }
                                else
                                {
                                    BillRow.Add("0");
                                }
                            }
                            BillRow.Add(TotalTimes.ToString());
                            BillRow.Add(v.TotalPromotion.ToString());
                            BillRowLST.Add(BillRow);
                        }
                    }
                }

                // export
                var ExcelStorePath = Server.MapPath("~") + "Public/Excel/Nhan.Vien/";
                var FileName = "Hoa_Don_" + string.Format("{0:dd_MM_yyyy_hh_mm_ss}", DateTime.Now) + ".xlsx";
                var UrlFileName = "http://" + Request.Url.Host + "/Public/Excel/Nhan.Vien/" + FileName;

                ExportXcel(_ExcelHeadRow, BillRowLST, ExcelStorePath + FileName);
                Bind_Paging();
                ScriptManager.RegisterStartupScript(BtnFakeExcel, this.GetType(), "Download Excel", "CallBackExportExcel('" + UrlFileName + "');UP_FormatPrice('.be-report-price'); SubTableFixHeight();SetActiveTableListing();", true);
            }
        }

        private static void ExportXcel(List<string> _ExcelHeadRow, List<List<string>> List, string Path)
        {
            try
            {
                CreateExcelFile.ExcelHeadRow = _ExcelHeadRow;
                CreateExcelFile.CreateExcel(List, Path);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi. Không tạo được file excel.\r\nException: " + ex.Message);
            }
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        /// <summary>
        /// Check permission
        /// </summary>
        private void CheckPermission()
        {
            if (Session["User_Id"] == null)
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                string[] Allow = new string[] { "root", "admin", "accountant", "salonmanager" };
                string[] AllowViewAllDate = new string[] { "root", "admin", "accountant" };
                var Permission = Session["User_Permission"].ToString().Trim();
                if (Array.IndexOf(Allow, Permission) != -1)
                {
                    Perm_Access = true;
                }

                if (Array.IndexOf(AllowViewAllDate, Permission) != -1)
                {
                    Perm_ViewAllData = true;
                }
            }

            // Call execute function
            ExecuteByPermission();
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        public struct Staff_TimeKeeping
        {
            public int Id { get; set; }
            public string Fullname { get; set; }
            public int Type { get; set; }
            public int IdShowroom { get; set; }
            public List<int> ServiceIds { get; set; }
            public List<ServiceStatistic> _ServiceStatistic { get; set; }
            public List<ProductStatistic> _ProductStatistic { get; set; }
            public int TotalService { get; set; }
            public string StrDataService { get; set; }
            public string StrTotalService { get; set; }
            public string StrDataStylist { get; set; }
            public int TotalPromotion { get; set; }
            public Nullable<int> Staff_Hairdresser_Id { get; set; }
            public Nullable<int> LuotKhach { get; set; }
            public Nullable<double> DoanhThu { get; set; }
            public Nullable<double> TongGioLam { get; set; }
            public Nullable<double> DoanhThuTB { get; set; }
            public Nullable<double> DoanhSoTB { get; set; }

        }

        public struct ServiceStatistic
        {
            public int ServiceId { get; set; }
            public int ServiceName { get; set; }
            public int ServiceTimes { get; set; }
            public int ServiceMoney { get; set; }
        }

        public struct ProductStatistic
        {
            public int ProductId { get; set; }
            public int ProductName { get; set; }
            public int Price { get; set; }
            public int ProductTimes { get; set; }
            public int ProductMoney { get; set; }
            public int ProductPromotion { get; set; }
        }

        public struct BillServiceJoin
        {
            public int Id { get; set; }
            public string Fullname { get; set; }
            public int Type { get; set; }
            public int IdShowroom { get; set; }
            public int BillIsDelete { get; set; }
            public int BillId { get; set; }
            public DateTime BillCreatedDate { get; set; }
        }

        public class cls_flowproduct : FlowProduct
        {
        }
    }
}