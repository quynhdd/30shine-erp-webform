﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="RunReportKQKD.aspx.cs" Inherits="_30shine.GUI.BackEnd.Report.RunReportKQKD" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Chạy báo cáo kết quả kinh doanh</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <style>
            .wp-page-report {
                width: 100%;
                float: left;
                margin: 20px;
            }

            .h1-text {
                width: 100%;
                float: left;
                text-align: center;
                font-size: 24px;
            }

            .wp-ctn {
                width: 50%;
                margin: 0 auto;
            }

            .datepicker-wp {
                width: 100%;
                float: left;
                text-align: center;
                margin-top: 20px;
                min-height: 500px;
            }

            .txtDateTime {
                border: 1px solid #D0D6D8;
                height: 31px;
                font-size: 14px !important;
                padding: 0 10px;
            }

            .btn.btn-send {
                border-radius: 0;
                margin-left: 20px;
            }
        </style>
        <div class="wp-page-report">

            <div class="wp-ctn">
                <h1 class="h1-text">Chạy lại báo cáo kết quả kinh doanh</h1>
                <p style="width:100%; float:left; text-align:center; font-size:18px;">Chạy trong ngay được từ 22h tới 23:59 . Chạy được trong ngày quá khứ , không chạy được ngày tương lai</p>
                <div class="datepicker-wp">
                    <asp:TextBox CssClass="txtDateTime st-head" ID="TxtDateTimeFrom" placeholder="Từ ngày"
                        ClientIDMode="Static" runat="server"></asp:TextBox>
                    <strong class="st-head" style="margin-left: 10px; margin-right: 10px;">
                        <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                    </strong>
                    <asp:TextBox CssClass="txtDateTime st-head" ID="TxtDateTimeTo" placeholder="Đến ngày"
                        ClientIDMode="Static" runat="server"></asp:TextBox>
                    <input type="button" class="btn btn-send" value="Chạy BCKQKD" onclick="runData()" />
                </div>
            </div>
        </div>
        <!-- Page loading -->
        <div class="page-loading">
            <p>Vui lòng đợi trong giây lát...</p>
        </div>
        <script src="/Assets/js/config.js"></script>
        <script type="text/javascript">
            jQuery(document).ready(function () {

                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) {
                        //$input.val($input.val().split(' ')[0]);
                    }
                });
            });

            // call api chay BCKQKD
            // func click luu data
            function runData(This) {
                var dateFrom = $("#TxtDateTimeFrom").val();
                var dateTo = $("#TxtDateTimeTo").val();
                startLoading();
                $.ajax({
                    type: "POST",
                    url: apisReportBCKQKD.domain + apisReportBCKQKD.PostRerunBCKQKD + "?dateFrom=" + dateFrom + "&dateTo=" + dateTo,
                    //async: false,
                    data: {},
                    contentType: "application/json; charset=utf-8",
                    dataType: "text xml",
                    success: function (textStatus, xhr) {
                        alert(xhr);
                        finishLoading();
                        location.reload();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        var responseText = jqXHR.responseJSON.message;
                        alert("Lỗi :  " + responseText);
                        finishLoading();
                        location.reload();
                        //alert("Có lỗi xảy ra !");
                    }
                    
                });
            };
        </script>
    </asp:Panel>
</asp:Content>
