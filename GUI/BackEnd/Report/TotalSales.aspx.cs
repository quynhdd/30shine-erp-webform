﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Globalization;
using LinqKit;
using System.Linq.Expressions;
using ExportToExcel;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.Report
{
    public partial class TotalSales : System.Web.UI.Page
    {
        protected Paging PAGING = new Paging();

        private int _TypeStaff;
        private int _StaffId;
        private bool IsProductFilter = false;

        protected bool Staff_All = false;
        protected bool Staff_Hairdresser = false;
        protected bool Staff_HairMassage = false;
        protected bool Staff_Singer = false;
        protected string THead = "";
        protected string StrProductPrice = "";
        public CultureInfo culture = new CultureInfo("vi-VN");
        public string timeFrom;
        public string timeTo;
        private Expression<Func<Staff, bool>> WhereStaff = PredicateBuilder.True<Staff>();
        private List<Service> _Service = new List<Service>();
        private List<Product> _Product = new List<Product>();
        protected string FirstDayOfMonth = string.Format("{0:dd/MM/yyyy}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1));
        protected string Today = String.Format("{0:dd/MM/yyyy}", DateTime.Now);
        public string sqlWhereStaff = "";
        protected int SalonId;
        protected cls_totalReport totalReport = new cls_totalReport();

        protected string Day1 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-2));
        protected string Day2 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-1));
        protected string Day3 = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
        public bool Perm_ViewAllData = false;
        private bool Perm_Access = false;
        protected string Permission = "";
        private int integer;
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (!IsPostBack)
            {
                TxtDateTimeFrom.Text = FirstDayOfMonth;
                TxtDateTimeTo.Text = Today;
                Library.Function.bindSalon(new List<DropDownList> { Salon }, Perm_ViewAllData);
                GenWhere();
            }
            else
            {
                GenWhere();
                //Exc_Filter();
            }
            RemoveLoading();
        }

        private void GenWhere()
        {
            _TypeStaff = HDF_TypeStaff.Value != "" ? Convert.ToInt32(HDF_TypeStaff.Value) : 0;
            _StaffId = HDF_Staff.Value != "" ? Convert.ToInt32(HDF_Staff.Value) : 0;

            if (_StaffId > 0)
            {
                WhereStaff = WhereStaff.And(w => w.Id == _StaffId);
                sqlWhereStaff += "and a.[SellerId] = " + _StaffId;
            }
            else if (_TypeStaff > 0)
            {
                WhereStaff = WhereStaff.And(w => w.Type == _TypeStaff);
                sqlWhereStaff += "and b.[Type] = " + _TypeStaff;
            }
            WhereStaff = WhereStaff.And(w => w.IsDelete != 1);

            SalonId = int.TryParse(Salon.SelectedValue, out integer) ? integer : 0;
            if (!Perm_ViewAllData)
            {
                WhereStaff = WhereStaff.And(p => p.SalonId == SalonId);
            }
            else
            {
                if (SalonId > 0)
                {
                    WhereStaff = WhereStaff.And(p => p.SalonId == SalonId);
                }
                else
                {
                    WhereStaff = WhereStaff.And(p => p.SalonId > 0);
                }
            }

            WhereStaff = WhereStaff.And(w => w.Permission != "accountant" && w.Permission != "admin");

            DateTime d1 = Convert.ToDateTime(TxtDateTimeFrom.Text.Trim(), culture);
            DateTime d2;
            if (TxtDateTimeTo.Text.Trim() != "")
            {
                d2 = Convert.ToDateTime(TxtDateTimeTo.Text.Trim(), culture).AddDays(1);
            }
            else
            {
                d2 = d1.AddDays(1);
            }
            timeFrom = String.Format("{0:MM/dd/yyyy}", d1);
            timeTo = String.Format("{0:MM/dd/yyyy}", d2);
        }

        public void Exc_Filter()
        {
            List<ExportDataCosmeticForMisa> lstCosmetic = GetDataCosmetics();
            if (lstCosmetic.Any())
            {
                foreach (var v in lstCosmetic)
                {
                    totalReport.TongTienNhapHang += (v.Price * v.Quantity);

                }
                totalReport.TongLuongMyPham += lstCosmetic.Sum(w => w.MoneyPercent);
                totalReport.TongTienDoanhThu += lstCosmetic.Sum(w => w.Money);
            }
            Bind_Paging(lstCosmetic.Count);
            RptTimeKeeping.DataSource = lstCosmetic.Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
            RptTimeKeeping.DataBind();
        }

        public List<Items> Get_Data_Product_v1()
        {
            using (var db = new Solution_30shineEntities())
            {
                var LST = new List<Items>();
                string sql = @"select c.Id, c.Name, c.Code, d.Total_KT, d.Total_TDN, d.Total_DC, d.Total
                                from Product as c
                                inner join
                                (
	                                select b.ProductId
		                                ,COALESCE((
			                                select SUM(Quantity)
			                                from FlowProduct 
			                                where SalonId = 2 and ProductId = b.ProductId
			                                and CreatedDate between '" + timeFrom + "' and '" + timeTo + "' and IsDelete != 1" +
                                        @"), 0) as Total_KT
		                                ,COALESCE((
			                                select SUM(Quantity) 
			                                from FlowProduct 
			                                where SalonId = 3 and ProductId = b.ProductId
			                                and CreatedDate between '" + timeFrom + "' and '" + timeTo + "' and IsDelete != 1" +
                                        @"), 0) as Total_TDN
                                        ,COALESCE((
			                                select SUM(Quantity) 
			                                from FlowProduct 
			                                where SalonId = 4 and ProductId = b.ProductId
			                                and CreatedDate between '" + timeFrom + "' and '" + timeTo + "' and IsDelete != 1" +
                                        @"), 0) as Total_DC
		                                ,COALESCE((
			                                select SUM(Quantity)
			                                from FlowProduct 
			                                where SalonId in (2,3,4) and ProductId = b.ProductId
			                                and CreatedDate between '" + timeFrom + "' and '" + timeTo + "' and IsDelete != 1" +
                                        @"), 0) as Total
	                                from FlowProduct as a
	                                inner join
	                                (
		                                select ProductId
		                                from FlowProduct
		                                where CreatedDate between '" + timeFrom + "' and '" + timeTo + "' and IsDelete != 1" +
                                        @"group by ProductId
	                                ) as b
	                                on a.Id = b.ProductId
                                ) as d
                                on c.Id = d.ProductId";
                LST = db.Database.SqlQuery<Items>(sql).ToList();
                return LST;
            }
        }

        public List<ExportDataCosmeticForMisa> GetDataCosmetics(bool RemovedProductIdWhenExportExcel = false)
        {
            List<ExportDataCosmeticForMisa> lstCosmeticsForMisa = new List<ExportDataCosmeticForMisa>();
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    string whereSalon = "";
                    if (SalonId > 0)
                    {
                        whereSalon = " and SalonId = " + SalonId + " ";
                    }
                    else
                    {
                        whereSalon = " and SalonId > 0 ";
                    }
                    string sql = @"SELECT c.Id, c.Name,c.Price, c.Code, c.Cost, ISNULL(b.Quantity,0) AS Quantity, b.Money, b.Salary, CONVERT(VARCHAR(10), b.CreatedDate,105) AS CreatedDate, s.ShortName AS SalonName, b.VoucherPercent, (b.Quantity * c.Price * Cast(b.VoucherPercent as float) / Cast(100 as float)) AS MoneyPercent
                                    FROM
                                    (
                                        SELECT a.ProductId, FORMAT(a.CreatedDate, 'dd/MM/yyyy') AS CreatedDate, a.SalonId, ISNULL(a.VoucherPercent,0) AS VoucherPercent,COALESCE(SUM(Quantity), 0) as Quantity, Cast(Coalesce(SUM(money), 0) as float) as Money, Cast(Coalesce(SUM(salary), 0) / 100 as float) as Salary
	                                    FROM
	                                    (
		                                    SELECT *,
			                                    (CASE
				                                    WHEN f.PromotionMoney > 0 then 0
				                                    WHEN f.VoucherPercent = 100 then 0
				                                    WHEN (f.PromotionMoney = 0 and f.VoucherPercent <> 100) then f.Quantity * f.Price * Cast(f.ForSalary as float) * Cast(100 - f.VoucherPercent as float) / Cast(100 as float)
			                                    END) AS salary,
                                                (CASE
					                                WHEN f.PromotionMoney > 0 then f.Quantity * f.Price - f.PromotionMoney
					                                WHEN f.VoucherPercent = 100 then 0
					                                WHEN (f.PromotionMoney = 0 and f.VoucherPercent <> 100) then f.Quantity * f.Price * Cast(100 - f.VoucherPercent as float) / Cast(100 as float)
				                                END) AS money
		                                    FROM 
		                                    (
			                                    SELECT e.*
			                                    FROM
                                                FlowProduct AS e
                                                inner join
			                                    (
				                                    SELECT *
				                                    FROM BillServiceHis
				                                    WHERE IsDelete = 0 and Pending = 0 and 
				                                    CreatedDate between '" + timeFrom + @"' and '" + timeTo + @"'
				                                    and ProductIds != '' and ProductIds is not null " + whereSalon + @"
			                                    ) AS d
			                                    ON d.Id = e.BillId and e.IsDelete = 0
                                                INNER JOIN Product as p
			                                    ON e.ProductId = p.Id
		                                    ) AS f
		                                    WHERE f.IsDelete = 0
                                            and f.ComboId is null
	                                    ) AS a
	                                    GROUP BY a.ProductId, FORMAT(a.CreatedDate, 'dd/MM/yyyy'), a.SalonId, a.VoucherPercent
                                    ) AS b
                                    inner join Product AS c
                                    ON b.ProductId = c.Id
						            INNER JOIN dbo.Tbl_Salon AS s
						            ON b.SalonId = s.Id
	                                ORDER BY CONVERT(VARCHAR(10), b.CreatedDate,105), b.SalonId, b.VoucherPercent DESC";
                    lstCosmeticsForMisa = db.Database.SqlQuery<ExportDataCosmeticForMisa>(sql).ToList();
                }
            }
            catch (Exception ex)
            {
                new Helpers.PushNotiErrorToSlack().Push("GA1LZN9AM", ex.Message + ", MAIN: " + ex.StackTrace + ", " + Library.Function.JavaScript.Serialize("Input: " + timeFrom + timeTo + SalonId.ToString()),
                                                        this.ToString() + ".TotalSales", "dungnm",
                                                        HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host);
            }
            return lstCosmeticsForMisa;
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            //Bind_Paging();
            Exc_Filter();
            if (IsProductFilter)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Script", "SetActiveTableListing();ReplaceZezoValue();showTrPrice();", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Script", "SetActiveTableListing();ReplaceZezoValue();", true);
            }
            RemoveLoading();
        }

        protected void Bind_Paging(int TotalRow)
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(TotalRow) : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();
            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage(int TotalRow)
        {
            TotalRow -= PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        }

        /// <summary>
        /// Export Excell for misa
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Exc_ExportExcelForMisa(object sender, EventArgs e)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    if (TxtDateTimeFrom.Text.Trim() != "")
                    {
                        IEnumerable<ExportDataCosmeticForMisa> lstExports = GetDataCosmetics(true);
                        var _ExcelHeadRow = new List<string>();
                        var RowLST = new List<List<string>>();
                        _ExcelHeadRow.Add("Ngày");
                        _ExcelHeadRow.Add("Salon");
                        _ExcelHeadRow.Add("Mã sản phẩm");
                        _ExcelHeadRow.Add("Sản phẩm");
                        _ExcelHeadRow.Add("Số lượng");
                        _ExcelHeadRow.Add("Giá bán");
                        _ExcelHeadRow.Add("Thành tiền");
                        _ExcelHeadRow.Add("Chiết khấu (%)");
                        _ExcelHeadRow.Add("Tiền CK");
                        _ExcelHeadRow.Add("Tổng doanh thu");
                        if (lstExports.Any())
                        {
                            foreach (var v in lstExports)
                            {
                                var Row = new List<string>();
                                Row.Add(v.CreatedDate.ToString());
                                Row.Add(v.SalonName);
                                Row.Add(v.Code);
                                Row.Add(v.Name);
                                Row.Add(v.Quantity.ToString());
                                Row.Add(string.Format("{0:#,####}", v.Price.ToString()));
                                Row.Add(string.Format("{0:#,####}", (v.Price * v.Quantity).ToString()));
                                Row.Add(string.Format("{0:#,####}", v.VoucherPercent.ToString()));
                                Row.Add(string.Format("{0:#,####}", v.MoneyPercent.ToString()));
                                Row.Add(string.Format("{0:#,####}", (v.Money).ToString()));
                                RowLST.Add(Row);

                            }
                            var ExcelStorePath = Server.MapPath("~") + "Public/Excel/Hoa.Don/";
                            var FileName = "My_pham_ban_cho_misa_" + string.Format("{0:dd_MM_yyyy_hh_mm_ss}", DateTime.Now) + ".xlsx";
                            var UrlFileName = "http://" + Request.Url.Host + "/Public/Excel/Hoa.Don/" + FileName;
                            ExportXcel(_ExcelHeadRow, RowLST, ExcelStorePath + FileName);
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Download Excel", "CallBackExportExcel('" + UrlFileName + "');UP_FormatPrice('.be-report-price'); SubTableFixHeight();SetActiveTableListing(); alert(\"Xuất excel thành công.\")", true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Export Excell
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Exc_ExportExcel(object sender, EventArgs e)
        {
            using (var db = new Solution_30shineEntities())
            {
                if (TxtDateTimeFrom.Text.Trim() != "")
                {
                    var list = GetDataCosmetics();
                    var _ExcelHeadRow = new List<string>();
                    var RowLST = new List<List<string>>();
                    var Row = new List<string>();
                    _ExcelHeadRow.Add("Id");
                    _ExcelHeadRow.Add("Tên sản phẩm");
                    _ExcelHeadRow.Add("Mã Code");
                    _ExcelHeadRow.Add("Số lượng");
                    _ExcelHeadRow.Add("Giá bán");
                    _ExcelHeadRow.Add("Lương nhân viên");
                    _ExcelHeadRow.Add("Thành tiền");
                    _ExcelHeadRow.Add("Tổng tiền chiết khấu");
                    _ExcelHeadRow.Add("Tổng doanh thu");
                    if (list.Count > 0)
                    {
                        foreach (var v in list)
                        {
                            Row = new List<string>();
                            Row.Add(v.Id.ToString());
                            Row.Add(v.Name);
                            Row.Add(v.Code);
                            Row.Add(v.Quantity.ToString());
                            // Giá nhập sản phẩm
                            Row.Add(string.Format("{0:#,####}", v.Price.ToString()));
                            Row.Add(string.Format("{0:#,####}", v.Salary.ToString()));
                            Row.Add(string.Format("{0:#,####}", (v.Price * v.Quantity).ToString()));
                            Row.Add(string.Format("{0:#,####}", v.MoneyPercent.ToString()));
                            Row.Add(string.Format("{0:#,####}", (v.Money).ToString()));
                            RowLST.Add(Row);

                        }
                        var ExcelStorePath = Server.MapPath("~") + "Public/Excel/Hoa.Don/";
                        var FileName = "My_pham_ban_tong_hop_" + string.Format("{0:dd_MM_yyyy_hh_mm_ss}", DateTime.Now) + ".xlsx";
                        var UrlFileName = "http://" + Request.Url.Host + "/Public/Excel/Hoa.Don/" + FileName;
                        ExportXcel(_ExcelHeadRow, RowLST, ExcelStorePath + FileName);
                        Bind_Paging(int.Parse(HDF_EXCELPage.Value));
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Download Excel", "CallBackExportExcel('" + UrlFileName + "');UP_FormatPrice('.be-report-price'); SubTableFixHeight();SetActiveTableListing(); alert(\"Xuất excel thành công.\")", true);
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_ExcelHeadRow"></param>
        /// <param name="List"></param>
        /// <param name="Path"></param>
        private static void ExportXcel(List<string> _ExcelHeadRow, List<List<string>> List, string Path)
        {
            try
            {
                CreateExcelFile.ExcelHeadRow = _ExcelHeadRow;
                CreateExcelFile.CreateExcel(List, Path);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi. Không tạo được file excel.\r\nException: " + ex.Message);
            }
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }
    }

    public class Items
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public int Total_KT { get; set; }
        public int Total_TDN { get; set; }
        public int Total_DC { get; set; }
        public int Total { get; set; }
    }

    public class cls_totalReport
    {
        public double TongTienNhapHang { get; set; }
        public double TongTienDoanhThu { get; set; }
        public double TongLuongMyPham { get; set; }
        public double TongLoiNhuanGop { get; set; }
        public int TongSoLuot { get; set; }
    }
    public class ExportDataCosmeticForMisa
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public int Cost { get; set; }
        public int Price { get; set; }
        public int Quantity { get; set; }
        public double Money { get; set; }
        public double Salary { get; set; }
        public string CreatedDate { get; set; }
        public string SalonName { get; set; }
        public int VoucherPercent { get; set; }
        public double MoneyPercent { get; set; }
    }

    public class cls_saleItems
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public int Quantity { get; set; }
        public double Salary { get; set; }
        public int? Price { get; set; }
        public double MoneyPercent { get; set; }

    }
}