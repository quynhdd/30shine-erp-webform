﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using LinqKit;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.Report
{
    public partial class ThongKeDoanhSoCheckin : System.Web.UI.Page
    {
        private string PageID = "BC_DS_CK";
        protected Paging PAGING = new Paging();

        protected bool Staff_All = false;
        protected bool Staff_Hairdresser = false;
        protected bool Staff_HairMassage = false;
        protected bool Staff_Singer = false;
        protected string THead = "";
        public CultureInfo culture = new CultureInfo("vi-VN");
        public DateTime timeFrom;
        public DateTime timeTo;
        private Expression<Func<Staff, bool>> WhereStaff = PredicateBuilder.True<Staff>();
        private List<Service> _Service = new List<Service>();
        private List<Product> _Product = new List<Product>();
        protected string FirstDayOfMonth = string.Format("{0:dd/MM/yyyy}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1));
        protected string Today = String.Format("{0:dd/MM/yyyy}", DateTime.Now);
        protected int ThisMon;// = DateTime.Now.Month;
        protected int DayOfMon;// = DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month);;
        protected int ConfigWorkDay = 0;

        private bool Perm_ViewAllData = false;
        private bool Perm_Access = false;
        protected bool isRoot = false;
        private int salonID;
        private int staffID;
        protected int? totalPoint = 0;
        protected int? totalMoney = 0;
        protected int? PointBefore15 = 3; //Đơn vị điểm cho khách book
        protected int? PointBetween15and20 = 1; //Đơn vị điểm cho khách book
        protected int? PointAfter20 = 0; //Đơn vị điểm cho khách book
        protected int? PointNull = 1; //Đơn vị điểm cho khách book ###
        protected Store_Staff_WaitTime_Result dataCheckin;
        public Solution_30shineEntities db = new Solution_30shineEntities();
        protected Store_Staff_WaitTime_Result totalWaitTime;
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                ////Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                ////Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                TxtDateTimeFrom.Text = FirstDayOfMonth;
                TxtDateTimeTo.Text = Today;
                Library.Function.bindSalon(new List<DropDownList> { Salon }, Perm_ViewAllData);
                bindDepartment();
                bindStaff();
            }
            else
            {
                //
            }
            RemoveLoading();
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public ThongKeDoanhSoCheckin()
        {
            db = new Solution_30shineEntities();
            totalWaitTime = new Store_Staff_WaitTime_Result();
            totalWaitTime.PointBefore15 = 0;
            totalWaitTime.PointAfter20 = 0;
            totalWaitTime.PointBetween15and20 = 0;
            totalWaitTime.PointNotTime = 0;
        }

        /// <summary>
        /// Get StaffID
        /// </summary>
        /// <returns></returns>
        private int getStaffID()
        {
            try
            {
                return Convert.ToInt32(ddlStaff.SelectedValue);
                
            }
            catch (Exception)
            {

                return 0;
            }
        }

        /// <summary>
        /// Get SalonID
        /// </summary>
        /// <returns></returns>
        private int getSalonID()
        {
            try
            {
                return Convert.ToInt32(Salon.SelectedValue);
            }
            catch (Exception)
            {
                return 0;
            }
        }

        /// <summary>
        /// Lấy dữ liệu
        /// </summary>
        /// <returns></returns>
        private List<cls_WaitTime_staff> getData()
        {
            List<cls_WaitTime_staff> ListTotalWaitTime = new List<cls_WaitTime_staff>();
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    this.salonID = this.getSalonID();
                    this.staffID = this.getStaffID();
                    var timeFrom = Library.Format.getDateTimeFromString(TxtDateTimeFrom.Text);
                    if (timeFrom == null)
                    {
                        throw new Exception("Vui lòng nhập ngày bắt đầu");
                    }
                    var timeTo = Library.Format.getDateTimeFromString(TxtDateTimeTo.Text);
                    if (timeTo != null)
                    {
                        timeTo = timeTo.Value.AddDays(1);
                    }
                    else
                    {
                        timeTo = timeFrom.Value.AddDays(1);
                    }
                   
                    if (this.staffID > 0)
                    {
                        Store_Staff_WaitTime_Result list = db.Store_Staff_WaitTime(timeFrom, timeTo, this.staffID).FirstOrDefault();
                        if (list != null)
                        {
                            cls_WaitTime_staff data = new cls_WaitTime_staff();
                            data.PointAfter20 = list.PointAfter20;
                            data.PointBefore15 = list.PointBefore15;
                            data.PointBetween15and20 = list.PointBetween15and20;
                            data.PointNotTime = list.PointNotTime;
                            data.totalPoint = list.PointAfter20 * this.PointAfter20 + list.PointBefore15 * this.PointBefore15 + list.PointBetween15and20 * this.PointBetween15and20 + list.PointNotTime * this.PointNull;
                            data.totalMoney = data.totalPoint * 750;
                            data.StaffName = ddlStaff.SelectedItem.Text;
                            ListTotalWaitTime.Add(data);
                        }
                       
                    }
                    else
                    {
                        try
                        {
                            var ListCheckin = db.Staffs.Where(r => r.IsDelete == 0 && r.Active == 1 && (r.isAccountLogin != 1 || r.isAccountLogin == null)
                            && r.SalonId == this.salonID && r.Type == 5).Select(r=> new { Name=r.Fullname, Id=r.Id}).ToList();
                            if (ListCheckin != null)
                            {
                                foreach (var i in ListCheckin)
                                {
                                    var list = db.Store_Staff_WaitTime(timeFrom, timeTo, i.Id).FirstOrDefault();
                                    if (list!=null)
                                    {
                                        cls_WaitTime_staff data = new cls_WaitTime_staff();
                                        data.PointAfter20 = list.PointAfter20;
                                        data.PointBefore15 = list.PointBefore15;
                                        data.PointBetween15and20 = list.PointBetween15and20;
                                        data.PointNotTime = list.PointNotTime;
                                        data.totalPoint = list.PointAfter20 * this.PointAfter20 + list.PointBefore15 * this.PointBefore15 + list.PointBetween15and20 * this.PointBetween15and20 + list.PointNotTime * this.PointNull;
                                        data.totalMoney = data.totalPoint * 750;
                                        data.StaffName = i.Name;
                                        ListTotalWaitTime.Add(data);
                                        
                                    }
                                }
                            }
                        }
                        catch(Exception ex) { }

                    }
                    if (ListTotalWaitTime != null)
                    {
                        rptSalaryCheckin.DataSource = ListTotalWaitTime;
                        rptSalaryCheckin.DataBind();
                    }
                    
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return ListTotalWaitTime;
        }

        /// <summary>
        /// Bind Department
        /// </summary>
        private void bindDepartment()
        {
            using (var db = new Solution_30shineEntities())
            {
                var department = db.Staff_Type.Where(w => w.IsDelete != 1 && w.Publish == true).ToList();
                var first = new Staff_Type();
                first.Id = 0;
                first.Name = "Chọn bộ phận";
                department.Insert(0, first);

                ddlDepartment.DataTextField = "Name";
                ddlDepartment.DataValueField = "Id";
                ddlDepartment.DataSource = department;
                ddlDepartment.DataBind();
                ddlDepartment.SelectedIndex = 0;
            }
        }

        /// <summary>
        /// Bind list nhân viên
        /// </summary>
        private void bindStaff()
        {
            using (var db = new Solution_30shineEntities())
            {
                var department = 5;
                var salon = Convert.ToInt32(Salon.SelectedValue);
                var staff = new List<Staff>();
                var whereSalon = "";
                var whereDepartment = "";
                var sql = "";
                if (department > 0)
                {
                    whereDepartment = " and [Type] = " + department;
                }
                if (salon > 0)
                {
                    whereSalon = " and SalonId = " + salon;
                }
                sql = @"select * from Staff 
                        where IsDelete != 1 and Active = 1 
                        and (isAccountLogin != 1 or isAccountLogin is null)" + whereSalon + whereDepartment;
                staff = db.Staffs.SqlQuery(sql).ToList();

                var first = new Staff();
                first.Id = 0;
                first.Fullname = "Chọn nhân viên";
                staff.Insert(0, first);

                ddlStaff.DataTextField = "Fullname";
                ddlStaff.DataValueField = "Id";
                ddlStaff.DataSource = staff;
                ddlStaff.DataBind();
                //ddlStaff.SelectedIndex = 0;
            }
        }

        /// <summary>
        /// Thông kê thời gian chờ
        /// </summary>
        /// Nguyễn Mạnh Dũng 
        /// 08/08/2017
        /// <returns></returns>
        public int GetWaitTime(int BillID)
        {
            BillService bs = new BillService();
            var WaitTime = 0;
            try
            {
                //Check Id
                var _id = db.BillServices.Where(o => o.Id == BillID).ToString();
                if (_id != null)
                {
                    //Check bookid 
                    var book = db.Bookings.FirstOrDefault(f => f.Id == bs.BookingId);
                    if (book != null && book.IsBookOnline == true)
                    {
                        DateTime? pvgTime = bs.InProcedureTime;
                        DateTime? checkinTime = bs.CreatedDate;
                        DateTime bookHour = new DateTime();
                        var hour = db.BookHours.FirstOrDefault(w => w.Id == book.HourId);

                        //Check các error
                        if (bs.CreatedDate == null)
                        {
                            throw new Exception("Lỗi thời gian checkin");
                        }

                        if (bs.InProcedureTime == null)
                        {
                            throw new Exception("Lỗi thời gian gội");
                        }

                        if (book.CreatedDate == null)
                        {
                            throw new Exception("Lỗi thời gian checkin");
                        }

                        //Nếu khác null gán bookHour có value là Datetime
                        if (hour != null && hour.HourFrame != null)
                        {
                            bookHour = book.CreatedDate.Value.Date.Add(hour.HourFrame.Value);
                        }
                        else
                        {
                            throw new Exception("Lỗi giờ book.");
                        }

                        //TH1: Nếu checkin sau giờ book
                        if (checkinTime >= bookHour)
                        {
                            WaitTime = ((DateTime)pvgTime - (DateTime)checkinTime).Minutes;

                        }
                        //TH2: Nếu checkin trước giờ book
                        else
                        {
                            WaitTime = ((DateTime)pvgTime - (DateTime)bookHour).Minutes;

                        }
                    }

                }
            }
            catch (Exception ex)
            {
                throw new Exception("Đã xảy ra lỗi trong quá trình thực hiện!!! " + ex.Message);
            }

            return WaitTime;
        }

        /// <summary>
        /// Bind danh sách nhân viên theo bộ phận
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void bindStaffByDepartment(object sender, EventArgs e)
        {
            bindStaff();
        }

        /// <summary>
        /// event click to load data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void _BtnClick(object sender, EventArgs e)
        {
            //Bind();
            getData();
            ScriptManager.RegisterStartupScript(this, GetType(), "Script", "SetActiveTableListing();", true);
            RemoveLoading();
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        ///// <summary>
        ///// Check permission
        ///// </summary>
        //private void CheckPermission()
        //{
        //    if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
        //    {
        //        var MsgParam = new List<KeyValuePair<string, string>>();
        //        UIHelpers.Redirect("/dang-nhap.html", MsgParam);
        //    }
        //    else
        //    {
        //        string[] Allow = new string[] { "root", "admin", "accountant", "salonmanager", "reception", "checkin" };
        //        string[] AllowViewAllDate = new string[] { "root", "admin", "accountant" };
        //        string[] Root = new string[] { "root" };
        //        var Permission = Session["User_Permission"].ToString().Trim();
        //        if (Array.IndexOf(Allow, Permission) != -1)
        //        {
        //            Perm_Access = true;
        //        }

        //        if (Array.IndexOf(AllowViewAllDate, Permission) != -1)
        //        {
        //            Perm_ViewAllData = true;
        //        }
        //        if (Array.IndexOf(Root, Permission) != -1)
        //        {
        //            isRoot = true;
        //        }
        //    }

        //    // Call execute function
        //    ExecuteByPermission();
        //}

        //private void ExecuteByPermission()
        //{
        //    if (!Perm_Access)
        //    {
        //        ContentWrap.Visible = false;
        //        NotAllowAccess.Visible = true;
        //    }
        //}
    }

    public class cls_staff : Staff
    {
        /// <summary>
        /// Tên bộ phận
        /// </summary>
        public string departmentName { get; set; }
        /// <summary>
        /// Tên bậc kỹ năng
        /// </summary>
        public string skillLevelName { get; set; }
        /// <summary>
        /// Chấm công
        /// </summary>
        public int workDay { get; set; }
        /// <summary>
        /// Lương làm thêm, part-time
        /// </summary>
        public double partTimeSalary { get; set; }
        /// <summary>
        /// Lương phụ cấp
        /// </summary>
        public double allowanceSalary { get; set; }
        /// <summary>
        /// Lương mỹ phẩm
        /// </summary>
        public double productSalary { get; set; }
        /// <summary>
        /// Lương dịch vụ
        /// </summary>
        public double serviceSalary { get; set; }
        /// <summary>
        /// Lương cứng (tính theo ngày công)
        /// </summary>
        public double fixSalary { get; set; }
        /// <summary>
        /// Lương cứng cơ bản theo tháng
        /// </summary>
        public int baseSalary { get; set; }
        /// <summary>
        /// Ngày công trong tháng (= ngày của tháng - 2)
        /// </summary>
        public int workDayInMonth { get; set; }
        /// <summary>
        /// Thưởng / Phạt
        /// </summary>
        public int mistake { get; set; }
    }

    public class cls_Store_Staff_WaitTime
    {
        public int Id { get; set; }
        public string CustomerCode { get; set; }
        public Nullable<int> CustomerId { get; set; }
        public Nullable<int> ReceptionId { get; set; }
        public Nullable<int> BookingId { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> InProcedureTime { get; set; }
        public Nullable<System.DateTime> DateTimeBook { get; set; }
        public Nullable<int> WaitTimeInProcedure { get; set; }
    }
    public class cls_reportTotal
    {
        /// <summary>
        /// Bộ phận ID
        /// </summary>
        public int departmentId { get; set; }
        /// <summary>
        /// Tên bộ phận
        /// </summary>
        public string departmentName { get; set; }
        /// <summary>
        /// Tổng lương (ko tính lương sản phẩm)
        /// </summary>
        public double salaryNoProduct { get; set; }
        /// <summary>
        /// Tổng lương
        /// </summary>
        public double salary { get; set; }
    }
    public class cls_WaitTime_staff
    {
        public Nullable<int> PointBefore15 { get; set; }
        public Nullable<int> PointBetween15and20 { get; set; }
        public Nullable<int> PointAfter20 { get; set; }
        public Nullable<int> PointNotTime { get; set; }
        public double? totalPoint { get; set; }
        public double? totalMoney { get; set; }
        public string StaffName { get; set; }
        
    }
}