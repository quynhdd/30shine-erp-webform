﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Globalization;
using LinqKit;
using System.Linq.Expressions;
using ExportToExcel;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.Report
{
    public partial class Report_Service : System.Web.UI.Page
    {
        private string PageID = "BC_DV";
        protected Paging PAGING = new Paging();

        private int _TypeStaff;
        private int _StaffId;
        private bool IsProductFilter = false;

        protected bool Staff_All = false;
        protected bool Staff_Hairdresser = false;
        protected bool Staff_HairMassage = false;
        protected bool Staff_Singer = false;
        protected string THead = "";
        protected string StrProductPrice = "";
        public CultureInfo culture = new CultureInfo("vi-VN");
        public string timeFrom;
        public string timeTo;
        private Expression<Func<Staff, bool>> WhereStaff = PredicateBuilder.True<Staff>();
        private List<Service> _Service = new List<Service>();
        private List<Product> _Product = new List<Product>();
        protected string FirstDayOfMonth = string.Format("{0:dd/MM/yyyy}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1));
        protected string Today = String.Format("{0:dd/MM/yyyy}", DateTime.Now);
        public string sqlWhereStaff = "";
        protected int SalonId;
        protected cls_totalReport totalReport = new cls_totalReport();

        protected string Day1 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-2));
        protected string Day2 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-1));
        protected string Day3 = string.Format("{0:dd/MM/yyyy}", DateTime.Now);

        public bool Perm_ViewAllData = false;
        private bool Perm_Access = false;
        protected string Permission = "";
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (!IsPostBack)
            {
                TxtDateTimeFrom.Text = FirstDayOfMonth;
                TxtDateTimeTo.Text = Today;
                Library.Function.bindSalon(new List<DropDownList> { Salon }, Perm_ViewAllData);
            }
            //RemoveLoading();
        }

        private void GenWhere()
        {
            _TypeStaff = HDF_TypeStaff.Value != "" ? Convert.ToInt32(HDF_TypeStaff.Value) : 0;
            _StaffId = HDF_Staff.Value != "" ? Convert.ToInt32(HDF_Staff.Value) : 0;

            if (_StaffId > 0)
            {
                WhereStaff = WhereStaff.And(w => w.Id == _StaffId);
                sqlWhereStaff += "and a.[SellerId] = " + _StaffId;
            }
            else if (_TypeStaff > 0)
            {
                WhereStaff = WhereStaff.And(w => w.Type == _TypeStaff);
                sqlWhereStaff += "and b.[Type] = " + _TypeStaff;
            }
            WhereStaff = WhereStaff.And(w => w.IsDelete != 1);

            SalonId = Convert.ToInt32(Salon.SelectedValue);
            if (!Perm_ViewAllData)
            {
                WhereStaff = WhereStaff.And(p => p.SalonId == SalonId);
            }
            else
            {
                if (SalonId > 0)
                {
                    WhereStaff = WhereStaff.And(p => p.SalonId == SalonId);
                }
                else
                {
                    WhereStaff = WhereStaff.And(p => p.SalonId > 0);
                }
            }

            WhereStaff = WhereStaff.And(w => w.Permission != "accountant" && w.Permission != "admin");

            DateTime d1 = Convert.ToDateTime(TxtDateTimeFrom.Text.Trim(), culture);
            DateTime d2;
            if (TxtDateTimeTo.Text.Trim() != "")
            {
                d2 = Convert.ToDateTime(TxtDateTimeTo.Text.Trim(), culture).AddDays(1);
            }
            else
            {
                d2 = d1.AddDays(1);
            }
            timeFrom = String.Format("{0:MM/dd/yyyy}", d1);
            timeTo = String.Format("{0:MM/dd/yyyy}", d2);
        }

        private string genSql()
        {
            string sql = "";
            string whereSalon = "";
            string whereTime = "";
            DateTime timeFrom = new DateTime();
            DateTime timeTo = new DateTime();

            SalonId = Convert.ToInt32(Salon.SelectedValue);
            if (!Perm_ViewAllData)
            {
                whereSalon = " and bill.SalonId = " + SalonId;
            }
            else
            {
                if (SalonId > 0)
                {
                    whereSalon = " and bill.SalonId = " + SalonId;
                }
                else
                {
                    whereSalon = " and bill.SalonId > 0";
                }
            }

            if (TxtDateTimeFrom.Text != "")
            {
                timeFrom = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                if (TxtDateTimeTo.Text != "")
                {
                    timeTo = Convert.ToDateTime(TxtDateTimeTo.Text, culture).AddDays(1);
                }
                else
                {
                    timeTo = timeFrom.AddDays(1);
                }
                whereTime = " and bill.CreatedDate between '" + string.Format("{0:yyyy/MM/dd}", timeFrom) + "' and '" + string.Format("{0:yyyy/MM/dd}", timeTo) + "'";
            }
            sql = @"
                    BEGIN 
					    WITH temp AS
						    (
						    SELECT 
							       flowService.ServiceId AS serviceId,
							       FORMAT( bill.CreatedDate, 'dd/MM/yyyy', 'en-US' ) AS workDate,
							       salon.ShortName AS salonName,
							       bill.SalonId AS salonId,
							       s.Code AS Code, 
							       s.Name AS Name,
							       flowService.Price AS price, 
							       flowService.VoucherPercent  AS voucherPercent,
							       flowService.Quantity AS quantity,
							       CAST (SUM( flowService.Price * flowService.Quantity * (CAST(100 - flowService.VoucherPercent AS FLOAT)/Cast(100 AS FLOAT)) ) AS BIGINT) AS totalMoney
						    FROM dbo.BillServiceHis AS bill
							      INNER JOIN FlowService AS flowService ON  bill.Id = flowService.BillId and flowService.IsDelete != 1
							      LEFT JOIN dbo.Service AS s ON s.Id = flowService.ServiceId
							      LEFT JOIN dbo.Tbl_Salon AS salon ON salon.Id = bill.SalonId
						    WHERE bill.IsDelete != 1 AND bill.Pending != 1 AND salon.IsDelete = 0 AND flowService.IsDelete = 0 AND s.IsDelete = 0 " +
                                    whereTime + whereSalon +
                            @"GROUP BY bill.CreatedDate,salon.ShortName,s.Code,s.Name,flowService.Price,flowService.Quantity,flowService.VoucherPercent,flowService.ServiceId,bill.SalonId
						    )
					    SELECT  temp.serviceId as Id,
                                temp.salonId,
                                temp.workDate,
                                temp.salonName,
                                temp.Code,
                                temp.Name,
                                temp.price,
                                temp.voucherPercent AS voucherPercentFlow, 
                                SUM(temp.quantity) AS quantity, 
                                SUM(temp.quantity) AS times,
                                SUM(temp.totalMoney) AS totalMoney   
					    FROM temp 
					    GROUP BY temp.workDate,temp.salonId,temp.salonName,temp.Name,temp.price,temp.voucherPercent,temp.serviceId,temp.Code
                        ORDER BY temp.salonName DESC
				    END
                        ";

            return sql;
        }

        private List<cls_reportService> getData()
        {
            var list = new List<cls_reportService>();
            using (var db = new Solution_30shineEntities())
            {
                string sql = genSql();
                if (sql != "")
                {
                    list = db.Database.SqlQuery<cls_reportService>(sql).ToList();
                }
            }
            return list;
        }

        public void Exc_Filter()
        {
            var LST = new List<cls_reportService>();
            LST = getData();
            if (LST.Count > 0)
            {
                foreach (var v in LST)
                {
                    totalReport.TongSoLuot = v.times;
                    totalReport.TongTienDoanhThu += v.totalMoney;
                }
                Bind_Paging(LST.Count);
            }
            RptTimeKeeping.DataSource = LST.Skip(PAGING._Offset).Take(PAGING._Segment).ToList(); ;
            RptTimeKeeping.DataBind();
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            //Bind_Paging();
            Exc_Filter();

            if (IsProductFilter)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Script", "SetActiveTableListing();showTrPrice();", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Script", "SetActiveTableListing();", true);
            }

            RemoveLoading();
        }

        /// <summary>
        /// Paging
        /// </summary>
        /// <param name="TotalRow"></param>
        protected void Bind_Paging(int TotalRow)
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(TotalRow) : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage(int TotalRow)
        {
            TotalRow -= PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Exc_ExportExcel(object sender, EventArgs e)
        {
            using (var db = new Solution_30shineEntities())
            {
                if (TxtDateTimeFrom.Text.Trim() != "")
                {
                    var list = getData();
                    var _ExcelHeadRow = new List<string>();
                    var RowLST = new List<List<string>>();
                    var Row = new List<string>();
                    _ExcelHeadRow.Add("Ngày");
                    _ExcelHeadRow.Add("Tên salon");
                    _ExcelHeadRow.Add("Mã dịch vụ");
                    _ExcelHeadRow.Add("Tên dịch vụ");
                    _ExcelHeadRow.Add("Số lượng");
                    _ExcelHeadRow.Add("Đơn giá");
                    _ExcelHeadRow.Add("Thành tiền");
                    _ExcelHeadRow.Add("Chiết khấu %");
                    _ExcelHeadRow.Add("Tiền chiết khấu");
                    _ExcelHeadRow.Add("Tổng doanh thu");
                    if (list.Count > 0)
                    {
                        var TienChietKhau = 0;
                        foreach (var v in list)
                        {
                            TienChietKhau = (v.Quantity * v.Price ?? 0) - Convert.ToInt32(v.totalMoney);
                            Row = new List<string>();
                            Row.Add(v.workDate);
                            Row.Add(v.salonName);
                            Row.Add(v.Code);
                            Row.Add(v.Name);
                            Row.Add(v.Quantity.ToString());
                            Row.Add(v.Price.ToString());
                            Row.Add((v.Quantity * v.Price ?? 0).ToString());
                            Row.Add(v.voucherPercentFlow.ToString());
                            Row.Add(TienChietKhau.ToString());
                            Row.Add(string.Format("{0:#,####}", v.totalMoney.ToString()));
                            RowLST.Add(Row);

                        }
                        var ExcelStorePath = Server.MapPath("~") + "Public/Excel/Hoa.Don/";
                        var FileName = "Dich_vu_" + string.Format("{0:dd_MM_yyyy_hh_mm_ss}", DateTime.Now) + ".xlsx";
                        var UrlFileName = "http://" + Request.Url.Host + "/Public/Excel/Hoa.Don/" + FileName;
                        ExportXcel(_ExcelHeadRow, RowLST, ExcelStorePath + FileName);
                        Bind_Paging(int.Parse(HDF_EXCELPage.Value));
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Download Excel", "CallBackExportExcel('" + UrlFileName + "');UP_FormatPrice('.be-report-price'); SubTableFixHeight();SetActiveTableListing(); alert(\"Xuất excel thành công.\")", true);
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_ExcelHeadRow"></param>
        /// <param name="List"></param>
        /// <param name="Path"></param>
        private static void ExportXcel(List<string> _ExcelHeadRow, List<List<string>> List, string Path)
        {
            try
            {
                CreateExcelFile.ExcelHeadRow = _ExcelHeadRow;
                CreateExcelFile.CreateExcel(List, Path);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi. Không tạo được file excel.\r\nException: " + ex.Message);
            }
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }
    }

    public class cls_reportService : Service
    {
        public int voucherPercentFlow { get; set; }
        public int salonId { get; set; }
        public string salonName { get; set; }
        public string workDate { get; set; }
        public int times { get; set; }
        public long totalMoney { get; set; }
    }
}