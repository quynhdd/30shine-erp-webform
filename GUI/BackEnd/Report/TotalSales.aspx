﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TotalSales.aspx.cs" Inherits="_30shine.GUI.BackEnd.Report.TotalSales" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<%--<%@ Register Src="~/GUI/BackEnd/Report/ReportMenu.ascx" TagName="ReportMenu" TagPrefix="uc1" %>--%>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

        <%--<uc1:ReportMenu runat="server" id="ReportMenu" />--%>

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Báo cáo &nbsp;&#187; </li>
                        <li class="be-report-li"><a href="javascript:void(0);"><i class="fa fa-th-large"></i>Mỹ phẩm</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing be-report be-report-timekeeping">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <asp:ScriptManager ID="SM01" runat="server"></asp:ScriptManager>
                <div class="row">
                    <div class="filter-item">
                        <strong class="st-head"><i class="fa fa-clock-o"></i>Thời gian</strong>
                        <div class="time-wp">
                            <asp:TextBox CssClass="txtDateTime st-head" ID="TxtDateTimeFrom" placeholder="Từ ngày" Style="margin-left: 10px!important"
                                ClientIDMode="Static" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="ValidateTime" ControlToValidate="TxtDateTimeFrom" runat="server" CssClass="fb-cover-error" Text="Vui lòng chọn thời gian!"></asp:RequiredFieldValidator>
                        </div>
                        <strong class="st-head" style="margin-left: 10px;">
                            <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                        </strong>
                        <asp:TextBox CssClass="txtDateTime st-head" ID="TxtDateTimeTo" placeholder="Đến ngày" Style="margin-left: 10px!important"
                            ClientIDMode="Static" runat="server"></asp:TextBox>
                        <br />
                        <div class="tag-wp" style="padding-top: 0px; padding-left: 15px; margin-bottom: 20px;">
                            <p class="tag tag-date" onclick="viewDataByDate($(this), '<%=Day1 %>')">Hôm kia <span class="txt-date"><%=Day1 %></span></p>
                            <p class="tag tag-date tag-date-yesterday" onclick="viewDataByDate($(this), '<%=Day2 %>')">Hôm qua<span class="txt-date"><%=Day2 %></span></p>
                            <p class="tag tag-date tag-date-today" onclick="viewDataByDate($(this), '<%=Day3 %>')">Hôm nay<span class="txt-date"><%=Day3 %></span></p>
                        </div>
                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="Salon" runat="server" ClientIDMode="Static" Style="width: 190px;"></asp:DropDownList>
                    </div>
                    <%--<div class="filter-item">
                <select id="KindView" name="KindView" runat="server" ClientIDMode="Static" style="margin: 12px 0; width: 190px;" >
                    <option value="1" selected="selected">Báo cáo theo dịch vụ</option>
                    <option value="2">Báo cáo theo sản phẩm</option>
                </select>
            </div>--%>

                    <asp:Panel ID="ViewData" CssClass="st-head btn-viewdata" ClientIDMode="Static"
                        onclick="excPaging(1)" runat="server">
                        Xem dữ liệu
                    </asp:Panel>
                    <%if (Perm_ViewAllData)
                        {%>
                    <%-- <asp:Panel ID="fff" CssClass="st-head btn-viewdata"
                        ClientIDMode="Static" runat="server" onclick="EntryExportExcel('all')">
                        Xuất File Excel
                    </asp:Panel>--%>
                    <asp:Panel ID="ExcForMisa" CssClass="st-head btn-viewdata"
                        ClientIDMode="Static" runat="server" onclick="EntryExportExcelForMisa()">
                        Xuất BC chi tiết
                    </asp:Panel>
                    <asp:Panel ID="Panel1" CssClass="st-head btn-viewdata"
                        ClientIDMode="Static" runat="server" onclick="EntryExportExcel()">
                        Xuất BC tổng hợp
                    </asp:Panel>
                    <%} %>
                    <a href="/admin/bao-cao/doanh-so-my-pham.html" class="st-head btn-viewdata">Reset Filter</a>
                    <%--<div class="export-wp drop-down-list-wp">
                <asp:Panel ID="fff" CssClass="st-head btn-viewdata"
                    ClientIDMode="Static" runat="server" onclick="$(this).parent().find('.ul-drop-down-list').toggle();">Xuất File Excel</asp:Panel>
                <ul class="ul-drop-down-list" style="display:none;">
                    <li onclick="$('#HDF_ExportType').val('money');EntryExportExcel('all')">Tiền công</li>
                    <li onclick="$('#HDF_ExportType').val('quantity');EntryExportExcel('all')">Số lượng DV/SP</li>
                </ul>                
            </div>--%>
                </div>
                <div class="row">
                    <strong class="st-head"><i class="fa fa-file-text"></i>Số lượng bán ra</strong>
                </div>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" ClientIDMode="Static">
                    <ContentTemplate>

                        <div class="row">
                            <table class="table-total-report-1">
                                <tbody>
                                    <% if (Perm_ViewAllData)
                                        { %>
                                    <tr>
                                        <td>Tổng thành tiền</td>
                                        <td style="text-align: right; padding-left: 20px;">
                                            <span style="font-family: Roboto Condensed Bold;"><%=String.Format("{0:#,###}",totalReport.TongTienNhapHang).Replace(',','.') %></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Tổng chiết khấu</td>
                                        <td style="text-align: right; padding-left: 20px;">
                                            <span style="font-family: Roboto Condensed Bold;"><%=String.Format("{0:#,###}",totalReport.TongLuongMyPham).Replace(',','.') %></span>
                                        </td>
                                    </tr>
                                    <% } %>
                                    <tr>
                                        <td>Tổng doanh thu</td>
                                        <td style="text-align: right; padding-left: 20px;">
                                            <span style="font-family: Roboto Condensed Bold;"><%=String.Format("{0:#,###}",totalReport.TongTienDoanhThu).Replace(',','.') %></span>
                                        </td>
                                    </tr>
                               <%--     <% if (Perm_ViewAllData)
                                        { %>
                                    <tr>
                                        <td>Tổng lợi nhuận gộp</td>
                                        <td style="text-align: right; padding-left: 20px;">
                                            <span style="font-family: Roboto Condensed Bold;"><%=String.Format("{0:#,###}",totalReport.TongLoiNhuanGop).Replace(',','.') %></span>
                                        </td>
                                    </tr>
                                    <% } %>--%>
                                </tbody>
                            </table>
                        </div>

                    </ContentTemplate>
                </asp:UpdatePanel>
                <!-- Row Table Filter -->
                <div class="table-func-panel" style="margin-top: -33px;">
                    <div class="table-func-elm">
                        <span>Số hàng / Page : </span>
                        <div class="table-func-input-wp">
                            <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                            <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                            <ul class="ul-opt-segment">
                                <li data-value="10">10</li>
                                <li data-value="20">20</li>
                                <li data-value="30">30</li>
                                <li data-value="40">40</li>
                                <li data-value="50">50</li>
                                <li data-value="1000000">Tất cả</li>
                            </ul>
                            <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                        </div>
                    </div>
                </div>
                <!-- End Row Table Filter -->
                <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                    <ContentTemplate>
                        <div class="row table-wp">
                            <table class="table-add table-listing">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Ngày</th>
                                        <th>Tên Salon</th>
                                        <th>ID</th>
                                        <th>Item</th>
                                        <th>Mã ( Code )</th>
                                        <th>Số lượng</th>
                                        <th>Đơn giá</th>
                                        <% if (Perm_ViewAllData)
                                            { %>
                                        <th>Lương nhân viên</th>
                                        <% } %>
                                        <th>Thành tiền</th>
                                        <% if (Perm_ViewAllData)
                                            { %>
                                        <th>Chiết khấu (%)</th>
                                        <th>Tiền chiết khấu</th>
                                        <th>Tổng doanh thu</th>
                                        <% } %>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="RptTimeKeeping" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# Container.ItemIndex + 1 + PAGING._Offset %></td>
                                                <td><%# Eval("CreatedDate") %></td>
                                                <td><%# Eval("SalonName") %></td>
                                                <td><%# Eval("Id") %></td>
                                                <td><%# Eval("Name") %></td>
                                                <td><%# Eval("Code") %></td>
                                                <td><%# Eval("Quantity") %></td>
                                                <td><%# String.Format("{0:#,###}",Convert.ToInt32(Eval("Price"))).Replace(',','.')%></td>
                                                <% if (Perm_ViewAllData)
                                                    { %>
                                                <td><%# String.Format("{0:#,###}",Convert.ToInt32(Eval("Salary"))).Replace(',','.') %></td>
                                                <% } %>
                                                <td>
                                                    <%# String.Format("{0:#,###}",(Convert.ToDouble(Eval("Price")) * Convert.ToInt32(Eval("Quantity")))).Replace(',','.') %>
                                                </td>
                                                <% if (Perm_ViewAllData)
                                                    { %>
                                                <td><%# Eval("VoucherPercent") %></td>
                                                <td>
                                                    <%# String.Format("{0:#,###}",(Convert.ToDouble(Eval("Price")) * Convert.ToInt32(Eval("VoucherPercent"))) / 100).Replace(',','.') %>
                                                </td>
                                                <td>
                                                    <%# String.Format("{0:#,###}",
                                                            (Convert.ToDouble(Eval("Price")) * Convert.ToInt32(Eval("Quantity"))) 
                                                         -  (Convert.ToDouble(Eval("Price")) * Convert.ToInt32(Eval("VoucherPercent"))) / 100 ).Replace(',','.') %>
                                                </td>
                                                <% } %>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>
                        <!-- Paging -->
                        <div class="site-paging-wp">
                            <% if (PAGING.TotalPage > 1)
                                { %>
                            <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                                <% if (PAGING._Paging.Prev != 0)
                                    { %>
                                <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                                <% } %>
                                <asp:Repeater ID="RptPaging" runat="server">
                                    <ItemTemplate>
                                        <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                            <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>>
                                            <%# Eval("PageNum") %>
                                        </a>
                                    </ItemTemplate>
                                </asp:Repeater>
                                W
                                <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                                    { %>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                                <% } %>
                            </asp:Panel>
                            <% } %>
                        </div>
                        <!-- End Paging -->
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                <asp:Button ID="BtnFakeExcel" ClientIDMode="Static" runat="server" OnClick="Exc_ExportExcel" Style="display: none;" />
                <asp:Button ID="BtnFakeExcelForMisa" ClientIDMode="Static" runat="server" OnClick="Exc_ExportExcelForMisa" Style="display: none;" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />

                <!-- Hidden Field-->
                <asp:HiddenField ID="HDF_TypeStaff" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="HDF_Staff" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="HDF_ExcelType" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="HDF_ExportType" ClientIDMode="Static" runat="server" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HiddenField1" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_EXCELPage" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_EXCELSegment" />
                <!-- END Hidden Field-->
            </div>
            <%-- END Listing --%>
        </div>

        <link href="/Assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" />
        <script src="/Assets/js/jquery.mCustomScrollbar.js"></script>
        <script type="text/ecmascript">
            jQuery(document).ready(function () {
                // Add active menu
                $("#glbAdminSales").addClass("active");
                $("#glbAdminReportProduct").addClass("active");
                $("li.be-report-li").addClass("active");

                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) { }
                });

                //============================
                // Bind Staff
                //============================
                BindStaffFilter();

                // View data yesterday
                $(".tag-date-yesterday").click();

            });

            function showTrPrice() {
                $(".tr-price").show();
            }

            function BindStaffFilter() {
                $(".eb-select").bind("focus", function () {
                    EBSelect_ShowBox($(this).parent().find(".eb-select-data"));
                });
                $(window).bind("click", function (e) {
                    console.log(e);
                    if ((!e.target.className.match("eb-select") && e.target.parentElement.className.match("ul-listing-staff")) ||
                        (!e.target.className.match("eb-select") && !e.target.parentElement.className.match("ul-listing-staff"))
                        && !e.target.className.match("mCSB_dragger_bar")) {
                        EBSelect_HideBox();
                    }
                });
                //============================
                // Scroll Staff Filter
                //============================
                $('.eb-select-data').each(function () {
                    if ($(this).height() > 230) {
                        $(this).mCustomScrollbar({
                            theme: "dark-2",
                            scrollInertia: 100
                        });
                    }
                });
            }

            function BindIdToHDF(THIS, id, HDF_DomId, Input_DomId) {
                var text = THIS.innerText.trim(),
                    HDF_Dom = document.getElementById(HDF_DomId),
                    InputText = document.getElementById(Input_DomId);
                HDF_Dom.value = id;
                InputText.value = text;
                InputText.setAttribute("data-value", id);
                ajaxGetStaffsByType(id);
                if (HDF_DomId == "HDF_TypeStaff") {
                    $("#StaffName").val("");
                    $("#HDF_Staff").val("");
                }
            }

            function ajaxGetStaffsByType(type) {
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/Report/TimeKeeping.aspx/Load_Staff_ByType",
                    data: '{type : "' + type + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        var mission = JSON.parse(response.d);
                        if (mission.success) {
                            var staffs = JSON.parse(mission.msg);
                            if (staffs.length > 0) {
                                var tmp = "";
                                $.each(staffs, function (i, v) {
                                    tmp += '<li data-code="' + v.Code + '" data-id="' + v.Id + '"' +
                                        'onclick="BindIdToHDF(this,' + v.Id + ',\'HDF_Staff\',\'StaffName\')">' +
                                        v.Fullname + '</li>';
                                });
                                $("#UlListStaff").empty().append(tmp);
                            }

                        } else {
                            var msg = "Không tìm thấy mã khách hàng trong hệ thống.";
                            showMsgSystem(msg, "warning");
                        }
                    },
                    failure: function (response) { alert(response.d); }
                });
            }

            function EBSelect_ShowBox(Dom) {
                EBSelect_HideBox();
                Dom.show();
            }

            function EBSelect_HideBox() {
                $(".eb-select-data").hide();
            }

            function EBSelect_BindData() {

            }

            function ReplaceZezoValue() {
                $("table.table-listing td").each(function () {
                    if ($(this).text().trim() == "0") {
                        $(this).text("-");
                    }
                });
            }

            function viewDataByDate(This, time) {
                $(".tag-date.active").removeClass("active");
                This.addClass("active");
                $("#TxtDateTimeTo").val("");
                $("#TxtDateTimeFrom").val(time);
                $("#ViewData").click();
            }
        </script>
    </asp:Panel>
</asp:Content>

