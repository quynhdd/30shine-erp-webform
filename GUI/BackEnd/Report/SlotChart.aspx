﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SlotChart.aspx.cs" MasterPageFile="~/TemplateMaster/SiteMaster.Master" Inherits="_30shine.GUI.BackEnd.Report.SlotChart" %>
<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
    <link href="/Assets/js/select2/select2.min.css" rel="stylesheet" />
    <script src="/Assets/js/select2/select2.min.js"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <link href="/Assets/css/highcharts.css" rel="stylesheet" />
    <script>
        $('.select').select2();
    </script>
    <style>
        .p-name-body-report span {
            float: right;
            padding-right: 5px;
        }

        .blue {
            background: #00ff00;
        }

        .warning {
            background: #ffff00;
        }

        .danger {
            background: #db0000;
        }


        #totalReport {
            display: none;
        }

        .wp-content {
            width: 650px;
            max-width: 100%;
            margin: 0 auto;
        }

        .wp-report {
            width: 100%;
            float: left;
        }

        .report {
            width: 50%;
            float: left;
            border: 1px solid #aaa;
            padding-bottom: 20px;
            height: 200px;
        }

        .header-report {
            width: 100%;
            float: left;
        }

        .title-report {
            float: left;
            border: 1px solid #aaa;
            font-family: Roboto Condensed Bold;
            padding: 6px 5px 3px 5px;
            width: 40%;
            /*background-repeat: no-repeat;
                background-size: 23px;
                background-position: 99px 3px;*/
            position: relative;
        }

        .img-mockup {
            position: absolute;
            right: 3px;
            width: 23px;
            top: 3px;
        }

        .title-doanhthu {
            /*background-image: url(/Assets/images/Report/Doanhthu.png);*/
            background-color: #ffff00;
            /*background-position: 123px 3px;*/
        }

        .title-khach {
            /*background-image: url(/Assets/images/Report/Khach.png);*/
            background-color: #5b9bd5;
            /*background-position: 115px 3px;*/
        }

        .title-nangxuat {
            /*background-image: url(/Assets/images/Report/Nangsuat.png);*/
            background-color: #ffd966;
            /*background-position: 115px 3px;*/
        }

        .title-chatluong {
            /*background-image: url(/Assets/images/Report/Chatluong.png);*/
            background-color: #bdd7ee;
            /*background-position: 125px 3px;*/
        }

        .title-nhansu {
            /*background-image: url(/Assets/images/Report/Nhansu.png);*/
            background-color: #c55a11;
        }

        .title-booking {
            /*background-image: url(/Assets/images/Report/Booking.png);*/
            background-color: #50b347;
        }

        .body-report {
            width: 100%;
            float: left;
        }

        .body-lef {
            width: 33%;
            float: left;
            padding: 0px 5px 0px 15px;
            padding-top: 40px;
        }

        .value-hight {
            width: 100%;
            float: left;
            font-family: Roboto Condensed Bold;
            font-size: 26px;
        }

        .des-value {
            width: 100%;
            float: left;
        }

        .p-name-body-report {
            width: 58%;
            float: left;
            text-align: right;
            padding: 0 0 0 5px;
            font-family: Roboto Condensed Regular;
        }

        .body-right {
            width: 67%;
            float: left;
        }

        .row-content {
            width: 100%;
            float: left;
            padding: 0px 5px;
            margin-bottom: 10px;
        }

        .name-body-right {
            width: 60%;
            float: left;
            padding: 0 5px;
        }

        .value-body-right {
            width: 100%;
            float: left;
        }

        .p-value-body-right {
            /*width: 42%;*/
            float: left;
            font-family: Roboto Condensed Regular;
        }

        .select2-container {
            width: 190px !important;
            margin-top: 5px !important;
        }

        .select2-container--default .select2-selection--single {
            height: 32px !important;
            padding-top: 1px !important;
        }

        .fix-fullwidth .p-name-body-report {
            width: 52%;
        }

        .div-more {
            width: 100%;
            float: left;
            text-align: center;
            margin: 30px 0px;
        }

        .st-head.btn-viewdata.viewDetail {
            float: none;
            padding: 8px 20px;
            cursor: pointer;
        }

        #table-detail {
            display: none;
        }

        .wp-table {
            width: 100%;
            margin: 0 auto;
            padding: 0 30px;
        }

        .ctn-table {
            width: 100%;
            float: left;
        }

        @media only screen and (max-width: 768px) {
            .wp-table {
                width: 100%;
                padding: 0;
            }

            .wp-content {
                width: 100%;
            }

            .report {
                height: 165px;
                padding-bottom: 0px;
            }

            .row-content {
                margin-bottom: 5px;
            }

            .body-lef {
                padding-top: 20px;
            }
        }

        @media only screen and (max-width: 600px) {
            .report {
                width: 100%;
                height: inherit;
                padding-bottom: 10px;
            }



            .p-name-body-report {
                width: 55%;
            }

            .p-value-body-right {
                width: 45%;
            }
        }
    </style>
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <%--<uc1:ReportMenu runat="server" id="ReportMenu" />--%>
        <style>
            .table-wp table thead th {
                font-weight: normal;
                font-family: Roboto Condensed Bold;
            }
        </style>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Báo cáo &nbsp;&#187; </li>
                        <li class="be-report-li"><a href="javascript:void(0);"><i class="fa fa-th-large"></i>Biểu đồ slot</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing be-report" onload="abc()">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <!-- Filter -->
                <div class="row">
                    <div class="filter-item" style="margin-left: 0;">
                        <strong class="st-head" style="margin-right: 10px;"><i class="fa fa-clock-o"></i>Thời gian</strong>
                        <div class="datepicker-wp">
                            <asp:TextBox CssClass="txtDateTime st-head" ID="TxtDateTimeFrom" placeholder="Từ ngày"
                                ClientIDMode="Static" runat="server" autocomplete="off"></asp:TextBox>

                            <p class="st-head" style="margin-left: 10px; margin-right: 10px;">
                                <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                            </p>
                            <asp:TextBox CssClass="txtDateTime st-head" ID="TxtDateTimeTo" placeholder="Đến ngày"
                                ClientIDMode="Static" runat="server" autocomplete="off"></asp:TextBox>
                        </div>
                        <div class="filter-item">
                            <asp:DropDownList ID="Salon" CssClass="select form-control" runat="server" ClientIDMode="Static" Style="width: 190px;"></asp:DropDownList>
                        </div>
                        <p class="st-head btn-viewdata fix-style" data-type="2" onclick="DrawChart()">Xem dữ liệu</p>
                    </div>
                </div>
                <!-- End Filter -->

                <div class="row">
                    <strong class="st-head"><i class="fa fa-file-text"></i>TÓM TẮT DỮ LIỆU CHẤM CÔNG THEO BIỂU ĐỒ</strong>

                </div>
                <div class="row">
                    <div class="col-xs-12 ctn-table">
                        <div class="table-wp" id="table-wp-bckqkd">
                            <div id="chart" style="width: 80%; min-height: 600px; margin: 0 auto"></div>
                        </div>
                    </div>
                    <div class="col-xs-6">
                    </div>
                </div>


            </div>
            <%-- END Listing --%>
        </div>
        <!-- Page loading -->
        <div class="page-loading">
            <p>Vui lòng đợi trong giây lát...</p>
        </div>


    </asp:Panel>
    <link href="/Assets/js/select2/select2.min.css" rel="stylesheet" />
    <script src="/Assets/js/select2/select2.min.js"></script>
    <script src="/Assets/js/highcharts.js"></script>
    <script>
        $('.select').select2();
    </script>
    <style>
        .select2-container {
            width: 190px !important;
            margin-top: 5px !important;
        }

        .select2-container--default .select2-selection--single {
            height: 32px !important;
            padding-top: 1px !important;
        }
    </style>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            // Add active menu
            $("#glbAdminSales").addClass("active");
            $("#glbAdminReportSales").addClass("active");
            $("li.be-report-li").addClass("active");

            // Price format
            UP_FormatPrice('.be-report-price');
            //============================
            // Datepicker
            //============================
            $('.txtDateTime').datetimepicker({
                dayOfWeekStart: 1,
                lang: 'vi',
                startDate: '2014/10/10',
                format: 'd/m/Y',
                dateonly: true,
                showHour: false,
                showMinute: false,
                timepicker: false,
                onChangeDateTime: function (dp, $input) { }
            });
        });
        var data;
        var chart;

        // Load the Visualization API and the piechart package.
        google.charts.load('current', { 'packages': ['corechart'] });

        // Set a callback to run when the Google Visualization API is loaded.
        //google.charts.setOnLoadCallback(DrawChart);

        // Callback that creates and populates a data table,
        // instantiates the pie chart, passes in the data and
        // draws it.
        function DrawChart() {
            var kt = true;
            var error = '';
            var timeFrom = $('#TxtDateTimeFrom').val();
            var timeTo = $('#TxtDateTimeTo').val();
            var salonId = $('#Salon').val();
            if (timeFrom === "") {
                kt = false;
                error = "Bạn phải chọn ngày bắt đầu";
                $('#TxtDateTimeFrom').css("border-color", "red");
            }
            else if (salonId === "" || salonId === "0") {
                kt = false;
                error = "Bạn phải chọn Salon";
                $('#Salon').css("border-color", "red");
            }
            if (!kt) {
                ShowMessage('', error, 3);
            }
            else {
                var array = new Array();
                var khung_gio = new Array();
                var tong_slot = new Array();
                var tong_book = new Array();
                var buoc_3 = new Array();
                var slot_tang_ca = new Array();
                var tong_bill = new Array();
                $.ajax({
                    url: '/GUI/BackEnd/Report/SlotChart.aspx/BindData',
                    dataType: 'json',
                    data: `{timeFrom:'${timeFrom}',timeTo:'${timeTo}',salonId:${parseInt(salonId)}}`,
                    contentType: 'application/json;charset:utf-8',
                    type: 'POST',
                    success: function (response) {
                        //console.log(JSON.parse(response.d));
                        jsonData = JSON.parse(response.d);
                        for (i = 0; i < jsonData.length; i++) {
                            khung_gio.push(jsonData[i].Hour.toString());
                            tong_slot.push(jsonData[i].tong_slot);
                            tong_book.push(jsonData[i].tong_book);
                            buoc_3.push(jsonData[i].buoc_3_khong_thanh_cong);
                            slot_tang_ca.push(jsonData[i].slot_tang_ca);
                            tong_bill.push(jsonData[i].tong_bill);
                        }
                        //array.push
                        console.log(khung_gio);
                        // draw chart
                        $('#chart').highcharts({
                            chart: {
                                type: "line"
                            },
                            title: {
                                text: "Tổng slot chấm công"
                            },
                            xAxis: {
                                categories: khung_gio,
                                title: {
                                    text: "Khung giờ"
                                }
                            },
                            yAxis: {
                                title: {
                                    text: "Slot"
                                }
                            },
                            plotOptions: {
                                line: {
                                    dataLabels: {
                                        enabled: true
                                    },
                                    enableMouseTracking: false
                                }
                            },
                            series: [
                                {
                                    name: 'Tổng slot',
                                    data: tong_slot
                                },
                                {
                                    name: 'Tổng book',
                                    data: tong_book
                                },
                                {
                                    name: 'Tổng bước 3 không thành công',
                                    data: buoc_3
                                },
                                {
                                    name: 'Tổng tăng ca',
                                    data: slot_tang_ca
                                },
                                {
                                    name: 'Tổng bill',
                                    data: tong_bill
                                }
                            ]
                        });
                    },
                    error: function (xhr, err) {
                        ShowMessage('', xhr.responseText, 4);
                    }
                });
            }
        }
    </script>
    <script>
        $(document).ready(function () {
            //checkScreen();
            $("tbody.tbody-report").css({ "height": "" + screen.height - 250 + "" });
            $('tbody.tbody-report').scroll(function (e) {
                $('thead.th-report').css("left", -$("tbody").scrollLeft());
                $('thead.th-report th:nth-child(1)').css("left", $("tbody").scrollLeft());
                $('tbody.tbody-report td:nth-child(1)').css({ "left": $("tbody").scrollLeft() }); //,  "display" : "block", "border-bottom" : "none"
            });
        });

        $("#viewDetail").click(function () {
            var Viewdetail = $("#viewDetail");
            if (!Viewdetail.hasClass("active")) {
                console.log("true");
                $("#table-detail").css({ "display": "block" });
                Viewdetail.text("Ẩn chi tiết");
                Viewdetail.addClass("active");
                $('html,body').animate({
                    scrollTop: $("#table-detail").offset().top
                },
                    'slow');
            }
            else {
                console.log("false");
                $("#table-detail").css({ "display": "none" });
                Viewdetail.text("Xem chi tiết");
                Viewdetail.removeClass("active");
                $('html,body').animate({
                    scrollTop: $(".wp.customer-add.customer-listing.be-report").offset().top
                },
                    'slow');
            }
        });

    </script>
</asp:Content>
