﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Report_Sales.aspx.cs" Inherits="_30shine.GUI.BackEnd.Report.Report_Sales" %>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">

    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>

    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <%--<uc1:ReportMenu runat="server" id="ReportMenu" />--%>
        <style>
            .table-wp table thead th { font-weight: normal; font-family: Roboto Condensed Bold; }
        </style>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Báo cáo &nbsp;&#187; </li>
                        <li class="be-report-li"><a href="javascript:void(0);"><i class="fa fa-th-large"></i>Báo cáo - Vận hành</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing be-report" onload="abc()">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <!-- Filter -->
                <div class="row">
                    <div class="filter-item" style="margin-left: 0;">
                        <strong class="st-head" style="margin-right: 10px;"><i class="fa fa-clock-o"></i>Thời gian</strong>
                        <div class="datepicker-wp">
                            <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeFrom" placeholder="Từ ngày"
                                ClientIDMode="Static" runat="server"></asp:TextBox>

                            <strong class="st-head" style="margin-left: 10px; margin-right: 10px;">
                                <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                            </strong>
                            <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeTo" placeholder="Đến ngày"
                                ClientIDMode="Static" runat="server"></asp:TextBox>
                        </div>
                        <% if (Perm_ViewAllDate)
                            { %>
                        <br />
                        <% }
                            else
                            { %>
                        <style>
                            .datepicker-wp { display: none; }
                            .customer-listing .tag-wp { padding-left: 0; padding-top: 15px; }
                        </style>
                        <% } %>
                        <div class="tag-wp">
                            <p class="tag tag-date" onclick="viewDataByDate($(this), '<%=Day1 %>')">Hôm kia <span class="txt-date"><%=Day1 %></span></p>
                            <p class="tag tag-date" onclick="viewDataByDate($(this), '<%=Day2 %>')">Hôm qua<span class="txt-date"><%=Day2 %></span></p>
                            <p class="tag tag-date tag-date-today" onclick="viewDataByDate($(this), '<%=Day3 %>')">Hôm nay<span class="txt-date"><%=Day3 %></span></p>
                        </div>
                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="Salon" runat="server" CssClass="select form-control" ClientIDMode="Static" Style="width: 190px;"></asp:DropDownList>
                    </div>

                    <asp:Panel ID="ViewData" CssClass="st-head btn-viewdata" Text="Xem dữ liệu"
                        ClientIDMode="Static" onclick="excPaging(1)" runat="server">
                        Xem dữ liệu
                    </asp:Panel>
                    <a href="/admin/bao-cao/hoa-don.html" class="st-head btn-viewdata">Reset Filter</a>
                    <div class="export-wp drop-down-list-wp" style="display: none;">
                        <asp:Panel ID="fff" CssClass="st-head btn-viewdata"
                            ClientIDMode="Static" runat="server" onclick="EntryExportExcel('all')">
                            Xuất File Excel
                        </asp:Panel>
                        <%--OnClick="Exc_ExportExcel" --%>
                        <ul class="ul-drop-down-list" style="display: none;">
                            <li onclick="EntryExportExcel('all')">Tất cả dữ liệu</li>
                            <li onclick="EntryExportExcel('current page')">Trang hiện tại</li>
                        </ul>
                    </div>
                    <%--<asp:Panel ID="Btn_ExportExcel" CssClass="st-head btn-viewdata"
                ClientIDMode="Static" onclick="excExcel('Export')" runat="server">Xuất File Excel</asp:Panel>--%>
                    <%--<asp:Panel ID="Btn_ImportExcel" CssClass="st-head btn-viewdata"
                ClientIDMode="Static" onclick="excExcel('Import')" runat="server">Nhập File Excel</asp:Panel>--%>
                </div>
                <!-- End Filter -->
                <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
                <asp:UpdatePanel ID="UPTotal" runat="server" ClientIDMode="Static" style="margin-top: 15px;">
                    <ContentTemplate>
                        <div class="row">
                            <strong class="st-head"><i class="fa fa-file-text"></i>Tổng doanh thu</strong>

                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="table-wp">
                                    <table style="border: 1px solid black; border-collapse: collapse; padding: 5px;">
                                        <thead>
                                            <tr>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; font-family: Roboto Condensed Bold; font-weight: normal; text-align: center;" rowspan="2"></th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; font-family: Roboto Condensed Bold; font-weight: normal; text-align: center;" colspan="8">DOANH SỐ</th>
                                                <th colspan="3" style="border: 1px solid black; border-collapse: collapse; padding: 5px; font-family: Roboto Condensed Bold; font-weight: normal; text-align: center;">DOANH THU</th>
                                                <th colspan="13" style="border: 1px solid black; border-collapse: collapse; padding: 5px; font-family: Roboto Condensed Bold; font-weight: normal; text-align: center;">VẬN HÀNH</th>
                                            </tr>
                                            <tr>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px;">Lượt khách</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px;">Shine Combo</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px;">Kid Combo</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px;">Dưỡng Protein</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px;">Uốn</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px;">Nhuộm</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px;">Tẩy</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px;">Tẩy X2</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px;">Tổng</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px;">Dịch vụ</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px;">Mỹ phẩm</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px;">Stylist</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px;">Skinner</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px;">Chất lượng (%)</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px;">% Khách cũ</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px;">%Khách đặt trước </th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px;">%Khách chờ</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px;">% Lưu ảnh</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px;">Doanh thu /ST/h</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px;">Lượt khách /ST/h</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px;">Lượt khách DV/ngày</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px;">Doanh thu DV/ngày</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <% foreach (var myItem in ToanHeThong)
                                                { %>
                                            <tr>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px;"><%=myItem.ShortName %></th>
                                                <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;"><%=string.Format("{0:#,0.##}", myItem.TongHoaDon) %></td>
                                                <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;"><%=string.Format("{0:#,0.##}", myItem.DsShineCombo) %></td>
                                                <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;"><%=string.Format("{0:#,0.##}", myItem.DsKidCombo) %></td>
                                                <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;"><%=string.Format("{0:#,0.##}", myItem.DsDuongProtein) %></td>
                                                <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;"><%=string.Format("{0:#,0.##}", myItem.DsUon) %></td>
                                                <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;"><%=string.Format("{0:#,0.##}", myItem.DsNhuom) %></td>
                                                <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;"><%=string.Format("{0:#,0.##}", myItem.DsTay) %></td>
                                                <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;"><%=string.Format("{0:#,0.##}", myItem.DsTay2Lan) %></td>
                                                <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;"><%=string.Format("{0:#,0.##}", myItem.TongDoanhThu) %></td>
                                                <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;"><%=string.Format("{0:#,0.##}", myItem.TongDoanhTthuDichVu) %></td>
                                                <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;"><%=string.Format("{0:#,0.##}", myItem.TongDoanhTthuMyPham) %></td>
                                                <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;"><%=myItem.StylistDiLam %></td>
                                                <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;"><%=myItem.SkinnerDiLam %></td>
                                                <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;"><%=myItem.PercentBill_RatHaiLong %> - <%=myItem.PercentBill_HaiLong %> - <%=myItem.PercentBill_ChuaHaiLong %> </td>
                                                <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;"><%=myItem.PercentCusBack %>  </td>
                                                <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;"><%=myItem.PercentCusBook %></td>
                                                <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;"><%=myItem.PercentBill_ChoDuoi15 %> - <%=myItem.PercentBill_ChoTren15 %> - <%=myItem.PercentBill_ChoNULL %></td>
                                                <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;"><%=myItem.Percent_UpImage %></td>
                                                 <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;"><%=string.Format("{0:#,0.##}", myItem.PercentDoanhThuOnWorkHour) %></td>
                                                <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;"><%=myItem.PercentDoanhSOnWorkHour %></td>
                                                <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;"><%=string.Format("{0:#,0.##}", myItem.DoanhSoDVTrongKy) %></td>
                                                <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;"><%=string.Format("{0:#,0.##}", myItem.DoanhThuDVTrongKy) %></td>
                                            </tr>
                                            <% } %>

                                            <asp:Repeater ID="rptData" runat="server">
                                                <ItemTemplate>
                                                    <tr>
                                                        <th style="border: 1px solid black; border-collapse: collapse; padding: 5px;"><%#Eval("ShortName") %></th>
                                                        <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;"><%#string.Format("{0:#,0.##}", Eval("TongHoaDon")) %></td>
                                                        <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;"><%#string.Format("{0:#,0.##}", Eval("DsShineCombo")) %></td>
                                                        <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;"><%#string.Format("{0:#,0.##}", Eval("DsKidCombo")) %></td>
                                                        <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;"><%#string.Format("{0:#,0.##}", Eval("DsDuongProtein")) %></td>
                                                        <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;"><%#string.Format("{0:#,0.##}", Eval("DsUon")) %></td>
                                                        <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;"><%#string.Format("{0:#,0.##}", Eval("DsNhuom")) %></td>
                                                        <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;"><%#string.Format("{0:#,0.##}", Eval("DsTay")) %></td>
                                                        <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;"><%#string.Format("{0:#,0.##}", Eval("DsTay2Lan")) %></td>
                                                        <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;"><%#string.Format("{0:#,0.##}", Eval("TongDoanhThu")) %></td>
                                                        <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;"><%#string.Format("{0:#,0.##}", Eval("TongDoanhTthuDichVu")) %></td>
                                                        <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;"><%#string.Format("{0:#,0.##}", Eval("TongDoanhTthuMyPham")) %></td>
                                                        <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;"><%#Eval("StylistDiLam") %></td>
                                                        <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;"><%#Eval("SkinnerDiLam") %></td>
                                                        <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;"><%#Eval("PercentBill_RatHaiLong") %> - <%#Eval("PercentBill_HaiLong") %> - <%#Eval("PercentBill_ChuaHaiLong") %></td>
                                                        <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;"><%#Eval("PercentCusBack") %></td>
                                                        <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;"><%#Eval("PercentCusBook") %></td>

                                                        <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;"><%#Eval("PercentBill_ChoDuoi15") %> - <%#Eval("PercentBill_ChoTren15") %> - <%#Eval("PercentBill_ChoNULL") %></td>
                                                       <%-- <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;"></td>--%>
                                                        <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;"><%#Eval("Percent_UpImage") %></td>
                                                        <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;"><%#Eval("PercentDoanhThuOnWorkHour") %></td>
                                                        <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;"><%#string.Format("{0:#,0.##}", Eval("PercentDoanhSOnWorkHour")) %></td>
                                                        <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;"><%#string.Format("{0:#,0.##}", Eval("DoanhSoDVTrongKy")) %></td>
                                                        <td style="border: 1px solid black; border-collapse: collapse; padding: 5px;"><%#string.Format("{0:#,0.##}", Eval("DoanhThuDVTrongKy")) %></td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-xs-6">
                            </div>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>

                <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="IsPaging" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_ExcelType" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_EXCELPage" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_EXCELSegment" />
            </div>
            <%-- END Listing --%>
        </div>
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 190px !important;
                margin-top: 5px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }
        </style>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                // Add active menu
                $("#glbAdminSales").addClass("active");
                $("#glbAdminReportSales").addClass("active");
                $("li.be-report-li").addClass("active");

                // Price format
                UP_FormatPrice('.be-report-price');

                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) {
                        //$input.val($input.val().split(' ')[0]);
                    }
                });

                // View data today
                //$(".tag-date-today").click();
            });

            function SubTableFixHeight() {
                $("tbody table.sub-table").each(function () {
                    $(this).height($(this).parent().height() + 1);
                });
            }

            function viewDataByDate(This, time) {
                $(".tag-date.active").removeClass("active");
                This.addClass("active");
                $("#TxtDateTimeTo").val("");
                $("#TxtDateTimeFrom").val(time);
                $("#ViewData").click();
            }



            var sDate = "26/5/2016";
            var date = Date.parse(sDate, "yyyy-MM-dd");
            console.log(date);

        </script>

    </asp:Panel>
</asp:Content>
