﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Globalization;
using System.Web.Script.Serialization;
using ExportToExcel;
using Excel;
using LinqKit;
using System.Net.Mail;
using System.Net;
using System.Data.SqlClient;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.Report
{
    public partial class Report_Sales : System.Web.UI.Page
    {
        private string PageID = "BC_VH";
        CultureInfo culture = new CultureInfo("vi-VN");
        protected List<Store_Report_Sale_V3_Result> ToanHeThong = new List<Store_Report_Sale_V3_Result>();
        protected bool Perm_ViewAllData = false;
        protected bool Perm_ViewAllDate = false;
        private bool Perm_Access = false;
        protected bool IsAccountant = false;
        private string sql = "";

        protected string Day1 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-2));
        protected string Day2 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-1));
        protected string Day3 = string.Format("{0:dd/MM/yyyy}", DateTime.Now);

        private Solution_30shineEntities db;
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                ////Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //Perm_ViewAllDate = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllDate", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_ViewAllDate = permissionModel.CheckPermisionByAction("Perm_ViewAllDate", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        public Report_Sales()
        {
            db = new Solution_30shineEntities();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (!IsPostBack)
            {
                var staffId = Convert.ToInt32(Session["User_Id"].ToString());
                Library.Function.bindSalon_NotHoiQuan(new List<DropDownList> { Salon }, Perm_ViewAllData);
            }
            else
            {
                RemoveLoading();
            }
        }
    

        protected void BindData()
        {
            if (TxtDateTimeFrom.Text != "")
            {
                DateTime _FromDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture).AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
                DateTime _ToDate;
                if (TxtDateTimeTo.Text != "")
                {
                    _ToDate = Convert.ToDateTime(TxtDateTimeTo.Text, culture).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
                }
                else
                {
                    _ToDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
                }
                int SalonId = Convert.ToInt32(Salon.SelectedValue);
                sql = @"-- Báo cáo vận hành 
                        declare
                        @SalonId int,
                        @FromDate datetime,
                        @ToDate datetime
                        set @SalonId = " + SalonId + @"
                        set @FromDate = '" + _FromDate + @"'
                        set @ToDate = '" + _ToDate + @"'

                        begin
                        -- tong Bill đánh giá
                        with TempTotalBillservice as
                        (
                        select  a.SalonId , COUNT(a.Id) as TotalBillService  from BillService as a 
                        left join Tbl_Salon as salon on a.SalonId = salon.Id
                        where
	                        a.CreatedDate > @FromDate and a.CreatedDate < @ToDate 
	                        and ((a.SalonId = @SalonId) or (@SalonId = 0)) 
	                        and a.IsDelete =0 and a.Pending = 0 and a.ServiceIds !='' and Mark is not null 
	                        and salon.IsDelete = 0 and IsSalonHoiQuan =0
	                        group by a.SalonId ,salon.Id
                        )   ,
                        --select * from TempTotalBillservice
                        --end  
                        --Tổng Bill đánh giá Rất hài lòng
                        TempTotalBillRatHaiLong as
                        (select s.Id as SalonId ,COUNT(a.Id) as TotalBill_RatHaiLong from Tbl_Salon as s
                        left join  BillService as a on a.SalonId = s.Id
                        where
	                        a.CreatedDate > @FromDate and a.CreatedDate < @ToDate 
	                        and ((a.SalonId = @SalonId) or (@SalonId = 0)) 
	                        and a.IsDelete =0 and a.Pending = 0 and a.ServiceIds !='' and Mark =3
	                        and s.IsDelete = 0 and s.IsSalonHoiQuan = 0
	                        group by a.SalonId , s.Id ),  
                        --Tổng bill đánh giá Hài lòng
                        TempTotalBill_HaiLong as
                        (select a.SalonId ,COUNT(a.Id) as TotalBill_HaiLong from BillService as a 
                        left join Tbl_Salon as salon on a.SalonId = salon.Id
                        where
	                        a.CreatedDate > @FromDate and a.CreatedDate < @ToDate 
	                        and ((a.SalonId = @SalonId) or (@SalonId = 0)) 
	                        and a.IsDelete =0 and a.Pending = 0 and a.ServiceIds !='' and Mark =2	
	                        and salon.IsDelete =0 and salon.IsSalonHoiQuan = 0
	                        group by a.SalonId,salon.Id),
	                        --select * from TempTotalBill_HaiLong
	                        --end
                        --Tổng Bill đánh giá chua hài lòng
                        TempTotalBill_ChuaHaiLong as
                        (select a.SalonId ,COUNT(a.Id) as TotalBill_ChuaHaiLong from BillService as a  
                        left join Tbl_Salon as salon on a.SalonId = salon.Id
                        where
	                        a.CreatedDate > @FromDate and a.CreatedDate < @ToDate 
	                        and ((a.SalonId = @SalonId) or (@SalonId = 0)) 
	                        and a.IsDelete =0 and a.Pending = 0 and a.ServiceIds !='' and Mark =1
	                        and salon.IsDelete = 0 and salon.IsSalonHoiQuan = 0
	                        group by a.SalonId , salon.Id) ,
                        -- Phần trănm đánh giá rất hài lòng
                        PercentBill_RatHaiLong as
                        (	select Round(cast(b.TotalBill_RatHaiLong as float)* cast(100 as float)/ cast((case when a.TotalBillService = 0 then 1 else a.TotalBillService end  ) as float), 2) as PercentBill_RatHaiLong , a.SalonId
	                         from TempTotalBillservice as a
	                        inner join TempTotalBillRatHaiLong as b on a.SalonId = b.SalonId
                        ) ,
                        --select * from PercentBill_RatHaiLong
                        --end
                        -- Phần trănm đánh giá hài lòng
                        PercentBill_HaiLong as
                        (
	                        select Round(cast(b.TotalBill_HaiLong as float)* cast(100 as float)/ cast((case when a.TotalBillService = 0 then 1 else a.TotalBillService end  ) as float), 2) as PercentBill_HaiLong , a.SalonId
	                         from TempTotalBillservice as a
	                        inner join TempTotalBill_HaiLong as b on a.SalonId = b.SalonId
                        ) ,
                        --select * from PercentBill_HaiLong
                        --end
                        -- Phần trănm đánh giá chưa hài lòng
                        PercentBill_ChuaHaiLong as
                        (
	                        select Round(cast(b.TotalBill_ChuaHaiLong as float)* cast(100 as float)/ cast((case when a.TotalBillService = 0 then 1 else a.TotalBillService end  ) as float), 2) as PercentBill_ChuaHaiLong , a.SalonId
	                         from TempTotalBillservice as a
	                        inner join TempTotalBill_ChuaHaiLong as b on a.SalonId = b.SalonId
                        ),
                        --select * from PercentBill_ChuaHaiLong
                        --end
                        ---Doanh số Tổng hóa đơn Dịch Vụ
                        TempTongHoaDon as (
                        select COUNT(a.Id) as TongHoaDon,  a.SalonId from BillService  as a
                        left join Tbl_Salon as salon on a.SalonId = salon.Id
	                          where 
	                        a.CreatedDate > @FromDate and a.CreatedDate < @ToDate 
	                        and ((a.SalonId = @SalonId) or (@SalonId = 0))
	                        and a.ServiceIds !='' and a.IsDelete =0 and a.Pending = 0 
	                        and salon.IsDelete = 0 and salon.IsSalonHoiQuan = 0
	                        group by a.SalonId , salon.Id)   ,
	                        -- tổng bill service
                        TongBill_Service as
                        (	select a.Id,a.CustomerId,a.SalonId from BillService as a 
                        left join Tbl_Salon as salon on a.SalonId = salon.Id
	                        where  
	                        a.CreatedDate > @FromDate and a.CreatedDate < @ToDate 
	                        and ((a.SalonId = @SalonId) or (@SalonId = 0)) 
	                        and a.IsDelete =0 and a.Pending = 0 and a.ServiceIds !='' and a.ServiceIds is not null
	                        and salon.IsDelete = 0 and salon.IsSalonHoiQuan = 0
	                         )	, 
	                         --select * from TongBill_Service
	                         --end
                         --- Khách đã sử dụng dịch vụ trước kỳ
	                      KhachSuDungDVTruocKy as
                         (select a.CustomerId  from BillService as a 
                        left join Tbl_Salon as salon on a.SalonId = salon.Id
	                        where  
	                        a.CreatedDate < @FromDate 
	                        and ((a.SalonId = @SalonId) or (@SalonId = 0)) 
	                        and a.IsDelete =0 and a.Pending = 0 and a.ServiceIds !='' and a.ServiceIds is not null
	                        and salon.IsDelete = 0 and salon.IsSalonHoiQuan = 0
	                        group by  a.CustomerId 
	                         ),--,
	                         
	                         --Tinh so lan khach su dung dich vu tai salon trong ky da group khach hang  va salon
	                           SoLanKhachSuDungDV as
                         (select a.CustomerId,a.SalonId, (COUNT(CustomerId)-1) as SoLan from BillService as a 
							left join Tbl_Salon as salon on a.SalonId = salon.Id
	                        where  
	                        a.CreatedDate > @FromDate and a.CreatedDate < @ToDate 
	                        and ((a.SalonId = @SalonId) or (@SalonId = 0)) 
	                        and a.IsDelete =0 and a.Pending = 0 and a.ServiceIds !='' and a.ServiceIds is not null
	                        and salon.IsDelete = 0 and salon.IsSalonHoiQuan = 0
	                        group by a.SalonId, a.CustomerId
	                         ),
	                        --select * from SoLanKhachSuDungDV order by CustomerId asc
	                        --end
	                         -- Số lần sử dụng của khách theo salon
	                         SoLanKhachSuDungTheoSaLon as
	                         (
								select  SUM(SoLan) as SoLan ,SalonId  from SoLanKhachSuDungDV 
								group by SalonId
	                         ),
	                         --select * from SoLanKhachSuDungTheoSaLon
	                         --end
	                        
	                         --select * from temp order by CustomerId
	                         --end
	                         ---Dem khach hang quay lai
	                         CountKhachHangQuayLai as 
	                         (
							  select a.SalonId,Count(a.CustomerId) as CoutCus from SoLanKhachSuDungDV as a
								inner join KhachSuDungDVTruocKy as b on a.CustomerId = b.CustomerId
								group by SalonId
	                         ),
	                         --select * from tempb
	                         --end
	                         ---Khach quay lai = (SoLanKhachSuDungTheoSaLon + CountKhachHangQuayLai) * 100 / TempTongHoaDon
	                         tbl_PercentCusBack as 
	                         (
								select salon.Id as SalonId ,
								Round(cast((a.SoLan + b.CoutCus) as float) * cast(100 as float)/ cast((case when c.TongHoaDon = 0 then 1 else c.TongHoaDon end  ) as float), 2) as PercentCusBack 
								from Tbl_Salon as salon
								left join SoLanKhachSuDungTheoSaLon as a on salon.Id = a.SalonId
								left join CountKhachHangQuayLai as b on salon.Id = b.SalonId 
								left join TempTongHoaDon as  c on salon.Id = c.SalonId
								where  salon.IsDelete = 0 and IsSalonHoiQuan =0
	                         ),

                        -- tong khach Book Đặt trước
                        TempCusBook as (
                        select COUNT(a.Id) as TongKhachBook,  a.SalonId from BillService  as a
                        left join Tbl_Salon as salon on a.SalonId = salon.Id
                        inner join Booking as book on a.BookingId = book.Id
	                          where 
	                        a.CreatedDate > @FromDate and a.CreatedDate < @ToDate 
	                        and ((a.SalonId = @SalonId) or (@SalonId = 0))
		                        and salon.IsDelete = 0 and IsSalonHoiQuan =0
	                        and a.ServiceIds !='' and a.IsDelete =0 and a.Pending = 0 
	                        and a.BookingId > 0  and book.IsBookOnline = 1
	                        group by a.SalonId ,salon.Id),	
	                        --select * from TempCusBook end
                        --- tong phần trăn Khách đặt lịch trước
                        TempPercentBook as 
                        (
	                        select a.SalonId  , Round(cast(b.TongKhachBook as float)* cast(100 as float)/ cast((case when a.TongHoaDon = 0 then 1 else a.TongHoaDon end  ) as float), 2) as PercentCusBook 
	                        from TempTongHoaDon as a
	                        inner join TempCusBook as b on a.SalonId = b.SalonId
                        ),
                        --select * from TempPercentBook end
                        --select * from tbl_PercentCusBack end 
                        -- Tong bill Up Images
                        TempBillUpImages as (
                        select COUNT(a.Id) as TongBillUpImages,  a.SalonId from BillService  as a
                        left join Tbl_Salon as salon on a.SalonId = salon.Id
	                          where 
	                        a.CreatedDate > @FromDate and a.CreatedDate < @ToDate 
	                        and ((a.SalonId = @SalonId) or (@SalonId = 0))
		                        and salon.IsDelete = 0 and IsSalonHoiQuan =0
	                        and a.ServiceIds !='' and a.IsDelete =0 and a.Pending = 0 
	                        and a.Images  is not null
	                        group by a.SalonId ,salon.Id),
	                        --select * from TempBillUpImages
	                        --end
                        --- Percent Bill Upload Images
                        TempPercentBillUpImages as
                        (
	                        select a.SalonId  , Round(cast(b.TongBillUpImages as float)* cast(100 as float)/ cast((case when a.TongHoaDon = 0 then 1 else a.TongHoaDon end  ) as float), 2) as Percent_UpImage
	                        from TempTongHoaDon as a
	                        inner join TempBillUpImages as b on a.SalonId = b.SalonId
                        ),

                        --select * from TempPercentBillUpImages
                        --	end
                        ---Doanh số ShineCombo
                        DsShineCombo as
                        (select SUM(b.Quantity) DsShineCombo ,a.SalonId from BillService  as a 
                        inner join FlowService as b on a.Id = b.BillId 
                        left join Tbl_Salon as salon on a.SalonId = salon.Id
	                        where   
	                        a.CreatedDate > @FromDate and a.CreatedDate < @ToDate 
	                        and ((a.SalonId = @SalonId) or (@SalonId = 0)) 
	                        and a.IsDelete =0 and a.Pending = 0
	                        and b.ServiceId = 53 and  b.IsDelete =0
	                        and salon.IsDelete = 0 and salon.IsSalonHoiQuan = 0
	                        group by a.SalonId , salon.Id) ,
                        ---Doanh số Kid Combo
                        DsKidCombo as
                        (select SUM(b.Quantity) as DsKidCombo ,a.SalonId from BillService  as a 
                        inner join FlowService as b on a.Id = b.BillId 
                        left join Tbl_Salon as salon on a.SalonId = salon.Id
	                        where   
	                        a.CreatedDate > @FromDate and a.CreatedDate < @ToDate 
	                        and ((a.SalonId = @SalonId) or (@SalonId = 0)) 
	                        and a.IsDelete =0 and a.Pending = 0
	                        and b.ServiceId = 74 and  b.IsDelete =0
	                        and salon.IsDelete = 0 and salon.IsSalonHoiQuan = 0
                        group by a.SalonId , salon.Id)    ,
								
                        ---  Doanh số Dưỡng Protein	
                        DsProtein as(
                        select SUM(b.Quantity) as DsDuongProtein ,a.SalonId from BillService  as a 
                        inner join FlowService as b on a.Id = b.BillId 
                        left join Tbl_Salon as salon on a.SalonId = salon.Id
	                        where   
	                        a.CreatedDate > @FromDate and a.CreatedDate < @ToDate 
	                        and ((a.SalonId = @SalonId) or (@SalonId = 0)) 
	                        and a.IsDelete =0 and a.Pending = 0
	                        and b.ServiceId = 69 and  b.IsDelete =0
	                        and salon.IsDelete = 0 and salon.IsSalonHoiQuan = 0
	                        group by a.SalonId , salon.Id)  ,
                        ---  Doanh số Uốn
                        DsUon as (
                        select SUM(b.Quantity) DsUon ,a.SalonId from BillService  as a 
                        inner join FlowService as b on a.Id = b.BillId 
                        left join Tbl_Salon as salon on a.SalonId = salon.Id
	                        where   
	                        a.CreatedDate > @FromDate and a.CreatedDate < @ToDate 
	                        and ((a.SalonId = @SalonId) or (@SalonId = 0)) 
	                        and a.IsDelete =0 and a.Pending = 0
	                        and b.ServiceId = 16 and  b.IsDelete =0
	                        and salon.IsDelete = 0 and salon.IsSalonHoiQuan = 0
	                        group by a.SalonId , salon.Id) ,
                        ---  Doanh số Nhuộm
                        DsNhuom as 
                        (select SUM(b.Quantity) DsNhuom ,a.SalonId from BillService  as a 
                        inner join FlowService as b on a.Id = b.BillId 
                        left join Tbl_Salon as salon on a.SalonId = salon.Id
	                        where   
	                        a.CreatedDate > @FromDate and a.CreatedDate < @ToDate 
	                        and ((a.SalonId = @SalonId) or (@SalonId = 0)) 
	                        and a.IsDelete =0 and a.Pending = 0
	                        and b.ServiceId = 14 and  b.IsDelete =0
	                        and salon.IsDelete = 0 and salon.IsSalonHoiQuan = 0
                        group by a.SalonId , salon.Id) ,
                        ---  Doanh số Tẩy
                        DsTay as
                        (select SUM(b.Quantity) DsTay ,a.SalonId from BillService  as a 
                        inner join FlowService as b on a.Id = b.BillId 
                        left join Tbl_Salon as salon on a.SalonId = salon.Id
	                        where   
	                        a.CreatedDate > @FromDate and a.CreatedDate < @ToDate 
	                        and ((a.SalonId = @SalonId) or (@SalonId = 0)) 
	                        and a.IsDelete =0 and a.Pending = 0
	                        and b.ServiceId = 17 and  b.IsDelete =0
	                        and salon.IsDelete = 0 and salon.IsSalonHoiQuan = 0
                        group by a.SalonId , salon.Id) ,
                        ---  Doanh số Tẩy 2 lần
                        DsTay2Lan  as(
                        select COUNT(b.Quantity) DsTay2Lan ,a.SalonId from BillService  as a 
                        inner join FlowService as b on a.Id = b.BillId 
                        left join Tbl_Salon as salon on a.SalonId = salon.Id
	                        where   
	                        a.CreatedDate > @FromDate and a.CreatedDate < @ToDate 
	                        and ((a.SalonId = @SalonId) or (@SalonId = 0)) 
	                        and a.IsDelete =0 and a.Pending = 0
	                        and b.ServiceId = 24 and  b.IsDelete =0
	                        and salon.IsDelete = 0 and salon.IsSalonHoiQuan = 0
	                        group by a.SalonId , salon.Id) ,
                        --- Tổng  Doanh thu 
                        TongDoanhThu as (
                        select Round( cast(SUM(a.TotalMoney) as float) /1000,0)  as TongDoanhThu ,a.SalonId
						from BillService  as a
                        left join Tbl_Salon as salon on a.SalonId = salon.Id
	                         where 
	                        a.CreatedDate > @FromDate and a.CreatedDate < @ToDate 
	                        and ((a.SalonId = @SalonId) or (@SalonId = 0)) 
	                        and  a.IsDelete =0 and a.Pending = 0 
	                        and salon.IsDelete = 0 and salon.IsSalonHoiQuan = 0
	                        group by a.SalonId , salon.Id) ,
                        --- Tổng  Doanh thu  Dịch vụ
                        TongDoanhThuDichVu as (
                        select ( Round( cast((SUM(b.Price * b.Quantity *(100 - b.VoucherPercent)/100)) as float) /1000 ,0)) as TongDoanhTthuDichVu,  a.SalonId from BillService  as a
                        inner join FlowService as b on a.Id = b.BillId
                        left join Tbl_Salon as salon on a.SalonId = salon.Id
	                         where 
	                        a.CreatedDate > @FromDate and a.CreatedDate < @ToDate 
	                        and ((a.SalonId = @SalonId) or (@SalonId = 0)) 
	                        and  a.IsDelete =0 and a.Pending = 0 and a.ServiceIds !=''
	                        and b.IsDelete = 0
	                        and salon.IsDelete = 0 and salon.IsSalonHoiQuan = 0
                        group by a.SalonId , salon.Id) ,
	
                        --- Tổng  Doanh thu  Mỹ Phẩm
                        TongDoanhThuMyPham as (
                        select ( Round (cast( (SUM(b.Price * b.Quantity * (100 - b.VoucherPercent) /100) )as float) / 1000 , 0)) as TongDoanhTthuMyPham,  a.SalonId from BillService  as a
                        inner join FlowProduct as b on a.Id = b.BillId
                        left join Tbl_Salon as salon on a.SalonId = salon.Id
	                         where 
	                        a.CreatedDate > @FromDate and a.CreatedDate < @ToDate 
	                        and ((a.SalonId = @SalonId) or (@SalonId = 0)) 
	                        and  a.IsDelete =0 and a.Pending = 0 
	                        and b.IsDelete = 0
	                        and salon.IsDelete = 0 and salon.IsSalonHoiQuan = 0
	                        group by a.SalonId , salon.Id) ,
                        --- Vận hành Stylist đi làm	
                        StylistDiLam as(
                        select Round(cast(COUNT(a.Id) as float)/ (DATEDIFF(D,@FromDate,@ToDate)), 3) as StylistDiLam , a.SalonId
                         from FlowTimeKeeping as a 
                        inner join Staff as b on a.StaffId =b.Id
                        left join Tbl_Salon as salon on a.SalonId = salon.Id
	                        where
	                        a.WorkDate >= @FromDate and a.WorkDate < @ToDate 
	                        and ((a.SalonId = @SalonId) or (@SalonId = 0)) 
	                        and a.IsDelete = 0 and a.IsEnroll = 1 
	                        and b.Type = 1 
	                        and salon.IsDelete = 0 and salon.IsSalonHoiQuan = 0
	                        group by a.SalonId , salon.Id) ,
	                        --- Doanh số dịch vụ theo Stylist = Tong Doanh so / So Stylist di lam / Thoi gian trong ky
                        TempPercentDsStylistTrenStylist as 
                        (
	                        select a.SalonId , 
	                        --Round(cast(COUNT(a.TongHoaDon) as float)/ ((DATEDIFF(D,@FromDate,@ToDate) *  ), 3)
	
	                        Round(cast(a.TongHoaDon as float)/ cast((case when b.StylistDiLam = 0 then 1 else b.StylistDiLam end  ) as float) / (DATEDIFF(D,@FromDate,@ToDate)) , 2)  as PercentDstylistTrenStylist
	                        from  TempTongHoaDon as a
	                        inner join StylistDiLam as b on a.SalonId = b.SalonId
                        ),
                        --select * from TempPercentDsStylistTrenStylist end  
                        --- Doanh Thu dịch vụ theo Stylist = Tong Doanh Thu / So Stylist di lam / Thoi gian trong ky
                        TempPercentDThuStylistTrenStylist as 
                        (
	                        select a.SalonId ,
	                       ( Round(cast(a.TongDoanhTthuDichVu as float)/ cast((case when b.StylistDiLam = 0 then 1 else b.StylistDiLam end  ) as float) / (DATEDIFF(D,@FromDate,@ToDate)) , 0) )  as PercentDThuStylistTrenStylist
	                        from  TongDoanhThuDichVu as a
	                        inner join StylistDiLam as b on a.SalonId = b.SalonId
                        ),
                        --select * from TempPercentDThuStylistTrenStylist end
                        --Doanh số DV TB/ngày trong kỳ = Tong doanh so / Thoi gian trong ky
                        TempDoanhSoTrongKy as
                        (
	                        select a.SalonId , Round(cast(a.TongHoaDon as float)/ (DATEDIFF(D,@FromDate,@ToDate)), 3) as DoanhSoDVTrongKy
	                         from TempTongHoaDon as a ),
                        --select * from TempDoanhSoTrongKy end
                        --Doanh số DV TB/ngày trong kỳ = Tong doanh so / Thoi gian trong ky
                        TempDoanhThuTrongKy as
                        (
	                        select a.SalonId , (Round(cast(a.TongDoanhTthuDichVu as float)/ (DATEDIFF(D,@FromDate,@ToDate)), 0) ) as DoanhThuDVTrongKy
	                         from TongDoanhThuDichVu as a 
                        ),
                        --select * from TempDoanhThuTrongKy end
                        --- Vận hành Skinner  đi làm	
                        SkinnerDiLam as (
                        select Round(cast(COUNT(a.Id) as float)/ (DATEDIFF(D,@FromDate,@ToDate)), 3) as SkinnerDiLam , a.SalonId
                         from FlowTimeKeeping as a 
                        inner join Staff as b on a.StaffId =b.Id
                        left join Tbl_Salon as salon on a.SalonId = salon.Id
	                        where
	                        a.WorkDate >= @FromDate and a.WorkDate < @ToDate 
	                        and ((a.SalonId = @SalonId) or (@SalonId = 0)) 
	                        and a.IsDelete = 0 and a.IsEnroll = 1 
	                        and b.Type = 2 
	                        and salon.IsDelete = 0 and salon.IsSalonHoiQuan = 0
	                        group by a.SalonId , salon.Id) ,
	                         --- Tinh thoi gian ho
	                      TempThoiGianCho as 
	                      (
							SELECT *							
							from (
								select bill.Id, salon.Id as SalonId ,
										case 
											when (bill.BookingId is not null and (b.IsBookAtSalon is null or b.IsBookAtSalon != 1) and bill.CreatedDate > Convert(datetime, CONVERT(nvarchar(10), b.DatedBook, 110) + ' ' + CONVERT(nvarchar(10), c.HourFrame))) -- check in sau h book
											then DATEDIFF(minute, bill.CreatedDate, bill.InProcedureTime) 
											when (bill.BookingId is not null and (b.IsBookAtSalon is null or b.IsBookAtSalon != 1) and Convert(datetime, CONVERT(nvarchar(10), b.DatedBook, 110) + ' ' + CONVERT(nvarchar(10), c.HourFrame)) > bill.CreatedDate) -- check in trc h book
											then DATEDIFF(minute, Convert(datetime, CONVERT(nvarchar(10), b.DatedBook, 110) + ' ' + CONVERT(nvarchar(10), c.HourFrame)), bill.InProcedureTime)
											when (bill.BookingId is null or b.IsBookAtSalon = 1 or Convert(datetime, CONVERT(nvarchar(10), b.DatedBook, 110) + ' ' + CONVERT(nvarchar(10), c.HourFrame)) is null) -- khách k book
											then DATEDIFF(MINUTE, bill.CreatedDate, bill.InProcedureTime)
											else NULL
										end as WaitTimeInProcedure
									from Tbl_Salon as salon 
									left join BillService as bill on bill.SalonId = salon.Id
									left join booking b on bill.BookingId = b.Id
									left join BookHour c on b.HourId = c.Id
									where bill.IsDelete != 1 and bill.Pending != 1
									and (bill.ServiceIds is not null or bill.ServiceIds != '')
									and salon.IsDelete = 0 and salon.IsSalonHoiQuan = 0
									and bill.CompleteBillTime between @FromDate and @ToDate
		and ((bill.SalonId = @SalonId) or (@SalonId =0))  ) as tbl ), 
							--select * from TempThoiGianCho end		
							--- Tong thoi gian cho Duoi 15 Phut
							TempTotalTimeDuoi_15P as 
							(
								select COUNT(a.Id) as tong , a.SalonId  from TempThoiGianCho as a where a.WaitTimeInProcedure <= 15
								group by a.SalonId
							),
							--- Tong thoi gian cho Tren 15 Phut
							TempTotalTimeTren_15P as 
							(
								select COUNT(a.Id) as tong , a.SalonId  from TempThoiGianCho as a where a.WaitTimeInProcedure > 15
								group by a.SalonId
							),
							--- Thoi gian cho = null
							TempTotalTimeNULL as (
							select COUNT(a.Id) as tong , a.SalonId  from TempThoiGianCho as a where a.WaitTimeInProcedure is null
								group by a.SalonId
							),
							--- PercentThoiGianCho  < 15
							PercentThoiGianChoDuoi15P as 
							(
								select a.SalonId,
								 Round(cast(b.tong as float)* cast(100 as float)/ cast((case when a.TongHoaDon = 0 then 1 else a.TongHoaDon end  ) as float), 2) as PercentBill_ChoDuoi15
								from TempTotalTimeDuoi_15P as b
								left join TempTongHoaDon as a on b.SalonId = a.SalonId 
							),
							--- PercentThoiGianCho  > 15
							PercentThoiGianChoTren15P as 
							(
								select a.SalonId,
								 Round(cast(b.tong as float)* cast(100 as float)/ cast((case when a.TongHoaDon = 0 then 1 else a.TongHoaDon end  ) as float), 2) as PercentBill_ChoTren15
								from TempTotalTimeTren_15P as b
								left join TempTongHoaDon as a on b.SalonId = a.SalonId 
							),
							--- PercentThoiGianCho NULL
							PercentThoiGianChoNULL as 
							(
								select a.SalonId,
								 Round(cast(b.tong as float)* cast(100 as float)/ cast((case when a.TongHoaDon = 0 then 1 else a.TongHoaDon end  ) as float), 2) as PercentBill_ChoNULL
								from TempTotalTimeNULL as b
								left join TempTongHoaDon as a on b.SalonId = a.SalonId 
							),
	                        
                        ------- GHEP CAC BANG TRUY VAN THANH BAO CAO
                        BaoCaoVanHanh1 as 
                        (
	                        select s.Id as SalonId , s.ShortName, s.[Order],
	                        Coalesce(c1.TongHoaDon, 0) as TongHoaDon,
	                        Coalesce(c2.DsShineCombo,0) as DsShineCombo,
	                        Coalesce(c3.DsKidCombo , 0) as DsKidCombo,
	                        Coalesce(c23.DsDuongProtein, 0) as DsDuongProtein,
	                        Coalesce(c4.DsUon,0) as DsUon,
	                        Coalesce(c5.DsNhuom,0) as DsNhuom,
	                        Coalesce(c6.DsTay,0) as DsTay,
	                        Coalesce(c7.DsTay2Lan,0) as DsTay2Lan
	
	                         from Tbl_Salon as s
	                         left join TempTongHoaDon   as c1 on s.Id = c1.SalonId --Tong Hoa don DICH VU
	                         left join DsShineCombo as c2 on  s.Id = c2.SalonId
	                         left join DsKidCombo as c3 on s.Id = c3.SalonId
	                         LEFT join DsProtein as c23 on s.Id = c23.SalonId
	                         left join DsUon as c4 on s.Id = c4.SalonId
	                         left join DsNhuom as c5 on s.Id = c5.SalonId
	                         left join DsTay as c6 on s.Id = c6.SalonId
	                         left join DsTay2Lan as c7 on s.Id = c7.SalonId
	                         where s.IsDelete = 0 and s.Publish = 1 and s.IsSalonHoiQuan = 0  and ((s.Id = @SalonId) or (@SalonId = 0)) 
                        ),
                        BaoCaoVanHanh2 as (
                        select s.Id as Id1 , 
	
	                        Coalesce(c8.TongDoanhThu,0) as TongDoanhThu,
	                        Coalesce(c9.TongDoanhTthuDichVu,0) as TongDoanhTthuDichVu,
	                        Coalesce(c10.TongDoanhTthuMyPham,0) as TongDoanhTthuMyPham,
	                        Coalesce(c11.StylistDiLam,0) as StylistDiLam,
	                        Coalesce(c12.SkinnerDiLam,0) as SkinnerDiLam,
	                        Coalesce(c13.PercentBill_RatHaiLong,0) as PercentBill_RatHaiLong,
	                        Coalesce(c14.PercentBill_HaiLong,0) as PercentBill_HaiLong
	
	                         from Tbl_Salon as s
	
	                         left join TongDoanhThu as c8 on s.Id = c8.SalonId
	                         left join TongDoanhThuDichVu as c9 on s.Id = c9.SalonId
	                         left join TongDoanhThuMyPham as c10 on s.Id = c10.SalonId
	                         left join StylistDiLam as c11 on s.Id = c11.SalonId
	                         left join SkinnerDiLam as c12 on s.Id = c12.SalonId
	                          left join PercentBill_RatHaiLong as c13 on s.Id = c13.SalonId
	                         left join PercentBill_HaiLong as c14 on s.Id = c14.SalonId
	                         where s.IsDelete = 0 and s.Publish = 1 and s.IsSalonHoiQuan = 0  and ((s.Id = @SalonId) or (@SalonId = 0)) 
                        ),
                        BaoCaoVanHanh3 as 
                        (
	                        select s.Id  , 
	
	                        Coalesce(c15.PercentBill_ChuaHaiLong,0) as PercentBill_ChuaHaiLong,
	                        Coalesce(c16.PercentCusBack,0) as PercentCusBack,
	                        Coalesce(c17.PercentCusBook,0) as PercentCusBook,
	                        Coalesce(c18.Percent_UpImage,0) as Percent_UpImage,
	                        Coalesce(c19.PercentDstylistTrenStylist,0) as PercentDstylistTrenStylist,
	                        Coalesce(c20.PercentDThuStylistTrenStylist,0) as PercentDThuStylistTrenStylist,
	                        Coalesce(c21.DoanhSoDVTrongKy,0) as DoanhSoDVTrongKy,
	                        Coalesce(c22.DoanhThuDVTrongKy,0) as DoanhThuDVTrongKy
	
	                         from Tbl_Salon as s
	
	                         left join PercentBill_ChuaHaiLong as c15 on s.Id = c15.SalonId
	                         left join tbl_PercentCusBack as c16 on s.Id = c16.SalonId
	                         left join TempPercentBook as c17 on s.Id = c17.SalonId
	                         left join TempPercentBillUpImages as c18 on s.Id = c18.SalonId
	                         left join TempPercentDsStylistTrenStylist as c19 on s.Id = c19.SalonId 
	                         left join TempPercentDThuStylistTrenStylist as c20 on s.Id = c20.SalonId
	                         left join TempDoanhSoTrongKy as c21 on s.Id = c21.SalonId
	                         left join TempDoanhThuTrongKy as c22 on s.Id = c22.SalonId
	                        where s.IsDelete = 0 and s.Publish = 1 and s.IsSalonHoiQuan = 0  and ((s.Id = @SalonId) or (@SalonId = 0)) 
                        ),
                        BaoCaoVanHanh4 as
                        (
							select c24.SalonId as SalonId4 , 
							Coalesce(c24.PercentBill_ChoDuoi15,0) as PercentBill_ChoDuoi15,
							Coalesce(c25.PercentBill_ChoTren15,0) as PercentBill_ChoTren15,
							Coalesce( c26.PercentBill_ChoNULL,0) as PercentBill_ChoNULL
							from PercentThoiGianChoDuoi15P as c24
							left join PercentThoiGianChoTren15P as c25 on c24.SalonId = c25.SalonId
							left join PercentThoiGianChoNULL as c26 on c24.SalonId = c26.SalonId
                        ) 
                        select * from BaoCaoVanHanh1 as a 
                        left join BaoCaoVanHanh2 as b on a.SalonId = b.Id1
                        left join BaoCaoVanHanh3 as c on a.SalonId = c.Id 
                        left join BaoCaoVanHanh4 as d on a.SalonId = d.SalonId4
                        order by a.[Order] asc
                        end ";
                //var lst = db.Database.SqlQuery<Store_Report_Sale_V2_Result>(sql).Where(a => a.TongHoaDon > 0).ToList();
                var lst = db.Store_Report_Sale_V3(SalonId, _FromDate, _ToDate).Where(a => a.TongHoaDon > 0).ToList();

                //// Tính năng suất trung bình của Stylist
                //var NSTB_Stylist = new cls_nstb_item();
                //var temp = lst;                
                //if (temp.Count > 0)
                //{
                //    var loop = 0;
                //    foreach (var v in temp)
                //    {
                //        NSTB_Stylist = getData_NSTB_Stylist(v.SalonId, _FromDate, _ToDate);
                //        lst[loop].PercentDstylistTrenStylist = Math.Round((Double)(NSTB_Stylist.TotalQuantity / NSTB_Stylist.TotalHour), 2);
                //        lst[loop].PercentDThuStylistTrenStylist = Math.Round((Double)(NSTB_Stylist.TotalMoney / NSTB_Stylist.TotalHour), 2);
                //    }
                //}

                rptData.DataSource = lst;
                rptData.DataBind();
                if (SalonId == 0)
                {
                    int TotalSalon = lst.Count;
                    var itemAdd = new Store_Report_Sale_V3_Result();
                    //NSTB_Stylist = getData_NSTB_Stylist(0, _FromDate, _ToDate);

                    itemAdd.ShortName = "Toàn hệ thống";
                    itemAdd.TongHoaDon = lst.Sum(item => item.TongHoaDon);
                    itemAdd.DsShineCombo = lst.Sum(item => item.DsShineCombo);
                    itemAdd.DsKidCombo = lst.Sum(item => item.DsKidCombo);
                    itemAdd.DsDuongProtein = lst.Sum(item => item.DsDuongProtein);
                    itemAdd.DsUon = lst.Sum(item => item.DsUon);
                    itemAdd.DsNhuom = lst.Sum(item => item.DsNhuom);
                    itemAdd.DsTay = lst.Sum(item => item.DsTay);
                    itemAdd.DsTay2Lan = lst.Sum(item => item.DsTay2Lan);
                    itemAdd.TongDoanhThu = lst.Sum(item => item.TongDoanhThu);
                    itemAdd.TongDoanhTthuDichVu = lst.Sum(item => item.TongDoanhTthuDichVu);
                    itemAdd.TongDoanhTthuMyPham = lst.Sum(item => item.TongDoanhTthuMyPham);
                    itemAdd.StylistDiLam = lst.Sum(item => item.StylistDiLam);
                    itemAdd.SkinnerDiLam = lst.Sum(item => item.SkinnerDiLam);
                    itemAdd.PercentBill_RatHaiLong = Math.Round((Double)(lst.Sum(item => item.PercentBill_RatHaiLong)) / TotalSalon, 2);
                    itemAdd.PercentBill_HaiLong = Math.Round((Double)(lst.Sum(item => item.PercentBill_HaiLong)) / TotalSalon, 2);
                    itemAdd.PercentBill_ChuaHaiLong = Math.Round((Double)(lst.Sum(item => item.PercentBill_ChuaHaiLong)) / TotalSalon, 2);
                    itemAdd.PercentCusBack = Math.Round((Double)(lst.Sum(item => item.PercentCusBack)) / TotalSalon, 2);
                    itemAdd.PercentCusBook = Math.Round((Double)(lst.Sum(item => item.PercentCusBook)) / TotalSalon, 2);
                    itemAdd.PercentBill_ChoDuoi15 = Math.Round((Double)(lst.Sum(item => item.PercentBill_ChoDuoi15)) / TotalSalon, 2);
                    itemAdd.PercentBill_ChoTren15 = Math.Round((Double)(lst.Sum(item => item.PercentBill_ChoTren15)) / TotalSalon, 2);
                    itemAdd.PercentBill_ChoNULL = Math.Round((Double)(lst.Sum(item => item.PercentBill_ChoNULL)) / TotalSalon, 2);

                    itemAdd.Percent_UpImage = Math.Round((Double)(lst.Sum(item => item.Percent_UpImage)) / TotalSalon, 2);
                    itemAdd.PercentDoanhSOnWorkHour = Math.Round((Double)(lst.Sum(item => item.PercentDoanhSOnWorkHour)) / TotalSalon, 2);
                    itemAdd.PercentDoanhThuOnWorkHour = Math.Round((Double)(lst.Sum(item => item.PercentDoanhThuOnWorkHour)) / TotalSalon, 2);
                    //itemAdd.PercentDstylistTrenStylist = Math.Round((Double)(NSTB_Stylist.TotalQuantity / NSTB_Stylist.TotalHour), 2);
                    //itemAdd.PercentDThuStylistTrenStylist = Math.Round((Double)(NSTB_Stylist.TotalMoney / NSTB_Stylist.TotalHour), 2);
                    itemAdd.DoanhSoDVTrongKy = lst.Sum(item => item.DoanhSoDVTrongKy);
                    itemAdd.DoanhThuDVTrongKy = lst.Sum(item => item.DoanhThuDVTrongKy);
                    //ListReturListBoobHour.Add(item);
                    ToanHeThong.Add(itemAdd);

                }
                //var lst = Store_Report_Sale_V2_Result


            }
        }

        /////////////////////////////////////
        // Xử lý lấy giá trị năng suất trung bình của Stylist
        /////////////////////////////////////    
        /// <summary>
        /// Xử lý dữ liệu
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private cls_nstb_item getData_NSTB_Stylist(int salonId, DateTime timeFrom, DateTime timeTo)
        {
            try
            {
                var data = getDataFromStoreSQL(salonId, timeFrom, timeTo);
                var list = new List<cls_nstb_item>();
                var item = new cls_nstb_item();
                var itemService = new cls_service_item();
                var index = -1;
                var TotalReport = new cls_nstb_item();
                TotalReport.ListItemServices = new List<cls_service_item>();

                if (data.Count > 0)
                {
                    foreach (var v in data)
                    {
                        index = list.FindIndex(w => w.StylistId == v.StylistId);
                        if (index == -1)
                        {
                            item = new cls_nstb_item();
                            item.StylistId = v.StylistId;
                            item.TotalHour = v.TotalHour;
                            item.TotalBill = v.TotalBill;
                            list.Add(item);
                        }

                        // Xử lý cho báo cáo tổng
                        TotalReport.TotalQuantity += v.TotalQuantity;
                        TotalReport.TotalMoney += v.TotalMoney;
                    }

                    if (list.Count > 0)
                    {
                        foreach (var v in list)
                        {
                            TotalReport.TotalBill += v.TotalBill;
                            TotalReport.TotalHour += v.TotalHour;
                        }
                    }
                }

                return TotalReport;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// Truy vấn data từ DB
        /// </summary>
        /// <returns></returns>
        private List<cls_Store_HRM_ThongKeNangSuatStylist> getDataFromStoreSQL(int salonId, DateTime timeFrom, DateTime timeTo)
        {
            try
            {
                var staffId = 0;
                object[] paras =
                {
                    new SqlParameter("@TimeFrom", timeFrom),
                    new SqlParameter("@TimeTo", timeTo),
                    new SqlParameter("@TimeTo2", timeTo.AddDays(-1)),
                    new SqlParameter("@SalonId", salonId),
                    new SqlParameter("@StylistId", staffId)
                };
                var list = new List<cls_Store_HRM_ThongKeNangSuatStylist>();
                using (var db = new Solution_30shineEntities())
                {
                    list = db.Database.SqlQuery<cls_Store_HRM_ThongKeNangSuatStylist>("Store_HRM_ThongKeNangSuatStylist @TimeFrom, @TimeTo, @TimeTo2, @SalonId, @StylistId", paras).ToList();

                }
                return list;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Xử lý khi onclick
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void _BtnClick(Object sender, EventArgs e)
        {
            BindData();
        }
        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }


        /// <summary>
        /// Class dữ liệu từ store
        /// </summary>
        public class cls_Store_HRM_ThongKeNangSuatStylist
        {
            public int StylistId { get; set; }
            public string StylistName { get; set; }
            public int ServiceId { get; set; }
            public string ServiceName { get; set; }
            public int TotalQuantity { get; set; }
            public double TotalMoney { get; set; }
            public double TotalHour { get; set; }
            public int TotalBill { get; set; }
        }

        /// <summary>
        /// Class report item
        /// </summary>
        public class cls_nstb_item
        {
            /// <summary>
            /// Stylist ID
            /// </summary>
            public int StylistId { get; set; }
            /// <summary>
            /// Stylist Name
            /// </summary>
            public string StylistName { get; set; }
            /// <summary>
            /// Danh sách doanh số dịch vụ
            /// </summary>
            public List<cls_service_item> ListItemServices { get; set; }
            /// <summary>
            /// Doanh số
            /// </summary>
            public int TotalQuantity { get; set; }
            /// <summary>
            /// Doanh thu
            /// </summary>
            public double TotalMoney { get; set; }
            /// <summary>
            /// Tổng giờ làm
            /// </summary>
            public double TotalHour { get; set; }
            /// <summary>
            /// Tổng lượt khách
            /// </summary>
            public int TotalBill { get; set; }
            /// <summary>
            /// Doanh số trung bình / giờ
            /// </summary>
            public double DsTB_H { get; set; }
            /// <summary>
            /// Doanh thu trung bình / giờ
            /// </summary>
            public double DtTB_H { get; set; }
        }

        /// <summary>
        /// Class thể hiện doanh số theo dịch vụ
        /// </summary>
        public class cls_service_item
        {
            public int ServiceId { get; set; }
            public string ServiceName { get; set; }
            public int Quantity { get; set; }
            public double TotalMoney { get; set; }
        }
    }
}