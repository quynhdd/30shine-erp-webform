﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="reportAccountingTurnover.aspx.cs" Inherits="_30shine.GUI.BackEnd.Report.ReportAccounting.reportAccountingTurnover" %>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">

    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>

    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <%--<uc1:ReportMenu runat="server" id="ReportMenu" />--%>
        <script>
</script>
        <style>
            .table-wp {
                width: 100%;
                float: left;
            }

                .table-wp table th {
                    font-family: Roboto Condensed Bold;
                    font-weight: normal;
                }

            table {
                border: 1px solid black;
                border-collapse: collapse;
                padding: 5px;
            }

                table tr td, table tr th {
                    border: 1px solid black;
                    border-collapse: collapse;
                    padding: 5px;
                    min-width: 100px;
                    text-align: center;
                }

                table tr.active {
                    background: #ffe400;
                }

            .wp960.content-wp {
                overflow-y: scroll;
            }
        </style>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Báo cáo &nbsp;» </li>
                        <li class="be-report-li active"><a href="javascript:void(0);"><i class="fa fa-th-large"></i>Hướng dẫn quản lý nộp doanh thu</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="wp customer-add customer-listing be-report" onload="abc()">

            <div class="wp960 content-wp">
                <!-- Filter -->
                <div class="row">
                    <div class="filter-item" style="margin-left: 0;">
                        <strong class="st-head" style="margin-right: 10px;"><i class="fa fa-clock-o"></i>Thời gian</strong>
                        <div class="datepicker-wp">
                            <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeFrom" placeholder="Từ ngày" autocomplete="off"
                                ClientIDMode="Static" runat="server"></asp:TextBox>

                            <strong class="st-head" style="margin-left: 10px; margin-right: 10px;">
                                <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                            </strong>
                            <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeTo" placeholder="Đến ngày" autocomplete="off"
                                ClientIDMode="Static" runat="server"></asp:TextBox>
                        </div>
                        <div class="filter-item">
                            <asp:DropDownList ID="Salon" runat="server" CssClass="select form-control" ClientIDMode="Static" Style="width: 190px;"></asp:DropDownList>
                        </div>

                    </div>
                    <p class="st-head btn-viewdata" data-type="2" onclick="viewDataByDate($(this))">Xem dữ liệu</p>
                    <a href="javascript://" class="st-head btn-viewdata" onclick="ExportExcel($(this))">Xuất File Excel</a>
                    <div class="export-wp drop-down-list-wp" style="display: none;">
                        <div id="fff" class="st-head btn-viewdata" onclick="EntryExportExcel('all')">
                            Xuất File Excel
                        </div>
                        <ul class="ul-drop-down-list" style="display: none;">
                            <li onclick="EntryExportExcel('all')">Tất cả dữ liệu</li>
                            <li onclick="EntryExportExcel('current page')">Trang hiện tại</li>
                        </ul>
                    </div>
                </div>
                <!-- End Filter -->
                <div id="UPTotal" style="margin-top: 15px;">
                    <div class="row">
                        <strong class="st-head"><i class="fa fa-file-text"></i>Báo cáo hướng dẫn quản lý nộp doanh thu</strong>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="table-wp" id="table-wp-bckqkd" style="min-height: 584.4px; max-height: 779.2px;">
                                <table id="table-bckqkd" style="border: 1px solid black; border-collapse: collapse; padding: 5px;">
                                    <thead>
                                        <tr>
                                            <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;" rowspan="2">STT</th>
                                            <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;" rowspan="2">Salon</th>
                                            <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;" rowspan="2">TỔNG DOANH THU DV + MP</th>
                                            <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;" rowspan="2">Doanh thu DV</th>
                                            <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;" rowspan="2">Doanh thu MP</th>
                                            <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;" rowspan="2">Doanh thu Shine CB Free</th>
                                            <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;" rowspan="2">ID KẾ TOÁN</th>
                                            <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;" rowspan="2">ID KẾ TOÁN *</th>
                                            <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;" rowspan="2">Số lượng Shine CB Free</th>
                                            <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;" rowspan="2">NỘP TK CÔNG TY
                                            </th>
                                            <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;" colspan="2">TK CÁ NHÂN ( C = c1+ c2 )</th>
                                        </tr>
                                        <tr>
                                            <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;">NỘP TK CÁ NHÂN</th>
                                            <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;">QUẸT THẺ</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbodyBCKQKD">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- Page loading -->
        <div class="page-loading">
            <p>Vui lòng đợi trong giây lát...</p>
        </div>
         <link href="/Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="/Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 190px !important;
                margin-top: 5px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }
        </style>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                // Add active menu
                $("#glbAdminSales").addClass("active");
                $("#glbAdminKetQuaKinhDoanh").addClass("active");
                $("li.be-report-li").addClass("active");

                // Price format
                UP_FormatPrice('.be-report-price');

                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) {
                        //$input.val($input.val().split(' ')[0]);
                    }
                });

                // View data today
                //$(".tag-date-today").click();

            });

            function SubTableFixHeight() {
                $("tbody table.sub-table").each(function () {
                    $(this).height($(this).parent().height() + 1);
                });
            }
            //============================

            // Khai bao 
            var uri = window.ReportAccounting;
            // call api
            function viewDataByDate(This) {
                //khai bao bien
                var sumTotalIncome = 0;
                var sumTotalServiceIncome = 0;
                var sumTotalProductIncome = 0;
                var sumShineComboFreeIncome = 0;
                var sumShineCombo = 0;
                var sumRevokeNumberRHL4 = 0;
                var sumShineComboFreeNumber = 0;
                var sumSendCompanyAccount = 0;
                var sumSendPersonalAccount = 0;
                var sumSwipeCardMoney = 0;


                try {
                    var STT = 0;
                    var FromDate = "";
                    var ToDate = "";
                    var salonId = $("#Salon").val();
                    if (!$("#TxtDateTimeFrom").val()) {
                        alert("Bạn chưa chọn ngày");
                        return
                    }
                    FromDate = $("#TxtDateTimeFrom").val()
                    if (!$("#TxtDateTimeTo").val()) {
                        alert("Bạn chưa chọn ngày");
                        return;
                    }
                    ToDate = $("#TxtDateTimeTo").val()

                    startLoading();
                    $.ajax({
                        type: "GET",
                        url: uri.domain + uri.accountingReport + '?salonId=' + salonId + '&dateFrom=' + FromDate + '&dateTo=' + ToDate,
                        //async: false,
                        //data: ,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (Data) {
                            console.log(Data);
                            $('#tbodyBCKQKD').empty();
                            jQuery.each(Data, function (key, item) {

                                sumTotalIncome = sumTotalIncome + parseInt(item.totalIncome);
                                sumTotalServiceIncome = sumTotalServiceIncome + parseInt(item.totalServiceIncome);
                                sumTotalProductIncome = sumTotalProductIncome + parseInt(item.totalProductIncome);
                                sumShineComboFreeIncome = sumShineComboFreeIncome + parseInt(item.shineComboFreeIncome);
                                sumShineCombo = sumShineCombo + parseInt(item.shineCombo);
                                sumRevokeNumberRHL4 = sumRevokeNumberRHL4 + parseInt(item.revokeNumberRHL4);
                                sumShineComboFreeNumber = sumShineComboFreeNumber + parseInt(item.shineComboFreeNumber);
                                sumSendCompanyAccount = sumSendCompanyAccount + parseInt(item.sendCompanyAccount);
                                sumSendPersonalAccount = sumSendPersonalAccount + parseInt(item.sendPersonalAccount);
                                sumSwipeCardMoney = sumSwipeCardMoney + parseInt(item.swipeCardMoney);

                                //append data
                                $('<tr>' +
                                    '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;">' + ++STT + '</td>' +
                                    '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;">' + item.salonName + '</td>' +
                                    '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;">' + formatPrice(item.totalIncome) + '</td>' +
                                    '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;">' + formatPrice(item.totalServiceIncome) + '</td>' +
                                    '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;">' + formatPrice(item.totalProductIncome) + '</td>' +
                                    '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;">' + formatPrice(item.shineComboFreeIncome) + '</td>' +
                                    '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;">' + formatPrice(item.shineCombo) + '</td>' +
                                    '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;">' + formatPrice(item.revokeNumberRHL4) + '</td>' +
                                    '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;">' + formatPrice(item.shineComboFreeNumber) + '</td>' +
                                    '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;">' + formatPrice(item.sendCompanyAccount) + '</td>' +
                                    '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;">' + formatPrice(item.sendPersonalAccount) + '</td>' +
                                    '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;">' + formatPrice(item.swipeCardMoney) + '</td>' +
                                    '</tr>').appendTo($('#tbodyBCKQKD'));
                            });
                            // append total
                            $('#tbodyBCKQKD').prepend('<tr>' +
                                '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;" colspan="2"> Tổng </td>' +
                                '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;">' + formatPrice(sumTotalIncome) + '</td>' +
                                '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;">' + formatPrice(sumTotalServiceIncome) + '</td>' +
                                '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;">' + formatPrice(sumTotalProductIncome) + '</td>' +
                                '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;">' + formatPrice(sumShineComboFreeIncome) + '</td>' +
                                '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;">' + formatPrice(sumShineCombo) + '</td>' +
                                '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;">' + formatPrice(sumRevokeNumberRHL4) + '</td>' +
                                '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;">' + formatPrice(sumShineComboFreeNumber) + '</td>' +
                                '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;">' + formatPrice(sumSendCompanyAccount) + '</td>' +
                                '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;">' + formatPrice(sumSendPersonalAccount) + '</td>' +
                                '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;">' + formatPrice(sumSwipeCardMoney) + '</td>' +
                                '</tr>');
                            //.prepend($('#tbodyBCKQKD'));
                            finishLoading();
                        },
                        error: function (objData, error) {
                            $('#tbodyBCKQKD').empty();
                            alert('Có lỗi xảy ra ! Vui lòng liên hệ với nhóm phát triển !');
                            finishLoading();
                            //console.log("objData", objData);
                            //console.log("error", error);
                        }
                    });
                } catch (e) {
                    alert("Lỗi " + e.message);
                }

            }

            // Xuat file excel
            function ExportExcel(This) {
                // function export excel
                funcExportExcelHtml("#table-wp-bckqkd", "báo cáo hướng dẫn quản lý nộp doanh thu");
            };
        </script>

    </asp:Panel>
</asp:Content>
