﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="reportAccountingTax.aspx.cs" Inherits="_30shine.GUI.BackEnd.Report.ReportAccounting.reportAccountingTax" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>BÁO CÁO NĂNG SUẤT- ĐIỂM HÀI LÒNG	
    </title>
</asp:Content>
<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">

    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>

    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <%--<uc1:ReportMenu runat="server" id="ReportMenu" />--%>
        <script>
</script>
        <style>
            .table-wp {
                width: 100%;
                float: left;
            }

                .table-wp table th {
                    font-family: Roboto Condensed Bold;
                    font-weight: normal;
                }

            table {
                border: 1px solid black;
                border-collapse: collapse;
                padding: 5px;
            }

                table tr td, table tr th {
                    border: 1px solid black;
                    border-collapse: collapse;
                    padding: 5px;
                    min-width: 100px;
                    text-align: center;
                }

                table tr.active {
                    background: #ffe400;
                }

            .wp960.content-wp {
                overflow-y: scroll;
            }
        </style>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Báo cáo &nbsp;» </li>
                        <li class="be-report-li active"><a href="javascript:void(0);"><i class="fa fa-th-large"></i>Năng xuất - điểm hài lòng</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="wp customer-add customer-listing be-report" onload="abc()">

            <div class="wp960 content-wp">
                <!-- Filter -->
                <div class="row">
                    <div class="filter-item" style="margin-left: 0;">
                        <strong class="st-head" style="margin-right: 10px;"><i class="fa fa-clock-o"></i>Thời gian</strong>
                        <div class="datepicker-wp">
                            <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeFrom" placeholder="Từ ngày" autocomplete="off"
                                ClientIDMode="Static" runat="server"></asp:TextBox>

                            <strong class="st-head" style="margin-left: 10px; margin-right: 10px;">
                                <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                            </strong>
                            <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeTo" placeholder="Đến ngày" autocomplete="off"
                                ClientIDMode="Static" runat="server"></asp:TextBox>
                        </div>
                        <div class="filter-item">
                            <asp:DropDownList ID="Salon" runat="server" CssClass="select form-control" ClientIDMode="Static" Style="width: 190px;"></asp:DropDownList>
                        </div>
                        <div class="filter-item">
                            <asp:DropDownList ID="ddlStaffType" runat="server" ClientIDMode="static" Style="margin: 12px 0; width: 150px;"></asp:DropDownList>
                        </div>
                    </div>
                    <p class="st-head btn-viewdata" onclick="viewDataByDate($(this))">Xem dữ liệu</p>
                    <a href="javascript://" class="st-head btn-viewdata" onclick="ExportExcel($(this))">Xuất File Excel</a>
                    <div class="export-wp drop-down-list-wp" style="display: none;">
                        <div id="fff" class="st-head btn-viewdata" onclick="EntryExportExcel('all')">
                            Xuất File Excel
                        </div>
                        <ul class="ul-drop-down-list" style="display: none;">
                            <li onclick="EntryExportExcel('all')">Tất cả dữ liệu</li>
                            <li onclick="EntryExportExcel('current page')">Trang hiện tại</li>
                        </ul>
                    </div>
                </div>
                <!-- End Filter -->
                <div id="UPTotal" style="margin-top: 15px;">
                    <div class="row">
                        <strong class="st-head"><i class="fa fa-file-text"></i>Báo cáo năng xuất - điểm hài lòng</strong>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="table-wp" id="table-report" style="min-height: 584.4px; max-height: 779.2px;">
                                <table id="table-repot" style="border: 1px solid black; border-collapse: collapse; padding: 5px;">
                                    <thead id="theadReport">
                                    </thead>
                                    <tbody id="tbodyReport">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- Page loading -->
        <div class="page-loading">
            <p>Vui lòng đợi trong giây lát...</p>
        </div>
         <link href="/Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="/Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 190px !important;
                margin-top: 5px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }
        </style>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                // Add active menu
                $("#glbAdminSales").addClass("active");
                $("#glbAdminKetQuaKinhDoanh").addClass("active");
                $("li.be-report-li").addClass("active");

                // Price format
                UP_FormatPrice('.be-report-price');

                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) {
                        //$input.val($input.val().split(' ')[0]);
                    }
                });

                // View data today
                //$(".tag-date-today").click();

            });

            function SubTableFixHeight() {
                $("tbody table.sub-table").each(function () {
                    $(this).height($(this).parent().height() + 1);
                });
            }
            //============================

            // Khai bao 
            var uri = window.ReportAccounting;
            // call api
            function viewDataByDate(This) {
                try {
                    var FromDate = "";
                    var ToDate = "";
                    var salonId = $("#Salon").val();
                    if (parseInt(salonId) === 0) {
                        alert("Chức năng chỉ xem được 1 salon ! Mời bạn chọn Salon !");
                        return;
                    }
                    if (!$("#TxtDateTimeFrom").val()) {
                        alert("Bạn chưa chọn ngày");
                        return;
                    }
                    FromDate = $("#TxtDateTimeFrom").val()
                    if (!$("#TxtDateTimeTo").val()) {
                        alert("Bạn chưa chọn ngày");
                        return;
                    }
                    ToDate = $("#TxtDateTimeTo").val()
                    var staffType = $("#ddlStaffType").val();
                    //var abc = uri.domain + uri.accountingReportTax + '?salonId=' + salonId + '&staffType=' + staffType + '&dateFrom=' + FromDate + '&dateTo=' + ToDate;
                    //console.log("abc", abc);
                    //return;
                    startLoading();
                    $.ajax({
                        type: "GET",
                        url: uri.domain + uri.accountingReportTax + '?salonId=' + salonId + '&staffType=' + staffType + '&dateFrom=' + FromDate + '&dateTo=' + ToDate,
                        //async: false,
                        //data: ,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (Data) {

                            //console.log("DAta", Data);
                            //if (Data = "undefined" || Data === null) {
                            //    alert("Không có data");
                            //    finishLoading();
                            //    return;
                            //}
                            console.log(Data);
                            appendReportAccountingTax(Data);
                            finishLoading();
                        },
                        error: function (objData, error) {
                            $('#tbodyBCKQKD').empty();
                            alert('Có lỗi xảy ra ! Vui lòng liên hệ với nhóm phát triển !');
                            finishLoading();

                        }
                    });
                } catch (e) {
                    alert("Lỗi " + e.message);
                }

            }

            // Xuat file excel
            function ExportExcel(This) {
                // function export excel
                funcExportExcelHtml("#UPTotal", "Bao_cao_nang_xuat_diem_hai_long");
            };
        </script>


        <script>
            function appendReportAccountingTax(obj) {
                // Gan lai data ( Test )
                var Data = obj;
                // Xu ly Header
                var header = Data.header; // gan lai
                console.log("Data.header.totalServiceTurnoverBonusAll", Data.header.totalServiceTurnoverBonusAll);
                var headerShineCombo = Data.header.shineComBo;
                var shineComboSubHeader = Data.header.shineComBo.subHeader;
                //return;
                var headerService = null; //Data.header.service[i]
                var subHeaderService = null; //Data.header.service[i].subHeader
                const CountHeaderService = Data.header.service.length;
                const countColspanShinecombo = Object.keys(shineComboSubHeader).length;
                //return;
                var countColspanService = null;
                var serviceSubHeader = null; //Data.header.service[i].subHeader
                const countColspanHeader = 1;
                var strHeader = "";
                var strSubHeader = "";
                var strSubHeaderService = "";
                strHeader =
                    "<tr>" +
                    //STT
                    "<th style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\" rowspan=\"2\">STT</th>" +
                    // ten salon
                    "<th style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\" rowspan=\"2\">" + header.salonName + "</th>" +
                    // id nhan vien
                    "<th style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\" rowspan=\"2\">" + header.id + "</th>" +
                    //ten nhan vien
                    "<th style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 170px; text-align: center;\" rowspan=\"2\">" + header.staffName + "</th>" +
                    // level
                    "<th style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\" rowspan=\"2\">" + header.level + "</th>" +
                    // ten bo phan
                    "<th style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\" rowspan=\"2\">" + header.departmentName + "</th>" +
                    // tong luong
                    "<th style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\" rowspan=\"2\">" + header.totalSalary + "</th>" +
                    // luong co ban theo ngay cong    
                    "<th style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\" rowspan=\"2\">" + header.fixSalaryByDay + "</th>" +
                    // tong thuong doanh so dich vu ( khong tang ca + tang ca ngay + tang ca gio)
                    "<th style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\" rowspan=\"2\">" + header.totalServiceTurnoverBonusAll + "</th>" +
                    // tong thuong doanh so dich vu ( khong tang ca )
                    "<th style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\" rowspan=\"2\">" + header.totalServiceTurnoverBonus + "</th>" +
                    // tong thuong doanh so dich vu tang ca ngay
                    "<th style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\" rowspan=\"2\">" + header.totalServiceTurnoverDayOvertimeBonus + "</th>" +
                    // tong thuong doanh so dich vu tang ca theo gio
                    "<th style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\" rowspan=\"2\">" + header.totalServiceTurnoverHoursOvertimeBonus + "</th>" +
                    // ngay cong
                    "<th style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\" rowspan=\"2\">" + header.workDay + "</th>" +
                    // tong diem hai long da tinh theo cong thuc giam tru
                    "<th style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\" rowspan=\"2\">" + header.totalServiceRatedVerySatisfied + "</th>" +
                    // Dich vu shinecombo
                    "<th style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"  colspan=\"" + countColspanShinecombo + "\"> " + headerShineCombo.serviceName + "</th> ";
                for (var i = 0; i < CountHeaderService; i++) {
                    headerService = Data.header.service[i];
                    subHeaderService = Data.header.service[i].subHeader;
                    countColspanService = Object.keys(subHeaderService).length;
                    //return;
                    // cac dich vu khac
                    strHeader = strHeader +
                        "<th style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"  colspan=\"" + countColspanService + "\"> " + headerService.serviceName + "</th > ";
                    // sub string html  service
                    strSubHeaderService = strSubHeaderService +
                        // tong so luong dich vu
                        "<th style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + subHeaderService.totalServiceNumber + "</th > " +
                        // RHL5**
                        "<th style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + subHeaderService.verySatisfied5Hour + "</th > " +
                        // RHL5*
                        "<th style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + subHeaderService.verySatisfied5Day + "</th > " +
                        // RHL4*
                        //"<th style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + subHeaderService.RHL4 + "</th > " +
                        // RHL3*
                        "<th style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + subHeaderService.verySatisfied3 + "</th > " +
                        // RHL2*
                        "<th style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + subHeaderService.satisfied + "</th > " +
                        // KHL
                        "<th style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + subHeaderService.notSatisfied + "</th > ";
                };
                strHeader = strHeader +
                    "</tr>";
                strSubHeader =
                    "<tr>" +
                    // Tổng số lượng Shine Combo
                    "<th style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + shineComboSubHeader.totalNumberOfShineCombo + "</th> " +
                    // Tổng Bill Shine combo cũ RHL
                    "<th style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + shineComboSubHeader.totalServiceRatedVerySatisfiedOld + "</th>" +
                    // Tổng Bill Shine combo mới RHL
                    "<th style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + shineComboSubHeader.totalServiceRatedVerySatisfiedNew + "</th>" +
                    // RHL5**
                    "<th style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + shineComboSubHeader.verySatisfied5Hour + "</th> " +
                    // RHL5*
                    "<th style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + shineComboSubHeader.verySatisfied5Day + "</th> " +
                    // RHL4*
                    "<th style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + shineComboSubHeader.verySatisfied4 + "</th> " +
                    // RHL3*
                    "<th style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + shineComboSubHeader.verySatisfied3 + "</th> " +
                    // RHL3 GIam gia
                    "<th style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + shineComboSubHeader.verySatisfied3Discount + "</th> " +
                    // RHL2*
                    "<th style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + shineComboSubHeader.satisfied + "</th> " +
                    // KHL
                    "<th style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + shineComboSubHeader.notSatisfied + "</th> " +
                    // sub string html  service
                    strSubHeaderService +
                    "</tr>";
                $("#theadReport").empty();
                $("#theadReport").append(strHeader).append(strSubHeader);
                // End header

                // xu ly phan data total
                var DataTotal = Data.total;
                var dataTotalService = null;
                var countTotalService = null;
                var itemTotalService = null;
                var itemTotalShinecombo = null;
                var strTotal = "";
                itemTotalShinecombo = DataTotal.dataShineCombo;
                countTotalService = DataTotal.dataOtherService.length;
                //return;
                strTotal = strTotal +
                    "<tr>" +
                    "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\" colspan=\"2\">Tổng</td> " +
                    "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\" colspan=\"4\"></td> " +
                    "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + formatPrice(Math.round(DataTotal.totalSalary)) + "</td> " +
                    "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + formatPrice(Math.round(DataTotal.fixSalaryByDay)) + "</td> " +
                    "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + formatPrice(DataTotal.totalServiceTurnoverBonusAll) + "</td> " +
                    "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + formatPrice(DataTotal.totalServiceTurnoverBonus) + "</td> " +
                    "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + formatPrice(DataTotal.totalServiceTurnoverDayOvertimeBonus) + "</td> " +
                    "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + formatPrice(DataTotal.totalServiceTurnoverHoursOvertimeBonus) + "</td> " +
                    "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + DataTotal.workDay + "</td> " +
                    "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + DataTotal.totalServiceRatedVerySatisfied + "</td> " +
                    "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + itemTotalShinecombo.totalNumberOfShineCombo + "</td> " +
                    "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + itemTotalShinecombo.totalServiceRatedVerySatisfiedOld + "</td> " +
                    "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + itemTotalShinecombo.totalServiceRatedVerySatisfiedNew + "</td> " +
                    "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + itemTotalShinecombo.verySatisfied5Hour + "</td> " +
                    "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + itemTotalShinecombo.verySatisfied5Day + "</td> " +
                    "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + itemTotalShinecombo.verySatisfied4 + "</td> " +
                    "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + itemTotalShinecombo.verySatisfied3 + "</td> " +
                    "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + itemTotalShinecombo.verySatisfied3Discount + "</td> " +
                    "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + itemTotalShinecombo.satisfied + "</td> " +
                    "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + itemTotalShinecombo.notSatisfied + "</td> ";

                for (var j = 0; j < countTotalService; j++) {
                    itemTotalService = DataTotal.dataOtherService[j];

                    strTotal = strTotal +
                        // Tong so luong dich vu**
                        "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + itemTotalService.totalServiceNumber + "</td> " +
                        // RHL5**
                        "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + itemTotalService.verySatisfied5Hour + "</td> " +
                        // RHL5*
                        "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + itemTotalService.verySatisfied5Day + "</td> " +
                        // RHL3*
                        "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + itemTotalService.verySatisfied3 + "</td> " +
                        // RHL2*
                        "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + itemTotalService.satisfied + "</td> " +
                        // KHL
                        "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + itemTotalService.notSatisfied + "</td> ";
                }
                strTotal = strTotal +
                    "</tr>";


                // Remove append data

                //$("#tbodyReport").append(strBody);

                //Remove append data
                $("#tbodyReport").empty();
                $("#tbodyReport").append(strTotal);
                // End total

                // xu ly phan data body
                var DataBody = Data.data;
                var countDateBody = Data.data.length;
                var dataService = null;
                var itemData = null;
                var countService = null;
                var itemService = null;
                var itemShinecombo = null;
                var strBody = "";
                var STT = 0;
                for (var i = 0; i < countDateBody; i++) {
                    itemData = DataBody[i];
                    itemShinecombo = itemData.dataShineCombo;

                    countService = itemData.dataOtherService.length;
                    //return;
                    strBody = strBody +
                        "<tr>" +
                        "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + ++STT + "</td> " +
                        "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + itemData.salonName + "</td> " +
                        "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + itemData.id + "</td> " +
                        "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 170px; text-align: center;\"> " + itemData.staffName + "</td> " +
                        "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + itemData.level + "</td> " +
                        "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + itemData.departmentName + "</td> " +
                        "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + formatPrice(parseInt(itemData.totalSalary)) + "</td> " +
                        "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + formatPrice(parseInt(itemData.fixSalaryByDay)) + "</td> " +
                        "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + formatPrice(itemData.totalServiceTurnoverBonusAll) + "</td> " +
                        "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + formatPrice(itemData.totalServiceTurnoverBonus) + "</td> " +
                        "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + formatPrice(itemData.totalServiceTurnoverDayOvertimeBonus) + "</td> " +
                        "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + formatPrice(itemData.totalServiceTurnoverHoursOvertimeBonus) + "</td> " +
                        "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + itemData.workDay + "</td> " +
                        "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + itemData.totalServiceRatedVerySatisfied + "</td> " +
                        "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + itemShinecombo.totalNumberOfShineCombo + "</td> " +
                        "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + itemShinecombo.totalServiceRatedVerySatisfiedOld + "</td> " +
                        "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + itemShinecombo.totalServiceRatedVerySatisfiedNew + "</td> " +
                        "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + itemShinecombo.verySatisfied5Hour + "</td> " +
                        "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + itemShinecombo.verySatisfied5Day + "</td> " +
                        "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + itemShinecombo.verySatisfied4 + "</td> " +
                        "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + itemShinecombo.verySatisfied3 + "</td> " +
                        "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + itemShinecombo.verySatisfied3Discount + "</td> " +
                        "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + itemShinecombo.satisfied + "</td> " +
                        "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + itemShinecombo.notSatisfied + "</td> ";

                    for (var j = 0; j < countService; j++) {
                        itemService = itemData.dataOtherService[j];

                        strBody = strBody +
                            // Tong so dich vu
                            "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + itemService.totalServiceNumber + "</td> " +
                            // RHL5**
                            "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + itemService.verySatisfied5Hour + "</td> " +
                            // RHL5*
                            "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + itemService.verySatisfied5Day + "</td> " +
                            // RHL3*
                            "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + itemService.verySatisfied3 + "</td> " +
                            // RHL2*
                            "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + itemService.satisfied + "</td> " +
                            // KHL
                            "<td style=\"border: 1px solid black; border-collapse: collapse; padding: 5px; min-width: 100px; text-align: center;\"> " + itemService.notSatisfied + "</td> ";
                    }
                    strBody = strBody +
                        "</tr>";
                }

                // Remove append data
                //$("#tbodyReport").empty();
                //$("#tbodyReport").append(strBody);

                //Remove append data
                $("#tbodyReport").append(strBody);
                // End body
                $('html,body').animate({
                    scrollTop: $("#UPTotal").offset().top
                },
                    'slow');
            };



        </script>

    </asp:Panel>
</asp:Content>
