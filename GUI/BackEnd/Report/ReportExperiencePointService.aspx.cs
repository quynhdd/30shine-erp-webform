﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using Libraries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.Report
{
    public partial class ReportExperiencePointService : System.Web.UI.Page
    {
        private bool Perm_Access;
        public bool Perm_ViewAllData;
        private bool Perm_ShowSalon;
        public int StaffId = 0;

        /// <summary>
        /// check permission
        /// </summary>
        private void SetPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                int integer;
                StaffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, StaffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, StaffId);
                Perm_ShowSalon = permissionModel.CheckPermisionByAction("Perm_ShowSalon", pageId, StaffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        /// <summary>
        /// Executeby permission
        /// </summary>
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                //BindRegion();
                Library.Function.bindSalon(new List<DropDownList> { ddlSalon }, Perm_ViewAllData);
                if (!Perm_ViewAllData)
                {
                    var item = new ListItem("Chọn tất cả", "0");
                    ddlSalon.Items.Insert(0, item);
                }

            }
        }
        //public void BindRegion()
        //{
        //    try
        //    {
        //        using (var db = new Solution_30shineEntities())
        //        {
        //            var listRegion = db.Tbl_Config.Where(r => r.Description == @"Cấu hình vùng salon").ToList();
        //            if (listRegion.Any())
        //            {
        //                var record = new Tbl_Config();
        //                record.Label = "Chọn tất cả";
        //                record.Value = "0";
        //                listRegion.Insert(0, record);

        //                ddlRegion.DataTextField = "Label";
        //                ddlRegion.DataValueField = "Value";
        //                ddlRegion.DataSource = listRegion;
        //                ddlRegion.DataBind();
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        throw e;
        //    }
        //}
    }
}