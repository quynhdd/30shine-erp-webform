﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Globalization;
using System.Web.Script.Serialization;
using ExportToExcel;
using Excel;
using System.Net.Mail;
using System.Net;

namespace _30shine.GUI.BackEnd.Report
{
    public partial class CustomerMailReport : System.Web.UI.Page
    {
        public CustomerReport  _CustomerReport;
        protected void Page_Load(object sender, EventArgs e)
        {
            Get_CustomerData();
            //SendMail(_CustomerReport, "vong.lv@hkphone.com.vn");//,vongebn@gmail.com,nguyen.hoang@hkphone.com.vn
        }

        public void Get_CustomerData()
        {
            using (var db = new Solution_30shineEntities())
            {
                _CustomerReport.qtt_Customer = db.Customers.Count(w => w.IsDelete != 1).ToString();
                _CustomerReport.qtt_CustomerHasInfo = db.Customers.Count(w => w.IsDelete != 1 && w.Phone != "").ToString();
                _CustomerReport.qtt_CustomerHasImage = db.BillServices.Where(w=>w.IsDelete != 1 && w.Images != "" && w.Images != null).GroupBy(g => g.CustomerCode).Count().ToString();
            }
        }

        protected static void SendMail(CustomerReport _CustomerReport , string ReceiveEmail)
        {
            //Reading sender Email credential from web.config file
            string HostAdd = "smtp.gmail.com";
            string FromEmailid = "service.30shine@gmail.com";
            string Pass = "v8Yy2MdfXH9s";
            string ToEmail = ReceiveEmail;
            string body = "";
            body += "<p>Báo cáo dữ liệu khách hàng hàng tuần : </p>";
            body += "<b>Tổng số khách hàng : </b> " + _CustomerReport.qtt_Customer + "<br />";
            body += "<b>Số khách hàng có thông tin : </b> " + _CustomerReport.qtt_CustomerHasInfo + "<br />";
            body += "<b>Số khách hàng được gán ảnh : </b> " + _CustomerReport.qtt_CustomerHasImage + "<br />";

            //creating the object of MailMessage
            MailMessage mailMessage = new MailMessage();
            mailMessage.From = new MailAddress(FromEmailid); //From Email Id
            mailMessage.Subject = "Report data khách hàng - 30Shine Solution"; //Subject of Email
            mailMessage.Body = body; //body or message of Email
            mailMessage.IsBodyHtml = true;
            //Adding Multiple recipient email id logic
            string[] Multi = ToEmail.Split(','); //spiliting input Email id string with comma(,)
            foreach (string Multiemailid in Multi)
            {
                mailMessage.To.Add(new MailAddress(Multiemailid)); //adding multi reciver's Email Id
            }
            SmtpClient smtp = new SmtpClient(); // creating object of smptpclient
            smtp.Host = HostAdd; //host of emailaddress for example smtp.gmail.com etc

            //network and security related credentials
            smtp.EnableSsl = true;
            NetworkCredential NetworkCred = new NetworkCredential();
            NetworkCred.UserName = mailMessage.From.Address;
            NetworkCred.Password = Pass;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = NetworkCred;
            smtp.Port = 587;
            smtp.Send(mailMessage); //sending Email
        }
    }

    public struct CustomerReport
    {
        public string qtt_Customer { get; set; }
        public string qtt_CustomerHasInfo { get; set; }
        public string qtt_CustomerHasImage { get; set; }
        public string mailBody { get; set; }
    }
}