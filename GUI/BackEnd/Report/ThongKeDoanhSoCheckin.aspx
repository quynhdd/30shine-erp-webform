﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ThongKeDoanhSoCheckin.aspx.cs" Inherits="_30shine.GUI.BackEnd.Report.ThongKeDoanhSoCheckin" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>
<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

        <%--<uc1:ReportMenu runat="server" id="ReportMenu"  />---%>

        <style>
            table.table-report-department
            {
            }

            table.table-report-department
            {
                border-collapse: collapse;
            }

            table.table-report-department, .table-report-department th, .table-report-department td
            {
                border: 1px solid #bababa;
                display: block;
            }

            .table-report-department th
            {
                font-weight: normal;
                font-family: Roboto Condensed Bold;
                padding: 5px 10px;
            }

            .table-report-department td
            {
                padding: 5px 10px;
            }
        </style>

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Báo cáo &nbsp;&#187; </li>
                        <li class="be-report-li"><a href="javascript:void(0);"><i class="fa fa-th-large"></i>Thử nghiệm lương doanh số Checkin</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing be-report be-report-timekeeping">
            <%-- Listing --%>
            <asp:ScriptManager ID="SM01" runat="server"></asp:ScriptManager>
            <div class="wp960 content-wp">
                <div class="row">
                    <strong class="st-head"><i class="fa fa-clock-o"></i>Thời gian</strong>
                    <div class="time-wp">
                        <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeFrom" placeholder="Từ ngày"
                            ClientIDMode="Static" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="ValidateTime" ControlToValidate="TxtDateTimeFrom" runat="server" CssClass="fb-cover-error" Text="Vui lòng chọn thời gian!"></asp:RequiredFieldValidator>
                    </div>
                    <strong class="st-head" style="margin-left: 10px;">
                        <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                    </strong>
                    <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeTo" placeholder="Đến ngày"
                        ClientIDMode="Static" runat="server"></asp:TextBox>
                    <asp:UpdatePanel runat="server" ID="UPsalon">
                        <ContentTemplate>
                            <div class="filter-item">
                                <asp:DropDownList ID="Salon" CssClass="form-control select" runat="server" ClientIDMode="Static" Style="width: 190px;" OnSelectedIndexChanged="bindStaffByDepartment" AutoPostBack="true"></asp:DropDownList>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="row">
                    <strong class="st-head"><i class="fa fa-filter"></i>Lọc kết quả</strong>
                    <asp:UpdatePanel runat="server" ID="UPStaff">
                        <ContentTemplate>
                            <div class="filter-item" style="display: none;">
                                <asp:DropDownList ID="ddlDepartment" CssClass="form-control" runat="server" ClientIDMode="Static" Style="width: 190px;" OnSelectedIndexChanged="bindStaffByDepartment" AutoPostBack="true"></asp:DropDownList>
                            </div>
                            <div class="filter-item">
                                <asp:DropDownList ID="ddlStaff" CssClass="form-control" runat="server" ClientIDMode="Static" Style="width: 190px;"></asp:DropDownList>
                            </div>
                            <div class="filter-item" style="display: none;">
                                <asp:DropDownList ID="Active" runat="server" CssClass="form-control" ClientIDMode="Static" Style="margin: 12px 0; width: 149px;">
                                    <asp:ListItem Text="Đang làm việc" Value="1" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="Đã nghỉ" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </ContentTemplate>
                        <Triggers></Triggers>
                    </asp:UpdatePanel>
                    <asp:Panel ID="ViewDataFilter" CssClass="st-head btn-viewdata" ClientIDMode="Static"
                        onclick="excPaging(1)" runat="server">
                        Xem dữ liệu
                    </asp:Panel>
                    <a href="#" class="st-head btn-viewdata">Reset Filter</a>
                </div>
                <div class="row">
                    <strong class="st-head"><i class="fa fa-file-text"></i>Thông kê doanh số Checkin</strong>
                </div>
                <!-- Row Table Filter -->
                <div class="table-func-panel" style="margin-top: -33px;">
                    <div class="table-func-elm">
                        <span>Số hàng / Page : </span>
                        <div class="table-func-input-wp">
                            <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                            <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                            <ul class="ul-opt-segment">
                                <li data-value="10">10</li>
                                <li data-value="20">20</li>
                                <li data-value="30">30</li>
                                <li data-value="40">40</li>
                                <li data-value="50">50</li>
                                <li data-value="1000000">Tất cả</li>
                            </ul>
                            <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                        </div>
                    </div>
                </div>
                <!-- End Row Table Filter -->
                <asp:UpdatePanel ID="UPTableListing" runat="server">
                    <ContentTemplate>
                        <div class="row table-wp" id="tableWrap" runat="server">
                            <table class="table-add table-listing table-listing-salary">
                                <thead>
                                    <tr>
                                        <th>Nhân viên</th>
                                        <th>Hệ số:750</th>
                                        <th>Khách book chờ (<= 15')
                                            <br />
                                            ( x 3đ )</th>
                                        <th>Khách book chờ (15'-20')
                                            <br />
                                            (x 1đ )</th>
                                        <th>Khách book chờ ( > 20')
                                            <br />
                                            ( x 0đ )</th>
                                        <th>Khách book chờ
                                            <br />
                                            (Không có dữ liệu)
                                            <br />
                                            ( x 1đ )</th>
                                        <th>Tổng điểm</th>
                                        <th>Tổng tiền</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="rptSalaryCheckin" runat="server">
                                        <ItemTemplate>
                                    <tr>
                                        <td rowspan="2"><%#Eval("StaffName") %></td>
                                        <td>Số bill</td>
                                        <td><%# String.Format("{0:#,###}", Eval("PointBefore15")).Replace(",", ".") %></td>
                                        <td><%# String.Format("{0:#,###}",Eval("PointBetween15and20")).Replace(",", ".") %></td>
                                        <td><%# String.Format("{0:#,###}",Eval("PointAfter20")).Replace(",", ".") %></td>
                                        <td><%# String.Format("{0:#,###}",Eval("PointNotTime")).Replace(",", ".") %></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Điểm doanh số</td>
                                        <td><%# String.Format("{0:#,###}", Convert.ToInt32(Eval("PointBefore15")) * PointBefore15).Replace(",", ".") %></td>
                                        <td><%# String.Format("{0:#,###}", Convert.ToInt32(Eval("PointBetween15and20")) * PointBetween15and20).Replace(",", ".") %></td>
                                        <td><%# String.Format("{0:#,###}", Convert.ToInt32(Eval("PointAfter20")) * PointAfter20).Replace(",", ".") %></td>
                                        <td><%# String.Format("{0:#,###}", Convert.ToInt32(Eval("PointNotTime")) * PointNull).Replace(",", ".") %></td>
                                        <td><%# String.Format("{0:#,###}", Eval("totalPoint")).Replace(",", ".")%></td>
                                        <td><%# String.Format("{0:#,###}", Eval("totalMoney")).Replace(",", ".")%></td>
                                    </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    <%--<tr>
                                        <td colspan="5">Tổng điểm doanh số</td>
                                        <td><%= String.Format("{0:#,###}", totalPoint).Replace(",", ".")%></td>
                                    </tr>--%>
                                </tbody>
                            </table>
                        </div>
                        <!-- Paging -->
                        <div class="site-paging-wp">
                            <% if (PAGING.TotalPage > 1)
                                { %>
                            <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                                <% if (PAGING._Paging.Prev != 0)
                                    { %>
                                <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                                <% } %>
                                <asp:Repeater ID="RptPaging" runat="server">
                                    <ItemTemplate>
                                        <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                            <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>>
                                            <%# Eval("PageNum") %>
                                        </a>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                                    { %>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                                <% } %>
                            </asp:Panel>
                            <% } %>
                        </div>
                        <!-- End Paging -->
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />

                <!-- Hidden Field-->
                <asp:HiddenField ID="HDF_TypeStaff" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="HDF_Staff" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="HDF_ExcelType" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="HDF_ExportType" ClientIDMode="Static" runat="server" />
                <!-- END Hidden Field-->
            </div>
            <%-- END Listing --%>
        </div>

        <style>
            .be-report table.table-listing.table-listing-salary td
            {
                padding: 7px 5px !important;
            }
        </style>
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 190px !important;
                margin-top: 5px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }
        </style>
        <link href="/Assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" />
        <script src="/Assets/js/jquery.mCustomScrollbar.js"></script>
        <script type="text/ecmascript">
            jQuery(document).ready(function () {
                // Add active menu
                $("#glbAdminSales").addClass("active");
                $("#glbAdminReportSalary").addClass("active");
                $("li.be-report-li").addClass("active");

                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) { }
                });

                //============================
                // Bind Staff
                //============================
                BindStaffFilter();

            });

            function BindStaffFilter() {
                $(".eb-select").bind("focus", function () {
                    EBSelect_ShowBox($(this).parent().find(".eb-select-data"));
                });
                $(window).bind("click", function (e) {
                    console.log(e);
                    if ((!e.target.className.match("eb-select") && e.target.parentElement.className.match("ul-listing-staff")) ||
                        (!e.target.className.match("eb-select") && !e.target.parentElement.className.match("ul-listing-staff"))
                    && !e.target.className.match("mCSB_dragger_bar")) {
                        EBSelect_HideBox();
                    }
                });
                //============================
                // Scroll Staff Filter
                //============================
                $('.eb-select-data').each(function () {
                    if ($(this).height() > 230) {
                        $(this).mCustomScrollbar({
                            theme: "dark-2",
                            scrollInertia: 100
                        });
                    }
                });
            }

            function BindIdToHDF(THIS, id, HDF_DomId, Input_DomId) {
                var text = THIS.innerText.trim(),
                    HDF_Dom = document.getElementById(HDF_DomId),
                    InputText = document.getElementById(Input_DomId);
                HDF_Dom.value = id;
                InputText.value = text;
                InputText.setAttribute("data-value", id);
                ajaxGetStaffsByType(id);
                if (HDF_DomId == "HDF_TypeStaff") {
                    $("#StaffName").val("");
                    $("#HDF_Staff").val("");
                }
            }

            function ajaxGetStaffsByType(type) {
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/Report/TimeKeeping.aspx/Load_Staff_ByType",
                    data: '{type : "' + type + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        var mission = JSON.parse(response.d);
                        if (mission.success) {
                            var staffs = JSON.parse(mission.msg);
                            if (staffs.length > 0) {
                                var tmp = "";
                                $.each(staffs, function (i, v) {
                                    tmp += '<li data-code="' + v.Code + '" data-id="' + v.Id + '"' +
                                            'onclick="BindIdToHDF(this,' + v.Id + ',\'HDF_Staff\',\'StaffName\')">' +
                                            v.Fullname + '</li>';
                                });
                                $("#UlListStaff").empty().append(tmp);
                            }

                        } else {
                            var msg = "Không tìm thấy mã khách hàng trong hệ thống.";
                            showMsgSystem(msg, "warning");
                        }
                    },
                    failure: function (response) { alert(response.d); }
                });
            }

            function EBSelect_ShowBox(Dom) {
                EBSelect_HideBox();
                Dom.show();
            }

            function EBSelect_HideBox() {
                $(".eb-select-data").hide();
            }

            function EBSelect_BindData() {

            }

            function ReplaceZezoValue() {
                $("table.table-listing td").each(function () {
                    if ($(this).text().trim() == "0") {
                        $(this).text("-");
                    }
                });
            }


        </script>

    </asp:Panel>
</asp:Content>
