﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/TemplateMaster/SiteMaster.Master" CodeBehind="Report_KQKD_New.aspx.cs" Inherits="_30shine.GUI.BackEnd.Report.Report_KQKD_New" %>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
    <link href="/Assets/css/erp.30shine.com/fixStyleCommon.css" rel="stylesheet" />
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>

    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <%--<uc1:ReportMenu runat="server" id="ReportMenu" />--%>
        <script>


</script>
        <style>
            .table-wp {
                width: 100%;
                float: left;
                /*overflow-x: scroll;*/
            }

                .table-wp table th {
                    font-family: Roboto Condensed Bold;
                    font-weight: normal;
                }

            table tr td, table tr th {
                border: 1px solid black;
                border-collapse: collapse;
                padding: 5px;
                /*min-width: 100px;*/
                text-align: center;
            }

            table tr.active {
                background: #ffe400;
            }
        </style>

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Báo cáo &nbsp;&#187; </li>
                        <li class="be-report-li"><a href="javascript:void(0);"><i class="fa fa-th-large"></i>Kết quả kinh doanh</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing be-report" onload="abc()">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <!-- Filter -->
                <div class="row">
                    <div class="filter-item" style="margin-left: 0;">
                        <strong class="st-head" style="margin-right: 10px;"><i class="fa fa-clock-o"></i>Thời gian</strong>
                        <div class="datepicker-wp">
                            <asp:TextBox CssClass="txtDateTime st-head" ID="TxtDateTimeFrom" placeholder="Từ ngày"
                                ClientIDMode="Static" runat="server" autocomplete="off"></asp:TextBox>

                            <p class="st-head" style="margin-left: 10px; margin-right: 10px;">
                                <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                            </p>
                            <asp:TextBox CssClass="txtDateTime st-head" ID="TxtDateTimeTo" placeholder="Đến ngày"
                                ClientIDMode="Static" runat="server" autocomplete="off"></asp:TextBox>
                        </div>
                        <% if (Perm_ViewAllDate)
                            { %>
                        <%--<br />--%>
                        <% }
                            else
                            { %>
                        <style>
                            .datepicker-wp {
                                display: none;
                            }

                            .customer-listing .tag-wp {
                                padding-left: 0;
                                padding-top: 15px;
                            }
                        </style>
                        <% } %>
                        <p class="st-head btn-viewdata fix-style" data-type="2" onclick="viewDataByDate($(this))">Xem dữ liệu</p>
                        <a href="/admin/bao-cao/hoa-don.html" class="st-head btn-viewdata">Reset Filter</a>
                        <a href="javascript://" class="st-head btn-viewdata" onclick="ExportExcel($(this))">Xuất File Excel</a>
                        <div class="tag-wp">
                            <p class="tag tag-date" style="margin: 5px" data-time="<%=Day1 %>" data-type="1" onclick="viewDataByDate($(this))">Hôm kia</p>
                            <p class="tag tag-date" style="margin: 5px" data-time="<%=Day2 %>" data-type="1" onclick="viewDataByDate($(this))">Hôm qua</p>
                            <p class="tag tag-date" style="margin: 5px" data-time="<%=Day3 %>" data-type="0" onclick="viewDataByDate($(this))">Hôm nay</p>
                        </div>
                    </div>

                    <%--<asp:Panel ID="ViewData" CssClass="st-head btn-viewdata" Text="Xem dữ liệu"
                        ClientIDMode="Static" onclick="viewDataByDate($(this)" runat="server">
                        Xem dữ liệu
                    </asp:Panel>--%>

                    <div class="export-wp drop-down-list-wp" style="display: none;">
                        <asp:Panel ID="fff" CssClass="st-head btn-viewdata"
                            ClientIDMode="Static" runat="server" onclick="EntryExportExcel('all')">
                            Xuất File Excel
                        </asp:Panel>
                        <%--OnClick="Exc_ExportExcel" --%>
                        <ul class="ul-drop-down-list" style="display: none;">
                            <li onclick="EntryExportExcel('all')">Tất cả dữ liệu</li>
                            <li onclick="EntryExportExcel('current page')">Trang hiện tại</li>
                        </ul>
                    </div>
                    <%--<asp:Panel ID="Btn_ExportExcel" CssClass="st-head btn-viewdata"
                ClientIDMode="Static" onclick="excExcel('Export')" runat="server">Xuất File Excel</asp:Panel>--%>
                    <%--<asp:Panel ID="Btn_ImportExcel" CssClass="st-head btn-viewdata"
                ClientIDMode="Static" onclick="excExcel('Import')" runat="server">Nhập File Excel</asp:Panel>--%>
                </div>
                <!-- End Filter -->
                <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
                <asp:UpdatePanel ID="UPTotal" runat="server" ClientIDMode="Static" style="margin-top: 15px;">
                    <ContentTemplate>
                        <div class="row">
                            <strong class="st-head"><i class="fa fa-file-text"></i>TÓM TẮT KẾT QUẢ KINH DOANH THEO NGÀY</strong>

                        </div>
                        <div class="row">
                            <div class="col-xs-12 ctn-table">
                                <div class="table-wp" id="table-wp-bckqkd">
                                    <table class="table-report" id="table-bckqkd">
                                        <thead class="th-report">
                                            <tr>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 100px; */ text-align: center;" rowspan="2">Salon</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 100px; */ text-align: center;" rowspan="2">TỔNG LỢI NHUẬN SAU THUẾ</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 100px; */ text-align: center;" rowspan="2">XẾP HẠNG SALON</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 100px; */ text-align: center;" rowspan="2">LƯỢT KHÁCH</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 100px; */ text-align: center;" colspan="10">DOANH THU</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 100px; */ text-align: center;" rowspan="2">Thu nhập khác</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 100px; */ text-align: center; background-color: yellow !important" rowspan="2">GIÁ VỐN TRỰC TIẾP</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 100px; */ text-align: center;" rowspan="2">Giá vốn - dịch vụ  tiền lương dịch vụ</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 100px; */ text-align: center;" rowspan="2">Tỷ lệ Lương DV/ DT DV %</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 100px; */ text-align: center;" rowspan="2">Giá vốn - dịch vụ - tiền lương mỹ phẩm</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 100px; */ text-align: center;" rowspan="2">Lương BV + Checkout + QL</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 100px; */ text-align: center;" rowspan="2">Gía vốn - MP bán</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 100px; */ text-align: center;" rowspan="2">Gía vốn bù trừ - MP bán</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 100px; */ text-align: center;" rowspan="2">Lương thưởng doanh số KCS</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 100px; */ text-align: center;" rowspan="2">Giá vốn - dịch vụ - mỹ phẩm, CCDC sử dụng - Khăn</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 100px; */ text-align: center; background-color: yellow !important" rowspan="2">CHI PHÍ SALON</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 100px; */ text-align: center;" rowspan="2">Thuê cửa hàng đã bao gồm chi phí thuế</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 100px; */ text-align: center;" rowspan="2">Phân bổ đầu tư khấu hao, chi phí trả trước</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 100px; */ text-align: center;" rowspan="2">CP quảng cáo</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 100px; */ text-align: center;" rowspan="2">Chi phí SMS ( nhắn booking)</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 100px; */ text-align: center;" rowspan="2">Chi phí vận chuyển TQ</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 100px; */ text-align: center;" rowspan="2">THUẾ GTGT</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 100px; */ text-align: center;" rowspan="2">TNDN</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 100px; */ text-align: center;" rowspan="2">Tiền điện. nước. </th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 100px; */ text-align: center;" rowspan="2">Chi phí vận hành phát sinh hàng ngày (Salon)</th>
                                                <%-- <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 100px; */ text-align: center;" colspan="7">Chi phí tiền lương BPC</th>--%>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 100px; */ text-align: center; background-color: yellow !important;" rowspan="2">CHI PHÍ QUẢN LÝ </th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 100px; */ text-align: center;" rowspan="2">Cước điện thoại. internet</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 100px; */ text-align: center;" rowspan="2">Tiền BHXH, KPCĐ</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 100px; */ text-align: center;" rowspan="2">Chi phí thuê văn phòng Thái Hà + Phí dịch vụ</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 100px; */ text-align: center;" rowspan="2">Lương cơ bản</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 100px; */ text-align: center;" rowspan="2">Lương thưởng khác</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 100px; */ text-align: center;" rowspan="2">CHI PHÍ LƯƠNG IT</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 100px; */ text-align: center;" rowspan="2">Tiền BHXH BPC</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 100px; */ text-align: center;" rowspan="2">Chi phí vận hành phát sinh hàng ngày (BPC)</th>
                                            </tr>
                                            <tr>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 100px; */ text-align: center;">Doanh thu thuần</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 100px; */ text-align: center;">Tổng doanh thu</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 100px; */ text-align: center;">Dịch vụ</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 100px; */ text-align: center;">Mỹ phẩm</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 100px; */ text-align: center;">DT Shine MB</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 100px; */ text-align: center;">DT Shine MB phân bổ trong ngày</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 100px; */ text-align: center;">DT Shine MB phân bổ trong kỳ kế toán</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 100px; */ text-align: center;">Tổng DT/ khách</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 100px; */ text-align: center;">Tổng DTDV/ khách</th>
                                                <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; /*min-width: 100px; */ text-align: center;">Tổng DT MP/ khách</th>


                                            </tr>
                                        </thead>
                                        <tbody class="tbody-report" id="tbodyBCKQKD">

                                            <%--<tr>
                                                <td>Salon</td>
                                                <td>TỔNG LỢI NHUẬN SAU THUẾ"</td>
                                                <td>LƯỢT KHÁCH</td>
                                                <td>Tổng doanh thu</td>
                                                <td>Dịch vụ</td>
                                                <td>Mỹ phẩm</td>
                                                <td>Tổng DT/ khách</td>
                                                <td>" Tổng DTDV/ khách"</td>
                                                <td>" Tổng DT MP/ khách"</td>
                                                <td>"GIÁ VỐN TRỰC TIẾP"</td>
                                                <td>"Giá vốn - dịch vụ  tiền lương dịch vụ"</td>
                                                <td>tỷ lệ Lương DV/ DT DV</td>
                                                <td>Giá vốn - dịch vụ - tiền lương mỹ phẩm</td>
                                                <td>Gía vốn - MP bán</td>
                                                <td>Lương thưởng doanh số KCS</td>
                                                <td>Giá vốn - dịch vụ - mỹ phẩm, CCDC sử dụng - Khăn</td>
                                                <td>Tiền điện. nước. </td>
                                                <td>CHI PHÍ SALON</td>
                                                <td>Thuê cửa hàng đã bao gồm chi phí thuế</td>
                                                <td>Phân bổ đầu tư khấu hao, chi phí trả trước</td>
                                                <td>CP quảng cáo</td>
                                                <td>Chi phí SMS ( nhắn booking)</td>
                                                <td>Chi phí vận chuyển TQ</td>
                                                <td>Cước điện thoại. internet</td>
                                                <td>Tiền BHXH, KPCĐ</td>
                                                <td>THUẾ GTGT</td>
                                                <td>TNDN</td>
                                                <td>"Chi phí vận hành phát sinh hàng ngày"</td>
                                                <td>CHI PHÍ QUẢN LÝ </td>
                                                <td>Chi phí thuê văn phòng Thái Hà + Phí dịch vụ</td>
                                                <td>Lương cơ bản</td>
                                                <td>Lương KPI/OKR</td>
                                                <td>CHI PHÍ LƯƠNG IT</td>
                                                <td>Tiền BHXH BPC</td>
                                                <td>Chi phí vận hành phát sinh hàng ngày</td>
                                            </tr>--%>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-xs-6">
                            </div>
                        </div>
                        <br />
                        <div class="row form-group" id="divNote" runat="server">
                            <div class="col-xs-1">Ghi chú: </div>
                            <div class="col-xs-4">
                                <%--<textarea class="form-control" id="Note" rows="3" disabled><%=textNote %></textarea>--%>
                            </div>
                        </div>
                    </ContentTemplate>
                    <%--   <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                    </Triggers>--%>
                </asp:UpdatePanel>

                <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="IsPaging" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_ExcelType" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_EXCELPage" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_EXCELSegment" />
            </div>
            <%-- END Listing --%>
        </div>
        <!-- Page loading -->
        <div class="page-loading">
            <p>Vui lòng đợi trong giây lát...</p>
        </div>

        <script type="text/javascript">
            jQuery(document).ready(function () {
                // Add active menu
                //$("#glbAdminSales").addClass("active");
                //$("#glbAdminKetQuaKinhDoanh").addClass("active");
                //$("li.be-report-li").addClass("active");

                // Price format
                UP_FormatPrice('.be-report-price');

                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) {
                        //$input.val($input.val().split(' ')[0]);
                    }
                });

                // View data today
                //$(".tag-date-today").click();
                $('tbody.tbody-report').scroll(function (e) {
                    $('thead.th-report').css("left", -$("tbody").scrollLeft());
                    $('thead.th-report tr:nth-child(1) th:nth-child(1)').css("left", $("tbody").scrollLeft());
                    $('tbody.tbody-report td:nth-child(1)').css({ "left": $("tbody").scrollLeft() }); //,  "display" : "block", "border-bottom" : "none"
                });
            });

            //function SubTableFixHeight() {
            //    $("tbody table.sub-table").each(function () {
            //        $(this).height($(this).parent().height() + 1);
            //    });
            //}
            //============================

        </script>

    </asp:Panel>
    <script src="/Assets/js/config.js?32121"></script>
    <script src="/Assets/js/erp.30shine.com/jsReport/jquery.Report.js?3213223123"></script>
</asp:Content>
