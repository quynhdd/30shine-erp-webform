﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using DocumentFormat.OpenXml.CustomProperties;
using ExportToExcel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.EntityClient;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.Report
{
    public partial class ExportExcell : System.Web.UI.Page
    {
        private Solution_30shineEntities db = new Solution_30shineEntities();
        CultureInfo culture = new CultureInfo("vi-VN");
        protected int a = 0;
        private bool Perm_Access = false;
        private bool Perm_Export = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                BindScript();
                SqlQuery.Enabled = false;
            }
        }

        /// <summary>
        /// Set permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = true;// permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Export = true;// permissionModel.CheckPermisionByAction("Perm_Export", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        /// <summary>
        /// Excute
        /// </summary>
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
            if (Session["User_Name"].ToString() == "Root")
            {
                PanelAdmin.Visible = true;
            }
            else PanelAdmin.Visible = false;
        }

        /// <summary>
        /// bill script to dropdown
        /// </summary>
        protected void BindScript()
        {
            var listScript = db.ScriptDatas.Where(w => w.IsDelete == false || w.IsDelete == null).OrderByDescending(o => o.Name).ToList();


            var Key = 0;

            Script.DataTextField = "Name";
            Script.DataValueField = "Id";

            ListItem item = new ListItem("Chọn câu lệnh", "0");
            Script.Items.Insert(0, item);

            foreach (var v in listScript)
            {
                Key++;
                item = new ListItem(v.Name, v.Id.ToString());
                Script.Items.Insert(Key, item);
            }
        }

        /// <summary>
        /// export excell from gridview 
        /// </summary>
        private void ExportListFromTable()
        {
            //var fileName = (FileName.Text == null || FileName.Text == "") ? "ExcelExport.xlsx" : ConvertString(FileName.Text.Replace(" ", "_")) + ".xlsx";
            var fileName = (Script.SelectedItem.Text == null || Script.SelectedItem.Text == "") ? "ExcelExport.xlsx" : ConvertString(Script.SelectedItem.Text.Replace(" ", "_")) + ".xlsx";
            var sql = @"" + SqlQuery.Text + "";
            string strConnection = ConfigurationManager.ConnectionStrings["SQLConnectionStringBI"].ConnectionString;
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            SqlConnection connection;
            SqlDataReader dataReader;
            SqlCommand command;
            connection = new SqlConnection(strConnection);
            builder.ConnectionString = strConnection;
            string UserID = builder["User ID"] as string;
            if (UserID == "bi")
            {
                try
                {
                    connection.Open();
                    command = new SqlCommand(sql, connection);
                    dataReader = command.ExecuteReader();
                    var columns = new List<string>();
                    var listRow = new List<List<string>>();
                    for (int i = 0; i < dataReader.FieldCount; i++)
                    {
                        columns.Add(dataReader.GetName(i));
                    }
                    while (dataReader.Read())
                    {
                        var row = new List<string>();
                        foreach (var item in columns)
                        {
                            var a = dataReader[item].ToString();
                            row.Add(a);
                        }
                        listRow.Add(row);
                    }
                    var ExcelStorePath = Server.MapPath("~") + "Public/Excel/Hoa.Don/";
                    var UrlFileName = "http://" + Request.Url.Host + "/Public/Excel/Hoa.Don/" + fileName;
                    ExportXcel(columns, listRow, ExcelStorePath + fileName);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Download Excel", "CallBackExportExcel('" + UrlFileName + "')", true);
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            else Label1.Text = "Tài khoản truy cập cơ sở dữ liệu không chính xác. Vui lòng kiểm tra lại chuỗi connectionString";
        }

        /// <summary>
        /// Export Excel
        /// </summary>
        /// <param name="_ExcelHeadRow"></param>
        /// <param name="List"></param>
        /// <param name="Path"></param>
        private static void ExportXcel(List<string> _ExcelHeadRow, List<List<string>> List, string Path)
        {
            try
            {
                CreateExcelFile.ExcelHeadRow = _ExcelHeadRow;
                CreateExcelFile.CreateExcel(List, Path);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi. Không tạo được file excel.\r\nException: " + ex.Message);
            }
        }

        /// <summary>
        /// upload file 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void UploadFile(object sender, EventArgs e)
        {
            string path = Server.MapPath("~/ScriptData/") + Path.GetFileName(FileUpload.FileName);
            if (File.Exists(path))
            {
                if (Path.GetExtension(FileUpload.FileName) != ".sql" || Path.GetFileName(FileUpload.FileName) == null)
                {
                    lblMessage.Text = "Sai định dạng file. Vui lòng kiểm tra lại";
                }
                else
                {
                    foreach (HttpPostedFile uploadedFile in FileUpload.PostedFiles)
                    {
                        uploadedFile.SaveAs(System.IO.Path.Combine(Server.MapPath("~/ScriptData/"), uploadedFile.FileName));
                        lblMessage.Text += String.Format("{0} uploaded! ", uploadedFile.FileName);
                        var checkExist = db.ScriptDatas.Where(w => w.Name == uploadedFile.FileName).FirstOrDefault();
                        if (checkExist == null)
                        {
                            ScriptData script = new ScriptData();
                            script.Name = Path.GetFileName(uploadedFile.FileName);
                            script.CreatedDate = DateTime.Now;
                            script.File = Server.MapPath("~/ScriptData/") + Path.GetFileName(uploadedFile.FileName);
                            db.ScriptDatas.Add(script);

                        }

                    }
                    db.SaveChanges();
                }
            }
            else
            {
                if (Path.GetExtension(FileUpload.FileName) != ".sql" || Path.GetFileName(FileUpload.FileName) == null)
                {
                    lblMessage.Text = "Sai định dạng file. Vui lòng kiểm tra lại";
                }
                else
                {
                    foreach (HttpPostedFile uploadedFile in FileUpload.PostedFiles)
                    {
                        uploadedFile.SaveAs(System.IO.Path.Combine(Server.MapPath("~/ScriptData/"), uploadedFile.FileName));
                        lblMessage.Text += String.Format("{0} uploaded! ", uploadedFile.FileName);
                        var checkExist = db.ScriptDatas.Where(w => w.Name == uploadedFile.FileName).FirstOrDefault();
                        if (checkExist == null)
                        {
                            ScriptData script = new ScriptData();
                            script.Name = Path.GetFileName(uploadedFile.FileName);
                            script.CreatedDate = DateTime.Now;
                            script.File = Server.MapPath("~/ScriptData/") + Path.GetFileName(uploadedFile.FileName);
                            db.ScriptDatas.Add(script);

                        }
                    }
                    db.SaveChanges();
                }
            }
        }

        /// <summary>
        /// read file and export to sqlquery textbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ReadFile(object sender, EventArgs e)
        {
            if (Script.SelectedValue != "0")
            {
                string path = Server.MapPath("~/ScriptData/") + Script.SelectedItem.Text;

                string text = File.ReadAllText(@"" + path + "", Encoding.UTF8);
                SqlQuery.Text = text;
            }
        }

        /// <summary>
        /// export to excel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void _BtnClick(object sender, EventArgs e)
        {
            ExportListFromTable();
        }

        /// <summary>
        /// remove loading
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        /// <summary>
        /// Add loading
        /// </summary>
        public void AddLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "addLoading();", true);
        }

        /// <summary>
        /// vietnamese character
        /// </summary>
        private static readonly string[] VietnameseSigns = new string[]
        {

            "aAeEoOuUiIdDyY",

            "áàạảãâấầậẩẫăắằặẳẵ",

            "ÁÀẠẢÃÂẤẦẬẨẪĂẮẰẶẲẴ",

            "éèẹẻẽêếềệểễ",

            "ÉÈẸẺẼÊẾỀỆỂỄ",

            "óòọỏõôốồộổỗơớờợởỡ",

            "ÓÒỌỎÕÔỐỒỘỔỖƠỚỜỢỞỠ",

            "úùụủũưứừựửữ",

            "ÚÙỤỦŨƯỨỪỰỬỮ",

            "íìịỉĩ",

            "ÍÌỊỈĨ",

            "đ",

            "Đ",

            "ýỳỵỷỹ",

            "ÝỲỴỶỸ"
        };

        /// <summary>
        /// convert vietnamese to english
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        protected string ConvertString(string str)
        {
            for (int i = 1; i < VietnameseSigns.Length; i++)
            {
                for (int j = 0; j < VietnameseSigns[i].Length; j++)
                    str = str.Replace(VietnameseSigns[i][j], VietnameseSigns[0][i - 1]);
            }
            return str;
        }
    }
}