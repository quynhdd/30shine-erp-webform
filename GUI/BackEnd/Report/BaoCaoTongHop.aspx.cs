﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Globalization;
using System.Web.Script.Serialization;
using ExportToExcel;
using Excel;
using LinqKit;
using System.Net.Mail;
using System.Net;
using System.Data.SqlClient;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.Report
{
    public partial class BaoCaoTongHop : System.Web.UI.Page
    {
        private Solution_30shineEntities db;
        protected List<List<ProductBasic>> ProductList;
        private Expression<Func<BillService, bool>> Where = PredicateBuilder.True<BillService>();
        CultureInfo culture = new CultureInfo("vi-VN");

        private string PageID = "TEST_KQKD";
        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        protected bool Perm_ViewAllData = false;
        protected bool Perm_ViewAllDate = false;
        private bool Perm_Access = false;
        protected bool IsAccountant = false;

        protected string Day1 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-2));
        protected string Day2 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-1));
        protected string Day3 = string.Format("{0:dd/MM/yyyy}", DateTime.Now);

        protected string table = "";
        private DateTime timeFrom;
        private DateTime timeTo;
        private int salonId;
        protected string textNote;
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //Perm_ViewAllDate = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllDate", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_ViewAllDate = permissionModel.CheckPermisionByAction("Perm_ViewAllDate", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        public BaoCaoTongHop()
        {
            db = new Solution_30shineEntities();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //SetPermission();

            if (!IsPostBack)
            {
                TxtDateTimeFrom.Text = String.Format("{0:dd/MM/yyyy}", DateTime.Now);
                //genReportTable();
            }
            else
            {
                RemoveLoading();
            }
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            genReportTable();
        }

        /// <summary>
        /// Lấy full dữ liệu báo cáo kết quả kinh doanh
        /// </summary>
        /// <returns></returns>
        private List<cls_ketquakinhdoanh_itemsalon> getData_KetQuaKinhDoanh()
        {
            int temp;
            var data = new List<cls_ketquakinhdoanh_itemsalon>();
            var item = new cls_ketquakinhdoanh_itemsalon();
            var itemTotal = new cls_ketquakinhdoanh_itemsalon();

            cls_sta totalSta = new cls_sta();
            cls_sta totalStaMon = new cls_sta();
            cls_sta2 totalSta2 = new cls_sta2();
            cls_sta2 totalperBooking = new cls_sta2();
            cls_sta2 totalperImages = new cls_sta2();
            cls_skinner totalSinner = new cls_skinner();
            cls_rating rating = new cls_rating();
            cls_rating ratingTotal = new cls_rating();
            int days1 = 0;
            //try
            //{
            if (TxtDateTimeFrom.Text != "")
            {
                timeFrom = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                if (!timeFrom.Equals(null) && TxtDateTimeTo.Text != "")
                {
                    timeTo = Convert.ToDateTime(TxtDateTimeTo.Text, culture).AddDays(1);
                }
                else
                {
                    timeTo = timeFrom.AddDays(1);
                }
            }
            else
            {
                timeFrom = DateTime.Now;
                timeTo = timeFrom.AddDays(1);
            }
            days1 = int.TryParse((timeTo - timeFrom).Days.ToString(), out temp) ? temp : 0;


            var listSalon = getData_ListSalon();

            if (listSalon.Count > 0)
            {
                foreach (var it in listSalon)
                {
                    item = new cls_ketquakinhdoanh_itemsalon();
                    salonId = it.Id;
                    item.SalonId = it.Id;
                    item.SalonName = it.ShortName.ToUpper();
                    item.LuotKhach = getData_LuotKhach(false);
                    item.DV_ShineCombo100k = getData_DoanhSo_DichVu(53);
                    item.DV_KidCombo = getData_DoanhSo_DichVu(74);
                    item.DV_DuongProtein = getData_DoanhSo_DichVu(69);
                    item.DV_Uon250k = getData_DoanhSo_DichVu(16);
                    item.DV_Nhuom180k = getData_DoanhSo_DichVu(14);
                    item.DV_Tay100k = getData_DoanhSo_DichVu(17);
                    item.DV_Tay2Lan200k = getData_DoanhSo_DichVu(24);

                    item.DT_Salon = getData_DoanhThu_DichVu();
                    item.DT_MyPham = getData_DoanhThu_SanPham(false);
                    item.DT_Salon_MyPham = item.DT_Salon + item.DT_MyPham;

                    item.GV_NhanVien_HoaHong = getData_GV_LuongMyPhamNhanVienSalon();
                    item.GV_MyPhamXuatBan = getData_GV_MyPhamXuatBan(false);
                    item.GV_TienLuongNhanVien = getData_GV_LuongNhanVienSalon();
                    item.GV_MyPhamVatTu = getData_GV_MyPhamVatTu();

                    item.CP_BH_QL_Khac = getData_FlowImport(2);
                    item.CP_Marketing = getData_FlowImport(3);
                    //item.CP_Marketing = Convert.ToInt64(getData_MarketingCostBySalon(salonId));
                    item.CP_IT = getData_FlowImport(9);
                    item.CP_PhatSinh = getData_FlowImport(4);

                    item.LNG = item.DT_Salon_MyPham - (item.GV_MyPhamVatTu + item.GV_MyPhamXuatBan + item.GV_NhanVien_HoaHong + item.GV_TienLuongNhanVien + item.CP_BH_QL_Khac + item.CP_Marketing + item.CP_PhatSinh + item.CP_IT);

                    // Tổng
                    itemTotal.LuotKhach += item.LuotKhach;
                    itemTotal.DV_ShineCombo100k += item.DV_ShineCombo100k;
                    itemTotal.DV_KidCombo += item.DV_KidCombo;
                    itemTotal.DV_DuongProtein += item.DV_DuongProtein;
                    itemTotal.DV_Uon250k += item.DV_Uon250k;
                    itemTotal.DV_Nhuom180k += item.DV_Nhuom180k;
                    itemTotal.DV_Tay100k += item.DV_Tay100k;
                    itemTotal.DV_Tay2Lan200k += item.DV_Tay2Lan200k;

                    itemTotal.DT_Salon += item.DT_Salon;
                    itemTotal.DT_MyPham += item.DT_MyPham;
                    itemTotal.DT_Salon_MyPham += item.DT_Salon_MyPham;

                    itemTotal.GV_NhanVien_HoaHong += item.GV_NhanVien_HoaHong;
                    itemTotal.GV_MyPhamXuatBan += item.GV_MyPhamXuatBan;
                    itemTotal.GV_TienLuongNhanVien += item.GV_TienLuongNhanVien;
                    itemTotal.GV_MyPhamVatTu += item.GV_MyPhamVatTu;

                    itemTotal.CP_BH_QL_Khac += item.CP_BH_QL_Khac;
                    itemTotal.CP_Marketing += item.CP_Marketing;
                    //itemTotal.CP_Marketing = Convert.ToInt64(getData_MarketingCostBySalon(0));
                    itemTotal.CP_IT += item.CP_IT;
                    itemTotal.CP_PhatSinh += item.CP_PhatSinh;

                    itemTotal.LNG += item.LNG;

                    var bills = db.BillServices.Where(w => w.IsDelete != 1 && w.Pending != 1 && w.SalonId == salonId && w.CreatedDate >= timeFrom && w.CreatedDate < timeTo).ToList();
                    // Tổng bills
                    itemTotal.Total_Bills += bills.Count;
                    foreach (var v in bills)
                    {
                        temp = Convert.ToInt32(v.TotalMoney);
                        item.Total_Money += temp;
                        itemTotal.Total_Money += temp;
                        JavaScriptSerializer serializer = new JavaScriptSerializer();

                        // Product Salon
                        var ProductListThis = serializer.Deserialize<List<ProductBasic>>(v.ProductIds);
                        if (ProductListThis != null)
                        {
                            foreach (var v2 in ProductListThis)
                            {
                                temp = v2.Promotion != 0 ? (v2.Price * v2.Quantity - v2.Promotion) : (Convert.ToInt32(v2.Price * v2.Quantity) * (100 - v2.VoucherPercent) / 100);
                                item.Money_Products += temp;
                                item.Money_Products_Salon += temp;
                                itemTotal.Money_Products += temp;
                                itemTotal.Money_Products_Salon += temp;
                            }
                        }

                        // Service
                        var ServiceListThis = serializer.Deserialize<List<ProductBasic>>(v.ServiceIds);
                        if (ServiceListThis != null)
                        {
                            foreach (var v2 in ServiceListThis)
                            {
                                temp = Convert.ToInt32(v2.Price * v2.Quantity) * (100 - v2.VoucherPercent) / 100;
                                item.Money_Services += temp;
                                itemTotal.Money_Services += temp;
                            }
                        }
                        item.Bills_Service++;
                        itemTotal.Bills_Service++;

                        item.Total_Bills++;

                        /// Tính số lần rating
                        switch (v.Mark)
                        {
                            case 1:
                                item.rating_bad++;
                                itemTotal.rating_bad++;
                                item.rating_total++;
                                itemTotal.rating_total++;
                                break;
                            case 2:
                                item.rating_good++;
                                itemTotal.rating_good++;
                                item.rating_total++;
                                itemTotal.rating_total++;
                                break;
                            case 3:
                                item.rating_great++;
                                itemTotal.rating_great++;
                                item.rating_total++;
                                itemTotal.rating_total++;
                                break;
                            default: break;
                        }
                    }
                    item.PerRating = String.Format("{0:00.00}", item.rating_great / item.rating_total * 100).Replace('.', ',') + " - " + String.Format("{0:00.00}", item.rating_good / item.rating_total * 100).Replace('.', ',') + " - " + String.Format("{0:00.00}", 100 - ((item.rating_great / item.rating_total * 100) + (item.rating_good / item.rating_total * 100))).Replace('.', ',');

                    /// Chỉ số thống kê
                    /// 1. sta1 : Doanh thu dịch vụ từ stylist / số stylist (đối với mỗi salon và toàn bộ salon)
                    ///     số stylist = số stylist ngày 1 + số stylist ngày 2 + ... (theo khoảng thời gian)
                    /// 2. sta2 : Số bill dịch vụ / số stylist
                    /// 3. sta3 : % khách quay lại, bill khách cũ / tổng số bill
                    /// 4. sta4 : Doanh số DV TB/ngày trong kỳ (time2 - time1)
                    /// 5. sta5 : Doanh thu DV TB/ngày trong kỳ (time2 - time1)

                    /// Tính sta1, sta2

                    var staData = getData_DoanhSoDoanhThuDV_Stylist();
                    object[] parasSta2 =
                   {
                        new SqlParameter("@timeFrom",String.Format("{0:yyyy/MM/dd}", timeFrom)),
                        new SqlParameter("@timeTo",String.Format("{0:yyyy/MM/dd}", timeTo)),
                        new SqlParameter("@salonId",salonId)
                    };
                    var skinData = db.Database.SqlQuery<cls_skinner>("sp_BC_getData_DoanhSoDoanhThuDV_Stylist @timeFrom, @timeTo, @salonId", parasSta2).FirstOrDefault();

                    // Stylist đi làm (theo chấm công)

                    var stylistWork = getData_Staff_Work(1);
                    staData.stylist = stylistWork.Count;

                    if (staData != null)
                    {
                        totalSta.bills += staData.bills;
                        totalSta.money += staData.money;
                        totalSta.stylist += staData.stylist;

                        if (staData.stylist > 0)
                        {
                            item.DT_Stylist = (float)staData.money / staData.stylist;
                            item.DS_Stylist = (float)staData.bills / staData.stylist;
                            if (days1 > 0)
                                item.StylistWork = (float)staData.stylist / days1;
                        }
                    }

                    // Skinner đi làm (theo chấm công)
                    var skinnerWork = getData_Staff_Work(2);
                    skinData.skinner = skinnerWork.Count;

                    if (skinData != null)
                    {
                        totalSinner.skinner += skinData.skinner;
                        if (skinData.skinner > 0)
                        {
                            if (days1 > 0)
                                item.SkinnerWork = (float)skinData.skinner / days1;
                        }
                    }

                    /// Tính sta3 : tỷ lệ khách quay lại
                    var lstCustomerIds = getData_OlderCustomerByTime();

                    if (lstCustomerIds.Count > 0)
                    {
                        var strCustomerIds = "";
                        foreach (var ccode in lstCustomerIds)
                        {
                            strCustomerIds += ccode.CustomerId.ToString() + ",";
                        }
                        strCustomerIds = strCustomerIds.TrimEnd(',');

                        string CustomerIds = strCustomerIds;
                        var staData2 = getData_OlderCustomerTotal(CustomerIds);
                        if (staData2 != null)
                        {
                            totalSta2.old_bills += staData2.old_bills;
                            totalSta2.all_bills += staData2.all_bills;

                            if (staData2.all_bills > 0)
                            {
                                item.OlderCustomer = (float)staData2.old_bills / staData2.all_bills * 100;
                            }
                        }
                    }

                    //Tỷ lệ khách đặt lịch trước

                    var bookingData = getData_BookingPercent();
                    if (bookingData != null)
                    {
                        totalperBooking.old_bills += bookingData.old_bills;
                        totalperBooking.all_bills += bookingData.all_bills;

                        if (bookingData.all_bills > 0)
                        {
                            item.PerBooking = (float)bookingData.old_bills / bookingData.all_bills * 100;
                        }

                    }

                    //Tỷ lệ lưu lại ảnh

                    var imagesData = getData_SaveImagePercent();
                    if (imagesData != null)
                    {
                        totalperImages.old_bills += imagesData.old_bills;
                        totalperImages.all_bills += imagesData.all_bills;

                        if (imagesData.all_bills > 0)
                        {
                            item.PerImage = (float)imagesData.old_bills / imagesData.all_bills * 100;
                        }
                    }

                    /// Tính sta4, sta5
                    // tổng doanh số dịch vụ trong kỳ (tính trên tất cả bill )

                    var staData3 = getData_DoanhSoDoanhThuDV_TB();
                    if (staData3 != null)
                    {
                        totalStaMon.bills += staData3.bills;
                        totalStaMon.money += staData3.money;

                        if (days1 > 0)
                        {
                            item.DS_TB = (float)item.Bills_Service / days1;
                            item.DT_TB = (float)item.Money_Services / days1;
                        }
                    }
                    data.Add(item);
                }

                // Bộ phận S4M
                item = new cls_ketquakinhdoanh_itemsalon();
                salonId = 8; //  ID trong bảng KetQuaKinhDoanh_Salon
                item.SalonName = "S4M";
                item.GV_TienLuongNhanVien = getData_GV_LuongNhanVienS4M();
                item.DT_MyPham = getData_FlowImport(6, "s4m");
                item.CP_BH_QL_Khac = getData_FlowImport(2, "s4m");
                item.CP_Marketing = getData_FlowImport(3, "s4m");
                item.CP_IT = getData_FlowImport(9, "s4m");
                item.CP_PhatSinh = getData_FlowImport(4, "s4m");
                item.LNG = item.DT_Salon_MyPham - (item.GV_MyPhamVatTu + item.GV_MyPhamXuatBan + item.GV_NhanVien_HoaHong + item.GV_TienLuongNhanVien + item.CP_BH_QL_Khac + item.CP_Marketing + item.CP_PhatSinh + item.CP_IT);
                data.Add(item);

                // Tổng + Bộ phận S4M
                itemTotal.LuotKhach += item.LuotKhach;
                itemTotal.DV_ShineCombo100k += item.DV_ShineCombo100k;
                itemTotal.DV_KidCombo += item.DV_KidCombo;
                itemTotal.DV_DuongProtein += item.DV_DuongProtein;
                itemTotal.DV_Uon250k += item.DV_Uon250k;
                itemTotal.DV_Nhuom180k += item.DV_Nhuom180k;
                itemTotal.DV_Tay100k += item.DV_Tay100k;
                itemTotal.DV_Tay2Lan200k += item.DV_Tay2Lan200k;

                itemTotal.DT_Salon += item.DT_Salon;
                itemTotal.DT_MyPham += item.DT_MyPham;
                itemTotal.DT_Salon_MyPham += item.DT_Salon_MyPham;

                itemTotal.GV_NhanVien_HoaHong += item.GV_NhanVien_HoaHong;
                itemTotal.GV_MyPhamXuatBan += item.GV_MyPhamXuatBan;
                itemTotal.GV_TienLuongNhanVien += item.GV_TienLuongNhanVien;
                itemTotal.GV_MyPhamVatTu += item.GV_MyPhamVatTu;

                itemTotal.CP_BH_QL_Khac += item.CP_BH_QL_Khac;
                itemTotal.CP_Marketing += item.CP_Marketing;
                //itemTotal.CP_Marketing = Convert.ToInt64(getData_MarketingCostBySalon(0));
                itemTotal.CP_IT += item.CP_IT;
                itemTotal.CP_PhatSinh += item.CP_PhatSinh;

                itemTotal.LNG += item.LNG;
                if (totalSta.stylist > 0)
                {
                    itemTotal.DT_Stylist = (float)totalSta.money / totalSta.stylist;
                    itemTotal.DS_Stylist = (float)totalSta.bills / totalSta.stylist;
                    if (days1 > 0)
                        itemTotal.StylistWork = (float)totalSta.stylist / days1;
                }
                if (totalSinner.skinner > 0)
                {
                    if (days1 > 0)
                        itemTotal.SkinnerWork = (float)totalSinner.skinner / days1;
                }
                if (totalSta2.all_bills > 0)
                {
                    itemTotal.OlderCustomer = (float)totalSta2.old_bills / totalSta2.all_bills * 100;
                }
                if (totalperBooking.all_bills > 0)
                {
                    itemTotal.PerBooking = (float)totalperBooking.old_bills / totalperBooking.all_bills * 100;
                }
                if (totalperImages.all_bills > 0)
                {
                    itemTotal.PerImage = (float)totalperImages.old_bills / totalperImages.all_bills * 100;
                }
                if (days1 > 0)
                {
                    itemTotal.DS_TB = (float)itemTotal.Bills_Service / days1;
                    itemTotal.DT_TB = (float)itemTotal.Money_Services / days1;
                }
                itemTotal.PerRating = String.Format("{0:00.00}", itemTotal.rating_great / itemTotal.rating_total * 100).Replace('.', ',') + " - " + String.Format("{0:00.00}", itemTotal.rating_good / itemTotal.rating_total * 100).Replace('.', ',') + " - " + String.Format("{0:00.00}", 100 - ((itemTotal.rating_great / itemTotal.rating_total * 100) + (itemTotal.rating_good / itemTotal.rating_total * 100))).Replace('.', ',');

                itemTotal.SalonName = "TOÀN HỆ THỐNG";
                data.Insert(0, itemTotal);
            }

            //}
            //catch { }

            return data;
        }


        protected void SendMail(string ReceiveEmail, string mainBody, string note)
        {
            //Reading sender Email credential from web.config file
            string HostAdd = "smtp.gmail.com";
            string FromEmailid = "30shine.servicereport@gmail.com";
            string Pass = "ZE9pwjWgBUJG57lYXRVq";
            string ToEmail = ReceiveEmail;
            string body = "";
            body += "<p>Báo cáo tổng hợp ngày : " + TxtDateTimeFrom.Text + " </p>";
            //body += "<p>Báo cáo tổng hợp ngày : " + "7/7/2017" + " </p>"; // Trường hợp service lỗi gửi bù
            body += mainBody;
            body += "<p>Ghi chú :  " + note + "</p>";

            //creating the object of MailMessage
            MailMessage mailMessage = new MailMessage();
            mailMessage.From = new MailAddress(FromEmailid); //From Email Id
            mailMessage.Subject = "Báo cáo hàng ngày - 30Shine Solution"; //Subject of Email
            mailMessage.Body = body; //body or message of Email
            mailMessage.IsBodyHtml = true;
            //Adding Multiple recipient email id logic
            string[] Multi = ToEmail.Split(','); //spiliting input Email id string with comma(,)
            foreach (string Multiemailid in Multi)
            {
                mailMessage.To.Add(new MailAddress(Multiemailid)); //adding multi reciver's Email Id
            }
            SmtpClient smtp = new SmtpClient(); // creating object of smptpclient
            smtp.Host = HostAdd; //host of emailaddress for example smtp.gmail.com etc

            //network and security related credentials
            smtp.EnableSsl = true;
            NetworkCredential NetworkCred = new NetworkCredential();
            NetworkCred.UserName = mailMessage.From.Address;
            NetworkCred.Password = Pass;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = NetworkCred;
            smtp.Port = 587;
            smtp.Send(mailMessage); //sending Email
        }

        private List<Tbl_Salon> getData_ListSalon()
        {
            return db.Tbl_Salon.Where(w => w.IsDelete != 1 && w.Publish == true && w.IsSalonHoiQuan != true).OrderBy(w => w.Order).ToList();
        }

        /// <summary>
        /// Doanh thu doanh số theo Stylist
        /// </summary>
        /// <returns></returns>
        private cls_sta getData_DoanhSoDoanhThuDV_Stylist()
        {
            object[] paras =
                    {
                        new SqlParameter("@timeFrom",String.Format("{0:yyyy/MM/dd}", timeFrom)),
                        new SqlParameter("@timeTo",String.Format("{0:yyyy/MM/dd}", timeTo)),
                        new SqlParameter("@salonId",salonId)
                    };
            return db.Database.SqlQuery<cls_sta>("sp_BC_getData_DoanhSoDoanhThuDV_Stylist @timeFrom, @timeTo, @salonId", paras).FirstOrDefault();
        }

        /// <summary>
        /// nhân viên đi làm theo chấm công
        /// </summary>
        /// <returns></returns>
        private List<int> getData_Staff_Work(int staff_type)
        {
            object[] parasSt =
                    {
                        new SqlParameter("@timeFrom",String.Format("{0:yyyy/MM/dd}", timeFrom)),
                        new SqlParameter("@timeTo",String.Format("{0:yyyy/MM/dd}", timeTo.AddDays(-1))),
                        new SqlParameter("@salonId",salonId),
                        new SqlParameter("@staff_type", staff_type)
                    };
            return db.Database.SqlQuery<int>("sp_BC_getData_Staff_Work @timeFrom, @timeTo, @salonId, @staff_type", parasSt).ToList();
        }

        private List<cls_sta3> getData_OlderCustomerByTime()
        {
            object[] parasSta3 =
                     {
                        new SqlParameter("@timeFrom", String.Format("{0:yyyy/MM/dd}", timeFrom)),
                        new SqlParameter("@timeTo", String.Format("{0:yyyy/MM/dd}", timeTo)),
                        new SqlParameter("@salonId", salonId)
                    };
            return db.Database.SqlQuery<cls_sta3>("sp_BC_getData_OlderCustomerByTime @timeFrom, @timeTo, @salonId", parasSta3).ToList();
        }

        private cls_sta2 getData_OlderCustomerTotal(string strCustomerIds)
        {
            object[] parasOlderCus =
                        {
                            new SqlParameter("@timeFrom",String.Format("{0:yyyy/MM/dd}", timeFrom)),
                            new SqlParameter("@timeTo",String.Format("{0:yyyy/MM/dd}", timeTo)),
                            new SqlParameter("@salonId",salonId),
                            new SqlParameter("@CustomerIds", strCustomerIds)
                        };
            return db.Database.SqlQuery<cls_sta2>("sp_BC_getData_OlderCustomerTotal @timeFrom, @timeTo, @salonId, @CustomerIds", parasOlderCus).FirstOrDefault();
        }

        private cls_sta2 getData_BookingPercent()
        {
            object[] parasBooking =
                    {
                        new SqlParameter("@timeFrom",String.Format("{0:yyyy/MM/dd}", timeFrom)),
                        new SqlParameter("@timeTo",String.Format("{0:yyyy/MM/dd}", timeTo)),
                        new SqlParameter("@salonId",salonId)
                    };
            return db.Database.SqlQuery<cls_sta2>("sp_BC_getData_BookingPercent @timeFrom, @timeTo, @salonId", parasBooking).FirstOrDefault();
        }

        private cls_sta2 getData_SaveImagePercent()
        {
            object[] parasImage =
                   {
                        new SqlParameter("@timeFrom",String.Format("{0:yyyy/MM/dd}", timeFrom)),
                        new SqlParameter("@timeTo",String.Format("{0:yyyy/MM/dd}", timeTo)),
                        new SqlParameter("@salonId",salonId)
                    };
            return db.Database.SqlQuery<cls_sta2>("sp_BC_getData_SaveImagePercent @timeFrom, @timeTo, @salonId", parasImage).FirstOrDefault();
        }


        private cls_sta getData_DoanhSoDoanhThuDV_TB()
        {
            object[] parasSta =
                    {
                        new SqlParameter("@timeFrom",String.Format("{0:yyyy/MM/dd}", timeFrom)),
                        new SqlParameter("@timeTo",String.Format("{0:yyyy/MM/dd}", timeTo)),
                        new SqlParameter("@salonId",salonId)
                    };
            return db.Database.SqlQuery<cls_sta>("sp_BC_getData_DoanhSoDoanhThuDV_TB @timeFrom, @timeTo, @salonId", parasSta).FirstOrDefault();
        }

        /// <summary>
        /// Lượt khách/Đơn hàng
        /// </summary>
        /// <param name="salonId"></param>
        /// <param name="timeFrom"></param>
        /// <param name="timeTo"></param>
        /// <returns></returns>
        private long getData_LuotKhach(bool isOnline)
        {
            if (isOnline)
            {
                return db.BillServices.Count(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.IsDelete != 1 && w.IsOnline == true);
            }
            else
            {
                return db.BillServices.Count(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.IsDelete != 1 && w.Pending != 1 && w.SalonId == salonId && (w.ServiceIds != null && w.ServiceIds != ""));
            }
        }

        /// <summary>
        /// Doanh số dịch vụ
        /// Shine Combo 100k : ID 53
        /// Uốn 250k : ID 16
        /// Nhuộm 180k : ID 14
        /// Tẩy 100k : ID 17
        /// </summary>
        /// <param name="serviceId"></param>
        /// <returns></returns>
        private long getData_DoanhSo_DichVu(int serviceId)
        {
            object[] paras =
            {
                new SqlParameter("@timeFrom",String.Format("{0:yyyy/MM/dd}", timeFrom)),
                new SqlParameter("@timeTo",String.Format("{0:yyyy/MM/dd}", timeTo)),
                new SqlParameter("@salonId",salonId),
                new SqlParameter("@serviceId",serviceId)
            };
            long result = db.Database.SqlQuery<long>("sp_BC_getData_DoanhSo_DichVu @timeFrom, @timeTo, @salonId, @serviceId", paras).SingleOrDefault();
            return result;
        }

        /// <summary>
        /// DT - dịch vụ SALON
        /// Doanh thu dịch vụ
        /// </summary>
        /// <returns></returns>
        private long getData_DoanhThu_DichVu()
        {
            object[] paras =
            {
                new SqlParameter("@timeFrom",String.Format("{0:yyyy/MM/dd}", timeFrom)),
                new SqlParameter("@timeTo",String.Format("{0:yyyy/MM/dd}", timeTo)),
                new SqlParameter("@salonId",salonId)
            };
            long result = db.Database.SqlQuery<long>("sp_BC_getData_DoanhThu_DichVu @timeFrom, @timeTo, @salonId", paras).SingleOrDefault();
            return result;
        }

        /// <summary>
        /// DT - dịch vụ MỸ PHẨM
        /// Doanh thu sản phẩm (bao gồm cả đơn bán hàng online)
        /// Với hóa đơn online, cần làm rõ sẽ lấy ngày tạo bill (CreatedDate) hay ngày thanh toán (CompletedTime) làm mốc thời gian hiển thị
        /// </summary>
        /// <returns></returns>
        private long getData_DoanhThu_SanPham(bool isOnline)
        {
            object[] paras =
            {
                new SqlParameter("@timeFrom",String.Format("{0:yyyy/MM/dd}", timeFrom)),
                new SqlParameter("@timeTo",String.Format("{0:yyyy/MM/dd}", timeTo)),
                new SqlParameter("@salonId",salonId),
                new SqlParameter("@isOnline", isOnline)
            };
            long result = db.Database.SqlQuery<long>("sp_BC_getData_DoanhThu_SanPham @timeFrom, @timeTo, @salonId, @isOnline", paras).SingleOrDefault();
            return result;
        }

        /// <summary>
        /// GV - Mỹ phẩm xuất bán
        /// </summary>
        /// <returns></returns>
        private long getData_GV_MyPhamXuatBan(bool isOnline)
        {
            object[] paras =
            {
                new SqlParameter("@timeFrom",String.Format("{0:yyyy/MM/dd}", timeFrom)),
                new SqlParameter("@timeTo",String.Format("{0:yyyy/MM/dd}", timeTo)),
                new SqlParameter("@salonId",salonId),
                new SqlParameter("@isOnline", isOnline)
            };
            long result = db.Database.SqlQuery<long>("sp_BC_getData_GV_MyPhamXuatBan @timeFrom, @timeTo, @salonId, @isOnline", paras).SingleOrDefault();
            return result;
        }

        /// <summary>
        /// GV - Mỹ phẩm xuất dùng
        /// </summary>
        /// <returns></returns>
        private long getData_GV_MyPhamVatTu()
        {
            object[] paras =
           {
                new SqlParameter("@timeFrom",String.Format("{0:yyyy/MM/dd}", timeFrom)),
                new SqlParameter("@timeTo",String.Format("{0:yyyy/MM/dd}", timeTo)),
                new SqlParameter("@salonId",salonId)
            };
            long result = db.Database.SqlQuery<long>("sp_BC_getData_GV_MyPhamVatTu @timeFrom, @timeTo, @salonId", paras).SingleOrDefault();
            return result;
        }

        /// <summary>
        /// GV - Tiền lương nhân viên ( stylist, skinner, lễ tân, bảo vê, TSL) 
        /// </summary>
        /// <returns></returns>
        private long getData_GV_LuongNhanVienSalon()
        {
            object[] paras =
           {
                new SqlParameter("@timeFrom",String.Format("{0:yyyy/MM/dd}", timeFrom)),
                new SqlParameter("@timeTo",String.Format("{0:yyyy/MM/dd}", timeTo)),
                new SqlParameter("@salonId",salonId)
            };
            long result = db.Database.SqlQuery<long>("sp_BC_getData_GV_LuongNhanVienSalon @timeFrom, @timeTo, @salonId", paras).SingleOrDefault();
            return result;
        }

        /// <summary>
        /// GV - % nhân viên, % hoa hồng LAZADA, tiền ship hàng nội thành
        /// </summary>
        /// <returns></returns>
        private long getData_GV_LuongMyPhamNhanVienSalon()
        {
            object[] paras =
            {
                new SqlParameter("@timeFrom",String.Format("{0:yyyy/MM/dd}", timeFrom)),
                new SqlParameter("@timeTo",String.Format("{0:yyyy/MM/dd}", timeTo)),
                new SqlParameter("@salonId",salonId)
            };
            long result = db.Database.SqlQuery<long>("sp_BC_getData_GV_LuongMyPhamNhanVienSalon @timeFrom, @timeTo, @salonId", paras).SingleOrDefault();
            return result;
        }

        /// <summary>
        /// GV - Lương nhân viên Stylist4Men
        /// Bộ phận S4M, Type = 11
        /// </summary>
        /// <returns></returns>
        private long getData_GV_LuongNhanVienS4M()
        {
            object[] paras =
          {
                new SqlParameter("@timeFrom",String.Format("{0:yyyy/MM/dd}", timeFrom)),
                new SqlParameter("@timeTo",String.Format("{0:yyyy/MM/dd}", timeTo))
            };
            long result = db.Database.SqlQuery<long>("sp_BC_getData_GV_LuongNhanVienS4M @timeFrom, @timeTo", paras).SingleOrDefault();
            return result;
        }

        /// <summary>
        /// Lấy dữ liệu nhập
        /// </summary>
        /// <param name="itemId"></param>
        /// <returns></returns>
        private long getData_FlowImport(int itemId, string meta = null)
        {
            object[] paras =
            {
                new SqlParameter("@timeFrom",String.Format("{0:yyyy/MM/dd}", timeFrom)),
                new SqlParameter("@timeTo",String.Format("{0:yyyy/MM/dd}", timeTo)),
                new SqlParameter("@salonId",salonId),
                new SqlParameter("@meta",meta ?? Convert.DBNull),
                new SqlParameter("@KQKDItemId",itemId)

            };
            long result = db.Database.SqlQuery<long>("sp_BC_getData_FlowImport @timeFrom, @timeTo, @salonId, @meta, @KQKDItemId", paras).SingleOrDefault();
            return result;
        }

        /// <summary>
        /// Chi phí Marketing 
        /// </summary>
        /// <returns></returns>
        private double getData_MarketingCostBySalon(int idSalon)
        {
            object[] paras =
          {
                new SqlParameter("@TuNgay",String.Format("{0:yyyy/MM/dd}", timeFrom)),
                new SqlParameter("@DenNgay",String.Format("{0:yyyy/MM/dd}", timeTo.AddDays(-1))),
                new SqlParameter("@salonId",idSalon)
            };
            double result = db.Database.SqlQuery<double>("sp_BC_getData_MarketingCostBySalon @TuNgay, @DenNgay, @salonId", paras).SingleOrDefault();
            return result;
        }

        private void genReportTable()
        {
            var data = getData_KetQuaKinhDoanh();
            /// Generate table
            if (data != null)
            {
                var tr = "<tr><th style='border: 1px solid black; border-collapse: collapse; padding: 5px;' rowspan='2'></th><th style='border: 1px solid black; border-collapse: collapse; padding: 5px;' rowspan='2'>LNG</th><th style='border: 1px solid black; border-collapse: collapse; padding: 5px;' colspan='8'>DOANH SỐ</th><th colspan='3' style='border: 1px solid black; border-collapse: collapse; padding: 5px; font-family: Roboto Condensed Bold; font-weight: normal; text-align: center;'>DOANH THU</th><th colspan='8' style='border: 1px solid black; border-collapse: collapse; padding: 5px; font-family: Roboto Condensed Bold; font-weight: normal; text-align: center;'>CHI PHÍ</th><th colspan='10' style='border: 1px solid black; border-collapse: collapse; padding: 5px; font-family: Roboto Condensed Bold; font-weight: normal; text-align: center;'>VẬN HÀNH</th>";
                var tr1 = "<tr>";
                var tr2 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>Lượt khách</th>";
                var tr3 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>Shine Combo</th>";
                var tr27 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>Kid Combo</th>";
                var tr28 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>Dưỡng Protein</th>";
                var tr4 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>Uốn</th>";
                var tr5 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>Nhuộm</th>";
                var tr6 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>Tẩy</th>";
                var tr7 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>Tẩy 2 lần</th>";
                var tr8 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>Tổng</th>";
                var tr9 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>Dịch vụ</th>";
                var tr10 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>Mỹ phẩm</th>";
                var tr11 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>GV - lương MP</th>";
                var tr12 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>GV - Mỹ phẩm</th>";
                var tr13 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>GV - Lương cứng & Doanh số DV</th>";
                var tr14 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>GV - MP, vật tư DV</th>";
                var tr15 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>BH + QL + khác phân bổ </th>";
                var tr16 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>MKT (FB)</th>";
                //var tr17 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>IT hàng ngày </th>";
                var tr18 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>Phát sinh trong ngày</th>";

                var tr19 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>Stylist đi làm</th>";
                var tr20 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>Skinner đi làm</th>";
                //var tr21 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>Đánh giá chất lượng (%)</th>";
                var tr22 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>Tỷ lệ khách cũ (%)</th>";
                var tr23 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>Tỷ lệ khách đặt trước (%)</th>";
                var tr24 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>Tỷ lệ lưu lại ảnh (%)</th>";
                var tr25 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>Doanh số DV theo Stylist / Số Stylist</th>";
                var tr26 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>Doanh thu DV theo Stylist / Số Stylist</th>";
                //var tr27 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>Doanh số DV TB/ngày trong kỳ</th>";
                //var tr28 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>Doanh thu DV TB/ngày trong kỳ</th>";
                //var tr19 = "<th style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>LNG - Toàn 30 Shine</th>";

                tr26 += "</tr>";

                var result = "";
                foreach (var v in data)
                {
                    var row1 = "<tr><th style='border: 1px solid black; border-collapse: collapse; padding: 5px; font-family: Roboto Condensed Bold; font-weight: normal;'>" + v.SalonName + "</th>";
                    row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:#,###}", v.LNG).Replace(',', '.') + "</td>";
                    row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:#,###}", v.LuotKhach).Replace(',', '.') + "</td>";
                    row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:#,###}", v.DV_ShineCombo100k).Replace(',', '.') + "</td>";
                    row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:#,###}", v.DV_KidCombo).Replace(',', '.') + "</td>";
                    row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:#,###}", v.DV_DuongProtein).Replace(',', '.') + "</td>";
                    row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:#,###}", v.DV_Uon250k).Replace(',', '.') + "</td>";
                    row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:#,###}", v.DV_Nhuom180k).Replace(',', '.') + "</td>";
                    row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:#,###}", v.DV_Tay100k).Replace(',', '.') + "</td>";
                    row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:#,###}", v.DV_Tay2Lan200k).Replace(',', '.') + "</td>";
                    row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:#,###}", v.DT_Salon_MyPham).Replace(',', '.') + "</td>";
                    row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:#,###}", v.DT_Salon).Replace(',', '.') + "</td>";
                    row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:#,###}", v.DT_MyPham).Replace(',', '.') + "</td>";
                    row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:#,###}", v.GV_NhanVien_HoaHong).Replace(',', '.') + "</td>";
                    row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:#,###}", v.GV_MyPhamXuatBan).Replace(',', '.') + "</td>";
                    row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:#,###}", v.GV_TienLuongNhanVien).Replace(',', '.') + "</td>";
                    row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:#,###}", v.GV_MyPhamVatTu).Replace(',', '.') + "</td>";
                    row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:#,###}", v.CP_BH_QL_Khac).Replace(',', '.') + "</td>";
                    row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:#,###}", v.CP_Marketing).Replace(',', '.') + "</td>";
                    //row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:#,###}", v.CP_IT).Replace(',', '.') + "</td>";
                    row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:#,###}", v.CP_PhatSinh).Replace(',', '.') + "</td>";

                    row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:#,###}", v.StylistWork).Replace(',', '.') + "</td>";
                    row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:#,###}", v.SkinnerWork).Replace(',', '.') + "</td>";
                    //row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + v.PerRating + "</td>";
                    row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:00.00}", v.OlderCustomer).Replace('.', ',') + " (%)</td>";
                    row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:00.00}", v.PerBooking).Replace('.', ',') + " (%)</td>";
                    row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:00.00}", v.PerImage).Replace('.', ',') + " (%)</td>";
                    row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:00.00}", v.DS_Stylist).Replace(',', '.') + "</td>";
                    row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:#,###}", v.DT_Stylist).Replace(',', '.') + "</td>";
                    //row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:00.00}", v.DS_TB).Replace(',', '.') + "</td>";
                    //row1 += "<td style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>" + String.Format("{0:#,###}", v.DT_TB).Replace(',', '.') + "</td>";

                    row1 += "</tr>";
                    result += row1;
                }

                table += @"<table style='border: 1px solid black; border-collapse: collapse; padding: 5px;'>
                                    <thead>" + tr + tr1 + tr2 + tr3 + tr27 + tr28 + tr4 + tr5 + tr6 + tr7 + tr8 + tr9 + tr10 + tr11 + tr12 + tr13 + tr14 + tr15 + tr16 + /*tr17 +*/ tr18 + tr19 + tr20 + /*tr21 +*/ tr22 + tr23 + tr24 + tr25 + tr26 + /*tr27 + tr28 +*/ @"</thead>
                                    <tbody>" + result + @"</tbody>
                                </table>";
                if (TxtDateTimeFrom.Text == TxtDateTimeTo.Text || (TxtDateTimeFrom.Text != "" && TxtDateTimeTo.Text == ""))
                {
                    textNote = getData_NoteByDate(Convert.ToDateTime(TxtDateTimeFrom.Text, culture));
                }

                SendMail("doan.linh@hkphone.com.vn,hoang.nguyen@30shine.com,thanh.son@30shine.com,trong.do@30shine.com,cuong.le@30shine.com,quang.hung@30shine.com,hien.dothu@30shine.com,quan.ngo@30shine.com", table, textNote);
                //SendMail("vong.lv@hkphone.com.vn,quan.ngo@30shine.com,hien.dothu@30shine.com,ha.tran@30shine.com", table, textNote);
                //SendMail("helloimys.g6@gmail.com", table, textNote);
                //SendMail("quynh.dinh@30shine.com", table, textNote);
            }
            else
            {
                //
            }
        }

        /// <summary>
        /// Lấy dữ liệu ghi chú
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        private string getData_NoteByDate(DateTime date)
        {
            var note = db.KetQuaKinhDoanh_FlowImport.FirstOrDefault(w => w.IsDelete != true && w.ImportDate == date && w.KQKDSalonId == null && w.KQKDItemId == null);
            if (note != null)
            {
                return note.Note;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }



        private class cls_ketquakinhdoanh_itemsalon
        {
            public int SalonId { get; set; }
            public string SalonName { get; set; }
            public long LuotKhach { get; set; }
            public long DV_ShineCombo100k { get; set; }
            public long DV_KidCombo { get; set; }
            public long DV_DuongProtein { get; set; }
            public long DV_Uon250k { get; set; }
            public long DV_Nhuom180k { get; set; }
            public long DV_Tay100k { get; set; }
            public long DV_Tay2Lan200k { get; set; }
            public long DT_Salon_MyPham { get; set; }
            public long DT_Salon { get; set; }
            public long DT_MyPham { get; set; }
            public long GV_NhanVien_HoaHong { get; set; }
            public long GV_MyPhamXuatBan { get; set; }
            public long GV_TienLuongNhanVien { get; set; }
            public long GV_MyPhamVatTu { get; set; }
            public long CP_BH_QL_Khac { get; set; }
            public long CP_Marketing { get; set; }
            public long CP_IT { get; set; }
            public long CP_PhatSinh { get; set; }
            public long LNG { get; set; }
            public float StylistWork { get; set; }
            public float SkinnerWork { get; set; }
            public string PerRating { get; set; }
            public float OlderCustomer { get; set; }
            public float PerBooking { get; set; }
            public float PerImage { get; set; }
            public float DS_Stylist { get; set; }
            public float DT_Stylist { get; set; }
            public float DS_TB { get; set; }
            public float DT_TB { get; set; }
            public long Total_Bills { get; set; }
            public long Bills_Service { get; set; }
            public long Bills_Product { get; set; }
            public long Total_Money { get; set; }
            public long Money_Services { get; set; }
            public long Money_Products { get; set; }
            public long Money_Products_Salon { get; set; }
            public float rating_great { get; set; }
            public float rating_good { get; set; }
            public float rating_bad { get; set; }
            public float rating_total { get; set; }
        }
    }
}
