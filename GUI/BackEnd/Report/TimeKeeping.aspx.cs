﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Globalization;
using LinqKit;
using System.Linq.Expressions;
using ExportToExcel;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.Report
{
    public partial class TimeKeeping : System.Web.UI.Page
    {
        protected Paging PAGING = new Paging();

        private int _TypeStaff;
        private int _StaffId;

        protected bool Staff_All = false;
        protected bool Staff_Hairdresser = false;
        protected bool Staff_HairMassage = false;
        protected bool Staff_Singer = false;
        protected string THead = "";

        private Expression<Func<Staff, bool>> WhereStaff = PredicateBuilder.True<Staff>();
        public CultureInfo culture = new CultureInfo("vi-VN");

        private string PageID = "";
        private bool Perm_ViewAllData = false;
        private bool Perm_Access = false;
        protected void checkPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //string perName = Session["User_Permission"].ToString();
                //string url = Request.Url.AbsolutePath.ToString();
                //Solution_30shineEntities db = new Solution_30shineEntities();
                //var pem = (from m in db.Tbl_Permission_Map
                //           join mn in db.Tbl_Permission_Menu on m.mID equals mn.mID
                //           where m.pName == perName && mn.Url_Main.EndsWith(url) || mn.Url_Rewrite.EndsWith(url)
                //           select new { m.aID }
                //           ).FirstOrDefault();
                //if (pem != null)
                //{
                //    var pemArray = pem.aID.Split(',');
                //    if (Array.IndexOf(pemArray, "1") > -1)
                //    {
                //        Perm_Access = true;
                //        if (Array.IndexOf(pemArray, "2") > -1)
                //        {
                //            Perm_ViewAllData = true;
                //        }

                //    }
                //    else
                //    {
                //        ContentWrap.Visible = false;
                //        NotAllowAccess.Visible = true;
                //    }

                //}
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();

            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            checkPermission();

            if (!IsPostBack)
            {
                Library.Function.bindSalon(new List<DropDownList> { Salon }, Perm_ViewAllData);
                Bind_TypeStaff();
                Bind_All_ByDay();
            }
            else
            {
                GenWhere();    
                Exc_Filter();
            }
            RemoveLoading();
        }

        private void GenWhere()
        {
            _TypeStaff = HDF_TypeStaff.Value != "" ? Convert.ToInt32(HDF_TypeStaff.Value) : 0;
            _StaffId = HDF_Staff.Value != "" ? Convert.ToInt32(HDF_Staff.Value) : 0;

            if (_StaffId > 0)
            {
                WhereStaff = WhereStaff.And(w => w.Id == _StaffId);
            }
            if (_TypeStaff > 0)
            {
                WhereStaff = WhereStaff.And(w => w.Type == _TypeStaff);
            }
            WhereStaff = WhereStaff.And(w => w.IsDelete != 1);

            int SalonId = Convert.ToInt32(Salon.SelectedValue);
            if (!Perm_ViewAllData)
            {
                WhereStaff = WhereStaff.And(p => p.SalonId == SalonId);
            }
            else
            {
                if (SalonId > 0)
                {
                    WhereStaff = WhereStaff.And(p => p.SalonId == SalonId);
                }
                else
                {
                    WhereStaff = WhereStaff.And(p => p.SalonId > 0);
                }
            }

            WhereStaff = WhereStaff.And(w => w.Permission == "staff" || w.Permission == "reception");
        }
        

        public void Exc_Filter()
        {
            var LSTStaff = new List<Staff_TimeKeeping>();
            var kindView = KindView.Value != "" ? Convert.ToInt32(KindView.Value) : 0;
            if (kindView == 1)
            {
                THead = Get_Services();
                LSTStaff = Get_DataBy_Filter_Service(true);   
            }
            else if (kindView == 2)
            {
                THead = Get_Products();
                LSTStaff = Get_DataBy_Filter_Product(true);
            }

            RptTimeKeeping.DataSource = LSTStaff;
            RptTimeKeeping.DataBind();
        }

        private List<Staff_TimeKeeping> Get_DataBy_Filter_Service(bool paging)
        {
            using (var db = new Solution_30shineEntities())
            {
                _TypeStaff = HDF_TypeStaff.Value != "" ? Convert.ToInt32(HDF_TypeStaff.Value) : 0;
                _StaffId = HDF_Staff.Value != "" ? Convert.ToInt32(HDF_Staff.Value) : 0;

                CultureInfo culture = new CultureInfo("vi-VN");
                //var WhereStaff = PredicateBuilder.True<Staff>();
                var LST_Staff = new List<Staff_TimeKeeping>();
                var LST_Staff2 = new List<Staff_TimeKeeping>();                
                var OBJ_Staff_TimeKeeping = new Staff_TimeKeeping();
                var lstServiceStatistic = new List<ServiceStatistic>();
                var LST0 = new List<_30shine.MODEL.ENTITY.EDMX.Staff>();
                int index = -1;
                DateTime d1 = Convert.ToDateTime(TxtDateTimeFrom.Text.Trim(), culture);
                DateTime d2;
                if (TxtDateTimeTo.Text.Trim() != "")
                {
                    d2 = Convert.ToDateTime(TxtDateTimeTo.Text.Trim(), culture).AddDays(1);
                }
                else
                {
                    d2 = d1.AddDays(1);
                }
                string timeFrom = String.Format("{0:MM/dd/yyyy}", d1);
                string timeTo = String.Format("{0:MM/dd/yyyy}", d2);

                //var _Service = db.Services.Where(w => w.IsDelete != 1 && w.Publish == 1 && (w.IsFreeService != 1 || w.IsFreeService == null)).OrderByDescending(o => o.Id).ToList();
                string sql_service = @"select b.* 
                                    from
                                    (
                                    select ServiceId from FlowService
                                    where IsDelete != 1
                                    and (CreatedDate between '" + timeFrom + "' and '" + timeTo + "')" +
                                        @"group by ServiceId
                                    ) as a
                                    join Service as b
                                    on a.ServiceId = b.Id";
                var _Service = db.Services.SqlQuery(sql_service).ToList();
                
                // Get staff
                if (paging)
                {
                    LST0 = db.Staffs.AsExpandable().Where(WhereStaff).OrderBy(o => o.Id).Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                }
                else
                {
                    LST0 = db.Staffs.AsExpandable().Where(WhereStaff).OrderBy(o => o.Id).ToList();
                }

                // Time condition
                if (TxtDateTimeFrom.Text != "")
                {
                    DateTime _FromDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture).AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
                    DateTime _ToDate;
                    if (TxtDateTimeTo.Text != "")
                    {
                        _ToDate = Convert.ToDateTime(TxtDateTimeTo.Text, culture).AddDays(1);
                    }
                    else
                    {
                        _ToDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture).AddDays(1);
                    }

                    // Join BillService
                    var LST1 = new List<BillServiceJoin>();
                    LST1 = (from staff in LST0
                            from bill in db.BillServices
                                //join bill in db.BillServices on staff.Id equals bill.Staff_Hairdresser_Id
                            .Where(w => w.CreatedDate > _FromDate && w.CreatedDate < _ToDate && w.IsDelete != 1 && (w.Staff_Hairdresser_Id == staff.Id || w.Staff_HairMassage_Id == staff.Id))
                            //.DefaultIfEmpty()
                            select new BillServiceJoin
                            {
                                Id = staff.Id,
                                Fullname = staff.Fullname,
                                Type = Convert.ToInt32(staff.Type),
                                IdShowroom = Convert.ToInt32(staff.IdShowroom),
                                BillIsDelete = Convert.ToInt32(bill.IsDelete),
                                BillId = bill.Id,
                                BillCreatedDate = Convert.ToDateTime(bill.CreatedDate)
                            }).ToList();

                    // Join FlowService
                    var LST2 = LST1.Join(db.FlowServices,
                                       a => a.BillId,
                                       b => b.BillId,
                                       (a, b) => new
                                       {
                                           a.Id,
                                           a.Fullname,
                                           a.Type,
                                           a.IdShowroom,
                                           a.BillId,
                                           b.ServiceId,
                                           FlS_IsDelete = b.IsDelete
                                       }
                                   )
                                   .Where(w => w.FlS_IsDelete != 1)
                                   .GroupBy(g => g.Id)
                                   .Select(s => new
                                   {
                                       Id = s.First().Id,
                                       Fullname = s.First().Fullname,
                                       Type = Convert.ToInt32(s.First().Type),
                                       IdShowroom = Convert.ToInt32(s.First().IdShowroom),
                                       ServiceIds = s.Select(ss => new { ss.ServiceId }).ToList()
                                   })
                                   .ToList();

                    // Map data to staff
                    if (LST2.Count > 0)
                    {
                        foreach (var v in LST2)
                        {
                            var service = new ServiceStatistic();
                            var _ServiceTmp = new ServiceStatistic();
                            lstServiceStatistic = new List<ServiceStatistic>();
                            var strservice = "";

                            // Classify service (Phân loại dịch vụ)
                            if (v.ServiceIds.Count > 0)
                            {
                                foreach (var v2 in v.ServiceIds)
                                {
                                    service = new ServiceStatistic();
                                    index = lstServiceStatistic.FindIndex(fi => fi.ServiceId == v2.ServiceId);
                                    if (index == -1)
                                    {
                                        service.ServiceId = Convert.ToInt32(v2.ServiceId);
                                        service.ServiceTimes = 1;
                                        lstServiceStatistic.Add(service);
                                    }
                                    else
                                    {
                                        _ServiceTmp = new ServiceStatistic();
                                        service = lstServiceStatistic.Find(f => f.ServiceId == v2.ServiceId);
                                        _ServiceTmp.ServiceId = service.ServiceId;
                                        _ServiceTmp.ServiceTimes = service.ServiceTimes + 1;
                                        lstServiceStatistic[index] = _ServiceTmp;
                                    }
                                }
                                if (_Service.Count > 0)
                                {
                                    foreach (var v3 in _Service)
                                    {
                                        index = lstServiceStatistic.FindIndex(fi => fi.ServiceId == v3.Id);
                                        if (index == -1)
                                        {
                                            strservice += "<td>" + "-" + "</td>";
                                        }
                                        else
                                        {
                                            strservice += "<td>" + lstServiceStatistic[index].ServiceTimes + "</td>";
                                        }
                                    }
                                }
                            }

                            OBJ_Staff_TimeKeeping.Id = v.Id;
                            OBJ_Staff_TimeKeeping.Fullname = v.Fullname;
                            OBJ_Staff_TimeKeeping.Type = v.Type;
                            OBJ_Staff_TimeKeeping.IdShowroom = v.IdShowroom;
                            OBJ_Staff_TimeKeeping._ServiceStatistic = lstServiceStatistic;
                            OBJ_Staff_TimeKeeping.TotalService = v.ServiceIds.Count;
                            OBJ_Staff_TimeKeeping.StrDataService = strservice;

                            LST_Staff.Add(OBJ_Staff_TimeKeeping);
                        }
                    }

                    // Merge staff
                    if (LST0.Count > 0)
                    {
                        var strserviceDefault = "";
                        var loop = 0;
                        if (_Service.Count > 0)
                        {
                            foreach (var v3 in _Service)
                            {
                                strserviceDefault += "<td>" + "-" + "</td>";
                            }
                        }
                        foreach (var v in LST0)
                        {
                            index = LST_Staff.FindIndex(fi=>fi.Id == v.Id);
                            if (index == -1)
                            {
                                OBJ_Staff_TimeKeeping = new Staff_TimeKeeping();
                                OBJ_Staff_TimeKeeping.Id = v.Id;
                                OBJ_Staff_TimeKeeping.Fullname = v.Fullname;
                                OBJ_Staff_TimeKeeping.Type = Convert.ToInt32(v.Type);
                                OBJ_Staff_TimeKeeping.IdShowroom = Convert.ToInt32(v.IdShowroom);
                                OBJ_Staff_TimeKeeping.TotalService = 0;
                                OBJ_Staff_TimeKeeping.StrDataService = strserviceDefault;
                                LST_Staff2.Add(OBJ_Staff_TimeKeeping);
                            }
                            else
                            {
                                LST_Staff2.Add(LST_Staff[loop]);
                                loop++;
                            }
                        }
                    }

                }

                return LST_Staff2;
            }            
        }

        private List<Staff_TimeKeeping> Get_DataBy_Filter_Product(bool paging)
        {
            using (var db = new Solution_30shineEntities())
            {
                _TypeStaff = HDF_TypeStaff.Value != "" ? Convert.ToInt32(HDF_TypeStaff.Value) : 0;
                _StaffId = HDF_Staff.Value != "" ? Convert.ToInt32(HDF_Staff.Value) : 0;

                CultureInfo culture = new CultureInfo("vi-VN");
                //var WhereStaff = PredicateBuilder.True<Staff>();
                var LST_Staff = new List<Staff_TimeKeeping>();
                var LST_Staff2 = new List<Staff_TimeKeeping>();
                var OBJ_Staff_TimeKeeping = new Staff_TimeKeeping();
                var lstProductStatistic = new List<ProductStatistic>();
                var LST0 = new List<_30shine.MODEL.ENTITY.EDMX.Staff>();
                int index = -1;
                //var _Product = db.Products.Where(w=>w.IsDelete != 1 && w.Publish == 1).OrderByDescending(o => o.Id).ToList();
                DateTime d1 = Convert.ToDateTime(TxtDateTimeFrom.Text.Trim(), culture);
                DateTime d2;
                if (TxtDateTimeTo.Text.Trim() != "")
                {
                    d2 = Convert.ToDateTime(TxtDateTimeTo.Text.Trim(), culture).AddDays(1);
                }
                else
                {
                    d2 = d1.AddDays(1);
                }
                string timeFrom = String.Format("{0:MM/dd/yyyy}", d1);
                string timeTo = String.Format("{0:MM/dd/yyyy}", d2);

                string sql_product = @"select b.* 
                                    from
                                    (
                                    select ProductId from FlowProduct
                                    where IsDelete != 1
                                    and (CreatedDate between '" + timeFrom + "' and '" + timeTo + "')" +
                                        @"group by ProductId
                                    ) as a
                                    join Product as b
                                    on a.ProductId = b.Id";
                var _Product = db.Products.SqlQuery(sql_product).ToList();

                // Get staff
                if (paging)
                {
                    LST0 = db.Staffs.AsExpandable().Where(WhereStaff).OrderBy(o => o.Id).Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                }
                else
                {
                    LST0 = db.Staffs.AsExpandable().Where(WhereStaff).OrderBy(o => o.Id).ToList();
                }

                // Time condition
                if (TxtDateTimeFrom.Text != "")
                {
                    DateTime _FromDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture).AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
                    DateTime _ToDate;
                    if (TxtDateTimeTo.Text != "")
                    {
                        _ToDate = Convert.ToDateTime(TxtDateTimeTo.Text, culture).AddDays(1);
                    }
                    else
                    {
                        _ToDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture).AddDays(1);
                    }
                    // Join BillService
                    var LST1 = new List<BillServiceJoin>();
                    LST1 = (from staff in LST0
                            join bill in db.BillServices on staff.Id equals bill.SellerId
                            where (bill.CreatedDate > _FromDate && bill.CreatedDate < _ToDate && bill.IsDelete != 1) 
                            select new BillServiceJoin
                            {
                                Id = staff.Id,
                                Fullname = staff.Fullname,
                                Type = Convert.ToInt32(staff.Type),
                                IdShowroom = Convert.ToInt32(staff.IdShowroom),
                                BillIsDelete = Convert.ToInt32(bill.IsDelete),
                                BillId = bill.Id,
                                BillCreatedDate = Convert.ToDateTime(bill.CreatedDate)
                            }).ToList();
                    
                    // Join FlowProduct
                    var LST2 = LST1.Join(db.FlowProducts,
                                       a => a.BillId,
                                       b => b.BillId,
                                       (a, b) => new
                                       {
                                           a.Id,
                                           a.Fullname,
                                           a.Type,
                                           a.IdShowroom,
                                           a.BillId,
                                           b.ProductId
                                       }
                                   )
                                   .GroupBy(g => g.Id)
                                   .Select(s => new
                                   {
                                       Id = s.First().Id,
                                       Fullname = s.First().Fullname,
                                       Type = Convert.ToInt32(s.First().Type),
                                       IdShowroom = Convert.ToInt32(s.First().IdShowroom),
                                       ProductIds = s.Select(ss => new { ss.ProductId }).ToList()
                                   })
                                   .ToList();

                    // Map data to staff
                    if (LST2.Count > 0)
                    {
                        foreach (var v in LST2)
                        {
                            var product = new ProductStatistic();
                            var _ProductTmp = new ProductStatistic();
                            lstProductStatistic = new List<ProductStatistic>();
                            var strproduct = "";

                            // Classify service (Phân loại dịch vụ)
                            if (v.ProductIds.Count > 0)
                            {
                                foreach (var v2 in v.ProductIds)
                                {
                                    product = new ProductStatistic();
                                    index = lstProductStatistic.FindIndex(fi => fi.ProductId == v2.ProductId);
                                    if (index == -1)
                                    {
                                        product.ProductId = Convert.ToInt32(v2.ProductId);
                                        product.ProductTimes = 1;
                                        lstProductStatistic.Add(product);
                                    }
                                    else
                                    {
                                        _ProductTmp = new ProductStatistic();
                                        product = lstProductStatistic.Find(f => f.ProductId == v2.ProductId);
                                        _ProductTmp.ProductId = product.ProductId;
                                        _ProductTmp.ProductTimes = product.ProductTimes + 1;
                                        lstProductStatistic[index] = _ProductTmp;
                                    }
                                }
                                if (_Product.Count > 0)
                                {
                                    foreach (var v3 in _Product)
                                    {
                                        index = lstProductStatistic.FindIndex(fi => fi.ProductId == v3.Id);
                                        if (index == -1)
                                        {
                                            strproduct += "<td>" + "-" + "</td>";
                                        }
                                        else
                                        {
                                            strproduct += "<td>" + lstProductStatistic[index].ProductTimes + "</td>";
                                        }
                                    }
                                }
                            }

                            OBJ_Staff_TimeKeeping.Id = v.Id;
                            OBJ_Staff_TimeKeeping.Fullname = v.Fullname;
                            OBJ_Staff_TimeKeeping.Type = v.Type;
                            OBJ_Staff_TimeKeeping.IdShowroom = v.IdShowroom;
                            OBJ_Staff_TimeKeeping._ProductStatistic = lstProductStatistic;
                            OBJ_Staff_TimeKeeping.TotalService = v.ProductIds.Count;
                            OBJ_Staff_TimeKeeping.StrDataService = strproduct;

                            LST_Staff.Add(OBJ_Staff_TimeKeeping);
                        }
                    }

                    // Merge staff
                    if (LST0.Count > 0)
                    {
                        var strserviceDefault = "";
                        var loop = 0;
                        if (_Product.Count > 0)
                        {
                            foreach (var v3 in _Product)
                            {
                                strserviceDefault += "<td>" + "-" + "</td>";
                            }
                        }
                        foreach (var v in LST0)
                        {
                            index = LST_Staff.FindIndex(fi => fi.Id == v.Id);
                            if (index == -1)
                            {
                                OBJ_Staff_TimeKeeping = new Staff_TimeKeeping();
                                OBJ_Staff_TimeKeeping.Id = v.Id;
                                OBJ_Staff_TimeKeeping.Fullname = v.Fullname;
                                OBJ_Staff_TimeKeeping.Type = Convert.ToInt32(v.Type);
                                OBJ_Staff_TimeKeeping.IdShowroom = Convert.ToInt32(v.IdShowroom);
                                OBJ_Staff_TimeKeeping.TotalService = 0;
                                OBJ_Staff_TimeKeeping.StrDataService = strserviceDefault;
                                LST_Staff2.Add(OBJ_Staff_TimeKeeping);
                            }
                            else
                            {
                                LST_Staff2.Add(LST_Staff[loop]);
                                loop++;
                            }
                        }
                    }
                }

                return LST_Staff2;
            }
        }

        private void Bind_All_ByDay()
        {
            using (var db = new Solution_30shineEntities())
            {
                var LST = db.Staffs.AsExpandable().Where(WhereStaff).OrderBy(o => o.Id).ToList();
                Rpt_Staff.DataSource = LST;
                Rpt_Staff.DataBind();
            }
        }

        public string Get_Services()
        {
            using (var db = new Solution_30shineEntities())
            {
                var str = "";
                //var LST = db.Services.Where(w => w.IsDelete != 1 && w.Publish == 1 && (w.IsFreeService != 1 || w.IsFreeService == null)).OrderByDescending(w => w.Id).ToList();
                DateTime d1 = Convert.ToDateTime(TxtDateTimeFrom.Text.Trim(), culture);
                DateTime d2;
                if (TxtDateTimeTo.Text.Trim() != "")
                {
                    d2 = Convert.ToDateTime(TxtDateTimeTo.Text.Trim(), culture).AddDays(1);
                }
                else
                {
                    d2 = d1.AddDays(1);
                }
                string timeFrom = String.Format("{0:MM/dd/yyyy}", d1);
                string timeTo = String.Format("{0:MM/dd/yyyy}", d2);

                //var _Service = db.Services.Where(w => w.IsDelete != 1 && w.Publish == 1 && (w.IsFreeService != 1 || w.IsFreeService == null)).OrderByDescending(o => o.Id).ToList();
                string sql_service = @"select b.* 
                                    from
                                    (
                                    select ServiceId from FlowService
                                    where IsDelete != 1
                                    and (CreatedDate between '" + timeFrom + "' and '" + timeTo + "')" +
                                        @"group by ServiceId
                                    ) as a
                                    join Service as b
                                    on a.ServiceId = b.Id";
                var LST = db.Services.SqlQuery(sql_service).ToList();
                if (LST.Count > 0)
                {
                    foreach (var v in LST)
                    {
                        str += "<th>" + v.Name + "</th>";
                    }
                }
                return str;
            }
        }

        public string Get_Products()
        {
            using (var db = new Solution_30shineEntities())
            {
                var str = "";
                //var LST = db.Products.Where(w => w.IsDelete != 1 && w.Publish == 1).OrderByDescending(w => w.Id).ToList();
                DateTime d1 = Convert.ToDateTime(TxtDateTimeFrom.Text.Trim(), culture);
                DateTime d2;
                if (TxtDateTimeTo.Text.Trim() != "")
                {
                    d2 = Convert.ToDateTime(TxtDateTimeTo.Text.Trim(), culture).AddDays(1);
                }
                else
                {
                    d2 = d1.AddDays(1);
                }
                string timeFrom = String.Format("{0:MM/dd/yyyy}", d1);
                string timeTo = String.Format("{0:MM/dd/yyyy}", d2);

                string sql_product = @"select b.* 
                                    from
                                    (
                                    select ProductId from FlowProduct
                                    where IsDelete != 1
                                    and (CreatedDate between '" + timeFrom + "' and '" + timeTo + "')" +
                                        @"group by ProductId
                                    ) as a
                                    join Product as b
                                    on a.ProductId = b.Id";
                var LST = db.Products.SqlQuery(sql_product).ToList();
                if (LST.Count > 0)
                {
                    foreach (var v in LST)
                    {
                        str += "<th>" + v.Name + "</th>";
                    }
                }
                return str;
            }
        }
        
        public void Bind_TypeStaff()
        {
            using (var db = new Solution_30shineEntities())
            {
                var LST = db.Staff_Type.Where(w => w.IsDelete != 1 && w.Publish == true).OrderBy(o => o.Order).ToList();
                Rpt_StaffTye.DataSource = LST;
                Rpt_StaffTye.DataBind();
            }
        }
        
        [WebMethod]
        public static string Load_Staff_ByType( string type)
        {
            {
                var _Msg = new Msg();
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                var Type = Convert.ToInt32(type);
                using (var db = new Solution_30shineEntities())
                {
                    var WhereLoadStaff = PredicateBuilder.True<_30shine.MODEL.ENTITY.EDMX.Staff>();
                    WhereLoadStaff = WhereLoadStaff.And(w=>w.Type == Type);
                    if (HttpContext.Current.Session["SalonId"] != null)
                    {
                        int integer;
                        int Salonid = int.TryParse(HttpContext.Current.Session["SalonId"].ToString(), out integer) ? integer : 0;
                        if (Salonid > 0)
                        {
                            WhereLoadStaff = WhereLoadStaff.And(w => w.SalonId == Salonid);
                        }                        
                    }
                    var LST = db.Staffs.AsExpandable().Where(WhereLoadStaff).Select(s => new { s.Id, s.Fullname, s.Code}).ToList();

                    if (LST.Count > 0)
                    {
                        _Msg.success = true;
                        _Msg.msg = serializer.Serialize(LST);
                    }
                    else
                    {
                        _Msg.success = false;
                    }
                }
                return serializer.Serialize(_Msg);
            }
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            Bind_Paging();
            Exc_Filter();
            ScriptManager.RegisterStartupScript(this, GetType(), "Script", "SetActiveTableListing();ReplaceZezoValue();", true);
            RemoveLoading();
        }

        protected void Bind_Paging()
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? (!HDF_Page.Value.Equals("") ? Convert.ToInt32(HDF_Page.Value) : 1) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage() : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage()
        {
            using (var db = new Solution_30shineEntities())
            {
                _TypeStaff = HDF_TypeStaff.Value != "" ? Convert.ToInt32(HDF_TypeStaff.Value) : 0;
                _StaffId = HDF_Staff.Value != "" ? Convert.ToInt32(HDF_Staff.Value) : 0;

                CultureInfo culture = new CultureInfo("vi-VN");
                //var WhereStaff = PredicateBuilder.True<Staff>();
                int Count = 0;

                //if (_StaffId > 0)
                //{
                //    WhereStaff = WhereStaff.And(w => w.Id == _StaffId);
                //}
                //if (_TypeStaff > 0)
                //{
                //    WhereStaff = WhereStaff.And(w => w.Type == _TypeStaff);
                //}
                //WhereStaff = WhereStaff.And(w => w.IsDelete != 1);

                // Get staff
                if (TxtDateTimeFrom.Text != "")
                {
                    Count = db.Staffs.AsExpandable().Count(WhereStaff);
                }

                int TotalRow = Count - PAGING._TopNewsNum;
                int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
                return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
            }            
        }

        protected void Exc_ExportExcel(object sender, EventArgs e)
        {
            using (var db = new Solution_30shineEntities())
            {
                var _ExcelHeadRow = new List<string>();
                var serializer = new JavaScriptSerializer();
                var BillRowLST = new List<List<string>>();
                var BillRow = new List<string>();
                _ExcelHeadRow.Add("Tên nhân viên");

                var TotalTimes = 0;
                var Index = -1;
                var LSTStaff = new List<Staff_TimeKeeping>();
                var kindView = KindView.Value != "" ? Convert.ToInt32(KindView.Value) : 0;
                if (kindView == 1)
                {
                    // Listing service
                    var LST_Service = db.Services.Where(w => w.IsDelete != 1 && w.Publish == 1 && (w.IsFreeService != 1 || w.IsFreeService == null)).OrderByDescending(w => w.Id).ToList();
                    if (LST_Service.Count > 0)
                    {
                        foreach (var v in LST_Service)
                        {
                            _ExcelHeadRow.Add(v.Name);
                        }                        
                    }
                    _ExcelHeadRow.Add("Tổng số");

                    LSTStaff = Get_DataBy_Filter_Service(false);
                    if (LSTStaff.Count > 0)
                    {
                        foreach (var v in LSTStaff)
                        {
                            TotalTimes = 0;
                            BillRow = new List<string>();
                            BillRow.Add(v.Fullname);
                            foreach (var v2 in LST_Service)
                            {
                                if (v._ServiceStatistic != null && v._ServiceStatistic.Count > 0)
                                {
                                    Index = v._ServiceStatistic.FindIndex(fi => fi.ServiceId == v2.Id);
                                    if (Index != -1)
                                    {
                                        BillRow.Add(v._ServiceStatistic[Index].ServiceTimes.ToString());
                                        TotalTimes += v._ServiceStatistic[Index].ServiceTimes;
                                    }
                                    else
                                    {
                                        BillRow.Add("0");
                                    }
                                }
                                else
                                {
                                    BillRow.Add("0");
                                }    
                            }
                            
                            BillRow.Add(TotalTimes.ToString());
                            BillRowLST.Add(BillRow);
                        }
                    }                    
                }
                else if (kindView == 2)
                {
                    // Listing product
                    var LST_Product = db.Products.Where(w => w.IsDelete != 1 && w.Publish == 1).OrderByDescending(w => w.Id).ToList();
                    if (LST_Product.Count > 0)
                    {
                        foreach (var v in LST_Product)
                        {
                            _ExcelHeadRow.Add(v.Name);
                        }
                    }
                    _ExcelHeadRow.Add("Tổng số");

                    LSTStaff = Get_DataBy_Filter_Product(false);
                    if (LSTStaff.Count > 0)
                    {
                        foreach (var v in LSTStaff)
                        {
                            TotalTimes = 0;
                            BillRow = new List<string>();
                            BillRow.Add(v.Fullname);
                            foreach (var v2 in LST_Product)
                            {
                                if (v._ProductStatistic != null && v._ProductStatistic.Count > 0)
                                {
                                    Index = v._ProductStatistic.FindIndex(fi => fi.ProductId == v2.Id);
                                    if (Index != -1)
                                    {
                                        BillRow.Add(v._ProductStatistic[Index].ProductTimes.ToString());
                                        TotalTimes += v._ProductStatistic[Index].ProductTimes;
                                    }
                                    else
                                    {
                                        BillRow.Add("0");
                                    }
                                }
                                else
                                {
                                    BillRow.Add("0");
                                }
                            }
                            BillRow.Add(TotalTimes.ToString());
                            BillRowLST.Add(BillRow);
                        }
                    }
                }
                
                // export
                var ExcelStorePath = Server.MapPath("~") + "Public/Excel/Nhan.Vien/";
                var FileName = "Hoa_Don_" + string.Format("{0:dd_MM_yyyy_hh_mm_ss}", DateTime.Now) + ".xlsx";
                var UrlFileName = "http://" + Request.Url.Host + "/Public/Excel/Nhan.Vien/" + FileName;

                ExportXcel(_ExcelHeadRow, BillRowLST, ExcelStorePath + FileName);
                Bind_Paging();
                ScriptManager.RegisterStartupScript(BtnFakeExcel, this.GetType(), "Download Excel", "CallBackExportExcel('" + UrlFileName + "');UP_FormatPrice('.be-report-price'); SubTableFixHeight();SetActiveTableListing();", true);
            }
        }

        private static void ExportXcel(List<string> _ExcelHeadRow, List<List<string>> List, string Path)
        {
            try
            {
                CreateExcelFile.ExcelHeadRow = _ExcelHeadRow;
                CreateExcelFile.CreateExcel(List, Path);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi. Không tạo được file excel.\r\nException: " + ex.Message);
            }
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        /// <summary>
        /// Check permission
        /// </summary>
        private void CheckPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                string[] Allow = new string[] { "root", "admin", "salonmanager", "accountant" };
                string[] AllowViewAllDate = new string[] { "root", "admin", "accountant" };
                var Permission = Session["User_Permission"].ToString().Trim();
                if (Array.IndexOf(Allow, Permission) != -1)
                {
                    Perm_Access = true;
                }

                if (Array.IndexOf(AllowViewAllDate, Permission) != -1)
                {
                    Perm_ViewAllData = true;
                }
            }

            // Call execute function
            ExecuteByPermission();
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
    }
}