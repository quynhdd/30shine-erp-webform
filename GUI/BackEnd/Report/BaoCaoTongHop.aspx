﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="BaoCaoTongHop.aspx.cs" Inherits="_30shine.GUI.BackEnd.Report.BaoCaoTongHop" %>

<asp:Content runat="server" ContentPlaceHolderID="head">
    <title>Báo cáo tổng hợp theo ngày - Gửi email báo cáo</title>
    <script type="text/javascript">
            var date, hour, min, sec;

            var auto = setInterval(setClickViewData, 1000);

            function setClickViewData()
            {
                date = new Date();
                hour = date.getHours();
                min = date.getMinutes();
                sec = date.getSeconds();
                if (hour == 23 && min == 0 && sec == 0)
                {
                    $("#ViewData").click();
                    console.log(date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds());
                }                
            }
        </script>
</asp:Content>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">

    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>

    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <%--<uc1:ReportMenu runat="server" id="ReportMenu" />--%>

        <style>
            .table-wp table th { font-family: Roboto Condensed Bold; font-weight: normal; }
        </style>

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Báo cáo &nbsp;&#187; </li>
                        <li class="be-report-li"><a href="javascript:void(0);"><i class="fa fa-th-large"></i>Kết quả kinh doanh</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing be-report" onload="abc()">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <!-- Filter -->
                <div class="row">
                    <div class="filter-item" style="margin-left: 0;">
                        <strong class="st-head" style="margin-right: 10px;"><i class="fa fa-clock-o"></i>Thời gian</strong>
                        <div class="datepicker-wp">
                            <asp:TextBox CssClass="txtDateTime st-head" ID="TxtDateTimeFrom" placeholder="Từ ngày"
                                ClientIDMode="Static" runat="server"></asp:TextBox>

                            <strong class="st-head" style="margin-left: 10px; margin-right: 10px;">
                                <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                            </strong>
                            <asp:TextBox CssClass="txtDateTime st-head" ID="TxtDateTimeTo" placeholder="Đến ngày"
                                ClientIDMode="Static" runat="server"></asp:TextBox>
                        </div>
                        <% if (Perm_ViewAllDate)
                        { %>
                        <br />
                        <% }
                        else
                        { %>
                        <style>
                            .datepicker-wp { display: none; }
                            .customer-listing .tag-wp { padding-left: 0; padding-top: 15px; }
                        </style>
        <% } %>
        <div class="tag-wp" style="display:none;">
            <p class="tag tag-date" onclick="viewDataByDate($(this), '<%=Day1 %>')">Hôm kia <span class="txt-date"><%=Day1 %></span></p>
            <p class="tag tag-date" onclick="viewDataByDate($(this), '<%=Day2 %>')">Hôm qua<span class="txt-date"><%=Day2 %></span></p>
            <p class="tag tag-date tag-date-today" onclick="viewDataByDate($(this), '<%=Day3 %>')">Hôm nay<span class="txt-date"><%=Day3 %></span></p>
        </div>
        </div>

            <asp:Panel ID="ViewData" CssClass="st-head btn-viewdata" Text="Xem dữ liệu"
                ClientIDMode="Static" onclick="excPaging(1)" runat="server">
                Xem dữ liệu
        </asp:Panel>
                    <%-- excPaging(1) --%>
        <div class="export-wp drop-down-list-wp" style="display: none;">
            <asp:Panel ID="fff" CssClass="st-head btn-viewdata"
                ClientIDMode="Static" runat="server" onclick="EntryExportExcel('all')">
                Xuất File Excel
            </asp:Panel>
            <%--OnClick="Exc_ExportExcel" --%>
            <ul class="ul-drop-down-list" style="display: none;">
                <li onclick="EntryExportExcel('all')">Tất cả dữ liệu</li>
                <li onclick="EntryExportExcel('current page')">Trang hiện tại</li>
            </ul>
        </div>
        <%--<asp:Panel ID="Btn_ExportExcel" CssClass="st-head btn-viewdata"
                ClientIDMode="Static" onclick="excExcel('Export')" runat="server">Xuất File Excel</asp:Panel>--%>
        <%--<asp:Panel ID="Btn_ImportExcel" CssClass="st-head btn-viewdata"
                ClientIDMode="Static" onclick="excExcel('Import')" runat="server">Nhập File Excel</asp:Panel>--%>
        </div> 
        <!-- End Filter -->
        <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
        <asp:UpdatePanel ID="UPTotal" runat="server" ClientIDMode="Static" style="margin-top: 15px;">
            <ContentTemplate>
                <div class="row">
                    <strong class="st-head"><i class="fa fa-file-text"></i>TÓM TẮT KẾT QUẢ KINH DOANH THEO NGÀY</strong>

                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="table-wp">
                            <%=table %>
                        </div>
                    </div>
                    <div class="col-xs-6">
                    </div>
                </div>
                <br />
                
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>

        <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
        <asp:HiddenField runat="server" ClientIDMode="Static" ID="IsPaging" />
        <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
        <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_ExcelType" />
        <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_EXCELPage" />
        <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_EXCELSegment" />
        </div> 
    <%-- END Listing --%>
</div>

        <script type="text/javascript">
            jQuery(document).ready(function () {
                // Add active menu
                $("#glbAdminSales").addClass("active");
                $("#glbAdminKetQuaKinhDoanh").addClass("active");
                $("li.be-report-li").addClass("active");

                // Price format
                UP_FormatPrice('.be-report-price');

                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) {
                        //$input.val($input.val().split(' ')[0]);
                    }
                });

                // View data today
                //$(".tag-date-today").click();
            });

            function SubTableFixHeight() {
                $("tbody table.sub-table").each(function () {
                    $(this).height($(this).parent().height() + 1);
                });
            }

            function viewDataByDate(This, time) {
                $(".tag-date.active").removeClass("active");
                This.addClass("active");
                $("#TxtDateTimeTo").val("");
                $("#TxtDateTimeFrom").val(time);
                $("#ViewData").click();
            }

            //============================
            // Event delete
            //============================
            function del(This, code, name) {
                var code = code || null,
                    name = name || null,
                    Row = This;
                if (!code) return false;

                // show EBPopup
                $(".confirm-yn").openEBPopup();
                $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn xóa [ " + name + " ] ?");

                $("#EBPopup .yn-yes").bind("click", function () {
                    $.ajax({
                        type: "POST",
                        url: "/GUI/SystemService/Ajax/Del.aspx/Delele_Bill",
                        data: '{Code : "' + code + '"}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json", success: function (response) {
                            var mission = JSON.parse(response.d);
                            if (mission.success) {
                                delSuccess();
                                Row.remove();
                            } else {
                                delFailed();
                            }
                        },
                        failure: function (response) { alert(response.d); }
                    });
                });
                $("#EBPopup .yn-no").bind("click", function () {
                    autoCloseEBPopup(0);
                });
            }

            //var sDate = "26/5/2016";
            //var date = Date.parse(sDate, "yyyy-MM-dd");
            //console.log(date);

        </script>

    </asp:Panel>
</asp:Content>
