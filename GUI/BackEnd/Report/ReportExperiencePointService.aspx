﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/TemplateMaster/SiteMasterVersion1.Master" CodeBehind="ReportExperiencePointService.aspx.cs" Inherits="_30shine.GUI.BackEnd.Report.ReportExperiencePointService" %>

<asp:Content ID="CustomerAdd" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel ID="ContentWrap" runat="server" ClientIDMode="Static">

        <div class="container-fluid sub-menu">
            <div class="row">
                <div class="col-md-12 pt-3 pb-2">
                    <ul>
                        <li>
                            <label style="font-size: 16px">Báo cáo </label>
                            &nbsp;&#187;</li>
                        <li><i class="fas fa-list-alt mr-1 align-middle"></i><a class="align-middle" href="/admin/bao-cao/diem-trai-nghiem-dv.html">Trải nghiệm dịch vụ &nbsp;&#187;</a></li>
                        <li><i class="fas fa-list-alt mr-1 align-middle"></i><a class="align-middle" href="/admin/bao-cao/import-feedback-khach-hang.html">Import Feedback khách hàng</a></li>
                    </ul>
                </div>
                <div class="col-md-12">
                    <div class="sub-menu-boder"></div>
                </div>
            </div>
        </div>
        <form runat="server">
            <div class="container-fluid pt-2 form-group">
                <div class="row">
                    <div class="form-group col-auto">
                        <strong><i class="fa fa-filter"></i>Lọc kết quả &nbsp;&#187; </strong>
                    </div>

                    <div class="col-auto">
                        <div class="form-group search-content">
                            <input type="text" placeholder="Từ ngày" name="fromDate" class="datetime-picker form-control float-left w-auto" />
                            <i class="fa fa-arrow-circle-right float-left mt-2 mr-2 ml-2" style="color: #D0D6D8;"></i>
                            <input type="text" placeholder="Đến ngày" name="toDate" class="datetime-picker form-control float-left w-auto" />
                        </div>
                        <div class="form-group search-content">
                            <input type="text" placeholder="Từ ngày so sánh" name="fromDateCondition" class="datetime-picker form-control float-left w-auto" />
                            <i class="fa fa-arrow-circle-right float-left mt-2 mr-2 ml-2" style="color: #D0D6D8;"></i>
                            <input type="text" placeholder="Đến ngày so sánh" name="toDateCondition" class="datetime-picker form-control float-left w-auto" />
                        </div>
                    </div>
                    <div class="form-group col-auto">
                        <asp:DropDownList ID="ddlSalon" data-name="salonId" ClientIDMode="Static" CssClass="select form-control" runat="server" onchange="ChangeSalon(event)"></asp:DropDownList>
                    </div>
                    <div class="form-group col-auto region">
                        <select class="select form-control" data-name="regionId" onchange="ChangeRegion(event)"></select>
                    </div>
                    <div class="form-group col-auto">
                        <div id="ViewDataFilter" class="form-group col-auto pl-0" onclick="GetData()">
                            <div class="btn-viewdata">Xem dữ liệu</div>
                        </div>
                    </div>
                    <div class="col-md-12 col-xl-12">
                        <div class="sub-menu-boder-low"></div>
                    </div>
                </div>
            </div>
            <div class="container-fluid form-group">
                <table id="table-listing" class="table table-bordered selectable table-listing">
                    <thead>
                        <tr>
                            <th rowspan="2" class="align-middle text-center">ID Salon</th>
                            <th rowspan="2" class="align-middle text-center">Salon</th>
                            <th rowspan="2" class="align-middle text-center">ĐIỂM CHẤT LƯỢNG</th>
                            <th rowspan="2" class="align-middle text-center">Xếp hạng</th>
                            <th rowspan="2" class="align-middle text-center">Điểm CL kỳ trước</th>
                            <th rowspan="2" class="align-middle text-center">Xếp hạng kỳ trước</th>
                            <th rowspan="2" class="align-middle text-center">Rating TB</th>
                            <th rowspan="2" class="align-middle text-center">Lỗi GS</th>
                            <th colspan="3" class="align-middle text-center">Lỗi chất lượng cắt</th>
                            <th rowspan="2" class="align-middle text-center">Feedback</th>
                            <th rowspan="2" class="align-middle text-center">KH chờ > 15p</th>
                            <th rowspan="2" class="align-middle text-center">Lượt khách</th>
                        </tr>
                        <tr>
                            <th>Lỗi SCSC </th>
                            <th>Lỗi Uốn</th>
                            <th>Lỗi thiếu ảnh</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>

        </form>
        <style>
            .form-control {
                height: 31px !important;
                width: 150px !important;
            }

            .search-content {
                height: 31px;
            }

            table.table-bordered.dataTable th:last-child, table.table-bordered.dataTable th:last-child, table.table-bordered.dataTable td:last-child, table.table-bordered.dataTable td:last-child {
                border-right-width: 1px;
            }

            table tr[data-salon-id="0"] {
                font-weight: bold;
            }
        </style>
        <script>
            var table = null;
            var perm_ViewAllData = <%=Perm_ViewAllData.ToString().ToLower()%>;
            var domain = '<%=Libraries.AppConstants.URL_API_REPORTS%>';
            var staffId = <%=StaffId%>;
            $(document).ready(function () {
                BindRegion();
            })
            function ChangeSalon(event) {
                if (event.target.value != "0") {
                    $('select[data-name=regionId]').attr('disabled', 'disabled');
                }
                else {
                    $('select[data-name=regionId]').removeAttr('disabled');
                }
                $('select[data-name=regionId]').val(0);
                $('.select').select2();
            }
            function ChangeRegion(event) {
                if (event.target.value != "0") {
                    $('select[data-name=salonId]').attr('disabled', 'disabled');
                }
                else {
                    $('select[data-name=salonId]').removeAttr('disabled');
                }
                $('select[data-name=salonId]').val(0);
                $('.select').select2();
            }
            function GetData() {
                var fromDate, toDate, fromDateCondition, toDateCondition, salonId, salonRegionId;

                salonId = ~~$('#ddlSalon').val();
                salonRegionId = ~~$('select[data-name=regionId]').val();
                if (!perm_ViewAllData && salonId === 0 && salonRegionId === 0) {
                    ShowMessage('Lưu ý','Vui lòng chọn salon hoặc vùng',3,7000);
                    finishLoading();
                    return;
                }
                startLoading();
                fromDate = $('input[name=fromDate]').val();
                toDate = $('input[name=toDate]').val();
                fromDateCondition = $('input[name=fromDateCondition]').val();
                toDateCondition = $('input[name=toDateCondition]').val();
                //salonId = ~~$('select[data-name=salonId]').val();
                //salonRegionId = ~~$('select[data-name=regionId]').val();

                var querry = '?salonId=' + salonId + '&regionId=' + salonRegionId + '&fromDate=' + encodeURIComponent(fromDate) + '&toDate=' + encodeURIComponent(toDate);
                var querryCondition = '?salonId=' + salonId + '&regionId=' + salonRegionId + '&fromDate=' + encodeURIComponent(fromDateCondition) + '&toDate=' + encodeURIComponent(toDateCondition);
                AjaxGetData(querry, querryCondition);
            }

            function AjaxGetData(querry, querryCondition) {
                $.ajax({
                    type: "GET",
                    async: true,
                    url: domain + "/api/report-point-service/" + querry,
                    dataType: "json",
                    contentType: "applicate/json; charset=utf-8",
                    success: function (data, textStatus, jqXHR) {
                        AjaxGetDataCondition(querryCondition, data);
                    },
                    statusCode: {
                        404: function () {
                            ShowMessage("", "page not found", 4, 7000);
                        },
                        400: function () {
                            ShowMessage("", "Lỗi ngày không hợp lệ", 4, 7000);
                        },
                        500: function () {
                            ShowMessage("", "Đã có lỗi xảy ra", 4, 7000);
                        }

                    },
                    complete: function () {
                        finishLoading();
                    }
                });
            }
            function AjaxGetDataCondition(querryCondition, dataStandal) {
                var dataCondition = null;
                $.ajax({
                    type: "GET",
                    async: false,
                    url: domain + "/api/report-point-service" + querryCondition,
                    dataType: "json",
                    contentType: "applicate/json; charset=utf-8",
                    success: function (data, textStatus, jqXHR) {
                        dataCondition = data
                    },
                    complete: function () {
                        finishLoading();
                    }
                });
                BindData(dataStandal, dataCondition);
            }

            function BindData(dataStandal, dataCondition) {
                if (table) {
                    $('#table-listing').DataTable().clear();
                    $('#table-listing').DataTable().destroy();
                }
                if (dataStandal) {
                    var body = '';
                    var bodyFirst = "";
                    // Tính điểm chất lượng standal.
                    $(dataStandal).each(function (index, value) {
                        var pointQuanlity = value.ratingTB / 5 - (value.totalMonitorStaff + value.waitTime15 + (value.totalErorrScsc + value.totalErrorScscCurling) * 2 + value.errorImage * 0.5 + value.feedback * 20) / value.totalBill;
                        value.pointQuanlity = pointQuanlity;
                    });
                    // Sap xep diem chat luong standal.
                    dataStandal.sort(function (a, b) {
                        if (a.salonId == 0 || b.salonId == 0) {
                            return 0;
                        }
                        return a.pointQuanlity < b.pointQuanlity ? 1 : -1;
                    });
                    // Tinh xep hang diem chat luong.
                    $(dataStandal).each(function (index, value) {
                        value.rankStandal = index + 1;
                    });
                    if (dataCondition) {
                        // Tính điểm chất lượng Condition
                        $(dataCondition).each(function (index, value) {
                            var pointQuanlityCondition = value.ratingTB / 5 - (value.totalMonitorStaff + value.waitTime15 + (value.totalErorrScsc + value.totalErrorScscCurling) * 2 + value.errorImage * 0.5 + value.feedback * 20) / value.totalBill;
                            value.pointQuanlityCondition = pointQuanlityCondition;
                        });
                        // Sap xep diem chat luong standal.
                        dataCondition.sort(function (a, b) {
                            if (a.salonId == 0 || b.salonId == 0) {
                                return 0;
                            }
                            return a.pointQuanlityCondition < b.pointQuanlityCondition ? 1 : -1;
                        });
                        // Tinh xep hang diem chat luong.
                        $(dataCondition).each(function (index, value) {
                            value.rankCondition = index + 1;
                        });
                    }

                    $(dataStandal).each(function (index, value) {
                        let pointQuanlityCondition = null;
                        let rankCondition = null;
                        if (dataCondition) {
                            $(dataCondition).each(function (index1, value1) {
                                if (value1.salonId === value.salonId) {
                                    pointQuanlityCondition = value1.pointQuanlityCondition;
                                    rankCondition = value1.rankCondition;
                                }
                            });
                        }

                        if (value.salonId == 0) {
                            bodyFirst += `<tr data-salon-id="${value.salonId}">
                                            <td class="align-middle text-center"></td>
                                            <td class="align-middle text-center">${value.salonShortName}</td>
                                            <td class="align-middle text-center" style="background-color:${GetColorCondition(value.pointQuanlity * 100)}">${round(value.pointQuanlity * 100, 2)}%</td>
                                            <td class="align-middle text-center"></td>
                                            <td class="align-middle text-center" style="background-color:${GetColorCondition(pointQuanlityCondition * 100)}">${round(pointQuanlityCondition * 100, 2)}%</td>
                                            <td class="align-middle text-center"></td>
                                            <td class="align-middle text-center">${round(value.ratingTB, 2)}</td>
                                            <td class="align-middle text-center">${value.totalMonitorStaff}</td>
                                            <td class="align-middle text-center">${value.totalErorrScsc}</td>
                                            <td class="align-middle text-center">${value.totalErrorScscCurling}</td>
                                            <td class="align-middle text-center">${value.errorImage}</td>
                                            <td class="align-middle text-center">${value.feedback}</td>
                                            <td class="align-middle text-center">${value.waitTime15}</td>
                                            <td class="align-middle text-center">${value.totalBill}</td>
                                        </tr>`
                        }
                        else {
                            body += `<tr data-salon-id="${value.salonId}">
                                    <td class="align-middle text-center">${value.salonId}</td>
                                    <td class="align-middle text-center">${value.salonShortName}</td>
                                    <td class="align-middle text-center" style="background-color:${GetColorCondition(value.pointQuanlity * 100)}">${round(value.pointQuanlity * 100, 2)}%</td>
                                    <td class="align-middle text-center">${value.rankStandal || ""}</td>
                                    <td class="align-middle text-center" style="background-color:${GetColorCondition(pointQuanlityCondition * 100)}">${round(pointQuanlityCondition * 100, 2)}%</td>
                                    <td class="align-middle text-center">${rankCondition || ""}</td>
                                    <td class="align-middle text-center">${round(value.ratingTB, 2)}</td>
                                    <td class="align-middle text-center">${value.totalMonitorStaff}</td>
                                    <td class="align-middle text-center">${value.totalErorrScsc}</td>
                                    <td class="align-middle text-center">${value.totalErrorScscCurling}</td>
                                    <td class="align-middle text-center">${value.errorImage}</td>
                                    <td class="align-middle text-center">${value.feedback}</td>
                                    <td class="align-middle text-center">${value.waitTime15}</td>
                                    <td class="align-middle text-center">${value.totalBill}</td>
                                </tr>`
                        }
                    });

                    $('#table-listing tbody').append(bodyFirst + body);
                    table = $('#table-listing').DataTable(
                        {
                            dom: 'Bfrtip',
                            buttons: [
                                'excelHtml5',
                            ],
                            paging: false,
                            searching: false,
                            info: false,
                            fixedHeader: true,
                            ordering: false,
                            scrollY: "70vh",
                            scrollX: false,
                            scrollCollapse: true,
                        });
                }
            }
            function GetColorCondition(value) {
                if (value > 85) {
                    return "#D9EAD3";
                }
                else if (value >= 80 && value <= 85) {
                    return "#FFF2CD";
                }
                else if (value < 80) {
                    return "#F3CCCC";
                }
                else {
                    return "";
                }
            }
            function BindRegion() {

                $.ajax({
                    type: "GET",
                    url: domain + "/api/operation-report-v3/list-asm",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data, textStatus, jqXHR) {
                        if (data) {
                            var option = '<option value="0">Chọn vùng asm</option>';
                            if (!perm_ViewAllData) {
                                $(data).each(function (index, value) {
                                    if (value.id === staffId) {
                                        option += '<option value="' + value.id + '">' + value.fullName + '</option>';
                                    }
                                });
                            }
                            else {
                                $(data).each(function (index, value) {
                                    option += '<option value="' + value.id + '">' + value.fullName + '</option>';
                                });
                            }
                            $('select[data-name=regionId]').append(option);
                            $('.select').select2();
                        }

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        if (jqXHR.status === 400) {
                            return;
                        }
                        if (jqXHR.status != 200) {
                            let strError = 'Đã có lỗi xảy ra vui lòng liên hệ nhóm phát triển';
                            ShowMessage("Cảnh báo", strError + " <br>- status:" + jqXHR.status + ',' + textStatus + '<br>- ' + errorThrown, 3, 7000);
                        }
                        finishLoading();
                    }
                });
            }

        </script>
    </asp:Panel>
</asp:Content>

