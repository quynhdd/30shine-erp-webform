﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.EntityClient;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.Report
{
    public partial class SlotChart : System.Web.UI.Page
    {
        public static Solution_30shineEntities db = new Solution_30shineEntities();
        CultureInfo culture = new CultureInfo("vi-VN");
        protected bool Perm_ViewAllData = true;
        protected bool Perm_ViewAllDate = false;
        private bool Perm_Access = false;
        protected bool IsAccountant = false;

        /// <summary>
        /// Set permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_ViewAllDate = true;
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        /// <summary>
        /// Execute by permission
        /// </summary>
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        /// <summary>
        /// Page load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                TxtDateTimeFrom.Text = String.Format("{0:dd/MM/yyyy}", DateTime.Now);
                Library.Function.bindSalon_NotHoiQuan(new List<DropDownList> { Salon }, Perm_ViewAllData);
            }
            else
            {
                RemoveLoading();
            }
        }

        /// <summary>
        /// Remove loading 
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        /// <summary>
        /// Get data
        /// </summary>
        /// <param name="timeFrom"></param>
        /// <param name="timeTo"></param>
        /// <param name="salonId"></param>
        /// <returns></returns>
        private static List<ResponseData> GetData(string timeFrom, string timeTo, int salonId)
        {
            try
            {
                CultureInfo culture = new CultureInfo("vi-VN");
                var _timeFrom = Convert.ToDateTime(timeFrom, culture);
                var _timeTo = Convert.ToDateTime(timeTo, culture);
                DateTime date = _timeFrom;
                var firstDayOfMonth = new DateTime(date.Year, date.Month, 1);
                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                var numberOfDay = lastDayOfMonth.Subtract(firstDayOfMonth).TotalDays;
                // raw sql
                var sql = $@"declare 
                        @salonId int,
                        @dateFrom datetime,
                        @dateTo datetime,
                        @numberOfDay int,
                        @firstDayOfMonth datetime,
                        @lastDayOfMonth datetime
                        set @salonId = {salonId}
                        set @dateFrom = '{string.Format("{0:yyyy-MM-dd}", _timeFrom)}';
                        set @dateTo = '{string.Format("{0:yyyy-MM-dd}", _timeTo)}';
                        set @firstDayOfMonth = '{string.Format("{0:yyyy-MM-dd}", firstDayOfMonth)}';
                        set @lastDayOfMonth = '{string.Format("{0:yyyy-MM-dd}", lastDayOfMonth)}';
                        set @numberOfDay = DATEDIFF(DAY,@firstDayOfMonth,@lastDayOfMonth);
                        begin
                        with
                        TimeKeeping as
                        (
                        SELECT LTRIM(RTRIM(m.n.value('.[1]','varchar(1000)'))) AS HourId, t.WorkTimeId, t.StylistId, t.SalonId
	                        FROM
	                        (
		                        SELECT ft.SalonId,ft.StaffId as StylistId,ft.WorkDate,ft.WorkTimeId,CAST('<XMLRoot><RowData>' + REPLACE(ft.HourIds,',','</RowData><RowData>') + '</RowData></XMLRoot>' AS XML) AS x
		                        from FlowTimeKeeping as ft
		                        inner join Staff  as Staff on ft.StaffId = Staff.Id
		                        where ft.IsDelete = 0 and ft.IsEnroll = 1
		                        and ft.WorkDate >= @dateFrom and ft.WorkDate < DATEADD(day, 1, @dateTo)
		                        and (ft.SalonId = @salonId or @salonId = 0)
		                        and Staff.[Type] = 1
	                        )t
	                        CROSS APPLY x.nodes('/XMLRoot/RowData')m(n)
                        ),--select * from TimeKeeping

                        TotalSlot as 
                        (
	                        select a.HourId, b.Hour, COUNT(HourId) as TotalSlot 
	                        from TimeKeeping as a 
	                        join BookHour as b 
	                        on a.HourId = b.Id
	                        where b.IsDelete = 0 and b.Publish = 1 and (b.SalonId = @salonId or @salonId = 0) 
	                        group by a.HourId, b.Hour 
                        ),

                        BookAll as
                        (
	                        select HourId, IsBookAtSalon, IsBookOnline
	                        from Booking 
	                        where DatedBook >= @dateFrom and DatedBook < DATEADD(day, 1, @dateTo) and (SalonId = @salonId or @salonId = 0) and IsDelete = 0 
                        ),

                        TotalBook as
                        (
	                        select HourId, COUNT(HourId) as TotalBook from BookAll group by HourId
                        ),

                        TotalBookOnline as
                        (
	                        select HourId, COUNT(HourId) as TotalBook from BookAll where IsBookOnline = 1 group by HourId
                        ),

                        TotalBookAtSalon as
                        (
	                        select HourId, COUNT(HourId) as TotalBook from BookAll where IsBookAtSalon = 1 group by HourId
                        ),

                        TotalBillBookOnline as
                        (
	                        select HourId, COUNT(*) as TotalBillBookOnline 
	                        from 
	                        (select b.HourId
	                        from BillService as a
	                        join Booking as b 
	                        on a.BookingId = b.Id 
	                        where a.IsDelete = 0 and b.IsDelete = 0 and a.Pending = 0 and b.DatedBook >= @dateFrom and b.DatedBook < DATEADD(day, 1, @dateTo) and (b.SalonId = @salonId  or @salonId = 0)) as a 
	                        group by HourId
                        ),

                        Finaltracking as
                        (
                        select a.sm, COUNT(a.sm) as TotalTracking from 
                        (
	                        (
		                        select Distinct 
		                        (
			                        Case when
				                        CONVERT(Time, a.CreatedTime) >= CONVERT(Time, b.HourFrame) and CONVERT(Time, a.CreatedTime) < CONVERT(Time,DATEADD(Minute, 30, b.HourFrame)) 
				                        then
				                        b.id
				                        end
		                        ) as sm, 
		                        a.Phone
		                        from [TrackingWeb_30Shine].dbo.TrackingWeb_DataV2 as a 
		                        join BookHour as b 
		                        on CONVERT(Time, a.CreatedTime) >= CONVERT(Time, b.HourFrame) 
		                        and CONVERT(Time, a.CreatedTime) < CONVERT(Time,DATEADD(Minute, 30, b.HourFrame)) 
		                        and EventID = 3 and Booking_ID is null and (a.SalonID = @salonId or @salonId = 0) and b.IsDelete = 0 and b.Publish = 1 and (b.SalonId = @salonId or @salonId = 0)
		                        and a.CreatedTime >= @dateFrom and a.CreatedTime < DATEADD(day, 1, @dateto) 
	                        )
	                        except
	                        (
		                        select Distinct 
		                        (
			                        Case when
				                        CONVERT(Time, a.CreatedTime) >= CONVERT(Time, b.HourFrame) and CONVERT(Time, a.CreatedTime) < CONVERT(Time,DATEADD(Minute, 30, b.HourFrame)) 
				                        then
				                        b.id
				                        end
		                        ) as sm, 
		                        a.Phone
		                        from [TrackingWeb_30Shine].dbo.TrackingWeb_DataV2 as a 
		                        join BookHour as b 
		                        on CONVERT(Time, a.CreatedTime) >= CONVERT(Time, b.HourFrame) 
		                        and CONVERT(Time, a.CreatedTime) < CONVERT(Time,DATEADD(Minute, 30, b.HourFrame)) 
		                        and EventID = 3 and Booking_ID > 0 and (a.SalonID = @salonId or @salonId = 0) and b.IsDelete = 0 and b.Publish = 1 and (b.SalonId = @salonId or @salonId = 0)
		                        and a.CreatedTime >= @dateFrom and a.CreatedTime < DATEADD(day, 1, @dateto) 
	                        )
                        ) as a 
                        group by a.sm
                        ),

                        StaffOverTimeDay as
                        (
	                        select TimeKeeping.StaffId from
	                        (
		                        select StaffId, COUNT(WorkDate) as NumberOfDay from FlowTimeKeeping where WorkDate >= @firstDayOfMonth and WorkDate < DATEADD(day, 1, @lastDayOfMonth)
		                        and (SalonId = @salonId or @salonId = 0)
		                        and IsEnroll = 1 and IsDelete = 0  group by StaffId
	                        ) as TimeKeeping
	                        join Staff as b on TimeKeeping.StaffId = b.Id where NumberOfDay > (@numberOfDay - 4) and b.Type = 1
                        ),

                        DayOver as
                        (
	                        select * from 
	                        (
		                        select *, ROW_NUMBER() OVER (PARTITION BY StaffId ORDER BY StaffId, WorkDate DESC) as rn from 
			                        (select a.StaffId, b.WorkDate from StaffOverTimeDay as a
			                        join FlowTimeKeeping as b on a.StaffId = b.StaffId 
			                        where b.WorkDate >= @firstDayOfMonth and b.WorkDate < DATEADD(day, 1, @lastDayOfMonth) and (b.SalonId = @salonId or @salonId = 0) and b.IsEnroll = 1 and IsDelete = 0 ) as ListAllDay
	                        ) as ListRowNumber where rn < 5
                        ),

                        ListHourDayOver as
                        (
	                        SELECT LTRIM(RTRIM(m.n.value('.[1]','varchar(1000)'))) AS HourId
	                        FROM
	                        (
		                        SELECT ft.SalonId,ft.StaffId as StylistId,ft.WorkDate,ft.WorkTimeId,CAST('<XMLRoot><RowData>' + REPLACE(ft.HourIds,',','</RowData><RowData>') + '</RowData></XMLRoot>' AS XML) AS x
		                        from FlowTimeKeeping as ft
		                        inner join DayOver  as Staff on ft.StaffId = Staff.StaffId and ft.WorkDate = Staff.WorkDate
		                        inner join Staff  as st on ft.StaffId = st.Id
		                        where ft.IsDelete = 0 and IsEnroll = 1
		                        and ft.WorkDate >= @dateFrom and ft.WorkDate < DATEADD(day, 1, @dateTo)
		                        and (ft.SalonId = @salonId or @salonId = 0)
		                        and ft.[Type] = 1
	                        )t
	                        CROSS APPLY x.nodes('/XMLRoot/RowData')m(n)
                        ),

                        FinalOvertimeSlot as 
                        (
	                        select a.HourId, a.Hour, SUM(a.TotalSlot) as TotalOvertimeSlot from 
	                        ((select a.HourId, b.Hour, COUNT(HourId) as TotalSlot 
	                        from ListHourDayOver as a 
	                        join BookHour as b 
	                        on a.HourId = b.Id 
	                        group by a.HourId, b.Hour) 
	                        Union
                            (select a.HourId, c.Hour, Count(HourId) as TotalOverTime from TimeKeeping as a
	                        join WorkTime as b on a.WorkTimeId = b.Id
	                        join BookHour as c on a.HourId = c.Id
	                        where (c.HourFrame not between b.StrartTime and b.EnadTime) group by HourId,c.Hour )) as a group by a.HourId, a.Hour
                        )
                        select * from
                        (select bh.Id, b.Hour, 
						ISNULL(b.TotalSlot,0) as 'tong_slot', 
						ISNULL(tb.TotalBook,0) as 'tong_book',
                        ISNULL(ft.TotalTracking,0) as 'buoc_3_khong_thanh_cong', 
                        ISNULL(e.TotalOvertimeSlot,0) as 'slot_tang_ca',
                        ISNULL(d.TotalBillBookOnline,0) as 'tong_bill', 
						ISNULL((b.TotalSlot - e.TotalOvertimeSlot),0) as 'slot_ko_tang_ca', 
                        ISNULL(f.TotalBook,0) as 'book_online',
						ISNULL(g.TotalBook,0) as 'book_at_salon' 
                        from TotalSlot as b 
                        left join TotalBillBookOnline as d on b.HourId = d.HourId
                        left join FinalOvertimeSlot as e on b.HourId = e.HourId
                        left join TotalBookOnline as f on b.HourId = f.HourId
                        left join TotalBookAtSalon as g on b.HourId = g.HourId
                        left join TotalBook as tb on b.HourId = tb.HourId
                        left join Finaltracking as ft on ft.sm = b.HourId
                        inner join BookHour as bh on b.HourId = bh.Id) as dm
                        order by dm.Id asc
						end";
                //conection string
                string strConnection = ConfigurationManager.ConnectionStrings["SQLConnectionStringBI"].ConnectionString;
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
                SqlConnection connection;
                SqlDataReader dataReader;
                SqlCommand command;
                connection = new SqlConnection(strConnection);
                builder.ConnectionString = strConnection;
                string UserID = builder["User ID"] as string;
                var list = new List<ResponseData>();
                if (UserID == "bi")
                {
                    try
                    {
                        connection.Open();
                        command = new SqlCommand(sql, connection);
                        dataReader = command.ExecuteReader();
                        while (dataReader.Read())
                        {
                            var item = new ResponseData();
                            item.Id = Convert.ToInt32(dataReader["Id"]);
                            item.Hour = dataReader["Hour"].ToString();
                            item.tong_slot = Convert.ToInt32(dataReader["tong_slot"]);
                            item.tong_book = Convert.ToInt32(dataReader["tong_book"]);
                            item.buoc_3_khong_thanh_cong = Convert.ToInt32(dataReader["buoc_3_khong_thanh_cong"]);
                            item.slot_tang_ca = Convert.ToInt32(dataReader["slot_tang_ca"]);
                            item.tong_bill = Convert.ToInt32(dataReader["tong_bill"]);
                            item.slot_ko_tang_ca = Convert.ToInt32(dataReader["slot_ko_tang_ca"]);
                            item.book_online = Convert.ToInt32(dataReader["book_online"]);
                            item.book_at_salon = Convert.ToInt32(dataReader["book_at_salon"]);
                            list.Add(item);
                        }
                        dataReader.Close();
                        command.Dispose();
                        connection.Close();
                    }
                    catch (Exception e)
                    {
                        throw e;
                    }
                }
                else
                {
                    throw new Exception();
                }

                return list;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Bind data
        /// </summary>
        /// <param name="timeFrom"></param>
        /// <param name="timeTo"></param>
        /// <param name="salonId"></param>
        /// <returns></returns>
        [WebMethod]
        public static string BindData(string timeFrom, string timeTo, int salonId)
        {
            try
            {
                var dt = new JavaScriptSerializer().Serialize(GetData(timeFrom, timeTo, salonId));
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.StackTrace);
            }
        }

        /// <summary>
        /// Response data
        /// </summary>
        public class ResponseData
        {
            public int Id { get; set; }
            public string Hour { get; set; }
            public int? tong_slot { get; set; }
            public int? tong_book { get; set; }
            public int? buoc_3_khong_thanh_cong { get; set; }
            public int? slot_tang_ca { get; set; }
            public int? slot_ko_tang_ca { get; set; }
            public int? tong_bill { get; set; }
            public int? book_online { get; set; }
            public int? book_at_salon { get; set; }
        }
    }
}