﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/TemplateMaster/SiteMasterVersion1.Master" CodeBehind="ServiceSalaryReports.aspx.cs" Inherits="_30shine.GUI.BackEnd.Report.ServiceSalaryReports" %>

<asp:Content ID="CustomerAdd" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel ID="ContentWrap" runat="server" ClientIDMode="Static">
        <div class="container-fluid sub-menu">
            <div class="row">
                <div class="col-md-12 pt-3 pb-2">
                    <ul>
                        <li>Báo cáo &nbsp;&#187; Tổng hợp lương</li>
                    </ul>
                </div>
                <div class="col-md-12">
                    <div class="sub-menu-boder"></div>
                </div>
            </div>
        </div>
        <form runat="server">
            <div class="container-fluid pt-2 pb-2">
                <div class="row">
                    <div class="form-group col-xl-auto">
                        <strong><i class="fa fa-filter"></i>Lọc kết quả &nbsp;&#187; </strong>
                    </div>
                    <div class="col-xl-auto form-group col-md-12 col-lg-12 col-sm-12 text-center">
                        <div class="float-left menu-time" onclick="customerView($(this))">
                            <label class="label">Tháng trước</label><br />
                            <asp:Label ID="beforeMonth" ClientIDMode="Static" runat="server" Text="Label"></asp:Label>
                        </div>
                        <div class="float-left menu-time text-center" onclick="customerView($(this))">
                            <label class="label">7 ngày trước</label><br />
                            <asp:Label ID="sevenDayBefore" ClientIDMode="Static" runat="server" Text="Label"></asp:Label>
                        </div>
                        <div class="float-left menu-time text-center" onclick="customerView($(this))">
                            <label class="label">Hôm nay</label><br />
                            <asp:Label ID="toDay" ClientIDMode="Static" runat="server" Text="Label"></asp:Label>
                        </div>

                    </div>
                    <div class="col-xl-auto col-lg-auto col-md-auto col-sm-auto form-group">
                        <input type="text" id="fromDate" placeholder="Từ ngày" class="datetime-picker form-control" />
                    </div>
                    <div class="col-xl-auto col-lg-auto col-md-auto col-sm-auto form-group">
                        <input type="text" id="toDate" placeholder="Đến ngày" class="datetime-picker form-control" />
                    </div>
                    <div class="col-xl-auto col-lg-auto col-md-auto col-sm-auto form-group">
                        <label class="mr-2">Chọn Salon</label>
                        <asp:DropDownList ID="ddlSalon" runat="server" ClientIDMode="Static" CssClass="select"></asp:DropDownList>
                    </div>
                    <div class="form-group col-xl-auto col-lg-auto col-md-auto col-sm-auto form-group">
                        <div id="ViewDataFilter" class="form-group col-md-12 col-xl-auto pl-0" onclick="defaultViewData()">
                            <div class="btn-viewdata">Xem dữ liệu</div>
                        </div>
                    </div>
                    <div class="col-md-12 col-xl-12">
                        <div class="sub-menu-boder-low"></div>
                    </div>
                </div>
            </div>
            <div class="container-fluid table-listing pt-2">
            </div>
            <div class="table-temp d-none">
                <table class="table">
                    <thead>
                        <tr>
                            <th class="align-middle text-center" rowspan="3" style="background-color: #dddddd; min-width: 100px">SALON</th>
                            <th class="align-middle text-center" rowspan="3" style="background-color: dimgrey; color: white">TỈ LỆ
                                <br />
                                (G)=((D+H)/F)*100%</th>
                            <th class="align-middle text-center" rowspan="3" style="background-color: dimgrey; color: white">TỔNG LƯƠNG
                                <br />
                                (C)=D+E+H</th>
                            <th class="align-middle text-center" rowspan="3" style="background-color: dimgrey; color: white">LƯƠNG CỨNG
                                <br />
                                (H)=a+b+c+d+e</th>
                            <th class="align-middle text-center" rowspan="3" style="background-color: dimgrey; color: white">LƯƠNG DỊCH VỤ
                                <br />
                                (D)=1+2+3+4+5</th>
                            <th class="align-middle text-center" rowspan="3" style="background-color: dimgrey; color: white">TIP
                                <br />
                                (E)=6+7+8+9</th>
                            <th class="align-middle text-center" rowspan="3" style="background-color: dimgrey; color: white">LƯƠNG TĂNG CA GIỜ</th>
                            <th class="align-middle text-center" rowspan="3" style="background-color: dimgrey; color: white">LƯƠNG TĂNG CA NGÀY</th>
                            <th class="align-middle text-center" colspan="5" rowspan="2" style="background-color: #00c8ff">LƯƠNG CỨNG THEO BỘ PHẬN</th>
                            <th class="align-middle text-center" colspan="5" rowspan="2" style="background-color: #ffe400">LƯƠNG DỊCH VỤ THEO BỘ PHẬN</th>
                            <th class="align-middle text-center" colspan="4" rowspan="2" style="background-color: #ffd800">LƯƠNG SẢN PHẨM THEO BỘ PHẬN</th>
                            <th class="align-middle text-center" colspan="2" rowspan="2" style="background-color: cornflowerblue;">DOANH THU SHINE MEMBER</th>
                            <th class="align-middle text-center" rowspan="3" style="background-color: cornflowerblue;">DOANH THU MP</th>
                            <th class="align-middle text-center" rowspan="3" style="background-color: cornflowerblue;">DOANH THU DV(F)</th>
                            <th class="align-middle text-center" style="background-color: cornflowerblue;" colspan="4">DOANH THU DƯỠNG PROTEIN</th>
                        </tr>
                        <tr>
                            <th class="text-center" colspan="2">THEO SP</th>
                            <th class="text-center" colspan="2">THEO DV</th>
                        </tr>
                        <tr>
                            <th class="align-middle text-center" style="min-width: 85px;">STYLIST
                                <div class="d-none">LC</div>
                                (a)</th>
                            <th class="align-middle text-center" style="min-width: 85px;">SKINNER
                                <div class="d-none">LC</div>
                                (b)</th>
                            <th class="align-middle text-center" style="min-width: 85px;">CHECKIN
                                <div class="d-none">LC</div>
                                (c)</th>
                            <th class="align-middle text-center" style="min-width: 85px;">CHECKOUT
                                <div class="d-none">LC</div>
                                (d)</th>
                            <th class="align-middle text-center" style="min-width: 85px;">BẢO VỆ
                                <div class="d-none">LC</div>
                                (e)</th>

                            <th class="align-middle text-center" style="min-width: 85px;">STYLIST
                                <div class="d-none">DV</div>
                                (1)</th>
                            <th class="align-middle text-center" style="min-width: 85px;">SKINNER
                                <div class="d-none">DV</div>
                                (2)</th>
                            <th class="align-middle text-center" style="min-width: 85px;">CHECKIN
                                <div class="d-none">DV</div>
                                (3)</th>
                            <th class="align-middle text-center" style="min-width: 85px;">CHECKOUT
                                <div class="d-none">DV</div>
                                (4)</th>
                            <th class="align-middle text-center" style="min-width: 85px;">BẢO VỆ
                                <div class="d-none">DV</div>
                                (5)</th>
                            <th class="align-middle text-center" style="min-width: 85px;">STYLIST
                                <div class="d-none">SP</div>
                                (6)</th>
                            <th class="align-middle text-center" style="min-width: 85px;">SKINNER
                                <div class="d-none">SP</div>
                                (7)</th>
                            <th class="align-middle text-center" style="min-width: 85px;">CHECKIN
                                <div class="d-none">SP</div>
                                (8)</th>
                            <th class="align-middle text-center" style="min-width: 85px;">CHECKOUT
                                <div class="d-none">SP</div>
                                (9)</th>
                            <th class="align-middle text-center">SL
                                <div class="d-none">MB</div>
                            </th>
                            <th class="align-middle text-center" style="min-width: 52px;">Tổng tiền
                                <div class="d-none">MB</div>
                            </th>
                            <th class="align-middle text-center">SL
                                <div class="d-none">SPP</div>
                            </th>
                            <th class="align-middle text-center" style="min-width: 52px">Tổng tiền
                                <div class="d-none">SPP</div>
                            </th>
                            <th class="align-middle text-center">SL
                                <div class="d-none">DVP</div>
                            </th>
                            <th class="align-middle text-center">Tổng tiền
                                <div class="d-none">DVP</div>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </form>
        <!-- Page loading -->
        <div class="page-loading">
            <p>Vui lòng đợi trong giây lát...</p>
        </div>
        <style>
            .menu-time {
                margin-right: 7px;
                font-size: 13px;
            }

                .menu-time .label {
                    padding: 0px 15px;
                    background: #000000;
                    color: #ffffff;
                    cursor: pointer;
                    border-radius: 15px;
                    line-height: 26px;
                    width: 94px;
                }

            .form-control {
                height: 31px !important;
            }

            .select2.select2-container.select2-container--default {
                width: 180px !important;
            }

            .table-listing table tbody tr,
            .table-listing table tbody tr td,
            .table-listing table thead tr th,
            .table-listing table, table thead {
                border-color: #000000
            }

                .table-listing table thead tr th {
                    font-family: Roboto Condensed Bold;
                }

            .table-listing {
                height: 100% !important;
                min-height: 80% !important;
            }

            #tableListing_wrapper button.buttons-excel {
                background-color: black;
                width: 100px;
            }

                #tableListing_wrapper button.buttons-excel span {
                    color: white
                }
        </style>
        <script>
            $(document).ready(function () {

            });
            function customerView(t) {
                let fromDate = t.find('span').attr('data-firstDay');
                let toDate = t.find('span').attr('data-endDay');
                if (typeof fromDate === "undefined" ||
                    fromDate === '' ||
                    fromDate === null ||
                    typeof toDate === "undefined" ||
                    toDate === "" ||
                    toDate === null) {
                    ShowMessage("Thông báo", "Vui lòng chọn ngày", 3);
                    return;
                }
                viewData(fromDate, toDate);
            }

            function defaultViewData() {
                let fromDate = $('#fromDate').val();
                let toDate = $('#toDate').val();

                if (typeof fromDate === "undefined" ||
                    fromDate === '' ||
                    fromDate === null ||
                    typeof toDate === "undefined" ||
                    toDate === "" ||
                    toDate === null) {
                    debugger;
                    ShowMessage("Thông báo", "Vui lòng chọn ngày", 3);
                    return;
                }
                viewData(fromDate, toDate);
            }

            function viewData(fromDate, toDate) {
                startLoading();
                let salonId = $('#ddlSalon').val();
                let data = {
                    fromDate: fromDate,
                    toDate: toDate,
                    salonId: salonId
                }
                //let response = AjaxPostApi(data, '/GUI/BackEnd/Report/ServiceSalaryReports.aspx/BindData', true);
                $.ajax({
                    type: "POST",
                    url: '/GUI/BackEnd/Report/ServiceSalaryReports.aspx/BindData',
                    async: true,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    //crossDomain: true,
                    data: JSON.stringify(data),
                    success: function (data, textStatus, jqXHR) {
                        BindData(data);
                        ShowMessage("Thành công", "", 2);
                        finishLoading();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        if (jqXHR.status != 200) {
                            let strError = 'Đã có lỗi xảy ra vui lòng liên hệ nhóm phát triển';
                            let message = "";
                            if (typeof jqXHR.responseJSON.Message != "undefined") {
                                message = jqXHR.responseJSON.Message
                            }
                            else {
                                message = jqXHR.responseText;
                            }
                            ShowMessage("Cảnh báo", strError + " <br>- status:" + jqXHR.status + ',' + textStatus + '<br>- message: ' + message + '<br>- ' + errorThrown, 3);
                        }
                        finishLoading();
                    }
                });
            };
            function BindData(response) {
                let objTr = "";
                $("#tableListing tbody").html("");
                if (typeof response === "object") {
                    $('.table-listing').empty();
                    var tableTemp = $('.table-temp table').attr('id', 'tableListing').clone();
                    $('.table-listing').append(tableTemp);
                    // Bind total
                    var recordTotal = response.d.total;
                    let tip = recordTotal.luong_sp_stylist +
                        recordTotal.luong_sp_skinner +
                        recordTotal.luong_sp_checkin +
                        recordTotal.luong_sp_checkout;
                    let luongDichVu = recordTotal.luong_dv_stylist +
                        recordTotal.luong_dv_skinner +
                        recordTotal.luong_dv_checkin +
                        recordTotal.luong_dv_checkout +
                        recordTotal.luong_dv_baove;
                    let luongCung = recordTotal.luong_cung_stylist +
                        recordTotal.luong_cung_skinner +
                        recordTotal.luong_cung_checkin +
                        recordTotal.luong_cung_checkout +
                        recordTotal.luong_cung_baove;
                    //let tiLe = recordTotal.tong_doanh_thu_dv === 0 ? 0 : (luongDichVu / recordTotal.tong_doanh_thu_dv * 100);
                    let tiLe = recordTotal.tong_doanh_thu_dv === 0 ? 0 : ((luongDichVu + luongCung) / recordTotal.tong_doanh_thu_dv * 100);
                    objTr += '<tr>' +
                        '<td style="background-color: #ffe400">TOÀN-HT</td>' +
                        '<td class="align-middle text-center" data-name="tiLe" style="background-color: #ffe400">' + addCommas(round(tiLe, 2)) + '</td>' +
                        '<td class="align-middle text-center" data-name="tongLuong" style="background-color: #ffe400">' + addCommas(round((tip + luongDichVu + luongCung), 0)) + '</td>' +
                        '<td class="align-middle text-center" data-name="luongDichVu" style="background-color: #ffe400">' + addCommas(round(luongCung, 0)) + '</td>' +
                        '<td class="align-middle text-center" data-name="luongDichVu" style="background-color: #ffe400">' + addCommas(round(luongDichVu, 0)) + '</td>' +
                        '<td class="align-middle text-center" data-name="tip" style="background-color: #ffe400">' + addCommas(round(tip, 0)) + '</td>' +
                        '<td class="align-middle text-center" data-name="tangCaGio" style="background-color: #ffe400">' + addCommas(round(recordTotal.tong_luong_tang_ca_tieng, 0)) + '</td>' +
                        '<td class="align-middle text-center" data-name="tangCaNgay" style="background-color: #ffe400">' + addCommas(round(recordTotal.tong_luong_tang_ca_ngay, 0)) + '</td>' +

                        '<td class="align-middle text-center" data-name="dvStylist" style="background-color: #ffe400">' + addCommas(round(recordTotal.luong_cung_stylist, 0)) + '</td>' +
                        '<td class="align-middle text-center" data-name="dvSkinner" style="background-color: #ffe400">' + addCommas(round(recordTotal.luong_cung_skinner, 0)) + '</td>' +
                        '<td class="align-middle text-center" data-name="dvCheckin" style="background-color: #ffe400">' + addCommas(round(recordTotal.luong_cung_checkin, 0)) + '</td>' +
                        '<td class="align-middle text-center" data-name="dvCheckout" style="background-color: #ffe400">' + addCommas(round(recordTotal.luong_cung_checkout, 0)) + '</td>' +
                        '<td class="align-middle text-center" data-name="dvBaoVe" style="background-color: #ffe400">' + addCommas(round(recordTotal.luong_cung_baove, 0)) + '</td>' +

                        '<td class="align-middle text-center" data-name="dvStylist" style="background-color: #ffe400">' + addCommas(round(recordTotal.luong_dv_stylist, 0)) + '</td>' +
                        '<td class="align-middle text-center" data-name="dvSkinner" style="background-color: #ffe400">' + addCommas(round(recordTotal.luong_dv_skinner, 0)) + '</td>' +
                        '<td class="align-middle text-center" data-name="dvCheckin" style="background-color: #ffe400">' + addCommas(round(recordTotal.luong_dv_checkin, 0)) + '</td>' +
                        '<td class="align-middle text-center" data-name="dvCheckout" style="background-color: #ffe400">' + addCommas(round(recordTotal.luong_dv_checkout, 0)) + '</td>' +
                        '<td class="align-middle text-center" data-name="dvBaoVe" style="background-color: #ffe400">' + addCommas(round(recordTotal.luong_dv_baove, 0)) + '</td>' +
                        '<td class="align-middle text-center" data-name="spStylist" style="background-color: #ffe400">' + addCommas(round(recordTotal.luong_sp_stylist, 0)) + '</td>' +
                        '<td class="align-middle text-center" data-name="spSkinner" style="background-color: #ffe400">' + addCommas(round(recordTotal.luong_sp_skinner, 0)) + '</td>' +
                        '<td class="align-middle text-center" data-name="spCheckin" style="background-color: #ffe400">' + addCommas(round(recordTotal.luong_sp_checkin, 0)) + '</td>' +
                        '<td class="align-middle text-center" data-name="spCheckout" style="background-color: #ffe400">' + addCommas(round(recordTotal.luong_sp_checkout, 0)) + '</td>' +
                        '<td class="align-middle text-center" data-name="memberSl" style="background-color: #ffe400">' + addCommas(round(recordTotal.total_shine_member, 0)) + '</td>' +
                        '<td class="align-middle text-center" data-name="memberTT" style="background-color: #ffe400">' + addCommas(round(recordTotal.total_price_shine_member, 0)) + '</td>' +
                        '<td class="align-middle text-center" data-name="spDoanhtdu" style="background-color: #ffe400">' + addCommas(round(recordTotal.tong_doanh_thu_mp, 0)) + '</td>' +
                        '<td class="align-middle text-center" data-name="dvDoanhtdu" style="background-color: #ffe400">' + addCommas(round(recordTotal.tong_doanh_thu_dv, 0)) + '</td>' +
                        '<td class="align-middle text-center" data-name="proteinSpSoLuong" style="background-color: #ffe400">' + addCommas(round(recordTotal.total_protein_689_product, 0)) + '</td>' +
                        '<td class="align-middle text-center" data-name="proteinSpTT" style="background-color: #ffe400">' + addCommas(round(recordTotal.total_price_protein_689, 0)) + '</td>' +
                        '<td class="align-middle text-center" data-name="proteinDvSoLuong" style="background-color: #ffe400">' + addCommas(round(recordTotal.protein_69_service, 0)) + '</td>' +
                        '<td class="align-middle text-center" data-name="proteinDvTT" style="background-color: #ffe400">' + addCommas(round(recordTotal.total_price_protein_69, 0)) + '</td>' +
                        '</tr>';
                    //bind row
                    $.each(response.d.list,
                        function (index, value) {
                            let tip = value.luong_sp_stylist +
                                value.luong_sp_skinner +
                                value.luong_sp_checkin +
                                value.luong_sp_checkout;
                            let luongDichVu = value.luong_dv_stylist +
                                value.luong_dv_skinner +
                                value.luong_dv_checkin +
                                value.luong_dv_checkout +
                                value.luong_dv_baove;
                            let luongCung = value.luong_cung_stylist +
                                value.luong_cung_skinner +
                                value.luong_cung_checkin +
                                value.luong_cung_checkout +
                                value.luong_cung_baove;
                            //let tiLe = value.tong_doanh_thu_dv === 0 ? 0 : (luongDichVu / value.tong_doanh_thu_dv * 100);
                            let tiLe = value.tong_doanh_thu_dv === 0 ? 0 : ((luongDichVu + luongCung) / value.tong_doanh_thu_dv * 100);
                            objTr += '<tr>' +
                                '<td>' + value.Salon + '</td>' +
                                '<td>' + addCommas(round(tiLe, 2)) + '</td>' +
                                '<td>' + addCommas((round((tip + luongDichVu + luongCung), 0))) + '</td>' +
                                '<td>' + addCommas(round(luongCung, 0)) + '</td>' +
                                '<td>' + addCommas(round(luongDichVu, 0)) + '</td>' +
                                '<td>' + addCommas(round(tip, 0)) + '</td>' +
                                '<td>' + addCommas(round(value.tong_luong_tang_ca_tieng, 0)) + '</td>' +
                                '<td>' + addCommas(round(value.tong_luong_tang_ca_ngay, 0)) + '</td>' +

                                '<td>' + addCommas(round(value.luong_cung_stylist, 0)) + '</td>' +
                                '<td>' + addCommas(round(value.luong_cung_skinner, 0)) + '</td>' +
                                '<td>' + addCommas(round(value.luong_cung_checkin, 0)) + '</td>' +
                                '<td>' + addCommas(round(value.luong_cung_checkout, 0)) + '</td>' +
                                '<td>' + addCommas(round(value.luong_cung_baove, 0)) + '</td>' +

                                '<td>' + addCommas(round(value.luong_dv_stylist, 0)) + '</td>' +
                                '<td>' + addCommas(round(value.luong_dv_skinner, 0)) + '</td>' +
                                '<td>' + addCommas(round(value.luong_dv_checkin, 0)) + '</td>' +
                                '<td>' + addCommas(round(value.luong_dv_checkout, 0)) + '</td>' +
                                '<td>' + addCommas(round(value.luong_dv_baove, 0)) + '</td>' +
                                '<td>' + addCommas(round(value.luong_sp_stylist, 0)) + '</td>' +
                                '<td>' + addCommas(round(value.luong_sp_skinner, 0)) + '</td>' +
                                '<td>' + addCommas(round(value.luong_sp_checkin, 0)) + '</td>' +
                                '<td>' + addCommas(round(value.luong_sp_checkout, 0)) + '</td>' +
                                '<td>' + addCommas(round(value.total_shine_member, 0)) + '</td>' +
                                '<td>' + addCommas(round(value.total_price_shine_member, 0)) + '</td>' +
                                '<td>' + addCommas(round(value.tong_doanh_thu_mp, 0)) + '</td>' +
                                '<td>' + addCommas(round(value.tong_doanh_thu_dv, 0)) + '</td>' +
                                '<td>' + addCommas(round(value.total_protein_689_product, 0)) + '</td>' +
                                '<td>' + addCommas(round(value.total_price_protein_689, 0)) + '</td>' +
                                '<td>' + addCommas(round(value.protein_69_service, 0)) + '</td>' +
                                '<td>' + addCommas(round(value.total_price_protein_69, 0)) + '</td>' +
                                '</tr>';
                        });

                    $("#tableListing tbody").empty();
                    $("#tableListing tbody").append(objTr);
                    ConfigTableListing();
                }
            }
            // ConfigTableListing
            function ConfigTableListing() {
                var height = $(document).height();
                var table = $('#tableListing').DataTable(
                    {
                        dom: 'Bfrtip',
                        buttons: [
                            'excelHtml5',
                        ],
                        paging: false,
                        searching: false,
                        info: false,
                        fixedHeader: true,
                        ordering: false,
                        fixedColumns: {
                            leftColumns: 2
                        },
                        scrollY: "700px",
                        scrollX: true,
                        scrollCollapse: true,
                    });
            }
        </script>
    </asp:Panel>
</asp:Content>


