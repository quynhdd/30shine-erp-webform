﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.Interface;
using Libraries;

namespace _30shine.GUI.BackEnd.Report
{
    public partial class ImportExperiencePointService : System.Web.UI.Page
    {
        private bool Perm_Access;
        public bool Perm_ViewAllData;
        private bool Perm_ShowSalon;

        /// <summary>
        /// check permission
        /// </summary>
        private void SetPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                int integer;
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_ShowSalon = permissionModel.CheckPermisionByAction("Perm_ShowSalon", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        /// <summary>
        /// Executeby permission
        /// </summary>
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
        }
    }
}