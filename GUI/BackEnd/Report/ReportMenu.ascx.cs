﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.Report
{
    public partial class ReportMenu : System.Web.UI.UserControl
    {
        protected string Permission = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            Permission = Session["User_Permission"].ToString().Trim();
        }
    }
}