﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.Report
{
    public partial class Report_KetQuaKinhDoanh : System.Web.UI.Page
    {
        private string PageID = "BETA_BC_KQKD";
        private Solution_30shineEntities db;
        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        protected bool Perm_ViewAllData = false;
        protected bool Perm_ViewAllDate = false;
        private bool Perm_Access = false;
        protected bool IsAccountant = false;
        CultureInfo culture = new CultureInfo("vi-VN");
        protected string Day1 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-2));
        protected string Day2 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-1));
        protected string Day3 = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
        public Report_KetQuaKinhDoanh()
        {
            db = new Solution_30shineEntities();
        }

        private DateTime timeFrom;
        private DateTime timeTo;
        protected string textNote;

        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //Perm_ViewAllDate = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllDate", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_ViewAllDate = permissionModel.CheckPermisionByAction("Perm_ViewAllDate", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                TxtDateTimeFrom.Text = String.Format("{0:dd/MM/yyyy}", DateTime.Now);
                //genReportTable();
                divNote.Visible = false;
            }
            else
            {
                RemoveLoading();
            }
        }
        /// <summary>
        /// _BtnClick
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void _BtnClick(object sender, EventArgs e)
        {
            BindData();
        }
        /// <summary>
        /// BindData // Bao cao ket qua kinh doanh
        /// </summary>
        protected void BindData()
        {
            List<cls_BaoCaoKinhDoanh> data = new List<cls_BaoCaoKinhDoanh>();
            var itemS4M = new cls_BaoCaoKinhDoanh();
            var TotalToanHeThong = new cls_BaoCaoKinhDoanh();

            if (TxtDateTimeFrom.Text != "")
            {
                timeFrom = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                if (!timeFrom.Equals(null) && TxtDateTimeTo.Text != "")
                {
                    timeTo = Convert.ToDateTime(TxtDateTimeTo.Text, culture).AddDays(1);
                }
                else
                {
                    timeTo = timeFrom.AddDays(1);
                }
            }
            else
            {
                timeFrom = DateTime.Now;
                timeTo = timeFrom.AddDays(1);
            }
            //list bao cao doanh so
            var lst1 = db.Store_BaoCaoDoanhSo_Version2(timeFrom, timeTo).ToList();
            //list bao cao thu chi
            var lst2 = db.Store_BaoCaoThuChi_Version2(timeFrom, timeTo).ToList();

            // MapData Lst1 and Lst2
            data = (from a in lst1
                    join b in lst2 on a.SalonId equals b.IdSalon1
                    orderby a.Order ascending
                    select new cls_BaoCaoKinhDoanh
                    {
                        SalonId = a.SalonId,
                        ShortName = a.ShortName,
                        TongHoaDon = a.TongHoaDon,
                        DsShineCombo = a.DsShineCombo,
                        DsKidCombo = a.DsKidCombo,
                        DsDuongProtein = a.DsDuongProtein,
                        DsNhuom = a.DsNhuom,
                        DsUon = a.DsUon,
                        DsDuoi = a.DsDuoi,
                        DsDuoi_Uon = a.DsUon + a.DsDuoi,
                        DsTay = a.DsTay,
                        DsTay2Lan = a.DsTay2Lan,
                        TongDoanhThu = b.TongDoanhThu,
                        CP_BH_QL_Khac = b.CP_BH_QL_Khac,
                        CP_IT = b.CP_IT,
                        CP_Marketing = b.CP_Marketing,
                        CP_PhtSinh = b.CP_PhtSinh,
                        GV_MyPhamVatTu = b.GV_MyPhamVatTu,
                        GV_MyPhamXuatBan = b.GV_MyPhamXuatBan,
                        GV_NhanVien_HoaHong = b.GV_NhanVien_HoaHong,
                        GV_TienLuongNhanVien = b.GV_TienLuongNhanVien,
                        LNG = b.LNG,
                        TongDoanhTthuDichVu = b.TongDoanhTthuDichVu,
                        TongDoanhTthuMyPham = b.TongDoanhTthuMyPham
                    }
                    ).ToList();
            // Bộ phận S4M
            // baos cao S4M
            var objS4M = db.BaoCaoKinhDoanhS4M(timeFrom, timeTo).SingleOrDefault();
            itemS4M = new cls_BaoCaoKinhDoanh();
            itemS4M.SalonId = objS4M.Id; //  ID trong bảng KetQuaKinhDoanh_Salon
            itemS4M.ShortName = objS4M.SalonName;
            itemS4M.GV_TienLuongNhanVien = objS4M.GV_TienLuongNhanVien;
            itemS4M.TongDoanhTthuMyPham = objS4M.TongDoanhTthuMyPham;
            itemS4M.CP_BH_QL_Khac = objS4M.CP_BH_QL_Khac;
            itemS4M.CP_Marketing = objS4M.CP_Marketing;
            itemS4M.CP_IT = objS4M.CP_IT;
            itemS4M.CP_PhtSinh = objS4M.CP_PhatSinh;
            itemS4M.LNG = objS4M.LNG;
            //add S4M 
            data.Add(itemS4M);
            //Tinh toan he thong
            TotalToanHeThong = new cls_BaoCaoKinhDoanh();
            TotalToanHeThong.SalonId = 0;
            TotalToanHeThong.ShortName = "TOÀN HỆ THỐNG";
            TotalToanHeThong.LNG = data.Sum(i => i.LNG);
            TotalToanHeThong.TongHoaDon = data.Sum(i => i.TongHoaDon);
            TotalToanHeThong.DsShineCombo = data.Sum(i => i.DsShineCombo);
            TotalToanHeThong.DsKidCombo = data.Sum(i => i.DsKidCombo);
            TotalToanHeThong.DsDuongProtein = data.Sum(i => i.DsDuongProtein);
            TotalToanHeThong.DsUon = data.Sum(i => i.DsUon);
            TotalToanHeThong.DsDuoi = data.Sum(i => i.DsDuoi);
            TotalToanHeThong.DsDuoi_Uon = TotalToanHeThong.DsUon + TotalToanHeThong.DsDuoi;
            TotalToanHeThong.DsNhuom = data.Sum(i => i.DsNhuom);
            TotalToanHeThong.DsTay = data.Sum(i => i.DsTay);
            TotalToanHeThong.DsTay2Lan = data.Sum(i => i.DsTay2Lan);
            TotalToanHeThong.TongDoanhThu = data.Sum(i => i.TongDoanhThu);
            TotalToanHeThong.TongDoanhTthuDichVu = data.Sum(i => i.TongDoanhTthuDichVu);
            TotalToanHeThong.TongDoanhTthuMyPham = data.Sum(i => i.TongDoanhTthuMyPham);
            TotalToanHeThong.GV_MyPhamVatTu = data.Sum(i => i.GV_MyPhamVatTu);
            TotalToanHeThong.GV_MyPhamXuatBan = data.Sum(i => i.GV_MyPhamXuatBan);
            TotalToanHeThong.GV_NhanVien_HoaHong = data.Sum(i => i.GV_NhanVien_HoaHong);
            TotalToanHeThong.GV_TienLuongNhanVien = data.Sum(i => i.GV_TienLuongNhanVien);
            TotalToanHeThong.CP_BH_QL_Khac = data.Sum(i => i.CP_BH_QL_Khac);
            TotalToanHeThong.CP_IT = data.Sum(i => i.CP_IT);
            TotalToanHeThong.CP_Marketing = data.Sum(i => i.CP_Marketing);
            TotalToanHeThong.CP_PhtSinh = data.Sum(i => i.CP_PhtSinh);
            data.Insert(0, TotalToanHeThong);

            rptData.DataSource = data;
            rptData.DataBind();

            /// Bind Note
            if (TxtDateTimeFrom.Text == TxtDateTimeTo.Text || (TxtDateTimeFrom.Text != "" && TxtDateTimeTo.Text == ""))
            {
                var note = db.KetQuaKinhDoanh_FlowImport.FirstOrDefault(w => w.IsDelete != true && w.ImportDate == timeFrom && w.KQKDSalonId == null && w.KQKDItemId == null);
                if (note != null)
                {
                    textNote = note.Note;
                    divNote.Visible = true;
                }
                else
                {
                    divNote.Visible = false;
                }
            }
            else
            {
                divNote.Visible = false;
            }
        }
    

        
        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }
        //Khai bao class temp Bao cao Kinh doanh
        public class cls_BaoCaoKinhDoanh
        {
            public int SalonId { get; set; }
            public string ShortName { get; set; }
            public Nullable<int> Order { get; set; }
            public Nullable<int> TongHoaDon { get; set; }
            public Nullable<int> DsShineCombo { get; set; }
            public Nullable<int> DsKidCombo { get; set; }
            public Nullable<int> DsDuongProtein { get; set; }
            public Nullable<int> DsUon { get; set; }
            public Nullable<int> DsDuoi { get; set; }
            public Nullable<int> DsDuoi_Uon { get; set; }
            public Nullable<int> DsNhuom { get; set; }
            public Nullable<int> DsTay { get; set; }
            public Nullable<int> DsTay2Lan { get; set; }
            public Nullable<double> LNG { get; set; }
            public int IdSalon1 { get; set; }
            public Nullable<double> TongDoanhTthuDichVu { get; set; }
            public Nullable<double> TongDoanhTthuMyPham { get; set; }
            public Nullable<double> TongDoanhThu { get; set; }
            public Nullable<double> GV_NhanVien_HoaHong { get; set; }
            public Nullable<double> GV_MyPhamXuatBan { get; set; }
            public Nullable<double> GV_TienLuongNhanVien { get; set; }
            public int IdSalon2 { get; set; }
            public Nullable<double> GV_MyPhamVatTu { get; set; }
            public Nullable<double> CP_BH_QL_Khac { get; set; }
            public Nullable<double> CP_Marketing { get; set; }
            public Nullable<double> CP_IT { get; set; }
            public Nullable<double> CP_PhtSinh { get; set; }
        }

    }
}