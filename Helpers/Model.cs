﻿using _30shine.MODEL.ENTITY.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Library
{
    public class Model
    {
        /// <summary>
        /// Get project database model
        /// </summary>
        /// <returns></returns>
        public static Solution_30shineEntities getProjectModel()
        {
            try
            {
                return new Solution_30shineEntities();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}