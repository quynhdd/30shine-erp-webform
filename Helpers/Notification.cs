﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace Library
{
    public class Notification
    {
        private string SERVER_API_KEY;
        private string SENDER_ID;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="apiKey"></param>
        /// <param name="senderId"></param>
        public Notification(string apiKey, string senderId)
        {
            SERVER_API_KEY = apiKey;
            SENDER_ID = senderId;
        }

        /// <summary>
        /// Gửi notification cho từng thiết bị qua Device Registration Token
        /// </summary>
        /// <param name="DEVICE_ID"></param>
        /// <param name="PRIORITY"></param>
        /// <param name="notification"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public Library.Class.cls_message SendToSingleDevice(Library.Class.cls_device_infor DEVICE_ID, string PRIORITY, Library.Class.cls_notification notification, List<KeyValuePair<string, string>> data)
        {
            var msg = new Library.Class.cls_message();
            String sResponseFromServer = "Opps! Something went wrong. Please try again.";
            PRIORITY = PRIORITY != "" ? PRIORITY : "normal";
            WebRequest tRequest;
            tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
            tRequest.Method = "post";
            tRequest.ContentType = "application/json";
            tRequest.Headers.Add(string.Format("Authorization: key={0}", SERVER_API_KEY));
            tRequest.Headers.Add(string.Format("Sender: id={0}", SENDER_ID));

            if (notification != null && DEVICE_ID != null && DEVICE_ID.DeviceRegistrationToken != "")
            {
                string to = ",\"to\" : \"" + DEVICE_ID.DeviceRegistrationToken + "\"";
                string dataJSON = "";
                if (data.Count > 0)
                {
                    dataJSON += ",\"data\" : {";
                    foreach (var v in data)
                    {
                        dataJSON += "\"" + v.Key + "\" : \"" + v.Value + "\"";
                    }
                    dataJSON += "}";
                }

                string postData = "{" +
                                      "\"priority\" : \"" + PRIORITY + "\"," +
                                      "\"notification\" : {" +
                                          "\"title\" : \"" + notification.title + "\"," +
                                          "\"text\" : \"" + notification.text + "\"," +
                                          "\"icon\" : \"" + notification.icon + "\"," +
                                          "\"tag\" : \"" + notification.tag + "\"," +
                                          "\"sound\" : \"" + notification.sound + "\"" +
                                      "}"
                                      + dataJSON
                                      + to +
                                  "}";

                Console.WriteLine(postData);
                Byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                tRequest.ContentLength = byteArray.Length;

                Stream dataStream = tRequest.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();

                WebResponse tResponse = tRequest.GetResponse();

                dataStream = tResponse.GetResponseStream();

                StreamReader tReader = new StreamReader(dataStream);

                sResponseFromServer = tReader.ReadToEnd();
                msg.status = "success";

                tReader.Close();
                dataStream.Close();
                tResponse.Close();
            }
            else
            {
                msg.status = "failed";
            }

            msg.message = sResponseFromServer;
            return msg;
        }

        /// <summary>
        /// Send notification to multiple device (array of registration token)
        /// </summary>
        /// <param name="DEVICE_IDS"></param>
        /// <param name="PRIORITY"></param>
        /// <param name="notification"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public Library.Class.cls_message SendToMultipleDevice(string DEVICE_IDS, string PRIORITY, Library.Class.cls_notification notification, List<KeyValuePair<string, string>> data)
        {
            var msg = new Library.Class.cls_message();
            String sResponseFromServer = "Opps! Something went wrong. Please try again.";
            PRIORITY = PRIORITY != "" ? PRIORITY : "normal";
            WebRequest tRequest;
            tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
            tRequest.Method = "post";
            tRequest.ContentType = "application/json";
            tRequest.Headers.Add(string.Format("Authorization: key={0}", SERVER_API_KEY));
            tRequest.Headers.Add(string.Format("Sender: id={0}", SENDER_ID));

            if ((notification != null || data.Count > 0) && DEVICE_IDS != "")
            {
                string registration_ids = ",\"registration_ids\" : [" + DEVICE_IDS + "]";
                string dataJSON = "";
                if (data.Count > 0)
                {
                    dataJSON += ",\"data\" : {";
                    foreach (var v in data)
                    {
                        dataJSON += "\"" + v.Key + "\" : \"" + v.Value + "\",";
                    }
                    dataJSON = dataJSON.TrimEnd(',') + "}";
                }

                string postData = "{" +
                                      "\"priority\" : \"" + PRIORITY + "\"," +
                                      "\"notification\" : {" +
                                          "\"title\" : \"" + notification.title + "\"," +
                                          "\"text\" : \"" + notification.text + "\"," +
                                          "\"icon\" : \"" + notification.icon + "\"," +
                                          "\"tag\" : \"" + notification.tag + "\"," +
                                          "\"sound\" : \"" + notification.sound + "\"" +
                                      "}"
                                      + dataJSON
                                      + registration_ids +
                                  "}";

                Console.WriteLine(postData);
                Byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                tRequest.ContentLength = byteArray.Length;

                Stream dataStream = tRequest.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();

                WebResponse tResponse = tRequest.GetResponse();

                dataStream = tResponse.GetResponseStream();

                StreamReader tReader = new StreamReader(dataStream);

                sResponseFromServer = tReader.ReadToEnd();
                msg.status = "success";

                tReader.Close();
                dataStream.Close();
                tResponse.Close();
            }
            else
            {
                msg.status = "failed";
            }

            msg.message = sResponseFromServer;
            return msg;
        }

        /// <summary>
        /// Send notification to topic
        /// </summary>
        /// <param name="PRIORITY"></param>
        /// <param name="notification"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public Library.Class.cls_message SendToTopic(string PRIORITY, Library.Class.cls_notification notification, List<KeyValuePair<string, string>> data)
        {
            var msg = new Library.Class.cls_message();
            String sResponseFromServer = "Opps! Something went wrong. Please try again.";
            PRIORITY = PRIORITY != "" ? PRIORITY : "normal";
            WebRequest tRequest;
            tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
            tRequest.Method = "post";
            tRequest.ContentType = "application/json";
            tRequest.Headers.Add(string.Format("Authorization: key={0}", SERVER_API_KEY));
            tRequest.Headers.Add(string.Format("Sender: id={0}", SENDER_ID));

            if (notification != null || data.Count > 0)
            {
                string dataJSON = "";
                if (data.Count > 0)
                {
                    dataJSON += ",\"data\" : {";
                    foreach (var v in data)
                    {
                        dataJSON += "\"" + v.Key + "\" : \"" + v.Value + "\"";
                    }
                    dataJSON += "}";
                }

                string postData = "{" +
                                      "\"priority\" : \"" + PRIORITY + "\"," +
                                      "\"notification\" : {" +
                                          "\"title\" : \"" + notification.title + "\"," +
                                          "\"text\" : \"" + notification.text + "\"," +
                                          "\"icon\" : \"" + notification.icon + "\"," +
                                          "\"tag\" : \"" + notification.tag + "\"," +
                                          "\"sound\" : \"" + notification.sound + "\"" +
                                      "}"
                                      + dataJSON +
                                      "\"to\": \"/topics/linhDQ\"" +
                                  "}";

                Console.WriteLine(postData);
                Byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                tRequest.ContentLength = byteArray.Length;

                Stream dataStream = tRequest.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();

                WebResponse tResponse = tRequest.GetResponse();

                dataStream = tResponse.GetResponseStream();

                StreamReader tReader = new StreamReader(dataStream);

                sResponseFromServer = tReader.ReadToEnd();
                msg.status = "success";

                tReader.Close();
                dataStream.Close();
                tResponse.Close();
            }
            else
            {
                msg.status = "failed";
            }

            msg.message = sResponseFromServer;
            return msg;
        }
    }
}

/*
 * Notification structure
 * Thông báo
{
	"priority" : "normal",
	"notification" : {
		"title" : "Khai trương Salon 702 Đường Láng",
		"text" : "Salon 30Shine 702 Đường Láng mới hoạt động từ ngày 11/08/2016, mời anh qua trải nghiệm dịch vụ!",
		"icon" : "ic_info_status_bar",
		"tag" : "06h15 09/09/2016",
		"sound" : "default"
	},
	"data" : {
		"title" : "Khai trương Salon 702 Đường Láng",
		"text" : "Salon 30Shine 702 Đường Láng mới hoạt động từ ngày 11/08/2016, mời anh qua trải nghiệm dịch vụ!",
		"icon" : "ic_info_status_bar",
		"send_time" : "06h15 09/09/2016",
		"sound" : "default",
		"image" : ""
	},
	"registration_ids" : ["cQ8DoNkBIpo:APA91bGzuvq2o...","fW7_i2poAhg:APA91bGg..."]
} 

  * Tin nhắn
{
	"priority" : "normal",
	"notification" : {
		"title" : "Tin nhắn từ 30Shine",
		"text" : "Anh ơi, sắp sinh nhật anh rồi đấy, qua 30Shine cắt tóc đẹp trai đón tuổi mới nào!",
		"icon" : ""ic_read_message_status_bar";",
		"tag" : "06h15 09/09/2016",
		"sound" : "default"
	},
	"data" : {
		"title" : "Khai trương Salon 702 Đường Láng",
		"text" : "Anh <#customername#> ơi, sắp sinh nhật anh rồi đấy, qua 30Shine cắt tóc đẹp trai đón tuổi mới nào!",
		"icon" : ""ic_read_message_status_bar";",
		"send_time" : "06h15 09/09/2016",
		"sound" : "default",
		"image" : ""
	},
	"registration_ids" : ["cQ8DoNkBIpo:APA91bGzuvq2o...","fW7_i2poAhg:APA91bGg..."]
} 
*/
