﻿using _30shine.MODEL.ENTITY.EDMX;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;

namespace _30shine.Helpers
{
    public static class StaffLib
    {
        /// <summary>
        /// Log staff skill level change
        /// </summary>
        /// <param name="staffId"></param>
        /// <param name="skillLevel"></param>
        /// <param name="db"></param>
        public static void AddLogSkillLevel(int staffId, int skillLevel, Solution_30shineEntities db)
        {
            if (staffId > 0 && skillLevel > 0)
            {
                var obj = db.Logs.OrderByDescending(o=>o.LogTime).FirstOrDefault(w => w.OBJId == staffId && w.Type == "staff_skill_level");
                if (obj == null || (obj != null && obj.Content != skillLevel.ToString()))
                {
                    obj = new Log();
                    obj.LogTime = DateTime.Now;
                    obj.OBJId = staffId;
                    obj.Content = skillLevel.ToString();
                    obj.Type = "staff_skill_level";
                    obj.Status = 1;
                    db.Logs.AddOrUpdate(obj);
                    db.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Get log skill level
        /// </summary>
        /// <param name="staffId"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static List<cls_logSkillLevel> getLogSkillLevel(int staffId, Solution_30shineEntities db)
        {
            var list = new List<cls_logSkillLevel>();
            list = db.Database.SqlQuery<cls_logSkillLevel>(@"
                        select _log.*, skillLevel.Name as skillLevelName
                        from [Log] as _log
                        left join Tbl_SkillLevel as skillLevel
                        on skillLevel.Id = _log.Content
                        where _log.[OBJId] = " + staffId + @" and _log.[Type] = 'staff_skill_level' and _log.[Status] = 1
                    ").OrderBy(o => o.LogTime).ToList();
            return list;
        }

        public class cls_logSkillLevel : Log
        {
            public string skillLevelName { get; set; }
        }
    }
}