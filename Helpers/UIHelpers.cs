﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Text.RegularExpressions;

namespace _30shine.Helpers
{
    public class UIHelpers : System.Web.UI.Page
    {
        Solution_30shineEntities db = new Solution_30shineEntities();
        public void ReloadPage()
        {
            HttpRequest Request = HttpContext.Current.Request;
            string host = "http://" + Request.Url.Host.ToString();
            string rawUrl = Request.RawUrl.ToString();
            string Url = host + rawUrl;
            System.Web.HttpContext.Current.Response.Redirect(Url);
        }

        public static void Redirect(string url, List<KeyValuePair<string, string>> MsgParam = null)
        {
            string Url = url + "?";

            if (MsgParam != null)
            {
                foreach (var v in MsgParam)
                {
                    Url += v.Key + "=" + v.Value + "&";
                }
            }
            Url = Url.TrimEnd('&', '?');
            System.Web.HttpContext.Current.Response.Redirect(Url);
        }

        #region GenerateSlug
        public string GenerateSlug(string title)
        {
            if (title == null) return "";
            else title = title.ToLower();

            const int maxlen = 1000;
            int len = title.Length;
            bool prevdash = false;
            var sb = new System.Text.StringBuilder(len);
            char c;

            for (int i = 0; i < len; i++)
            {
                c = title[i];
                if ((c >= 'a' && c <= 'z') || (c >= '0' && c <= '9'))
                {
                    sb.Append(c);
                    prevdash = false;
                }
                else if (c >= 'A' && c <= 'Z')
                {
                    // tricky way to convert to lowercase
                    sb.Append((char)(c | 32));
                    prevdash = false;
                }
                else if (c == ' ' || c == ',' || c == '.' || c == '/' ||
                    c == '\\' || c == '-' || c == '_' || c == '=')
                {
                    if (!prevdash && sb.Length > 0)
                    {
                        sb.Append('-');
                        prevdash = true;
                    }
                }
                else if ((int)c >= 128)
                {
                    int prevlen = sb.Length;
                    sb.Append(RemapInternationalCharToAscii(c));
                    if (prevlen != sb.Length) prevdash = false;
                }
                if (i == maxlen) break;
            }

            if (prevdash)
                return sb.ToString().Substring(0, sb.Length - 1);
            else
                return sb.ToString();
        }
        #endregion

        #region RemapInternationalCharToAscii
        public static string RemapInternationalCharToAscii(char c)
        {
            string s = c.ToString().ToLowerInvariant();
            if ("áảàạãăắằẳẵặâấầẩẫậäå".Contains(s))
            {
                return "a";
            }
            else if ("éèẻẽẹêếềểễệë".Contains(s))
            {
                return "e";
            }
            else if ("íìỉĩịýỳỹỷỵÿîïı".Contains(s))
            {
                return "i";
            }
            else if ("óòõỏọôốồổỗộơớờởỡợõöøőð".Contains(s))
            {
                return "o";
            }
            else if ("úùủũụưứừửữựûüŭů".Contains(s))
            {
                return "u";
            }
            else if ("çćčĉ".Contains(s))
            {
                return "c";
            }
            else if ("żźž".Contains(s))
            {
                return "z";
            }
            else if ("śşšŝ".Contains(s))
            {
                return "s";
            }
            else if ("ñń".Contains(s))
            {
                return "n";
            }
            else if ("ğĝ".Contains(s))
            {
                return "g";
            }
            else if (c == 'ř')
            {
                return "r";
            }
            else if (c == 'ł')
            {
                return "l";
            }
            else if (c == 'đ')
            {
                return "d";
            }
            else if (c == 'ß')
            {
                return "ss";
            }
            else if (c == 'Þ')
            {
                return "th";
            }
            else if (c == 'ĥ')
            {
                return "h";
            }
            else if (c == 'ĵ')
            {
                return "j";
            }
            else
            {
                return "";
            }
        }
        #endregion

        #region LimitWords
        public string LimitWords(string Word, int number, string StringAfter = "")
        {
            string word = "";

            if (Word.Length > 0)
            {
                string[] Words = Word.Split(' ');
                number = Words.Length <= number ? Words.Length : number;

                for (int i = 0; i < number; i++)
                {
                    word += Words[i] + " ";
                }
                return word.TrimEnd(' ') + (Words.Length > number ? StringAfter : "");
            }
            else
            {
                return word;
            }
        }
        #endregion

        #region
        public static void TriggerJsMsgSystem(Page _OBJ, string msg, string status, int duration)
        {
            ScriptManager.RegisterStartupScript(_OBJ, _OBJ.GetType(), "Message System",
                                    "showMsgSystem('" + msg + "','" + status + "','msg-system'," + duration + ");", true);
        }
        /// <summary>
        /// Trả thông báo về client 
        /// </summary>
        /// <param name="_OBJ"></param>
        /// <param name="msg"></param>
        /// <param name="status"></param>
        /// <param name="duration"></param>
        public static void TriggerJsMsgSystem_v1(Page _OBJ, string msg, string status, int duration)
        {
            ScriptManager.RegisterStartupScript(_OBJ, _OBJ.GetType(), "Message System", "showMsgSystem('" + msg + "','" + status + "'," + duration + ");", true);
        }
        public static void RemoveLoading(Page obj)
        {
            ScriptManager.RegisterStartupScript(obj, obj.GetType(), "loading", "removeLoading();", true);
        }
        #endregion

        /// <summary>
        /// Find controls by class name
        /// </summary>
        /// <param name="controls"></param>
        /// <param name="CssClass"></param>
        /// <returns></returns>
        public static IEnumerable<Control> GetMarkedControls(ControlCollection controls, string CssClass)
        {
            foreach (Control c in controls)
            {
                if (c is WebControl)
                {
                    var wc = c as WebControl;
                    if (wc.CssClass.Contains(CssClass))
                        yield return c;
                }

                foreach (Control ic in GetMarkedControls(c.Controls, CssClass))
                    yield return ic;
            }
        }

        public static string GetUniqueKey(int maxSize, int minSize)
        {
            char[] chars = new char[62];
            string a;
            a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            chars = a.ToCharArray();
            int size = maxSize;
            byte[] data = new byte[1];
            System.Security.Cryptography.RNGCryptoServiceProvider crypto = new System.Security.Cryptography.RNGCryptoServiceProvider();
            crypto.GetNonZeroBytes(data);
            size = maxSize;
            data = new byte[size];
            crypto.GetNonZeroBytes(data);
            System.Text.StringBuilder result = new System.Text.StringBuilder(size);
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length - minSize)]);
            }
            return result.ToString();
        }

        public static int getBillOrder(string billCode, int orderLen)
        {
            if (billCode != null && billCode != "")
            {
                int integer;
                var code = billCode.Substring(billCode.Length - orderLen, orderLen);
                return int.TryParse(code, out integer) ? integer : 0;
            }
            else
            {
                return 0;
            }
        }

        public void Bind_Salon(DropDownList ddlSalon, int SalonId, Boolean Perm_AllData)
        {
            var _Salons = db.Tbl_Salon.Where(w => w.IsDelete != 1 && w.Publish == true).OrderBy(o => o.Id).ToList();
            var Key = 0;
            SalonId = Convert.ToInt32(Session["SalonId"]);

            ddlSalon.DataTextField = "Salon";
            ddlSalon.DataValueField = "Id";

            if (Perm_AllData)
            {
                ListItem item = new ListItem("Chọn Salon", "0");
                ddlSalon.Items.Insert(0, item);

                foreach (var v in _Salons)
                {
                    Key++;
                    item = new ListItem(v.Name, v.Id.ToString());
                    ddlSalon.Items.Insert(Key, item);
                }
                ddlSalon.SelectedIndex = 0;
            }
            else
            {
                foreach (var v in _Salons)
                {
                    if (v.Id == SalonId)
                    {
                        ListItem item = new ListItem(v.Name, v.Id.ToString());
                        ddlSalon.Items.Insert(Key, item);
                        ddlSalon.SelectedIndex = SalonId;
                        ddlSalon.Enabled = false;
                    }

                }
            }

        }

        public void BindService(DropDownList ddlService)
        {
            var _Service = db.Services.Where(w => w.IsDelete != 1 && w.Publish == 1).OrderBy(o => o.Id).ToList();
            var Key = 0;

            ddlService.DataTextField = "Name";
            ddlService.DataValueField = "Id";
            ListItem item = new ListItem("Chọn dịch vụ", "0");
            ddlService.Items.Insert(0, item);

            foreach (var v in _Service)
            {
                Key++;
                item = new ListItem(v.Name, v.Id.ToString());
                ddlService.Items.Insert(Key, item);
            }
            ddlService.SelectedIndex = 0;
        }

        public void BindRating(DropDownList ddlRating)
        {
            var rating = db.Customer_Rating.Where(w => w.IsDelete != true && w.Publish == true).OrderBy(o => o.Id).ToList();
            var Key = 0;

            ddlRating.DataTextField = "Name";
            ddlRating.DataValueField = "Id";
            ListItem item = new ListItem("Chọn xếp hạng", "0");
            ddlRating.Items.Insert(0, item);

            foreach (var v in rating)
            {
                Key++;
                item = new ListItem(v.Name, v.Id.ToString());
                ddlRating.Items.Insert(Key, item);
            }
            ddlRating.SelectedIndex = 0;
        }

        public void BindUuDai(DropDownList ddlUuDai)
        {
            var rating = db.Customer_UuDai.Where(w => w.IsDelete != true && w.Publish == true).OrderBy(o => o.Id).ToList();
            var Key = 0;

            ddlUuDai.DataTextField = "Name";
            ddlUuDai.DataValueField = "Id";
            ListItem item = new ListItem("Chọn ưu đãi", "0");
            ddlUuDai.Items.Insert(0, item);

            foreach (var v in rating)
            {
                Key++;
                item = new ListItem(v.Name, v.Id.ToString());
                ddlUuDai.Items.Insert(Key, item);
            }
            ddlUuDai.SelectedIndex = 0;
        }
        public static string StringReplace(string _string, string searchStr)
        {
            try
            {
                var strTest = _string;
                var test = searchStr;
                var viTriChuoiTim = strTest.IndexOf(test);
                var first = strTest.Substring(0, viTriChuoiTim);
                var last = strTest.Substring(viTriChuoiTim + test.Length);
                var t2 = new Regex(@"(\d)").Replace(first, @"x");
                var t3 = new Regex(@"(\d)").Replace(last, @"x");
                var t4 = t2 + test + t3;
                return t4;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
    
    public struct Msg
    {
        public bool success { get; set; }
        public string msg { get; set; }
        public object data { get; set; }
    }

    public struct ProductBasic
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int Price { get; set; }
        public int Quantity { get; set; }
    }
}