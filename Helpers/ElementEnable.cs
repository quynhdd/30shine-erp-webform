﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Helpers
{
    public class ElementEnable
    {
        public string ElementName { get; set; }
        public bool Enable { get; set; }
        public string Type { get; set; }
    }
}
