﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace Helpers
{
    public class Locale
    {
        protected CultureInfo culture;

        public static CultureInfo getCultureInfo( string locale = "vi-VN")
        {
            return new CultureInfo(locale);
        }
    }
}