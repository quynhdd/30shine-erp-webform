﻿using _30shine.Helpers.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace _30shine.Helpers
{
    public class PushNotiErrorToSlack
    {
        /// <summary>
        /// Push noti to slack
        /// </summary>
        /// <param name="listGroup"></param>
        /// <param name="strError"></param>
        /// <param name="MethodName"></param>
        /// <param name="Author"></param>
        public void Push(string listGroup, string strError, string MethodName, string Author, string project)
        {
            var groups = listGroup.Split(',');
            if (groups.Count() < 1)
            {
                groups = new string[] { listGroup };
            }
            Task StaticRatingWaitTime = Task.Run(async () =>
            {
                await new Request().RunPostAsync(
                    Libraries.AppConstants.URL_API_PUSH_NOTICE + "/api/pushNotice/slack",
                    new
                    {
                        list_group_id = groups,
                        team = "erp",
                        message = strError,
                        module_name = MethodName + "( " + Author + ", " + project + ")"
                    }
                );
            });
        }
        public void PushDefault(string strError, string MethodName, string Author)
        {
            Task StaticRatingWaitTime = Task.Run(async () =>
            {
                await new Request().RunPostAsync(
                    Libraries.AppConstants.URL_API_PUSH_NOTICE + "/api/pushNotice/slack",
                    new
                    {
                        list_group_id = new string[] {Libraries.AppConstants.GROUP_SLACK},
                        team = "ERP",
                        message = strError,
                        module_name = MethodName + "( " + Author + ", " + HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host + ")"
                    }
                );
            });
        }
    }
}