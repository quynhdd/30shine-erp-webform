﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Net;

namespace _30shine.Helpers.Http
{
    public class Request
    {
        protected HttpClient Client;
        protected Uri URI;
        public Request()
        {
            GetClient();
        }

        public HttpClient GetClient()
        {
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            if (Client == null)
            {
                Client = new HttpClient();
            }
            return Client;
        }

        /// <summary>
        /// Parse uri
        /// </summary>
        /// <param name="uriString"></param>
        protected void ParseURI(string uriString)
        {
            URI = new Uri(uriString);
            Client.BaseAddress = URI; //new Uri(URI.GetLeftPart(UriPartial.Authority));
            Client.DefaultRequestHeaders.Accept.Clear();
            Client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        /// <summary>
        /// Run get request async
        /// </summary>
        /// <param name="uriString"></param>
        /// <returns></returns>
        public Task<HttpResponseMessage> RunGetAsync(string uriString)
        {
            Task<HttpResponseMessage> response = null;
            try
            {
                ParseURI(uriString);
                response = Client.GetAsync(URI.AbsolutePath);
                response.Result.EnsureSuccessStatusCode();

            }
            catch (Exception e)
            {
                // Push thông báo cho mọi người(by slack) khi có lỗi xảy ra
                new Helpers.PushNotiErrorToSlack().PushDefault(e.Message + ", " + uriString + ", Inner" + e.InnerException, this.ToString() + ".RunGetAsync", "phucdn");
            }
            return response;
        }
        /// <summary>
        /// using form query
        /// </summary>
        /// <param name="uriString"></param>
        /// <returns></returns>
        public Task<HttpResponseMessage> RunGetAsyncV1(string uriString)
        {
            Task<HttpResponseMessage> response = null;
            try
            {
                ParseURI(uriString);
                response = Client.GetAsync(uriString);
                response.Result.EnsureSuccessStatusCode();
            }
            catch (Exception e)
            {
                // Push thông báo cho mọi người(by slack) khi có lỗi xảy ra
                new Helpers.PushNotiErrorToSlack().PushDefault(e.Message + ", " + uriString + ", Inner" + e.InnerException, this.ToString() + ".RunGetAsyncV1", "phucdn");
            }
            return response;
        }
        /// <summary>
        /// Run post request async
        /// </summary>
        /// <param name="uriString"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public Task<HttpResponseMessage> RunPostAsync(string uriString, object data)
        {
            Task<HttpResponseMessage> response = null;
            try
            {
                ParseURI(uriString);
                response = Client.PostAsJsonAsync(uriString, data);
                response.Result.EnsureSuccessStatusCode();
            }
            catch (Exception e)
            {
                // Push thông báo cho mọi người(by slack) khi có lỗi xảy ra
                new Helpers.PushNotiErrorToSlack().PushDefault(e.Message + ", " + uriString + Library.Function.JavaScript.Serialize(data) + ", Inner:" + e.InnerException, this.ToString() + ".RunPostAsync", "phucdn");
            }
            return response;
        }
        /// <summary>
        /// Run put request async
        /// </summary>
        /// <param name="uriString"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public Task<HttpResponseMessage> RunPutAsync(string uriString, object data)
        {
            Task<HttpResponseMessage> response = null;
            try
            {
                ParseURI(uriString);
                response = Client.PutAsJsonAsync(uriString, data);
                response.Result.EnsureSuccessStatusCode();
            }
            catch (Exception e)
            {
                // Push thông báo cho mọi người(by slack) khi có lỗi xảy ra
                new Helpers.PushNotiErrorToSlack().PushDefault(e.Message + ", " + uriString + Library.Function.JavaScript.Serialize(data) + ", Inner:"+ e.InnerException, this.ToString() + ".RunPutAsync", "phucdn");
            }
            return response;
        }
        /// <summary>
        /// Run delete request async
        /// </summary>
        /// <param name="uriString"></param>
        /// <returns></returns>
        public Task<HttpResponseMessage> RunDeleteAsync(string uriString)
        {
            Task<HttpResponseMessage> response = null;
            try
            {
                ParseURI(uriString);
                response = Client.DeleteAsync(URI.AbsolutePath);
                response.Result.EnsureSuccessStatusCode();
            }
            catch (Exception e)
            {
                // Push thông báo cho mọi người(by slack) khi có lỗi xảy ra
                new Helpers.PushNotiErrorToSlack().PushDefault(e.Message + ", " + uriString + ", Inner"+ e.InnerException, this.ToString() + ".RunDeleteAsync", "phucdn");
            }
            return response;
        }
        public Task<HttpResponseMessage> RunDeleteAsyncV1(string uriString)
        {
            Task<HttpResponseMessage> response = null;
            try
            {
                ParseURI(uriString);
                response = Client.DeleteAsync(uriString);
                response.Result.EnsureSuccessStatusCode();
            }
            catch (Exception e)
            {
                // Push thông báo cho mọi người(by slack) khi có lỗi xảy ra
                new Helpers.PushNotiErrorToSlack().PushDefault(e.Message + ", " + uriString + ", Inner" + e.InnerException, this.ToString() + ".RunDeleteAsyncV1", "phucdn");
            }
            return response;
        }
    }
}