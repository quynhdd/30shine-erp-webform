﻿using _30shine.MODEL.Bussiness;
using _30shine.MODEL.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace _30shine.Helpers.Http
{
    public class RequestAccessToken
    {
        protected HttpClient _httpClient;
        protected Uri _uri;
        private IAuthenticationModel model = new AuthenticationModel();
        //
        public RequestAccessToken()
        {
            GetClient();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public HttpClient GetClient()
        {
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            if (_httpClient == null)
            {
                _httpClient = new HttpClient();
            }

            return _httpClient;
        }

        /// <summary>
        /// Parse Uri
        /// </summary>
        /// <param name="uri"></param>
        protected void ParseURI(string uri, String token)
        {
            _uri = new Uri(uri);
            _httpClient.BaseAddress = _uri;
            _httpClient.DefaultRequestHeaders.Accept.Clear();
            _httpClient.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            _httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
        }

        /// <summary>
        /// Run put async
        /// </summary>
        /// <param name="uriString"></param>
        /// <param name="data"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public Task<HttpResponseMessage> RunPutAsync(string uriString, object data, String token)
        {
            Task<HttpResponseMessage> response = null;
            try
            {
                ParseURI(uriString, token);
                response = _httpClient.PutAsJsonAsync(uriString, data);
                response.Result.EnsureSuccessStatusCode();
            }
            catch (Exception e)
            {
                // Push thông báo cho mọi người(by slack) khi có lỗi xảy ra
                new PushNotiErrorToSlack().PushDefault(e.Message + ", " + uriString + Library.Function.JavaScript.Serialize(data) + ", StackTrace:" + e.StackTrace, this.ToString() + ".RunPutAsync", "phucdn");
            }

            return response;
        }
    }
}