﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using _30shine.MODEL.ENTITY.EDMX;
using LinqKit;
using System.Data.Entity.Migrations;

namespace _30shine.Helpers
{
    public class SalaryLib01
    {
        //==============================================
        // Store giá trị lương theo từng ngày
        //==============================================
        //==============================================
        // Khởi tạo dữ liệu store lương cho ngày hôm nay
        //==============================================
        /// <summary>
        /// Khởi tạo danh sách lương cho nhân viên hôm nay
        /// </summary>
        private void initSalaryToday()
        {
            if (!isInitSalaryToday())
            {
                using (var db = new Solution_30shineEntities())
                {
                    var obj = new FlowSalary();
                    var dayInMonth = DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.Month) - 2;
                    var staffs = db.Staffs.Where(w => w.IsDelete != 1 && w.Active == 1 && (w.isAccountLogin == null || w.isAccountLogin != 1)).ToList();
                    var payon = db.Tbl_Payon.Where(w => w.IsDelete != 1 && w.Publish == true).ToList();
                    if (staffs.Count > 0)
                    {
                        foreach (var v in staffs)
                        {
                            obj = new FlowSalary();
                            obj.staffId = v.Id;
                            obj.CreatedDate = DateTime.Now;
                            obj.IsDelete = 0;
                            obj.sDate = DateTime.Today;
                            /// Lương cứng
                            obj.fixSalary = 0;
                            /// Lương phụ cấp
                            obj.allowanceSalary = computeAllowanceSalary(dayInMonth, v, payon);
                            /// Lương part-time
                            obj.partTimeSalary = 0;
                            /// Lương dịch vụ
                            obj.serviceSalary = 0;
                            /// Lương mỹ phẩm
                            obj.productSalary = 0;
                            db.FlowSalaries.AddOrUpdate(obj);
                            db.SaveChanges();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Check đã khởi tạo lương hôm nay
        /// </summary>
        /// <returns></returns>
        private bool isInitSalaryToday()
        {
            var init = false;
            using (var db = new Solution_30shineEntities())
            {
                var salary = db.FlowSalaries.FirstOrDefault(w => w.sDate == DateTime.Today && w.IsDelete != 1);
                if (salary != null)
                {
                    init = true;
                }
            }
            return init;
        }

        /// <summary>
        /// Tính lương phụ cấp (thêm ngay khi khởi tạo danh sách lương hôm nay)
        /// </summary>
        /// <param name="dayInMonth"></param>
        /// <param name="staff"></param>
        /// <param name="payon"></param>
        /// <returns></returns>
        private float computeAllowanceSalary(int dayInMonth, Staff staff, List<Tbl_Payon> payon)
        {
            float salary = 0;
            if (dayInMonth > 0 && staff != null)
            {
                var config = payon.FirstOrDefault(w => w.TypeStaffId == staff.Type && w.ForeignId == staff.SkillLevel && w.Hint == "base_salary" && w.PayByTime == 4 && w.IsDelete != 1 && w.Publish == true);
                if (config != null)
                {
                    salary = (float)config.Value / dayInMonth;
                }
            }
            return salary;
        }

        //=========================================
        // Cập nhật flow salary khi thêm mới bill
        //=========================================
        public static void updateFlowSalaryByBillAdd(BillService bill, Solution_30shineEntities db)
        {
            if (bill != null)
            {
                var fs = new FlowSalary();
                var payon = db.Tbl_Payon.Where(w => w.IsDelete != 1).ToList();
                /// Cập nhật cho stylist
                /// Lương cứng + Lương phụ cấp( đã thêm trong init ) + Lương part-time + Lương dịch vụ + Lương mỹ phẩm
                fs = db.FlowSalaries.FirstOrDefault(w => w.staffId == bill.Staff_Hairdresser_Id && w.sDate == DateTime.Today && w.IsDelete != 1);
                if (fs != null)
                {
                    var staff = db.Staffs.FirstOrDefault(w => w.Id == bill.Staff_Hairdresser_Id);
                    // 1. Lương cứng
                    fs.fixSalary = computeFixSalary(staff, db, payon);
                    // 2. Lương part-time (làm thêm)
                    fs.partTimeSalary = computeParttimeSalary(staff, db, payon);
                    // 3. Lương dịch vụ
                    fs.serviceSalary += computeServiceSalary(staff, bill, db, payon);
                    // 4. Lương mỹ phẩm
                    if (bill.Staff_Hairdresser_Id == bill.SellerId)
                    {
                        fs.productSalary += computeProductSalary(bill, db);
                    }
                    db.FlowSalaries.AddOrUpdate(fs);
                    db.SaveChanges();
                }
                /// Cập nhật cho skinner
                /// Lương cứng + Lương phụ cấp( đã thêm trong init ) + Lương part-time + Lương dịch vụ + Lương mỹ phẩm
                fs = db.FlowSalaries.FirstOrDefault(w => w.staffId == bill.Staff_HairMassage_Id && w.sDate == DateTime.Today && w.IsDelete != 1);
                if (fs != null)
                {
                    var staff = db.Staffs.FirstOrDefault(w => w.Id == bill.Staff_HairMassage_Id);
                    // 1. Lương cứng
                    fs.fixSalary = computeFixSalary(staff, db, payon);
                    // 2. Lương part-time (làm thêm)
                    fs.partTimeSalary = computeParttimeSalary(staff, db, payon);
                    // 3. Lương dịch vụ
                    fs.serviceSalary += computeServiceSalary(staff, bill, db, payon);
                    // 4. Lương mỹ phẩm
                    if (bill.Staff_HairMassage_Id == bill.SellerId)
                    {
                        fs.productSalary += computeProductSalary(bill, db);
                    }
                    db.FlowSalaries.AddOrUpdate(fs);
                    db.SaveChanges();
                }
                /// Cập nhật cho check-in
                /// Lương cứng + Lương phụ cấp( đã thêm trong init ) + Lương part-time + Lương dịch vụ
                fs = db.FlowSalaries.FirstOrDefault(w => w.staffId == bill.ReceptionId && w.sDate == DateTime.Today && w.IsDelete != 1);
                if (fs != null)
                {
                    var staff = db.Staffs.FirstOrDefault(w => w.Id == bill.ReceptionId);
                    // 1. Lương cứng
                    fs.fixSalary = computeFixSalary(staff, db, payon);
                    // 1. Lương part-time (làm thêm)
                    fs.partTimeSalary = computeParttimeSalary(staff, db, payon);
                    // 3. Lương dịch vụ
                    fs.serviceSalary += computeServiceSalary(staff, bill, db, payon);
                    db.FlowSalaries.AddOrUpdate(fs);
                    db.SaveChanges();
                }
                /// Cập nhật cho check-out
                /// Lương cứng + Lương phụ cấp( đã thêm trong init ) + Lương part-time + Lương mỹ phẩm
                if (bill.SellerId != bill.Staff_Hairdresser_Id && bill.SellerId != bill.ReceptionId && bill.SellerId != bill.ReceptionId)
                {
                    fs = db.FlowSalaries.FirstOrDefault(w => w.staffId == bill.SellerId && w.sDate == DateTime.Today && w.IsDelete != 1);
                    if (fs != null)
                    {
                        var staff = db.Staffs.FirstOrDefault(w => w.Id == bill.SellerId);
                        // 1. Lương cứng
                        fs.fixSalary = computeFixSalary(staff, db, payon);
                        // 2. Lương part-time (làm thêm)
                        fs.partTimeSalary = computeParttimeSalary(staff, db, payon);
                        // 3. Lương mỹ phẩm
                        fs.productSalary += computeProductSalary(bill, db);
                    }
                    db.FlowSalaries.AddOrUpdate(fs);
                    db.SaveChanges();
                }
            }
        }
        /// <summary>
        /// Cập nhật lương dịch vụ
        /// Cho stylist và skinner
        /// </summary>
        private static float computeServiceSalary(Staff staff, BillService bill, Solution_30shineEntities db, List<Tbl_Payon> payon)
        {
            float salary = 0;
            int unit_skinner = 2500;
            int unit_checkin = 1000;
            // Chỉ tính lương dịch vụ cho stylist, skinner, check-in
            if (staff != null && bill != null)
            {
                // Chỉ áp dụng tính lương trên từng dịch vụ theo cấu hình cho stylist
                if (staff.Type == 1)
                {
                    var services = db.FlowServices.Where(w => w.BillId == bill.Id).ToList();
                    if (services.Count > 0)
                    {
                        foreach (var v in services)
                        {
                            var config = payon.FirstOrDefault(w => w.TypeStaffId == staff.Type && w.KeyId == v.Id && w.ForeignId == staff.SkillLevel && w.IsDelete != 1);
                            if (config != null && config.Value != null)
                            {
                                salary += (float)config.Value;
                            }
                        }
                    }
                }
                // Tính lương dịch vụ cho skinner
                else if (staff.Type == 2)
                {
                    var point = db.Rating_ConfigPoint.FirstOrDefault(w => w.RealPoint == bill.Mark && w.Status == 1 && w.IsDelete != 1 && w.Hint == 1);
                    if (point != null)
                    {
                        salary = (float)point.ConventionPoint * unit_skinner;
                    }
                }
                else if (staff.Type == 3)
                {
                    var point = db.Rating_ConfigPoint.FirstOrDefault(w => w.RealPoint == bill.Mark && w.Status == 1 && w.IsDelete != 1 && w.Hint == 1);
                    if (point != null)
                    {
                        salary = (float)point.ConventionPoint * unit_checkin;
                    }
                }
            }
            return salary;
        }

        /// <summary>
        /// Cập nhật lương mỹ phẩm
        /// </summary>
        /// <param name="staffId"></param>
        /// <param name="billId"></param>
        /// <param name="db"></param>
        private static double computeProductSalary(BillService bill, Solution_30shineEntities db)
        {
            double salary = 0;
            if (bill != null && bill.SellerId > 0)
            {
                string sql = @"select Coalesce(sum(fp.Price * fp.Quantity * (Cast(Coalesce(p.ForSalary, 0) as float)/CAST(100 as float))), 0) as salary
                                from FlowProduct as fp
                                left join Product as p
                                on fp.ProductId = p.Id
                                where fp.BillId = " + bill.Id;
                salary = db.Database.SqlQuery<double>(sql).FirstOrDefault();

            }
            return salary;
        }

        /// <summary>
        /// Cập nhật lương cứng
        /// </summary>
        /// <param name="staff"></param>
        /// <param name="payon"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        private static float computeFixSalary(Staff staff, Solution_30shineEntities db, List<Tbl_Payon> payon)
        {
            float salary = 0;
            if (staff != null && isEnroll(staff, db))
            {
                int dayInMonth = DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.Month) - 2;
                var config = payon.FirstOrDefault(w => w.TypeStaffId == staff.Type && w.ForeignId == staff.SkillLevel && w.Hint == "base_salary" && (w.PayByTime == 1 || w.PayByTime == 2) && w.IsDelete != 1 && w.Publish == true);
                if (config != null)
                {
                    // Trả lương cứng theo tháng
                    if (config.PayByTime == 1)
                    {
                        salary = (float)config.Value / dayInMonth;
                    }
                    // Trả lương cứng theo ngày
                    else if (config.PayByTime == 2)
                    {
                        salary = (float)config.Value;
                    }
                }
            }
            return salary;
        }

        /// <summary>
        /// Cập nhật lương part-time
        /// </summary>
        /// <param name="staffId"></param>
        /// <param name="billId"></param>
        /// <param name="db"></param>
        private static float computeParttimeSalary(Staff staff, Solution_30shineEntities db, List<Tbl_Payon> payon)
        {
            float salary = 0;
            if (staff != null)
            {
                var ftk = db.FlowTimeKeepings.FirstOrDefault(w => w.StaffId == staff.Id && w.WorkDate == DateTime.Today && w.IsDelete != 1);
                var config = payon.FirstOrDefault(w => w.TypeStaffId == staff.Type && w.ForeignId == staff.SkillLevel && w.PayByTime == 3 && w.IsDelete != 1);
                if (ftk != null && ftk.WorkHour != null && config != null && config.Value != null)
                {
                    salary = (float)ftk.WorkHour * (float)config.Value;
                }
            }
            return salary;
        }

        /// <summary>
        /// Kiểm tra nhân viên đã được chấm công hay chưa
        /// - Riêng đối với stylist và skinner, công được tính khi 1 ngày tham gia đc ít nhất 3 bill
        /// </summary>
        /// <param name="staffId"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        private static bool isEnroll(Staff staff, Solution_30shineEntities db)
        {
            bool enroll = false;
            string whereStaff = "";
            string sql = "";
            int bills = 0;
            if (staff != null)
            {
                switch (staff.Type)
                {
                    case 1:
                        whereStaff = " and Staff_Hairdresser_Id = " + staff.Id;
                        sql = @"select COUNT(*) as times
                                from BillService
                                where CreatedDate between '" + String.Format("{0:yyyy/MM/dd}", DateTime.Today) + "' and '" + String.Format("{0:yyyy/MM/dd}", DateTime.Today) + @"'
                                and IsDelete != 1 and Pending != 1
                                and ServiceIds != '' and ServiceIds is not null" + whereStaff;
                        bills = db.Database.SqlQuery<int>(sql).FirstOrDefault();
                        if (bills >= 3)
                        {
                            enroll = true;
                        }
                        break;
                    case 2:
                        whereStaff = " and Staff_HairMassage_Id = " + staff.Id;
                        sql = @"select COUNT(*) as times
                                from BillService
                                where CreatedDate between '" + String.Format("{0:yyyy/MM/dd}", DateTime.Today) + "' and '" + String.Format("{0:yyyy/MM/dd}", DateTime.Today) + @"'
                                and IsDelete != 1 and Pending != 1
                                and ServiceIds != '' and ServiceIds is not null" + whereStaff;
                        bills = db.Database.SqlQuery<int>(sql).FirstOrDefault();
                        if (bills >= 3)
                        {
                            enroll = true;
                        }
                        break;
                    default:
                        var ftk = db.FlowTimeKeepings.FirstOrDefault(w => w.StaffId == staff.Id && w.WorkDate == DateTime.Today && w.IsDelete != 1);
                        if (ftk != null && ftk.IsEnroll == true)
                        {
                            enroll = true;
                        }
                        break;
                }
            }
            return enroll;
        }

        //===================================================
        // Cập nhật salary flow khi sửa bill
        //===================================================
        public static void updateFlowSalaryByBillAdd()
        {

        }

        //===================================================
        // Cập nhật salary flow khi chấm công hoặc sửa đổi chấm công
        //===================================================
    }
}