﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Web;
using System.Windows.Forms;
using System.IO;
using System.Drawing;
using TheArtOfDev.HtmlRenderer.WinForms;
using TheArtOfDev.HtmlRenderer.Core;
using System.Text.RegularExpressions;
using System.Drawing.Printing;
using System.Globalization;

namespace _30shine.Helpers
{
    public class PrintWinform
    {
        /// <summary>
        /// Phần in sử dụng thư viện của winform dành cho phiếu dịch vụ - 30Shine
        /// </summary>
        Font Font = new Font("Arial", 13, GraphicsUnit.Pixel);
        private int x = 1;
        private int y = 2;
        private List<ProductBasic> ProductList = new List<ProductBasic>();
        private List<ProductBasic> ServiceList = new List<ProductBasic>();
        private string _CustomerCode = "";
        private string _CustomerName = "";
        private string Stylist = "";
        private string Skinner = "";
        private string Seller = "";
        protected int SalonId = 0;
        private List<int> lstFreeService = new List<int>();
        private void Printing()
        {
            PrintDocument pd = new PrintDocument();

            pd.PrinterSettings.PrinterName = "RP58 Printer";
            pd.OriginAtMargins = true;
            PaperSize pageSize = new PaperSize();
            //pageSize.RawKind = 512; //this is number of created custom size 563x1251
            //pageSize.Height = 50;
            Margins margins = new Margins(3, 0, 0, 0);
            pd.DefaultPageSettings.Margins = margins;
            pd.DefaultPageSettings.Landscape = false;
            pd.PrintPage += new PrintPageEventHandler(pd_PrintPage);
            pd.Print();
        }

        private void pd_PrintPage(object sender, PrintPageEventArgs e)
        {
            // Chiều ngang khổ in : 189px
            List<KeyValuePair<string, string>> items = new List<KeyValuePair<string, string>>();

            // Print logo
            System.Drawing.Image img = System.Drawing.Image.FromFile(@"C:\logo.jpg");
            var imgRec = new Rectangle(new Point((e.MarginBounds.Width - 100) / 2, y), new Size(100, 60));
            e.Graphics.DrawImage(img, imgRec);
            x = 1;
            y += 65;

            // Render top
            items = new List<KeyValuePair<string, string>>();
            items.Add(new KeyValuePair<string, string>("Ngày " + String.Format("{0:dd/MM/yyyy}", DateTime.Now), ""));
            Render_Table(e, Brushes.Black, Pens.White, items, 184, 0, 22);

            // Render Khách hàng
            items = new List<KeyValuePair<string, string>>();
            items.Add(new KeyValuePair<string, string>("KH", _CustomerName));
            items.Add(new KeyValuePair<string, string>("Mã KH", _CustomerCode));
            Render_Table(e, Brushes.Black, Pens.White, items, 70, 114, 22);

            // Render service table
            if (ServiceList != null && ServiceList.Count > 0)
            {
                var totalMoney = 0;
                items = new List<KeyValuePair<string, string>>();
                items.Add(new KeyValuePair<string, string>("Dịch vụ", "SL"));
                foreach (var v in ServiceList)
                {
                    totalMoney += v.Price * v.Quantity;
                    items.Add(new KeyValuePair<string, string>(v.Name, v.Quantity.ToString()));
                }
                //items.Add(new KeyValuePair<string, string>("Gội đầu MassageGội đầu Massage Gội đầu Massage", "1"));
                Render_Table(e, Brushes.Black, Pens.Black, items, 154, 30, 28);

                // Render stylist, skinner
                CultureInfo elGR = CultureInfo.CreateSpecificCulture("el-GR");
                items = new List<KeyValuePair<string, string>>();
                items.Add(new KeyValuePair<string, string>("Stylist", Stylist));
                items.Add(new KeyValuePair<string, string>("Skinner", ""));
                //items.Add(new KeyValuePair<string, string>("Thành tiền", String.Format(elGR, "{0:0,0}", totalMoney) + " VNĐ"));
                Render_Table(e, Brushes.Black, Pens.White, items, 70, 114, 22);
            }

            // Render product table            
            items = new List<KeyValuePair<string, string>>();
            items.Add(new KeyValuePair<string, string>("Sản phẩm", "SL"));
            items.Add(new KeyValuePair<string, string>(" ", ""));
            items.Add(new KeyValuePair<string, string>(" ", ""));
            items.Add(new KeyValuePair<string, string>(" ", ""));
            Render_Table(e, Brushes.Black, Pens.Black, items, 154, 30, 28);

            // Render seller, total money
            items = new List<KeyValuePair<string, string>>();
            items.Add(new KeyValuePair<string, string>("Seller", ""));
            items.Add(new KeyValuePair<string, string>("Tổng số", ""));
            Render_Table(e, Brushes.Black, Pens.White, items, 70, 114, 22);

        }

        /// <summary>
        /// Drawing table
        /// </summary>
        /// <param name="e">PrintPageEventArgs e</param>
        /// <param name="items">List<KeyValuePair<string, string>> items</param>
        /// <param name="keyWidth">td width for key</param>
        /// <param name="valueWidth">td width for value</param>
        /// <param name="height">td height</param>
        public void Render_Table(PrintPageEventArgs e, Brush brush, Pen pen, List<KeyValuePair<string, string>> items, int keyWidth, int valueWidth, int height)
        {
            if (items.Count > 0)
            {
                // Construct 2 new StringFormat objects
                StringFormat format1 = new StringFormat(StringFormatFlags.NoClip);
                StringFormat format2 = new StringFormat(format1);

                // Set the LineAlignment and Alignment properties for
                // both StringFormat objects to different values.
                format1.LineAlignment = StringAlignment.Center;
                format1.Alignment = StringAlignment.Near;
                format2.LineAlignment = StringAlignment.Center;
                format2.Alignment = StringAlignment.Far;

                Rectangle rec1 = new Rectangle();
                Rectangle rec2 = new Rectangle();
                Size size1 = new Size();
                Size size2 = new Size();
                int numline = 1;

                if (items.Count > 0)
                {
                    // Remap data
                    var Items = new List<PrintRec>();
                    var Item = new PrintRec();
                    foreach (var v in items)
                    {
                        Item = new PrintRec();
                        Item.Key = v.Key;
                        Item.Value = v.Value;
                        numline = Divide_Line(e, Font, v.Key, keyWidth);
                        Item.Height = numline > 1 ? (int)Math.Ceiling((double)height * numline * 0.7) : height * numline;
                        Items.Add(Item);
                    }

                    // Draw content
                    foreach (var v in Items)
                    {
                        size1 = new Size(keyWidth, v.Height);
                        size2 = new Size(valueWidth, v.Height);
                        rec1 = new Rectangle(new Point(x, y), size1);
                        rec2 = new Rectangle(new Point(x + keyWidth, y), size2);

                        e.Graphics.DrawRectangle(pen, rec1);
                        e.Graphics.DrawRectangle(pen, rec2);
                        e.Graphics.DrawString(v.Key, Font,
                            brush, (RectangleF)rec1, format1);
                        e.Graphics.DrawString(v.Value, Font,
                            brush, (RectangleF)rec2, format2);

                        x = 1;
                        y += v.Height;
                    }
                    y++;
                }
            }
        }

        /// <summary>
        /// Divide lines width long string
        /// </summary>
        /// <param name="e"></param>
        /// <param name="font"></param>
        /// <param name="str"></param>
        /// <param name="boundWidth"></param>
        /// <returns></returns>
        public int Divide_Line(PrintPageEventArgs e, Font font, string str, int boundWidth)
        {
            int numberLine = 1;
            if ((int)e.Graphics.MeasureString("a", font).Width > 0)
            {
                int charsWidth = (int)e.Graphics.MeasureString(str, font).Width;
                numberLine = (int)Math.Ceiling((double)charsWidth / boundWidth);
            }
            return numberLine;
        }
    }

    public struct PrintRec
    {
        public string Key { get; set; }
        public string Value { get; set; }
        public int Height { get; set; }
    }
}