﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using _30shine.MODEL.ENTITY.EDMX;
using LinqKit;
using System.Data.Entity.Migrations;
using System.Threading.Tasks;

namespace _30shine.Helpers
{
    public class SalaryLib
    {
        private static SalaryLib instance;

        public SalaryLib()
        {
            //
        }

        public static SalaryLib Instance
        {
            get
            {
                return new SalaryLib();
            }            
        }

        /// <summary>
        /// Khởi tạo danh sách lương cho nhân viên hôm nay
        /// </summary>
        public void initSalaryToday(DateTime date)
        {
            if (!isInitSalaryToday(date))
            {
                using (var db = new Solution_30shineEntities())
                {
                    var obj = new FlowSalary();
                    var sql = @"select * from Staff
                            where (select Count(*) from FlowSalary where IsDelete != 1 and sDate ='" + date + "' and staffId = Staff.Id) = 0"
                         + " and IsDelete != 1 and Active = 1 and (isAccountLogin is null or isAccountLogin != 1) ";
                    var staffs = db.Database.SqlQuery<Staff>(sql).ToList();
                    //var staffs = db.Staffs.Where(w => w.IsDelete != 1 && w.Active == 1 && (w.isAccountLogin == null || w.isAccountLogin != 1)).ToList();
                    var payon = db.Tbl_Payon.Where(w => w.IsDelete != 1 && w.Publish == true).ToList();
                    if (staffs.Count > 0)
                    {
                        foreach (var v in staffs)
                        {
                            obj = new FlowSalary();
                            obj.staffId = v.Id;
                            obj.CreatedDate = DateTime.Now;
                            obj.IsDelete = 0;
                            obj.sDate = date;
                            /// Lương cứng
                            obj.fixSalary = computeFixSalary(v, date);
                            /// Lương phụ cấp
                            obj.allowanceSalary = computeAllowanceSalary(v, date);
                            /// Lương part-time
                            obj.partTimeSalary = 0;
                            /// Lương dịch vụ
                            obj.serviceSalary = 0;
                            /// Lương mỹ phẩm
                            obj.productSalary = 0;

                            obj.ratingPoint = 0;
                            obj.bill_normal = 0;
                            obj.bill_normal_bad = 0;
                            obj.bill_normal_good = 0;
                            obj.bill_normal_great = 0;
                            obj.bill_normal_norating = 0;

                            obj.bill_special = 0;
                            obj.bill_special_bad = 0;
                            obj.bill_special_good = 0;
                            obj.bill_special_great = 0;
                            obj.bill_special_norating = 0;

                            obj.Mistake_Point = 0;
                            obj.waitTimeMistake = 0;

                            db.FlowSalaries.AddOrUpdate(obj);
                            db.SaveChanges();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Check đã khởi tạo lương hôm nay
        /// </summary>
        /// <returns></returns>
        private bool isInitSalaryToday(DateTime date)
        {
            var init = false;
            using (var db = new Solution_30shineEntities())
            {
                var sql = @"select * from Staff
                            where (select Count(*) from FlowSalary where IsDelete != 1 and sDate ='" + date + "' and staffId = Staff.Id) = 0"
                          + " and IsDelete != 1 and Active = 1 and (isAccountLogin is null or isAccountLogin != 1) ";
                //var salary = db.FlowSalaries.FirstOrDefault(w => w.sDate == date && w.IsDelete != 1);
                var salary = db.Database.SqlQuery<Staff>(sql).ToList();
                if (salary == null)
                {
                    init = true;
                }
            }
            return init;
        }

        /// <summary>
        /// Check đã khởi tạo record statistic lương theo nhân viên hay chưa
        /// </summary>
        /// <param name="staffId"></param>
        /// <param name="date"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        private bool isInitSalaryTodayByStaff(int staffId, DateTime date)
        {
            using (var db = new Solution_30shineEntities())
            {
                var init = false;
                var salary = db.FlowSalaries.FirstOrDefault(w => w.staffId == staffId && w.sDate == date && w.IsDelete != 1);
                if (salary != null)
                {
                    init = true;
                }
                return init;
            }            
        }

        /// <summary>
        /// Khởi tạo record statistic lương theo nhân viên ID
        /// </summary>
        /// <param name="staffId"></param>
        /// <param name="date"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public FlowSalary initSalaryByStaffId(int staffId, DateTime date, int salonId)
        {
            using (var db = new Solution_30shineEntities())
            {
                var fs = new FlowSalary();
                if (staffId > 0)
                {
                    var staff = db.Staffs.FirstOrDefault(w => w.Id == staffId);
                    fs.staffId = staff.Id;
                    fs.CreatedDate = DateTime.Now;
                    fs.IsDelete = 0;
                    fs.sDate = date;
                    /// Lương cứng
                    fs.fixSalary = computeFixSalary(staff, date);
                    /// Lương phụ cấp
                    fs.allowanceSalary = computeAllowanceSalary(staff, date);
                    /// Lương part-time
                    fs.partTimeSalary = 0;
                    /// Lương dịch vụ
                    fs.serviceSalary = 0;
                    /// Lương mỹ phẩm
                    fs.productSalary = 0;
                    fs.SalonId = salonId;
                    db.FlowSalaries.AddOrUpdate(fs);
                    db.SaveChanges();
                }
                return fs;
            }            
        }

        /// <summary>
        /// Tính lương phụ cấp (thêm ngay khi khởi tạo danh sách lương hôm nay)
        /// </summary>
        /// <param name="dayInMonth"></param>
        /// <param name="staff"></param>
        /// <param name="payon"></param>
        /// <returns></returns>
        private float computeAllowanceSalary(Staff staff, DateTime date)
        {
            using (var db = new Solution_30shineEntities())
            {
                float salary = 0;
                var dayInMonth = DateTime.DaysInMonth(date.Year, date.Month) - 2;
                if (dayInMonth > 0 && staff != null)
                {
                    int payonValue = getPayonValue(staff, 4);
                    if (payonValue > 0)
                    {
                        salary = (float)payonValue / dayInMonth;
                    }
                }
                return salary;
            }
        }

        //=========================================
        // Cập nhật flow salary khi thêm mới bill
        //=========================================
        public void updateFlowSalaryByBill(BillService bill, bool isVoucherLongTime = false)
        {
            using (var db = new Solution_30shineEntities())
            {
                if (bill != null && bill.CompleteBillTime != null)
                {
                    var date = bill.CompleteBillTime.Value.Date;
                    var fs = new FlowSalary();
                    var staff = new Staff();
                    var payon = db.Tbl_Payon.Where(w => w.IsDelete != 1).ToList();
                    var rating = new Library.Class.cls_rating();

                    /// Cập nhật cho stylist
                    /// Lương cứng + Lương phụ cấp( đã thêm trong init ) + Lương part-time + Lương dịch vụ + Lương mỹ phẩm
                    fs = db.FlowSalaries.FirstOrDefault(w => w.staffId == bill.Staff_Hairdresser_Id && w.sDate == date && w.IsDelete != 1);
                    if (fs == null && bill.Staff_Hairdresser_Id > 0)
                    {
                        fs = initSalaryByStaffId(bill.Staff_Hairdresser_Id.Value, bill.CompleteBillTime.Value.Date, Convert.ToInt32(bill.SalonId));
                    }
                    if (fs != null && fs.Id > 0)
                    {
                        staff = db.Staffs.FirstOrDefault(w => w.Id == bill.Staff_Hairdresser_Id);
                        // 1. Lương cứng
                        fs.fixSalary = computeFixSalary(staff, date);
                        // 2. Lương part-time (làm thêm)
                        //fs.partTimeSalary = computeParttimeSalary(staff, db, date);
                        // 3. Điểm rating
                        rating = computeRatingPoint(staff, date, bill);
                        fs.ratingPoint = rating.point;
                        fs.bill_normal = rating.bill_normal;
                        fs.bill_normal_great = rating.bill_normal_great;
                        fs.bill_normal_good = rating.bill_normal_good;
                        fs.bill_normal_bad = rating.bill_normal_bad;
                        fs.bill_normal_norating = rating.bill_normal_norating;
                        fs.bill_special = rating.bill_special;
                        fs.bill_special_great = rating.bill_special_great;
                        fs.bill_special_good = rating.bill_special_good;
                        fs.bill_special_bad = rating.bill_special_bad;
                        fs.bill_special_norating = rating.bill_special_norating;
                        // 4. Lương dịch vụ
                        fs.serviceSalary = computeServiceSalary(bill, staff, fs.ratingPoint.Value);
                        // 5. Lương mỹ phẩm
                        if (bill.Staff_Hairdresser_Id == bill.SellerId)
                        {
                            fs.productSalary = computeProductSalary(bill, date);
                        }
                        fs.ModifiedDate = DateTime.Now;
                        db.FlowSalaries.AddOrUpdate(fs);
                        db.SaveChanges();
                    }

                    /// Cập nhật cho skinner
                    /// Lương cứng + Lương phụ cấp( đã thêm trong init ) + Lương part-time + Lương dịch vụ + Lương mỹ phẩm
                    fs = db.FlowSalaries.FirstOrDefault(w => w.staffId == bill.Staff_HairMassage_Id && w.sDate == date && w.IsDelete != 1);
                    if (fs == null && bill.Staff_HairMassage_Id > 0)
                    {
                        fs = initSalaryByStaffId(bill.Staff_HairMassage_Id.Value, bill.CompleteBillTime.Value.Date, Convert.ToInt32(bill.SalonId));
                    }
                    if (fs != null && fs.Id > 0)
                    {
                        staff = db.Staffs.FirstOrDefault(w => w.Id == bill.Staff_HairMassage_Id);
                        // 1. Lương cứng
                        fs.fixSalary = computeFixSalary(staff, date);
                        // 2. Lương part-time (làm thêm)
                        //fs.partTimeSalary = computeParttimeSalary(staff, db, date);
                        // 3. Điểm rating
                        rating = computeRatingPoint(staff, date, bill);
                        fs.ratingPoint = rating.point;
                        fs.bill_normal = rating.bill_normal;
                        fs.bill_normal_great = rating.bill_normal_great;
                        fs.bill_normal_good = rating.bill_normal_good;
                        fs.bill_normal_bad = rating.bill_normal_bad;
                        fs.bill_normal_norating = rating.bill_normal_norating;
                        fs.bill_special = rating.bill_special;
                        fs.bill_special_great = rating.bill_special_great;
                        fs.bill_special_good = rating.bill_special_good;
                        fs.bill_special_bad = rating.bill_special_bad;
                        fs.bill_special_norating = rating.bill_special_norating;
                        // 4. Lương dịch vụ
                        fs.serviceSalary = computeServiceSalary(bill, staff, fs.ratingPoint.Value);
                        // 5. Lương mỹ phẩm
                        if (bill.Staff_HairMassage_Id == bill.SellerId)
                        {
                            fs.productSalary = computeProductSalary(bill, date);
                        }
                        fs.ModifiedDate = DateTime.Now;
                        db.FlowSalaries.AddOrUpdate(fs);
                        db.SaveChanges();
                    }

                    /// Cập nhật cho check-in
                    /// Lương cứng + Lương phụ cấp( đã thêm trong init ) + Lương part-time + Lương dịch vụ
                    fs = db.FlowSalaries.FirstOrDefault(w => w.staffId == bill.ReceptionId && w.sDate == date && w.IsDelete != 1);
                    if (fs == null && bill.ReceptionId > 0)
                    {
                        fs = initSalaryByStaffId(bill.ReceptionId.Value, bill.CompleteBillTime.Value.Date, Convert.ToInt32(bill.SalonId));
                    }
                    if (fs != null && fs.Id > 0)
                    {
                        staff = db.Staffs.FirstOrDefault(w => w.Id == bill.ReceptionId);
                        // 1. Lương cứng
                        fs.fixSalary = computeFixSalary(staff, date);
                        // 2. Lương part-time (làm thêm)
                        //fs.partTimeSalary = computeParttimeSalary(staff, db, date);
                        // 3. Lương doanh số 
                        fs.serviceSalary = UpdateReceptionSalary(date, date.AddDays(+1), bill.ReceptionId);
                        // 4. 30shine care
                        if (!isVoucherLongTime)
                        {
                            // 5. Điểm rating
                            rating = computeRatingPoint(staff, date, bill);
                            fs.ratingPoint = rating.point;
                            fs.bill_normal = rating.bill_normal;
                            fs.bill_normal_great = rating.bill_normal_great;
                            fs.bill_normal_good = rating.bill_normal_good;
                            fs.bill_normal_bad = rating.bill_normal_bad;
                            fs.bill_normal_norating = rating.bill_normal_norating;
                            fs.bill_special = rating.bill_special;
                            fs.bill_special_great = rating.bill_special_great;
                            fs.bill_special_good = rating.bill_special_good;
                            fs.bill_special_bad = rating.bill_special_bad;
                            fs.bill_special_norating = rating.bill_special_norating;
                        }

                        fs.ModifiedDate = DateTime.Now;
                        db.FlowSalaries.AddOrUpdate(fs);
                        db.SaveChanges();
                    }

                    /// Cập nhật cho check-out
                    /// Lương cứng + Lương phụ cấp( đã thêm trong init ) + Lương part-time + Lương mỹ phẩm
                    if (Array.IndexOf(new int[] { bill.Staff_Hairdresser_Id.Value, bill.Staff_HairMassage_Id.Value, bill.ReceptionId.Value }, bill.SellerId) == -1)
                    {
                        fs = db.FlowSalaries.FirstOrDefault(w => w.staffId == bill.SellerId && w.sDate == date && w.IsDelete != 1);
                        if (fs == null && bill.SellerId > 0)
                        {
                            fs = initSalaryByStaffId(bill.SellerId.Value, bill.CompleteBillTime.Value.Date, Convert.ToInt32(bill.SalonId));
                        }
                        if (fs != null && fs.Id > 0)
                        {
                            staff = db.Staffs.FirstOrDefault(w => w.Id == bill.SellerId);
                            // 1. Lương cứng
                            fs.fixSalary = computeFixSalary(staff, date);
                            // 2. Lương part-time (làm thêm)
                            //fs.partTimeSalary = computeParttimeSalary(staff, db, date);
                            // 3. Lương mỹ phẩm
                            fs.productSalary = computeProductSalary(bill, date);
                            fs.ModifiedDate = DateTime.Now;
                            db.FlowSalaries.AddOrUpdate(fs);
                            db.SaveChanges();
                        }
                    }

                    /// Cập nhật cho TSL
                    /// 1. Xác định TSL là ai
                    /// 2. Cập nhật dòng lương cho TSL
                    var salonManager = db.Staffs.FirstOrDefault(w => w.SalonId == bill.SalonId && w.IsDelete != 1 && w.Active == 1 && w.Permission == "salonmanager");
                    if (salonManager != null)
                    {
                        fs = db.FlowSalaries.FirstOrDefault(w => w.staffId == salonManager.Id && w.sDate == date && w.IsDelete != 1);
                        if (fs == null && salonManager.Id > 0)
                        {
                            fs = initSalaryByStaffId(salonManager.Id, bill.CompleteBillTime.Value.Date, Convert.ToInt32(bill.SalonId));
                        }
                        if (fs != null && fs.Id > 0)
                        {
                            // 1. Lương cứng
                            //fs.fixSalary = computeFixSalary(salonManager, db, date);
                            // 2. Lương dịch vụ
                            fs.serviceSalary = computeServiceSalaryByCustomerReturn(bill, salonManager);
                            fs.ModifiedDate = DateTime.Now;
                            db.FlowSalaries.AddOrUpdate(fs);
                            db.SaveChanges();
                        }
                    }

                    /// Cập nhật cho Quỹ lương doanh số
                    fs = db.FlowSalaries.FirstOrDefault(w => w.staffId == 364 && w.sDate == date && w.IsDelete != 1);
                    if (fs == null)
                    {
                        fs = initSalaryByStaffId(364, bill.CompleteBillTime.Value.Date, Convert.ToInt32(bill.SalonId));
                    }
                    if (fs != null && fs.Id > 0)
                    {
                        staff = db.Staffs.FirstOrDefault(w => w.Id == 364);
                        // 1. Lương dịch vụ
                        fs.serviceSalary = computeServiceSalaryTotalBill(bill, staff);
                        fs.ModifiedDate = DateTime.Now;
                        db.FlowSalaries.AddOrUpdate(fs);
                        db.SaveChanges();
                    }

                }
            }
        }

        /// <summary>
        /// Thống kê doanh số lương checkin
        /// </summary>
        /// <param name="timeFrom"></param>
        /// <param name="timeTo"></param>
        /// <param name="staffId"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        private double UpdateReceptionSalary(DateTime timeFrom, DateTime timeTo, int? staffId)
        {
            using (var db = new Solution_30shineEntities())
            {
                double salary = 0;
                try
                {
                    if (staffId != null)
                    {
                        var totalMoney = db.Store_GetWaitTimeByStaffId_V2(timeFrom, timeTo, staffId).FirstOrDefault();
                        salary = (double)totalMoney.TotalMoney;
                    }
                }
                catch { }

                return salary;
            }            
        }
        /// <summary>
        /// Cập nhật lương dịch vụ
        /// Chỉ tính lương dịch vụ cho stylist, skinner, check-in
        /// </summary>
        /// <param name="staff"></param>
        /// <param name="bill"></param>
        /// <param name="db"></param>
        /// <param name="payon"></param>
        /// <returns></returns>
        private double computeServiceSalary(BillService bill, Staff staff, double ratingPoint)
        {
            using (var db = new Solution_30shineEntities())
            {
                double salary = 0;
                DateTime WorkDate = DateTime.Now;
                if (staff != null)
                {
                    salary = (double)ratingPoint * getPayonValue(staff, 5);
                }

                return salary;
            }
        }
        /// <summary>
        /// Hàm kiểm Lấy điểm hài lòng theo tăng ca mới.
        /// </summary>
        /// <param name="bill"></param>
        /// <param name="staff"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public double GetRatingPointVer2(BillService bill, Staff staff)
        {
            using (var db = new Solution_30shineEntities())
            {
                double? ratingPoint = 0;
                try
                {
                    bool isX2 = false;
                    DateTime BillCreateTime = bill.CreatedDate.Value;
                    DateTime BillCompleteTime = bill.CompleteBillTime.Value.Date;
                    DateTime BillCompleteTimeTo = BillCompleteTime.AddDays(1);
                    //Lấy hệ số tăng ca mới
                    cls_configPartTime GetCoeffientNew = configPartTime(staff.Id, BillCreateTime.Date);
                    //Lấy hệ số điểm hài lòng của stylist
                    if (staff.Type == 1)
                    {
                        var dataBill = (from a in db.BillServices
                                        join b in db.FlowServices on a.Id equals b.BillId
                                        join c in db.Rating_ConfigPoint on a.Mark equals c.RealPoint
                                        where a.IsDelete == 0 && a.Pending == 0 && a.ServiceIds != "" && a.Staff_Hairdresser_Id == staff.Id
                                              && a.CompleteBillTime >= BillCompleteTime && a.CompleteBillTime <= BillCompleteTimeTo &&
                                              b.IsDelete == 0 && c.Hint == 1 && c.IsDelete == 0 && c.Status == 1
                                        select new
                                        {
                                            CompleteBillTime = a.CompleteBillTime,
                                            Id = a.Id,
                                            BookingId = a.BookingId,
                                            CreatedDate = a.CreatedDate,
                                            Mark = a.Mark,
                                            Quanlity = b.Quantity,
                                            CoefficientRating = b.CoefficientRating,
                                            IsX2 = a.IsX2,
                                            ConventionPoint = c.ConventionPoint
                                        }).ToList();
                        foreach (var Bills in dataBill)
                        {
                            if (checkHourPartTime(Bills.BookingId.Value, staff, Bills.CreatedDate.Value))
                            {
                                ratingPoint += Bills.Quanlity * Bills.CoefficientRating * Bills.ConventionPoint * GetCoeffientNew.CoefficientStylist;
                                if (Bills.Id == bill.Id)
                                {
                                    isX2 = true;
                                }
                            }
                            else
                            {
                                ratingPoint += Bills.Quanlity * Bills.CoefficientRating * Bills.ConventionPoint;
                            }
                        }
                    }
                    //Lấy hệ số điểm hài lòng của Skinner
                    else if (staff.Type == 2)
                    {
                        var dataBill = (from a in db.BillServices
                                        join b in db.FlowServices on a.Id equals b.BillId
                                        join c in db.Rating_ConfigPoint on a.Mark equals c.RealPoint
                                        where a.IsDelete == 0 && a.Pending == 0 && a.ServiceIds != "" && a.Staff_HairMassage_Id == staff.Id
                                              && a.CompleteBillTime >= BillCompleteTime && a.CompleteBillTime <= BillCompleteTimeTo &&
                                              b.IsDelete == 0 && c.Hint == 1 && c.IsDelete == 0 && c.Status == 1
                                        select new
                                        {
                                            CompleteBillTime = a.CompleteBillTime,
                                            Id = a.Id,
                                            BookingId = a.BookingId,
                                            CreatedDate = a.CreatedDate,
                                            Mark = a.Mark,
                                            Quanlity = b.Quantity,
                                            CoefficientRating = b.CoefficientRating,
                                            IsX2 = a.IsX2,
                                            ConventionPoint = c.ConventionPoint
                                        }).ToList();
                        foreach (var Bills in dataBill)
                        {
                            if (checkHourPartTime(Bills.BookingId.Value, staff, Bills.CreatedDate.Value))
                            {
                                ratingPoint += Bills.Quanlity * Bills.CoefficientRating * Bills.ConventionPoint * GetCoeffientNew.coefficinetSkinner;
                                if (Bills.Id == bill.Id)
                                {
                                    isX2 = true;
                                }
                            }
                            else
                            {
                                ratingPoint += Bills.Quanlity * Bills.CoefficientRating * Bills.ConventionPoint;
                            }
                        }

                    }

                    // Update IsX2
                    bill.IsX2 = isX2;
                    db.BillServices.AddOrUpdate(bill);
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    //throw ex;
                }

                return ratingPoint.Value;
            }
        }

        /// <summary>
        /// Kiểm tra stylist,skinner có trong giờ tăng ca hay không.
        /// </summary>
        /// <param name="staff"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        private bool checkHourPartTime(int BookingId, Staff staff, DateTime WorkDate)
        {
            using (var db = new Solution_30shineEntities())
            {
                bool check = false;
                //TimeSpan WorkHour = new TimeSpan(IntHour, IntMinute, 0);
                int StaffId = staff.Id;
                try
                {
                    var WorkTime = db.FlowTimeKeepings.Where(r => r.IsDelete == 0 && r.IsEnroll == true && r.WorkDate == WorkDate.Date && r.StaffId == StaffId).FirstOrDefault();

                    if (WorkTime != null)
                    {
                        //Lay khung gio lam viec
                        int WorkTimeId = WorkTime.WorkTimeId.Value;
                        //Lay so gio tang ca
                        int WorkHour = WorkTime.WorkHour.Value;
                        //Lay chuoi gio tang ca
                        string[] strHourIds = WorkTime.HourIds.Split(',');
                        //var test = strHourIds.Length;
                        //int bookhourId = Convert.ToInt32(strHourIds[strHourIds.Length-1]);
                        int[] intHourIds = new int[strHourIds.Length];
                        for (int i = 0; i < strHourIds.Length; i++)
                        {
                            intHourIds[i] = strHourIds[i] != "" ? Convert.ToInt32(strHourIds[i]) : 0;
                        }
                        // kiem tra chuoi id gio, co gio tang ca hay khong
                        //TimeSpan BookHour = db.BookHours.Where(r => r.Id == bookhourId && r.IsDelete == 0).Select(r=>r.HourFrame).FirstOrDefault().Value;
                        //if (BookHour != null)
                        //{
                        var BookHourFrame = (
                                                   from b in db.Bookings
                                                   join a in db.BookHours on b.HourId equals a.Id
                                                   where a.IsDelete == 0 && a.Publish == true && b.IsDelete == 0 && b.Id == BookingId
                                                   select new
                                                   {
                                                       HourFrame = a.HourFrame,
                                                       HourId = a.Id
                                                   }).FirstOrDefault();


                        if (BookHourFrame != null)
                        {
                            var WorkTimeHour = db.WorkTimes.Where(r => r.Id == WorkTimeId).FirstOrDefault();
                            if (WorkTimeHour != null)
                            {
                                if (BookHourFrame.HourFrame < WorkTimeHour.StrartTime || BookHourFrame.HourFrame > WorkTimeHour.EnadTime)
                                {
                                    check = Array.IndexOf(intHourIds, BookHourFrame.HourId) != -1 ? true : false;
                                }
                            }
                        }
                        //}

                    }

                }
                catch (Exception ex)
                {
                    check = false;
                }
                return check;
            }
        }

        /// <summary>
        /// Cập nhật lương dịch vụ cho TSL (= số khách quay lại * configPayon)
        /// </summary>
        /// <param name="staff"></param>
        /// <param name="bill"></param>
        /// <param name="db"></param>
        /// <param name="payon"></param>
        /// <returns></returns>
        private double computeServiceSalaryByCustomerReturn(BillService bill, Staff staff)
        {
            using (var db = new Solution_30shineEntities())
            {
                double salary = 0;
                if (bill != null)
                {
                    int salonId = bill.SalonId.Value;
                    string timeFrom = String.Format("{0:yyyy/MM/dd}", bill.CreatedDate);
                    string timeTo = String.Format("{0:yyyy/MM/dd}", bill.CreatedDate.Value.AddDays(1));
                    int customerReturn = db.Store_Customer_Return(salonId, timeFrom, timeTo).FirstOrDefault().Value;
                    salary = (double)customerReturn * getPayonValue(staff, 7);
                }
                return salary;
            }
        }

        private double computeServiceSalaryTotalBill(BillService bill, Staff staff)
        {
            using (var db = new Solution_30shineEntities())
            {
                double salary = 0;
                if (bill != null)
                {
                    DateTime timeFrom = bill.CompleteBillTime.Value.Date;
                    DateTime timeTo = bill.CompleteBillTime.Value.Date.AddDays(1);
                    int bills = db.BillServices.Where(w => w.CompleteBillTime >= timeFrom && w.CompleteBillTime < timeTo && w.IsDelete != 1 && w.Pending != 1 && w.ServiceIds != "" && w.ServiceIds != null).Count();
                    salary = (double)bills * getPayonValue(staff, 6);
                }
                return salary;
            }            
        }

        private double computeServiceSalaryByBill(BillService bill, Staff staff)
        {
            using (var db = new Solution_30shineEntities())
            {
                double salary = 0;
                if (bill != null)
                {
                    int salonId = bill.SalonId.Value;
                    DateTime timeFrom = bill.CompleteBillTime.Value.Date;
                    DateTime timeTo = timeFrom.AddDays(1);
                    int records = db.BillServices.Where(w => w.IsDelete != 1 && w.Pending != 1 && w.CompleteBillTime >= timeFrom && w.CompleteBillTime < timeTo).Count();
                    salary = (double)records * getPayonValue(staff, 6);
                }
                return salary;
            }            
        }

        /// <summary>
        /// Cập nhật lương mỹ phẩm
        /// </summary>
        /// <param name="bill"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        private double computeProductSalary(BillService bill, DateTime date)
        {
            using (var db = new Solution_30shineEntities())
            {
                double salary = 0;
                if (bill != null && bill.SellerId > 0)
                {
                    string sql = @"select Coalesce(sum(Coalesce(fp.Price * fp.Quantity * (Cast(Coalesce(p.ForSalary, 0) as float)/CAST(100 as float)), 0)), 0) as salary
                                from BillService as bill
                                inner join FlowProduct as fp
                                on bill.Id = fp.BillId and fp.IsDelete != 1
                                left join Product as p
                                on fp.ProductId = p.Id
                                where bill.IsDelete != 1 and bill.Pending != 1
                                and fp.ComboId is null
                                and bill.CompleteBillTime between '" + String.Format("{0:yyyy/MM/dd}", date) + "' and '" + String.Format("{0:yyyy/MM/dd}", date.AddDays(1)) + @"'
                                and bill.SellerId = " + bill.SellerId;
                    salary = db.Database.SqlQuery<double>(sql).FirstOrDefault();

                }
                return salary;
            }            
        }

        /// <summary>
        /// Cập nhật lương cứng
        /// Có 2 trường hợp
        /// 1. Nhân viên được trả lương theo công thức Nhóm (Bộ phận)
        /// 2. Nhân viên được trả lương theo công thức cá nhân (Áp dụng cho 1 số cá nhân đặc biệt)
        /// </summary>
        /// <param name="staff"></param>
        /// <param name="payon"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        private float computeFixSalary(Staff staff,  DateTime date)
        {
            using (var db = new Solution_30shineEntities())
            {
                float salary = 0;
                if (staff != null)
                {
                    int dayInMonth = DateTime.DaysInMonth(date.Year, date.Month) - 2;
                    if (staff.SalaryByPerson == true || staff.RequireEnroll == false)
                    {
                        int payonValue = getPayonValue(staff, 1);
                        if (payonValue > 0 && dayInMonth > 0)
                        {
                            salary = (float)payonValue / DateTime.DaysInMonth(date.Year, date.Month);
                        }
                    }
                    else if (isEnroll(staff, date))
                    {
                        int payonValue = getPayonValue(staff, 1);
                        if (payonValue > 0 && dayInMonth > 0)
                        {
                            salary = (float)payonValue / dayInMonth;
                        }
                    }
                }
                return salary;
            }            
        }

        private Library.Class.cls_rating computeRatingPoint(Staff staff, DateTime date, BillService bill)
        {
            using (var db = new Solution_30shineEntities())
            {
                Library.Class.cls_rating rating = new Library.Class.cls_rating();
                cls_configPartTime configPT = new cls_configPartTime();
                string sql = "";
                if (staff != null)
                {
                    // Áp dụng tính bill đặc biệt cho stylist và skinner (vd : 1 bill có 2 dịch vụ thì tính điểm cho 2 dịch vụ)
                    switch (staff.Type)
                    {
                        case 1:
                            sql = @"select Cast(Coalesce(SUM(Coalesce(point, 0)),0) as float) as point,
                                        Coalesce(SUM(Coalesce(bill_special, 0)),0) as bill_special,
                                        Coalesce(SUM(Coalesce(bill_special_great, 0)),0) as bill_special_great,
                                        Coalesce(SUM(Coalesce(bill_special_good, 0)),0) as bill_special_good,
                                        Coalesce(SUM(Coalesce(bill_special_bad, 0)),0) as bill_special_bad,
                                        Coalesce(SUM(Coalesce(bill_special_norating, 0)),0) as bill_special_norating,
                                        Coalesce(SUM(Coalesce(bill_normal, 0)),0) as bill_normal,	
                                        Coalesce(SUM(Coalesce(bill_normal_great, 0)),0) as bill_normal_great,
                                        Coalesce(SUM(Coalesce(bill_normal_good, 0)),0) as bill_normal_good,
                                        Coalesce(SUM(Coalesce(bill_normal_bad, 0)),0) as bill_normal_bad,
                                        Coalesce(SUM(Coalesce(bill_normal_norating, 0)),0) as bill_normal_norating
                                    from
                                    (
	                                    select
		                                    b.BillId, b.Mark, b.point, b.bill_special,
		                                    ( case when bill_special = 1 and Mark = 3 then 1 else 0 end ) as bill_special_great,
		                                    ( case when bill_special = 1 and Mark = 2 then 1 else 0 end ) as bill_special_good,
		                                    ( case when bill_special = 1 and Mark = 1 then 1 else 0 end ) as bill_special_bad,
		                                    ( case when bill_special = 1 and Mark = 0 then 1 else 0 end ) as bill_special_norating,
		                                    ( case  when bill_special = 0 then 1 else 0 end ) as bill_normal,
		                                    ( case  when bill_special = 0 and Mark = 3 then 1 else 0 end ) as bill_normal_great,
		                                    ( case  when bill_special = 0 and Mark = 2 then 1 else 0 end ) as bill_normal_good,
		                                    ( case  when bill_special = 0 and Mark = 1 then 1 else 0 end ) as bill_normal_bad,
		                                    ( case  when bill_special = 0 and Mark = 0 then 1 else 0 end ) as bill_normal_norating
	                                    from
	                                    (
		                                    select 
			                                    fs.BillId, bill.Mark,
			                                    --SUM(fs.Quantity * fs.CoefficientRating * rcp.ConventionPoint) as point,
                                                Cast((
				                                    Case 
					                                    when bill.IsX2 = 1 then SUM(fs.Quantity * fs.CoefficientRating * rcp.ConventionPoint) * Cast(1 as float)
					                                    else SUM(fs.Quantity * fs.CoefficientRating * rcp.ConventionPoint)
				                                    end
			                                    ) as float) as point,
			                                    ( case 
				                                    when 
					                                    (select COUNT(*) from FlowService where BillId = fs.BillId and CoefficientRating > 1) > 0 
				                                    then 1 else 0 
			                                      end 
			                                    ) as bill_special
		                                    from FlowService as fs
		                                    inner join BillService as bill
		                                    on fs.BillId = bill.Id and fs.IsDelete != 1
		                                    inner join Rating_ConfigPoint as rcp
		                                    on rcp.RealPoint = bill.Mark and rcp.Hint = 1 and rcp.IsDelete != 1 and rcp.[Status] = 1
		                                    where bill.IsDelete != 1 and bill.Pending != 1 
			                                    and bill.CompleteBillTime between '" + string.Format("{0:yyyy/MM/dd}", date) + "' and '" + string.Format("{0:yyyy/MM/dd}", date.AddDays(1)) + @"'
			                                    and bill.ServiceIds != '' and bill.ServiceIds is not null
			                                    and bill.Staff_Hairdresser_Id = " + staff.Id + @"
		                                    group by fs.BillId, bill.Mark, bill.IsX2
	                                    ) as b
                                    ) as a";
                            rating = db.Database.SqlQuery<Library.Class.cls_rating>(sql).FirstOrDefault();
                            //Nếu áp dụng tăng ca theo hệ số điểm hài lòng thì cập nhật lại toàn bộ lương dịch vụ trong ngày..
                            configPT = configPartTime(staff.Id, date);
                            if (configPT.IsPartTimeRating == true)
                            {
                                //Khởi tạo kiểm tra có phải stylist || skinner
                                cls_stylist_skinner checkSTSK = checkStylistSkinner(staff.Id, staff.Id);
                                if (checkSTSK.stylist || checkSTSK.skinner)
                                {
                                    rating.point = GetRatingPointVer2(bill, staff);
                                }
                            }
                            break;
                        case 2:
                            sql = @"select Cast(Coalesce(SUM(Coalesce(point, 0)),0) as float) as point,
                                        Coalesce(SUM(Coalesce(bill_special, 0)),0) as bill_special,
                                        Coalesce(SUM(Coalesce(bill_special_great, 0)),0) as bill_special_great,
                                        Coalesce(SUM(Coalesce(bill_special_good, 0)),0) as bill_special_good,
                                        Coalesce(SUM(Coalesce(bill_special_bad, 0)),0) as bill_special_bad,
                                        Coalesce(SUM(Coalesce(bill_special_norating, 0)),0) as bill_special_norating,
                                        Coalesce(SUM(Coalesce(bill_normal, 0)),0) as bill_normal,	
                                        Coalesce(SUM(Coalesce(bill_normal_great, 0)),0) as bill_normal_great,
                                        Coalesce(SUM(Coalesce(bill_normal_good, 0)),0) as bill_normal_good,
                                        Coalesce(SUM(Coalesce(bill_normal_bad, 0)),0) as bill_normal_bad,
                                        Coalesce(SUM(Coalesce(bill_normal_norating, 0)),0) as bill_normal_norating
                                    from
                                    (
	                                    select
		                                    b.BillId, b.Mark, b.point, b.bill_special,
		                                    ( case when bill_special = 1 and Mark = 3 then 1 else 0 end ) as bill_special_great,
		                                    ( case when bill_special = 1 and Mark = 2 then 1 else 0 end ) as bill_special_good,
		                                    ( case when bill_special = 1 and Mark = 1 then 1 else 0 end ) as bill_special_bad,
		                                    ( case when bill_special = 1 and Mark = 0 then 1 else 0 end ) as bill_special_norating,
		                                    ( case  when bill_special = 0 then 1 else 0 end ) as bill_normal,
		                                    ( case  when bill_special = 0 and Mark = 3 then 1 else 0 end ) as bill_normal_great,
		                                    ( case  when bill_special = 0 and Mark = 2 then 1 else 0 end ) as bill_normal_good,
		                                    ( case  when bill_special = 0 and Mark = 1 then 1 else 0 end ) as bill_normal_bad,
		                                    ( case  when bill_special = 0 and Mark = 0 then 1 else 0 end ) as bill_normal_norating
	                                    from
	                                    (
		                                    select 
			                                    fs.BillId, bill.Mark,
			                                    --SUM(fs.Quantity * fs.CoefficientRating * rcp.ConventionPoint) as point,
                                                Cast((
				                                    Case 
					                                    when bill.IsX2 = 1 then SUM(fs.Quantity * fs.CoefficientRating * rcp.ConventionPoint) * Cast(1 as float)
					                                    else SUM(fs.Quantity * fs.CoefficientRating * rcp.ConventionPoint)
				                                    end
			                                    ) as float) as point,
			                                    ( case 
				                                    when 
					                                    (select COUNT(*) from FlowService where BillId = fs.BillId and CoefficientRating > 1) > 0 
				                                    then 1 else 0 
			                                      end 
			                                    ) as bill_special
		                                    from FlowService as fs
		                                    inner join BillService as bill
		                                    on fs.BillId = bill.Id and fs.IsDelete != 1
		                                    inner join Rating_ConfigPoint as rcp
		                                    on rcp.RealPoint = bill.Mark and rcp.Hint = 1 and rcp.IsDelete != 1 and rcp.[Status] = 1
		                                    where bill.IsDelete != 1 and bill.Pending != 1 
			                                    and bill.CompleteBillTime between '" + string.Format("{0:yyyy/MM/dd}", date) + "' and '" + string.Format("{0:yyyy/MM/dd}", date.AddDays(1)) + @"'
			                                    and bill.ServiceIds != '' and bill.ServiceIds is not null
			                                    and bill.Staff_HairMassage_Id = " + staff.Id + @"
		                                    group by fs.BillId, bill.Mark, bill.IsX2
	                                    ) as b
                                    ) as a";
                            rating = db.Database.SqlQuery<Library.Class.cls_rating>(sql).FirstOrDefault();
                            //Nếu áp dụng tăng ca theo hệ số điểm hài lòng thì cập nhật lại toàn bộ lương dịch vụ trong ngày..
                            configPT = configPartTime(staff.Id, date);
                            if (configPT.IsPartTimeRating == true)
                            {
                                //Khởi tạo kiểm tra có phải stylist || skinner
                                cls_stylist_skinner checkSTSK = checkStylistSkinner(staff.Id, staff.Id);
                                if (checkSTSK.stylist || checkSTSK.skinner)
                                {
                                    rating.point = GetRatingPointVer2(bill, staff);
                                }
                            }
                            break;
                        case 5:
                            sql = @"select Cast(Coalesce(SUM(Coalesce(point, 0)),0) as float) as point,
	                                Coalesce(SUM(Coalesce(bill_special, 0)),0) as bill_special,
	                                Coalesce(SUM(Coalesce(bill_special_great, 0)),0) as bill_special_great,
	                                Coalesce(SUM(Coalesce(bill_special_good, 0)),0) as bill_special_good,
	                                Coalesce(SUM(Coalesce(bill_special_bad, 0)),0) as bill_special_bad,
	                                Coalesce(SUM(Coalesce(bill_special_norating, 0)),0) as bill_special_norating,
	                                Coalesce(SUM(Coalesce(bill_normal, 0)),0) as bill_normal,	
	                                Coalesce(SUM(Coalesce(bill_normal_great, 0)),0) as bill_normal_great,
	                                Coalesce(SUM(Coalesce(bill_normal_good, 0)),0) as bill_normal_good,
	                                Coalesce(SUM(Coalesce(bill_normal_bad, 0)),0) as bill_normal_bad,
	                                Coalesce(SUM(Coalesce(bill_normal_norating, 0)),0) as bill_normal_norating
                                from
                                (
	                                select 
		                                Cast(rcp.ConventionPoint as float) as point,
		                                0 as bill_special,
		                                0 as bill_special_great,
		                                0 as bill_special_good,
		                                0 as bill_special_bad,
		                                0 as bill_special_norating,
		                                1 as bill_normal,
		                                ( case  when Mark = 3 then 1 else 0 end ) as bill_normal_great,
		                                ( case  when Mark = 2 then 1 else 0 end ) as bill_normal_good,
		                                ( case  when Mark = 1 then 1 else 0 end ) as bill_normal_bad,
		                                ( case  when Mark = 0 then 1 else 0 end ) as bill_normal_norating
	                                from  BillService as bill
	                                inner join Rating_ConfigPoint as rcp
	                                on rcp.RealPoint = bill.Mark and rcp.Hint = 1 and rcp.IsDelete != 1 and rcp.[Status] = 1
	                                where bill.IsDelete != 1 and bill.Pending != 1 
                                        and bill.CompleteBillTime between '" + string.Format("{0:yyyy/MM/dd}", date) + "' and '" + string.Format("{0:yyyy/MM/dd}", date.AddDays(1)) + @"'
                                        and bill.ServiceIds != '' and bill.ServiceIds is not null
		                                and bill.ReceptionId = " + staff.Id + @"
                                ) as a";
                            rating = db.Database.SqlQuery<Library.Class.cls_rating>(sql).FirstOrDefault();
                            break;
                    }
                }
                return rating;
            }
        }

        /// <summary>
        /// Cập nhật lương part-time
        /// </summary>
        /// <param name="staff"></param>
        /// <param name="db"></param>
        /// <param name="payon"></param>
        /// <returns></returns>
        private float computeParttimeSalary(Staff staff, DateTime date)
        {
            using (var db = new Solution_30shineEntities())
            {
                float salary = 0;
                if (staff != null)
                {
                    var ftk = db.FlowTimeKeepings.FirstOrDefault(w => w.StaffId == staff.Id && w.WorkDate == date && w.IsDelete != 1);
                    int payonValue = getPayonValue(staff, 3);

                    if (ftk != null && ftk.WorkHour != null && payonValue > 0)
                    {
                        salary = (float)ftk.WorkHour * (float)payonValue;
                    }
                    //lấy cấu hình kiểm tra có phải là stylist hoặc skinner 
                    var IsStylistSkinner = checkStylistSkinner(staff.Id, staff.Id);

                    if (IsStylistSkinner.stylist || IsStylistSkinner.skinner)
                    {
                        //lấy cấu hình tăng ca.
                        var configPT = configPartTime(staff.Id, date);
                        if (configPT.IsPartTimeRating == true && configPT.IsPartTimeHour == false)
                        {
                            salary = 0;
                        }
                        else if (configPT.IsPartTimeHour == false && configPT.IsPartTimeRating == false)
                        {
                            salary = 0;
                        }
                    }
                }
                return salary;
            }            
        }
        /// <summary>
        /// Lấy thông tin cấu hình Tăng ca trong bảng ConfigPartTime
        /// </summary>
        /// <param name="staff_id"></param>
        /// <param name="sDate"></param>
        /// <returns></returns>
        public cls_configPartTime configPartTime(int staff_id, DateTime sDate)
        {
            cls_configPartTime dataConfig = new cls_configPartTime();
            //khởi tạo giá trị mặc định
            dataConfig.IsPartTimeHour = true;
            dataConfig.IsPartTimeRating = false;
            dataConfig.CoefficientStylist = 1;
            dataConfig.coefficinetSkinner = 1;
            try
            {
                using (Solution_30shineEntities db = new Solution_30shineEntities())
                {
                    var salonPartTime = db.FlowTimeKeepings.Where(r => r.StaffId == staff_id && r.WorkDate == sDate && r.IsEnroll == true).FirstOrDefault();
                    if (salonPartTime != null)
                    {
                        var dataSalonConfig = db.Tbl_Salon.Where(r => r.IsDelete == 0 && r.Id == salonPartTime.SalonId).FirstOrDefault();
                        if (dataSalonConfig != null)
                        {
                            dataConfig.IsPartTimeHour = dataSalonConfig.IsPartTimeHours;
                            dataConfig.IsPartTimeRating = dataSalonConfig.IsPartTimeRating;
                            var dataConfigPartTime = db.ConfigPartTimes.Where(r => r.IsDelete == false && r.SalonId == dataSalonConfig.Id).FirstOrDefault();
                            if (dataConfigPartTime != null)
                            {
                                dataConfig.CoefficientStylist = dataConfigPartTime.CoefficientStylist;
                                dataConfig.coefficinetSkinner = dataConfigPartTime.CoefficinetSkinner;
                            }

                        }
                    }

                }
            }
            catch (Exception ex)
            {

            }

            return dataConfig;

        }
        /// <summary>
        /// kiểm tra xem id truyền vào có phải là stylist hoặc skinner không
        /// </summary>
        /// <param name="stylist_id">id stylist</param>
        /// <param name="skinner_id">id skinner</param>
        /// <returns></returns>
        public cls_stylist_skinner checkStylistSkinner(int stylist_id, int skinner_id)
        {
            cls_stylist_skinner objcheck = new cls_stylist_skinner();

            try
            {
                using (Solution_30shineEntities db = new Solution_30shineEntities())
                {
                    var dataST = db.Staffs.Where(r => r.IsDelete == 0 && r.Type == 1 && r.Id == stylist_id).FirstOrDefault();
                    var dataSK = db.Staffs.Where(r => r.IsDelete == 0 && r.Type == 2 && r.Id == skinner_id).FirstOrDefault();
                    objcheck.stylist = dataST == null ? false : true;
                    objcheck.skinner = dataSK == null ? false : true;
                }
            }
            catch (Exception ex)
            {
                objcheck.stylist = true;
                objcheck.skinner = true;

            }
            return objcheck;
        }

        /// <summary>
        /// Kiểm tra nhân viên đã được chấm công hay chưa
        /// - Riêng đối với stylist và skinner, công được tính khi 1 ngày tham gia đc ít nhất 3 bill
        /// </summary>
        /// <param name="staff"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        private bool isEnroll(Staff staff, DateTime date)
        {
            using (var db = new Solution_30shineEntities())
            {
                bool enroll = false;
                if (staff != null)
                {
                    var ftk = new FlowTimeKeeping();
                    //if (staff.SalonId > 0)
                    //{
                    //    ftk = db.FlowTimeKeepings.FirstOrDefault(w => w.StaffId == staff.Id && w.SalonId == staff.SalonId && w.WorkDate == date && w.IsDelete != 1);
                    //}
                    //else
                    //{ 
                    ftk = db.FlowTimeKeepings.FirstOrDefault(w => w.StaffId == staff.Id && w.WorkDate == date && w.IsDelete != 1);
                    //}
                    if (ftk != null && ftk.IsEnroll == true)
                    {
                        enroll = true;
                    }
                }
                return enroll;
            }            
        }

        /// <summary>
        /// Get hệ số lương dịch vụ
        /// </summary>
        /// <param name="department"></param>
        /// <param name="skillLevel"></param>
        /// <param name="payon"></param>
        /// <returns></returns>
        private int getPayonValue(Staff staff, int paybytime)
        {
            using (var db = new Solution_30shineEntities())
            {
                int value = 0;
                if (staff != null)
                {
                    var record = new Tbl_Payon();
                    if (staff.SalaryByPerson == true)
                    {
                        record = db.Tbl_Payon.FirstOrDefault(w => w.StaffId == staff.Id && w.PayByTime == paybytime && w.Hint == "base_salary");
                    }
                    else
                    {
                        record = db.Tbl_Payon.FirstOrDefault(w => w.TypeStaffId == staff.Type && w.ForeignId == staff.SkillLevel && w.PayByTime == paybytime && w.Hint == "base_salary");
                    }
                    if (record != null && record.Value != null)
                    {
                        value = record.Value.Value;
                    }
                }
                return value;
            }            
        }

        private int getPayonValueByStaffId(int staffId, int paybytime)
        {
            using (var db = new Solution_30shineEntities())
            {
                int value = 0;
                var obj = db.Tbl_Payon.FirstOrDefault(w => w.StaffId == staffId && w.PayByTime == paybytime && w.Hint == "base_salary");
                if (obj != null && obj.Value != null)
                {
                    value = obj.Value.Value;
                }
                return value;
            }            
        }

        //===================================================
        // Cập nhật salary flow khi chấm công
        //===================================================
        public void updateFlowSalaryByTimeKeeping(FlowTimeKeeping ftk)
        {
            using (var db = new Solution_30shineEntities())
            {
                if (ftk != null && ftk.WorkDate != null)
                {
                    var payon = db.Tbl_Payon.Where(w => w.IsDelete != 1).ToList();
                    var staff = db.Staffs.FirstOrDefault(w => w.Id == ftk.StaffId);
                    var fs = db.FlowSalaries.FirstOrDefault(w => w.staffId == ftk.StaffId && w.sDate == ftk.WorkDate && w.IsDelete != 1);
                    if (fs != null)
                    {

                        fs.fixSalary = computeFixSalaryByTimeKeeping(staff, ftk);
                        /// Cập nhật lương part-time
                        //fs.partTimeSalary = computeParttimeSalaryByTimeKeeping(staff, ftk, db);
                        fs.SalonId = ftk.SalonId;
                        db.FlowSalaries.AddOrUpdate(fs);
                        db.SaveChanges();
                    }
                    else
                    {
                        initSalaryByStaffId(ftk.StaffId, ftk.WorkDate.Value, ftk.SalonId);
                    }
                }
            }            
        }

        /// <summary>
        /// Tính lại lương cứng khi cập nhật chấm công
        /// Không áp dụng cho stylist(Type = 1), skinner(Type = 2)
        /// </summary>
        /// <param name="staff"></param>
        /// <param name="db"></param>
        /// <param name="payon"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        private float computeFixSalaryByTimeKeeping(Staff staff, FlowTimeKeeping ftk)
        {
            using (var db = new Solution_30shineEntities())
            {
                float salary = 0;
                if (staff != null && (isEnroll(staff, ftk.WorkDate.Value) || staff.RequireEnroll == false || staff.SalaryByPerson == true))
                {
                    int dayInMonth = DateTime.DaysInMonth(ftk.WorkDate.Value.Year, ftk.WorkDate.Value.Month) - 2;
                    int payonValue = getPayonValue(staff, 1);

                    if (payonValue > 0 && dayInMonth > 0)
                    {
                        salary = (float)payonValue / dayInMonth;
                    }
                }
                return salary;
            }
        }

        /// <summary>
        /// Tính lại lương part-time khi cập nhật chấm công
        /// </summary>
        /// <param name="staff"></param>
        /// <param name="db"></param>
        /// <param name="payon"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        private float computeParttimeSalaryByTimeKeeping(Staff staff, FlowTimeKeeping ftk)
        {
            using (var db = new Solution_30shineEntities())
            {
                float salary = 0;
                if (staff != null && ftk != null)
                {
                    int payonValue = getPayonValue(staff, 3);
                    if (ftk != null && ftk.WorkHour != null && payonValue > 0)
                    {
                        salary = (float)ftk.WorkHour * (float)payonValue;
                    }
                }
                return salary;
            }            
        }

        //============================
        // Dành cho Update
        //============================

        /// <summary>
        /// Cập nhật lương cho từng nhân viên theo ngày
        /// </summary>
        /// <param name="flowSL"></param>
        /// <param name="db"></param>
        public void forUpdate_updateSalaryByRecord(FlowSalary flowSL)
        {
            using (var db = new Solution_30shineEntities())
            {
                if (flowSL != null)
                {
                    double _double;
                    var staff = db.Staffs.FirstOrDefault(w => w.Id == flowSL.staffId);
                    // 1. Lương cứng
                    flowSL.fixSalary = forUpdate_computeFixSalary(staff, flowSL.sDate);
                    // 2. Lương part-time (làm thêm)
                    //flowSL.partTimeSalary = forUpdate_computeParttimeSalary(staff, flowSL.sDate, db);

                    // Điểm rating
                    var rating = forUpdate_computeRatingPoint(staff, flowSL.sDate);
                    flowSL.ratingPoint = rating.point;
                    flowSL.bill_normal = rating.bill_normal;
                    flowSL.bill_normal_great = rating.bill_normal_great;
                    flowSL.bill_normal_good = rating.bill_normal_good;
                    flowSL.bill_normal_bad = rating.bill_normal_bad;
                    flowSL.bill_normal_norating = rating.bill_normal_norating;
                    flowSL.bill_special = rating.bill_special;
                    flowSL.bill_special_great = rating.bill_special_great;
                    flowSL.bill_special_good = rating.bill_special_good;
                    flowSL.bill_special_bad = rating.bill_special_bad;
                    flowSL.bill_special_norating = rating.bill_special_norating;

                    if (staff.Type == 5)
                    {
                        // 3. Lương doanh số của checkin
                        flowSL.serviceSalary = UpdateReceptionSalary(flowSL.sDate, flowSL.sDate.AddDays(+1), staff.Id);
                    }
                    else
                    {
                        // 3. Lương dịch vụ
                        flowSL.serviceSalary = forUpdate_computeServiceSalary(staff, double.TryParse(flowSL.ratingPoint.ToString(), out _double) ? _double : 0);
                    }

                    // 4. Lương mỹ phẩm
                    flowSL.productSalary = forUpdate_computeProductSalary(staff, flowSL.sDate, db);
                    // 5. Lương phụ cấp
                    flowSL.allowanceSalary = computeAllowanceSalary(staff, flowSL.sDate);

                    db.FlowSalaries.AddOrUpdate(flowSL);
                    db.SaveChanges();
                }
            }            
        }

        /// <summary>
        /// Cập nhật flow lương riêng cho Quỹ lương doanh số
        /// </summary>
        /// <param name="flowSL"></param>
        /// <param name="db"></param>
        public void forUpdate_updateSalaryByRecordCustomerReturn(FlowSalary flowSL)
        {
            using (var db = new Solution_30shineEntities())
            {
                if (flowSL != null)
                {
                    var staff = db.Staffs.FirstOrDefault(w => w.Id == flowSL.staffId);
                    // 1. Lương cứng
                    flowSL.fixSalary = forUpdate_computeFixSalary(staff, flowSL.sDate);
                    // 3. Lương dịch vụ
                    flowSL.serviceSalary = forUpdate_computeServiceSalaryCustomerReturn(staff, flowSL.sDate, db);

                    db.FlowSalaries.AddOrUpdate(flowSL);
                    db.SaveChanges();
                }
            }            
        }

        public void forUpdate_updateSalaryByRecordQuyLuongDoanhSo(FlowSalary flowSL)
        {
            using (var db = new Solution_30shineEntities())
            {
                if (flowSL != null)
                {
                    var staff = db.Staffs.FirstOrDefault(w => w.Id == flowSL.staffId);
                    // 1. Lương dịch vụ
                    flowSL.serviceSalary = forUpdate_computeServiceSalaryTotalBill(staff, flowSL.sDate, db);
                    db.FlowSalaries.AddOrUpdate(flowSL);
                    db.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Cập nhật bản ghi FlowSalary cho nhân viên theo khoảng thời gian
        /// </summary>
        /// <param name="staff"></param>
        /// <param name="timeFrom"></param>
        /// <param name="timeTo"></param>
        /// <param name="db"></param>
        public void forUpdate_updateSalaryByStaff(Staff staff, DateTime timeFrom, DateTime timeTo)
        {
            using (var db = new Solution_30shineEntities())
            {
                if (!staff.Equals(null))
                {
                    while (timeFrom <= timeTo)
                    {
                        if (!isInitSalaryTodayByStaff(staff.Id, timeFrom))
                        {
                            initSalaryByStaffId(staff.Id, timeFrom, Convert.ToInt32(staff.SalonId));
                        }
                        //if (isEnroll(staff, db, timeFrom) || staff.RequireEnroll == false || staff.SalaryByPerson == true)
                        //{                        
                        var flowSL = db.FlowSalaries.FirstOrDefault(w => w.staffId == staff.Id && w.sDate == timeFrom && w.IsDelete != 1);
                        if (staff.Id == 364)
                        {
                            forUpdate_updateSalaryByRecordQuyLuongDoanhSo(flowSL);
                        }
                        else if (staff.Type == 9)
                        {
                            forUpdate_updateSalaryByRecordCustomerReturn(flowSL);
                        }
                        else
                        {
                            forUpdate_updateSalaryByRecord(flowSL);
                        }
                        //}
                        timeFrom = timeFrom.AddDays(1);
                    }
                }
            }
        }

        /// <summary>
        /// Tính lương cứng theo ngày
        /// </summary>
        /// <param name="staff"></param>
        /// <param name="date"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        private float forUpdate_computeFixSalary(Staff staff, DateTime date)
        {
            using (var db = new Solution_30shineEntities())
            {
                float salary = 0;
                if (staff != null)
                {
                    int dayInMonth = 0;
                    if (staff.SalaryByPerson == true || staff.RequireEnroll == false)
                    {
                        dayInMonth = DateTime.DaysInMonth(date.Year, date.Month);
                    }
                    else if (isEnroll(staff, date))
                    {
                        dayInMonth = DateTime.DaysInMonth(date.Year, date.Month) - 2;
                    }
                    int payonValue = getPayonValue(staff, 1);
                    if (payonValue > 0 && dayInMonth > 0)
                    {
                        salary = (float)payonValue / dayInMonth;
                    }
                }
                return salary;
            }            
        }

        /// <summary>
        /// Tính lương part-time theo ngày
        /// </summary>
        /// <param name="staff"></param>
        /// <param name="date"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        private float forUpdate_computeParttimeSalary(Staff staff, DateTime date)
        {
            using (var db = new Solution_30shineEntities())
            {
                float salary = 0;
                if (staff != null)
                {
                    var ftk = db.FlowTimeKeepings.FirstOrDefault(w => w.StaffId == staff.Id && w.WorkDate == date && w.IsDelete != 1);
                    int payonValue = getPayonValue(staff, 3);
                    if (ftk != null && ftk.WorkHour != null && payonValue > 0)
                    {
                        salary = (float)ftk.WorkHour * (float)payonValue;
                    }
                }
                return salary;
            }            
        }

        /// <summary>
        /// Tính lương dịch vụ
        /// </summary>
        /// <param name="staff"></param>
        /// <param name="ratingPoint"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        private double forUpdate_computeServiceSalary(Staff staff, double ratingPoint)
        {
            using (var db = new Solution_30shineEntities())
            {
                double salary = 0;
                // Chỉ tính lương dịch vụ cho stylist, skinner, check-in
                if (staff != null)
                {
                    salary = (double)ratingPoint * getPayonValue(staff, 5);
                }
                return salary;
            }            
        }

        /// <summary>
        /// Tính lương dịch vụ
        /// </summary>
        /// <param name="staff"></param>
        /// <param name="ratingPoint"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        private double forUpdate_computeServiceSalaryCustomerReturn(Staff staff, DateTime date, Solution_30shineEntities db)
        {
            double salary = 0;
            if (staff != null)
            {
                int salonId = staff.SalonId.Value;
                string timeFrom = String.Format("{0:yyyy/MM/dd}", date);
                string timeTo = String.Format("{0:yyyy/MM/dd}", date.AddDays(1));
                int customerReturn = db.Store_Customer_Return(salonId, timeFrom, timeTo).FirstOrDefault().Value;
                salary = (double)customerReturn * getPayonValue(staff, 7);
            }
            return salary;
        }

        private double forUpdate_computeServiceSalaryTotalBill(Staff staff, DateTime date, Solution_30shineEntities db)
        {
            double salary = 0;
            if (staff != null)
            {
                DateTime timeFrom = date;
                DateTime timeTo = date.AddDays(1);
                int bills = db.BillServices.Where(w => w.CompleteBillTime >= timeFrom && w.CompleteBillTime < timeTo && w.IsDelete != 1 && w.Pending != 1 && w.ServiceIds != "" && w.ServiceIds != null).Count();
                salary = (double)bills * getPayonValue(staff, 6);
            }
            return salary;
        }

        /// <summary>
        /// Tính lương mỹ phẩm
        /// </summary>
        /// <param name="staff"></param>
        /// <param name="date"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        private double forUpdate_computeProductSalary(Staff staff, DateTime date, Solution_30shineEntities db)
        {
            double salary = 0;
            if (staff != null)
            {
                string sql = @"select Coalesce(sum(Coalesce(fp.Price * fp.Quantity * (Cast(Coalesce(p.ForSalary, 0) as float)/CAST(100 as float)), 0)), 0) as salary
                                from BillService as bill
                                inner join FlowProduct as fp
                                on bill.Id = fp.BillId and fp.IsDelete != 1
                                left join Product as p
                                on fp.ProductId = p.Id
                                where bill.IsDelete != 1 and bill.Pending != 1
                                and bill.CompleteBillTime between '" + String.Format("{0:yyyy/MM/dd}", date) + "' and '" + String.Format("{0:yyyy/MM/dd}", date.AddDays(1)) + @"'
                                and bill.SellerId = " + staff.Id;
                salary = db.Database.SqlQuery<double>(sql).FirstOrDefault();

            }
            return salary;
        }

        /// <summary>
        /// Tính điểm rating
        /// </summary>
        /// <param name="staff"></param>
        /// <param name="date"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        private Library.Class.cls_rating forUpdate_computeRatingPoint(Staff staff, DateTime date)
        {
            using (var db = new Solution_30shineEntities()) {
                Library.Class.cls_rating rating = new Library.Class.cls_rating();
                cls_configPartTime configPT = new cls_configPartTime();
                string sql = "";
                if (staff != null && Array.IndexOf(new int[] { 1, 2, 5 }, staff.Type) != -1)
                {
                    // Áp dụng tính bill đặc biệt cho stylist và skinner (vd : 1 bill có 2 dịch vụ thì tính điểm cho 2 dịch vụ)
                    switch (staff.Type)
                    {
                        // stylist
                        case 1:
                            sql = @"select Coalesce(SUM(Coalesce(point, 0)),0) as point,
                                        Coalesce(SUM(Coalesce(bill_special, 0)),0) as bill_special,
                                        Coalesce(SUM(Coalesce(bill_special_great, 0)),0) as bill_special_great,
                                        Coalesce(SUM(Coalesce(bill_special_good, 0)),0) as bill_special_good,
                                        Coalesce(SUM(Coalesce(bill_special_bad, 0)),0) as bill_special_bad,
                                        Coalesce(SUM(Coalesce(bill_special_norating, 0)),0) as bill_special_norating,
                                        Coalesce(SUM(Coalesce(bill_normal, 0)),0) as bill_normal,	
                                        Coalesce(SUM(Coalesce(bill_normal_great, 0)),0) as bill_normal_great,
                                        Coalesce(SUM(Coalesce(bill_normal_good, 0)),0) as bill_normal_good,
                                        Coalesce(SUM(Coalesce(bill_normal_bad, 0)),0) as bill_normal_bad,
                                        Coalesce(SUM(Coalesce(bill_normal_norating, 0)),0) as bill_normal_norating
                                    from
                                    (
	                                    select
		                                    b.BillId, b.Mark, b.point, b.bill_special,
		                                    ( case when bill_special = 1 and Mark = 3 then 1 else 0 end ) as bill_special_great,
		                                    ( case when bill_special = 1 and Mark = 2 then 1 else 0 end ) as bill_special_good,
		                                    ( case when bill_special = 1 and Mark = 1 then 1 else 0 end ) as bill_special_bad,
		                                    ( case when bill_special = 1 and Mark = 0 then 1 else 0 end ) as bill_special_norating,
		                                    ( case  when bill_special = 0 then 1 else 0 end ) as bill_normal,
		                                    ( case  when bill_special = 0 and Mark = 3 then 1 else 0 end ) as bill_normal_great,
		                                    ( case  when bill_special = 0 and Mark = 2 then 1 else 0 end ) as bill_normal_good,
		                                    ( case  when bill_special = 0 and Mark = 1 then 1 else 0 end ) as bill_normal_bad,
		                                    ( case  when bill_special = 0 and Mark = 0 then 1 else 0 end ) as bill_normal_norating
	                                    from
	                                    (
		                                    select 
			                                    fs.BillId, bill.Mark,
			                                    --SUM(fs.Quantity * fs.CoefficientRating * rcp.ConventionPoint) as point,
                                                Cast((
				                                    Case 
					                                    when bill.IsX2 = 1 then SUM(fs.Quantity * fs.CoefficientRating * rcp.ConventionPoint) * Cast(1 as float)
					                                    else SUM(fs.Quantity * fs.CoefficientRating * rcp.ConventionPoint)
				                                    end
			                                    ) as float) as point,
			                                    ( case 
				                                    when 
					                                    (select COUNT(*) from FlowService where BillId = fs.BillId and CoefficientRating > 1) > 0 
				                                    then 1 else 0 
			                                      end 
			                                    ) as bill_special
		                                    from FlowService as fs
		                                    inner join BillService as bill
		                                    on fs.BillId = bill.Id and fs.IsDelete != 1
		                                    inner join Rating_ConfigPoint as rcp
		                                    on rcp.RealPoint = bill.Mark and rcp.Hint = 1 and rcp.IsDelete != 1 and rcp.[Status] = 1
		                                    where bill.IsDelete != 1 and bill.Pending != 1 
			                                    and bill.CompleteBillTime between '" + string.Format("{0:yyyy/MM/dd}", date) + "' and '" + string.Format("{0:yyyy/MM/dd}", date.AddDays(1)) + @"'
			                                    and bill.ServiceIds != '' and bill.ServiceIds is not null
			                                    and bill.Staff_Hairdresser_Id = " + staff.Id + @"
		                                    group by fs.BillId, bill.Mark, bill.IsX2
	                                    ) as b
                                    ) as a";
                            break;
                        // skinner
                        case 2:
                            sql = sql = @"select Coalesce(SUM(Coalesce(point, 0)),0) as point,
                                        Coalesce(SUM(Coalesce(bill_special, 0)),0) as bill_special,
                                        Coalesce(SUM(Coalesce(bill_special_great, 0)),0) as bill_special_great,
                                        Coalesce(SUM(Coalesce(bill_special_good, 0)),0) as bill_special_good,
                                        Coalesce(SUM(Coalesce(bill_special_bad, 0)),0) as bill_special_bad,
                                        Coalesce(SUM(Coalesce(bill_special_norating, 0)),0) as bill_special_norating,
                                        Coalesce(SUM(Coalesce(bill_normal, 0)),0) as bill_normal,	
                                        Coalesce(SUM(Coalesce(bill_normal_great, 0)),0) as bill_normal_great,
                                        Coalesce(SUM(Coalesce(bill_normal_good, 0)),0) as bill_normal_good,
                                        Coalesce(SUM(Coalesce(bill_normal_bad, 0)),0) as bill_normal_bad,
                                        Coalesce(SUM(Coalesce(bill_normal_norating, 0)),0) as bill_normal_norating
                                    from
                                    (
	                                    select
		                                    b.BillId, b.Mark, b.point, b.bill_special,
		                                    ( case when bill_special = 1 and Mark = 3 then 1 else 0 end ) as bill_special_great,
		                                    ( case when bill_special = 1 and Mark = 2 then 1 else 0 end ) as bill_special_good,
		                                    ( case when bill_special = 1 and Mark = 1 then 1 else 0 end ) as bill_special_bad,
		                                    ( case when bill_special = 1 and Mark = 0 then 1 else 0 end ) as bill_special_norating,
		                                    ( case  when bill_special = 0 then 1 else 0 end ) as bill_normal,
		                                    ( case  when bill_special = 0 and Mark = 3 then 1 else 0 end ) as bill_normal_great,
		                                    ( case  when bill_special = 0 and Mark = 2 then 1 else 0 end ) as bill_normal_good,
		                                    ( case  when bill_special = 0 and Mark = 1 then 1 else 0 end ) as bill_normal_bad,
		                                    ( case  when bill_special = 0 and Mark = 0 then 1 else 0 end ) as bill_normal_norating
	                                    from
	                                    (
		                                    select 
			                                    fs.BillId, bill.Mark,
			                                    --SUM(fs.Quantity * fs.CoefficientRating * rcp.ConventionPoint) as point,
                                                Cast((
				                                    Case 
					                                    when bill.IsX2 = 1 then SUM(fs.Quantity * fs.CoefficientRating * rcp.ConventionPoint) * Cast(1 as float)
					                                    else SUM(fs.Quantity * fs.CoefficientRating * rcp.ConventionPoint)
				                                    end
			                                    ) as float) as point,
			                                    ( case 
				                                    when 
					                                    (select COUNT(*) from FlowService where BillId = fs.BillId and CoefficientRating > 1) > 0 
				                                    then 1 else 0 
			                                      end 
			                                    ) as bill_special
		                                    from FlowService as fs
		                                    inner join BillService as bill
		                                    on fs.BillId = bill.Id and fs.IsDelete != 1
		                                    inner join Rating_ConfigPoint as rcp
		                                    on rcp.RealPoint = bill.Mark and rcp.Hint = 1 and rcp.IsDelete != 1 and rcp.[Status] = 1
		                                    where bill.IsDelete != 1 and bill.Pending != 1 
			                                    and bill.CompleteBillTime between '" + string.Format("{0:yyyy/MM/dd}", date) + "' and '" + string.Format("{0:yyyy/MM/dd}", date.AddDays(1)) + @"'
			                                    and bill.ServiceIds != '' and bill.ServiceIds is not null
			                                    and bill.Staff_HairMassage_Id = " + staff.Id + @"
		                                    group by fs.BillId, bill.Mark, bill.IsX2
	                                    ) as b
                                    ) as a";
                            break;
                        // check-in
                        case 5:
                            sql = @"select Cast(Coalesce(SUM(Coalesce(point, 0)),0) as float) as point,
	                                Coalesce(SUM(Coalesce(bill_special, 0)),0) as bill_special,
	                                Coalesce(SUM(Coalesce(bill_special_great, 0)),0) as bill_special_great,
	                                Coalesce(SUM(Coalesce(bill_special_good, 0)),0) as bill_special_good,
	                                Coalesce(SUM(Coalesce(bill_special_bad, 0)),0) as bill_special_bad,
	                                Coalesce(SUM(Coalesce(bill_special_norating, 0)),0) as bill_special_norating,
	                                Coalesce(SUM(Coalesce(bill_normal, 0)),0) as bill_normal,	
	                                Coalesce(SUM(Coalesce(bill_normal_great, 0)),0) as bill_normal_great,
	                                Coalesce(SUM(Coalesce(bill_normal_good, 0)),0) as bill_normal_good,
	                                Coalesce(SUM(Coalesce(bill_normal_bad, 0)),0) as bill_normal_bad,
	                                Coalesce(SUM(Coalesce(bill_normal_norating, 0)),0) as bill_normal_norating
                                from
                                (
	                                select 
		                                rcp.ConventionPoint as point,
		                                0 as bill_special,
		                                0 as bill_special_great,
		                                0 as bill_special_good,
		                                0 as bill_special_bad,
		                                0 as bill_special_norating,
		                                1 as bill_normal,
		                                ( case  when Mark = 3 then 1 else 0 end ) as bill_normal_great,
		                                ( case  when Mark = 2 then 1 else 0 end ) as bill_normal_good,
		                                ( case  when Mark = 1 then 1 else 0 end ) as bill_normal_bad,
		                                ( case  when Mark = 0 then 1 else 0 end ) as bill_normal_norating
	                                from  BillService as bill
	                                inner join Rating_ConfigPoint as rcp
	                                on rcp.RealPoint = bill.Mark and rcp.Hint = 1 and rcp.IsDelete != 1 and rcp.[Status] = 1
	                                where bill.IsDelete != 1 and bill.Pending != 1 
                                        and bill.CompleteBillTime between '" + string.Format("{0:yyyy/MM/dd}", date) + "' and '" + string.Format("{0:yyyy/MM/dd}", date.AddDays(1)) + @"'
                                        and bill.ServiceIds != '' and bill.ServiceIds is not null
		                                and bill.ReceptionId = " + staff.Id + @"
                                ) as a";
                            break;
                    }
                    rating = db.Database.SqlQuery<Library.Class.cls_rating>(sql).FirstOrDefault();
                }
                return rating;
            }
        }

        /// <summary>
        /// Thử nghiệm tính lương doanh số cho Checkin
        /// </summary>
        public void updateCheckinSalary(BillService Bill)
        {
            using (var db = new Solution_30shineEntities())
            {
                int maxTimeMinute = 15;

                if (Bill != null && Bill.ReceptionId != null && Bill.ReceptionId > 0)
                {
                    var sql = @"select COUNT(*) from BillService where CreatedDate >= '" + String.Format("{0:yyyy/MM/dd}", Bill.CreatedDate) + @"'
                                and IsDelete != 1 and Pending != 1 
                                and IsBooking = 1 and InProcedureTime is not null 
                                and DATEDIFF(MINUTE, CreatedDate, InProcedureTime) <= " + maxTimeMinute + @"
                                and ReceptionId = " + Bill.ReceptionId;
                    var goodServiceTimes = db.Database.SqlQuery<int>(sql).FirstOrDefault();

                }
            }
        }
        private void WriteLog(string strLog)
        {

            StreamWriter log;
            FileStream fileStream = null;
            DirectoryInfo logDirInfo = null;
            FileInfo logFileInfo;
            string dirPath = HttpContext.Current.Server.MapPath("~");

            string logFilePath = dirPath + "Logs\\";
            logFilePath = logFilePath + "Log-" + System.DateTime.Today.ToString("MM-dd-yyyy") + "." + "txt";
            logFileInfo = new FileInfo(logFilePath);
            logDirInfo = new DirectoryInfo(logFileInfo.DirectoryName);
            if (!logDirInfo.Exists) logDirInfo.Create();
            if (!logFileInfo.Exists)
            {
                fileStream = logFileInfo.Create();
            }
            else
            {
                fileStream = new FileStream(logFilePath, FileMode.Append);
            }
            log = new StreamWriter(fileStream);
            log.WriteLine(strLog);
            log.Close();
        }
    }

    public class cls_stylist_skinner
    {
        public bool stylist { get; set; }
        public bool skinner { get; set; }
    }
    public class cls_configPartTime
    {
        public bool? IsPartTimeHour { get; set; }
        public bool? IsPartTimeRating { get; set; }
        public double? CoefficientStylist { get; set; }
        public double? coefficinetSkinner { get; set; }
    }
}
//,Coalesce(SUM(Coalesce(Mistake_Point, 0)), 0) as mistake_point