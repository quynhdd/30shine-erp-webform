﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _30shine.Helpers
{
    public class CatchException
    {
        private string RecievedEmail = "vong.lv@hkphone.com.vn,nhat.nv@hkphone.com.vn";
        /// <summary>
        /// Gửi email báo cáo lỗi Exception
        /// </summary>
        /// <param name="pageTitle">Tiêu đề của page</param>
        /// <param name="url">Đường dẫn</param>
        /// <param name="method">Đường dẫn path vào file và Method</param>
        /// <param name="message">Exception message</param>
        public void sendReportException(string pageTitle, string url, string method, string message)
        {
            string subject = "Warning : Report Exception | " + pageTitle;
            string body = "Warning : Report Exception! <br/>";
            body += "Url : " + url + "<br/>";
            body += "Method : " + method + "<br/>";
            body += message;
            Library.Function.SendMailException(RecievedEmail, subject, body );
        }
    }
}