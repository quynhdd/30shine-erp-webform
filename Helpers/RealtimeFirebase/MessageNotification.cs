﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

namespace Project.RealtimeFirebase
{
    public class MessageNotification
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public MessageNotification()
        {
            //
        }

        /// <summary>
        /// Send notification website
        /// </summary>
        /// <param name="deviceId"></param>
        /// <param name="messageJSON"></param>
        /// <returns></returns>
        public string SendNotificationWeb(string deviceId, string messageJSON)
        {
            string SERVER_API_KEY = "AAAAEsp8qqU:APA91bHhuBG2nHV5va12o-yJ-LVZSxZouSUaMNkl8ezB4NWz6Bo-02QTjNFy0sHaZJj3UI7DH3d3doFbjIgm_TcwHgzi2cpVLA13VjHGnMoyocChxwlBZNW-Ze0V42H55pVKBTGPfBx9";
            var SENDER_ID = "80706579109";
            var value = messageJSON;
            WebRequest tRequest;
            tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
            tRequest.Method = "post";
            tRequest.ContentType = "application/json";
            tRequest.Headers.Add(string.Format("Authorization: key={0}", SERVER_API_KEY));

            tRequest.Headers.Add(string.Format("Sender: id={0}", SENDER_ID));

            string postData = "{" +
                                  "\"to\" : \"" + deviceId + "\"," +
                                  "\"priority\" : \"normal\"," +
                                  "\"notification\" : {" +
                                                        "\"body\" : \"30Shine Realtime.\"," +
                                                        "\"title\" : \"Message from 30Shine Realtime\"," +
                                                        "\"icon\" : \"https://30shine.com/images/logo/logo2.png\"" +
                                                      "}," +
                                  "\"data\" : {" +
                                                        "\"volume\" : \"3.21.15\"," +
                                                        "\"contents\" : "+ messageJSON +
                                            "}" +
                                "}";
            Console.WriteLine(postData);
            Byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            tRequest.ContentLength = byteArray.Length;

            Stream dataStream = tRequest.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();

            WebResponse tResponse = tRequest.GetResponse();

            dataStream = tResponse.GetResponseStream();

            StreamReader tReader = new StreamReader(dataStream);

            String sResponseFromServer = tReader.ReadToEnd();


            tReader.Close();
            dataStream.Close();
            tResponse.Close();
            return sResponseFromServer;
        }
    }
}