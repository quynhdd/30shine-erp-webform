﻿using _30shine.MODEL.ENTITY.EDMX;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace _30shine.Helpers
{
    public class ToolSalaryLib
    {
        //public static Solution_30shineEntities db = new Solution_30shineEntities();

        public ToolSalaryLib()
        {
        }
        public static double fixSalary { get; set; }
        public static bool updateFlowSalaryByType(int SalonId, int Type, string timeFrom, string timeTo)
        {
            using (var db = new Solution_30shineEntities())
            {
                bool success = false;
                try
                {
                    DateTime TimeFrom = Convert.ToDateTime(timeFrom, new CultureInfo("vi-VN"));
                    DateTime TimeTo = Convert.ToDateTime(timeTo, new CultureInfo("vi-VN"));
                    if (SalonId > 0 && Type > 0 && TimeTo != null && TimeFrom != null && TimeFrom <= TimeTo)
                    {
                        List<Staff> list = new List<Staff>();
                        list = (from s in db.Staffs
                                where s.SalonId == SalonId
                                && s.Type == Type && s.Active == 1 && s.IsDelete == 0
                                select s).ToList();
                        foreach (var item in list)
                        {
                            updateFlowSalaryByStaffOrBySalon(item.Id, item.SalonId.Value, timeFrom, timeTo);
                        }
                        success = true;
                    }
                    else
                    {
                        success = false;
                    }
                }
                catch (Exception ex)
                {
                    WriteLog(ex.ToString());
                    success = false;
                }
                return success;
            }
        }


        //=========================================
        // Cập nhật salary income cho staff hoặc cả salon
        //=========================================
        public static bool updateFlowSalaryByStaffOrBySalon(int StaffId, int SalonId, string timeFrom, string timeTo)
        {
            using (var db = new Solution_30shineEntities())
            {
                bool Success = false;
                try
                {
                    if (StaffId > 0 || SalonId > 0)
                    {
                        DateTime TimeFrom = Convert.ToDateTime(timeFrom, new CultureInfo("vi-VN"));
                        DateTime TimeTo = Convert.ToDateTime(timeTo, new CultureInfo("vi-VN"));
                        Staff staff = new Staff();
                        List<SalaryIncome> lstIncome = new List<SalaryIncome>();
                        if (TimeTo != null && TimeFrom != null && TimeFrom <= TimeTo)
                        {
                            /// Cập nhật cho stylist
                            /// Lương cứng + Lương phụ cấp( đã thêm trong init ) + Lương part-time + Lương dịch vụ + Lương mỹ phẩm
                            lstIncome = (from si in db.SalaryIncomes
                                         where
                                         si.IsDeleted == false
                                         && ((si.StaffId == StaffId) || (StaffId == 0))
                                         && ((si.SalonId == SalonId) || (SalonId == 0))
                                         && ((si.WorkDate >= TimeFrom) && (si.WorkDate <= TimeTo))
                                         select si).ToList();
                            if (lstIncome.Count > 0)
                            {
                                foreach (var income in lstIncome)
                                {

                                    var record = income;
                                    staff = db.Staffs.FirstOrDefault(w => w.Id == income.StaffId);
                                    fixSalary = record.FixedSalary.Value;
                                    // 4. Lương dịch vụ
                                    if (staff.Type == 1 || staff.Type == 2)
                                    {
                                        DataSalary salary = computeRatingPoint(staff, record.WorkDate.Value);
                                        record.RatingPoint = salary.RatingPoint;
                                        record.ServiceSalary = computeServiceSalary(staff, record.RatingPoint != null ? record.RatingPoint.Value : 0);
                                    }
                                    else if (staff.Type == 5)
                                    {
                                        // 4. 30shine care
                                        //if (!isVoucherLongTime)
                                        //{
                                        // 5. Điểm rating
                                        DataSalary salary = computeRatingPoint(staff, record.WorkDate.Value);
                                        record.RatingPoint = salary.RatingPoint;
                                        record.ServiceSalary = UpdateReceptionSalary(record.WorkDate.Value, record.WorkDate.Value.AddDays(+1), staff);

                                        // }
                                    }
                                    // 5. Lương mỹ phẩm
                                    record.ProductSalary = computeProductSalaryByStaff(staff.Id, record.WorkDate.Value).SalaryProduct;
                                    record.ModifiedTime = DateTime.Now;
                                    db.SalaryIncomes.AddOrUpdate(record);
                                    var update = db.SaveChanges();

                                    // Update statistic product/service
                                    UpdateStatisticProduct(income);
                                    UpdateStatisticService(income, staff.Type.Value);
                                }
                                Success = true;
                            }
                        }
                        else
                        {
                            Success = false;
                        }
                    }
                    else
                    {
                        Success = false;
                    }
                }
                catch (Exception ex)
                {
                    Success = false;
                    WriteLog(ex.ToString());
                }
                return Success;
            }
        }

        public static void UpdateStatisticProduct(SalaryIncome income)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    //List<FlowProduct> flowProduct = new List<FlowProduct>();
                    DateTime? workDate = income.WorkDate;
                    DateTime? workDateTo = income.WorkDate.Value.AddDays(1);
                    var flowProduct = (from fl in db.FlowProducts
                                       where fl.IsDelete == 0
                                           && (fl.CreatedDate >= workDate && fl.CreatedDate < workDateTo)
                                           && fl.SellerId == income.StaffId
                                       group fl by new { fl.SellerId, fl.SalonId, fl.ProductId } into g
                                       select new
                                       {
                                           SalonId = g.FirstOrDefault().SalonId,
                                           StaffId = g.FirstOrDefault().SellerId,
                                           ProductId = g.FirstOrDefault().ProductId,
                                           Total = g.Sum(a => a.Quantity),
                                           TotalMoney = g.Sum(a => a.Quantity) * g.FirstOrDefault().Price * (100 - g.FirstOrDefault().VoucherPercent) / 100
                                       }
                                   ).ToList();
                    if (flowProduct.Count > 0)
                    {
                        foreach (var item in flowProduct)
                        {
                            var statisticRecord = db.StatisticSalaryProducts.FirstOrDefault(
                                    w => w.SalonId == item.SalonId
                                    && w.StaffId == item.StaffId
                                    && w.ProductIds == item.ProductId
                                    && w.WorkDate == income.WorkDate);
                            if (statisticRecord != null)
                            {
                                statisticRecord.Total = item.Total;
                                statisticRecord.TotalMoney = item.TotalMoney;
                                statisticRecord.ModifiedTime = DateTime.Now;
                            }
                            else
                            {
                                statisticRecord = new StatisticSalaryProduct();
                                statisticRecord.SalonId = item.SalonId;
                                statisticRecord.StaffId = item.StaffId;
                                statisticRecord.ProductIds = item.ProductId;
                                statisticRecord.Total = item.Total;
                                statisticRecord.TotalMoney = item.TotalMoney;
                                statisticRecord.WorkDate = income.WorkDate;
                                statisticRecord.IsDelete = false;
                                statisticRecord.CreatedTime = DateTime.Now;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void UpdateStatisticService(SalaryIncome income, int department)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var timeFrom = income.WorkDate;
                    var timeTo = timeFrom.Value.AddDays(1);
                    var listForStaff = new List<StatisService>();
                    string sql = "";
                    switch (department)
                    {
                        case 1:
                            sql = $@"SELECT b.SalonId, b.Staff_Hairdresser_Id AS StaffId, 
                                            s.ServiceId AS ServiceIds, ISNULL(SUM(s.Quantity),0) AS Quantity, 
                                            ISNULL(SUM((s.Quantity * s.Price * CAST(100 - s.VoucherPercent AS FLOAT) / 100)),0) AS TotalMoney 
                                            FROM dbo.FlowService AS s
                                            INNER JOIN dbo.BillService AS b ON b.Id = s.BillId
                                            WHERE s.IsDelete = 0 AND b.IsDelete = 0 AND b.Pending = 0 
                                            and b.CreatedDate >= '{timeFrom}' and b.CreatedDate < '{timeTo}'
                                            AND b.Staff_Hairdresser_Id = {income.StaffId}
                                            GROUP BY b.SalonId,b.Staff_Hairdresser_Id, s.ServiceId";
                            listForStaff = db.Database.SqlQuery<StatisService>(sql).ToList();
                            break;
                        case 2:
                            sql = $@"SELECT b.SalonId, b.Staff_HairMassage_Id AS StaffId, 
                                            s.ServiceId AS ServiceIds, ISNULL(SUM(s.Quantity),0) AS Quantity, 
                                            ISNULL(SUM((s.Quantity * s.Price * CAST(100 - s.VoucherPercent AS FLOAT) / 100)),0) AS TotalMoney 
                                            FROM dbo.FlowService AS s
                                            INNER JOIN dbo.BillService AS b ON b.Id = s.BillId
                                            WHERE s.IsDelete = 0 AND b.IsDelete = 0 AND b.Pending = 0 
                                            and b.CreatedDate >= '{timeFrom}' and b.CreatedDate < '{timeTo}'
                                            AND b.Staff_HairMassage_Id = {income.StaffId}
                                            GROUP BY b.SalonId,b.Staff_HairMassage_Id, s.ServiceId";
                            listForStaff = db.Database.SqlQuery<StatisService>(sql).ToList();
                            break;
                        default: break;
                    }

                    if (listForStaff.Count > 0)
                    {
                        foreach (var item in listForStaff)
                        {
                            var statisticRecord = db.StatisticSalaryServices.FirstOrDefault(
                                    w => w.SalonId == item.SalonId
                                    && w.StaffId == item.StaffId
                                    && w.ServiceIds == item.ServiceIds
                                    && w.WorkDate == income.WorkDate);
                            if (statisticRecord != null)
                            {
                                statisticRecord.Total = Convert.ToInt32(item.Total);
                                statisticRecord.TotalMoney = item.TotalMoney;
                                statisticRecord.ModifiedTime = DateTime.Now;
                            }
                            else
                            {
                                statisticRecord = new StatisticSalaryService();
                                statisticRecord.SalonId = item.SalonId;
                                statisticRecord.StaffId = item.StaffId;
                                statisticRecord.ServiceIds = item.ServiceIds;
                                statisticRecord.Total = Convert.ToInt32(item.Total);
                                statisticRecord.TotalMoney = item.TotalMoney;
                                statisticRecord.WorkDate = income.WorkDate;
                                statisticRecord.IsDelete = false;
                                statisticRecord.CreatedTime = DateTime.Now;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Cập nhật lương mỹ phẩm
        /// </summary>
        /// <param name="bill"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        private static DataSalary computeProductSalaryByStaff(int? StaffId, DateTime date)
        {
            using (var db = new Solution_30shineEntities())
            {
                DataSalary record = new DataSalary();
                try
                {
                    if (StaffId != null && StaffId > 0)
                    {
                        string sql = @"SELECT ISNULL(SUM((fp.Quantity * fp.Price * Cast(fp.ForSalary as float) * Cast(100 - fp.VoucherPercent as float) / 100) / 100),0)  AS SalaryProduct
                                from BillService as bill
                                inner join FlowProduct as fp
                                on bill.Id = fp.BillId and fp.IsDelete != 1
                                left join Product as p
                                on fp.ProductId = p.Id
                                where bill.IsDelete != 1 and bill.Pending != 1
                                and fp.ComboId is null
                                and bill.CompleteBillTime between '" + String.Format("{0:yyyy/MM/dd}", date) + "' and '" + String.Format("{0:yyyy/MM/dd}", date.AddDays(1)) + @"'
                                and bill.SellerId = " + StaffId;
                        record = db.Database.SqlQuery<DataSalary>(sql).FirstOrDefault();
                        if (record is null)
                        {
                            record = new DataSalary();
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                return record;
            }
        }

        /// <summary>
        /// Thống kê doanh số lương checkin
        /// </summary>
        /// <param name="timeFrom"></param>
        /// <param name="timeTo"></param>
        /// <param name="staffId"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        private static double? UpdateReceptionSalary(DateTime timeFrom, DateTime timeTo, Staff staff)
        {
            using (var db = new Solution_30shineEntities())
            {
                double salary = 0;
                try
                {
                    if (staff != null)
                    {
                        string sql = @"begin
                                        With WaitTime as
                                        (
                                        SELECT Coalesce(SUM(CASE WHEN (WaitTimeInProcedure <= 15) THEN 1 ELSE 0 END), 0) as PointBefore15,
	                                        Coalesce(SUM(CASE WHEN WaitTimeInProcedure > 15  and WaitTimeInProcedure <= 20 THEN 1 ELSE 0 END), 0) as PointBetween15and20,
	                                        Coalesce(SUM(CASE WHEN WaitTimeInProcedure > 20 THEN 1 ELSE 0 END), 0) as PointAfter20,
	                                        Coalesce(SUM(CASE WHEN (WaitTimeInProcedure is null) THEN 1 ELSE 0 END), 0) as PointNotTime,
	                                        CheckinId,
	                                        CheckinName
                                        from (
	                                        select bill.Id as BillId, bill.BookingId, bill.CreatedDate, bill.CompleteBillTime, bill.ReceptionId as CheckinId, s.Fullname as CheckinName ,bill.InProcedureTime, Convert(datetime, CONVERT(nvarchar(10), b.DatedBook, 110) + ' ' + CONVERT(nvarchar(10), c.HourFrame)) as DateBook,
		                                        case 
			                                        when (bill.BookingId is not null and (b.IsBookAtSalon is null or b.IsBookAtSalon != 1) and bill.CreatedDate > Convert(datetime, CONVERT(nvarchar(10), b.DatedBook, 110) + ' ' + CONVERT(nvarchar(10), c.HourFrame))) -- check in sau h book
			                                        then DATEDIFF(minute, bill.CreatedDate, bill.InProcedureTime) 
			                                        when (bill.BookingId is not null and (b.IsBookAtSalon is null or b.IsBookAtSalon != 1) and Convert(datetime, CONVERT(nvarchar(10), b.DatedBook, 110) + ' ' + CONVERT(nvarchar(10), c.HourFrame)) > bill.CreatedDate) -- check in trc h book
			                                        then DATEDIFF(minute, Convert(datetime, CONVERT(nvarchar(10), b.DatedBook, 110) + ' ' + CONVERT(nvarchar(10), c.HourFrame)), bill.InProcedureTime)
			                                        when (bill.BookingId is null or b.IsBookAtSalon = 1 or Convert(datetime, CONVERT(nvarchar(10), b.DatedBook, 110) + ' ' + CONVERT(nvarchar(10), c.HourFrame)) is null) -- khách k book
			                                        then DATEDIFF(MINUTE, bill.CreatedDate, bill.InProcedureTime)
			                                        else NULL
		                                        end as WaitTimeInProcedure
			
	                                        from BillService as bill
	                                        left join booking b on bill.BookingId = b.Id
	                                        left join BookHour c on b.HourId = c.Id
	                                        inner join Staff s on s.Id = bill.ReceptionId
	                                        where bill.IsDelete != 1 and bill.Pending != 1
	                                        and (bill.ServiceIds is not null and bill.ServiceIds != '')
	                                        and bill.CompleteBillTime between '" + timeFrom + @"' and  '" + timeTo + @"' 
	                                        and bill.ReceptionId = '" + staff.Id + @"'
	
                                        ) as checkin
                                        group by CheckinId, CheckinName
                                        ),
                                        b as 
                                        (
                                        select a.*,
                                        Round((cast((ISNULL(a.PointBefore15,0)) as float) * 3 + cast( (ISNULL(a.PointBetween15and20,0)) as float) * 1  + cast( (ISNULL(a.PointAfter20,0)) as float) * 0 + cast( (ISNULL(a.PointNotTime,0)) as float) * 1),2) as TotalPoint
                                        from WaitTime as a
                                        )
                                        select * from b
                                        end";
                        var SalaryCheckin = db.Database.SqlQuery<SalaryCheckin>(sql).FirstOrDefault();
                        var salaryConfig = db.SalaryConfigs.FirstOrDefault(w => w.LevelId == staff.SkillLevel && w.DepartmentId == staff.Type);
                        if (SalaryCheckin != null && salaryConfig != null)
                        {
                            salary = SalaryCheckin.TotalPoint * Convert.ToDouble(salaryConfig.RattingSalary);
                        }
                    }
                }
                catch (Exception ex)
                { }
                return salary;
            }
        }
        /// <summary>
        /// Cập nhật lương dịch vụ
        /// Chỉ tính lương dịch vụ cho stylist, skinner, check-in
        /// </summary>
        /// <param name="staff"></param>
        /// <param name="ratingPoint"></param>
        /// <returns></returns>
        private static double computeServiceSalary(Staff staff, double ratingPoint)
        {
            using (var db = new Solution_30shineEntities())
            {
                double salary = 0;
                DateTime WorkDate = DateTime.Now;
                if (staff != null)
                {
                    if (SalaryConfig(staff) != null)
                    {
                        salary = Convert.ToDouble(ratingPoint * SalaryConfig(staff).RattingSalary);
                    }
                }
                return salary;
            }
        }
        /// <summary>
        /// Hàm kiểm Lấy điểm hài lòng theo tăng ca mới.
        /// </summary>
        /// <param name="bill"></param>
        /// <param name="staff"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static double GetRatingPointVer2(Staff staff, DateTime date, configPartTime config)
        {
            using (var db = new Solution_30shineEntities())
            {
                double? ratingPoint = 0;
                try
                {
                    bool isX2 = false;
                    DateTime BillCreateTime = date;
                    DateTime BillCompleteTime = date;
                    DateTime BillCompleteTimeTo = date.AddDays(1);
                    //Lấy hệ số tăng ca mới
                    configPartTime GetCoeffientNew = configPartTime(staff.Id, BillCreateTime.Date);
                    //Lấy hệ số điểm hài lòng của stylist
                    if (staff.Type == 1)
                    {
                        string sql = @"SELECT
                                            b.CompleteBillTime, b.Id, b.BookingId, b.CreatedDate, b.Mark, ISNULL(f.Quantity,0) AS Quantity,
                                            ISNULL(s.ServiceCoefficient, 0) AS ServiceCoefficient,
	                                        ISNULL(r.ConventionPoint,0) AS ConventionPoint, ISNULL(s.CoefficientOvertimeHour,0) AS CoefficientOvertimeHour, 
                                            ISNULL(s.CoefficientOvertimeDay,0) as CoefficientOvertimeDay, 
                                            ISNULL(g.CoefficientGolden, 1) AS CoefficientGolden, ISNULL(s.ServiceBonus,0) AS ServiceBonus 
                                        FROM dbo.FlowService AS f
                                        INNER JOIN dbo.BillService AS b ON f.BillId = b.Id
                                        INNER JOIN dbo.Rating_ConfigPoint AS r ON r.RealPoint = b.Mark
                                        LEFT JOIN dbo.ServiceSalonConfig AS s ON s.ServiceId = f.ServiceId AND s.DepartmentId = " + staff.Type + @" AND s.SalonId = b.SalonId AND s.IsPublish = 1 AND s.IsDelete = 0
                                        LEFT JOIN dbo.ServiceSalonGoldTime AS g ON g.HoursId = s.Id AND g.IsPublish = 1 AND g.IsDelete = 0
                                        WHERE 
                                            f.IsDelete = 0 AND b.Pending = 0 
                                            AND b.IsDelete = 0 AND r.IsDelete = 0 
                                            AND r.Hint = 3 AND r.[Status] = 1 
                                            AND b.CompleteBillTime >=  '" + String.Format("{0:yyyy/MM/dd}", BillCompleteTime) + @"'  AND b.CompleteBillTime < '" + String.Format("{0:yyyy/MM/dd}", BillCompleteTimeTo) + @"'
                                            AND b.Staff_Hairdresser_Id = " + staff.Id;
                        var dataBill = db.Database.SqlQuery<ListDataBill>(sql).ToList();
                        foreach (var Bills in dataBill)
                        {
                            // lay thang 
                            int Month = DateTime.Now.Month;
                            //lay nam
                            int Year = DateTime.Now.Year;
                            //lay ngay trong thang _ nam
                            int days = DateTime.DaysInMonth(Year, Month);
                            DateTime timeFrom = new DateTime(Bills.CreatedDate.Year, Bills.CreatedDate.Month, 1);
                            DateTime timeTo = Bills.CreatedDate;
                            int TimeKeeping = (from f in db.FlowTimeKeepings
                                               where f.WorkDate >= timeFrom && f.WorkDate <= timeTo && f.StaffId == staff.Id && f.IsEnroll == true && f.IsDelete == 0
                                               select f).Count();
                            if (TimeKeeping >= (days - 4) && config.IsPartTimeSalon == true && date >= timeFrom.AddDays(days - 5))
                            {
                                ratingPoint += Bills.Quantity * Bills.ServiceCoefficient * Bills.ConventionPoint * GetCoeffientNew.CoefficientSalon;
                            }
                            else if (checkHourPartTime(Bills.BookingId, staff, db, Bills.CreatedDate) && config.IsPartTimeRating == true)
                            {
                                ratingPoint += Bills.Quantity * Bills.ServiceCoefficient * Bills.ConventionPoint * GetCoeffientNew.CoefficientStylist;
                            }
                            else
                            {
                                ratingPoint += Bills.Quantity * Bills.ServiceCoefficient * Bills.ConventionPoint;
                            }
                        }
                    }
                    //Lấy hệ số điểm hài lòng của Skinner
                    else if (staff.Type == 2)
                    {
                        string sql = @"SELECT
                                            b.CompleteBillTime, b.Id, b.BookingId, b.CreatedDate, b.Mark, ISNULL(f.Quantity,0) AS Quantity,
                                            ISNULL(s.ServiceCoefficient, 0) AS ServiceCoefficient,
	                                        ISNULL(r.ConventionPoint,0) AS ConventionPoint, ISNULL(s.CoefficientOvertimeHour,0) AS CoefficientOvertimeHour, 
                                            ISNULL(s.CoefficientOvertimeDay,0) as CoefficientOvertimeDay, 
                                            ISNULL(g.CoefficientGolden, 1) AS CoefficientGolden, ISNULL(s.ServiceBonus,0) AS ServiceBonus 
                                        FROM dbo.FlowService AS f
                                        INNER JOIN dbo.BillService AS b ON f.BillId = b.Id
                                        INNER JOIN dbo.Rating_ConfigPoint AS r ON r.RealPoint = b.Mark
                                        LEFT JOIN dbo.ServiceSalonConfig AS s ON s.ServiceId = f.ServiceId AND s.DepartmentId = " + staff.Type + @" AND s.SalonId = b.SalonId AND s.IsPublish = 1 AND s.IsDelete = 0
                                        LEFT JOIN dbo.ServiceSalonGoldTime AS g ON g.HoursId = s.Id AND g.IsPublish = 1 AND g.IsDelete = 0
                                        WHERE 
                                            f.IsDelete = 0 AND b.Pending = 0 
                                            AND b.IsDelete = 0 AND r.IsDelete = 0 
                                            AND r.Hint = 3 AND r.[Status] = 1 
                                            AND b.CompleteBillTime >=  '" + String.Format("{0:yyyy/MM/dd}", BillCompleteTime) + @"'  AND b.CompleteBillTime < '" + String.Format("{0:yyyy/MM/dd}", BillCompleteTimeTo) + @"'
                                            AND b.Staff_HairMassage_Id = " + staff.Id;
                        var dataBill = db.Database.SqlQuery<ListDataBill>(sql).ToList();
                        foreach (var Bills in dataBill)
                        {
                            // lay thang 
                            int Month = DateTime.Now.Month;
                            //lay nam
                            int Year = DateTime.Now.Year;
                            //lay ngay trong thang _ nam
                            int days = DateTime.DaysInMonth(Year, Month);
                            DateTime timeFrom = new DateTime(Bills.CreatedDate.Year, Bills.CreatedDate.Month, 1);
                            DateTime timeTo = Bills.CreatedDate;
                            int TimeKeeping = (from f in db.FlowTimeKeepings
                                               where f.WorkDate >= timeFrom && f.WorkDate <= timeTo && f.StaffId == staff.Id && f.IsEnroll == true && f.IsDelete == 0
                                               select f).Count();
                            if (TimeKeeping >= (days - 4) && config.IsPartTimeSalon == true && date >= timeFrom.AddDays(days - 5))
                            {
                                ratingPoint += Bills.Quantity * Bills.ServiceCoefficient * Bills.ConventionPoint * GetCoeffientNew.CoefficientSalon;
                            }
                            else if (checkHourPartTime(Bills.BookingId, staff, db, Bills.CreatedDate) && config.IsPartTimeRating == true)
                            {
                                ratingPoint += Bills.Quantity * Bills.ServiceCoefficient * Bills.ConventionPoint * GetCoeffientNew.CoefficientStylist;
                            }
                            else
                            {
                                ratingPoint += Bills.Quantity * Bills.ServiceCoefficient * Bills.ConventionPoint;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Có lỗi xảy ra xin vui lòng liên hệ nhà phát triển!!!");
                }

                return Convert.ToDouble(ratingPoint);
            }
        }

        /// <summary>
        /// Kiểm tra stylist,skinner có trong giờ tăng ca hay không.
        /// </summary>
        /// <param name="staff"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        private static bool checkHourPartTime(int BookingId, Staff staff, Solution_30shineEntities db, DateTime WorkDate)
        {
            bool check = false;
            //TimeSpan WorkHour = new TimeSpan(IntHour, IntMinute, 0);
            int StaffId = staff.Id;
            try
            {

                var WorkTime = db.FlowTimeKeepings.Where(r => r.IsDelete == 0 && r.IsEnroll == true && r.WorkDate == WorkDate.Date && r.StaffId == StaffId).FirstOrDefault();

                if (WorkTime != null)
                {
                    //Lay khung gio lam viec
                    int WorkTimeId = WorkTime.WorkTimeId.Value;
                    //Lay so gio tang ca
                    int WorkHour = WorkTime.WorkHour.Value;
                    //Lay chuoi gio tang ca
                    string[] strHourIds = WorkTime.HourIds.Split(',');
                    //var test = strHourIds.Length;
                    //int bookhourId = Convert.ToInt32(strHourIds[strHourIds.Length-1]);
                    int[] intHourIds = new int[strHourIds.Length];
                    for (int i = 0; i < strHourIds.Length; i++)
                    {
                        intHourIds[i] = strHourIds[i] != "" ? Convert.ToInt32(strHourIds[i]) : 0;
                    }
                    // kiem tra chuoi id gio, co gio tang ca hay khong
                    //TimeSpan BookHour = db.BookHours.Where(r => r.Id == bookhourId && r.IsDelete == 0).Select(r=>r.HourFrame).FirstOrDefault().Value;
                    //if (BookHour != null)
                    //{
                    var BookHourFrame = (
                                               from b in db.Bookings
                                               join a in db.BookHours on b.HourId equals a.Id
                                               where a.IsDelete == 0 && a.Publish == true && b.IsDelete == 0 && b.Id == BookingId
                                               select new
                                               {
                                                   HourFrame = a.HourFrame,
                                                   HourId = a.Id
                                               }).FirstOrDefault();


                    if (BookHourFrame != null)
                    {
                        var WorkTimeHour = db.WorkTimes.Where(r => r.Id == WorkTimeId).FirstOrDefault();
                        if (WorkTimeHour != null)
                        {
                            if (BookHourFrame.HourFrame < WorkTimeHour.StrartTime || BookHourFrame.HourFrame > WorkTimeHour.EnadTime)
                            {
                                check = Array.IndexOf(intHourIds, BookHourFrame.HourId) != -1 ? true : false;
                            }
                        }
                    }
                    //}

                }

            }
            catch (Exception ex)
            {
                check = false;
            }
            return check;
        }

        private static double computeServiceSalaryTotalBill(BillService bill)
        {
            using (var db = new Solution_30shineEntities())
            {
                double salary = 0;
                try
                {
                    if (bill != null)
                    {
                        DateTime timeFrom = bill.CompleteBillTime.Value.Date;
                        DateTime timeTo = bill.CompleteBillTime.Value.Date.AddDays(1);
                        int? bills = db.BillServices.Where(w => w.CompleteBillTime >= timeFrom && w.CompleteBillTime < timeTo && w.IsDelete != 1 && w.Pending != 1 && w.ServiceIds != "" && w.ServiceIds != null).Count();
                        if (bills != null)
                        {
                            var config = db.SalaryConfigStaffs.FirstOrDefault(w => w.StaffId == 364 && w.IsDeleted == false);
                            if (config != null)
                            {
                                salary = Convert.ToDouble(bills * config.FixSalaryOscillation);
                            }
                        }
                    }
                }
                catch
                {

                }
                return salary;
            }
        }
        /// <summary>
        /// Cập nhật lương mỹ phẩm
        /// </summary>
        /// <param name="bill"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        private static double computeProductSalary(BillService bill, DateTime date)
        {
            using (var db = new Solution_30shineEntities())
            {
                double salary = 0;
                if (bill != null && bill.SellerId > 0)
                {
                    string sql = @"select Coalesce(sum(Coalesce(fp.Price * fp.Quantity * (Cast(Coalesce(p.ForSalary, 0) as float)/CAST(100 as float)), 0)), 0) as salary
                                from BillService as bill
                                inner join FlowProduct as fp
                                on bill.Id = fp.BillId and fp.IsDelete != 1
                                left join Product as p
                                on fp.ProductId = p.Id
                                where bill.IsDelete != 1 and bill.Pending != 1
                                and fp.ComboId is null
                                and bill.CompleteBillTime between '" + String.Format("{0:yyyy/MM/dd}", date) + "' and '" + String.Format("{0:yyyy/MM/dd}", date.AddDays(1)) + @"'
                                and bill.SellerId = " + bill.SellerId;
                    salary = db.Database.SqlQuery<double>(sql).FirstOrDefault();

                }
                return salary;
            }
        }
        private static DataSalary computeRatingPoint(Staff staff, DateTime date)
        {
            using (var db = new Solution_30shineEntities())
            {
                DataSalary rating = new DataSalary();
                configPartTime configPT = new configPartTime();
                string sql = "";
                if (staff != null)
                {
                    // Áp dụng tính bill đặc biệt cho stylist và skinner (vd : 1 bill có 2 dịch vụ thì tính điểm cho 2 dịch vụ)
                    switch (staff.Type)
                    {
                        case 1:
                            //Nếu áp dụng tăng ca theo hệ số điểm hài lòng thì cập nhật lại toàn bộ lương dịch vụ trong ngày..
                            configPT = configPartTime(staff.Id, date);
                            rating.RatingPoint = GetRatingPointVer2(staff, date, configPT);
                            break;
                        case 2:
                            configPT = configPartTime(staff.Id, date);
                            rating.RatingPoint = GetRatingPointVer2(staff, date, configPT);
                            break;
                        case 5:
                            sql = @"select Cast(Coalesce(SUM(Coalesce(point, 0)),0) as float) as RatingPoint
                                from
                                (
	                                select 
		                                Cast(rcp.ConventionPoint as float) as point
	                                from  BillService as bill
	                                inner join Rating_ConfigPoint as rcp
	                                on rcp.RealPoint = bill.Mark and rcp.Hint = 1 and rcp.IsDelete != 1 and rcp.[Status] = 1
	                                where bill.IsDelete != 1 and bill.Pending != 1 
                                        and bill.CompleteBillTime between '" + String.Format("{0:yyyy/MM/dd}", date) + "' and '" + String.Format("{0:yyyy/MM/dd}", date.AddDays(1)) + @"'
                                        and bill.ServiceIds != ''
		                                and bill.ReceptionId = " + staff.Id + @"
                                ) as a";
                            rating = db.Database.SqlQuery<DataSalary>(sql).FirstOrDefault();
                            break;
                    }
                }
                return rating;
            }
        }

        /// <summary>
        /// Lấy thông tin cấu hình Tăng ca trong bảng ConfigPartTime
        /// </summary>
        /// <param name="staff_id"></param>
        /// <param name="sDate"></param>
        /// <returns></returns>
        public static configPartTime configPartTime(int staff_id, DateTime sDate)
        {
            configPartTime dataConfig = new configPartTime();
            //khởi tạo giá trị mặc định
            dataConfig.IsPartTimeHour = false;
            dataConfig.IsPartTimeRating = false;
            dataConfig.IsPartTimeSalon = false;
            dataConfig.CoefficientStylist = 1;
            dataConfig.coefficinetSkinner = 1;
            try
            {
                using (Solution_30shineEntities db = new Solution_30shineEntities())
                {
                    var salonPartTime = db.FlowTimeKeepings.Where(r => r.StaffId == staff_id && r.WorkDate == sDate && r.IsEnroll == true).FirstOrDefault();
                    if (salonPartTime != null)
                    {
                        var dataSalonConfig = db.Tbl_Salon.Where(r => r.IsDelete == 0 && r.Id == salonPartTime.SalonId).FirstOrDefault();
                        if (dataSalonConfig != null)
                        {
                            dataConfig.IsPartTimeSalon = dataSalonConfig.IsPartTimeSalon;
                            dataConfig.IsPartTimeHour = dataSalonConfig.IsPartTimeHours;
                            dataConfig.IsPartTimeRating = dataSalonConfig.IsPartTimeRating;
                            var dataConfigPartTime = db.ConfigPartTimes.Where(r => r.IsDelete == false && r.SalonId == dataSalonConfig.Id).FirstOrDefault();
                            if (dataConfigPartTime != null)
                            {
                                dataConfig.CoefficientSalon = dataConfigPartTime.CoefficientSalon;
                                dataConfig.CoefficientStylist = dataConfigPartTime.CoefficientStylist;
                                dataConfig.coefficinetSkinner = dataConfigPartTime.CoefficinetSkinner;
                            }

                        }
                    }

                }
            }
            catch (Exception ex)
            {

            }
            return dataConfig;
        }
        /// <summary>
        /// kiểm tra xem id truyền vào có phải là stylist hoặc skinner không
        /// </summary>
        /// <param name="stylist_id">id stylist</param>
        /// <param name="skinner_id">id skinner</param>
        /// <returns></returns>
        public static cls_stylist_skinner checkStylistSkinner(int StaffId)
        {
            cls_stylist_skinner objcheck = new cls_stylist_skinner();
            objcheck.stylist = false;
            objcheck.skinner = false;
            try
            {
                using (Solution_30shineEntities db = new Solution_30shineEntities())
                {
                    var checkStylistSkinner = db.Staffs.Where(s => s.IsDelete == 0 && s.Id == StaffId).FirstOrDefault();
                    if (checkStylistSkinner.Type == 1)
                    {
                        objcheck.stylist = checkStylistSkinner == null ? false : true;
                    }
                    else if (checkStylistSkinner.Type == 2)
                    {
                        objcheck.skinner = checkStylistSkinner == null ? false : true;
                    }
                }
            }
            catch
            { }
            return objcheck;
        }

        /// <summary>
        /// Kiểm tra nhân viên đã được chấm công hay chưa
        /// - Riêng đối với stylist và skinner, công được tính khi 1 ngày tham gia đc ít nhất 3 bill
        /// </summary>
        /// <param name="staff"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        private static bool isEnroll(Staff staff, Solution_30shineEntities db, DateTime date)
        {
            bool enroll = false;
            if (staff != null)
            {
                var ftk = new FlowTimeKeeping();
                //if (staff.SalonId > 0)
                //{
                //    ftk = db.FlowTimeKeepings.FirstOrDefault(w => w.StaffId == staff.Id && w.SalonId == staff.SalonId && w.WorkDate == date && w.IsDelete != 1);
                //}
                //else
                //{ 
                ftk = db.FlowTimeKeepings.FirstOrDefault(w => w.StaffId == staff.Id && w.WorkDate == date && w.IsDelete != 1);
                //}
                if (ftk != null && ftk.IsEnroll == true)
                {
                    enroll = true;
                }
            }
            return enroll;
        }

        /// <summary>
        /// Get hệ số lương dịch vụ
        /// </summary>
        /// <param name="staff"></param>
        /// <returns></returns>
        private static SalaryConfig SalaryConfig(Staff staff)
        {
            using (var db = new Solution_30shineEntities())
            {
                try
                {
                    SalaryConfig salaryConfig = db.SalaryConfigs.FirstOrDefault(w => w.DepartmentId == staff.Type && w.LevelId == staff.SkillLevel && w.IsDeleted == false);
                    return salaryConfig;
                }
                catch
                {
                    throw new Exception();
                }
            }
        }

        private static void WriteLog(string strLog)
        {

            StreamWriter log;
            FileStream fileStream = null;
            DirectoryInfo logDirInfo = null;
            FileInfo logFileInfo;
            string dirPath = HttpContext.Current.Server.MapPath("~");
            string logFilePath = dirPath + "Logs\\";
            logFilePath = logFilePath + "Log-" + System.DateTime.Today.ToString("MM-dd-yyyy") + "." + "txt";
            logFileInfo = new FileInfo(logFilePath);
            logDirInfo = new DirectoryInfo(logFileInfo.DirectoryName);
            if (!logDirInfo.Exists) logDirInfo.Create();
            if (!logFileInfo.Exists)
            {
                fileStream = logFileInfo.Create();
            }
            else
            {
                fileStream = new FileStream(logFilePath, FileMode.Append);
            }
            log = new StreamWriter(fileStream);
            log.WriteLine(strLog);
            log.Close();
        }
    }
    public class configPartTime
    {
        public bool? IsPartTimeHour { get; set; }
        public bool? IsPartTimeSalon { get; set; }
        public bool? IsPartTimeRating { get; set; }
        public double? CoefficientStylist { get; set; }
        public double? CoefficientSalon { get; set; }
        public double? coefficinetSkinner { get; set; }
    }

    public class SalaryCheckin
    {
        public int PointBefore15 { get; set; }
        public int PointBetween15and20 { get; set; }
        public int PointAfter20 { get; set; }
        public int PointNotTime { get; set; }
        public int CheckinId { get; set; }
        public string CheckinName { get; set; }
        public double TotalPoint { get; set; }
    }
}
