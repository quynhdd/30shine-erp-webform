using _30shine.MODEL.CustomClass;
using Libraries;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace _30shine.Helpers
{
    public static class SecurityLib
    {
        public static string GenPassword(MD5 md5Hash, string input)
        {
            var scrPush = "uaefsfkoiu&@(^%=";
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input + scrPush));
            StringBuilder sBuilder = new StringBuilder();

            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            return sBuilder.ToString();
        }

        /// <summary>
        /// QuynhDD - 20180917
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string Encrypt(string value)
        {
            if (string.IsNullOrEmpty(value))
                return string.Empty;
            var md5 = new MD5CryptoServiceProvider();
            byte[] valueArray = Encoding.ASCII.GetBytes(value);
            valueArray = md5.ComputeHash(valueArray);
            var sb = new StringBuilder();
            for (int i = 0; i < valueArray.Length; i++)
                sb.Append(valueArray[i].ToString("x2").ToLower());
            return sb.ToString();
        }

        private static bool VerifyPassword(MD5 md5Hash, string input, string hash)
        {
            string hashOfInput = GenPassword(md5Hash, input);
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            if (0 == comparer.Compare(hashOfInput, hash))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// get user Id
        /// </summary>
        /// <returns></returns>
        public static int getUserId()
        {
            int integer;
            return int.TryParse(HttpContext.Current.Session["User_Id"].ToString(), out integer) ? integer : 0;
        }

        // Verify host from request
        // @return void
        public static dynamic VerifyHostFromRequest()
        {
            string[] verifiedDomain = {
                "30shine.com",
                "30shine.net",
                "210.211.99.159",
                "10.0.2.76",
                "erpdev.30shine.net",
            };
            string host = HttpContext.Current.Request.UrlReferrer.Host;

            if (Array.Exists(verifiedDomain, element => element == host) == true)
            {
                return true;
            }

            bool verified = false;
            foreach (string s in verifiedDomain)
            {
                if (host.LastIndexOf('.' + s) != -1)
                {
                    verified = true;
                    break;
                }
            }

            if (!verified)
            {
                HttpContext.Current.Response.Close();

            }

            return true;
        }

        /// <summary>
        /// Check quyền root hoặc admin
        /// </summary>
        /// <returns></returns>
        public static bool CheckPermissionAdminOrRoot()
        {
            var arrayPermision = HttpContext.Current.Session["User_Permission"].ToString().Split(',');
            if (arrayPermision.Length > 0)
            {
                string checkAdmin = Array.Find(arrayPermision, s => s.Equals("ADMIN"));
                string checkRoot = Array.Find(arrayPermision, s => s.Equals("root"));
                if (!String.IsNullOrEmpty(checkRoot) || !String.IsNullOrEmpty(checkAdmin))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// PhucDn 20180921
        /// </summary>
        /// <returns></returns>
        public static bool CheckQlGiamSat()
        {
            var objStaff = (AccountModelClass.AccountSession)HttpContext.Current.Session["AccountInfo"];
            var deparmentId = objStaff.UserDepartmentId;
            if (deparmentId == 19)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //public static string GetDomain()
        //{
        //    var domain = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host;//VD: http://erp.30shine.com
        //    var host = HttpContext.Current.Request.Url.Host;
        //    var port = HttpContext.Current.Request.Url.Port;
        //    var strUrlTest = AppConstants.LIST_URL_TEST;
        //    if (!string.IsNullOrEmpty(strUrlTest))
        //    {
        //        var listUrlTest = strUrlTest.IndexOf(host);
        //        if (listUrlTest >= 0)
        //        {
        //            return domain + ":" + port;
        //        }
        //    }
        //    return domain;
        //}
    }
}