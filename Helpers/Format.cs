﻿using Helpers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace Library
{
    public class Format
    {
        public static string getDateTimeString(DateTime dateTime, string format = "yyyy/MM/dd")
        {
            return String.Format("{0:" + format + "}", dateTime);
        }

        /// <summary>
        /// Convert To DateTime from string
        /// </summary>
        /// <param name="dateTimeString"></param>
        /// <returns></returns>
        public static DateTime? getDateTimeFromString(string dateTimeString, string locale = "vi-VN")
        {
            try
            {
                DateTime? dateTime;
                if (dateTimeString != "")
                {
                    dateTime = Convert.ToDateTime(dateTimeString, Locale.getCultureInfo(locale));
                }
                else
                {
                    dateTime = null;
                }
                return dateTime;
            }
            catch (Exception Ex)
            {
                throw new Exception("Lỗi Convert DateTime {" + Ex.Message + "}");
            }
        }

        /// <summary>
        /// Format number
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string formatNumber(dynamic obj, string format = "#,###")
        {
            return String.Format("{0:" + format + "}", obj).Replace(",", ".");
        }

        /// <summary>
        /// Format decimal
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="numberCharacter"></param>
        /// <returns></returns>
        public static string formatDecimal(dynamic obj, int numberCharacter)
        {
            var pattern = "";
            for (var i = 0; i < numberCharacter; i++)
            {
                pattern += "0";
            }
            var s = string.Format("{0:0." + pattern + "}", obj);

            if (s.EndsWith(pattern))
            {
                return ((int)obj).ToString();
            }
            else
            {
                return s;
            }
        }

        public static string ObjectToJson(dynamic _object)
        {
            var serializer = new JavaScriptSerializer();
            return serializer.Serialize(_object);
        }

        /// <summary>
        /// get culture info
        /// </summary>
        /// <returns></returns>
        public static CultureInfo GetCultureInfo()
        {
            return new CultureInfo("vi-VN");
        }

        /// <summary>
        /// replace phone
        /// </summary>
        /// <param name="phone">số điện thoại</param>
        /// <param name="repeat">chèn mấy số</param>
        /// <param name="format">định dạng của số điện thoại</param>
        /// <returns></returns>
        public static string ReplaceFirstPhone(string phone, int repeat = 4, string format = "X")
        {
            if (!String.IsNullOrEmpty(phone) && phone.Length > 4)
            {
                var concat = String.Concat(Enumerable.Repeat(format, repeat)).ToString();
                return phone.Remove(0, repeat).Insert(0, concat);
            }
            return phone;
        }
    }
}