﻿using _30shine.MODEL.ENTITY.EDMX;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Web;


namespace _30shine.Helpers
{
    public class ToolSalaryLib_V2
    {
        //public static Solution_30shineEntities db = new Solution_30shineEntities();

        public ToolSalaryLib_V2()
        {
        }
        public static double fixSalary { get; set; }
        //public static bool updateFlowSalaryByType(int SalonId, int Type, string timeFrom, string timeTo)
        //{
        //    using (var db = new Solution_30shineEntities())
        //    {
        //        bool success = false;
        //        try
        //        {
        //            DateTime TimeFrom = Convert.ToDateTime(timeFrom, new CultureInfo("vi-VN"));
        //            DateTime TimeTo = Convert.ToDateTime(timeTo, new CultureInfo("vi-VN"));
        //            if (SalonId > 0 && Type > 0 && TimeTo != null && TimeFrom != null && TimeFrom <= TimeTo)
        //            {
        //                List<Staff> list = new List<Staff>();
        //                list = (from s in db.Staffs
        //                        where s.SalonId == SalonId
        //                        && s.Type == Type && s.Active == 1 && s.IsDelete == 0
        //                        select s).ToList();
        //                foreach (var item in list)
        //                {
        //                    updateFlowSalaryByStaffOrBySalon(item.Id, item.SalonId.Value, timeFrom, timeTo);
        //                }
        //                success = true;
        //            }
        //            else
        //            {
        //                success = false;
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            throw ex;
        //        }
        //        return success;
        //    }
        //}


        //=========================================
        // Cập nhật salary income cho staff hoặc cả salon
        //=========================================
        public static bool updateFlowSalaryByStaffOrBySalon(int StaffId, int SalonId, int DepartmentId, string timeFrom, string timeTo)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    bool Success = false;
                    if (StaffId > 0 || SalonId > 0 || DepartmentId > 0)
                    {
                        DateTime TimeFrom = Convert.ToDateTime(timeFrom, new CultureInfo("vi-VN"));
                        DateTime TimeTo = Convert.ToDateTime(timeTo, new CultureInfo("vi-VN"));
                        Staff staff = new Staff();
                        List<SalaryIncome> lstIncome = new List<SalaryIncome>();
                        DataSalary dataSalary = new DataSalary();
                        if (TimeTo != null && TimeFrom != null && TimeFrom <= TimeTo)
                        {
                            /// Cập nhật cho stylist
                            /// Lương cứng + Lương phụ cấp( đã thêm trong init ) + Lương part-time + Lương dịch vụ + Lương mỹ phẩm
                            lstIncome = (from si in db.SalaryIncomes
                                         join s in db.Staffs on si.StaffId equals s.Id
                                         where
                                         si.IsDeleted == false
                                         && ((si.StaffId == StaffId) || (StaffId == 0))
                                         && ((si.SalonId == SalonId) || (SalonId == 0))
                                         && ((s.Type == DepartmentId) || (DepartmentId == 0))
                                         && ((si.WorkDate >= TimeFrom) && (si.WorkDate <= TimeTo))
                                         select si).ToList();
                            if (lstIncome.Count > 0)
                            {
                                foreach (var income in lstIncome)
                                {

                                    var record = income;
                                    staff = db.Staffs.FirstOrDefault(w => w.Id == income.StaffId);
                                    fixSalary = record.FixedSalary.Value;
                                    // 4. Lương dịch vụ
                                    dataSalary = GetRatingPointVer2(staff, record.WorkDate.Value);
                                    record.RatingPoint = dataSalary.RatingPoint;
                                    record.ServiceSalary = dataSalary.SalaryService;
                                    if (staff.Type == 5)
                                    {
                                        dataSalary = RatingPointReception(staff, record.WorkDate.Value);
                                        record.ServiceSalary = record.ServiceSalary + UpdateReceptionSalary(record.WorkDate.Value, record.WorkDate.Value.AddDays(+1), staff);
                                    }
                                    // 5. Lương mỹ phẩm
                                    record.ProductSalary = computeProductSalaryByStaff(staff.Id, record.WorkDate.Value).SalaryProduct;
                                    // add 20190321
                                    // 6. get luong KPIProduct
                                    var KPIProduct = KPISalaryProductByStaff(staff.Id, record.WorkDate.Value).KPISalary;
                                    // 7. get luong KPIService
                                    var KPIService = KPISalaryServiceByStaff(staff, record.WorkDate.Value).KPISalary;
                                    // sum 
                                    record.KPISalary = KPIProduct + KPIService;

                                    record.ModifiedTime = DateTime.Now;
                                    db.SalaryIncomes.AddOrUpdate(record);
                                    var update = db.SaveChanges();

                                    // Update statistic product/service
                                    UpdateStatisticProduct(income);
                                    UpdateStatisticService(income, staff.Type.Value);
                                }
                                Success = true;
                            }
                        }
                        else
                        {
                            Success = false;
                        }
                    }
                    else
                    {
                        Success = false;
                    }
                    return Success;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void UpdateStatisticProduct(SalaryIncome income)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    //List<FlowProduct> flowProduct = new List<FlowProduct>();
                    DateTime? workDate = income.WorkDate;
                    DateTime? workDateTo = income.WorkDate.Value.AddDays(1);
                    var flowProduct = (from fl in db.FlowProducts
                                       where fl.IsDelete == 0
                                           && (fl.CreatedDate >= workDate && fl.CreatedDate < workDateTo)
                                           && fl.SellerId == income.StaffId
                                       group fl by new { fl.SellerId, fl.SalonId, fl.ProductId } into g
                                       select new
                                       {
                                           SalonId = g.FirstOrDefault().SalonId,
                                           StaffId = g.FirstOrDefault().SellerId,
                                           ProductId = g.FirstOrDefault().ProductId,
                                           Total = g.Sum(a => a.Quantity),
                                           TotalMoney = g.Sum(a => a.Quantity) * g.FirstOrDefault().Price * (100 - g.FirstOrDefault().VoucherPercent) / 100
                                       }
                                   ).ToList();
                    if (flowProduct.Count > 0)
                    {
                        foreach (var item in flowProduct)
                        {
                            var statisticRecord = db.StatisticSalaryProducts.FirstOrDefault(
                                    w => w.SalonId == item.SalonId
                                    && w.StaffId == item.StaffId
                                    && w.ProductIds == item.ProductId
                                    && w.WorkDate == income.WorkDate);
                            if (statisticRecord != null)
                            {
                                statisticRecord.Total = item.Total;
                                statisticRecord.TotalMoney = item.TotalMoney;
                                statisticRecord.ModifiedTime = DateTime.Now;
                            }
                            else
                            {
                                statisticRecord = new StatisticSalaryProduct();
                                statisticRecord.SalonId = item.SalonId;
                                statisticRecord.StaffId = item.StaffId;
                                statisticRecord.ProductIds = item.ProductId;
                                statisticRecord.Total = item.Total;
                                statisticRecord.TotalMoney = item.TotalMoney;
                                statisticRecord.WorkDate = income.WorkDate;
                                statisticRecord.IsDelete = false;
                                statisticRecord.CreatedTime = DateTime.Now;
                            }
                            db.StatisticSalaryProducts.AddOrUpdate(statisticRecord);
                            db.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void UpdateStatisticService(SalaryIncome income, int department)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    string timeFrom = String.Format("{0:yyyy/MM/dd}", income.WorkDate);
                    string timeTo = String.Format("{0:yyyy/MM/dd}", income.WorkDate.Value.AddDays(1));
                    var listForStaff = new List<StatisService>();
                    string sql = "";
                    switch (department)
                    {
                        case 1:
                            sql = $@"SELECT b.SalonId, b.Staff_Hairdresser_Id AS StaffId, 
                                            s.ServiceId AS ServiceIds, ISNULL(SUM(s.Quantity),0) AS Total, 
                                            ISNULL(SUM((s.Quantity * s.Price * CAST(100 - s.VoucherPercent AS FLOAT) / 100)),0) AS TotalMoney 
                                            FROM dbo.FlowService AS s
                                            INNER JOIN dbo.BillService AS b ON b.Id = s.BillId
                                            WHERE s.IsDelete = 0 AND b.IsDelete = 0 AND b.Pending = 0 
                                            and b.CreatedDate >= '{timeFrom}' and b.CreatedDate < '{timeTo}'
                                            AND b.Staff_Hairdresser_Id = {income.StaffId}
                                            GROUP BY b.SalonId,b.Staff_Hairdresser_Id, s.ServiceId";
                            listForStaff = db.Database.SqlQuery<StatisService>(sql).ToList();
                            break;
                        case 2:
                            sql = $@"SELECT b.SalonId, b.Staff_HairMassage_Id AS StaffId, 
                                            s.ServiceId AS ServiceIds, ISNULL(SUM(s.Quantity),0) AS Total, 
                                            ISNULL(SUM((s.Quantity * s.Price * CAST(100 - s.VoucherPercent AS FLOAT) / 100)),0) AS TotalMoney 
                                            FROM dbo.FlowService AS s
                                            INNER JOIN dbo.BillService AS b ON b.Id = s.BillId
                                            WHERE s.IsDelete = 0 AND b.IsDelete = 0 AND b.Pending = 0 
                                            and b.CreatedDate >= '{timeFrom}' and b.CreatedDate < '{timeTo}'
                                            AND b.Staff_HairMassage_Id = {income.StaffId}
                                            GROUP BY b.SalonId,b.Staff_HairMassage_Id, s.ServiceId";
                            listForStaff = db.Database.SqlQuery<StatisService>(sql).ToList();
                            break;
                        default: break;
                    }

                    if (listForStaff.Count > 0)
                    {
                        foreach (var item in listForStaff)
                        {
                            var statisticRecord = db.StatisticSalaryServices.FirstOrDefault(
                                    w => w.SalonId == item.SalonId
                                    && w.StaffId == item.StaffId
                                    && w.ServiceIds == item.ServiceIds
                                    && w.WorkDate == income.WorkDate);
                            if (statisticRecord != null)
                            {
                                statisticRecord.Total = Convert.ToInt32(item.Total);
                                statisticRecord.TotalMoney = item.TotalMoney;
                                statisticRecord.ModifiedTime = DateTime.Now;
                            }
                            else
                            {
                                statisticRecord = new StatisticSalaryService();
                                statisticRecord.SalonId = item.SalonId;
                                statisticRecord.StaffId = item.StaffId;
                                statisticRecord.ServiceIds = item.ServiceIds;
                                statisticRecord.Total = Convert.ToInt32(item.Total);
                                statisticRecord.TotalMoney = item.TotalMoney;
                                statisticRecord.WorkDate = income.WorkDate;
                                statisticRecord.IsDelete = false;
                                statisticRecord.CreatedTime = DateTime.Now;
                            }
                            db.StatisticSalaryServices.AddOrUpdate(statisticRecord);
                            db.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Cập nhật lương mỹ phẩm
        /// </summary>
        /// <param name="bill"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        private static DataSalary computeProductSalaryByStaff(int? StaffId, DateTime date)
        {
            using (var db = new Solution_30shineEntities())
            {
                DataSalary record = new DataSalary();
                try
                {
                    if (StaffId != null && StaffId > 0)
                    {
                        string sql = $@"SELECT ISNULL(SUM((f.Quantity * f.Price * CAST(f.ForSalary AS FLOAT) * CAST(100 - f.VoucherPercent AS FLOAT) / 100) / 100),0)  AS SalaryProduct 
                                       FROM FlowProduct AS f
                                       WHERE f.ComboId IS NULL
                                       AND f.CreatedDate BETWEEN '{String.Format("{0:yyyy/MM/dd}", date)}' AND '{String.Format("{0:yyyy/MM/dd}", date.AddDays(1))}'
                                       AND f.SellerId = {StaffId}
                                       AND f.IsDelete = 0";
                        record = db.Database.SqlQuery<DataSalary>(sql).FirstOrDefault();
                        if (record is null)
                        {
                            record = new DataSalary();
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                return record;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="staffId"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        private static DataSalary KPISalaryProductByStaff(int? staffId, DateTime date)
        {
            DataSalary dataSalary = new DataSalary();
            try
            {
                if (staffId != null && staffId > 0)
                {
                    string sql = $@"SELECT CAST(ISNULL(SUM(f.Price),0) AS FLOAT) AS KPISalary 
                                       FROM FlowProduct AS f
                                       WHERE f.ComboId IS NULL
                                       AND f.CreatedDate BETWEEN '{String.Format("{0:yyyy/MM/dd}", date)}' AND '{String.Format("{0:yyyy/MM/dd}", date.AddDays(1))}'
                                       AND f.SellerId = {staffId}
                                       AND f.IsDelete = 0";
                    using (var db = new Solution_30shineEntities())
                    {
                        dataSalary = db.Database.SqlQuery<DataSalary>(sql).FirstOrDefault();
                    }
                    if (dataSalary is null)
                    {
                        dataSalary = new DataSalary();
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }

            return dataSalary;
        }

        /// <summary>
        /// Thống kê doanh số lương checkin
        /// </summary>
        /// <param name="timeFrom"></param>
        /// <param name="timeTo"></param>
        /// <param name="staffId"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        private static double? UpdateReceptionSalary(DateTime timeFrom, DateTime timeTo, Staff staff)
        {
            using (var db = new Solution_30shineEntities())
            {
                double salary = 0;
                if (staff != null)
                {
                    string sql = @"begin
                                    With WaitTime as
                                    (
                                    SELECT Coalesce(SUM(CASE WHEN (WaitTimeInProcedure <= 15) THEN 1 ELSE 0 END), 0) as PointBefore15,
                                     Coalesce(SUM(CASE WHEN WaitTimeInProcedure > 15  and WaitTimeInProcedure <= 20 THEN 1 ELSE 0 END), 0) as PointBetween15and20,
                                     Coalesce(SUM(CASE WHEN WaitTimeInProcedure > 20 THEN 1 ELSE 0 END), 0) as PointAfter20,
                                     Coalesce(SUM(CASE WHEN (WaitTimeInProcedure is null) THEN 1 ELSE 0 END), 0) as PointNotTime,
                                     CheckinId,
                                     CheckinName
                                    from (
                                     select bill.Id as BillId, bill.BookingId, bill.CreatedDate, bill.CompleteBillTime, bill.ReceptionId as CheckinId, s.Fullname as CheckinName ,bill.InProcedureTime, Convert(datetime, CONVERT(nvarchar(10), b.DatedBook, 110) + ' ' + CONVERT(nvarchar(10), c.HourFrame)) as DateBook,
                                      case 
                                       when (bill.BookingId is not null and (b.IsBookAtSalon is null or b.IsBookAtSalon != 1) and bill.CreatedDate > Convert(datetime, CONVERT(nvarchar(10), b.DatedBook, 110) + ' ' + CONVERT(nvarchar(10), c.HourFrame))) -- check in sau h book
                                       then DATEDIFF(minute, bill.CreatedDate, bill.InProcedureTime) 
                                       when (bill.BookingId is not null and (b.IsBookAtSalon is null or b.IsBookAtSalon != 1) and Convert(datetime, CONVERT(nvarchar(10), b.DatedBook, 110) + ' ' + CONVERT(nvarchar(10), c.HourFrame)) > bill.CreatedDate) -- check in trc h book
                                       then DATEDIFF(minute, Convert(datetime, CONVERT(nvarchar(10), b.DatedBook, 110) + ' ' + CONVERT(nvarchar(10), c.HourFrame)), bill.InProcedureTime)
                                       when (bill.BookingId is null or b.IsBookAtSalon = 1 or Convert(datetime, CONVERT(nvarchar(10), b.DatedBook, 110) + ' ' + CONVERT(nvarchar(10), c.HourFrame)) is null) -- khách k book
                                       then DATEDIFF(MINUTE, bill.CreatedDate, bill.InProcedureTime)
                                       else NULL
                                      end as WaitTimeInProcedure
                                     from BillService as bill
                                     left join booking b on bill.BookingId = b.Id
                                     left join BookHour c on b.HourId = c.Id
                                     inner join Staff s on s.Id = bill.ReceptionId
                                     where bill.IsDelete != 1 and bill.Pending != 1
                                     and (bill.ServiceIds is not null and bill.ServiceIds != '')
                                     and bill.CompleteBillTime between '" + timeFrom + @"' and  '" + timeTo + @"' 
                                     and bill.ReceptionId = '" + staff.Id + @"'
                                    ) as checkin
                                    group by CheckinId, CheckinName
                                    ),
                                    b as 
                                    (
                                    select a.*,
                                    Round((cast((ISNULL(a.PointBefore15,0)) as float) * 3 + cast( (ISNULL(a.PointBetween15and20,0)) as float) * 1  + cast( (ISNULL(a.PointAfter20,0)) as float) * 0 + cast( (ISNULL(a.PointNotTime,0)) as float) * 1),2) as TotalPoint
                                    from WaitTime as a
                                    )
                                    select * from b
                                    end";
                    var SalaryCheckin = db.Database.SqlQuery<SalaryCheckin>(sql).FirstOrDefault();

                    var salaryConfig = db.SalaryConfigs.FirstOrDefault(w => w.LevelId == staff.SkillLevel && w.DepartmentId == staff.Type);
                    if (SalaryCheckin != null && salaryConfig != null && salaryConfig.RattingSalary != null)
                    {
                        salary = SalaryCheckin.TotalPoint * Convert.ToDouble(salaryConfig.RattingSalary);
                    }
                }
                return salary;
            }
        }

        /// <summary>
        /// Hàm kiểm Lấy điểm hài lòng theo tăng ca mới.
        /// </summary>
        /// <param name="bill"></param>
        /// <param name="staff"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static DataSalary GetRatingPointVer2(Staff staff, DateTime date)
        {
            using (var db = new Solution_30shineEntities())
            {
                DataSalary record = new DataSalary();
                try
                {
                    bool isX2 = false;
                    DateTime BillCreateTime = date;
                    DateTime BillCompleteTime = date;
                    DateTime BillCompleteTimeTo = date.AddDays(1);
                    configPartTime configPT = configPartTime(staff.Id, date);
                    //Lấy hệ số tăng ca mới
                    SalaryConfig config = db.SalaryConfigs.FirstOrDefault(w => w.DepartmentId == staff.Type && w.LevelId == staff.SkillLevel);
                    List<ListDataBill> dataBill = new List<ListDataBill>();
                    string sql = "";
                    //Lấy hệ số điểm hài lòng của stylist
                    switch (staff.Type)
                    {
                        case 1:
                            sql = $@"SELECT
                                            b.CompleteBillTime, b.Id AS BillId, f.ServiceId, b.BookingId, b.CreatedDate, b.Mark, ISNULL(f.Quantity,0) AS Quantity,
                                            ISNULL(s.ServiceCoefficient, 0) AS ServiceCoefficient,
	                                        ISNULL(r.ConventionPoint,0) AS ConventionPoint, ISNULL(s.CoefficientOvertimeHour,0) AS CoefficientOvertimeHour, 
                                            ISNULL(s.CoefficientOvertimeDay,0) as CoefficientOvertimeDay, 
                                            ISNULL(g.CoefficientGolden, 1) AS CoefficientGolden, ISNULL(s.ServiceBonus,0) AS ServiceBonus 
                                        FROM dbo.FlowService AS f
                                        INNER JOIN dbo.BillService AS b ON f.BillId = b.Id
                                        INNER JOIN dbo.Rating_ConfigPoint AS r ON r.RealPoint = b.Mark
                                        LEFT JOIN dbo.ServiceSalonConfig AS s ON s.ServiceId = f.ServiceId AND s.DepartmentId = {staff.Type} AND s.SalonId = b.SalonId AND s.IsPublish = 1 AND s.IsDelete = 0
                                        LEFT JOIN dbo.ServiceSalonGoldTime AS g ON g.HoursId = s.Id AND g.IsPublish = 1 AND g.IsDelete = 0
                                        WHERE 
                                            f.IsDelete = 0 AND b.Pending = 0 
                                            AND b.IsDelete = 0 AND r.IsDelete = 0 
                                            AND r.Hint = 3 AND r.[Status] = 1 
                                            AND b.CompleteBillTime >=  '{String.Format("{0:yyyy/MM/dd}", BillCompleteTime)}'  AND b.CompleteBillTime < '{String.Format("{0:yyyy/MM/dd}", BillCompleteTimeTo)}'
                                            AND b.Staff_Hairdresser_Id = {staff.Id}
                                        ORDER BY b.CreatedDate ASC";
                            dataBill = db.Database.SqlQuery<ListDataBill>(sql).ToList();
                            break;
                        case 2:
                            sql = $@"SELECT
                                            b.CompleteBillTime, b.Id AS BillId, f.ServiceId, b.BookingId, b.CreatedDate, b.Mark, ISNULL(f.Quantity,0) AS Quantity,
                                            ISNULL(s.ServiceCoefficient, 0) AS ServiceCoefficient,
	                                        ISNULL(r.ConventionPoint,0) AS ConventionPoint, ISNULL(s.CoefficientOvertimeHour,0) AS CoefficientOvertimeHour, 
                                            ISNULL(s.CoefficientOvertimeDay,0) as CoefficientOvertimeDay, 
                                            ISNULL(g.CoefficientGolden, 1) AS CoefficientGolden, ISNULL(s.ServiceBonus,0) AS ServiceBonus 
                                        FROM dbo.FlowService AS f
                                        INNER JOIN dbo.BillService AS b ON f.BillId = b.Id
                                        INNER JOIN dbo.Rating_ConfigPoint AS r ON r.RealPoint = b.Mark
                                        LEFT JOIN dbo.ServiceSalonConfig AS s ON s.ServiceId = f.ServiceId AND s.DepartmentId = {staff.Type} AND s.SalonId = b.SalonId AND s.IsPublish = 1 AND s.IsDelete = 0
                                        LEFT JOIN dbo.ServiceSalonGoldTime AS g ON g.HoursId = s.Id AND g.IsPublish = 1 AND g.IsDelete = 0
                                        WHERE 
                                            f.IsDelete = 0 AND b.Pending = 0 
                                            AND b.IsDelete = 0 AND r.IsDelete = 0 
                                            AND r.Hint = 3 AND r.[Status] = 1 
                                            AND b.CompleteBillTime >=  '{String.Format("{0:yyyy/MM/dd}", BillCompleteTime)}'  AND b.CompleteBillTime < '{String.Format("{0:yyyy/MM/dd}", BillCompleteTimeTo)}'
                                            AND b.Staff_HairMassage_Id = {staff.Id}
                                        ORDER BY b.CreatedDate ASC";
                            dataBill = db.Database.SqlQuery<ListDataBill>(sql).ToList();
                            break;
                        case 4:
                            sql = $@"SELECT
                                            b.CompleteBillTime, b.Id AS BillId, f.ServiceId, b.BookingId, b.CreatedDate, b.Mark, ISNULL(f.Quantity,0) AS Quantity,
                                            ISNULL(s.ServiceCoefficient, 0) AS ServiceCoefficient,
	                                        ISNULL(r.ConventionPoint,0) AS ConventionPoint, ISNULL(s.CoefficientOvertimeHour,0) AS CoefficientOvertimeHour, 
                                            ISNULL(s.CoefficientOvertimeDay,0) as CoefficientOvertimeDay, 
                                            ISNULL(g.CoefficientGolden, 1) AS CoefficientGolden, ISNULL(s.ServiceBonus,0) AS ServiceBonus 
                                        FROM dbo.FlowService AS f
                                        INNER JOIN dbo.BillService AS b ON f.BillId = b.Id
                                        INNER JOIN dbo.Rating_ConfigPoint AS r ON r.RealPoint = b.Mark
                                        LEFT JOIN dbo.ServiceSalonConfig AS s ON s.ServiceId = f.ServiceId AND s.DepartmentId = {staff.Type} AND s.SalonId = b.SalonId AND s.IsPublish = 1 AND s.IsDelete = 0
                                        LEFT JOIN dbo.ServiceSalonGoldTime AS g ON g.HoursId = s.Id AND g.IsPublish = 1 AND g.IsDelete = 0
                                        WHERE 
                                            f.IsDelete = 0 AND b.Pending = 0 
                                            AND b.IsDelete = 0 AND r.IsDelete = 0 
                                            AND r.Hint = 3 AND r.[Status] = 1 
                                            AND b.CompleteBillTime >=  '{String.Format("{0:yyyy/MM/dd}", BillCompleteTime)}'  AND b.CompleteBillTime < '{String.Format("{0:yyyy/MM/dd}", BillCompleteTimeTo)}'
                                            AND b.SecurityId = {staff.Id}
                                        ORDER BY b.CreatedDate ASC";
                            dataBill = db.Database.SqlQuery<ListDataBill>(sql).ToList();
                            break;
                        case 5:
                            sql = $@"SELECT
                                            b.CompleteBillTime, b.Id AS BillId, f.ServiceId, b.BookingId, b.CreatedDate, b.Mark, ISNULL(f.Quantity,0) AS Quantity,
                                            ISNULL(s.ServiceCoefficient, 0) AS ServiceCoefficient,
	                                        ISNULL(r.ConventionPoint,0) AS ConventionPoint, ISNULL(s.CoefficientOvertimeHour,0) AS CoefficientOvertimeHour, 
                                            ISNULL(s.CoefficientOvertimeDay,0) as CoefficientOvertimeDay, 
                                            ISNULL(g.CoefficientGolden, 1) AS CoefficientGolden, ISNULL(s.ServiceBonus,0) AS ServiceBonus 
                                        FROM dbo.FlowService AS f
                                        INNER JOIN dbo.BillService AS b ON f.BillId = b.Id
                                        INNER JOIN dbo.Rating_ConfigPoint AS r ON r.RealPoint = b.Mark
                                        LEFT JOIN dbo.ServiceSalonConfig AS s ON s.ServiceId = f.ServiceId AND s.DepartmentId = {staff.Type} AND s.SalonId = b.SalonId AND s.IsPublish = 1 AND s.IsDelete = 0
                                        LEFT JOIN dbo.ServiceSalonGoldTime AS g ON g.HoursId = s.Id AND g.IsPublish = 1 AND g.IsDelete = 0
                                        WHERE 
                                            f.IsDelete = 0 AND b.Pending = 0 
                                            AND b.IsDelete = 0 AND r.IsDelete = 0 
                                            AND r.Hint = 3 AND r.[Status] = 1 
                                            AND b.CompleteBillTime >=  '{String.Format("{0:yyyy/MM/dd}", BillCompleteTime)}'  AND b.CompleteBillTime < '{String.Format("{0:yyyy/MM/dd}", BillCompleteTimeTo)}'
                                            AND b.ReceptionId = {staff.Id}
                                        ORDER BY b.CreatedDate ASC";
                            dataBill = db.Database.SqlQuery<ListDataBill>(sql).ToList();
                            break;
                        case 6:
                            sql = $@"SELECT
                                            b.CompleteBillTime, b.Id AS BillId, f.ServiceId, b.BookingId, b.CreatedDate, b.Mark, ISNULL(f.Quantity,0) AS Quantity,
                                            ISNULL(s.ServiceCoefficient, 0) AS ServiceCoefficient,
	                                        ISNULL(r.ConventionPoint,0) AS ConventionPoint, ISNULL(s.CoefficientOvertimeHour,0) AS CoefficientOvertimeHour, 
                                            ISNULL(s.CoefficientOvertimeDay,0) as CoefficientOvertimeDay, 
                                            ISNULL(g.CoefficientGolden, 1) AS CoefficientGolden, ISNULL(s.ServiceBonus,0) AS ServiceBonus 
                                        FROM dbo.FlowService AS f
                                        INNER JOIN dbo.BillService AS b ON f.BillId = b.Id
                                        INNER JOIN dbo.Rating_ConfigPoint AS r ON r.RealPoint = b.Mark
                                        LEFT JOIN dbo.ServiceSalonConfig AS s ON s.ServiceId = f.ServiceId AND s.DepartmentId = {staff.Type} AND s.SalonId = b.SalonId AND s.IsPublish = 1 AND s.IsDelete = 0
                                        LEFT JOIN dbo.ServiceSalonGoldTime AS g ON g.HoursId = s.Id AND g.IsPublish = 1 AND g.IsDelete = 0
                                        WHERE 
                                            f.IsDelete = 0 AND b.Pending = 0 
                                            AND b.IsDelete = 0 AND r.IsDelete = 0 
                                            AND r.Hint = 3 AND r.[Status] = 1 
                                            AND b.CompleteBillTime >=  '{String.Format("{0:yyyy/MM/dd}", BillCompleteTime)}'  AND b.CompleteBillTime < '{String.Format("{0:yyyy/MM/dd}", BillCompleteTimeTo)}'
                                            AND b.CheckoutId = {staff.Id}
                                        ORDER BY b.CreatedDate ASC";
                            dataBill = db.Database.SqlQuery<ListDataBill>(sql).ToList();
                            break;
                    }
                    db.Database.ExecuteSqlCommand($"UPDATE dbo.StaffBillServiceDetail SET IsDelete = 1 WHERE StaffId = {staff.Id} AND DepartmentId = {staff.Type} AND WorkDate BETWEEN '{BillCompleteTime}' AND '{BillCompleteTimeTo}'");
                    var listStaffBill = db.StaffBillServiceDetails.Where(w => w.StaffId == staff.Id && w.DepartmentId == staff.Type && w.WorkDate >= BillCompleteTime && w.WorkDate < BillCompleteTimeTo).ToList();
                    foreach (var item in dataBill)
                    {
                        var staffBill = new StaffBillServiceDetail();
                        //không tăng ca: 0, tăng ca giờ: 1, tăng ca ngày: 2
                        int OvertimeStatus = 0;
                        double SalaryCoeficient = 1,
                               RatingPoint = 0,
                               ServiceSalary = 0;


                        // lay thang 
                        int Month = DateTime.Now.Month;
                        //lay nam
                        int Year = DateTime.Now.Year;
                        //lay ngay trong thang _ nam
                        int days = DateTime.DaysInMonth(Year, Month);
                        DateTime timeFrom = new DateTime(item.CreatedDate.Year, item.CreatedDate.Month, 1);
                        DateTime timeTo = item.CreatedDate;
                        int TimeKeeping = (from f in db.FlowTimeKeepings
                                           where f.WorkDate >= timeFrom && f.WorkDate <= timeTo && f.StaffId == staff.Id && f.IsEnroll == true && f.IsDelete == 0
                                           select f).Count();
                        if (TimeKeeping > (days - 4) && configPT.IsPartTimeSalon == true && date >= timeFrom.AddDays(days - 4))
                        {
                            OvertimeStatus = 1;
                            SalaryCoeficient = item.CoefficientOvertimeDay;
                            item.CoefficientOvertimeHour = 1;
                        }
                        else if (checkHourPartTime(item.BookingId, staff, db, item.CreatedDate) && configPT.IsPartTimeRating == true)
                        {
                            OvertimeStatus = 2;
                            SalaryCoeficient = item.CoefficientOvertimeHour;
                            item.CoefficientOvertimeDay = 1;
                        }
                        else
                        {
                            item.CoefficientOvertimeHour = 1;
                            item.CoefficientOvertimeDay = 1;
                        }
                        RatingPoint = item.CoefficientOvertimeDay * item.CoefficientOvertimeHour * item.ServiceCoefficient *
                                              item.ConventionPoint * item.Quantity * item.CoefficientGolden;
                        record.RatingPoint += RatingPoint;
                        ServiceSalary = (item.CoefficientOvertimeDay * item.CoefficientOvertimeHour * item.ServiceCoefficient *
                                                  item.ConventionPoint * (config.RattingSalary ?? 0) * item.CoefficientGolden + item.ServiceBonus) * item.Quantity;
                        record.SalaryService += ServiceSalary;
                        if (listStaffBill.Any(w => w.BillId == item.BillId && w.ServiceId == item.ServiceId))
                        {
                            staffBill = listStaffBill.FirstOrDefault(w => w.BillId == item.BillId && w.ServiceId == item.ServiceId);
                            staffBill.StaffLevelId = staff.SkillLevel ?? 0;
                            staffBill.OvertimeStatusValue = OvertimeStatus;
                            staffBill.RatingMark = item.Mark;
                            staffBill.RatingMoney = config.RattingSalary ?? 0;
                            staffBill.SalaryCoeficient = SalaryCoeficient;
                            staffBill.ServiceCoeficient = item.ServiceCoefficient;
                            staffBill.ConventionPoint = item.ConventionPoint;
                            staffBill.ServiceIncomeBonus = ServiceSalary;
                            staffBill.ServiceBonus = item.ServiceBonus;
                            staffBill.Quantity = item.Quantity;
                            staffBill.WorkDate = item.CompleteBillTime;
                            staffBill.CreateTime = DateTime.Now;
                            staffBill.IsDelete = false;
                            db.StaffBillServiceDetails.AddOrUpdate(staffBill);
                        }
                        else
                        {
                            staffBill = new StaffBillServiceDetail()
                            {
                                BillId = item.BillId,
                                ServiceId = item.ServiceId,
                                DepartmentId = staff.Type ?? 0,
                                StaffId = staff.Id,
                                StaffLevelId = staff.SkillLevel ?? 0,
                                OvertimeStatusValue = OvertimeStatus,
                                RatingMark = item.Mark,
                                RatingMoney = config.RattingSalary ?? 0,
                                SalaryCoeficient = SalaryCoeficient,
                                ServiceCoeficient = item.ServiceCoefficient,
                                ConventionPoint = item.ConventionPoint,
                                ServiceIncomeBonus = ServiceSalary,
                                ServiceBonus = item.ServiceBonus,
                                Quantity = item.Quantity,
                                WorkDate = item.CompleteBillTime,
                                CreateTime = DateTime.Now,
                                IsDelete = false
                            };
                            db.StaffBillServiceDetails.Add(staffBill);
                        }
                    }
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                return record;
            }
        }

        /// <summary>
        /// KPISalaryService
        /// </summary>
        /// <param name="staff"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        private static DataSalary KPISalaryServiceByStaff(Staff staff, DateTime date)
        {
            DataSalary data = new DataSalary();
            try
            {
                DateTime BillCreateTime = date;
                DateTime BillCompleteTime = date;
                DateTime BillCompleteTimeTo = date.AddDays(1);
                List<DataKPISalaryService> dataKPISalaryServices = new List<DataKPISalaryService>();
                string sql = "";
                using (var db = new Solution_30shineEntities())
                {
                    // tinh KPI cho 2 department Stylist and Skinner
                    switch (staff.Type)
                    {
                        case 1:
                            sql = $@"SELECT
                                            f.ServiceId, ISNULL(f.Quantity,0) AS Quantity, CAST(f.Price AS FLOAT) AS Price, CAST(ISNULL(s.KPIPercent, 0) AS FLOAT) AS KPIPercent
                                        FROM dbo.FlowService AS f
                                        INNER JOIN dbo.BillService AS b ON f.BillId = b.Id
                                        LEFT JOIN dbo.ServiceSalonConfig AS s ON s.ServiceId = f.ServiceId AND s.DepartmentId = {staff.Type} AND s.SalonId = b.SalonId AND s.IsPublish = 1 AND s.IsDelete = 0
                                        WHERE 
                                            f.IsDelete = 0 AND b.Pending = 0 
                                            AND b.IsDelete = 0 
                                            AND b.CompleteBillTime >=  '{String.Format("{0:yyyy/MM/dd}", BillCompleteTime)}'  AND b.CompleteBillTime < '{String.Format("{0:yyyy/MM/dd}", BillCompleteTimeTo)}'
                                            AND b.Staff_Hairdresser_Id = {staff.Id}
                                        ORDER BY b.CreatedDate ASC";
                            dataKPISalaryServices = db.Database.SqlQuery<DataKPISalaryService>(sql).ToList();
                            break;
                        case 2:
                            sql = $@"SELECT
                                           f.ServiceId, ISNULL(f.Quantity,0) AS Quantity, CAST(f.Price AS FLOAT) AS Price, CAST(ISNULL(s.KPIPercent, 0) AS FLOAT) AS KPIPercent
                                        FROM dbo.FlowService AS f
                                        INNER JOIN dbo.BillService AS b ON f.BillId = b.Id
                                        LEFT JOIN dbo.ServiceSalonConfig AS s ON s.ServiceId = f.ServiceId AND s.DepartmentId = {staff.Type} AND s.SalonId = b.SalonId AND s.IsPublish = 1 AND s.IsDelete = 0
                                        WHERE 
                                            f.IsDelete = 0 AND b.Pending = 0 
                                            AND b.IsDelete = 0 
                                            AND b.CompleteBillTime >=  '{String.Format("{0:yyyy/MM/dd}", BillCompleteTime)}'  AND b.CompleteBillTime < '{String.Format("{0:yyyy/MM/dd}", BillCompleteTimeTo)}'
                                            AND b.Staff_HairMassage_Id = {staff.Id}
                                        ORDER BY b.CreatedDate ASC";
                            dataKPISalaryServices = db.Database.SqlQuery<DataKPISalaryService>(sql).ToList();
                            break;
                    }
                    if (dataKPISalaryServices.Count > 0)
                    {
                        foreach (var v in dataKPISalaryServices)
                        {
                            double kpiSalary = 0;
                            kpiSalary = (v.Quantity * v.Price) * v.KPIPercent / 100;
                            data.KPISalary += kpiSalary;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }

            return data;
        }

        /// <summary>
        /// Kiểm tra stylist,skinner có trong giờ tăng ca hay không.
        /// </summary>
        /// <param name="staff"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        private static bool checkHourPartTime(int BookingId, Staff staff, Solution_30shineEntities db, DateTime WorkDate)
        {
            bool check = false;
            //TimeSpan WorkHour = new TimeSpan(IntHour, IntMinute, 0);
            int StaffId = staff.Id;
            try
            {

                var WorkTime = db.FlowTimeKeepings.Where(r => r.IsDelete == 0 && r.IsEnroll == true && r.WorkDate == WorkDate.Date && r.StaffId == StaffId).FirstOrDefault();

                if (WorkTime != null)
                {
                    //Lay khung gio lam viec
                    int WorkTimeId = WorkTime.WorkTimeId.Value;
                    //Lay so gio tang ca
                    int WorkHour = WorkTime.WorkHour.Value;
                    //Lay chuoi gio tang ca
                    string[] strHourIds = WorkTime.HourIds.Split(',');
                    //var test = strHourIds.Length;
                    //int bookhourId = Convert.ToInt32(strHourIds[strHourIds.Length-1]);
                    int[] intHourIds = new int[strHourIds.Length];
                    for (int i = 0; i < strHourIds.Length; i++)
                    {
                        intHourIds[i] = strHourIds[i] != "" ? Convert.ToInt32(strHourIds[i]) : 0;
                    }
                    var BookHourFrame = (
                                               from b in db.Bookings
                                               join a in db.BookHours on b.HourId equals a.Id
                                               where a.IsDelete == 0 && a.Publish == true && b.Id == BookingId
                                               select new
                                               {
                                                   HourFrame = a.HourFrame,
                                                   HourId = a.Id
                                               }).FirstOrDefault();
                    if (BookHourFrame != null)
                    {
                        var WorkTimeHour = db.WorkTimes.Where(r => r.Id == WorkTimeId).FirstOrDefault();
                        if (WorkTimeHour != null)
                        {
                            if (BookHourFrame.HourFrame < WorkTimeHour.StrartTime || BookHourFrame.HourFrame > WorkTimeHour.EnadTime)
                            {
                                check = Array.IndexOf(intHourIds, BookHourFrame.HourId) != -1 ? true : false;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return check;
        }

        private static DataSalary RatingPointReception(Staff staff, DateTime date)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    DataSalary rating = new DataSalary();
                    string sql = "";
                    if (staff != null)
                    {
                        sql = @"select Cast(Coalesce(SUM(Coalesce(point, 0)),0) as float) as RatingPoint
                                from
                                (
	                                select 
		                                Cast(rcp.ConventionPoint as float) as point
	                                from  BillService as bill
	                                inner join Rating_ConfigPoint as rcp
	                                on rcp.RealPoint = bill.Mark and rcp.Hint = 3 and rcp.IsDelete != 1 and rcp.[Status] = 1
	                                where bill.IsDelete != 1 and bill.Pending != 1 
                                        and bill.CompleteBillTime between '" + String.Format("{0:yyyy/MM/dd}", date) + "' and '" + String.Format("{0:yyyy/MM/dd}", date.AddDays(1)) + @"'
                                        and bill.ServiceIds != ''
		                                and bill.ReceptionId = " + staff.Id + @"
                                ) as a";
                        rating = db.Database.SqlQuery<DataSalary>(sql).FirstOrDefault();
                    }
                    return rating;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Lấy thông tin cấu hình Tăng ca trong bảng ConfigPartTime
        /// </summary>
        /// <param name="staff_id"></param>
        /// <param name="sDate"></param>
        /// <returns></returns>
        public static configPartTime configPartTime(int staff_id, DateTime sDate)
        {
            configPartTime dataConfig = new configPartTime();
            //khởi tạo giá trị mặc định
            dataConfig.IsPartTimeHour = false;
            dataConfig.IsPartTimeRating = false;
            dataConfig.IsPartTimeSalon = false;
            dataConfig.CoefficientStylist = 1;
            dataConfig.coefficinetSkinner = 1;
            try
            {
                using (Solution_30shineEntities db = new Solution_30shineEntities())
                {
                    var salonPartTime = db.FlowTimeKeepings.Where(r => r.StaffId == staff_id && r.WorkDate == sDate && r.IsEnroll == true).FirstOrDefault();
                    if (salonPartTime != null)
                    {
                        var dataSalonConfig = db.Tbl_Salon.Where(r => r.IsDelete == 0 && r.Id == salonPartTime.SalonId).FirstOrDefault();
                        if (dataSalonConfig != null)
                        {
                            dataConfig.IsPartTimeSalon = dataSalonConfig.IsPartTimeSalon;
                            dataConfig.IsPartTimeHour = dataSalonConfig.IsPartTimeHours;
                            dataConfig.IsPartTimeRating = dataSalonConfig.IsPartTimeRating;
                            var dataConfigPartTime = db.ConfigPartTimes.Where(r => r.IsDelete == false && r.SalonId == dataSalonConfig.Id).FirstOrDefault();
                            if (dataConfigPartTime != null)
                            {
                                dataConfig.CoefficientSalon = dataConfigPartTime.CoefficientSalon;
                                dataConfig.CoefficientStylist = dataConfigPartTime.CoefficientStylist;
                                dataConfig.coefficinetSkinner = dataConfigPartTime.CoefficinetSkinner;
                            }

                        }
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dataConfig;
        }
    }
    public partial class ListDataBill
    {
        public DateTime CompleteBillTime { get; set; }
        public int BillId { get; set; }
        public int ServiceId { get; set; }
        public int BookingId { get; set; }
        public DateTime CreatedDate { get; set; }
        public int Mark { get; set; }
        public int Quantity { get; set; }
        public double ConventionPoint { get; set; }
        public double ServiceCoefficient { get; set; }
        public double CoefficientOvertimeHour { get; set; }
        public double CoefficientOvertimeDay { get; set; }
        public double CoefficientGolden { get; set; }
        public double ServiceBonus { get; set; }
    }

    public partial class DataSalary
    {
        public double RatingPoint { get; set; }
        public double SalaryService { get; set; }
        public double SalaryProduct { get; set; }
        public double KPISalary { get; set; }
    }

    public partial class StatisService
    {
        public int? SalonId { get; set; }
        public int? StaffId { get; set; }
        public int? ServiceIds { get; set; }
        public int Total { get; set; }
        public double TotalMoney { get; set; }
    }

    public partial class DataKPISalaryService
    {
        public int ServiceId { get; set; }
        public int Quantity { get; set; }
        public double Price { get; set; }
        public double KPIPercent { get; set; }
    }
}