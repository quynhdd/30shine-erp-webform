﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using _30shine.MODEL.ENTITY.EDMX;

namespace Library
{
    public class Class
    {
        public class cls_tree_node
        {
            public int Id { get; set; }
            public int Pid { get; set; }
            public string Name { get; set; }
            public List<cls_tree_node> Childs { get; set; }
        }
        public class cls_treeNode
        {
            public int Id { get; set; }
            public int Pid { get; set; }
            public string Name { get; set; }
            public List<int> ChildIds { get; set; }
        }

        /// <summary>
        /// Xuất mỹ phẩm
        /// </summary>
        public class cls_goods : ExportGood
        {
            /// <summary>
            /// Bảng danh sách các item (vật tư)
            /// </summary>
            public string goodsDetail { get; set; }
            /// <summary>
            /// Tên salon
            /// </summary>
            public string SalonName { get; set; }
            /// <summary>
            /// Tên người xuất hàng
            /// </summary>
            public string ExportName { get; set; }
            /// <summary>
            /// Tên người nhận hàng
            /// </summary>
            public string RecipientName { get; set; }
            /// <summary>
            /// Tổng giá nhập
            /// </summary>
            public double totalCost { get; set; }
        }

        public class cls_exportGoodsQuantity : Product
        {
            /// <summary>
            /// Tổng tiền giá nhập
            /// </summary>
            public int totalCost { get; set; }
            /// <summary>
            /// Tổng tiền giá bán
            /// </summary>
            public int totalPrice { get; set; }
        }

        /// <summary>
        /// Struct item tồn kho
        /// </summary>
        public class cls_inventoryItem
        {
            /// <summary>
            /// Id sản phẩm
            /// </summary>
            public int Id { get; set; }
            /// <summary>
            /// Tên sản phẩm
            /// </summary>
            public string Name { get; set; }
            /// <summary>
            /// Lượng nhập trong ngày
            /// </summary>
            public int importQuantity { get; set; }
            /// <summary>
            /// Lượng bán trong ngày
            /// </summary>
            public int sellQuantity { get; set; }
            /// <summary>
            /// Hàng trả về kho
            /// </summary>
            public int sendBackQuantity { get; set; }
            /// <summary>
            /// Lượng xuất ra ngoài ( Hàng tặng khách, hàng dùng cho quay video... )
            /// </summary>
            public int exportOutQuantity { get; set; }
            /// <summary>
            /// Lượng tồn trong ngày
            /// </summary>
            public int restQuantity { get; set; }
            /// <summary>
            /// Lượng tồn cuối ngày hôm trước
            /// </summary>
            public int restQuantityLastDay { get; set; }
        }

        public class cls_inventoryReport
        {
            public int salonId { get; set; }
            public string salonName { get; set; }
            public List<cls_inventoryItem> data { get; set; }
        }

        /// <summary>
        /// Struct sản phẩm nhập bán
        /// </summary>
        public class ProductBasic
        {
            public int Id { get; set; }
            /// <summary>
            /// Mã sản phẩm
            /// </summary>
            public string Code { get; set; }
            /// <summary>
            /// Tên sản phẩm
            /// </summary>
            public string Name { get; set; }
            /// <summary>
            /// Giá gốc
            /// </summary>
            public int Cost { get; set; }
            /// <summary>
            /// Giá bán
            /// </summary>
            public int Price { get; set; }
            /// <summary>
            /// Số lượng
            /// </summary>
            public int Quantity { get; set; }
            /// <summary>
            /// % khuyến mãi
            /// </summary>
            public int VoucherPercent { get; set; }
            /// <summary>
            /// Tiền khuyến mãi
            /// </summary>
            public int Promotion { get; set; }
            public string MapIdProduct { get; set; }
            public bool CheckCombo { get; set; }
        }
        /// <summary>
        /// thiết bị
        /// author: dungnm
        /// </summary>
        public class Cls_Device
        {
            public int Id { get; set; }

            public string SalonName { get; set; }

            public string StaffName { get; set; }
            public string DeviceName { get; set; }

            public string ImeiOrMacIP { get; set; }
        }
        /// <summary>
        /// checkin
        /// author: dungnm
        /// </summary>
        public class Cls_checkin
        {
            public DateTime? WorkDate { get; set; }

            public string FullName { get; set; }

            public int StaffId { get; set; }

            public string SalonName { get; set; }

            public TimeSpan? StartTime { get; set; }

            public DateTime? CheckinFirstTime { get; set; }

            public int DelayTime { get; set; }
            public string status { get; set; }
        }
        public class cls_id_quantity
        {
            public int Id { get; set; }
            public int Quantity { get; set; }
        }

        public class cls_inventory_data : Inventory_Data
        {
            public string salonName { get; set; }
        }

        /// <summary>
        /// Notification
        /// </summary>
        public class cls_notification
        {
            /// <summary>
            /// Tiêu đề
            /// </summary>
            public string title { get; set; }
            /// <summary>
            /// Body
            /// </summary>
            public string text { get; set; }
            /// <summary>
            /// Body assign cho data
            /// </summary>
            public string text_data { get; set; }
            public string icon { get; set; }
            public string tag { get; set; }
            public string sound { get; set; }
            /// <summary>
            /// Ảnh assign cho data
            /// </summary>
            public string image { get; set; }
            /// <summary>
            /// Giá trị xác định đối tượng được gửi notification
            /// </summary>
            public int sendTo { get; set; }
        }

        /// <summary>
        /// Message
        /// </summary>
        public class cls_message
        {
            public string status { get; set; }
            public string message { get; set; }
            public bool success { get; set; }
            public object data { get; set; }
            public string messageNoti { get; set; }

            public string campaignInfo { get; set; }
            public string dtShow { get; set; }
        }

        /// <summary>
        /// Rating
        /// </summary>
        public class cls_rating
        {
            public int staffId { get; set; }
            public int salonId { get; set; }
            
            public string staffName { get; set; }
            public double point { get; set; }
            public int bill_normal { get; set; }
            public int bill_normal_great { get; set; }
            public int bill_normal_good { get; set; }
            public int bill_normal_bad { get; set; }
            public int bill_normal_norating { get; set; }
            public int bill_special { get; set; }
            public int bill_special_great { get; set; }
            public int bill_special_good { get; set; }
            public int bill_special_bad { get; set; }
            public int bill_special_norating { get; set; }
            public int mistake_point { get; set; }
        }

        /// <summary>
        /// Class media
        /// </summary>
        public class cls_media
        {
            public string url { get; set; }
            public string title { get; set; }
            public string description { get; set; }
        }

        /// <summary>
        /// UserId, DeviceRegistrationToken theo thiết bị
        /// </summary>
        public class cls_device_infor
        {
            public int UserId { get; set; }
            public string DeviceRegistrationToken { get; set; }
        }

        /// <summary>
        /// Dữ liệu tồn kho
        /// </summary>
        public class cls_inventory_report
        {
            /// <summary>
            /// ID sản phẩm
            /// </summary>
            public int ProductId { get; set; }
            /// <summary>
            /// Tên sản phẩm
            /// </summary>
            public string ProductName { get; set; }
            /// <summary>
            /// Sản phẩm nhập từ kho (Tính đến cuối ngày trước đầu kỳ)
            /// </summary>
            public int ImportBefore { get; set; }
            /// <summary>
            /// Sản phẩm xuất trả kho (Tính đến cuối ngày trước đầu kỳ)
            /// </summary>
            public int ExportBefore { get; set; }
            /// <summary>
            /// Sản phẩm bán ra (Tính đến cuối ngày trước đầu kỳ)
            /// </summary>
            public int SellBefore { get; set; }
            /// <summary>
            /// Sản phẩm nhập từ kho trong kỳ
            /// </summary>
            public int ImportInTime { get; set; }
            /// <summary>
            /// Sản phẩm xuất trả kho trong kỳ
            /// </summary>
            public int ExportInTime { get; set; }
            /// <summary>
            /// Sản phẩm bán ra trong kỳ
            /// </summary>
            public int SellInTime { get; set; }
            /// <summary>
            /// Tồn kho đầu kỳ
            /// </summary>
            public int InvenBefore { get; set; }
            /// <summary>
            /// Tồn kho hiện tại
            /// </summary>
            public int InvenNow { get; set; }
        }

        public class cls_pending_bill
        {
            public int salonId { get; set; }
            public string salonName { get; set; }
            public List<cls_pending_bill_item> data { get; set; }
        }

        public class cls_pending_bill_item
        {
            public int Id { get; set; }
            public int billOrder { get; set; }
            public string TeamColor { get; set; }
            public DateTime? CreatedDate { get; set; }
            public DateTime? TimeWaitAtSalon { get; set; }
            public string CustomerName { get; set; }
            public string CustomerPhone { get; set; }
            public string PDFBillCode { get; set; }
            public string BillCode { get; set; }     
            public bool? IsBooking { get; set; }       
            public DateTime? InProcedureTime { get; set; }
            public DateTime? InProcedureTimeModifed { get; set; }
            public string SkinnerName { get; set; }
        }

        /// <summary>
        /// author: dungnm
        /// class using call method to api financial
        /// </summary>
        public partial class clsBill
        {
            public int Id { get; set; }
            public string CustomerCode { get; set; }
            public string ServiceIds { get; set; }
            public int? StaffHairdresserId { get; set; }
            public int? TotalMoney { get; set; }
            public byte? Vote { get; set; }
            public string CreatedDate { get; set; }
            public int? StaffHairMassageId { get; set; }
            public int? StaffMakeBill { get; set; }
            public string Note { get; set; }
            public int? Mark { get; set; }
            public byte? IsDelete { get; set; }
            public string Images { get; set; }
            public string ImagesRating { get; set; }
            public string ModifiedDate { get; set; }
            public int? SalonId { get; set; }
            public int? SellerId { get; set; }
            public bool? CustomerNoInfo { get; set; }
            public byte? Pending { get; set; }
            public string BillCode { get; set; }
            public string PdfbillCode { get; set; }
            public bool? ServiceError { get; set; }
            public bool? IsCusFamiliar { get; set; }
            public int? TimesUsedService { get; set; }
            public int? TeamId { get; set; }
            public bool? IsBooking { get; set; }
            public bool? IsBooked { get; set; }
            public string AppointmentTime { get; set; }
            public string ShampooTime0 { get; set; }
            public string ShampooTime1 { get; set; }
            public string HairCutTime0 { get; set; }
            public string HairCutTime1 { get; set; }
            public string FinishTime { get; set; }
            public byte? Status { get; set; }
            public int? ReceptionId { get; set; }
            public string CompleteBillTime { get; set; }
            public string CustomerCode1 { get; set; }
            public int? CustomerId { get; set; }
            public bool? VIPCard { get; set; }
            public string VIPCardGive { get; set; }
            public string VIPCardUse { get; set; }
            public bool? IsOnline { get; set; }
            public int? PayMethodId { get; set; }
            public bool? Paid { get; set; }
            public int? BillStatusId { get; set; }
            public int? FeeCOD { get; set; }
            public int? FeeExtra { get; set; }
            public bool? IsPayByCard { get; set; }
            public bool? IsX2 { get; set; }
            public string InProcedureTime { get; set; }
            public string InProcedureTimeModifed { get; set; }
            public int? BookingId { get; set; }
            public string ImageStatusId { get; set; }
            public string ImageChecked1 { get; set; }
            public string ImageChecked2 { get; set; }
            public string ImageChecked3 { get; set; }
            public string ImageChecked4 { get; set; }
            public string ErrorNote { get; set; }
            public string HCItem { get; set; }
            public string NoteByStylist { get; set; }
            public int? EstimateTimeCut { get; set; }
            public string UploadImageTime { get; set; }
            public string EstimateTimeCutTime { get; set; }
            public bool? IsSulphite { get; set; }
            public int? CheckoutId { get; set; }
        }


        /// <summary>
        /// Cấu trúc item thu chi | Quản lý thu chi
        /// </summary>
        public class cls_fund_receipt_item
        {
            public int Id { get; set; }
            public string Title { get; set; }
            public int Money { get; set; }
        }

        public class cls_fund_import_item : Fund_Import
        {
            public string accountTypeName { get; set; }
            public string sourceName { get; set; }
        }

        /// <summary>
        /// Báo cáo thống kê tài khoản tiền vốn
        /// </summary>
        public class cls_fund_report
        {
            /// <summary>
            /// Tài khoản Quỹ tiền mặt
            /// </summary>
            public long cashAccount { get; set; }
            /// <summary>
            /// Tài khoản Ngân hàng nội bộ
            /// </summary>
            public long bankAccount { get; set; }
            /// <summary>
            /// Tài khoản Ngân hàng công ty
            /// </summary>
            public long bankCompanyAccount { get; set; } 
            /// <summary>
            /// Tài khoản Ngân hàng tiết kiệm
            /// </summary>
            public long bankSavingAccount { get; set; }
            /// <summary>
            /// Tổng tài khoản
            /// </summary>
            public long totalAccount { get; set; }
            /// <summary>
            /// Tổng thu
            /// </summary>
            public long totalReceipt { get; set; }
            /// <summary>
            /// Tổng chi
            /// </summary>
            public long totalPayment { get; set; }
        }

        public class cls_S4M_BillTraning : Stylist4Men_BillCutFree
        {
            public string CustomerName { get; set; }
            public string CustomerPhone { get; set; }
            public string Classname { get; set; }
            public string StudentName { get; set; }

        }

        public class AjaxResponse
        {
            public bool success { get; set; }
            public string msg { get; set; }
            public List<dynamic> data { get; set; }
            public AjaxResponse()
            {
                data = new List<dynamic>();
            }
            public void Reset()
            {
                success = false;
                msg = "";
                data = new List<dynamic>();
            }
        }
        /// <summary>
        /// class call api
        /// </summary>
        public class CallApiCheckout
        {
            public int BillId { get; set; }
            public bool isStylistOvertime { get; set; }
            public bool isSkinnerOvertime { get; set; }
            public bool isVoucherLongTime { get; set; }
        }
    }
}