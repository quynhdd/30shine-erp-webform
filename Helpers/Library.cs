﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using _30shine.MODEL.ENTITY.EDMX;
using System.Web.Script.Serialization;
using System.Data.Entity.Migrations;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using System.IO;
using _30shine.Helpers;
using System.Web.UI;
using System.Security.Cryptography;
using System.Text;
using Google.Authenticator;
using Amazon.CognitoIdentityProvider;
using System.Data.Objects;

namespace Library
{
    /// <summary>
    /// Common class
    /// </summary>
    public class Function
    {
        public static JavaScriptSerializer serialize = new JavaScriptSerializer();
        private static Random random = new Random();

        // <summary>
        /// gen QR Code
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="uniqueKey"></param>
        /// <returns></returns>
        public static string GenerateQRCode(string username, string uniqueKey)
        {
            MD5 md5 = MD5.Create();
            //2fa setup
            TwoFactorAuthenticator tfa = new TwoFactorAuthenticator();
            string UserUniqueKey = uniqueKey;
            var setupInfo = tfa.GenerateSetupCode("30shine code", $"{DateTime.Now.ToString("yyyyMMddHHmmss")}-{username}", UserUniqueKey, 300, 300);
            string urlImg = setupInfo.QrCodeSetupImageUrl;
            return urlImg;
        }

        /// <summary>
        /// Custom exception cognito
        /// </summary>
        /// <param name="e">The exception of function</param>
        /// <param name="Message">The message of function</param>
        /// <returns></returns>
        public static string CustomExceptionAWS(Exception e, string type = "default")
        {
            var awsex = e as Amazon.Runtime.AmazonServiceException;
            var agex = e as System.AggregateException;
            string message = "", code = "";
            if (awsex != null)
            {
                code = awsex.ErrorCode;
            }
            else if(agex != null)
            {
                code = agex.InnerException.Message;
            }
            if (type.Equals("change_password") && code.Equals("Incorrect username or password."))
            {
                code = Libraries.AppConstants.PASSWORD_INCORRECT;
            }
            switch (code)
            {
                case Libraries.AppConstants.INTERNAL_ERROR_EXCEPTION:
                    message = "Amazon Cognito xảy ra lỗi, vui lòng liên hệ nhóm phát triển!";
                    break;
                case Libraries.AppConstants.INVALID_PARAMETER_EXCEPTION:
                    message = "Dữ liệu đầu vào không hợp lệ. Vui lòng kiểm tra lại!";
                    break;
                case Libraries.AppConstants.NOT_AUTHORIEZED_EXCEPTION:
                    message = "Tài khoản hoặc mật khẩu không đúng!";
                    break;
                case Libraries.AppConstants.PASSWORD_RESET_REQUIRED_EXCEPTION:
                    message = "Yêu cầu thay đổi mật khẩu!";
                    break;
                case Libraries.AppConstants.RESOURCE_NOT_FOUND_EXCEPTION:
                    message = "Amazon Cognito không thể tìm thấy tài nguyên được yêu cầu!";
                    break;
                case Libraries.AppConstants.TOO_MANY_REQUESTS_EXCEPTION:
                    message = "Thao tác này đã thực hiện quá nhiều yêu cầu. Vui lòng tạm dừng thao tác!";
                    break;
                case Libraries.AppConstants.USER_NOT_CONFIRMED_EXCEPTION:
                    message = "Tài khoản không được xác nhận, vui lòng liên hệ nhóm phát triển để được xác nhận tài khoản!";
                    break;
                case Libraries.AppConstants.USER_NOT_FOUND_EXCEPTION:
                    message = "Tài khoản không tồn tại";
                    break;
                case Libraries.AppConstants.PASSWORD_ATTEMPTS_EXCEEDED:
                    message = "Đăng nhập sai quá số lần quy định";
                    break;
                case Libraries.AppConstants.PASSWORD_INCORRECT:
                    message = "Kiểm tra lại mật khẩu cũ hoặc vui lòng đăng nhập lại để thao tác tiếp, xin lỗi vì sự bất tiện này. Trân trọng!";
                    break;
                case Libraries.AppConstants.USER_DISABLED:
                    message = "Tài khoản bị vô hiệu hoá, vui lòng liên hệ bộ phận vận hành để xác nhận!";
                    break;
                default:
                    message = e.Message;
                    break;
            }
            return message;
        }

        /// <summary>
        /// Generate a MD5 hash of string
        /// </summary>
        /// <param name="md5Hash"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string GenMD5(MD5 md5Hash, string input)
        {
            var scrPush = "uaefsfkoiu&@(^%=";
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input + scrPush));
            StringBuilder sBuilder = new StringBuilder();

            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            return sBuilder.ToString();
        }

        /// <summary>
        /// Regex password 
        /// (?=.*\d) => there is at least one digit (số)
        /// (?=.*[a-z]) => there is at least one lowercase character (thường)
        /// (?=.*[A-Z]) => there is at least one uppercase character (hoa)
        /// (?=.*[#$@!%&*?]) => there is at least one special character (ký tự đặc biệt)
        /// .{8,} => length is 8 or more (độ dài 8)
        /// </summary>
        /// <returns></returns>
        public static Regex GetRegexPassword()
        {
            Regex regex = new Regex(@"(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[#$@!%&*?]).{8,}");
            return regex;
        }

        /// <summary>
        /// Regex password 
        /// (?=.*\d) => there is at least one digit (số)
        /// </summary>
        /// <returns></returns>
        public static Regex GetRegexOnlyNumber()
        {
            Regex regex = new Regex(@"^[0-9]{6,6}$");
            return regex;
        }

        /// <summary>
        /// random string
        /// </summary>
        /// <param name="length">độ dài chuỗi</param>
        /// <returns></returns>
        public static string RandomString(int length)
        {
            const string chars = "abcdefghijklmnopqrstuvwxyz0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        /// <summary>
        /// Trả thông báo về client 
        /// </summary>
        /// <param name="_OBJ"></param>
        /// <param name="msg"></param>
        /// <param name="status"></param>        
        public static void TriggerJsMsgSystem(Page page, string msg, int status, int timeout = 3000)
        {
            ScriptManager.RegisterStartupScript(page, page.GetType(), "Message System", "ShowMessage('Thông báo','" + msg + "'," + status + ", " + timeout + ");", true);
        }

        ///===================================
        /// Các hàm xử lý tồn kho
        ///===================================
        /// <summary>
        /// Decode danh sách sản phẩm từ chuỗi json
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public static List<ProductBasic> genListFromJson(string json)
        {
            var serialize = new JavaScriptSerializer();
            var list = serialize.Deserialize<List<ProductBasic>>(json).ToList();
            var listReturn = new List<ProductBasic>();
            var index = -1;
            if (list.Count > 0)
            {
                foreach (var v in list)
                {
                    var goods = v;
                    index = listReturn.FindIndex(w => w.Id == v.Id);
                    if (index == -1)
                    {
                        listReturn.Add(goods);
                    }
                    else
                    {
                        var item = listReturn[index];
                        item.Quantity += v.Quantity;
                        listReturn[index] = item;
                    }
                }
            }
            return listReturn;
        }
        public static List<StaffBasic> genListFromStaff(string json)
        {
            var serialize = new JavaScriptSerializer();
            var list = serialize.Deserialize<List<StaffBasic>>(json).ToList();
            var listReturn = new List<StaffBasic>();
            var index = -1;
            if (list.Count > 0)
            {
                foreach (var v in list)
                {
                    var goods = v;
                    index = listReturn.FindIndex(w => w.Id == v.Id);
                    if (index == -1)
                    {
                        listReturn.Add(goods);
                    }
                    else
                    {
                        var item = listReturn[index];

                        listReturn[index] = item;
                    }
                }
            }
            return listReturn;
        }

        public static List<ProductBasic_QLKho> genListFromJson_QLKho(string json)
        {
            var serialize = new JavaScriptSerializer();
            var list = serialize.Deserialize<List<ProductBasic_QLKho>>(json).ToList();
            var listReturn = new List<ProductBasic_QLKho>();
            var index = -1;
            if (list.Count > 0)
            {
                foreach (var v in list)
                {
                    var goods = v;
                    index = listReturn.FindIndex(w => w.Id == v.Id);
                    if (index == -1)
                    {
                        listReturn.Add(goods);
                    }
                    else
                    {
                        var item = listReturn[index];
                        item.QuantityOrder += v.QuantityOrder;
                        item.QuantityReceived += v.QuantityReceived;
                        listReturn[index] = item;
                    }
                }
            }
            return listReturn;
        }

        /// <summary>
        /// Khởi tạo dữ liệu tồn kho cho các salon theo ngày
        /// </summary>
        /// <param name="db"></param>
        /// <param name="date"></param>
        public static void initInventoryByDay(Solution_30shineEntities db, DateTime date)
        {
            //1. Lấy danh sách salon
            //2. Kiểm tra đã init dữ liệu chưa
            //3. Nếu chưa thì tiến hành init dữ liệu
            var salons = db.Tbl_Salon.Where(w => w.IsDelete != 1 && w.Publish == true).OrderBy(o => o.Id).ToList();
            if (salons.Count > 0)
            {
                foreach (var v in salons)
                {
                    initInventory(db, date, v.Id);
                }
            }
        }

        /// <summary>
        /// Khởi tạo dữ liệu tồn kho theo salon
        /// </summary>
        /// <param name="db"></param>
        /// <param name="date"></param>
        /// <param name="salonId"></param>
        public static Inventory_Data initInventory(Solution_30shineEntities db, DateTime date, int salonId)
        {
            var obj = isInitInventory(db, date.Date, salonId);
            try
            {
                obj = isInitInventory(db, date.Date, salonId);
                if (obj == null)
                {
                    obj = new Inventory_Data();
                    obj.SalonId = salonId;
                    obj.iDate = date;
                    obj.CreatedTime = DateTime.Now;
                    obj.IsDelete = false;

                    var products = db.Products.Where(w => w.IsDelete != 1 && w.Publish == 1).OrderBy(o => o.Id).ToList();
                    var restLastDay = new List<Library.Class.cls_inventoryItem>();
                    var restLastDayJson = getRestQuantityLastDay(db, date.Date, salonId);
                    if (restLastDayJson != "")
                    {
                        restLastDay = serialize.Deserialize<List<Library.Class.cls_inventoryItem>>(restLastDayJson).ToList();
                    }
                    obj.InvenData = genInventoryListItem(products, restLastDay);

                    db.Inventory_Data.Add(obj);
                    db.SaveChanges();
                }
                return obj;
            }
            catch (Exception e)
            {
                //Push thông báo cho mọi người (by slack) khi có lỗi xảy ra
                // Push thông báo cho mọi người(by slack) khi có lỗi xảy ra
                new PushNotiErrorToSlack().Push("GA1LZN9AM", e.Message + ", initInventory: " + e.StackTrace + ", " + Library.Function.JavaScript.Serialize(obj),
                                                        "initInventory" + ".checkout", "phucdn",
                                                        HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host);
            }
            return obj;
        }


        /// <summary>
        /// Cập nhật tồn kho khi bán sản phẩm hoặc chuyển lại kho, hoặc xuất cho các hoạt động khác (tặng khách, quay video...)
        /// </summary>
        /// <param name="db"></param>
        /// <param name="date"></param>
        /// <param name="salonId"></param>
        /// <param name="productJson"></param>
        /// <param name="type">1 | sell (Bán sản phẩm ra), 2 | send_back_store (Trả lại kho), 3 | export_out (Xuất sản phẩm cho mục đích khác)</param>
        /// <returns></returns>
        public static void updateInventoryByProductExport(Solution_30shineEntities db, DateTime date, int salonId, string productJson, int type)
        {
            //1. Lấy dữ liệu row theo salonId và ngày nhập
            //2. Lấy tổng số lượng sản phẩm đã bán theo Id các sản phẩm trong lần bán này
            //3. Cập nhật dữ liệu tồn kho : lượng bán, sản phẩm còn lại (tồn kho)
            try
            {
                var products = new List<Library.Class.ProductBasic>();
                var obj = initInventory(db, date.Date, salonId);

                if (productJson != "")
                {
                    products = serialize.Deserialize<List<Library.Class.ProductBasic>>(productJson).ToList();
                }

                if (obj != null && obj.InvenData != null && obj.InvenData != "" && products.Count > 0 && Array.IndexOf(new int[] { 1, 2, 3 }, type) != -1)
                {
                    var index = -1;
                    var item = new Library.Class.cls_inventoryItem();
                    List<Library.Class.cls_inventoryItem> productInven = new List<Class.cls_inventoryItem>();
                    productInven = serialize.Deserialize<List<Library.Class.cls_inventoryItem>>(obj.InvenData).ToList();
                    if (products.Count > 0)
                    {
                        foreach (var v in products)
                        {
                            index = productInven.FindIndex(w => w.Id == v.Id);
                            if (index != -1)
                            {
                                item = productInven[index];
                                // Bán (sell)
                                if (type == 1)
                                {
                                    item.sellQuantity += v.Quantity;
                                }
                                // Gửi lại kho (send_back_store)
                                else if (type == 2)
                                {
                                    item.sendBackQuantity += v.Quantity;
                                }
                                // Xuất ngoài (xuất cho các hoạt động khác) (export_out)
                                else if (type == 3)
                                {
                                    item.exportOutQuantity += v.Quantity;
                                }
                                item.restQuantity -= v.Quantity;
                                productInven[index] = item;
                            }
                        }
                    }
                    obj.InvenData = serialize.Serialize(productInven);
                    obj.ModifiedTime = DateTime.Now;
                    db.Inventory_Data.AddOrUpdate(obj);
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                //Push thông báo cho mọi người (by slack) khi có lỗi xảy ra
                // Push thông báo cho mọi người(by slack) khi có lỗi xảy ra
                new PushNotiErrorToSlack().Push("GA1LZN9AM", e.Message + ", updateInventoryByProductExport: " + e.StackTrace + ", " + Library.Function.JavaScript.Serialize(productJson),
                                                        "updateInventoryByProductExport" + ".checkout", "phucdn",
                                                        HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host);
            }
        }

        /// <summary>
        /// Cập nhật dữ liệu tồn kho khi update bill
        /// </summary>
        /// <param name="db"></param>
        /// <param name="date"></param>
        /// <param name="salonId"></param>
        /// <param name="productInven"></param>
        /// <param name="products"></param>
        /// <returns></returns>
        public static void updateInventoryByBillUpdate(Solution_30shineEntities db, DateTime date, int salonId, int billId)
        {
            var item = new Library.Class.cls_inventoryItem();
            var products = db.FlowProducts.Where(w => w.BillId == billId).ToList();
            var productInven = getInventoryList(db, date, salonId);
            var obj = initInventory(db, date.Date, salonId);
            if (obj != null && obj.InvenData != null && obj.InvenData != "" && products.Count > 0)
            {
                var ids = "";
                var index = -1;
                foreach (var v in products)
                {
                    ids += v.Id + ",";
                }
                ids = ids.TrimEnd(',');
                var sql = @"select a.ProductId as Id, Coalesce(Sum(Coalesce(a.ExportQuantity, 0)), 0) as Quantity
                        from
                        (
	                        select fp.*, 
		                        (case 
			                        when fp.IsDelete != 1 then fp.Quantity
			                        else 0
		                        end)  as ExportQuantity
	                        from BillService as bill
	                        inner join FlowProduct as fp
	                        on fp.BillId = bill.Id
	                        where bill.IsDelete != 1 and bill.Pending != 1 
		                        and bill.CompleteBillTime >= '" + date.Date + "' and bill.CompleteBillTime < '" + date.AddDays(1).Date + @"'
		                        and bill.ProductIds != '' and bill.ProductIds is not null
		                        and bill.SalonId = " + salonId + @"
                        ) as a
                        group by a.ProductId
                        order by a.ProductId asc";
                var prdQuantity = db.Database.SqlQuery<Library.Class.cls_id_quantity>(sql).ToList();
                if (prdQuantity.Count > 0)
                {
                    foreach (var v in prdQuantity)
                    {
                        index = productInven.FindIndex(w => w.Id == v.Id);
                        if (index != -1)
                        {
                            item = productInven[index];
                            item.sellQuantity = v.Quantity;
                            item.restQuantity = item.restQuantityLastDay + item.importQuantity - item.sendBackQuantity - item.exportOutQuantity - v.Quantity;
                            productInven[index] = item;
                        }
                    }
                }

                obj.InvenData = serialize.Serialize(productInven);
                obj.ModifiedTime = DateTime.Now;
                db.Inventory_Data.AddOrUpdate(obj);
                db.SaveChanges();
            }
        }



        /// <summary>
        /// Cập nhật thống kê tồn kho khi cập nhật bản ghi nhập tồn kho
        /// </summary>
        /// <param name="db"></param>
        /// <param name="date"></param>
        /// <param name="salonId"></param>
        /// <param name="invenId"></param>
        /// <param name="type">1 | import (Nhập tồn kho), 2 | send_back_store (Trả lại kho), 3 | export_out (Xuất sản phẩm cho mục đích khác)</param>
        public static void updateInventoryByItemUpdate(Solution_30shineEntities db, DateTime date, int salonId, int invenId, int type, bool delete)
        {
            var item = new Library.Class.cls_inventoryItem();
            var products = db.Inventory_Flow.Where(w => w.InvenId == invenId).ToList();
            var obj = initInventory(db, date, salonId);
            if (obj != null && obj.InvenData != null && obj.InvenData != "" && products.Count > 0)
            {
                var productInven = getInventoryList(db, date.Date, salonId);
                var ids = "";
                var index = -1;
                var loop = 0;
                foreach (var v in products)
                {
                    ids += v.Id + ",";
                }
                ids = ids.TrimEnd(',');
                var sql = @"select a.ProductId as Id, Coalesce(Sum(Coalesce(a.ExportQuantity, 0)), 0) as Quantity
                        from
                        (
	                        select fi.*, 
		                        (case 
			                        when fi.IsDelete != 1 then fi.Quantity
			                        else 0
		                        end)  as ExportQuantity
	                        from Inventory_Import as inven
	                        inner join Inventory_Flow as fi 
	                        on fi.InvenId = inven.Id 
	                        where inven.ImportDate >= '" + date + "' and inven.ImportDate < '" + date.AddDays(1) + @"'
		                        and inven.ProductIds != '' and inven.ProductIds is not null
		                        and inven.SalonId = " + salonId + @"  and inven.ImportType = " + type +
                                (delete != true ? " and inven.IsDelete != 1" : " ") + @"
                        ) as a
                        group by a.ProductId
                        order by a.ProductId asc";
                var prdQuantity = db.Database.SqlQuery<Library.Class.cls_id_quantity>(sql).ToList();
                if (prdQuantity.Count > 0)
                {
                    foreach (var v in prdQuantity)
                    {
                        index = productInven.FindIndex(w => w.Id == v.Id);
                        if (index != -1)
                        {
                            item = productInven[index];
                            // Tồn kho nhập
                            if (type == 1)
                            {
                                item.importQuantity = v.Quantity;
                                item.restQuantity = item.restQuantityLastDay + v.Quantity - item.sellQuantity - item.exportOutQuantity - item.sendBackQuantity;
                            }
                            // Trả lại kho
                            else if (type == 2)
                            {
                                item.sendBackQuantity = v.Quantity;
                                item.restQuantity = item.restQuantityLastDay + item.importQuantity - item.sellQuantity - item.exportOutQuantity - v.Quantity;
                            }
                            // Xuất mục đích khác
                            else if (type == 3)
                            {
                                item.exportOutQuantity = v.Quantity;
                                item.restQuantity = item.restQuantityLastDay + item.importQuantity - item.sellQuantity - item.sendBackQuantity - v.Quantity;
                            }
                            productInven[index] = item;
                        }
                        loop++;
                    }
                }
                obj.InvenData = serialize.Serialize(productInven);
                obj.ModifiedTime = DateTime.Now;
                db.Inventory_Data.AddOrUpdate(obj);
                db.SaveChanges();
            }
        }

        /// <summary>
        /// Cập nhật vào tồn kho khi thêm sản phẩm mới (trong quản lý sản phẩm)
        /// </summary>
        /// <param name="db"></param>
        /// <param name="date"></param>
        /// <param name="product"></param>
        public static void updateInventoryByProductAdd(Solution_30shineEntities db, DateTime date, Product product)
        {
            var salons = db.Tbl_Salon.Where(w => w.IsDelete != 1 && w.Publish == true).OrderBy(o => o.Id).ToList();
            if (salons.Count > 0)
            {
                foreach (var v in salons)
                {
                    addProductToInventory(db, date, v.Id, product);
                }
            }
        }

        /// <summary>
        /// Thêm sản phẩm vào chuỗi json danh sách sản phẩm tồn kho
        /// </summary>
        /// <param name="db"></param>
        /// <param name="date"></param>
        /// <param name="salonId"></param>
        /// <param name="product"></param>
        /// <returns></returns>
        public static void addProductToInventory(Solution_30shineEntities db, DateTime date, int salonId, Product product)
        {
            var list = new List<Library.Class.cls_inventoryItem>();
            var item = new Library.Class.cls_inventoryItem();
            var obj = initInventory(db, date.Date, salonId);
            if (obj != null && product != null)
            {

                if (obj.InvenData != null && obj.InvenData != "")
                {
                    list = serialize.Deserialize<List<Library.Class.cls_inventoryItem>>(obj.InvenData).ToList();
                    var index = list.FindIndex(w => w.Id == product.Id);
                    if (index == -1)
                    {
                        item.Id = product.Id;
                        item.Name = product.Name;
                        item.restQuantity = 0;
                        item.restQuantityLastDay = 0;
                        item.sellQuantity = 0;
                        item.sendBackQuantity = 0;
                        item.exportOutQuantity = 0;
                        list.Add(item);
                    }
                }
                // Trường hợp sản phẩm chưa có dữ liệu tồn kho
                else
                {
                    item.Id = product.Id;
                    item.Name = product.Name;
                    item.restQuantity = 0;
                    item.restQuantityLastDay = 0;
                    item.sellQuantity = 0;
                    item.sendBackQuantity = 0;
                    item.exportOutQuantity = 0;
                    list.Add(item);
                }

                obj.InvenData = serialize.Serialize(list);
                obj.ModifiedTime = DateTime.Now;
                db.Inventory_Data.AddOrUpdate(obj);
                db.SaveChanges();
            }
        }

        /// <summary>
        /// Check đã khởi tạo dữ liệu tồn kho cho salon hay chưa
        /// </summary>
        /// <param name="db"></param>
        /// <param name="date"></param>
        /// <param name="salonId"></param>
        /// <returns></returns>
        public static Inventory_Data isInitInventory(Solution_30shineEntities db, DateTime date, int salonId)
        {
            return db.Inventory_Data.FirstOrDefault(w => w.iDate == date.Date && w.SalonId == salonId && w.IsDelete != true);
        }

        /// <summary>
        /// Tạo chuỗi json mảng dữ liệu tồn kho sản phẩm
        /// </summary>
        /// <param name="products"></param>
        /// <param name="restLastDay"></param>
        /// <returns></returns>
        public static string genInventoryListItem(List<Product> products, List<Library.Class.cls_inventoryItem> restLastDay)
        {
            var list = new List<Library.Class.cls_inventoryItem>();
            var item = new Library.Class.cls_inventoryItem();
            var index = -1;
            if (products.Count > 0)
            {
                foreach (var v in products)
                {
                    item = new Library.Class.cls_inventoryItem();
                    item.Id = v.Id;
                    item.Name = v.Name;
                    item.importQuantity = 0;
                    item.sellQuantity = 0;
                    item.sendBackQuantity = 0;
                    item.exportOutQuantity = 0;
                    index = restLastDay.FindIndex(w => w.Id == v.Id);
                    if (index != -1)
                    {
                        item.restQuantityLastDay = restLastDay[index].restQuantity;
                        item.restQuantity = restLastDay[index].restQuantity;
                    }
                    else
                    {
                        item.restQuantityLastDay = 0;
                        item.restQuantity = 0;
                    }
                    list.Add(item);
                }
            }
            return serialize.Serialize(list);
        }

        public static List<Library.Class.cls_inventoryItem> getInventoryList(Solution_30shineEntities db, DateTime date, int salonId)
        {
            var list = new List<Library.Class.cls_inventoryItem>();
            var obj = db.Inventory_Data.FirstOrDefault(w => w.iDate == date.Date && w.SalonId == salonId && w.IsDelete != true);
            if (obj != null && obj.InvenData != null && obj.InvenData != "")
            {
                list = serialize.Deserialize<List<Library.Class.cls_inventoryItem>>(obj.InvenData).ToList();
            }
            return list;
        }

        /// <summary>
        /// Lấy dữ liệu tồn kho cuối ngày hôm qua
        /// </summary>
        /// <param name="db"></param>
        /// <param name="date"></param>
        /// <param name="salonId"></param>
        /// <returns></returns>
        public static string getRestQuantityLastDay(Solution_30shineEntities db, DateTime date, int salonId)
        {
            var lastDay = date.AddDays(-1).Date;
            var obj = db.Inventory_Data.FirstOrDefault(w => w.SalonId == salonId && w.iDate == lastDay && w.IsDelete != true);
            return obj != null && obj.InvenData != null ? obj.InvenData : "";
        }

        public static List<Library.Class.ProductBasic> getListProductSellLastDay(Solution_30shineEntities db, DateTime date, int salonId)
        {
            var sql = @"select b.Id, b.Code, b.Name, b.Cost, b.Price, a.Quantity
                        from
                        (
	                        select fp.ProductId, Coalesce(SUM(Coalesce(fp.Quantity,0)),0) as Quantity
	                        from BillService as bill
	                        inner join FlowProduct as fp
	                        on fp.BillId = bill.Id and fp.IsDelete != 1
	                        where bill.IsDelete != 1 and bill.Pending != 1
	                            and bill.CompleteBillTime >= '" + date.AddDays(-1).Date + "' and bill.CompleteBillTime < '" + date.Date + @"'
	                            and bill.ProductIds != '' and bill.ProductIds is not null
                                and bill.SalonId = " + salonId + @"
	                        group by fp.ProductId	 
                        ) as a
                        inner join Product as b
                        on b.Id = a.ProductId
                        order by b.Id asc";
            return db.Database.SqlQuery<Library.Class.ProductBasic>(sql).ToList();
        }

        ///===================================
        /// Hàm chung
        ///===================================
        /// <summary>
        /// Lấy salonId
        /// </summary>
        /// <param name="Perm_ShowSalon"></param>
        /// <param name="salonSelectedValue"></param>
        /// <returns></returns>
        public static int getSalonId(bool permShowSalon, string salonSelectedValue)
        {
            int integer;
            if (permShowSalon)
            {
                return int.TryParse(salonSelectedValue, out integer) ? integer : 0;
            }
            else
            {
                if (HttpContext.Current.Session["SalonId"] != null)
                {
                    return int.TryParse(HttpContext.Current.Session["SalonId"].ToString(), out integer) ? integer : 0;
                }
                else
                {
                    return 0;
                }
            }
        }

        /// <summary>
        /// get Id từ query string
        /// </summary>
        /// <returns></returns>
        public static int getIdFromQueryString()
        {
            int integer;
            return int.TryParse(HttpContext.Current.Request.QueryString["Id"], out integer) ? integer : 0;
        }

        /// <summary>
        /// Bind danh sách salon
        /// </summary>
        /// <param name="ddls"></param>
        /// <param name="permViewAll"></param>
        public static void bindSalon(List<DropDownList> ddls, bool permViewAll)
        {
            using (var db = new Solution_30shineEntities())
            {
                var listSalon = new List<Tbl_Salon>();
                var SalonId = Convert.ToInt32(HttpContext.Current.Session["SalonId"]);
                var UserId = Convert.ToInt32(HttpContext.Current.Session["User_Id"]);
                if (permViewAll)
                {
                    listSalon = db.Tbl_Salon.Where(w => w.IsDelete == 0 && w.Publish == true).OrderBy(o => o.Order).ToList();
                    var item = new Tbl_Salon();
                    item.ShortName = "Chọn tất cả salon";
                    item.Id = 0;
                    listSalon.Insert(0, item);
                }
                else
                {
                    listSalon = (
                                        from p in db.PermissionSalonAreas.AsNoTracking()
                                        join s in db.Tbl_Salon.AsNoTracking() on p.SalonId equals s.Id
                                        where
                                        p.IsDelete == false && p.StaffId == UserId
                                        && s.Publish == true && s.IsDelete == 0
                                        select s
                                   ).OrderBy(o => o.Order).ToList();
                    if (listSalon.Count <= 0)
                    {
                        listSalon = db.Tbl_Salon.Where(w => w.Id == SalonId).ToList();
                    }
                }
                for (var i = 0; i < ddls.Count; i++)
                {
                    ddls[i].DataTextField = "ShortName";
                    ddls[i].DataValueField = "Id";
                    ddls[i].DataSource = listSalon;
                    ddls[i].DataBind();
                }
            }
        }
        /// <summary>
        /// Bind danh sách salon mặc định có salon
        /// </summary>
        /// <param name="ddls"></param>
        /// <param name="permViewAll"></param>
        public static void bindSalonSpecial(List<DropDownList> ddls, bool permViewAll)
        {
            using (var db = new Solution_30shineEntities())
            {
                var SalonId = Convert.ToInt32(HttpContext.Current.Session["SalonId"]);
                var UserId = Convert.ToInt32(HttpContext.Current.Session["User_Id"]);
                var staffKeeping = db.FlowTimeKeepings.Where(a => a.StaffId == UserId && a.WorkDate == EntityFunctions.TruncateTime(DateTime.Now) && a.IsEnroll == true && a.IsDelete == 0).FirstOrDefault();
                if (staffKeeping != null)
                {
                    SalonId = staffKeeping.SalonId;
                }
                else
                {
                    SalonId = Convert.ToInt32(HttpContext.Current.Session["SalonId"]);
                }

                var listSalon = new List<Tbl_Salon>();
                
                if (permViewAll)
                {
                    listSalon = db.Tbl_Salon.Where(w => w.IsDelete == 0 && w.Publish == true).OrderBy(o => o.Order).ToList();
                    var item = new Tbl_Salon();
                }
                else
                {
                    listSalon = (
                                        from p in db.PermissionSalonAreas.AsNoTracking()
                                        join s in db.Tbl_Salon.AsNoTracking() on p.SalonId equals s.Id
                                        where
                                        p.IsDelete == false && p.StaffId == UserId
                                        && s.Publish == true && s.IsDelete == 0
                                        select s
                                   ).OrderBy(o => o.Order).ToList();
                    if (listSalon.Count <= 0)
                    {
                        listSalon = db.Tbl_Salon.Where(w => w.Id == SalonId).ToList();
                    }
                }
                for (var i = 0; i < ddls.Count; i++)
                {
                    ddls[i].DataTextField = "ShortName";
                    ddls[i].DataValueField = "Id";
                    ddls[i].DataSource = listSalon;
                    ddls[i].DataBind();
                }
            }
        }

        /// <summary>
        /// Bind danh sách salon
        /// </summary>
        /// <param name="ddls"></param>
        /// <param name="permViewAll"></param>
        public static void bindSalon_NotHoiQuan(List<DropDownList> ddls, bool permViewAll)
        {
            using (var db = new Solution_30shineEntities())
            {
                var listSalon = new List<Tbl_Salon>();
                var SalonId = Convert.ToInt32(HttpContext.Current.Session["SalonId"]);
                var UserId = Convert.ToInt32(HttpContext.Current.Session["User_Id"]);
                if (permViewAll)
                {
                    listSalon = db.Tbl_Salon.Where(w => w.IsDelete == 0 && w.Publish == true && w.IsSalonHoiQuan == false).OrderBy(o => o.Order).ToList();
                    var item = new Tbl_Salon();
                    item.ShortName = "Chọn tất cả salon";
                    item.Id = 0;
                    listSalon.Insert(0, item);
                }
                else
                {
                    listSalon = (
                                        from p in db.PermissionSalonAreas.AsNoTracking()
                                        join s in db.Tbl_Salon.AsNoTracking() on p.SalonId equals s.Id
                                        where
                                        p.IsDelete == false && p.StaffId == UserId
                                        && s.Publish == true && s.IsDelete == 0 && s.IsSalonHoiQuan == false
                                        select s
                                   ).OrderBy(o => o.Order).ToList();
                    if (listSalon.Count <= 0)
                    {
                        listSalon = db.Tbl_Salon.Where(w => w.Id == SalonId).ToList();
                    }
                }
                for (var i = 0; i < ddls.Count; i++)
                {
                    ddls[i].DataTextField = "ShortName";
                    ddls[i].DataValueField = "Id";
                    ddls[i].DataSource = listSalon;
                    ddls[i].DataBind();
                }
            }
        }
        /// <summary>
        /// Bind danh sách salon trên timeline
        /// </summary>
        /// <param name="ddls"></param>
        /// <param name="permViewAll"></param>
        public static void bindSalon_NotHoiQuan_Timeline(List<DropDownList> ddls, bool permViewAll, DateTime WorkDate)
        {
            using (var db = new Solution_30shineEntities())
            {
                var listSalon = new List<Tbl_Salon>();
                var UserId = Convert.ToInt32(HttpContext.Current.Session["User_Id"]);
                var SalonId = 0; // Convert.ToInt32(HttpContext.Current.Session["SalonId"]);

                var staffKeeping = db.FlowTimeKeepings.Where(a => a.StaffId == UserId && a.WorkDate == EntityFunctions.TruncateTime(WorkDate) && a.IsDelete == 0 && a.IsEnroll == true).FirstOrDefault();
                if (staffKeeping != null)
                {
                    SalonId = staffKeeping.SalonId;
                }
                else
                {
                    SalonId = Convert.ToInt32(HttpContext.Current.Session["SalonId"]);
                }

                if (permViewAll)
                {
                    listSalon = db.Tbl_Salon.Where(w => w.IsDelete == 0 && w.Publish == true && w.IsSalonHoiQuan == false).OrderBy(o => o.Order).ToList();
                    var item = new Tbl_Salon();
                    item.ShortName = "Chọn tất cả salon";
                    item.Id = 0;
                    listSalon.Insert(0, item);
                }
                else
                {
                    listSalon = (
                                        from p in db.PermissionSalonAreas.AsNoTracking()
                                        join s in db.Tbl_Salon.AsNoTracking() on p.SalonId equals s.Id
                                        where
                                        p.IsDelete == false && p.StaffId == UserId
                                        && s.Publish == true && s.IsDelete == 0 && s.IsSalonHoiQuan == false
                                        select s
                                   ).OrderBy(o => o.Order).ToList();
                    if (listSalon.Count <= 0)
                    {
                        listSalon = db.Tbl_Salon.Where(w => w.Id == SalonId).ToList();
                    }
                }
                for (var i = 0; i < ddls.Count; i++)
                {
                    ddls[i].DataTextField = "ShortName";
                    ddls[i].DataValueField = "Id";
                    ddls[i].DataSource = listSalon;
                    ddls[i].DataBind();
                }
            }
        }
        /// <summary>
        /// Bind danh sách salon special
        /// </summary>
        /// <param name="ddls"></param>
        /// <param name="permViewAll"></param>
        public static void bindSalon_NotHoiQuanSpecial(List<DropDownList> ddls, bool permViewAll)
        {
            using (var db = new Solution_30shineEntities())
            {
                var listSalon = new List<Tbl_Salon>();
                var SalonId = Convert.ToInt32(HttpContext.Current.Session["SalonId"]);
                var UserId = Convert.ToInt32(HttpContext.Current.Session["User_Id"]);
                if (permViewAll)
                {
                    listSalon = db.Tbl_Salon.Where(w => w.IsDelete == 0 && w.Publish == true && w.IsSalonHoiQuan == false).OrderBy(o => o.Order).ToList();
                }
                else
                {
                    listSalon = (
                                        from p in db.PermissionSalonAreas.AsNoTracking()
                                        join s in db.Tbl_Salon.AsNoTracking() on p.SalonId equals s.Id
                                        where
                                        p.IsDelete == false && p.StaffId == UserId
                                        && s.Publish == true && s.IsDelete == 0 && s.IsSalonHoiQuan == false
                                        select s
                                   ).OrderBy(o => o.Order).ToList();
                    if (listSalon.Count > 0)
                    {
                        listSalon = db.Tbl_Salon.Where(w => w.Id == SalonId).ToList();
                    }
                }
                for (var i = 0; i < ddls.Count; i++)
                {
                    ddls[i].DataTextField = "ShortName";
                    ddls[i].DataValueField = "Id";
                    ddls[i].DataSource = listSalon;
                    ddls[i].DataBind();
                }
            }
        }
        /// <summary>
        /// Bind danh sách salon
        /// </summary>
        /// <param name="ddls"></param>
        /// <param name="permViewAll"></param>
        public static void bindSalonHoiQuan(List<DropDownList> ddls, bool permViewAll)
        {
            using (var db = new Solution_30shineEntities())
            {
                var listSalon = new List<Tbl_Salon>();
                var SalonId = Convert.ToInt32(HttpContext.Current.Session["SalonId"]);
                var UserId = Convert.ToInt32(HttpContext.Current.Session["User_Id"]);
                if (permViewAll)
                {
                    listSalon = db.Tbl_Salon.Where(w => w.IsDelete == 0 && w.Publish == true && w.IsSalonHoiQuan == true).OrderBy(o => o.Order).ToList();
                    var item = new Tbl_Salon();
                    item.ShortName = "Chọn tất cả salon";
                    item.Id = 0;
                    listSalon.Insert(0, item);
                }
                else
                {
                    listSalon = (
                                        from p in db.PermissionSalonAreas.AsNoTracking()
                                        join s in db.Tbl_Salon.AsNoTracking() on p.SalonId equals s.Id
                                        where
                                        p.IsDelete == false && p.StaffId == UserId
                                        && s.Publish == true && s.IsDelete == 0 && s.IsSalonHoiQuan == true
                                        select s
                                   ).OrderBy(o => o.Order).ToList();
                    if (listSalon.Count <= 0)
                    {
                        listSalon = db.Tbl_Salon.Where(w => w.Id == SalonId).ToList();
                    }
                }
                for (var i = 0; i < ddls.Count; i++)
                {
                    ddls[i].DataTextField = "ShortName";
                    ddls[i].DataValueField = "Id";
                    ddls[i].DataSource = listSalon;
                    ddls[i].DataBind();
                }
            }
        }

        /// <summary>
        /// Bind danh sách salon
        /// </summary>
        /// <param name="ddls"></param>
        /// <param name="permViewAll"></param>
        public static void bindSalonHoiQuanSpecial(List<DropDownList> ddls, bool permViewAll)
        {
            using (var db = new Solution_30shineEntities())
            {
                var listSalon = new List<Tbl_Salon>();
                var SalonId = Convert.ToInt32(HttpContext.Current.Session["SalonId"]);
                var UserId = Convert.ToInt32(HttpContext.Current.Session["User_Id"]);
                if (permViewAll)
                {
                    listSalon = db.Tbl_Salon.Where(w => w.IsDelete == 0 && w.Publish == true && w.IsSalonHoiQuan == true).OrderBy(o => o.Order).ToList();
                }
                else
                {
                    listSalon = (
                                        from p in db.PermissionSalonAreas.AsNoTracking()
                                        join s in db.Tbl_Salon.AsNoTracking() on p.SalonId equals s.Id
                                        where
                                        p.IsDelete == false && p.StaffId == UserId
                                        && s.Publish == true && s.IsDelete == 0 && s.IsSalonHoiQuan == true
                                        select s
                                   ).OrderBy(o => o.Order).ToList();
                    if (listSalon.Count <= 0)
                    {
                        listSalon = db.Tbl_Salon.Where(w => w.Id == SalonId).ToList();
                    }
                }
                for (var i = 0; i < ddls.Count; i++)
                {
                    ddls[i].DataTextField = "ShortName";
                    ddls[i].DataValueField = "Id";
                    ddls[i].DataSource = listSalon;
                    ddls[i].DataBind();
                }
            }
        }

        public static void bindInventoryOrder(List<DropDownList> ddls, bool permViewAll)
        {
            using (var db = new Solution_30shineEntities())
            {
                var listInventory = new List<IvInventory>();
                var SalonId = Convert.ToInt32(HttpContext.Current.Session["SalonId"]);
                var UserId = Convert.ToInt32(HttpContext.Current.Session["User_Id"]);
                if (permViewAll)
                {
                    listInventory = db.IvInventories.Where(w => w.IsDelete == false).OrderByDescending(o => o.Name).ToList();
                    var item = new IvInventory();
                    item.Name = "Chọn tất cả kho đặt hàng";
                    item.Id = 0;
                    listInventory.Insert(0, item);
                }
                else
                {
                    listInventory = (
                                        from p in db.PermissionSalonAreas.AsNoTracking()
                                        join s in db.Tbl_Salon.AsNoTracking() on p.SalonId equals s.Id
                                        join i in db.IvInventories.AsNoTracking() on p.SalonId equals i.SalonId
                                        where
                                        p.IsDelete == false && p.StaffId == UserId
                                        && s.Publish == true && s.IsDelete == 0
                                        && i.IsDelete == false
                                        select i
                                   ).OrderByDescending(o => o.Name).ToList();
                    if (listInventory.Count <= 0)
                    {
                        listInventory = db.IvInventories.Where(w => w.SalonId == SalonId && w.IsDelete == false).ToList();
                    }
                }
                for (var i = 0; i < ddls.Count; i++)
                {
                    ddls[i].DataTextField = "Name";
                    ddls[i].DataValueField = "Id";
                    ddls[i].DataSource = listInventory;
                    ddls[i].DataBind();
                }
            }
        }

        public static void bindInventoryPartner(List<DropDownList> ddls)
        {
            using (var db = new Solution_30shineEntities())
            {
                var listInventory = new List<IvInventory>();
                var SalonId = Convert.ToInt32(HttpContext.Current.Session["SalonId"]);
                var UserId = Convert.ToInt32(HttpContext.Current.Session["User_Id"]);

                listInventory = db.IvInventories.Where(w => w.IsDelete == false).OrderByDescending(o => o.Name).ToList();
                var item = new IvInventory();
                item.Name = "Chọn tất cả kho được yêu cầu";
                item.Id = 0;
                listInventory.Insert(0, item);

                for (var i = 0; i < ddls.Count; i++)
                {
                    ddls[i].DataTextField = "Name";
                    ddls[i].DataValueField = "Id";
                    ddls[i].DataSource = listInventory;
                    ddls[i].DataBind();
                }
            }
        }

        public static void bindInventoryTypeSalon(List<IvInventory> ddls)
        {
            using (var db = new Solution_30shineEntities())
            {
                var listInventory = new List<IvInventory>();
                var SalonId = Convert.ToInt32(HttpContext.Current.Session["SalonId"]);
                var UserId = Convert.ToInt32(HttpContext.Current.Session["User_Id"]);

                listInventory = db.IvInventories.Where(w => w.IsDelete == false && w.Type == Libraries.AppConstants.InventoryType.SALON).OrderByDescending(o => o.Id).ToList();
                //var item = new IvInventory();
                //item.Name = "Chọn tất cả kho được yêu cầu";
                //item.Id = 0;
                //listInventory.Insert(0, item);
                ddls.AddRange(listInventory);

                //for (var i = 0; i < ddls.Count; i++)
                //{
                //    ddls[i].DataTextField = "Name";
                //    ddls[i].DataValueField = "Id";
                //    ddls[i].DataSource = listInventory;
                //    ddls[i].DataBind();
                //}
            }
        }

        public static void bindOrderType(List<DropDownList> ddls, bool isListForm)
        {
            using (var db = new Solution_30shineEntities())
            {
                var listOrderType = new List<Tbl_Config>();
                listOrderType = db.Tbl_Config.Where(w => w.IsDelete == 0 & w.Key == "iv_order").OrderBy(o => o.Id).ToList();
                //dung cho form danh sach
                if (isListForm)
                {
                    var item = new Tbl_Config();
                    item.Label = "Chọn tất cả loại đơn hàng";
                    item.Value = "0";
                    listOrderType.Insert(0, item);
                }

                for (var i = 0; i < ddls.Count; i++)
                {
                    ddls[i].DataTextField = "Label";
                    ddls[i].DataValueField = "Value";
                    ddls[i].DataSource = listOrderType;
                    ddls[i].DataBind();
                }
            }
        }

        public static void bindOrderStatus(List<DropDownList> ddls)
        {
            using (var db = new Solution_30shineEntities())
            {
                var listOrderStatus = new List<Tbl_Status>();
                listOrderStatus = db.Tbl_Status.Where(w => w.IsDelete == false & w.ParentId == 74).OrderBy(o => o.Value).ToList();

                var item = new Tbl_Status();
                item.Name = "Chọn tất cả trạng thái";
                item.Value = 0;
                listOrderStatus.Insert(0, item);

                for (var i = 0; i < ddls.Count; i++)
                {
                    ddls[i].DataTextField = "Name";
                    ddls[i].DataValueField = "Value";
                    ddls[i].DataSource = listOrderStatus;
                    ddls[i].DataBind();
                }
            }
        }
        public static void bindProduct(List<DropDownList> ddls, bool permViewAll = true)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var listProduct = db.Products.Where(r => r.IsDelete == 0 && r.Publish == 1).ToList();
                    var firstProduct = new Product();
                    firstProduct.Name = "Chọn sản phẩm";
                    firstProduct.Id = 0;
                    listProduct.Insert(0, firstProduct);

                    for (var i = 0; i < ddls.Count; i++)
                    {
                        ddls[i].DataTextField = "Name";
                        ddls[i].DataValueField = "Id";
                        ddls[i].DataSource = listProduct;
                        ddls[i].DataBind();
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public static void bindService(List<DropDownList> ddls, bool permViewAll = true)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var listService = db.Services.Where(r => r.IsDelete == 0 && r.Publish == 1).ToList();
                    var firstService = new Service();
                    firstService.Name = "Chọn dịch vụ";
                    firstService.Id = 0;
                    listService.Insert(0, firstService);

                    for (var i = 0; i < ddls.Count; i++)
                    {
                        ddls[i].DataTextField = "Name";
                        ddls[i].DataValueField = "Id";
                        ddls[i].DataSource = listService;
                        ddls[i].DataBind();
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        /// <summary>
        /// bind all inventory Type
        /// </summary>
        /// <param name="ddls"></param>
        /// <param name="permViewAll"></param>
        public static void bindInventoryType(List<DropDownList> ddls, bool permViewAll = true)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var listType = new List<Tbl_Config>();
                    if (permViewAll)
                    {
                        listType = db.Tbl_Config.Where(r => r.IsDelete == 0 && r.Key == "iv_inventory_type").ToList();
                    }
                    else
                    {
                        listType = db.Tbl_Config.Where(r => r.IsDelete == 0 && r.Key == "iv_inventory_type" && (r.Value == "2" || r.Value == "3")).ToList();
                    }

                    var firstType = new Tbl_Config();
                    firstType.Label = "Chọn loại kho";
                    firstType.Value = "0";
                    listType.Insert(0, firstType);

                    for (var i = 0; i < ddls.Count; i++)
                    {
                        ddls[i].DataTextField = "Label";
                        ddls[i].DataValueField = "Value";
                        ddls[i].DataSource = listType;
                        ddls[i].DataBind();
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public static void bindCosmeticType(List<DropDownList> ddls, bool isListForm)
        {
            using (var db = new Solution_30shineEntities())
            {
                var listCosmeticType = new List<Tbl_Config>();
                listCosmeticType = db.Tbl_Config.Where(w => w.IsDelete == 0 & w.Key == "cosmetic_type").OrderBy(o => o.Id).ToList();
                //dung cho form danh sach
                if (isListForm)
                {
                    var item = new Tbl_Config();
                    item.Label = "Chọn tất cả loại hàng";
                    item.Value = "0";
                    listCosmeticType.Insert(0, item);
                }

                for (var i = 0; i < ddls.Count; i++)
                {
                    ddls[i].DataTextField = "Label";
                    ddls[i].DataValueField = "Value";
                    ddls[i].DataSource = listCosmeticType;
                    ddls[i].DataBind();
                }
            }
        }

        public static List<Tbl_Config> bindInventoryReason(List<DropDownList> ddls, bool isListForm)
        {
            using (var db = new Solution_30shineEntities())
            {
                var listReason = new List<Tbl_Config>();
                listReason = db.Tbl_Config.Where(w => w.IsDelete == 0 & w.Key == "iv_order_detail_reason").OrderBy(o => o.Id).ToList();
                //dung cho form danh sach
                if (isListForm)
                {
                    var item = new Tbl_Config();
                    item.Label = "Chọn tất cả loại lý do";
                    item.Value = "0";
                    listReason.Insert(0, item);
                }

                for (var i = 0; i < ddls.Count; i++)
                {
                    ddls[i].DataTextField = "Label";
                    ddls[i].DataValueField = "Value";
                    ddls[i].DataSource = listReason;
                    ddls[i].DataBind();
                }
                return listReason;
            }
        }
        /// <summary>
        /// bind list routing in dropdownlist 
        /// </summary>
        public static void bindRouting(List<DropDownList> ddls)
        {
            using (var db = new Solution_30shineEntities())
            {
                var listRouting = new List<IvRouting>();
                var list = db.IvRoutings.ToList();
                var item = new IvRouting();
                item.RoutingName = "Chọn tất cả tuyến đường";
                item.ID = 0;
                list.Insert(0, item);
                for (var i = 0; i < ddls.Count; i++)
                {
                    ddls[i].DataTextField = "RoutingName";
                    ddls[i].DataValueField = "ID";
                    ddls[i].DataSource = list;
                    ddls[i].DataBind();
                }

            }
        }

        public static List<Tbl_Config> bindProgressReportType(List<DropDownList> ddls, bool isListForm)
        {
            using (var db = new Solution_30shineEntities())
            {
                var listReason = new List<Tbl_Config>();
                listReason = db.Tbl_Config.Where(w => w.IsDelete == 0 & w.Key == "iv_order_filter").OrderBy(o => o.Id).ToList();
                //dung cho form danh sach
                if (isListForm)
                {
                    var item = new Tbl_Config();
                    item.Label = "Chọn tất cả loại tiến độ";
                    item.Value = "0";
                    listReason.Insert(0, item);
                }

                for (var i = 0; i < ddls.Count; i++)
                {
                    ddls[i].DataTextField = "Label";
                    ddls[i].DataValueField = "Value";
                    ddls[i].DataSource = listReason;
                    ddls[i].DataBind();
                }
                return listReason;
            }
        }
        public static void bindApiMethod(List<DropDownList> ddls, bool isListForm)
        {
            using (var db = new Solution_30shineEntities())
            {
                var listMethodType = new List<Tbl_Config>();
                listMethodType = db.Tbl_Config.Where(w => w.IsDelete == 0 & w.Key == "api_method_type").OrderBy(o => o.Id).ToList();
                //dung cho form danh sach
                if (isListForm)
                {
                    var item = new Tbl_Config();
                    item.Label = "Chọn tất cả loại method";
                    item.Value = "0";
                    listMethodType.Insert(0, item);
                }

                for (var i = 0; i < ddls.Count; i++)
                {
                    ddls[i].DataTextField = "Label";
                    ddls[i].DataValueField = "Value";
                    ddls[i].DataSource = listMethodType;
                    ddls[i].DataBind();
                }
            }
        }

        /// <summary>
        /// Bind bậc kỹ năng
        /// </summary>
        public static void Bind_SkillLevel(List<DropDownList> ddls)
        {
            using (var db = new Solution_30shineEntities())
            {
                var list = db.Tbl_SkillLevel.Where(w => w.IsDelete != 1 && w.Publish == true).OrderBy(o => o.Order).ToList();
                var item = new Tbl_SkillLevel();
                item.Name = "Chọn tất cả";
                item.Id = 0;
                list.Insert(0, item);
                for (var i = 0; i < ddls.Count; i++)
                {
                    ddls[i].DataTextField = "Name";
                    ddls[i].DataValueField = "Id";
                    ddls[i].DataSource = list;
                    ddls[i].DataBind();
                }
            }
        }

        /// <summary>
        /// Bind bộ phận
        /// </summary>
        /// <param name="ddls"></param>
        public static void Bind_Department(List<DropDownList> ddls)
        {
            using (var db = new Solution_30shineEntities())
            {
                var list = db.Staff_Type.Where(w => w.IsDelete == 0 && w.Publish == true).OrderBy(o => o.Order).ToList();
                var item = new Staff_Type();
                item.Name = "Chọn bộ phận";
                item.Id = 0;
                list.Insert(0, item);
                for (var i = 0; i < ddls.Count; i++)
                {
                    ddls[i].DataTextField = "Name";
                    ddls[i].DataValueField = "Id";
                    ddls[i].DataSource = list;
                    ddls[i].DataBind();
                }
            }
        }
        /// <summary>
        /// Bind lớp học
        /// </summary>
        /// <param name="ddls"></param>
        public static void bind_S4M_Class(List<DropDownList> ddls)
        {
            using (var db = new Solution_30shineEntities())
            {
                var list = new List<Stylist4Men_Class>();
                list = db.Stylist4Men_Class.Where(w => w.IsDelete != true && w.Publish == true).OrderBy(o => o.Id).ToList();
                var item = new Stylist4Men_Class();
                item.Name = "Chọn lớp học";
                item.Id = 0;
                list.Insert(0, item);
                for (var i = 0; i < ddls.Count; i++)
                {
                    ddls[i].DataTextField = "Name";
                    ddls[i].DataValueField = "Id";
                    ddls[i].DataSource = list;
                    ddls[i].DataBind();
                }
            }
        }

        /// <summary>
        /// Bind học viên theo lớp học ID
        /// </summary>
        /// <param name="ddls"></param>
        /// <param name="classId"></param>
        public static void bind_S4M_StudentByClass(List<DropDownList> ddls, int classId)
        {
            using (var db = new Solution_30shineEntities())
            {
                var list = new List<Stylist4Men_Student>();
                list = db.Stylist4Men_Student.Where(w => w.IsDelete != true && w.Publish == true && w.ClassId == classId).OrderBy(o => o.Id).ToList();
                var item = new Stylist4Men_Student();
                item.Fullname = "Chọn học viên";
                item.Id = 0;
                list.Insert(0, item);
                for (var i = 0; i < ddls.Count; i++)
                {
                    ddls[i].DataTextField = "Fullname";
                    ddls[i].DataValueField = "Id";
                    ddls[i].DataSource = list;
                    ddls[i].DataBind();
                }
            }
        }

        /// <summary>
        /// Validate số phone
        /// </summary>
        /// <param name="phone"></param>
        /// <returns></returns>
        public static Boolean checkPhone(string phone)
        {
            string res = "";
            res = phone.Substring(0, 1);
            if ((phone.Length < 10 || phone.Length > 11) || !res.Equals("0") || checknumber(phone) == false)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Validate field nhập tên
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static Boolean checkName(string name, int limitCharacters)
        {
            string sValue = name;
            string regex = @"^[a-zA-ZàáạảãâầấậẩẫăằắặẳẵđèéẹẻẽêềếệểễìíịỉĩòóọỏõôồốộổỗơờớợởỡùúụủũưừứựửữỳýỵỷỹÀÁẠẢÃÂẦẤẬẨẪĂẰẮẶẲẴĐÈÉẸẺẼÊỀẾỆỂỄÌÍỊỈĨÒÓỌỎÕÔỒỐỘỔỖƠỜỚỢỞỠÙÚỤỦŨƯỪỨỰỬỮỲÝỴỶỸàáạảãâầấậẩẫăằắặẳẵèéẹẻẽêềếệểễìíịỉĩòóọỏõôồốộổỗơờớợởỡùúụủũưừứựửữỳýỵỷỹÀÁẠẢÃÂẦẤẬẨẪĂẰẮẶẲẴÈÉẸẺẼÊỀẾỆỂỄÌÍỊỈĨÒÓỌỎÕÔỒỐỘỔỖƠỜỚỢỞỠÙÚỤỦŨƯỪỨỰỬỮỲÝỴỶỸ\s]+$";
            if (!Regex.IsMatch(sValue, regex) || sValue.Length > limitCharacters)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Validate ký tự kiểu số
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static bool checknumber(string str)
        {
            foreach (Char c in str)
            {
                if (!Char.IsDigit(c))
                    return false;
            }
            return true;
        }

        /// <summary>
        /// Function chuyển array từ C# -> array trong javascript
        /// </summary>
        public static class JavaScript
        {
            public static string Serialize(object o)
            {
                JavaScriptSerializer js = new JavaScriptSerializer();
                return js.Serialize(o);
            }
        }

        /// <summary>
        /// Mess funtions
        /// </summary>
        /// <param name="text"></param>
        public static void WriteToFile(string text, string path)
        {
            //string path = @"F:\service_30shine_log.txt";
            string dirPath = HttpContext.Current.Server.MapPath("~");
            string foldel = "";
            string fileName = "";
            if (path != "")
            {
                fileName = path.Split('\\')[path.Split('\\').Length - 1];
                foldel = path.Replace(fileName, "");
            }
            if (!Directory.Exists(dirPath + foldel))
            {
                Directory.CreateDirectory(dirPath + foldel);
            }

            using (StreamWriter writer = new StreamWriter(dirPath + foldel + fileName, true))
            {
                writer.WriteLine(text);
                writer.Close();
            }
        }
    }

    /// <summary>
    /// Tree class
    /// </summary>
    public class Tree
    {
        private Tree(List<Tbl_Category> list)
        {
            genTree(list);
        }

        public List<Library.Class.cls_treeNode> _Tree = new List<Library.Class.cls_treeNode>();
        public void genTree(List<Tbl_Category> list)
        {
            var Node = new Library.Class.cls_treeNode();
            if (list.Count > 0)
            {
                foreach (var v in list)
                {
                    Node = new Library.Class.cls_treeNode();
                    Node.Id = v.Id;
                    Node.Pid = v.Pid.Value;
                    Node.Name = v.Name;
                    Node.ChildIds = getChildIdsById(list, v.Id);
                    _Tree.Add(Node);
                }
            }
        }

        public List<Library.Class.cls_treeNode> getNodeById(int Id)
        {
            var Node = new List<Library.Class.cls_treeNode>();
            var node = new Library.Class.cls_treeNode();
            if (_Tree.Count > 0)
            {
                foreach (var v in _Tree)
                {
                    if (v.Id == Id)
                    {
                        Node.Add(v);
                        if (v.ChildIds.Count > 0)
                        {
                            foreach (var v2 in v.ChildIds)
                            {
                                getNodeById(v2);
                            }
                        }
                    }
                }
            }
            return Node;
        }

        public void addNode(Library.Class.cls_treeNode Node)
        {
            _Tree.Add(Node);
        }

        public List<Library.Class.cls_treeNode> getChildsById(int Id)
        {
            return _Tree.Where(w => w.Pid == Id).ToList();
        }

        public List<int> getChildIdsById(List<Tbl_Category> list, int Id)
        {
            return list.Where(w => w.Pid == Id).Select(s => s.Id).ToList();
        }

        public string printNode()
        {
            return excPrintNode("", new List<int>(), 1);
        }

        private string excPrintNode(string tree, List<int> isPrintIds, int level)
        {
            if (_Tree.Count > 0)
            {
                foreach (var v in _Tree)
                {
                    if (isPrintIds.IndexOf(v.Id) == -1)
                    {
                        tree += genTab(level) + v.Name + "<br/>";
                        isPrintIds.Add(v.Id);
                    }
                }
            }
            return tree;
        }

        private string genTab(int level)
        {
            var tab = "";
            for (var i = 0; i < level; i++)
            {
                tab += "-";
            }
            return tab;
        }
    }
}