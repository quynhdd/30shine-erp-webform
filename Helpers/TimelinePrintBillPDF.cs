﻿using _30shine.MODEL.ENTITY.EDMX;
using LinqKit;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace _30shine.Helpers
{
    public class TimelinePrintBillPDF
    {
        private List<Appointment> CustomerHistoryAppoint = new List<Appointment>();
        private List<Appointment> CustomerHistoryProduct = new List<Appointment>();
        private List<ProductBasic> ProductList = new List<ProductBasic>();
        private List<ProductBasic> ServiceList = new List<ProductBasic>();
        protected BillService billBooking = new BillService();

        /// <summary>
        /// In phiếu dạng file PDF
        /// </summary>
        /// <param name="isPending"></param>
        public static void GenPDF(bool isPending, SM_BillTemp bill)
        {
            var _bill = new cls_bill();
            using (var db = new Solution_30shineEntities())
            {
                if (bill != null)
                {
                    var sql = @"select bill.*, customer.Phone as cusPhone,
                                    customer.Fullname as customerName,
	                                stylist.Fullname as stylistName,
	                                skinner.Fullname as skinnerName,
	                                seller.Fullname as sellerName,
	                                checkin.Fullname as checkinName,
	                                BookHour.[Hour] as HourFrame, stylistBook.Fullname as StylistBookName
                                from SM_BillTemp as bill
                                left join Customer as customer
                                on bill.CustomerId = customer.Id
                                left join Staff as stylist
                                on bill.Staff_Hairdresser_Id = stylist.Id
                                left join Staff as skinner
                                on bill.Staff_HairMassage_Id = skinner.Id
                                left join Staff as seller
                                on bill.SellerId = seller.Id
                                left join Staff as checkin
                                on bill.ReceptionId = checkin.Id
                                left join SM_BookingTemp on SM_BookingTemp.Id = bill.BookingId
                                left join BookHour on BookHour.Id = SM_BookingTemp.HourId
                                left join Staff as stylistBook on stylistBook.Id = SM_BookingTemp.StylistId
                                where bill.Id = " + bill.Id;
                    _bill = db.Database.SqlQuery<cls_bill>(sql).FirstOrDefault();
                }
            }

            string htmlContent = GenHtmlContent(_bill);
            var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
            htmlToPdf.PageWidth = 65;
            htmlToPdf.PageHeight = 1000;
            htmlToPdf.Margins.Left = 0;
            htmlToPdf.Margins.Right = 0;
            htmlToPdf.Margins.Top = 0;
            htmlToPdf.Margins.Bottom = 0;
            var pdfBytes = htmlToPdf.GeneratePdf(htmlContent);

            string filename = "/Public/PDF/" + bill.PDFBillCode + ".pdf";
            string filepath = HttpContext.Current.Server.MapPath("~" + filename);
            File.WriteAllBytes(filepath, pdfBytes);
            //Response.ContentType = "application/pdf";
            //Response.ContentEncoding = System.Text.Encoding.UTF8;
            //Response.AddHeader("Content-Disposition", "Inline; filename=TEST.pdf");
            //Response.BinaryWrite(pdfBytes);
            //Response.Flush();
            //Response.End();
        }

        /// <summary>
        /// Lấy dữ liệu dịch vụ và sản phẩm từ bill hiện tại
        /// </summary>
        /// <param name="bill"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        private static List<ProductBasic> getDataBill(cls_bill bill, string type)
        {
            var list = new List<ProductBasic>();
            var serializer = new JavaScriptSerializer();
            switch (type)
            {
                case "service":
                    if (bill != null && bill.ServiceIds != null && bill.ServiceIds != "")
                    {
                        list = serializer.Deserialize<List<ProductBasic>>(bill.ServiceIds);
                    }
                    break;
                case "product":
                    if (bill != null && bill.ProductIds != null && bill.ProductIds != "")
                    {
                        list = serializer.Deserialize<List<ProductBasic>>(bill.ProductIds);
                    }
                    break;
                default: break;
            }
            return list;
        }

        /// <summary>
        /// Lấy dữ liệu lịch sử bill gần đây nhất của khách
        /// </summary>
        /// <param name="bill"></param>
        /// <param name="type">Dịch vụ hay Mỹ phẩm (service or product)</param>
        /// <returns></returns>
        private static cls_bill getDataHistory(cls_bill bill, string type)
        {
            var list = new cls_bill();
            if (bill != null && bill.CustomerCode != "")
            {
                using (var db = new Solution_30shineEntities())
                {
                    var wherePS = "";
                    var sql = @"select top 1 bill.*, customer.Phone as cusPhone,
	                        customer.Fullname as customerName,
	                        stylist.Fullname as stylistName,
	                        skinner.Fullname as skinnerName,
	                        seller.Fullname as sellerName,
	                        checkin.Fullname as checkinName
                        from SM_BookingTemp as bill
                        left join Customer as customer
                        on bill.CustomerId = customer.Id
                        left join Staff as stylist
                        on bill.Staff_Hairdresser_Id = stylist.Id
                        left join Staff as skinner
                        on bill.Staff_HairMassage_Id = skinner.Id
                        left join Staff as seller
                        on bill.SellerId = seller.Id
                        left join Staff as checkin
                        on bill.ReceptionId = checkin.Id
                        where bill.IsDelete != 1 and bill.Pending != 1 and bill.CustomerCode = '" + bill.CustomerCode + "'";
                    switch (type)
                    {
                        case "service":
                            wherePS = " and ServiceIds != '' and ServiceIds is not null";
                            break;
                        case "product":
                            wherePS = " and ProductIds != '' and ProductIds is not null";
                            break;
                        default: break;
                    }
                    sql += wherePS + " order by Id desc";
                    list = db.Database.SqlQuery<cls_bill>(sql).FirstOrDefault();
                }
            }
            return list;
        }

        private static List<cls_flowPromotion> getPromotion(cls_bill bill)
        {
            var list = new List<cls_flowPromotion>();
            using (var db = new Solution_30shineEntities())
            {
                if (bill != null)
                {
                    var sql = @"select fp.*, srv.Name
                                from FlowPromotion as fp
                                left join Service as srv
                                on fp.PromotionId = srv.Id
                                where fp.IsDelete != 1 and BillId = " + bill.Id;
                    list = db.Database.SqlQuery<cls_flowPromotion>(sql).ToList();
                }
            }
            return list;
        }

        /// <summary>
        /// Tạo nội dung bill (html content)
        /// </summary>
        /// <returns></returns>
        private static string GenHtmlContent(cls_bill bill)
        {
            var ServiceList = getDataBill(bill, "service");
            var ProductList = getDataBill(bill, "product");
            //var CustomerHistoryService = getDataHistory(bill, "service");
            //var CustomerHistoryProduct = getDataHistory(bill, "product");
            int times = 0;
            // gen service (dịch vụ)
            string service = "<table style='border-collapse: collapse;width: 100%;margin-top: 5px;'>" +
                                "<thead>" +
                                    "<tr>" +
                                        "<th style='font-family: Arial, sans-serif;font-size: 15px; border: 1px solid #000;width: 80%; padding: 5px; font-weight:normal;'>Dịch vụ</th>" +
                                        "<th style='font-family: Arial, sans-serif;font-size: 15px; border: 1px solid #000;width: 20%; padding: 5px; font-weight: normal;'>SL</th>" +
                                    "</tr>" +
                                "</thead>" +
                                "<tbody>";
            if (ServiceList != null && ServiceList.Count > 0)
            {
                var totalMoney = 0;
                foreach (var v in ServiceList)
                {
                    totalMoney += v.Price * v.Quantity;
                    service += "<tr>" +
                                    "<td style='font-family: Arial, sans-serif;font-size: 15px; border: 1px solid #000;width: 80%; padding: 5px;'>" + v.Name + "</td>" +
                                    "<td style='font-family: Arial, sans-serif;font-size: 15px; border: 1px solid #000;width: 20%; padding: 5px; text-align: right;'>" + v.Quantity.ToString() + "</td>" +
                                "</tr>";
                }
                // Render stylist, skinner
                //CultureInfo elGR = CultureInfo.CreateSpecificCulture("el-GR");
                //items.Add(new KeyValuePair<string, string>("Thành tiền", String.Format(elGR, "{0:0,0}", totalMoney) + " VNĐ"));
            }
            //else
            //{
            //    for (var i = 0; i < 2; i++)
            //    {
            //        service += "<tr>" +
            //                        "<td style='font-family: Arial, sans-serif;font-size: 15px; border: 1px solid #000;width: 80%; height: 28px;'></td>" +
            //                        "<td style='font-family: Arial, sans-serif;font-size: 15px; border: 1px solid #000;width: 20%; height: 28px;'></td>" +
            //                    "</tr>";
            //    }
            //}
            service += "<tr>" +
                            "<td style='font-family: Arial, sans-serif;font-size: 15px; border: 1px solid #000;width: 80%; height: 28px;'></td>" +
                            "<td style='font-family: Arial, sans-serif;font-size: 15px; border: 1px solid #000;width: 20%; height: 28px;'></td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td style='font-family: Arial, sans-serif;font-size: 15px; border: 1px solid #000;width: 80%; height: 28px;'></td>" +
                            "<td style='font-family: Arial, sans-serif;font-size: 15px; border: 1px solid #000;width: 20%; height: 28px;'></td>" +
                        "</tr>";
            service += "</tbody></table>";

            // gen product(sản phẩm)
            string product = "<table style='border-collapse: collapse;width: 100%;margin-top: 5px;'>" +
                                "<thead>" +
                                    "<tr>" +
                                        "<th style='font-family: Arial, sans-serif;font-size: 15px; border: 1px solid #000;width: 80%; padding: 5px; font-weight:normal;'>Sản phẩm</th>" +
                                        "<th style='font-family: Arial, sans-serif;font-size: 15px; border: 1px solid #000;width: 20%; padding: 5px; font-weight: normal;'>SL</th>" +
                                    "</tr>" +
                                "</thead>" +
                                "<tbody>";
            if (ProductList != null && ProductList.Count > 0)
            {
                var totalMoney = 0;
                foreach (var v in ProductList)
                {
                    totalMoney += v.Price * v.Quantity;
                    product += "<tr>" +
                                    "<td style='font-family: Arial, sans-serif;font-size: 15px; border: 1px solid #000;width: 80%; padding: 5px;'>" + v.Name + "</td>" +
                                    "<td style='font-family: Arial, sans-serif;font-size: 15px; border: 1px solid #000;width: 20%; padding: 5px; text-align: right;'>" + v.Quantity.ToString() + "</td>" +
                                "</tr>";
                }
                // Render stylist, skinner
                //CultureInfo elGR = CultureInfo.CreateSpecificCulture("el-GR");
                //items.Add(new KeyValuePair<string, string>("Thành tiền", String.Format(elGR, "{0:0,0}", totalMoney) + " VNĐ"));
            }
            //else
            //{
            //    for (var i = 0; i < 3; i++)
            //    {
            //        product += "<tr>" +
            //                        "<td style='font-family: Arial, sans-serif;font-size: 15px; border: 1px solid #000;width: 80%; height: 28px;'></td>" +
            //                        "<td style='font-family: Arial, sans-serif;font-size: 15px; border: 1px solid #000;width: 20%; height: 28px;'></td>" +
            //                    "</tr>";
            //    }
            //}
            product += "<tr>" +
                            "<td style='font-family: Arial, sans-serif;font-size: 15px; border: 1px solid #000;width: 80%; height: 28px;'></td>" +
                            "<td style='font-family: Arial, sans-serif;font-size: 15px; border: 1px solid #000;width: 20%; height: 28px;'></td>" +
                        "</tr>" +
                        "<tr>" +
                            "<td style='font-family: Arial, sans-serif;font-size: 15px; border: 1px solid #000;width: 80%; height: 28px;'></td>" +
                            "<td style='font-family: Arial, sans-serif;font-size: 15px; border: 1px solid #000;width: 20%; height: 28px;'></td>" +
                        "</tr>";
            product += "</tbody></table>";

            // gen promotion (các gói phụ trợ)
            var promotion = "";
            var promotionList = getPromotion(bill);
            if (promotionList.Count > 0)
            {
                foreach (var v in promotionList)
                {
                    promotion += "<div style='width: 50%; float: left; font-family: Arial, sans-serif;font-size: 15px; font-style: italic; margin-bottom: 0px; margin-top: 5px;' > " +
                                    "<div style='width: 12px; height: 12px; background: url(http://ql.30shine.com/Assets/images/checkbox3_12x12.png); margin-top: 2px; margin-right: 7px; float:left;'></div>" + v.Name +
                                "</div>";
                }
            }
                       

            // gen .print-wp style by printer
            string style = "";
            style += "width: 225px; padding-left: 15px;";
            //switch (SalonId)
            //{
            //    case 1:
            //        style += "width: 225px; padding-left: 15px;";
            //        break;
            //    case 2:
            //        style += "width: 225px; padding-left: 15px;";
            //        break;
            //    case 3:
            //        style += "width: 225px; padding-left: 15px;";
            //        break;
            //    case 4:
            //        style += "width: 225px; padding-left: 15px;";
            //        break;
            //    default:
            //        style += "width: 215px;";
            //        break;
            //}

            // gen htmlContent
            string htmlContent = "<html><head><meta charset='utf-8' /></head><body>";
            htmlContent += "<div class='print-wp' style='" + style + "'>";
            htmlContent += "<div class='logo' style='width: 100%; text-align: center; padding: 0; margin-bottom: 5px;'>" +
                                "<img class='prt-logo' src='http://ql.30shine.com/Assets/images/logo.jpg' style='width: 100px;' />" +
                            "</div>" +
                            "<div style='width: 100%; font-family: Arial, sans-serif;font-size: 15px; font-style: italic; margin-bottom: 2px;'>Ngày  " + String.Format("{0:dd/MM/yyyy}", DateTime.Now) + " - " + String.Format("{0:HH}", DateTime.Now) + "h" + String.Format("{0:mm}", DateTime.Now) + "</div>" +
                            /// bill code
                            "<div style='width: 100%; font-family: Arial, sans-serif;font-size: 20px;'>CODE  <b style='font-size: 20px;'>" + UIHelpers.getBillOrder(bill.BillCode, 4) + "</b>" + (bill.IsX2 == true ? " - Tăng ca" : "") + "</div>" +
                            /// customer
                            "<table style='width: 100%;margin-top: 3px;'>" +
                                "<tbody>" +
                                    "<tr>" +
                                        "<td style='font-family: Arial, sans-serif; font-size: 18px; min-width: 30px;'>KH</td>" +
                                        "<td style='font-family: Arial, sans-serif; font-size: 18px;'>" +
                                            " <b style='font-family: Arial, sans-serif; font-size: 15px; text-transform: uppercase; '>" + bill.CustomerName.ToUpper() + "</b>" +
                                        "</td>" +
                                    "</tr>" +
                                    "<tr>" +
                                        "<td style='font-family: Arial, sans-serif;font-size: 18px; min-width: 30px;'>Số ĐT</td>" +
                                        "<td style='font-family: Arial, sans-serif;font-size: 18px;'> " + bill.cusPhone + "</td>" +
                                    "</tr>" +
                                    //"<tr>" +
                                    //    "<td style='font-family: Arial, sans-serif;font-size: 16px; min-width: 30px;'>Số DV</td>" +
                                    //    "<td style='font-family: Arial, sans-serif;font-size: 16px;'> " + times + "</td>" +
                                    //"</tr>" +
                                    (
                                        bill.HourFrame != null ? "<tr>" +
                                                                    "<td style='font-family: Arial, sans-serif;font-size: 16px; min-width: 30px;'>Giờ book</td>" +
                                                                    "<td style='font-family: Arial, sans-serif;font-size: 16px;'> " + bill.HourFrame + "</td>" +
                                                                "</tr>" : ""
                                    ) +
                                    (
                                       bill.StylistBookName != null ? "<tr>" +
                                                                    "<td style='font-family: Arial, sans-serif;font-size: 16px; min-width: 30px;'>Stylist</td>" +
                                                                    "<td style='font-family: Arial, sans-serif;font-size: 16px;'> " + bill.StylistBookName + "</td>" +
                                                                "</tr>" : ""
                                    ) +
                                "</tbody>" +
                            "</table>" +
                            /// service
                            service +
                            /// promotion (gói phụ trợ)
                            promotion +
                            /// stylist
                            "<table style='width: 100%;'>" +
                                "<tbody>" +
                                    "<tr>" +
                                        "<td style='font-family: Arial, sans-serif;font-size: 15px; padding: 5px 5px 0px 0px;'>Stylist&nbsp;&nbsp;&nbsp;</td>" +
                                        "<td style='font-family: Arial, sans-serif;font-size: 15px; padding: 5px 5px 0px 0px; text-align: right;'>" +
                                            "<b style='font-family: Arial, sans-serif;font-size: 15px; font-weight: normal;'>" + bill.stylistName + "</b>" +
                                        "</td>" +
                                    "</tr>" +
                                    "<tr>" +
                                        "<td style='font-family: Arial, sans-serif;font-size: 15px; padding: 2px 5px 0px 0px;'>Skinner&nbsp;</td>" +
                                        "<td style='font-family: Arial, sans-serif;font-size: 15px; padding: 2px 5px 0px 0px; text-align: right;'>" + bill.skinnerName + "</td>" +
                                    "</tr>" +
                                    "<tr>" +
                                        "<td style='font-family: Arial, sans-serif;font-size: 15px; padding: 2px 5px 0px 0px;'>Checkin&nbsp;</td>" +
                                        "<td style='font-family: Arial, sans-serif;font-size: 15px; padding: 2px 5px 0px 0px; text-align: right;'>" + bill.checkinName + "</td>" +
                                    "</tr>" +
                                "</tbody>" +
                            "</table>" +
            // mini item
            //"<div style='width: 50%; float: left; font-family: Arial, sans-serif;font-size: 15px; font-style: italic; margin-bottom: 2px;'>" +
            //    "<div style='width: 12px; height: 12px; border: 1px solid #000; margin-top: 2px; margin-right: 7px; float:left;'></div> Cạo Mặt" +
            //"</div>" +
            /// Phụ trợ : Trị gầu, Dưỡng tóc, Ngăn rụng tóc
            //"<div style='width: 50%; float: left; font-family: Arial, sans-serif;font-size: 15px; font-style: italic; margin-bottom: 2px;'>" +
            //    "<div style='width: 12px; height: 12px; border: 1px solid #000; margin-top: 2px; margin-right: 7px; float:left;'></div> Trị gầu" +
            //"</div>" +
            //"<div style='width: 50%; float: left; font-family: Arial, sans-serif;font-size: 15px; font-style: italic; margin-bottom: 2px;'>" +
            //    "<div style='width: 12px; height: 12px; border: 1px solid #000; margin-top: 2px; margin-right: 7px; float:left;'></div> Dưỡng tóc" +
            //"</div>" +
            //"<div style='width: 50%; float: left; font-family: Arial, sans-serif;font-size: 15px; font-style: italic; margin-bottom: 2px;'>" +
            //    "<div style='width: 12px; height: 12px; border: 1px solid #000; margin-top: 2px; margin-right: 7px; float:left;'></div> Ngăn rụng tóc" +
            //"</div>" +
            //"<div style='width: 100%; font-family: Arial, sans-serif;font-size: 15px; font-style: italic; margin-bottom: 2px;'>" +
            //    "<div style='width: 12px; height: 12px; border: 1px solid #000; margin-top: 2px; margin-right: 7px; float:left;'></div> Lấy Ráy Tai" +
            //"</div>" +
            //"<div style='width: 100%; font-family: Arial, sans-serif;font-size: 15px; font-style: italic; margin-bottom: 2px;'>" +
            //    "<div style='width: 12px; height: 12px; border: 1px solid #000; margin-top: 2px; margin-right: 7px; float: left;'></div> Cắt Móng Tay" +
            //"</div>" +
            /// Sao đánh giá
            //                            @"<div style='width: 100%; float: left; padding: 5px 0 10px 0;'>
            //                                <div style='width: 15%; height: 20px; float: left; background: url(http://ql.30shine.com/Assets/images/star_20.png) center no-repeat; position: relative; top: -2px; text-align: right;'>:</div>
            //                                <div style='width: 15%; height: 20px; float: left; text-align: right;'>1</div>
            //                                <div style='width: 15%; height: 20px; float: left; text-align: right;'>2</div>
            //                                <div style='width: 15%; height: 20px; float: left; text-align: right;'>3</div>
            //                                <div style='width: 15%; height: 20px; float: left; text-align: right;'>4</div>
            //                                <div style='width: 15%; height: 20px; float: left; text-align: right;'>5</div>
            //                            </div>" +
            @"<div style='width: 100%; float: left; margin-top: 5px; margin-bottom: 5px;'>
                                <div style='width: 37%; float: left; font-family: Arial, sans-serif; padding-left: 3%;'>
                                    Đánh giá :
                                </div>
                                <div style='width: 60%; float: left;'>
                                    <div style='width: 100%; float: left; font-family: Arial, sans-serif;font-size: 15px; font-style: italic; margin-bottom: 2px;'>
                                        <div style='width: 12px; height: 12px; border: 1px solid #000; margin-top: 2px; margin-right: 7px; float:left;'></div> Rất hài lòng
                                    </div>
                                    <div style='width: 100%; float: left; font-family: Arial, sans-serif;font-size: 15px; font-style: italic; margin-bottom: 2px;'>
                                        <div style='width: 12px; height: 12px; border: 1px solid #000; margin-top: 2px; margin-right: 7px; float:left;'></div> Hài lòng
                                    </div>
                                    <div style='width: 100%; float: left; font-family: Arial, sans-serif;font-size: 15px; font-style: italic; margin-bottom: 2px;'>
                                        <div style='width: 12px; height: 12px; border: 1px solid #000; margin-top: 2px; margin-right: 7px; float:left;'></div> Chưa hài lòng
                                    </div>
                                </div>
                            </div>" +
                            /// product
                            product +
                            /// seller, total money
                            "<table style='font-family: Arial, sans-serif;font-size: 15px; width: 100%; margin-top: 5px;'>" +
                                "<tbody>" +
                                    "<tr>" +
                                        "<td style='font-family: Arial, sans-serif;font-size: 15px;'>Người bán </td>" +
                                        "<td>" + bill.sellerName + "</td>" +
                                    "</tr>" +
                                    "<tr>" +
                                        "<td style='font-family: Arial, sans-serif;font-size: 15px;'>Tổng số </td>" +
                                        "<td></td>" +
                                    "</tr>" +
                                "</tbody>" +
                            "</table>";

            htmlContent += "</body></html>";
            return htmlContent;
        }

        public class cls_flowPromotion : FlowPromotion
        {
            public string Name { get; set; }
        }

        public class cls_bill : BillService
        {
            /// <summary>
            /// Tên stylist
            /// </summary>
            public string stylistName { get; set; }
            /// <summary>
            /// Tên skinner
            /// </summary>
            public string skinnerName { get; set; }
            /// <summary>
            /// Tên người bán mỹ phẩm
            /// </summary>
            public string sellerName { get; set; }
            /// <summary>
            /// Tên nhân viên checkin đón khách
            /// </summary>
            public string checkinName { get; set; }
            /// <summary>
            /// Giờ book
            /// </summary>
            public string HourFrame { get; set; }
            /// <summary>
            /// Tên Stylist khách book
            /// </summary>
            public string StylistBookName { get; set; }
            public string cusPhone { get; set; }
        }
    }
}