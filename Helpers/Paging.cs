﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _30shine.Helpers
{
    public class Paging
    {
        public int _Page;
        public int _PageNumber;
        public int _PagingShowList = 5;
        public int _Segment = 10;
        public int _Offset;
        public int _TopNewsNum = 0;
        public int TotalPage;
        public int BoundLeft;
        public int BoundRight;
        public string Link;
        public List<ObjNumberPaging> PagingData = new List<ObjNumberPaging>();
        public ObjNumberPaging _ObjPaging = new ObjNumberPaging();
        public ObjNumberPaging _ObjPagingActive = new ObjNumberPaging();
        public ObjPaging _Paging;

        public ObjPaging Make_Paging()
        {
            ObjPaging MakePaging = new ObjPaging();
            MakePaging.ListPage = Make_ListNumberPaging();
            MakePaging.First = 0;
            MakePaging.Prev = _PageNumber - 1;
            MakePaging.Next = _PageNumber + 1;
            MakePaging.Last = TotalPage;
            MakePaging.Link = HttpContext.Current.Request.RawUrl.ToString();

            return MakePaging;
        }

        public List<ObjNumberPaging> Make_ListNumberPaging()
        {
            // Kiểm tra _Page truyền lên, page này sẽ active
            // Tạo miền xung quanh _Page và trả về trình duyệt

            double Alpha = (double)_PagingShowList / 2;
            double Even = _PagingShowList % 2;
            int Loop = Convert.ToInt32(Math.Ceiling(Alpha) - (Even == 0 ? 0 : 1));
            bool EvenSetLeft = false;
            int LoopLeft;
            int LoopRight;

            _ObjPagingActive.PageActive = true;
            _ObjPagingActive.PageNum = _PageNumber;

            if (_PageNumber <= _PagingShowList / 2)
            {
                var loop = TotalPage > _PagingShowList ? _PagingShowList : TotalPage;
                if (loop > 1)
                {
                    for (int i = 1; i <= loop; i++)
                    {
                        _ObjPaging.PageNum = i;
                        _ObjPaging.PageActive = i == _PageNumber ? true : false;
                        PagingData.Add(_ObjPaging);
                    }
                }
            }
            else
            {
                // Trường hợp đặc biệt khi chỉ hiển thị 1 hoặc 2 ô phân trang bên ngoài
                if (Alpha == 0.5)
                {
                    if (_ObjPaging.PageNum <= TotalPage)
                    {
                        PagingData.Add(_ObjPagingActive);
                    }
                }
                else if (Alpha == 1)
                {
                    _ObjPaging.PageActive = false;
                    _ObjPaging.PageNum = _PageNumber + 1;

                    if (_ObjPagingActive.PageNum <= TotalPage)
                    {
                        PagingData.Add(_ObjPagingActive);
                    }

                    if (_ObjPaging.PageNum <= TotalPage)
                    {
                        PagingData.Add(_ObjPaging);
                    }
                }
                // Các trường hợp khác hiển thị từ 3 ô trở lên
                else
                {
                    if (Even == 0)
                    {
                        if (EvenSetLeft)
                        {
                            LoopLeft = Loop - 1;
                            LoopRight = Loop;
                        }
                        else
                        {
                            LoopLeft = Loop;
                            LoopRight = Loop - 1;
                        }
                    }
                    else
                    {
                        LoopLeft = Loop;
                        LoopRight = Loop;
                    }

                    // Tạo miền bound trái
                    for (int i = LoopLeft; i >= 1; i--)
                    {
                        _ObjPaging.PageNum = _PageNumber - i;
                        _ObjPaging.PageActive = false;
                        if (_ObjPaging.PageNum <= TotalPage)
                        {
                            PagingData.Add(_ObjPaging);
                        }
                    }

                    // Add current page, set active
                    PagingData.Add(_ObjPagingActive);

                    // Tạo miền bound phải
                    for (int i = 1; i <= LoopRight; i++)
                    {
                        _ObjPaging.PageNum = _PageNumber + i;
                        _ObjPaging.PageActive = false;
                        if (_ObjPaging.PageNum <= TotalPage)
                        {
                            PagingData.Add(_ObjPaging);
                        }
                    }
                }

                BoundLeft = PagingData[0].PageNum;
                BoundRight = PagingData[PagingData.Count - 1].PageNum;
            }

            return PagingData;
        }
    }

    public struct ObjPaging
    {
        private int _First;
        private int _Prev;
        private List<ObjNumberPaging> _ListPage;
        private int _Next;
        private int _Last;
        private string _Link;
        public int First
        {
            get { return _First; }
            set { _First = value; }
        }
        public int Prev
        {
            get { return _Prev; }
            set { _Prev = value; }
        }
        public List<ObjNumberPaging> ListPage
        {
            get { return _ListPage; }
            set { _ListPage = value; }
        }
        public int Next
        {
            get { return _Next; }
            set { _Next = value; }
        }
        public int Last
        {
            get { return _Last; }
            set { _Last = value; }
        }
        public string Link
        {
            get { return _Link; }
            set { _Link = value; }
        }
    }

    public struct ObjNumberPaging
    {
        private int _PageNum;
        private bool _PageActive;
        public int PageNum
        {
            get { return _PageNum; }
            set { _PageNum = value; }
        }
        public bool PageActive
        {
            get { return _PageActive; }
            set { _PageActive = value; }
        }
    }
}