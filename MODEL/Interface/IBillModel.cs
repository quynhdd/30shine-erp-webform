﻿using _30shine.MODEL.ENTITY.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _30shine.MODEL.IO
{
    public interface IBillModel
    {
        /// <summary>
        /// Insert bản ghi [BillService]
        /// </summary>
        /// <param name="bill"></param>
        BillService Add(BillService bill);
        /// <summary>
        /// Cập nhật bản ghi [BillService]
        /// </summary>
        /// <param name="bill"></param>
        BillService Update(BillService bill);
        /// <summary>
        /// Xóa bản ghi [BillService]
        /// </summary>
        /// <param name="ID"></param>
        bool Delete(int ID);
        /// <summary>
        /// Lấy danh sách bản ghi [BillService] theo Salon
        /// </summary>
        /// <param name="timeFrom"></param>
        /// <param name="timeTo"></param>
        /// <param name="salonID"></param>
        /// <returns></returns>
        List<BillService> GetListBySalon(DateTime timeFrom, DateTime timeTo, int salonID);
        /// <summary>
        /// Lấy danh sách bản ghi [BillService] theo nhân viên
        /// </summary>
        /// <param name="timeFrom"></param>
        /// <param name="timeTo"></param>
        /// <param name="stylistID"></param>
        /// <returns></returns>
        List<BillService> GetListByStylist(DateTime timeFrom, DateTime timeTo, int staffID);
        bool CheckHourPartTime(int BookingId, Staff staff, DateTime WorkDate);
    }
}