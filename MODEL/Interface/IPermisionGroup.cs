﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using _30shine.Helpers;

namespace _30shine.MODEL.Interface
{
   public interface IPermisionGroup
    {
        Msg IsDuplicateGroupPermision(string namePermision);
        Msg AddGroupPermision(ENTITY.EDMX.PermissionErp obj);
        Msg ListGroupPermision();
    }
}
