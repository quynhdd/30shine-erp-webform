﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using _30shine.Helpers;
using _30shine.MODEL.ENTITY.EDMX;

namespace _30shine.MODEL.Interface
{
    interface IContract
    {
        Msg Update(Contract objContract);
        Msg Insert(Contract objContract);
        Msg Delete(int contractId);
        Msg Getlist(int contractType, int departmentId);
        Msg GetById(int contractId);
        Msg checkDuplicateContract(int departmentId, int contractType);
    }
}
