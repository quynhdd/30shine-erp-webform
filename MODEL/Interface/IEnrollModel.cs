﻿using _30shine.MODEL.ENTITY.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _30shine.MODEL.Interface
{
    interface IEnrollModel
    {
        FlowTimeKeeping Add(FlowTimeKeeping obj);
        FlowTimeKeeping Update(FlowTimeKeeping obj);
        bool Delete(int id);
        bool IsEnroll(int staffId, DateTime workDate, int? salonId = null);
    }
}
