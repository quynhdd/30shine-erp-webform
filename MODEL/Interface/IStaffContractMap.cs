﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using _30shine.Helpers;
using _30shine.MODEL.ENTITY.EDMX;
namespace _30shine.MODEL.Interface
{
    interface IStaffContractMap
    {
        Msg InsertMapContract(DateTime? contractDate, int contractType, int staffId);
        Msg Insert(StaffContractMap objContract);
        Msg Update(StaffContractMap objContract);
        Msg Delete(int Id);
        Msg GetById(int Id);
        Msg GetList(Expression<Func<StaffContractMap, bool>> expression);
        Msg Get(Expression<Func<StaffContractMap, bool>> expression);
        Msg UnActive(StaffContractMap obj);

    }
}
