﻿using _30shine.MODEL.ENTITY.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _30shine.MODEL.Interface
{
    public interface IDeviceModel
    {
        Device Add(Device obj);
        Device Update(Device obj);
        List<Library.Class.Cls_Device> ListDevice(int? Type);
    }
}
