﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using _30shine.MODEL.ENTITY.EDMX;

namespace _30shine.MODEL.Interface
{
    public interface IStaffFluctuationModel
    {
        Staff_Fluctuations AddOrUpdateRecruitment(Staff objStaff);
        Staff_Fluctuations UpdateStaff(Staff objStaff);
    }
}
