﻿using _30shine.MODEL.ENTITY.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _30shine.MODEL.Interface
{
    public interface ISalonServiceConfig
    {
        List<_30shine.MODEL.CustomClass.SalonServiceConfig> GetListByService(int ServiceId, int DepartmentId);
        List<_30shine.MODEL.CustomClass.SalonServiceConfig> GetListBySalon(int SalonId, int DepartmentId);
        int Add(ServiceSalonConfig record);
        int Update(ServiceSalonConfig record);
        int UpdatePublish(int Id, bool IsPublish);
    }
}
