﻿using _30shine.MODEL.ENTITY.EDMX;
using Amazon.Extensions.CognitoAuthentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace _30shine.MODEL.Interface
{
    public interface IAuthenticationModel
    {
        void SignUp(string username, string password);
        AuthFlowResponse GetAccessTokenUserForLogin(string UserName, string Password);
        Task<AuthFlowResponse> RefreshToken(string username, string idToken, string accessToken, string refreshToken);
        void ChangePassword(string token, string oldPassword, string newPassword);
        void AdminDeleteUser(string username);
        void AdminDisableUser(string username);
        void AdminEnableUser(string username);
        void AdminConfirmSignUp(string username);
        Tuple<User, Staff> GetUser(string username);
        Tuple<User, Staff> GetUserByStaffId(int Id);
        void AdminUpdateUserAttribute(string username, string code);
        string AdminGetAttribute(string username);
        User AddUser(int staffId, string userName, int isLogin, string email, string phone);
        void UpdateUser(int staffId, string email, string phone, byte isActive);
        User GetByUserName(string userName);
        User GetByEmail(string email);
        List<GUI.BackEnd.User.UserListing> GetList(int SalonId, int DepartmentId);
        bool CheckUserName(string userName, int staffId);
        bool CheckEmail(string email, int staffId);
        void AddMapUserStaff(User user, Staff staff);
        void DeleteOrDisableMapUserStaff(Staff employ, bool firstLogin = false, int type = 0);
        string GenPassword(MD5 md5Hash, string input);
        //
        bool CheckUserExistCognito(string userName);
    }
}
