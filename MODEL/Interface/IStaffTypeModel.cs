﻿using _30shine.MODEL.ENTITY.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _30shine.MODEL.IO
{
    interface IStaffTypeModel
    {
        Staff_Type GetByMeta(string Meta);
        Staff_Type GetById(int Id);
    }
}
