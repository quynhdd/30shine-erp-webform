﻿using _30shine.MODEL.ENTITY.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _30shine.MODEL.IO
{
    public interface IPayonConfigModel
    {
        /// <summary>
        /// Lấy danh sách bản ghi [Tbl_Payon]
        /// </summary>
        /// <returns>List<Tbl_Payon></returns>
        List<Tbl_Payon> GetList();
        /// <summary>
        /// Lấy giá trị cấu hình lương cho nhân viên
        /// </summary>
        /// <param name="staff">[Staff]</param>
        /// <param name="key">"salary_base" : lương cứng, "salary_parttime" : lương part-time, "salary_allowance" : lương phụ cấp, "salary_coefficient_rating" : hệ số cấu hình lương</param>
        /// <returns></returns>
        int GetValueByKey(Staff staff, string key);
    }
}
