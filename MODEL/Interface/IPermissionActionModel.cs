﻿using _30shine.MODEL.ENTITY.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace _30shine.MODEL.Interface
{
    public interface IPermissionActionModel
    {
        PermissionAction Add(PermissionAction record);
        PermissionAction Update(PermissionAction record);
        PermissionAction Delete(int id);
        PermissionAction GetById(int id);
        PermissionAction Load(Expression<Func<PermissionAction, bool>> expression);
        List<PermissionAction> GetList(Expression<Func<PermissionAction, bool>> expression);
    }
}
