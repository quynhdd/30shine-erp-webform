﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using _30shine.MODEL.ENTITY.EDMX;

namespace _30shine.MODEL.Interface
{
    interface IPermissionModel
    {
        string GetPermisionNames(int staffId);
        void DisposeConnectString();
        Regex GetRegexPageId();
        Regex GetRegexPageIdSpecial();
        bool CheckPermisionByAction(string actionName, string strPageId, int staffId);
        string GetMenuV3(int staffId);
        List<Tbl_Permission> GetListPermission();
        Tbl_Permission GetPermissionById(int id);
        Tbl_Permission GetPermissionByName(string permissionName);
        List<Tbl_Permission_Action> ListAction();
        bool GetActionByActionNameAndPageId(string actionName,string pageID,string permission);
        string GetMenu(int pId);
        string GetMenu_V2(int pId);
        /// <summary>
        /// Lấy tất cả dữ liệu trong bảng Menu
        /// </summary>
        /// <param name="parentId"></param>
        /// <returns></returns>
       // List<Store_Permission_TreeView_Menu_Result> GetAllMenu(int parentId);
        /// <summary>
        /// Lấy Module trong bảng Menu
        /// </summary>
        /// <returns></returns>
        List<Tbl_Permission_Menu> GetModule();
    }
}
