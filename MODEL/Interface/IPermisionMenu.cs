﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using _30shine.Helpers;

namespace _30shine.MODEL.Interface
{
    interface IPermisionMenu
    {
        Msg Update(ENTITY.EDMX.PermissionMenu obj);
        Msg Add(ENTITY.EDMX.PermissionMenu obj);
        Msg Delete(int id);
        Msg GetById(int id);
        Msg GetList(int MenuId);
        Msg GetList();
        bool CheckDuplicatePageId(string pageId);
    }
}
