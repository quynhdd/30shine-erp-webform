﻿using _30shine.MODEL.ENTITY.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace _30shine.MODEL.Interface
{
    public interface IOrderRecruitingStaffModel
    {
        List<OrderRecruitingStaff> GetList(Expression<Func<OrderRecruitingStaff, bool>> expression);
        void Add(List<_30shine.GUI.BackEnd.TuyenDungV2.OrderRecruitmentStaffSub> list, int creatorId);
    }
}
