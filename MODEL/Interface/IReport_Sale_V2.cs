﻿using _30shine.MODEL.ENTITY.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _30shine.MODEL.Interface
{
    interface IReport_Sale_V2
    {
        List<Tbl_Salon> ListSalon();
        List<CustomClass.Report_Sales_V2.BCVH> BCVH(DateTime timeFrom, DateTime timeTo, int salonId);
        List<CustomClass.Report_Sales_V2.BC_KQKD> BC_KQKD(DateTime timeFrom, DateTime timeTo);
        List<CustomClass.Report_Sales_V2.BCVH> GetDataToday_BCVH(int SalonId, DateTime timeFrom, DateTime timeTo);
        List<CustomClass.Report_Sales_V2.BCVH> GetDataToday_BCVH_v2(int SalonId, DateTime timeFrom, DateTime timeTo);
        List<CustomClass.Report_Sales_V2.BC_KQKD> GetDataToday_BCKQKD(DateTime timeFrom, DateTime timeTo);
        

    }
}
