﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.CustomClass;
using _30shine.Helpers;
namespace _30shine.MODEL.IO
{
    public interface IStaffMistakeModel
    {
        /// <summary>
        /// Add Staff_Mistake
        /// </summary>
        Staff_Mistake Add(Staff_Mistake obj);
        /// <summary>
        /// Update Staff_Mistake
        /// </summary>
        Staff_Mistake Update(Staff_Mistake obj);
        /// <summary>
        /// GetList StaffMistake
        /// </summary>
        /// <returns></returns>
        List<StaffMistakeClass> GetList(DateTime timeForm, DateTime timeTo, int salonId, int TypeId, int FormMissTakeId);
        /// <summary>
        /// Get List theo Id
        /// </summary>
        StaffMistakeClass GetListId(int id);
        List<StaffMistake5CClass> GetList5C(int _salonId, DateTime? _workDate);
        Msg AddOrUpdate5C(SalaryIncomeChange _obj5C);
        Msg AddOrUpdateSalaryIncome(int Point, int StaffId, DateTime WorkDate, int Salon_Id);



    }
}
