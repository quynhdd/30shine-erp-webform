﻿using _30shine.MODEL.ENTITY.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _30shine.MODEL.IO
{
    interface ISalaryModel
    {
        FlowSalary Add(FlowSalary obj);
        FlowSalary Update(FlowSalary obj);
        bool Delete(FlowSalary obj);
        List<FlowSalary> GetListFlowSalary();
        /// <summary>
        /// Lấy bản ghi [FlowSalary] theo ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        FlowSalary GetFlowSalaryById(int id);
        /// <summary>
        /// Lấy danh sách bản ghi [FlowSalary] cho nhân viên theo khoảng thời gian
        /// </summary>
        /// <param name="staff"></param>
        /// <param name="timeFrom"></param>
        /// <param name="timeTo"></param>
        /// <returns></returns>
        List<FlowSalary> GetFlowSalaryByStaffDate(Staff staff, DateTime timeFrom, DateTime timeTo);
        /// <summary>
        /// Lấy bản ghi [FlowSalary] theo ngày cho nhân viên
        /// </summary>
        /// <param name="staff"></param>
        /// <param name="sDate"></param>
        /// <returns></returns>
        FlowSalary GetFlowSalaryByStaffDate(Staff staff, DateTime sDate);
        /// <summary>
        /// Khởi tạo dòng lương statistic hàng ngày, tạo các bản ghi [FlowSalary] cho nhân viên
        /// Nhân viên yêu cầu điểm danh, thì khi chấm công sẽ được tạo luôn 1 bản ghi statistic lương [FlowSalary] cho ngày được chấm công. 
        /// Nếu công đoạn chấm công ghi lỗi thì sẽ được ghi tiếp ở đây.
        /// Nhân viên không yêu cầu điểm danh sẽ được ghi tự động bản ghi [FlowSalary] theo method này
        /// </summary>
        void InitFlowSalaryByDay();
        /// <summary>
        /// Cập nhật bản ghi lương [FlowSalary] cho các nhân viên tham gia vào bill
        /// Nhân viên tham gia vào bill gồm có:
        /// 1. Stylist (Field Staff_Hairdresser_Id)
        /// 2. Skinner (Field Staff_HairMassage_Id)
        /// 3. Checkin (Field ReceptionId)
        /// 4. Người bán mỹ phẩm (Field SellerId)
        /// </summary>
        void UpdateFlowSalaryByBill();
    }
}
