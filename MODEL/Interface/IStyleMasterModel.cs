﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using _30shine.MODEL.ENTITY.EDMX;

namespace _30shine.MODEL.Interface
{
    interface IStyleMasterModel
    {
        List<StyleMaster_Status> ListStatus();
        List<StyleMaster> ListStyleMaster(DateTime dateTime, int status);
        object StyleMaster(int id);
        bool Delete(int Id);
        bool Approve(int id, int userId, int statusId);
    }
}
