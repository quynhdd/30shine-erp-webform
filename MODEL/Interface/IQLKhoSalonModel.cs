﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.CustomClass;

namespace _30shine.MODEL.Interface
{
    interface IQLKhoSalonModel
    {
        List<QLKhoSalonClass> GetListNoSalon(DateTime TimeFrom, DateTime TimeTo, int SalonId);
    }
}
