﻿using _30shine.MODEL.ENTITY.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using _30shine.MODEL;
using System.Web.Services;
using _30shine.MODEL.CustomClass;
using _30shine.Helpers;
namespace _30shine.MODEL.IO
{
    public interface IStaffModel
    {
        Msg GetSalonShortName(int salonId);
        Msg GetPathContractTemp(int DepartmentId, int TypeContract);
        Staff Add(Staff obj);
        Staff Update(Staff obj);
        Staff Delete(int id);
        List<BO.StaffModel.OutputStaffListing> GetListStaff(DateTime timefrom, DateTime timeto, List<int> listSalonId, int staffTypeId, int skillLevelId, int active, int snDay, int snMonth, string filterEmail, int permission, int contract, int profile);
        List<StaffMistake5CClass> GetList5CStaffBySalon(int salonId, int _DepartmentId, DateTime WorkDate);
        Staff GetStaffById(int id);
        Staff GetByOrderCode(string orderCode);
        bool IssetPhone(string phone, Staff staff = null);
        bool IssetEmail(string email, Staff staff = null);
        bool IssetPersonId(string personId, Staff staff = null);
        bool IssetBankAccount(string bankAccount, Staff staff = null);
        bool IssetTaxCode(string taxCode, Staff staff = null);
        bool IssetInsurranceNumber(string insurNumber, Staff staff = null);
        float GetStaffKcsById(DateTime timeFrom, DateTime timeTo, int salonId, int stylistId);
        int GetStaffErrorCountId(DateTime timeFrom, DateTime timeTo, int salonId, int stylistId);
        List<Staff> GetListStaffEnrollBySalon(int salonId, DateTime workDate);
        List<Staff> ListGiaoVienS4M(int typeGV);
        List<Staff_Type> ListDepartment();
        TuyenDung_UngVien GetDataRecruitment(int Id);
        string BindHairStyle(string timeFrom, string timeTo, int StylisId);
        object GetDataFiles(DateTime date, List<int> salonIds);
    }
}
