﻿using _30shine.MODEL.ENTITY.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _30shine.MODEL.IO
{
    interface IEmploymentUserTestModel
    {
        /// <summary>
        /// Thêm mới bản ghi [TuyenDung_NguoiTest]
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        TuyenDung_NguoiTest Add(TuyenDung_NguoiTest obj);
        /// <summary>
        /// Cập nhật bản ghi [TuyenDung_NguoiTest]
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        TuyenDung_NguoiTest Update(TuyenDung_NguoiTest obj);
        /// <summary>
        /// Xóa bản ghi [TuyenDung_NguoiTest]
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        bool Delete(int id);
        /// <summary>
        /// Lấy danh sách người test
        /// </summary>
        /// <returns></returns>
        List<TuyenDung_NguoiTest> GetListUser();
        /// <summary>
        /// Lấy bản ghi [TuyenDung_NguoiTest] theo ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        TuyenDung_NguoiTest GetUserByID(int id);
    }
}
