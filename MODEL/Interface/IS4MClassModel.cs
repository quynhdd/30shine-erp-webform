﻿using System.Collections.Generic;
using _30shine.MODEL.ENTITY.EDMX;

namespace _30shine.MODEL.Interface
{
    interface IS4MClassModel
    {
        List<store_list_Stylist4Men_Class_Result> AllList();
        Stylist4Men_Class Add(Stylist4Men_Class obj);
        int? Update(Stylist4Men_Class obj);
        Stylist4Men_Class GetById(int? id);

    }
}
