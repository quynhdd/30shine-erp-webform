using _30shine.GUI.BackEnd.Infrastructure;
using _30shine.MODEL.ENTITY.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace _30shine.MODEL.Interface
{
    public interface IOrderDeviceBorkenHandlingModel
    {
        void Add(List<OrderBrokenDeviceHandlingSub> list, int creatorId);
        List<ProgressCompleteData> GetListProgression(DateTime TimeFrom, DateTime TimeTo, List<int> SalonId);
        List<Progression> GetProgression(DateTime TimeFrom, DateTime TimeTo, string region, List<int> salon);
        List<OrderBrokenDeviceHandling> GetList(Expression<Func<OrderBrokenDeviceHandling, bool>> expression);
    }
}
