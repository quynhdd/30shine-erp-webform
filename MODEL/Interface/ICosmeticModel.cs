﻿using _30shine.MODEL.ENTITY.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _30shine.MODEL.Interface
{
    interface ICosmeticModel
    {
       string GenBillCode(int salonId, string prefix, DateTime OrderDate, int template);
       string Add(int salonId,DateTime OrderDate);
    }
}
