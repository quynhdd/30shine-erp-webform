﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static _30shine.MODEL.CustomClass.AccountModelClass;

namespace _30shine.MODEL.Interface
{
    interface IAccountModel
    {
        AccountSession AccountInfor { get; set; }
        void Login(string account, string password);
        void Logout();
        AccountSession GetAccountInfor();
        void SetAccountInfor(AccountSession obj);
        bool IsAccountLogin();
    }
}