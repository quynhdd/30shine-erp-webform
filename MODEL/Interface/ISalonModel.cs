﻿using _30shine.MODEL.ENTITY.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _30shine.MODEL.IO
{
    public interface ISalonModel
    {
        /// <summary>
        /// Lấy toàn bộ danh sách bản ghi [Tbl_Salon], bao gồm cả salon chính và salon hội quán
        /// </summary>
        /// <returns>List<Tbl_Salon></returns>
        List<Tbl_Salon> GetList();
        /// <summary>
        /// Lấy danh sách salon chính
        /// </summary>
        /// <returns>List<Tbl_Salon></returns>
        List<Tbl_Salon> GetListCapital();
        /// <summary>
        /// Lấy danh sách salon hội quán
        /// </summary>
        /// <returns>List<Tbl_Salon></returns>
        List<Tbl_Salon> GetListHQ();
        /// <summary>
        /// Lấy bản ghi [Tbl_Salon] theo Id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns>Tbl_Salon|null</returns>
        Tbl_Salon GetItemByID(int id);
        // Get salon by short name
        Tbl_Salon GetByShortName(string sortName);
    }
}
