﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using _30shine.MODEL.ENTITY.EDMX;
namespace _30shine.MODEL.Interface
{
   public interface IStylist4MenTuitionModel
    {
        Stylist4Men_Tuition Add(Stylist4Men_Tuition obj);
        Stylist4Men_Tuition Update(Stylist4Men_Tuition obj);
    }
}
