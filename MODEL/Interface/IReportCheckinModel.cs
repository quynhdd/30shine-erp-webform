﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using _30shine.MODEL.ENTITY.EDMX;
using static Library.Class;

namespace _30shine.MODEL.Interface
{
    interface IReportCheckinModel
    {
        List<Cls_checkin> ListCheckin(string timeFrom, string timeTo, int SalonId, int Status, int StaffId);
    }
}
