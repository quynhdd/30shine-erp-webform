﻿using _30shine.MODEL.ENTITY.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _30shine.MODEL.IO
{
    interface ISkillLevelModel
    {
        Tbl_SkillLevel GetByName(string Name);
        Tbl_SkillLevel GetById(int Id);
    }
}
