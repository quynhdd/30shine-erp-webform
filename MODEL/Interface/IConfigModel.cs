﻿using _30shine.MODEL.ENTITY.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _30shine.MODEL.IO
{
    public interface IConfigModel
    {
        /// <summary>
        /// Lấy danh sách bản ghi cấu hình [Tbl_Config]
        /// </summary>
        /// <returns>List<Tbl_Config></returns>
        List<Tbl_Config> GetList();
        /// <summary>
        /// Lấy giá trị cấu hình theo [key]
        /// </summary>
        /// <param name="key"></param>
        /// <returns>string|null</returns>
        string GetValueByKey(string key);
    }
}
