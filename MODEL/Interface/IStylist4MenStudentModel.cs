﻿using System.Collections.Generic;
using _30shine.MODEL.ENTITY.EDMX;
using System;

namespace _30shine.MODEL.Interface
{
    public interface IStylist4MenStudentModel
    {
        Stylist4Men_Student Add(Stylist4Men_Student obj);
        Stylist4Men_Student Update(Stylist4Men_Student obj);
        Stylist4Men_Student UpdateNumberOfCreateProfile(Stylist4Men_Student obj);
        Stylist4Men_Student Delete(int Id);
        List<Stylist4Men_Student> GetListStudent();
        Stylist4Men_Student IssetPhoneNumer(string value);
        List<Store_Stylist4Men_HocVienWhereSalonAndClass_V1_Result> GetByAllListStudent(DateTime timeFrom, DateTime timeTo, int ClassId, int levelOfConcern, int numberOfCreateProfile);
        Stylist4Men_Student GetById(int Id);
        bool IssetEmail(string value);
        Stylist4Men_Student GetRecordNew();
       

    }
}
