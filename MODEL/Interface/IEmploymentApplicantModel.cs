﻿using _30shine.MODEL.ENTITY.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _30shine.MODEL.Interface
{
    interface IEmploymentApplicantModel
    {
        TuyenDung_UngVien Add(TuyenDung_UngVien obj);
        TuyenDung_UngVien Update(TuyenDung_UngVien obj);
        bool Delete(int id);
        TuyenDung_UngVien GetApplicantById(int id);
    }
}
