﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using _30shine.MODEL.ENTITY.EDMX;
namespace _30shine.MODEL.Interface
{
    public interface ITblStatusModel
    {
        List<Tbl_Status> GetList();
        List<Tbl_Status> Getname(int value);
        List<Tbl_Status> GetList_Staff();
    }
}
