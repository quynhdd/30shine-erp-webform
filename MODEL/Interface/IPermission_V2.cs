﻿using _30shine.MODEL.ENTITY.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _30shine.MODEL.Interface
{
    interface IPermission_V2
    {
        List<Tbl_Permission> GetListPermission();
        Tbl_Permission GetPermissionById(int id);
        Tbl_Permission GetPermissionByName(string permissionName);
        List<Tbl_Permission_Action> ListAction();
        //List<cls_Permission> GetPermission(int staffId);
        bool GetActionByActionNameAndPageId(string actionName, string pageId, int staffId);
        bool CheckAction(string actionName, string pageId, int staffId);
        List<Store_Permission_GetMenu_By_Staff_Result> GetMenu(int StaffId);
        string GetMenu_V2_2(int staffId);
        //string GetMenu_V2(int pId);
        //List<Store_Permission_TreeView_Menu_Result> GetAllMenu(int parentId);
        List<Tbl_Permission_Menu> GetModule();


    }
}
