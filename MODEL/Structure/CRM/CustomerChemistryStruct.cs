﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _30shine.MODEL.Structure.CRM
{
    /// <summary>
    /// Khách hóa chất
    /// </summary>
    public class CustomerChemistryStruct
    {
        /// <summary>
        /// Class dữ liệu khách hóa chất
        /// </summary>
        public class cls_khach_hoa_chat
        {
            public int CustomerId { get; set; }
            public string CustomerName { get; set; }
            public string CustomerPhone { get; set; }
            public int BillID { get; set; }
            public string HairMode { get; set; }
            public bool IsMessage { get; set; }
        }
    }
}