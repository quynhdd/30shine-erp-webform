﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Project.Model.Structure
{
    public class CommonClass
    {
        public class cls_message
        {
            public object data { get; set; }
            public string status { get; set; }
            public string message { get; set; }
            public bool success { get; set; }
        }
    }
}  