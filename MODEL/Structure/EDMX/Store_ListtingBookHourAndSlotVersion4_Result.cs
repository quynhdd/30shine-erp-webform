//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace _30shine.MODEL.Structure.EDMX
{
    using System;
    
    public partial class Store_ListtingBookHourAndSlotVersion4_Result
    {
        public Nullable<int> SalonId { get; set; }
        public Nullable<double> SlotALL { get; set; }
        public Nullable<int> CurrentOrder { get; set; }
        public Nullable<int> CurrentOrderNew { get; set; }
        public Nullable<int> CurrentOrderOld { get; set; }
        public Nullable<System.TimeSpan> HourFrame { get; set; }
        public string Hour { get; set; }
        public int Id { get; set; }
    }
}
