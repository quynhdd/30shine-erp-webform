//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace _30shine.MODEL.Structure.EDMX
{
    using System;
    using System.Collections.Generic;
    
    public partial class TinhThanh
    {
        public int ID { get; set; }
        public string TenTinhThanh { get; set; }
        public Nullable<int> VungMienID { get; set; }
        public Nullable<int> ThuTu { get; set; }
        public Nullable<int> TrangThai { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    }
}
