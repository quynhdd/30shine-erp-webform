//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace _30shine.MODEL.Structure.EDMX
{
    using System;
    
    public partial class Store_Temp_Checkin_Result
    {
        public int BillId { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> CompleteBillTime { get; set; }
        public Nullable<System.DateTime> InProcedureTime { get; set; }
        public Nullable<bool> IsBooking { get; set; }
        public Nullable<bool> IsPayByCard { get; set; }
        public Nullable<int> TotalMoney { get; set; }
        public Nullable<int> StylistId { get; set; }
        public Nullable<int> SkinnerId { get; set; }
        public Nullable<int> CheckinId { get; set; }
        public Nullable<int> SellerId { get; set; }
        public Nullable<int> CustomerId { get; set; }
        public string Images { get; set; }
        public Nullable<int> Mark { get; set; }
        public Nullable<int> BookingId { get; set; }
        public Nullable<int> WaitTimeInProcedure { get; set; }
        public Nullable<int> TotalCompleteTime { get; set; }
        public Nullable<int> ServiceId { get; set; }
        public string ServiceName { get; set; }
        public Nullable<int> ProductId { get; set; }
        public string ProductName { get; set; }
        public Nullable<int> Price { get; set; }
        public Nullable<int> Quantity { get; set; }
        public Nullable<int> VoucherPercent { get; set; }
        public Nullable<int> PromotionMoney { get; set; }
        public string StylistName { get; set; }
        public string SkinnerName { get; set; }
        public string CheckinName { get; set; }
        public string SellerName { get; set; }
        public string CustomerName { get; set; }
        public string CustomerPhone { get; set; }
        public string ListServices { get; set; }
        public string ListProducts { get; set; }
    }
}
