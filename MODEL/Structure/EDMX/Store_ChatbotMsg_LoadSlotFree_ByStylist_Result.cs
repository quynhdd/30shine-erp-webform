//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace _30shine.MODEL.Structure.EDMX
{
    using System;
    
    public partial class Store_ChatbotMsg_LoadSlotFree_ByStylist_Result
    {
        public int EnrollId { get; set; }
        public Nullable<int> SalonId { get; set; }
        public Nullable<System.DateTime> WorkDate { get; set; }
        public Nullable<int> StylistId { get; set; }
        public string StylistName { get; set; }
        public Nullable<int> GroupLevelId { get; set; }
        public Nullable<int> TeamId { get; set; }
        public Nullable<int> HourId { get; set; }
        public string HourFrameStr { get; set; }
    }
}
