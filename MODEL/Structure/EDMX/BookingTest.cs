//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace _30shine.MODEL.Structure.EDMX
{
    using System;
    using System.Collections.Generic;
    
    public partial class BookingTest
    {
        public int Id { get; set; }
        public string CustomerName { get; set; }
        public string CustomerPhone { get; set; }
        public Nullable<int> HourId { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<byte> IsDelete { get; set; }
        public Nullable<int> SalonId { get; set; }
        public Nullable<int> StylistId { get; set; }
        public Nullable<int> UserId { get; set; }
        public string Note { get; set; }
        public Nullable<System.DateTime> DatedBook { get; set; }
        public Nullable<bool> IsMakeBill { get; set; }
        public Nullable<int> FlowTimeKeepingId { get; set; }
        public Nullable<bool> IsSMS { get; set; }
        public Nullable<int> SMSStatus { get; set; }
        public Nullable<System.DateTime> SendDate { get; set; }
        public Nullable<int> OS { get; set; }
        public Nullable<bool> NewCustomer { get; set; }
        public string NoteDelete { get; set; }
    }
}
