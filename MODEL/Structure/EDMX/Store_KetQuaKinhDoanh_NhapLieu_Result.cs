//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace _30shine.MODEL.Structure.EDMX
{
    using System;
    
    public partial class Store_KetQuaKinhDoanh_NhapLieu_Result
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public Nullable<System.DateTime> CreatedTime { get; set; }
        public Nullable<System.DateTime> ModifiedTime { get; set; }
        public Nullable<bool> IsDelete { get; set; }
        public Nullable<bool> Publish { get; set; }
        public Nullable<int> Order { get; set; }
        public Nullable<long> Value { get; set; }
    }
}
