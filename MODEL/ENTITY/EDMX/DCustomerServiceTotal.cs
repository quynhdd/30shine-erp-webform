//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace _30shine.MODEL.ENTITY.EDMX
{
    using System;
    using System.Collections.Generic;
    
    public partial class DCustomerServiceTotal
    {
        public int Id { get; set; }
        public Nullable<int> CustomerId { get; set; }
        public Nullable<int> ServiceId { get; set; }
        public Nullable<int> BillId { get; set; }
        public Nullable<System.DateTime> DateLastOfBill { get; set; }
        public Nullable<int> Total { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<bool> IsDelete { get; set; }
    }
}
