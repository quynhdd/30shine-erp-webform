//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace _30shine.MODEL.ENTITY.EDMX
{
    using System;
    using System.Collections.Generic;
    
    public partial class Mobile
    {
        public int Id { get; set; }
        public Nullable<int> AndroidVersion { get; set; }
        public string IOSVersion { get; set; }
        public string AccessKey { get; set; }
        public string PrivateKey { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public bool IsDelete { get; set; }
        public string AppKey { get; set; }
        public Nullable<int> AppReview { get; set; }
    }
}
