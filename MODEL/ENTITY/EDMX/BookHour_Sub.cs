//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace _30shine.MODEL.ENTITY.EDMX
{
    using System;
    using System.Collections.Generic;
    
    public partial class BookHour_Sub
    {
        public int Id { get; set; }
        public int HourId { get; set; }
        public int SubHourId { get; set; }
        public string Hour { get; set; }
        public System.TimeSpan HourFrame { get; set; }
        public int SalonId { get; set; }
        public bool Publish { get; set; }
        public bool IsDelete { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    }
}
