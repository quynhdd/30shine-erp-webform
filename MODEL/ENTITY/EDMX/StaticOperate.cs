//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace _30shine.MODEL.ENTITY.EDMX
{
    using System;
    using System.Collections.Generic;
    
    public partial class StaticOperate
    {
        public int Id { get; set; }
        public Nullable<int> SalonId { get; set; }
        public Nullable<double> Stylist { get; set; }
        public Nullable<double> Skinner { get; set; }
        public Nullable<double> PerfectBill { get; set; }
        public Nullable<double> GoodBill { get; set; }
        public Nullable<double> BadBill { get; set; }
        public Nullable<double> OldCustomer { get; set; }
        public Nullable<double> BookingOnline { get; set; }
        public Nullable<double> BillOver15m { get; set; }
        public Nullable<double> BillUnder15m { get; set; }
        public Nullable<double> BillTimeNull { get; set; }
        public Nullable<double> BillHasImage { get; set; }
        public Nullable<double> ProfitByHour { get; set; }
        public Nullable<double> ProfitByDay { get; set; }
        public Nullable<double> ServiceByDay { get; set; }
        public Nullable<double> ServiceByHour { get; set; }
        public Nullable<System.DateTime> WorkDate { get; set; }
        public Nullable<System.DateTime> CreatedTime { get; set; }
        public Nullable<System.Guid> Uid { get; set; }
        public Nullable<byte> MigrateStatus { get; set; }
    }
}
