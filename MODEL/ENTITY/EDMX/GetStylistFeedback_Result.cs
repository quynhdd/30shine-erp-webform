//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace _30shine.MODEL.ENTITY.EDMX
{
    using System;
    
    public partial class GetStylistFeedback_Result
    {
        public Nullable<long> Rank { get; set; }
        public Nullable<int> StaffId { get; set; }
        public Nullable<double> Point { get; set; }
        public Nullable<int> BillErrors { get; set; }
        public Nullable<int> BillNotImages { get; set; }
        public string Fullname { get; set; }
        public Nullable<int> Total { get; set; }
    }
}
