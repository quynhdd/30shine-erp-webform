//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace _30shine.MODEL.ENTITY.EDMX
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_ListBack30Day
    {
        public int Id { get; set; }
        public string CusName { get; set; }
        public string CusPhone { get; set; }
        public Nullable<int> CusId { get; set; }
        public Nullable<bool> Call { get; set; }
        public string Note { get; set; }
        public Nullable<bool> Publish { get; set; }
        public Nullable<bool> IsDelete { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public Nullable<System.DateTime> ModyfiDate { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public Nullable<int> SalonId { get; set; }
        public Nullable<System.Guid> Uid { get; set; }
        public Nullable<byte> MigrateStatus { get; set; }
    }
}
