//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace _30shine.MODEL.ENTITY.EDMX
{
    using System;
    
    public partial class Store_Calculator_Money_Time_Result
    {
        public Nullable<int> STAFF_ID { get; set; }
        public string FULLNAME { get; set; }
        public Nullable<int> TOTAL_MONEY { get; set; }
        public Nullable<int> TOTAL_SLOT { get; set; }
        public Nullable<int> TOTAL_WORKHOUR { get; set; }
        public Nullable<int> AVG_MONEY { get; set; }
    }
}
