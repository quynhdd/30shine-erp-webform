//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace _30shine.MODEL.ENTITY.EDMX
{
    using System;
    
    public partial class Get_Status_Result
    {
        public int id { get; set; }
        public Nullable<bool> IsOld { get; set; }
        public Nullable<bool> IsBooked { get; set; }
        public Nullable<bool> IsCheckIn { get; set; }
        public Nullable<System.DateTime> DatedBook { get; set; }
        public Nullable<bool> IsServed { get; set; }
        public string SalonAddress { get; set; }
        public string SalonName { get; set; }
        public string StylistName { get; set; }
        public string PreviousSalon { get; set; }
        public string PreviousStylist { get; set; }
        public Nullable<int> PreviousStylistId { get; set; }
        public Nullable<int> CheckOutDayNumber { get; set; }
    }
}
