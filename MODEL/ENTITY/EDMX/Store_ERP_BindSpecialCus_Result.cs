//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace _30shine.MODEL.ENTITY.EDMX
{
    using System;
    
    public partial class Store_ERP_BindSpecialCus_Result
    {
        public int Id { get; set; }
        public Nullable<int> CustomerId { get; set; }
        public Nullable<int> CustomerTypeId { get; set; }
        public Nullable<bool> Publish { get; set; }
        public Nullable<int> Browse_ID { get; set; }
        public Nullable<bool> IsDelete { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<bool> IsSMS { get; set; }
        public Nullable<int> SMSStatus { get; set; }
        public Nullable<System.DateTime> SendDate { get; set; }
        public string Fullname { get; set; }
        public string Phone { get; set; }
        public string NhanVien { get; set; }
        public string LoaiLoi { get; set; }
        public string LstErrorStaff_Name { get; set; }
        public string LstErrorTypeCutReason { get; set; }
        public Nullable<System.DateTime> maxDate { get; set; }
        public string ReasonDiff { get; set; }
    }
}
