//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace _30shine.MODEL.ENTITY.EDMX
{
    using System;
    using System.Collections.Generic;
    
    public partial class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public Nullable<int> Type { get; set; }
        public Nullable<int> Price { get; set; }
        public Nullable<int> Old_Price { get; set; }
        public Nullable<byte> IsVoucher { get; set; }
        public Nullable<int> VoucherValue { get; set; }
        public string Description { get; set; }
        public string TechInfo { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<byte> Publish { get; set; }
        public Nullable<byte> IsDelete { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<int> VoucherPercent { get; set; }
        public Nullable<byte> Status { get; set; }
        public Nullable<int> Cost { get; set; }
        public Nullable<double> ForSalary { get; set; }
        public Nullable<int> CategoryId { get; set; }
        public Nullable<int> OnWebId { get; set; }
        public Nullable<int> BrandId { get; set; }
        public string Volume { get; set; }
        public string ModelName { get; set; }
        public string MapIdProduct { get; set; }
        public Nullable<int> PriceCombo { get; set; }
        public Nullable<bool> CheckCombo { get; set; }
        public Nullable<int> Order { get; set; }
        public Nullable<bool> CheckInventoryHC { get; set; }
        public Nullable<int> GroupProductId { get; set; }
        public Nullable<bool> IsCheckVatTu { get; set; }
        public Nullable<System.Guid> Uid { get; set; }
        public Nullable<byte> MigrateStatus { get; set; }
        public string BarCode { get; set; }
        public Nullable<int> MemberTypeAdd { get; set; }
        public Nullable<int> MemberAddDay { get; set; }
        public string InventoryType { get; set; }
        public int CoefficientOfWaitingDays { get; set; }
        public int StopBusiness { get; set; }
    }
}
