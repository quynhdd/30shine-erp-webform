//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace _30shine.MODEL.ENTITY.EDMX
{
    using System;
    
    public partial class Store_Video_Listting_and_Paging_Result
    {
        public Nullable<long> NUMBER { get; set; }
        public int Id { get; set; }
        public string Link { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Thumb { get; set; }
        public Nullable<long> ViewCount { get; set; }
        public Nullable<long> CommentCount { get; set; }
        public Nullable<long> DislikeCount { get; set; }
        public Nullable<long> LikeCount { get; set; }
        public Nullable<bool> IsDelete { get; set; }
        public Nullable<bool> Status { get; set; }
        public Nullable<System.DateTime> DateCreate { get; set; }
        public Nullable<System.DateTime> DateChanged { get; set; }
        public Nullable<System.DateTime> PublishDate { get; set; }
        public string VideoId { get; set; }
        public string Chanel { get; set; }
        public Nullable<System.Guid> Uid { get; set; }
        public Nullable<byte> MigrateStatus { get; set; }
    }
}
