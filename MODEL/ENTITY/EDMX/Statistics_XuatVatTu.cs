//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace _30shine.MODEL.ENTITY.EDMX
{
    using System;
    using System.Collections.Generic;
    
    public partial class Statistics_XuatVatTu
    {
        public int ID { get; set; }
        public int ExportGoods_ID { get; set; }
        public Nullable<int> TotalBill_XuatVatTu { get; set; }
        public Nullable<int> Staff_ID { get; set; }
        public Nullable<int> Salon_ID { get; set; }
        public Nullable<System.DateTime> Date_XuatVatTu { get; set; }
        public Nullable<System.DateTime> CreateTime { get; set; }
        public Nullable<System.DateTime> ModifiedTime { get; set; }
        public Nullable<bool> IsDelete { get; set; }
        public Nullable<int> Product_ID { get; set; }
        public Nullable<System.Guid> Uid { get; set; }
        public Nullable<byte> MigrateStatus { get; set; }
    }
}
