//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace _30shine.MODEL.ENTITY.EDMX
{
    using System;
    
    public partial class BaoCaoKinhDoanhS4M_Result
    {
        public Nullable<double> LNG { get; set; }
        public int Id { get; set; }
        public string SalonName { get; set; }
        public Nullable<double> GV_TienLuongNhanVien { get; set; }
        public Nullable<double> TongDoanhTthuMyPham { get; set; }
        public Nullable<double> CP_BH_QL_Khac { get; set; }
        public Nullable<double> CP_Marketing { get; set; }
        public Nullable<double> CP_IT { get; set; }
        public Nullable<double> CP_PhatSinh { get; set; }
    }
}
