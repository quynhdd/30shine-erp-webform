//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace _30shine.MODEL.ENTITY.EDMX
{
    using System;
    using System.Collections.Generic;
    
    public partial class StaticExpense
    {
        public int Id { get; set; }
        public Nullable<int> SalonId { get; set; }
        public string ShortName { get; set; }
        public Nullable<double> TotalSales { get; set; }
        public Nullable<double> TotalServiceProfit { get; set; }
        public Nullable<double> TotalCosmeticProfit { get; set; }
        public Nullable<double> ProductSalary { get; set; }
        public Nullable<double> TotalProductPrice { get; set; }
        public Nullable<double> TotalStaffSalary { get; set; }
        public Nullable<double> TotalProductUsed { get; set; }
        public Nullable<double> OtherExpense { get; set; }
        public Nullable<double> MKTExpense { get; set; }
        public Nullable<double> ITExpense { get; set; }
        public Nullable<double> DayExpense { get; set; }
        public Nullable<double> LNGAll30shine { get; set; }
        public Nullable<System.DateTime> WorkDate { get; set; }
        public Nullable<System.DateTime> CreatedTime { get; set; }
        public Nullable<System.Guid> Uid { get; set; }
        public Nullable<byte> MigrateStatus { get; set; }
        public Nullable<double> TotalIncomeAfterTax { get; set; }
        public Nullable<double> NumberOfTurns { get; set; }
        public Nullable<double> TotalTransactionPerCus { get; set; }
        public Nullable<double> TotalServicePerCus { get; set; }
        public Nullable<double> TotalProductPerCus { get; set; }
        public Nullable<double> DirectFee { get; set; }
        public Nullable<double> SalaryServicePerServiceIncome { get; set; }
        public Nullable<double> TotalProductCapital { get; set; }
        public Nullable<double> TotalPayOffKCS { get; set; }
        public Nullable<double> TotalDailyCostInventory { get; set; }
        public Nullable<double> ElectricityAndWaterBill { get; set; }
        public Nullable<double> SalonFee { get; set; }
        public Nullable<double> RentWithTax { get; set; }
        public Nullable<double> CapitalSpending { get; set; }
        public Nullable<double> TotalSMSExpenses { get; set; }
        public Nullable<double> ShippingExpend { get; set; }
        public Nullable<double> InternetAndPhoneBill { get; set; }
        public Nullable<double> SocialInsuranceAndFixedCost { get; set; }
        public Nullable<double> Tax { get; set; }
        public Nullable<double> IncomeTaxes { get; set; }
        public Nullable<double> SalonUnplannedSpending { get; set; }
        public Nullable<double> ManageFee { get; set; }
        public Nullable<double> OfficeRentAndServiceCose { get; set; }
        public Nullable<double> OfficeStaffSalary { get; set; }
        public Nullable<double> SalesSalary { get; set; }
        public Nullable<double> OfficeStaffSocialInsurance { get; set; }
        public Nullable<double> UnplannedSpending { get; set; }
        public Nullable<System.DateTime> ModifyTime { get; set; }
        public Nullable<double> Compensation { get; set; }
        public Nullable<double> TotalOtherIncome { get; set; }
        public double TotalSMDistributionToday { get; set; }
        public double TotalProductSM { get; set; }
        public double TotalSMDistributon { get; set; }
        public double TotalNetRevenue { get; set; }
        public double TotalSecurityCheckoutSalary { get; set; }
    }
}
