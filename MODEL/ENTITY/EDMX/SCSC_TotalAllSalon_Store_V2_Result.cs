//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace _30shine.MODEL.ENTITY.EDMX
{
    using System;
    
    public partial class SCSC_TotalAllSalon_Store_V2_Result
    {
        public int SalonId { get; set; }
        public string ShortName { get; set; }
        public Nullable<int> Order { get; set; }
        public int TongBill { get; set; }
        public Nullable<int> BillChuaDanhGia { get; set; }
        public Nullable<double> Bill_NotImg { get; set; }
        public Nullable<double> Percent_BillError { get; set; }
        public Nullable<double> Percent_BillImg_MoLech { get; set; }
        public Nullable<double> Percent_ErrorKCS { get; set; }
        public Nullable<double> PointSCSC_TB { get; set; }
        public int SalonID2 { get; set; }
        public int TotalShapeError { get; set; }
        public int TotalConnectiveError { get; set; }
        public int TotalSharpNessError { get; set; }
        public int TotalCompletionError { get; set; }
    }
}
