//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace _30shine.MODEL.ENTITY.EDMX
{
    using System;
    
    public partial class Store_S4MStudent_GetListPointHoiQuan_Result
    {
        public int StudentId { get; set; }
        public int StaffId { get; set; }
        public int DiemDuongNet { get; set; }
        public int DiemHinhKhoi { get; set; }
        public int DiemFade { get; set; }
        public int DiemSaySap { get; set; }
        public int DiemCaoTatoo { get; set; }
        public int DiemUon { get; set; }
        public int DiemNhuom { get; set; }
        public string Images { get; set; }
    }
}
