//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace _30shine.MODEL.ENTITY.EDMX
{
    using System;
    using System.Collections.Generic;
    
    public partial class FeedbackPointService
    {
        public int Id { get; set; }
        public Nullable<int> SalonId { get; set; }
        public Nullable<int> FeedbackQuantity { get; set; }
        public Nullable<System.DateTime> WorkDate { get; set; }
        public Nullable<bool> IsDelete { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    }
}
