//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace _30shine.MODEL.ENTITY.EDMX
{
    using System;
    using System.Collections.Generic;
    
    public partial class BackupTableServiceSalonConfig
    {
        public int Id { get; set; }
        public Nullable<int> SalonId { get; set; }
        public Nullable<int> ServiceId { get; set; }
        public Nullable<int> DepartmentId { get; set; }
        public Nullable<double> ServiceCoefficient { get; set; }
        public Nullable<double> ServiceBonus { get; set; }
        public Nullable<double> CoefficientOvertimeHour { get; set; }
        public double CoefficientOvertimeDay { get; set; }
        public System.DateTime BackupDate { get; set; }
        public Nullable<System.DateTime> CreatedTime { get; set; }
        public Nullable<System.DateTime> ModifiedTime { get; set; }
        public Nullable<bool> IsPublish { get; set; }
        public Nullable<bool> IsDelete { get; set; }
    }
}
