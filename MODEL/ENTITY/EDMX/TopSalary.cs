//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace _30shine.MODEL.ENTITY.EDMX
{
    using System;
    using System.Collections.Generic;
    
    public partial class TopSalary
    {
        public int id { get; set; }
        public int departmentId { get; set; }
        public int staffId { get; set; }
        public string staffName { get; set; }
        public string salonName { get; set; }
        public string skillLevel { get; set; }
        public double salary { get; set; }
        public int month { get; set; }
        public int year { get; set; }
        public System.DateTime createTime { get; set; }
        public Nullable<bool> isDelete { get; set; }
    }
}
