//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace _30shine.MODEL.ENTITY.EDMX
{
    using System;
    using System.Collections.Generic;
    
    public partial class OrderRecruitingStaff
    {
        public int Id { get; set; }
        public Nullable<int> RegionId { get; set; }
        public Nullable<int> SalonId { get; set; }
        public Nullable<int> DepartmentId { get; set; }
        public Nullable<int> CreatorId { get; set; }
        public Nullable<int> StaffId { get; set; }
        public Nullable<System.DateTime> StaffDate { get; set; }
        public Nullable<int> Type { get; set; }
        public Nullable<int> Quantity { get; set; }
        public Nullable<System.DateTime> Needed { get; set; }
        public Nullable<System.DateTime> Completed { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<int> ReasonRevoke { get; set; }
        public Nullable<int> CandidateId { get; set; }
        public string Description { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<bool> IsDelete { get; set; }
        public Nullable<bool> HavingTry { get; set; }
    }
}
