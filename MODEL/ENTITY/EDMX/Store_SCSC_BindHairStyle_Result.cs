//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace _30shine.MODEL.ENTITY.EDMX
{
    using System;
    
    public partial class Store_SCSC_BindHairStyle_Result
    {
        public Nullable<int> HairStyle_ID { get; set; }
        public string NameHair { get; set; }
        public Nullable<int> TotalHairStyle { get; set; }
        public Nullable<double> PointSCSC_TB { get; set; }
        public Nullable<double> Percent_KCS { get; set; }
        public Nullable<double> Total_NotImages { get; set; }
        public int TotalBillErrorShape { get; set; }
        public int TotalBillErrorSharpNess { get; set; }
        public int TotalBillErrorConnective { get; set; }
        public int TotalBillErrorCompletion { get; set; }
    }
}
