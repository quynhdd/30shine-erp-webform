//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace _30shine.MODEL.ENTITY.EDMX
{
    using System;
    using System.Collections.Generic;
    
    public partial class IvInventoryHistory
    {
        public int Id { get; set; }
        public Nullable<int> InventoryId { get; set; }
        public int ProductId { get; set; }
        public double Cost { get; set; }
        public Nullable<System.DateTime> AccountingDate { get; set; }
        public Nullable<int> Begin { get; set; }
        public Nullable<int> Import { get; set; }
        public Nullable<int> Export { get; set; }
        public Nullable<int> SellOrUse { get; set; }
        public Nullable<int> End { get; set; }
        public Nullable<double> Volume { get; set; }
        public Nullable<double> Quantify { get; set; }
        public Nullable<double> VolumeRemain { get; set; }
        public Nullable<int> SuggestOrder { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<bool> IsDelete { get; set; }
        public int UsedInService { get; set; }
        public int ExportToStaff { get; set; }
        public int ExportToMainWH { get; set; }
        public int ExportToSalon { get; set; }
        public int ImportFromPartner { get; set; }
        public int ImportFromSalon { get; set; }
        public int ImportFromStaff { get; set; }
        public int ImportFromReturn { get; set; }
        public int BeingTransported { get; set; }
        public int ImportFromMainWH { get; set; }
    }
}
