//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace _30shine.MODEL.ENTITY.EDMX
{
    using System;
    using System.Collections.Generic;
    
    public partial class IvInventory
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Type { get; set; }
        public Nullable<int> SalonId { get; set; }
        public Nullable<int> StaffId { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<bool> IsDelete { get; set; }
        public int ParentId { get; set; }
        public int DeliveryHoursAfterOrderDate { get; set; }
        public Nullable<int> RoutingId { get; set; }
        public Nullable<int> OrderRoutingID { get; set; }
    }
}
