//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace _30shine.MODEL.ENTITY.EDMX
{
    using System;
    using System.Collections.Generic;
    
    public partial class Stylist4Men_Tuition
    {
        public int Id { get; set; }
        public Nullable<int> StudentId { get; set; }
        public Nullable<int> AmountCollected { get; set; }
        public Nullable<int> PayTheMoney { get; set; }
        public Nullable<System.DateTime> CreaetedTime { get; set; }
        public Nullable<System.DateTime> ModifiledTime { get; set; }
        public Nullable<bool> Publish { get; set; }
        public Nullable<bool> IsDelete { get; set; }
        public Nullable<System.Guid> Uid { get; set; }
        public Nullable<byte> MigrateStatus { get; set; }
    }
}
