﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _30shine.MODEL.ENTITY.EDMX
{
    #region Staff
    public partial class Staff
    {
        public string StaffTypeName
        {
            get {
                using (var db = new Solution_30shineEntities())
                {
                    var str = "";
                    var obj = db.Staff_Type.FirstOrDefault(w=>w.Id == Type);
                    if (obj != null)
                    {
                        str = obj.Name;
                    }
                    return str;
                }
            }
        }

        public string SkillLevelName
        {
            get
            {
                using (var db = new Solution_30shineEntities())
                {
                    var str = "";
                    var obj = db.Tbl_SkillLevel.FirstOrDefault(w => w.Id == SkillLevel);
                    if (obj != null)
                    {
                        str = obj.Name;
                    }
                    return str;
                }
            }
        }

        public string SalonName
        {
            get
            {
                using (var db = new Solution_30shineEntities())
                {
                    var str = "";
                    var obj = db.Tbl_Salon.FirstOrDefault(w => w.Id == SalonId);
                    if (obj != null)
                    {
                        str = obj.Name;
                    }
                    return str;
                }
            }
        }

        public string CityName
        {
            get
            {
                using (var db = new Solution_30shineEntities())
                {
                    var str = "";
                    var obj = db.TinhThanhs.FirstOrDefault(w => w.ID == CityId);
                    if (obj != null)
                    {
                        str = obj.TenTinhThanh;
                    }
                    return str;
                }
            }            
        }

        public string DistrictName
        {
            get
            {
                using (var db = new Solution_30shineEntities())
                {
                    var str = "";
                    var obj = db.QuanHuyens.FirstOrDefault(w => w.ID == DistrictId);
                    if (obj != null)
                    {
                        str = obj.TenQuanHuyen;
                    }
                    return str;
                }
            }
        }

        public string LevelName { get; set; }

        //public string Images { get; set; }
        //public bool Publish { get; set; }
        //public bool CEO { get; set; }
        //public string About { get; set; }
        //public string Avatar { get; set; }
    }
    #endregion
}