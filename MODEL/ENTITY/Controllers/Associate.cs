﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _30shine.MODEL.ENTITY.EDMX
{
    #region BillService
    public partial class BillService
    {
        public string CustomerName
        {
            get;
            set;
        }

        public string CustomerPhone
        {
            get;
            set;
        }

        public int CustomerReturn
        {
            get;
            set;
        }

        public string ServiceCode
        {
            get;
            set;
        }

        public string ServiceName
        {
            get;
            set;
        }

        public int ServicePrice
        {
            get;
            set;
        }

        public string ProductNames
        {
            get;
            set;
        }

        public string ServiceNames
        {
            get;
            set;
        }

        public string HairdresserName
        {
            get;
            set;
        }

        public string HairMassageName
        {
            get;
            set;
        }

        public string CosmeticName
        {
            get;
            set;
        }

        public string ListImg
        {
            get;
            set;
        }
    }
    #endregion

    #region BillService
    public partial class Appointment
    {
        public string CustomerName
        {
            get;
            set;
        }

        public string CustomerPhone
        {
            get;
            set;
        }

        public int CustomerReturn
        {
            get;
            set;
        }

        public string ServiceCode
        {
            get;
            set;
        }

        public string ServiceName
        {
            get;
            set;
        }

        public int ServicePrice
        {
            get;
            set;
        }

        public string ProductNames
        {
            get;
            set;
        }

        public string ServiceNames
        {
            get;
            set;
        }

        public string HairdresserName
        {
            get;
            set;
        }

        public string HairMassageName
        {
            get;
            set;
        }

        public string CosmeticName
        {
            get;
            set;
        }

        public string ListImg
        {
            get;
            set;
        }
    }
    #endregion


    #region Staff
    public partial class Staff
    {
        public int Total_Hairdresser
        {
            get;
            set;
        }

        public int Total_HairMassage
        {
            get;
            set;
        }
    }
    #endregion

    #region Product
    public partial class Product
    {
        public int Quantity
        {
            get;
            set;
        }
        public int Promotion
        {
            get;
            set;
        }
    }
    #endregion

    #region Service
    public partial class Service
    {
        public int Quantity
        {
            get;
            set;
        }
    }
    #endregion

    #region Customer
    public partial class Customer
    {
        public string CityName
        {
            get
            {
                string str = "";
                using (var db = new Solution_30shineEntities())
                {
                    var obj = db.TinhThanhs.FirstOrDefault(w => w.ID == CityId);
                    if (obj != null)
                        str = obj.TenTinhThanh;
                }
                return str;
            }
        }

        public string DistrictName
        {
            get
            {
                string str = "";
                using (var db = new Solution_30shineEntities())
                {
                    var obj = db.QuanHuyens.FirstOrDefault(w => w.ID == DistrictId);
                    if (obj != null)
                        str = obj.TenQuanHuyen;
                }
                return str;
            }
        }

        public string InforFlowName
        {
            get
            {
                string str = "";
                using (var db = new Solution_30shineEntities())
                {
                    var obj = db.SocialThreads.FirstOrDefault(w => w.Id == Info_Flow);
                    if (obj != null)
                        str = obj.Name;
                }
                return str;
            }
        }

        public int CustomerReturn
        {
            get;
            set;
        }
    }
    #endregion
}

public struct StaffBasic
{
    public int Id { get; set; }
    /// <summary>
    /// Mã nhân viên
    /// </summary>
    public string FullName { get; set; }
    /// <summary>
    /// Tên nhân viên
}
public struct ProductBasic {
    public int Id { get; set; }
    /// <summary>
    /// Mã sản phẩm
    /// </summary>
    public string Code { get; set; }
    /// <summary>
    /// Tên sản phẩm
    /// </summary>
    public string Name { get; set; }
    /// <summary>
    /// Giá gốc
    /// </summary>
    public int Cost { get; set; }
    /// <summary>
    /// Giá bán
    /// </summary>
    public int Price { get; set; } 
    /// <summary>
    /// Số lượng
    /// </summary>
    public int Quantity { get; set; }
    /// <summary>
    /// % khuyến mãi
    /// </summary>
    public int VoucherPercent { get; set; }
    /// <summary>
    /// Tiền khuyến mãi
    /// </summary>
    public int Promotion { get; set; }
    public string MapIdProduct { get; set; }
    public bool CheckCombo { get; set; }
    public int? Order { get; set; }
    /// <summary>
    /// Check mức dùng vật tư
    /// </summary>
    public bool IsCheckVatTu { get; set; }
}

public struct ProductBasic_QLKho
{
    public int Id { get; set; }
    /// <summary>
    /// Mã sản phẩm
    /// </summary>
    public string Code { get; set; }
    /// <summary>
    /// Tên sản phẩm
    /// </summary>
    public string Name { get; set; }
    /// <summary>
    /// Giá gốc
    /// </summary>
    public int Cost { get; set; }
    /// <summary>
    /// Giá bán
    /// </summary>
    public int Price { get; set; }
    /// <summary>
    /// Số lượng đặt hàng
    /// </summary>
    public int QuantityOrder { get; set; }
    /// <summary>
    /// Số lượng nhận về (chính là số lượng kho xuất về cho salon)
    /// </summary>
    public int QuantityReceived { get; set; }
    /// <summary>
    /// Số lượng hàng nợ (là số lượng hàng kho xuất thiếu cho salon)
    /// </summary>
    public int QuantityOwe { get; set; }
    /// <summary>
    /// % khuyến mãi
    /// </summary>
    public int VoucherPercent { get; set; }
    /// <summary>
    /// Tiền khuyến mãi
    /// </summary>
    public int Promotion { get; set; }
}