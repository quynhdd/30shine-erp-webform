﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using _30shine.MODEL.Structure.CRM;
using _30shine.MODEL.ENTITY.EDMX;

namespace _30shine.MODEL.Data.CRM
{
    public class CustomerChemistryData
    {
        public Solution_30shineEntities db = new Solution_30shineEntities();

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="db"></param>
        public CustomerChemistryData(Solution_30shineEntities db)
        {
            this.db = db;
        }

        /// <summary>
        /// Lấy data khách lần đầu dùng dịch vụ và cũng là hóa chất
        /// </summary>
        /// <param name="salonID"></param>
        /// <param name="timeFrom"></param>
        /// <param name="timeTo"></param>
        /// <returns></returns>
        public List<CustomerChemistryStruct.cls_khach_hoa_chat> getListCustomer_NoChemistry(int salonID, DateTime timeFrom, DateTime timeTo, string SMS_Signature)
        {
            var cls_Khach = new CustomerChemistryStruct.cls_khach_hoa_chat();
           string chemistryServiceIDs = "14,16,17,24";
            string timeFromString = String.Format("{0:yyyy/MM/dd}", timeFrom);
            string timeToString = String.Format("{0:yyyy/MM/dd}", timeTo);
            //string sql = @"
            //            -- 1. Lấy toàn bộ danh sách khách hàng sử dụng dịch vụ lần đầu tiên trong kỳ thông qua BillService : D1
            //         -- 2. Lấy danh sách khách hàng sử dụng hóa chất trong kỳ : D2
            //         -- 3. Khách lần đầu dùng dịch vụ và không dùng hóa chất : D = D1 - D2 (Loại trừ : D1 Except D2)
            //            declare @salonId int,
            //              @timeFrom nvarchar(10),
            //              @timeTo nvarchar(10),
            //                    @Signature nvarchar(100);
            //            set @salonId = " + salonID + @";
            //            set @timeFrom = '" + timeFromString + @"';
            //            set @timeTo = '" + timeToString + @"';
            //            set @Signature = '" + SMS_Signature + @"';
            //            with a as
            //         (
            //          select bill.CustomerId
            //          from BillService as bill
            //          inner join FlowService as fs on fs.BillId = bill.Id and fs.IsDelete != 1
            //          where bill.IsDelete != 1 and bill.Pending != 1
            //           and bill.CreatedDate between @timeFrom and @timeTo
            //           and bill.SalonId = @salonId
            //           and (select COUNT(*) from BillService where CustomerId = bill.CustomerId and CreatedDate < @timeFrom) = 0
            //          group by bill.CustomerId
            //         ),
            //         b as(
            //          select bill.CustomerId
            //          from BillService as bill
            //          inner join FlowService as fs on fs.BillId = bill.Id and fs.IsDelete != 1
            //          where bill.IsDelete != 1 and bill.Pending != 1
            //           and bill.CreatedDate between @timeFrom and @timeTo		
            //           and bill.SalonId = @salonId
            //           and fs.ServiceId in (" + chemistryServiceIDs + @")
            //          group by bill.CustomerId
            //         ),
            //         c as 
            //         (
            //          select * from a
            //          except
            //          select * from b
            //         ),
            //         d as
            //         (
            //          select Customer.Id as CustomerId, Customer.Fullname as CustomerName, Customer.Phone as CustomerPhone,
            //           bill.Id as BillID, 
            //                (N'Anh '+Customer.Fullname+N' ơi, em gửi anh hướng dẫn chăm sóc, tạo kiểu tóc anh mới cắt ở 30Shine tại '+hairMode.CareLink+N'. Anh cần hỗ trợ gì cứ nhắn lại em nhé!  '" + SMS_Signature + @") as HairMode
            //          from c  
            //          inner join BillService as bill on bill.CustomerId = c.CustomerId
            //          left join Customer_HairMode_Bill as hairModeBill on hairModeBill.CustomerId = c.CustomerId and hairModeBill.BillId = bill.Id
            //          left join Api_HairMode as hairMode on hairMode.Id = hairModeBill.HairStyleId
            //          inner join Customer on c.CustomerId = Customer.Id	
            //                left join Tbl_Salon on Tbl_Salon.Id = bill.SalonId 
            //                where
            //         )

            //         select * from d order by d.CustomerId";
            List<CustomerChemistryStruct.cls_khach_hoa_chat> _list = new  List<CustomerChemistryStruct.cls_khach_hoa_chat>();
            var sql = db.store_sms_NoChemistry(salonID, timeFrom, timeTo, SMS_Signature).ToList();

            foreach(var item in sql)
            {
                cls_Khach = new CustomerChemistryStruct.cls_khach_hoa_chat();
                cls_Khach.BillID = item.BillID;
                cls_Khach.CustomerId = item.CustomerId;
                cls_Khach.CustomerName = item.CustomerName;
                cls_Khach.CustomerPhone = item.CustomerPhone;
                cls_Khach.HairMode = item.HairMode;
                _list.Add(cls_Khach);
            }
            return _list;

        }

        /// <summary>
        /// Lấy danh sách khách lần đầu dùng hóa chất (có thể là lần dùng dịch vụ thứ n)
        /// </summary>
        /// <param name="salonID"></param>
        /// <param name="timeFrom"></param>
        /// <param name="timeTo"></param>
        /// <returns></returns>
        public List<CustomerChemistryStruct.cls_khach_hoa_chat> getListCustomer_ChemistryFirstTime(int salonID, DateTime timeFrom, DateTime timeTo, string SMS_Signature)
        {
            string chemistryServiceIDs = "14,16,17,24";
            string timeFromString = String.Format("{0:yyyy/MM/dd}", timeFrom);
            string timeToString = String.Format("{0:yyyy/MM/dd}", timeTo);
            var cls_Khach = new CustomerChemistryStruct.cls_khach_hoa_chat();
            //string sql = @"
            //            -- 1. Lấy danh sách khách hàng sử dụng hóa chất trong kỳ : D2
            //         -- 1. Lấy danh sách khách hàng đã từng sử dụng hóa chất >= 1 lần trước kỳ 
            //         -- 3. Khách lần đầu dùng dùng hóa chất : D = D1 - D2 (Loại trừ : D1 Except D2)
            //            declare @salonId int,
            //              @timeFrom nvarchar(10),
            //              @timeTo nvarchar(10),
            //                    @Signature nvarchar(100);
            //            set @salonId = " + salonID + @";
            //            set @timeFrom = '" + timeFromString + @"';
            //            set @timeTo = '" + timeToString + @"';
            //            set @Signature = '" + SMS_Signature + @"';
            //            with a as
            //         (
            //          select bill.CustomerId
            //          from BillService as bill
            //          inner join FlowService as fs on fs.BillId = bill.Id and fs.IsDelete != 1
            //          where bill.IsDelete != 1 and bill.Pending != 1
            //           and bill.CreatedDate between @timeFrom and @timeTo		
            //           and bill.SalonId = @salonId
            //           and fs.ServiceId in (" + chemistryServiceIDs + @")
            //          group by bill.CustomerId
            //         ),

            //         b as(
            //          select bill.CustomerId
            //          from BillService as bill
            //          inner join FlowService as fs on fs.BillId = bill.Id and fs.IsDelete != 1
            //          inner join a on a.CustomerId = bill.CustomerId
            //          where bill.IsDelete != 1 and bill.Pending != 1
            //           and bill.CreatedDate < @timeFrom		
            //           and bill.SalonId = @salonId
            //           and fs.ServiceId in (" + chemistryServiceIDs + @")			
            //          group by bill.CustomerId
            //         ),
            //         c as 
            //         (
            //          select * from a
            //          except
            //          select * from b
            //         ),
            //         d as
            //         (
            //          select Customer.Id as CustomerId, Customer.Fullname as CustomerName, Customer.Phone as CustomerPhone,
            //           bill.Id as BillID, 
            //           (N'Anh '+Customer.Fullname+N' ơi, tóc a mới uốn nhuộm cần chăm sóc kĩ hơn bthg. A xem thêm hướng dẫn tại http://bit.ly/30shinehc nhé.  " + SMS_Signature + @" ) as HairMode
            //          from c  
            //          inner join BillService as bill on bill.CustomerId = c.CustomerId
            //          left join Tbl_Salon on Tbl_Salon.Id = bill.SalonId
            //          inner join Customer on c.CustomerId = Customer.Id	
            //          where bill.IsDelete != 1 and bill.Pending != 1
            //           and bill.CreatedDate between @timeFrom and @timeTo
            //           and bill.SalonId = @salonId
            //         )

            //         select * from d order by d.CustomerId";
            List<CustomerChemistryStruct.cls_khach_hoa_chat> _list = new List<CustomerChemistryStruct.cls_khach_hoa_chat>();
            var sql = db.store_sms_ChemistryFirstTime(salonID, timeFrom, timeTo, SMS_Signature).ToList();
            foreach (var item in sql)
            {
                cls_Khach = new CustomerChemistryStruct.cls_khach_hoa_chat();
                cls_Khach.BillID = item.BillID;
                cls_Khach.CustomerId = item.CustomerId;
                cls_Khach.CustomerName = item.CustomerName;
                cls_Khach.CustomerPhone = item.CustomerPhone;
                cls_Khach.HairMode = item.HairMode;
                _list.Add(cls_Khach);
            }
            return _list;
        }
    }
}