﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
namespace _30shine.MODEL.Bussiness
{
    public class Stylist4MenTuitionModel : IStylist4MenTuitionModel
    {
        private Solution_30shineEntities db;
        public Stylist4MenTuitionModel()
        {
            db = new Solution_30shineEntities();
        }

        /// <summary>
        /// Add
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public Stylist4Men_Tuition Add(Stylist4Men_Tuition obj)
        {
            try
            {
                if (obj != null)
                {
                    var record = new Stylist4Men_Tuition();
                    record.StudentId = obj.StudentId;
                    record.AmountCollected = obj.AmountCollected;
                    record.PayTheMoney = obj.PayTheMoney;
                    record.CreaetedTime = obj.CreaetedTime;
                    record.IsDelete = obj.IsDelete;
                    record.Publish = obj.Publish;
                    this.db.Stylist4Men_Tuition.Add(record);
                    this.db.SaveChanges(); 
                }
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public Stylist4Men_Tuition Update(Stylist4Men_Tuition obj)
        {
            try
            {
                return obj;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}