﻿using _30shine.MODEL.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using _30shine.MODEL.ENTITY.EDMX;
using System.Web.Script.Serialization;
using System.Web.Services;
using static Library.Class;

namespace _30shine.MODEL.Bussiness
{
    public class DeviceModel : IDeviceModel
    {
        public Device Add(Device obj)
        {
            try
            {
                var device = new Device();
                using (var db = new Solution_30shineEntities())
                {
                    device.DeviceName = obj.DeviceName;
                    device.Description = obj.Description;
                    device.OwnerId = obj.OwnerId;
                    device.OwnerType = obj.OwnerType;
                    device.CreatedDate = obj.CreatedDate;
                    device.ImeiOrMacIP = obj.ImeiOrMacIP;
                    device.UniqueKey = obj.UniqueKey;
                    device.IsDelete = obj.IsDelete;
                    db.Devices.Add(device);
                    var exc = db.SaveChanges();
                    if (exc > 0)
                    {
                        var mapId = new MapDeviceOwner();
                        mapId.DeviceId = device.Id;
                        mapId.OwnerId = device.OwnerId;
                        mapId.IsDelete = false;
                        db.MapDeviceOwners.Add(mapId);
                        db.SaveChanges();
                    }
                }
                return device;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public Device Update(Device obj)
        {
            try
            {
                var device = new Device();
                using (var db = new Solution_30shineEntities())
                {
                    device = (from c in db.Devices
                                     where c.Id == obj.Id
                                     select c).FirstOrDefault();
                    device.DeviceName = obj.DeviceName;
                    device.Description = obj.Description;
                    device.OwnerId = obj.OwnerId;
                    device.OwnerType = obj.OwnerType;
                    device.ModifiedDate = obj.ModifiedDate;
                    device.ImeiOrMacIP = obj.ImeiOrMacIP;
                    device.IsDelete = obj.IsDelete;
                    var exc = db.SaveChanges();
                    if (exc > 0)
                    {
                        var mapId = new MapDeviceOwner();
                        mapId = (from c in db.MapDeviceOwners
                                 where c.DeviceId == device.Id && c.OwnerId == device.OwnerId
                                  select c).FirstOrDefault();
                        mapId.DeviceId = device.Id;
                        mapId.OwnerId = device.OwnerId;
                        mapId.IsDelete = false;
                        db.SaveChanges();
                    }
               }
                return device;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Cls_Device> ListDevice(int? Type)
        {
            try
            {
                List<Cls_Device> lstClsDevice = new List<Cls_Device>();
                using (var db = new Solution_30shineEntities())
                {
                    Cls_Device cls_Device = new Cls_Device();
                    if (Type == 0)
                    {
                        var lstDevice = (from c in db.Devices
                                         join f in db.Tbl_Salon on c.OwnerId equals f.Id into a
                                         from f in a.DefaultIfEmpty()
                                         where
                                         c.IsDelete == false
                                         && f.Publish == true
                                         && f.IsDelete == 0
                                         && c.OwnerType == Type
                                         select new
                                         {
                                             Id = c.Id,
                                             SalonName = f.Name,
                                             DeviceName = c.DeviceName,
                                             IMEIorMacIp = c.ImeiOrMacIP,
                                         }).ToList();
                        foreach (var item in lstDevice)
                        {
                            cls_Device = new Cls_Device();
                            cls_Device.Id = item.Id;
                            cls_Device.SalonName = item.SalonName;
                            cls_Device.DeviceName = item.DeviceName;
                            cls_Device.ImeiOrMacIP = item.IMEIorMacIp;
                            lstClsDevice.Add(cls_Device);
                        }
                    }
                    else if (Type == 1)
                    {
                        var lstDevice = (from c in db.Devices
                                         join f in db.Staffs on c.OwnerId equals f.Id into a
                                         from f in a.DefaultIfEmpty()
                                         where
                                         c.IsDelete == false
                                         && f.Active == 1
                                         && f.IsDelete == 0
                                         && c.OwnerType == Type
                                         select new
                                         {
                                             Id = c.Id,
                                             StaffName = f.Fullname,
                                             DeviceName = c.DeviceName,
                                             IMEIorMacIp = c.ImeiOrMacIP,
                                         }).ToList();
                        foreach (var item in lstDevice)
                        {
                            cls_Device = new Cls_Device();
                            cls_Device.Id = item.Id;
                            cls_Device.StaffName = item.StaffName;
                            cls_Device.DeviceName = item.DeviceName;
                            cls_Device.ImeiOrMacIP = item.IMEIorMacIp;
                            lstClsDevice.Add(cls_Device);
                        }
                    }
                }
                return lstClsDevice;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}