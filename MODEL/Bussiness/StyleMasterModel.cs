﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity.Migrations;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;

namespace _30shine.MODEL.Bussiness
{
    public class StyleMasterModel : IStyleMasterModel
    {
        Solution_30shineEntities db = new Solution_30shineEntities();

        public bool Delete(int id)
        {
            var r = db.StyleMasters.FirstOrDefault(s => s.Id == id);
            if (r != null)
            {
                r.IsDelete = true;
                db.StyleMasters.AddOrUpdate(r);
                var result = db.SaveChanges();
                //if (result > 0)
                //{
                //    return true;
                //}
                return true;
            }
            else return true;
        }

        public bool Approve(int id, int userId, int statusId)
        {
            var r = db.StyleMasters.FirstOrDefault(s => s.Id == id);
            if (r != null)
            {
                r.ApproveTime = DateTime.Now;
                r.ApproveUserId = userId;
                r.StyleMasterStatusId = statusId;
                r.TotalLike = 0;
                db.StyleMasters.AddOrUpdate(r);
                var result = db.SaveChanges();
                if (result > 0)
                {
                    return true;
                }
                else return false;
            }
            else return false;
        }

        /// <summary>
        /// return all list status
        /// </summary>
        /// <returns></returns>
        public List<StyleMaster_Status> ListStatus()
        {
            return db.StyleMaster_Status.Where(s => s.isDelete == false).ToList();
        }
        /// <summary>
        /// return all Data by month and statusId
        /// </summary>
        /// <param name="dateTime"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public List<StyleMaster> ListStyleMaster(DateTime dateTime, int status)
        {
            if (dateTime == null)
            {
                var month = DateTime.Now.Month;
                var year = DateTime.Now.Year;
                if (status == 0)
                {
                    return db.StyleMasters
                        .Where(s => s.CreatedTime.Value.Month == month && s.CreatedTime.Value.Year == year && s.IsDelete == false).OrderByDescending(s => s.ApproveTime).ToList();
                }
                else
                    return db.StyleMasters
                        .Where(s => s.CreatedTime.Value.Month == month && s.CreatedTime.Value.Year == year && s.StyleMasterStatusId == status && s.IsDelete == false).OrderByDescending(s => s.ApproveTime).ToList();
            }
            else
            {
                var month = dateTime.Month;
                var year = dateTime.Year;
                if (status == 0)
                {
                    return db.StyleMasters
                        .Where(s => s.CreatedTime.Value.Month == month && s.CreatedTime.Value.Year == year && s.IsDelete == false).OrderByDescending(s => s.ApproveTime).ToList();
                }
                else
                    return db.StyleMasters
                        .Where(s => s.CreatedTime.Value.Month == month && s.CreatedTime.Value.Year == year && s.StyleMasterStatusId == status && s.IsDelete == false).OrderByDescending(s => s.ApproveTime).ToList();
            }

        }
        public object StyleMaster(int id)
        {
            return db.StyleMasters.FirstOrDefault(w => w.Id == id);
        }
    }
}