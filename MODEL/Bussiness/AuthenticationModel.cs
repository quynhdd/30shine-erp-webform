﻿using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using Amazon.CognitoIdentityProvider;
using Amazon.CognitoIdentityProvider.Model;
using Amazon.Extensions.CognitoAuthentication;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace _30shine.MODEL.Bussiness
{
    public class AuthenticationModel : IAuthenticationModel
    {
        private static Amazon.RegionEndpoint Region = Amazon.RegionEndpoint.APSoutheast1;
        private AmazonCognitoIdentityProviderClient providerClient =
                    new AmazonCognitoIdentityProviderClient(Libraries.AppConstants.ACCESS_KEY, Libraries.AppConstants.SECRET_KEY, Region);

        /// <summary>
        /// Get access token when user login
        /// </summary>
        /// <param name="UserName">The user name of the user</param>
        /// <param name="Password">The current password of the user.</param>
        /// <returns></returns>
        public AuthFlowResponse GetAccessTokenUserForLogin(string UserName, string Password)
        {
            try
            {
                //Create CognitoUserPool and CognitoUser objects. Call the StartWithSrpAuthAsync method with an InitiateSrpAuthRequest that contains the user password.

                //get user pool of cognito
                CognitoUserPool cognitoUserPool = new CognitoUserPool(Libraries.AppConstants.POOL_ID, Libraries.AppConstants.APP_CLIENT_ID, providerClient);

                //get user in user pool
                CognitoUser cognitoUser = new CognitoUser(UserName, Libraries.AppConstants.APP_CLIENT_ID, cognitoUserPool, providerClient);

                //password request syntax
                InitiateSrpAuthRequest authRequest = new InitiateSrpAuthRequest()
                {
                    Password = Password
                };

                //Call the StartWithSrpAuthAsync method with an InitiateSrpAuthRequest that contains the user password.
                AuthFlowResponse authResponse = cognitoUser.StartWithSrpAuthAsync(authRequest).Result;

                //if force change password
                UpdateUserAttributesRequest updateUserAttributesRequest = new UpdateUserAttributesRequest();

                if (authResponse.AuthenticationResult == null)
                {
                    if (authResponse.ChallengeName == ChallengeNameType.NEW_PASSWORD_REQUIRED)
                    {
                        authResponse = cognitoUser.RespondToNewPasswordRequiredAsync(new RespondToNewPasswordRequiredRequest()
                        {
                            SessionID = authResponse.SessionID,
                            NewPassword = Password
                        }).Result;
                        updateUserAttributesRequest.AccessToken = authResponse.AuthenticationResult.AccessToken;
                    }
                    else
                    {
                        updateUserAttributesRequest.AccessToken = "";
                    }
                }
                //end
                return authResponse;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// refresh token
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public async Task<AuthFlowResponse> RefreshToken(string username, string idToken, string accessToken, string refreshToken)
        {
            try
            {
                //get user pool of cognito
                CognitoUserPool cognitoUserPool = new CognitoUserPool(Libraries.AppConstants.POOL_ID, Libraries.AppConstants.APP_CLIENT_ID, providerClient);

                var user = cognitoUserPool.GetUser(username);

                user.SessionTokens = new CognitoUserSession(idToken, accessToken, refreshToken, DateTime.Now, DateTime.Now.AddMinutes(24 * 60));
                // refress token
                var authResponse = await user.StartWithRefreshTokenAuthAsync(new InitiateRefreshTokenAuthRequest()
                {
                    AuthFlowType = AuthFlowType.REFRESH_TOKEN
                }).ConfigureAwait(false);

                UpdateUserAttributesRequest updateUserAttributesRequest = new UpdateUserAttributesRequest();

                if (authResponse.AuthenticationResult.AccessToken != null)
                {
                    // set accesstoken new 
                    updateUserAttributesRequest.AccessToken = authResponse.AuthenticationResult.AccessToken;
                }
                else
                {
                    updateUserAttributesRequest.AccessToken = "";
                }
                //end
                return authResponse;
            }

            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Changes the password on the cognito account associated with the <paramref name="token"/>.
        /// </summary>
        /// <param name="token">The access token of the user.</param>
        /// <param name="currentPassword">The current password of the user.</param>
        /// <param name="newPassword">The new passord for the user.</param>
        /// The that represents the asynchronous operation, containing the
        /// of the operation.
        public void ChangePassword(string token, string oldPassword, string newPassword)
        {
            try
            {
                ChangePasswordRequest changePasswordRequest = new ChangePasswordRequest()
                {
                    AccessToken = token,
                    PreviousPassword = oldPassword,
                    ProposedPassword = newPassword
                };
                ChangePasswordResponse changePasswordResponse = providerClient.ChangePasswordAsync(changePasswordRequest).Result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Admin delete user with the <paramref name="username"/>.
        /// </summary>
        /// <param name="username">The user name of the user.</param>
        public void AdminDeleteUser(string username)
        {
            try
            {
                //request syntax
                AdminDeleteUserRequest adminDeleteUserRequest = new AdminDeleteUserRequest()
                {
                    Username = username,
                    UserPoolId = Libraries.AppConstants.POOL_ID
                };
                //response cong
                AdminDeleteUserResponse adminDeleteUserResponse = providerClient.AdminDeleteUserAsync(adminDeleteUserRequest).Result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Admin disable user with the <paramref name="username"/>.
        /// </summary>
        /// <param name="username">The user name of the user.</param>
        public void AdminDisableUser(string username)
        {
            try
            {
                //request syntax
                AdminDisableUserRequest adminDisableUserRequest = new AdminDisableUserRequest()
                {
                    Username = username,
                    UserPoolId = Libraries.AppConstants.POOL_ID
                };
                //response cong
                AdminDisableUserResponse adminDisableUserResponse = providerClient.AdminDisableUserAsync(adminDisableUserRequest).Result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Admin enable user with the <paramref name="username"/>.
        /// </summary>
        /// <param name="username">The user name of the user.</param>
        public void AdminEnableUser(string username)
        {
            try
            {
                //request syntax
                AdminEnableUserRequest adminEnableUserRequest = new AdminEnableUserRequest()
                {
                    Username = username,
                    UserPoolId = Libraries.AppConstants.POOL_ID
                };
                //response cong
                AdminEnableUserResponse adminEnableUserResponse = providerClient.AdminEnableUserAsync(adminEnableUserRequest).Result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Registers the specified <paramref name="username"/> in Cognito with the given password,
        /// as an asynchronous operation. Also submits the validation data to the pre sign-up lambda trigger.
        /// </summary>
        /// <param name="username">The user to create.</param>
        /// <param name="password">The password for the user to register with</param>
        public void SignUp(string username, string password)
        {
            try
            {
                SignUpRequest signUpRequest = new SignUpRequest()
                {
                    ClientId = Libraries.AppConstants.APP_CLIENT_ID,
                    Username = username,
                    Password = password
                };

                //var attributePassword = new AttributeType
                //{
                //    Name = "custom:password",
                //    Value = password
                //};

                //signUpRequest.UserAttributes.Add(attributePassword);

                SignUpResponse signUpResponse = providerClient.SignUpAsync(signUpRequest).Result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Admin confirms the specified <paramref name="user"/>, regardless of the confirmation code
        /// as an asynchronous operation.
        /// </summary>
        /// <param name="user">The user to confirm.</param>
        /// <returns>
        /// The <see cref="Task"/> that represents the asynchronous operation, containing the <see cref="IdentityResult"/>
        /// of the operation.
        /// </returns>
        public void AdminConfirmSignUp(string username)
        {
            try
            {
                AdminConfirmSignUpRequest adminConfirmSignUpRequest = new AdminConfirmSignUpRequest()
                {
                    Username = username,
                    UserPoolId = Libraries.AppConstants.POOL_ID
                };
                AdminConfirmSignUpResponse adminConfirmSignUpResponse = providerClient.AdminConfirmSignUpAsync(adminConfirmSignUpRequest).Result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Get user in db
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public Tuple<User, Staff> GetUser(string username)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    User obj = null;
                    Staff obj2 = null;
                    var user = (
                            from u in db.Users
                            join s in db.Staffs on u.StaffId equals s.Id
                            where
                            u.IsDelete == false && u.IsActive == true
                            && s.IsDelete == 0 && s.Active == 1 && u.Username == username
                            select new
                            {
                                user = u,
                                staff = s
                            }
                        ).FirstOrDefault();
                    if (user != null)
                    {
                        obj = user.user;
                        obj2 = user.staff;
                    }
                    return new Tuple<User, Staff>(obj, obj2);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Get user in db
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public Tuple<User, Staff> GetUserByStaffId(int Id)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    User obj = null;
                    Staff obj2 = null;
                    var user = (
                            from u in db.Users
                            join s in db.Staffs on u.StaffId equals s.Id
                            where
                            u.IsDelete == false && u.IsActive == true
                            && s.IsDelete == 0 && s.Active == 1 && u.StaffId == Id
                            select new
                            {
                                user = u,
                                staff = s
                            }
                        ).FirstOrDefault();
                    if (user != null)
                    {
                        obj = user.user;
                        obj2 = user.staff;
                    }
                    return new Tuple<User, Staff>(obj, obj2);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// update attribute
        /// </summary>
        /// <param name="username">The user name of user.</param>
        /// <param name="code">The code of user.</param>
        public void AdminUpdateUserAttribute(string username, string code)
        {
            try
            {
                var attributeCode = new List<AttributeType>()
            {
                new AttributeType(){Name = "custom:code_authentication", Value = code }
            };

                AdminUpdateUserAttributesRequest addCustomAttributesRequest = new AdminUpdateUserAttributesRequest()
                {
                    UserAttributes = attributeCode,
                    Username = username,
                    UserPoolId = Libraries.AppConstants.POOL_ID
                };

                AdminUpdateUserAttributesResponse addCustomAttributesResponse = providerClient.AdminUpdateUserAttributesAsync(addCustomAttributesRequest).Result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// admin get attr 
        /// </summary>
        /// <param name="username">The user name of the user.</param>
        /// <returns></returns>
        public string AdminGetAttribute(string username)
        {
            string value = "";
            try
            {
                AdminGetUserRequest adminGetUserRequest = new AdminGetUserRequest()
                {
                    Username = username,
                    UserPoolId = Libraries.AppConstants.POOL_ID
                };

                AdminGetUserResponse adminGetUserResponse = providerClient.AdminGetUserAsync(adminGetUserRequest).Result;

                if (adminGetUserResponse.UserAttributes != null)
                {
                    value = adminGetUserResponse.UserAttributes.Where(w => w.Name == "custom:code_authentication").FirstOrDefault().Value;
                }
                return value;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Add user to db
        /// </summary>
        /// <param name="staffId">id của nhân viên</param>
        /// <param name="userName">tài khoản của nhân viên</param>
        /// <param name="isLogin">Loại tài khoản login (1 là tài khoản salon) || (0 là tài khoản cá nhân)</param>
        /// <param name="email"></param>
        /// <param name="phone"></param>
        public User AddUser(int staffId, string userName, int isLogin, string email, string phone)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var timelive = isLogin == 1 ? 10 : 60;
                    var user = new User()
                    {
                        UserId = Guid.NewGuid().ToString(),
                        Username = userName,
                        TimeLivePassword = timelive,
                        StaffId = staffId,
                        Valid2FA = false,
                        Email = email,
                        Phone = phone,
                        CreatedDate = DateTime.Now,
                        LastChangePassword = DateTime.Now,
                        RequiredValid2FA = false,
                        FirstLogin = false,
                        IsActive = true,
                        IsDelete = false
                    };
                    db.Users.Add(user);
                    db.SaveChanges();

                    return user;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// update user
        /// </summary>
        /// <param name="staffId"></param>
        /// <param name="email"></param>
        /// <param name="phone"></param>
        /// <param name="isActive"></param>
        public void UpdateUser(int staffId, string email, string phone, byte isActive)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var active = isActive == 1 ? true : false;
                    var user = db.Users.FirstOrDefault(f => f.StaffId == staffId && f.IsDelete == false);
                    if (user != null)
                    {
                        user.IsActive = active;
                        user.ModifiedDate = DateTime.Now;
                        user.Email = email;
                        user.Phone = phone;
                        db.Users.AddOrUpdate(user);
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// map user vs staff
        /// </summary>
        /// <param name="staffId"></param>
        /// <param name="userId"></param>
        public void AddMapUserStaff(User user, Staff staff)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var map = new UserStaff();
                    if ((staff.Type == 5 || staff.Type == 6) && (staff.isAccountLogin == 0 || staff.isAccountLogin == null))
                    {
                        //check account of salon
                        var generalAcc = (
                                             from u in db.Users
                                             join s in db.Staffs on u.StaffId equals s.Id
                                             where
                                             u.IsDelete == false && u.IsActive == true
                                             && s.IsDelete == 0 && s.Active == 1
                                             && s.SalonId == staff.SalonId && s.Type == staff.Type
                                             && s.isAccountLogin == 1
                                             select new
                                             {
                                                 user = u,
                                                 staff = s
                                             }
                                         ).FirstOrDefault();
                        if (generalAcc != null)
                        {
                            map = new UserStaff();
                            map.StaffId = user.StaffId;
                            map.UserId = generalAcc.user.UserId;
                            map.CreatedTime = DateTime.Now;
                            db.UserStaffs.Add(map);
                            db.SaveChanges();
                        }
                        map = new UserStaff();
                        map.StaffId = user.StaffId;
                        map.UserId = user.UserId;
                        map.CreatedTime = DateTime.Now;
                        db.UserStaffs.Add(map);
                        db.SaveChanges();
                    }
                    else
                    {
                        map.StaffId = user.StaffId;
                        map.UserId = user.UserId;
                        map.CreatedTime = DateTime.Now;
                        db.UserStaffs.Add(map);
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Xoá hoặc vô hiệu hoá sử dụng tài khoản
        /// </summary>
        /// <param name="employ">nhân viên</param>
        /// <param name="firstLogin">login lần đầu</param>
        /// <param name="type">có bắt buộc login lần đâu hay k</param>
        public void DeleteOrDisableMapUserStaff(Staff employ, bool firstLogin = false, int type = 0)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    if (employ != null)
                    {
                        if ((employ.Type == 5 || employ.Type == 6) && (employ.isAccountLogin == 0 || employ.isAccountLogin == null))
                        {
                            var map = db.UserStaffs.Where(w => w.StaffId == employ.Id).ToList();
                            foreach (var item in map)
                            {
                                var user = db.Users.FirstOrDefault(f => f.UserId == item.UserId && f.IsDelete == false);
                                if (user != null)
                                {
                                    var staff = db.Staffs.Where(r => r.Id == user.StaffId && r.IsDelete == 0).Select(r => new { isLogin = r.isAccountLogin, staffId = r.Id }).FirstOrDefault();
                                    if (staff != null && staff.isLogin == 1 && type == 1)
                                    {
                                        user.FirstLogin = firstLogin;
                                        db.Users.AddOrUpdate(user);
                                    }
                                }
                                db.UserStaffs.Remove(item);
                            }
                        }
                        else
                        {
                            var map = db.UserStaffs.FirstOrDefault(w => w.StaffId == employ.Id);
                            if (map != null)
                            {
                                db.UserStaffs.Remove(map);
                            }
                        }
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// get user by user name
        /// </summary>
        /// <param name="userName">The user name of user.</param>
        /// <returns></returns>
        public User GetByUserName(string userName)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    return db.Users.FirstOrDefault(f => f.Username == userName && f.IsActive == true && f.IsDelete == false);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// get user by user name
        /// </summary>
        /// <param name="userName">The user name of user.</param>
        /// <returns></returns>
        public User GetByEmail(string email)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    return db.Users.FirstOrDefault(f => f.Email == email);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// get user by user name
        /// </summary>
        /// <param name="userName">The user name of user.</param>
        /// <returns></returns>
        public bool CheckUserName(string userName, int staffId)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    return db.Users.FirstOrDefault(f => f.Username == userName && f.StaffId != staffId && f.IsActive == true && f.IsDelete == false) != null ? true : false;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// get user by user name
        /// </summary>
        /// <param name="userName">The user name of user.</param>
        /// <returns></returns>
        public bool CheckEmail(string email, int staffId)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    return db.Users.FirstOrDefault(f => f.Email == email && f.StaffId != staffId && f.IsActive == true && f.IsDelete == false) != null ? true : false;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Get user in db
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public List<GUI.BackEnd.User.UserListing> GetList(int SalonId, int DepartmentId)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    return (
                                from u in db.Users
                                join s in db.Staffs on u.StaffId equals s.Id
                                join t in db.Staff_Type on s.Type equals t.Id
                                join o in db.Tbl_Salon on s.SalonId equals o.Id
                                where
                                u.IsDelete == false && u.IsActive == true
                                && s.IsDelete == 0 && s.Active == 1
                                && ((s.SalonId == SalonId) || (SalonId == 0))
                                && ((s.Type == DepartmentId) || (DepartmentId == 0))
                                && o.IsSalonHoiQuan == false
                                select new GUI.BackEnd.User.UserListing
                                {
                                    StaffId = s.Id,
                                    StaffName = s.Fullname,
                                    SalonName = o.Name,
                                    DepartmentName = t.Name,
                                    UserName = u.Username,
                                    TimelivePassword = u.TimeLivePassword ?? 0,
                                    FirstLogin = u.FirstLogin == false ? "Đã đăng nhập." : "Chưa đăng nhập.",
                                    RequiredValid2FA = u.RequiredValid2FA == true ? "Có" : "Không"
                                }
                           ).OrderByDescending(o => o.StaffId).ToList();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        ///  Gen password use Md5
        /// </summary>
        /// <param name="md5Hash"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        public string GenPassword(MD5 md5Hash, string input)
        {
            var scrPush = "uaefsfkoiu&@(^%=";
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input + scrPush));
            StringBuilder sBuilder = new StringBuilder();

            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            return sBuilder.ToString();
        }

        public bool CheckUserExistCognito(string userName)
        {
            try
            {
                AdminGetUserRequest adminGetUserRequest = new AdminGetUserRequest()
                {
                    Username = userName,
                    UserPoolId = Libraries.AppConstants.POOL_ID
                };

                AdminGetUserResponse adminGetUserResponse = providerClient.AdminGetUserAsync(adminGetUserRequest).Result;

                if (adminGetUserResponse != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                var awsex = e as Amazon.Runtime.AmazonServiceException;
                var agex = e as System.AggregateException;
                string code = "";
                if (awsex != null)
                {
                    code = awsex.ErrorCode;
                }
                else if (agex != null)
                {
                    code = agex.InnerException.Message;
                }
                if (code.Equals(Libraries.AppConstants.USER_NOT_FOUND_EXCEPTION))
                {
                    return false;
                }
                else
                {
                    throw e;
                }
            }
        }
    }
}