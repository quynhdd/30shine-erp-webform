﻿using _30shine.MODEL.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using _30shine.MODEL.ENTITY.EDMX;

namespace _30shine.MODEL.Bussiness
{
    public class EnrollModel : IEnrollModel
    {
        public Solution_30shineEntities db;
        public EnrollModel()
        {
            this.db = new Solution_30shineEntities();
        }

        public FlowTimeKeeping Add(FlowTimeKeeping obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public bool IsEnroll(int staffId, DateTime workDate, int? salonId = default(int?))
        {
            try
            {
                FlowTimeKeeping enrollRecord;
                if (salonId == null)
                {
                    enrollRecord = this.db.FlowTimeKeepings.FirstOrDefault(
                        w => w.StaffId == staffId 
                        && w.WorkDate == workDate.Date);
                }
                else
                {
                    enrollRecord = this.db.FlowTimeKeepings.FirstOrDefault(
                        w=>w.StaffId == staffId 
                        && w.WorkDate == workDate.Date 
                        && w.SalonId == salonId);
                }

                if (enrollRecord != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public FlowTimeKeeping Update(FlowTimeKeeping obj)
        {
            throw new NotImplementedException();
        }
    }
}