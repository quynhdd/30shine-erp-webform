﻿using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using LinqKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace _30shine.MODEL.Bussiness
{
    public class CosmeticModel : ICosmeticModel
    {
        /// <summary>
        /// Gen bill code salon order 
        /// author: dungnm
        /// </summary>
        /// <param name="salonId"></param>
        /// <param name="prefix"></param>
        /// <param name="template"></param>
        /// <returns></returns>
        public string GenBillCode(int salonId, string prefix, DateTime OrderDate, int template = 4)
        {
            using (var db = new Solution_30shineEntities())
            {
                var Where = PredicateBuilder.True<QLKho_SalonOrder>();
                DateTime today = OrderDate;
                Where = Where.And(w => w.IsDelete != true && w.OrderDate == today);
                if (salonId > 0)
                {
                    Where = Where.And(w => w.SalonId == salonId);
                }
                return prefix + String.Format("{0:ddMMyy}", OrderDate) + salonId;
            }
        }

        /// <summary>
        /// Xuất lô mỹ phẩm
        /// author: dungnm
        /// </summary>
        public string Add(int salonId, DateTime OrderDate)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var CodeBill = GenBillCode(salonId, "MP", OrderDate, 4);
                    var checkOfToday = db.QLKho_SalonOrder.FirstOrDefault(w => w.OrderDate == OrderDate.Date && w.SalonId == salonId && w.CheckCosmetic == 1 && w.BillCode == CodeBill);
                    if (checkOfToday == null)
                    {
                        checkOfToday = new QLKho_SalonOrder();
                        checkOfToday.IsDelete = false;
                        checkOfToday.OrderDate = OrderDate.Date;
                        checkOfToday.CreatedTime = DateTime.Now;
                        checkOfToday.StatusId = 1;
                        checkOfToday.BillCode = CodeBill;
                        checkOfToday.SalonId = salonId;
                        checkOfToday.CheckCosmetic = 1;
                        db.QLKho_SalonOrder.Add(checkOfToday);
                        db.SaveChanges();
                    }
                    else
                    {
                        checkOfToday = new QLKho_SalonOrder();
                    }
                    return CodeBill;
                }
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}