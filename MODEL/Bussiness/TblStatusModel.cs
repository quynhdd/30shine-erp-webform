﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
namespace _30shine.MODEL.Bussiness
{
    public class TblStatusModel : ITblStatusModel
    {
        private Solution_30shineEntities db;
        public TblStatusModel()
        {
            db = new Solution_30shineEntities();
        }

        /// <summary>
        /// Get list mức độ quan tâm
        /// </summary>
        /// <returns></returns>
        public List<Tbl_Status> GetList()
        {
            try
            {
                var list = db.Tbl_Status.Where(w => w.IsDelete == false && w.Publish == true && w.ParentId == 1).ToList();
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Get list mức độ quan tâm
        /// </summary>
        /// <returns></returns>
        public List<Tbl_Status> GetList_Staff()
        {
            try
            {
                var list = db.Tbl_Status.Where(w => w.IsDelete == false && w.Publish == true && w.ParentId == 9).ToList();
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get name
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public List<Tbl_Status> Getname(int value)
        {
            try
            {
                var data = db.Tbl_Status.Where(w => w.Id == value).ToList();
                return data;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}