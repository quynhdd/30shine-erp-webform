﻿using _30shine.MODEL.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;

namespace _30shine.MODEL.BO
{
    public class EmploymentUserTestModel : IEmploymentUserTestModel
    {
        Solution_30shineEntities db;
        List<TuyenDung_NguoiTest> listUser;
        public EmploymentUserTestModel()
        {
            this.db = new Solution_30shineEntities();
        }
        public TuyenDung_NguoiTest Add(TuyenDung_NguoiTest obj)
        {
            try
            {
                var record = new TuyenDung_NguoiTest();
                record.FullName = obj.FullName;
                record.Email = obj.Email;
                record.Telephone = obj.Telephone;
                record.CreatedTime = DateTime.Now;
                record.IsDelete = false;
                record.StaffId = obj.StaffId;

                this.db.TuyenDung_NguoiTest.Add(record);
                this.db.SaveChanges();

                return record;
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public TuyenDung_NguoiTest Update(TuyenDung_NguoiTest obj)
        {
            try
            {
                if (obj != null)
                {
                    var record = this.GetUserByID(obj.Id);
                    if (record != null)
                    {
                        record.FullName = obj.FullName;
                        record.Email = obj.Email;
                        record.Telephone = obj.Telephone;
                        record.StaffId = obj.StaffId;
                        record.ModifiedTime = DateTime.Now;

                        this.db.TuyenDung_NguoiTest.AddOrUpdate(record);
                        this.db.SaveChanges();

                        return record;
                    }
                    else
                    {
                        throw new Exception("Lỗi. Không tồn tại bản ghi.");
                    }
                }
                else
                {
                    throw new Exception("Lỗi. Không tồn tại bản ghi.");
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                var record = this.GetUserByID(id);
                if (record != null)
                {
                    record.IsDelete = true;
                    record.ModifiedTime = DateTime.Now;
                    this.db.TuyenDung_NguoiTest.AddOrUpdate(record);
                    this.db.SaveChanges();

                    return true;
                }
                else
                {
                    throw new Exception("Lỗi. Không tồn tại bản ghi.");
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<TuyenDung_NguoiTest> GetListUser()
        {
            try
            {
                return this.db.TuyenDung_NguoiTest.Where(w=>w.IsDelete == false).ToList();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public TuyenDung_NguoiTest GetUserByID(int id)
        {
            try
            {
                return this.listUser.FirstOrDefault(w=>w.Id == id);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}