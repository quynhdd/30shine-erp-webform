﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static Library.Class;
using System.Web.Script.Serialization;
using System.Web.Services;
using _30shine.MODEL.Interface;
using _30shine.MODEL.ENTITY.EDMX;
using System.Globalization;

namespace _30shine.MODEL.Bussiness
{
    public class ReportCheckinModel : IReportCheckinModel
    {
        protected CultureInfo culture = new CultureInfo("vi-VN");
        public List<Cls_checkin> ListCheckin(string timeFrom, string timeTo, int SalonId, int Status, int StaffId)
        {
            try
            {
                List<Cls_checkin> lstClscheckin = new List<Cls_checkin>();
                DateTime TimeFrom = new DateTime();
                DateTime TimeTo = new DateTime();
                if (timeFrom != "")
                {
                    TimeFrom = Convert.ToDateTime(timeFrom, culture);
                    if (timeTo != "")
                    {
                        if (timeFrom == timeTo)
                        {
                            TimeTo = Convert.ToDateTime(timeFrom, culture).AddDays(1);
                        }
                        else
                        {
                            TimeTo = Convert.ToDateTime(timeTo, culture).AddDays(1);
                        }
                    }
                    else
                    {
                        TimeTo = TimeFrom;
                    }

                    using (var db = new Solution_30shineEntities())
                    {
                        Cls_checkin cls_checkin = new Cls_checkin();
                        switch (Status)
                        {
                            case 0:
                                var lstcheckin = (from ck in db.CheckinCheckouts
                                                  join f in db.FlowTimeKeepings on ck.FlowTimeKeepingId equals f.Id
                                                  join w in db.WorkTimes on f.WorkTimeId equals w.Id
                                                  join s in db.Tbl_Salon on ck.SalonId equals s.Id
                                                  join st in db.Staffs on ck.StaffId equals st.Id into a
                                                  from st in a.DefaultIfEmpty()
                                                  where
                                                  s.Publish == true && s.IsDelete == 0
                                                  && ((ck.StaffId == StaffId) || (StaffId == 0))
                                                  && ((ck.SalonId == SalonId) || (SalonId == 0))
                                                  && ((ck.CheckinTime > TimeFrom) && (ck.CheckinTime < TimeTo))
                                                  && f.DelayTime > 0
                                                  select new
                                                  {
                                                      WorkDate = f.WorkDate,
                                                      FullName = st.Fullname,
                                                      StaffId = st.Id,
                                                      SalonName = s.Name,
                                                      StartTime = w.StrartTime,
                                                      CheckinFirstTime = f.CheckinFirstTime,
                                                      DelayTime = f.DelayTime
                                                  }).ToList();
                                foreach (var item in lstcheckin)
                                {
                                    cls_checkin = new Cls_checkin();
                                    cls_checkin.WorkDate = item.WorkDate;
                                    cls_checkin.FullName = item.FullName;
                                    cls_checkin.StaffId = item.StaffId;
                                    cls_checkin.SalonName = item.SalonName;
                                    cls_checkin.StartTime = item.StartTime;
                                    cls_checkin.CheckinFirstTime = item.CheckinFirstTime;
                                    cls_checkin.DelayTime = Convert.ToInt32(item.DelayTime);
                                    cls_checkin.status = item.DelayTime > 0 ? "Muộn" : "Ok";
                                    lstClscheckin.Add(cls_checkin);
                                }
                                break;
                            case 1:
                                 lstcheckin = (from ck in db.CheckinCheckouts
                                                  join f in db.FlowTimeKeepings on ck.FlowTimeKeepingId equals f.Id
                                               join w in db.WorkTimes on f.WorkTimeId equals w.Id
                                               join s in db.Tbl_Salon on ck.SalonId equals s.Id
                                                  join st in db.Staffs on ck.StaffId equals st.Id into a
                                                  from st in a.DefaultIfEmpty()
                                                  where
                                                  s.Publish == true && s.IsDelete == 0
                                                  && ((ck.StaffId == StaffId) || (StaffId == 0))
                                                  && ((ck.SalonId == SalonId) || (SalonId == 0))
                                                  && ((ck.CheckinTime > TimeFrom) && (ck.CheckinTime < TimeTo))
                                                  && f.DelayTime <= 0
                                               select new
                                                  {
                                                      WorkDate = f.WorkDate,
                                                      FullName = st.Fullname,
                                                      StaffId = st.Id,
                                                      SalonName = s.Name,
                                                      StartTime = w.StrartTime,
                                                      CheckinFirstTime = f.CheckinFirstTime,
                                                      DelayTime = f.DelayTime
                                                  }).ToList();
                                foreach (var item in lstcheckin)
                                {
                                    cls_checkin = new Cls_checkin();
                                    cls_checkin.WorkDate = item.WorkDate;
                                    cls_checkin.FullName = item.FullName;
                                    cls_checkin.StaffId = item.StaffId;
                                    cls_checkin.SalonName = item.SalonName;
                                    cls_checkin.StartTime = item.StartTime;
                                    cls_checkin.CheckinFirstTime = item.CheckinFirstTime;
                                    cls_checkin.DelayTime = Convert.ToInt32(item.DelayTime);
                                    cls_checkin.status = item.DelayTime > 0 ? "Muộn" : "Ok";
                                    lstClscheckin.Add(cls_checkin);
                                }
                                break;
                            case 2:
                                lstcheckin = (from ck in db.CheckinCheckouts
                                                  join f in db.FlowTimeKeepings on ck.FlowTimeKeepingId equals f.Id
                                              join w in db.WorkTimes on f.WorkTimeId equals w.Id
                                              join s in db.Tbl_Salon on ck.SalonId equals s.Id
                                                  join st in db.Staffs on ck.StaffId equals st.Id into a
                                                  from st in a.DefaultIfEmpty()
                                                  where
                                                  s.Publish == true && s.IsDelete == 0
                                                  && ((ck.StaffId == StaffId) || (StaffId == 0))
                                                  && ((ck.SalonId == SalonId) || (SalonId == 0))
                                                  && ((ck.CheckinTime > TimeFrom) && (ck.CheckinTime < TimeTo))
                                                  select new
                                                  {
                                                      WorkDate = f.WorkDate,
                                                      FullName = st.Fullname,
                                                      StaffId = st.Id,
                                                      SalonName = s.Name,
                                                      StartTime = w.StrartTime,
                                                      CheckinFirstTime = f.CheckinFirstTime,
                                                      DelayTime = f.DelayTime
                                                  }).ToList();
                                foreach (var item in lstcheckin)
                                {
                                    cls_checkin = new Cls_checkin();
                                    cls_checkin.WorkDate = item.WorkDate;
                                    cls_checkin.FullName = item.FullName;
                                    cls_checkin.StaffId = item.StaffId;
                                    cls_checkin.SalonName = item.SalonName;
                                    cls_checkin.StartTime = item.StartTime;
                                    cls_checkin.CheckinFirstTime = item.CheckinFirstTime;
                                    cls_checkin.DelayTime = Convert.ToInt32(item.DelayTime);
                                    cls_checkin.status = item.DelayTime > 0 ? "Muộn" : "Ok";
                                    lstClscheckin.Add(cls_checkin);
                                }
                                break;
                            default:
                                return null;
                               
                        }
                    }
                }
                return lstClscheckin;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}