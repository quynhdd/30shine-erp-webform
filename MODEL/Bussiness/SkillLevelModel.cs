﻿using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.IO;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;

namespace _30shine.MODEL.BO
{
    public class SkillLevelModel : ISkillLevelModel
    {
        private Solution_30shineEntities db;
        private static SkillLevelModel _Instance;
        public SkillLevelModel()
        {
            this.db = new Solution_30shineEntities();
        }

        /**
         * Get Instance object
         * @return StaffTypeModel
         **/
        public static SkillLevelModel Instance
        {
            get
            {
                if( _Instance == null)
                {
                    _Instance = new SkillLevelModel();
                }
                return _Instance;
            }
        }

        public Tbl_SkillLevel GetById(int Id)
        {
            try
            {
                return this.db.Tbl_SkillLevel.FirstOrDefault(w=>w.Id == Id);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public Tbl_SkillLevel GetByName(string Name)
        {
            return db.Tbl_SkillLevel.FirstOrDefault(w => w.Name == Name );
        }
        
    }
}