﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.MODEL.Interface;

namespace _30shine.MODEL.Bussiness
{
    public class BookingModel : IBookingModel
    {
        /// <summary>
        /// Update stylist change
        /// </summary>
        /// <param name="objBooking"></param>
        /// <returns></returns>
        public BookingChangeStylist UpdateBkStylist(BookingChangeStylist objBooking)
        {
            try
            {
                if (objBooking != null)
                {
                    using (var db = new Solution_30shineEntities())
                    {
                        var bookingUpdates = db.BookingChangeStylists.OrderByDescending(o => o.Id).FirstOrDefault(o => o.BookingTmpId == objBooking.BookingTmpId);
                        var bookingUpdate = db.BookingChangeStylists.OrderByDescending(o => o.Id).FirstOrDefault(o => o.BookingTmpId == objBooking.BookingTmpId && o.CustomerPhone == objBooking.CustomerPhone);
                        bookingUpdate = new BookingChangeStylist();
                        bookingUpdate.BookingTmpId = objBooking.BookingTmpId;
                        bookingUpdate.CustomerPhone = objBooking.CustomerPhone;
                        bookingUpdate.HourId = objBooking.HourId;
                        if (bookingUpdates == null)
                        {
                            bookingUpdate.StylistCurrentId = objBooking.StylistCurrentId;
                            bookingUpdate.StylistChangeId = null;
                        }
                        else if (bookingUpdates.StylistChangeId == null)
                        {
                            bookingUpdate.StylistCurrentId = bookingUpdates.StylistCurrentId;
                            bookingUpdate.StylistChangeId = objBooking.StylistCurrentId;
                        }
                        else
                        {
                            bookingUpdate.StylistCurrentId = bookingUpdates.StylistChangeId;
                            bookingUpdate.StylistChangeId = objBooking.StylistCurrentId;
                        }
                        bookingUpdate.CreatedTime = DateTime.Now;
                        bookingUpdate.ModifiedTime = DateTime.Now;
                        bookingUpdate.Publish = true;
                        bookingUpdate.IsDelete = false;
                        db.BookingChangeStylists.Add(bookingUpdate);
                        db.SaveChanges();
                    }
                }
                return objBooking;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
    }
}