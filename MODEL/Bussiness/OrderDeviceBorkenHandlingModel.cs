using _30shine.GUI.BackEnd.Infrastructure;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace _30shine.MODEL.Bussiness
{
    public class OrderDeviceBorkenHandlingModel : IOrderDeviceBorkenHandlingModel
    {
        /// <summary>
        /// insert
        /// </summary>
        /// <param name="record"></param>
        public void Add(List<OrderBrokenDeviceHandlingSub> list, int creatorId)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    if (list.Count > 0)
                    {
                        var config = db.Tbl_Config.Where(f => f.Key == "order_device_category" && f.IsDelete == 0 && f.Status == 1).ToList();
                        var data = list.Select(c => new OrderBrokenDeviceHandling
                        {
                            OrderStaffId = creatorId,
                            RegionId = c.RegionId,
                            SalonId = c.SalonId,
                            CategoryId = c.CategoryId,
                            TeamId = c.TeamId,
                            BrokenDate = Convert.ToDateTime(c.DateBrokenSub, new CultureInfo("vi-VN")),
                            ImagesBroken = c.ImagesBroken,
                            DescriptionBroken = c.DescriptionBroken,
                            LevelPriority = c.LevelPriority,
                            DesiredDeadline = c.DesiredDeadlineSub != "" ? (DateTime?)Convert.ToDateTime(c.DesiredDeadlineSub, new CultureInfo("vi-VN")) : null,
                            CorrectDeadline = DateTime.Now.AddHours(int.TryParse(config.FirstOrDefault(f => f.Value == c.CategoryId.ToString()).Description, out var result) ? result : 0),
                            StaffIdHandling = 0,
                            StatusHandling = 1,
                            ProcessHandling = 0,
                            StatusCheck = 1,
                            ReasonRevoke = 0,
                            CreatedDate = DateTime.Now,
                            IsDelete = false
                        }).ToList();
                        foreach (var v in data)
                        {
                            var record = new OrderBrokenDeviceHandling();
                            record = v;
                            record.BrokenDate = new DateTime(v.BrokenDate.Value.Year, v.BrokenDate.Value.Month, v.BrokenDate.Value.Day, 7, 0, 0);
                            record.DesiredDeadline = new DateTime(v.DesiredDeadline.Value.Year, v.DesiredDeadline.Value.Month, v.DesiredDeadline.Value.Day, 23, 59, 59);
                            db.OrderBrokenDeviceHandlings.Add(record);
                        }
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ProgressCompleteData> GetListProgression(DateTime TimeFrom, DateTime TimeTo, List<int> SalonId)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    SalonId.Insert(0, 0);
                    string sql = $@"WITH data
                                    AS 
                                    (
                                    SELECT a.Id AS SalonId, a.ShortName AS SalonName, b.Value, b.Label, COUNT(c.Id) AS QuantityOrder FROM dbo.Tbl_Salon a
                                    INNER JOIN dbo.Tbl_Config b ON b.[Key] = 'order_device_category'
                                    LEFT JOIN dbo.OrderBrokenDeviceHandling c ON a.Id = c.SalonId AND c.CategoryId = b.Value AND c.IsDelete = 0 AND c.CreatedDate BETWEEN '{TimeFrom}' AND '{TimeTo}'
                                    WHERE a.IsSalonHoiQuan = 0 AND a.Publish = 1 AND a.IsDelete = 0
                                    GROUP BY a.Id, a.ShortName, b.Value, b.Label
                                    )
                                    SELECT a.Id AS SalonId, a.ShortName AS SalonName, '0' AS TONGVALUE, N'Tổng' AS TONGLABEL, 
	                                       (
		                                       HETHONGDIEN.QuantityOrder + HETHONGNUOC.QuantityOrder + HETHONGDEN.QuantityOrder + GHEGUONGCAT.QuantityOrder + 
		                                       TRANTUONGSANCUA.QuantityOrder + GUONGGOIXA.QuantityOrder + SUNGHH.QuantityOrder + HUTMUN.QuantityOrder +
		                                       DIEUHOA.QuantityOrder + THANGMAY.QuantityOrder + WC.QuantityOrder + LETAN.QuantityOrder + MAYGIAT.QuantityOrder +
		                                       BINHNONGLANH.QuantityOrder + KHUVUCKHAC.QuantityOrder + AMLY.QuantityOrder
	                                       ) AS TONGQUANTITY,
	                                       HETHONGDIEN.Value AS HETHONGDIENVALUE, HETHONGDIEN.Label AS HETHONGDIENLABEL, HETHONGDIEN.QuantityOrder AS HETHONGDIENQUANTITY,
	                                       HETHONGNUOC.Value AS HETHONGNUOCVALUE, HETHONGNUOC.Label AS HETHONGNUOCLABEL, HETHONGNUOC.QuantityOrder AS HETHONGNUOCQUANTITY,
	                                       HETHONGDEN.Value AS HETHONGDENVALUE, HETHONGDEN.Label AS HETHONGDENLABEL, HETHONGDEN.QuantityOrder AS HETHONGDENQUANTITY,
	                                       GHEGUONGCAT.Value AS GHEGUONGCATVALUE, GHEGUONGCAT.Label AS GHEGUONGCATABEL, GHEGUONGCAT.QuantityOrder AS GHEGUONGCATQUANTITY,
	                                       TRANTUONGSANCUA.Value AS TRANTUONGSANCUAVALUE, TRANTUONGSANCUA.Label AS TRANTUONGSANCUALABEL, TRANTUONGSANCUA.QuantityOrder AS TRANTUONGSANCUAQUANTITY,
	                                       GUONGGOIXA.Value AS GUONGGOIXAVALUE, GUONGGOIXA.Label AS GUONGGOIXALABEL, GUONGGOIXA.QuantityOrder AS GUONGGOIXAQUANTITY,
	                                       SUNGHH.Value AS SUNGHHVALUE, SUNGHH.Label AS SUNGHHLABEL, SUNGHH.QuantityOrder AS SUNGHHQUANTITY,
	                                       HUTMUN.Value AS HUTMUNVALUE, HUTMUN.Label AS HUTMUNLABEL, HUTMUN.QuantityOrder AS HUTMUNQUANTITY,
	                                       DIEUHOA.Value AS DIEUHOAVALUE, DIEUHOA.Label AS DIEUHOALABEL, DIEUHOA.QuantityOrder AS DIEUHOAQUANTITY,
	                                       THANGMAY.Value AS THANGMAYVALUE, THANGMAY.Label AS THANGMAYLABEL, THANGMAY.QuantityOrder AS THANGMAYQUANTITY,
	                                       WC.Value AS WCVALUE, WC.Label AS WCLABEL, WC.QuantityOrder AS WCQUANTITY,
	                                       LETAN.Value AS LETANVALUE, LETAN.Label AS LETANLABEL, LETAN.QuantityOrder AS LETANQUANTITY,
	                                       BINHNONGLANH.Value AS BINHNONGLANHVALUE, BINHNONGLANH.Label AS BINHNONGLANHLABEL, BINHNONGLANH.QuantityOrder AS BINHNONGLANHQUANTITY,
	                                       MAYGIAT.Value AS MAYGIATVALUE, MAYGIAT.Label AS MAYGIATLABEL, MAYGIAT.QuantityOrder AS MAYGIATQUANTITY,
	                                       KHUVUCKHAC.Value AS KHUVUCKHACVALUE, KHUVUCKHAC.Label AS KHUVUCKHACLABEL, KHUVUCKHAC.QuantityOrder AS KHUVUCKHACQUANTITY,
                                           AMLY.Value AS AMLYVALUE, AMLY.Label AS AMLYLABEL, AMLY.QuantityOrder AS AMLYQUANTITY
                                    FROM dbo.Tbl_Salon a
                                    INNER JOIN data HETHONGDIEN ON HETHONGDIEN.Value = 1 AND HETHONGDIEN.SalonId = a.Id
                                    INNER JOIN data HETHONGNUOC ON HETHONGNUOC.Value = 2 AND HETHONGNUOC.SalonId = a.Id
                                    INNER JOIN data HETHONGDEN ON HETHONGDEN.Value = 3 AND HETHONGDEN.SalonId = a.Id
                                    INNER JOIN data GHEGUONGCAT ON GHEGUONGCAT.Value = 4 AND GHEGUONGCAT.SalonId = a.Id
                                    INNER JOIN data TRANTUONGSANCUA ON TRANTUONGSANCUA.Value = 5 AND TRANTUONGSANCUA.SalonId = a.Id
                                    INNER JOIN data GUONGGOIXA ON GUONGGOIXA.Value = 6 AND GUONGGOIXA.SalonId = a.Id
                                    INNER JOIN data SUNGHH ON SUNGHH.Value = 7 AND SUNGHH.SalonId = a.Id
                                    INNER JOIN data HUTMUN ON HUTMUN.Value = 8 AND HUTMUN.SalonId = a.Id
                                    INNER JOIN data DIEUHOA ON DIEUHOA.Value = 9 AND DIEUHOA.SalonId = a.Id
                                    INNER JOIN data THANGMAY ON THANGMAY.Value = 10 AND THANGMAY.SalonId = a.Id
                                    INNER JOIN data WC ON WC.Value = 11 AND WC.SalonId = a.Id
                                    INNER JOIN data LETAN ON LETAN.Value = 12 AND LETAN.SalonId = a.Id
                                    INNER JOIN data BINHNONGLANH ON BINHNONGLANH.Value = 13 AND BINHNONGLANH.SalonId = a.Id
                                    INNER JOIN data MAYGIAT ON MAYGIAT.Value = 14 AND MAYGIAT.SalonId = a.Id
                                    INNER JOIN data KHUVUCKHAC ON KHUVUCKHAC.Value = 15 AND KHUVUCKHAC.SalonId = a.Id
                                    INNER JOIN data AMLY ON AMLY.Value = 16 AND AMLY.SalonId = a.Id
                                    ORDER BY a.Id";
                    var list = db.Database.SqlQuery<ProgressCompleteData>(sql).ToList();
                    var listTotal = list.Select(c => new ProgressCompleteData
                    {
                        SalonId = 0,
                        SalonName = "30shine",
                        BINHNONGLANHLABEL = c.BINHNONGLANHLABEL,
                        BINHNONGLANHQUANTITY = list.Where(w => w.BINHNONGLANHVALUE == c.BINHNONGLANHVALUE).Sum(s => s.BINHNONGLANHQUANTITY),
                        BINHNONGLANHVALUE = c.BINHNONGLANHVALUE,
                        DIEUHOALABEL = c.DIEUHOALABEL,
                        DIEUHOAQUANTITY = list.Where(w => w.DIEUHOAVALUE == c.DIEUHOAVALUE).Sum(s => s.DIEUHOAQUANTITY),
                        DIEUHOAVALUE = c.DIEUHOAVALUE,
                        GHEGUONGCATABEL = c.GHEGUONGCATABEL,
                        GHEGUONGCATQUANTITY = list.Where(w => w.GHEGUONGCATVALUE == c.GHEGUONGCATVALUE).Sum(s => s.GHEGUONGCATQUANTITY),
                        GHEGUONGCATVALUE = c.GHEGUONGCATVALUE,
                        GUONGGOIXALABEL = c.GUONGGOIXALABEL,
                        GUONGGOIXAQUANTITY = list.Where(w => w.GUONGGOIXAVALUE == c.GUONGGOIXAVALUE).Sum(s => s.GUONGGOIXAQUANTITY),
                        GUONGGOIXAVALUE = c.GUONGGOIXAVALUE,
                        HETHONGDENLABEL = c.HETHONGDENLABEL,
                        HETHONGDENQUANTITY = list.Where(w => w.HETHONGDENVALUE == c.HETHONGDENVALUE).Sum(s => s.HETHONGDENQUANTITY),
                        HETHONGDENVALUE = c.HETHONGDENVALUE,
                        HETHONGDIENLABEL = c.GHEGUONGCATABEL,
                        HETHONGDIENQUANTITY = list.Where(w => w.HETHONGDIENVALUE == c.HETHONGDIENVALUE).Sum(s => s.HETHONGDIENQUANTITY),
                        HETHONGDIENVALUE = c.HETHONGDIENVALUE,
                        HETHONGNUOCLABEL = c.HETHONGNUOCLABEL,
                        HETHONGNUOCQUANTITY = list.Where(w => w.HETHONGNUOCVALUE == c.HETHONGNUOCVALUE).Sum(s => s.HETHONGNUOCQUANTITY),
                        HETHONGNUOCVALUE = c.HETHONGNUOCVALUE,
                        HUTMUNLABEL = c.HUTMUNLABEL,
                        HUTMUNQUANTITY = list.Where(w => w.HUTMUNVALUE == c.HUTMUNVALUE).Sum(s => s.HUTMUNQUANTITY),
                        HUTMUNVALUE = c.HUTMUNVALUE,
                        KHUVUCKHACLABEL = c.KHUVUCKHACLABEL,
                        KHUVUCKHACQUANTITY = list.Where(w => w.KHUVUCKHACVALUE == c.KHUVUCKHACVALUE).Sum(s => s.KHUVUCKHACQUANTITY),
                        KHUVUCKHACVALUE = c.KHUVUCKHACVALUE,
                        LETANLABEL = c.LETANLABEL,
                        LETANQUANTITY = list.Where(w => w.LETANVALUE == c.LETANVALUE).Sum(s => s.LETANQUANTITY),
                        LETANVALUE = c.LETANVALUE,
                        SUNGHHLABEL = c.SUNGHHLABEL,
                        SUNGHHQUANTITY = list.Where(w => w.SUNGHHVALUE == c.SUNGHHVALUE).Sum(s => s.SUNGHHQUANTITY),
                        SUNGHHVALUE = c.SUNGHHVALUE,
                        THANGMAYLABEL = c.THANGMAYLABEL,
                        THANGMAYQUANTITY = list.Where(w => w.THANGMAYVALUE == c.THANGMAYVALUE).Sum(s => s.THANGMAYQUANTITY),
                        THANGMAYVALUE = c.THANGMAYVALUE,
                        TONGLABEL = c.TONGLABEL,
                        TONGQUANTITY = list.Where(w => w.TONGVALUE == c.TONGVALUE).Sum(s => s.TONGQUANTITY),
                        TONGVALUE = c.TONGVALUE,
                        TRANTUONGSANCUALABEL = c.TRANTUONGSANCUALABEL,
                        TRANTUONGSANCUAQUANTITY = list.Where(w => w.TRANTUONGSANCUAVALUE == c.TRANTUONGSANCUAVALUE).Sum(s => s.TRANTUONGSANCUAQUANTITY),
                        TRANTUONGSANCUAVALUE = c.TRANTUONGSANCUAVALUE,
                        WCLABEL = c.WCLABEL,
                        WCQUANTITY = list.Where(w => w.WCVALUE == c.WCVALUE).Sum(s => s.WCQUANTITY),
                        WCVALUE = c.WCVALUE,
                        MAYGIATLABEL = c.MAYGIATLABEL,
                        MAYGIATQUANTITY = list.Where(w => w.MAYGIATVALUE == c.MAYGIATVALUE).Sum(s => s.MAYGIATQUANTITY),
                        MAYGIATVALUE = c.MAYGIATVALUE,
                        AMLYLABEL = c.AMLYLABEL,
                        AMLYQUANTITY = list.Where(w => w.AMLYVALUE == c.AMLYVALUE).Sum(s => s.AMLYQUANTITY),
                        AMLYVALUE = c.AMLYVALUE
                    }).FirstOrDefault();
                    list.Insert(0, listTotal);
                    return list.Where(w => SalonId.Contains(w.SalonId)).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Progression> GetProgression(DateTime TimeFrom, DateTime TimeTo, string region, List<int> salon)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var response = new ResponseData();
                    response.progressions = new List<Progression>();
                    var list = db.OrderBrokenDeviceHandlings.Where(w => w.CreatedDate >= TimeFrom && TimeTo > w.CreatedDate).ToList();
                    //all
                    var listAll = list.Select(c => new Progression
                    {
                        RegionName = "30Shine",
                        TotalCorrect = list.Where(w => w.ReasonRevoke == 0).Count(),
                        DeadlineCorrect = list.Where(w => w.StatusCheck == 2 && w.StatusCheckTime <= w.CorrectDeadline).Count(),
                        PercentCorrect = Convert.ToDouble(
                                             Convert.ToDouble(list.Where(w => w.ReasonRevoke == 0).Count()) > 0 ?
                                             Convert.ToDouble(list.Where(w => w.StatusCheck == 2 && w.StatusCheckTime <= w.CorrectDeadline).Count()) /
                                             Convert.ToDouble(list.Where(w => w.ReasonRevoke == 0).Count()) * 100 : 0
                                         )
                    }).FirstOrDefault();

                    if (listAll != null)
                    {
                        response.progressions.Insert(0, listAll);
                    }

                    var listByRegion = list.Where(w => salon.Contains(w.SalonId.Value)).Select(c => new Progression
                    {
                        RegionName = region,
                        TotalCorrect = list.Where(w => w.ReasonRevoke == 0 && salon.Contains(w.SalonId.Value)).Count(),
                        DeadlineCorrect = list.Where(w => w.StatusCheck == 2 && w.StatusCheckTime <= w.CorrectDeadline && salon.Contains(w.SalonId.Value)).Count(),
                        PercentCorrect = Convert.ToDouble(
                                             Convert.ToDouble(list.Where(w => w.ReasonRevoke == 0 && salon.Contains(w.SalonId.Value)).Count()) > 0 ?
                                             Convert.ToDouble(list.Where(w => w.StatusCheck == 2 && w.StatusCheckTime <= w.CorrectDeadline && salon.Contains(w.SalonId.Value)).Count()) /
                                             Convert.ToDouble(list.Where(w => w.ReasonRevoke == 0 && salon.Contains(w.SalonId.Value)).Count()) * 100 : 0
                                         )
                    }).FirstOrDefault();

                    if (listByRegion != null)
                    {
                        response.progressions.Insert(1, listByRegion);
                    }
                    return response.progressions;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        private Solution_30shineEntities db;
        public OrderDeviceBorkenHandlingModel()
        {
            this.db = new Solution_30shineEntities();
        }
        public List<OrderBrokenDeviceHandling> GetList(Expression<Func<OrderBrokenDeviceHandling, bool>> expression)
        {
            return db.OrderBrokenDeviceHandlings.Where(expression).ToList();
        }
    }
}