﻿using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _30shine.MODEL.Bussiness
{
    public class PermissionModel_V2 : IPermission_V2
    {
        private Solution_30shineEntities db;
        public PermissionModel_V2()
        {
            this.db = new Solution_30shineEntities();
        }
        public List<Tbl_Permission> GetListPermission()
        {
            try
            {
                return this.db.Tbl_Permission.ToList();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool GetActionByActionNameAndPageId(string actionName, string pageId, int staffId)
        {
            try
            {
                if (staffId == 0)
                {
                    return true;
                }

                var aID = db.Tbl_Permission_Action.FirstOrDefault(a => a.aName == actionName);
                if (aID == null)
                {
                    return false;
                }
                var _aID = Convert.ToInt32(aID.aID);
                var result = (from map in db.Tbl_Permission_Map_V2
                              join m in db.Tbl_Permission_Menu on map.mID equals m.mID
                              join roles in db.Tbl_Permission_Staff_Roles on map.pID equals roles.PermissionId
                              where roles.StaffId == staffId && map.aID == _aID && m.mPageId == pageId
                              orderby map.aID descending
                              select map).FirstOrDefault();
                if (result != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public bool CheckAction(string actionName, string pageId, int staffId)
        {
            try
            {
                if (staffId == 0)
                {
                    return true;
                }

                var aID = db.Tbl_Permission_Action.FirstOrDefault(a => a.aName == actionName);
                if (aID == null)
                {
                    return false;
                }
                var _aID = Convert.ToInt32(aID.aID);
                var result = (from map in db.Tbl_Permission_Map_V2
                              join m in db.Tbl_Permission_Menu on map.mID equals m.mID
                              join roles in db.Tbl_Permission_Staff_Roles on map.pID equals roles.StaffId
                              where roles.StaffId == staffId && map.aID == _aID && m.mPageId == pageId
                              select map).FirstOrDefault();
                if (result != null)
                {
                    if (result.aStatus == true)
                    {
                        return true;
                    }
                    else return false;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Tbl_Permission_Action> ListAction()
        {
            throw new NotImplementedException();
        }
        public Tbl_Permission GetPermissionById(int id)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// build Menu
        /// </summary>
        /// <param name="staffId"></param>
        /// <returns></returns>
        public string GetMenu_V2_2(int staffId)
        {
            using (var db = new Solution_30shineEntities())
            {
                var menu = "";
                //var listPr = new List<Tbl_Permission_Menu>();
                if (staffId == 0)
                {
                    var listPr = (from m in db.Tbl_Permission_Menu_V2
                                  where m.mParentID == 0 && m.MenuShow == true
                                  select m).ToList();
                    foreach (var item in listPr)
                    {
                        menu += "<li>";
                        menu += "<a href='" + item.Url_Rewrite + "'";
                        if (item.Target != null)
                        {
                            menu += "target = '" + item.Target + "'";
                        }
                        menu += ">" + item.mName + "</a>";
                        var listChild = (from m in db.Tbl_Permission_Menu_V2
                                         where m.mParentID == item.mID && m.MenuShow == true
                                         select m).ToList();
                        if (listChild != null)
                        {
                            menu += "<ul class='ul-sub-menu'>";
                            foreach (var itemChild in listChild)
                            {
                                menu += "<li>";
                                menu += "<a href='" + itemChild.Url_Rewrite + "'";
                                if (itemChild.Target != null)
                                {
                                    menu += "target = '" + itemChild.Target + "'";
                                }
                                menu += ">" + itemChild.mName + "</a>";
                                var listChild1 = (from m in db.Tbl_Permission_Menu_V2
                                                  where m.mParentID == itemChild.mID && m.MenuShow == true
                                                  select m).ToList();
                                if (listChild1 != null)
                                {
                                    menu += "<ul class='ul-sub2-menu'>";
                                    foreach (var itemChild1 in listChild1)
                                    {
                                        menu += "<li>";
                                        menu += "<a href='" + itemChild1.Url_Rewrite + "'";
                                        if (itemChild1.Target != null)
                                        {
                                            menu += "target = '" + itemChild1.Target + "'";
                                        }
                                        menu += ">" + itemChild1.mName + "</a>";
                                        var listChild2 = (from m in db.Tbl_Permission_Menu_V2
                                                          where m.mParentID == itemChild1.mID && m.MenuShow == true
                                                          select m).ToList();
                                        if (listChild2 != null)
                                        {
                                            menu += "<ul class='ul-sub2-menu'>";
                                            foreach (var itemChild2 in listChild2)
                                            {
                                                menu += "<li>";
                                                menu += "<a href='" + itemChild2.Url_Rewrite + "'";
                                                if (itemChild2.Target != null)
                                                {
                                                    menu += "target = '" + itemChild2.Target + "'";
                                                }
                                                menu += ">" + itemChild2.mName + "</a>";
                                                menu += "</li>";
                                            }
                                            menu += "</ul>";
                                        }
                                        menu += "</li>";
                                    }
                                    menu += "</ul>";
                                }
                                menu += "</li>";
                            }
                            menu += "</ul>";
                        }
                        menu += "</li>";
                    }
                }
                else
                {
                    var listMenuShow = GetMenu(staffId);
                    var listParent = listMenuShow.Where(m => m.mParentID == 0 && m.MenuShow == true).ToList();
                    foreach (var item in listParent)
                    {
                        menu += "<li><a href='" + item.Url_Rewrite + "'";
                        if (item.Target != null)
                        {
                            menu += "target='" + item.Target + "'";
                        }
                        else menu += "";
                        menu += ">" + item.mName + "</a>";
                        var listChild = listMenuShow.Where(m => m.mParentID == item.mID && m.MenuShow == true).ToList();
                        if (listChild != null)
                        {
                            menu += "<ul class='ul-sub-menu'>";
                            foreach (var itemChild in listChild)
                            {
                                menu += "<li>";
                                menu += "<a href='" + itemChild.Url_Rewrite + "'";
                                if (itemChild.Target != null)
                                {
                                    menu += "target='" + itemChild.Target + "'";
                                }
                                else menu += "";
                                menu += ">" + itemChild.mName + "</a>";
                                var listChild1 = listMenuShow.Where(m => m.mParentID == itemChild.mID && m.MenuShow == true).ToList();
                                if (listChild1 != null)
                                {
                                    menu += "<ul class='ul-sub2-menu'>";
                                    foreach (var itemChild1 in listChild1)
                                    {
                                        menu += "<li>";
                                        menu += "<a href='" + itemChild1.Url_Rewrite + "'";
                                        if (itemChild1.Target != null)
                                        {
                                            menu += "target='" + itemChild1.Target + "'";
                                        }
                                        else menu += "";
                                        menu += ">" + itemChild1.mName + "</a>";
                                        var listChild2 = listMenuShow.Where(m => m.mParentID == itemChild1.mID && m.MenuShow == true).ToList();
                                        if (listChild2 != null)
                                        {
                                            menu += "<ul class='ul-sub2-menu'>";
                                            foreach (var itemChild2 in listChild2)
                                            {
                                                menu += "<li>";
                                                menu += "<a href='" + itemChild2.Url_Rewrite + "'";
                                                if (itemChild2.Target != null)
                                                {
                                                    menu += "target='" + itemChild2.Target + "'";
                                                }
                                                else menu += "";
                                                menu += ">" + itemChild2.mName + "</a>";
                                                menu += "</li>";
                                            }
                                            menu += "</ul>";
                                        }
                                        menu += "</li>";
                                    }
                                    menu += "</ul>";
                                }
                                menu += "</li>";
                            }
                            menu += "</ul>";
                        }
                        menu += "</li>";
                    }
                }

                return menu;
            }
        }
        public Tbl_Permission GetPermissionByName(string permissionName)
        {
            try
            {
                return this.db.Tbl_Permission.FirstOrDefault(w => w.pName == permissionName);
            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// return all menu of permissions by StaffId
        /// </summary>
        /// <param name="StaffId"></param>
        /// <returns></returns>
        public List<Store_Permission_GetMenu_By_Staff_Result> GetMenu(int StaffId)
        {
            //List<cls_Permission_Menu> listMenu = new List<cls_Permission_Menu>();
            //List<cls_Permission_Menu> listMenu2 = new List<cls_Permission_Menu>();
            //foreach (var v in GetPermission(StaffId))
            //{
            var lsMenu = db.Store_Permission_GetMenu_By_Staff(StaffId).ToList();

            //foreach (var item in lsMenu)
            //{
            //    cls_Permission_Menu pMenu = new cls_Permission_Menu();
            //    pMenu.Id = item.mID;
            //    pMenu.ParentId = item.mParentID;
            //    pMenu.MenuName = item.mName;
            //    pMenu.Url = item.Url_Rewrite;
            //    pMenu.MenuShow = item.MenuShow;
            //    pMenu.CssTag = item.mClassTag;
            //    pMenu.Target = item.Target;
            //    pMenu.Icon = item.mIcon;
            //    pMenu.PageId = item.mPageId;
            //    listMenu.Add(pMenu);
            //}
            //}
            return lsMenu;
        }
        ///// <summary>
        ///// lấy tất cả Nhóm quyền theo ID nhân viên
        ///// </summary>
        ///// <param name="staffId"></param>
        ///// <returns></returns>
        //public List<cls_Permission> GetPermission(int staffId)
        //{
        //    Solution_30shineEntities db = new Solution_30shineEntities();
        //    var list = new List<cls_Permission>();
        //    list = (from t in db.Tbl_Permission_Staff_Roles
        //        where t.StaffId == staffId
        //        select new cls_Permission()
        //        {
        //            Id = t.Id,
        //            PermissionId = t.PermissionId,
        //            Name = t.PermissionName
        //        }).ToList();
        //    return list;
        //}
        //public List<Store_Permission_TreeView_Menu_Result> GetAllMenu(int parentId)
        //{
        //    Solution_30shineEntities db = new Solution_30shineEntities();
        //    return db.Store_Permission_TreeView_Menu(parentId).ToList();
        //}
        public List<Tbl_Permission_Menu> GetModule()
        {
            Solution_30shineEntities db = new Solution_30shineEntities();
            return db.Tbl_Permission_Menu.Where(m => m.mParentID == 0).ToList();
        }

    }
}