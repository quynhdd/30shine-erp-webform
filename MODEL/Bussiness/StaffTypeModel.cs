﻿using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.IO;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;

namespace _30shine.MODEL.BO
{
    public class StaffTypeModel : IStaffTypeModel
    {
        private Solution_30shineEntities db;
        private static StaffTypeModel _Instance;
        public StaffTypeModel()
        {
            this.db = new Solution_30shineEntities();
        }

        /**
         * Get Instance object
         * @return StaffTypeModel
         **/
        public static StaffTypeModel Instance
        {
            get
            {
                if( _Instance == null)
                {
                    _Instance = new StaffTypeModel();
                }
                return _Instance;
            }
        }

        public Staff_Type GetById(int Id)
        {
            try
            {
                return this.db.Staff_Type.FirstOrDefault(w=>w.Id == Id);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public Staff_Type GetByMeta(string Meta)
        {
            return db.Staff_Type.FirstOrDefault(w => w.Meta == Meta );
        }
        
    }
}