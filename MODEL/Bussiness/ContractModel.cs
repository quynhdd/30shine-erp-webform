﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using _30shine.Helpers;
using System.Data.Entity.Migrations;
using _30shine.MODEL.CustomClass;
namespace _30shine.MODEL.Bussiness
{
    public class ContractModel : IContract
    {
        private Solution_30shineEntities db;
        public ContractModel()
        {
            db = new Solution_30shineEntities();
        }
        public Msg Update(Contract objContract)
        {
            var result = new Msg();
            if (objContract != null)
            {
                try
                {
                    var record = db.Contracts.Where(r => r.Id == objContract.Id && r.IsDelete == false).FirstOrDefault();
                    if (record != null)
                    {
                        record.ModifiedTime = DateTime.Now;
                        record.Name = objContract.Name;
                        record.PathContract = objContract.PathContract;
                        record.TypeContract = objContract.TypeContract;
                        record.Content = objContract.Content;
                        record.DepartmentId = objContract.DepartmentId;
                        record.DescriptionContract = objContract.DescriptionContract;
                        db.Contracts.AddOrUpdate(record);
                        var exc = db.SaveChanges();
                        if (exc > 0)
                        {
                            result.success = true;
                            result.msg = "Cập nhật thành công!";
                        }
                        else
                        {
                            result.success = false;
                            result.msg = "Không lưu được dữ liệu";
                        }
                    }
                    else
                    {
                        result.success = false;
                        result.msg = "Không tìm thấy hợp đồng !";
                    }
                }
                catch (Exception ex)
                {
                    result.success = false;
                    result.msg = "Đã có lỗi xảy ra !";
                }
            }
            else
            {
                result.success = false;
                result.msg = "Tham số truyền vào không hợp lệ!";
            }
            return result;
        }
        public Msg Insert(Contract objContract)
        {
            var result = new Msg();
            if (objContract != null)
            {
                try
                {
                    db.Contracts.Add(objContract);
                    var exc = db.SaveChanges();
                    if (exc > 0)
                    {
                        result.success = true;
                        result.msg = "Thành công!";
                    }
                    else
                    {
                        result.success = false;
                        result.msg = "Không lưu được dữ liệu!";
                    }
                }
                catch (Exception ex)
                {
                    result.success = false;
                    result.msg = "Đã có lỗi xảy ra!";
                }
            }
            else
            {
                result.success = false;
                result.msg = "Tham số truyên vào không hợp lệ";

            }
            return result;
        }
        public Msg Delete(int contractId)
        {
            var result = new Msg();
            if (contractId != 0)
            {
                try
                {
                    var record = db.Contracts.Where(r => r.IsDelete == false && r.Id == contractId).FirstOrDefault();
                    if (record != null)
                    {
                        record.IsDelete = true;
                        db.Contracts.AddOrUpdate(record);
                        var exc = db.SaveChanges();
                        if (exc > 0)
                        {
                            result.success = true;
                            result.msg = "Thành công!";
                        }
                        else
                        {
                            result.success = false;
                            result.msg = "Không lưu được dữ liệu";
                        }
                    }
                    else
                    {
                        result.success = false;
                        result.msg = "Không tìm thấy dữ liệu";
                    }
                }
                catch (Exception ex)
                {
                    result.success = false;
                    result.msg = "Đã có lỗi xảy ra!";
                }
            }
            else
            {
                result.success = false;
                result.msg = "Tham số truyên vào không hợp lệ";
            }
            return result;
        }
        public Msg Getlist(int contractType, int departmentId)
        {
            var result = new Msg();
            try
            {
                var listTemp = (from a in db.Contracts
                                join b in db.Staff_Type on a.DepartmentId equals b.Id
                                where a.IsDelete == false &&
                                      ((a.TypeContract == contractType && contractType > 0) || (contractType == 0)) &&
                                      ((a.DepartmentId == departmentId && departmentId > 0) || (departmentId == 0))
                                select new ContractClass
                                {
                                    Id = a.Id,
                                    Name = a.Name,
                                    Content = a.Content,
                                    DepartmentId = a.DepartmentId,
                                    TypeContract = a.TypeContract,
                                    PathContract = a.PathContract,
                                    DescriptionContract = a.DescriptionContract,
                                    DepartmentName = b.Name
                                }).ToList();
                var listConfigTemp = db.Tbl_Config.Where(r => r.IsDelete == 0 && r.Key == "hop_dong")
                    .Select(w => new
                    {
                        Name = w.Label,
                        Value = w.Value
                    }).ToList();
                var listConfig = listConfigTemp.Select(
                    r => new
                    {
                        Name = r.Name,
                        Value = Convert.ToInt32(r.Value)
                    }).ToList();
                var list = (from a in listTemp
                            join b in listConfig on a.TypeContract equals b.Value
                            select new ContractClass
                            {
                                Id = a.Id,
                                Name = a.Name,
                                Content = a.Content,
                                DepartmentId = a.DepartmentId,
                                TypeContract = a.TypeContract,
                                PathContract = a.PathContract,
                                DescriptionContract = a.DescriptionContract,
                                DepartmentName = a.DepartmentName,
                                ContractNameType = b.Name
                            }).ToList();

                if (list.Count > 0)
                {
                    result.success = true;
                    result.msg = "Thành công";
                    result.data = list;
                }
                else
                {
                    result.success = false;
                    result.msg = "Không tìm thấy dữ liệu";
                }
            }
            catch (Exception ex)
            {
                result.success = false;
                result.msg = "Đã có lỗi xảy ra!";
            }
            return result;
        }
        public Msg GetById(int contractId)
        {
            var result = new Msg();
            if (contractId != 0)
            {
                try
                {
                    var record = db.Contracts.Where(r => r.IsDelete == false && r.Id == contractId).FirstOrDefault();
                    if (record != null)
                    {
                        result.success = true;
                        result.msg = "Thành công";
                        result.data = record;
                    }
                    else
                    {
                        result.success = false;
                        result.msg = "Không tìm thấy dữ liệu";
                    }
                }
                catch (Exception ex)
                {
                    result.success = false;
                    result.msg = "Đã có lỗi xảy ra!";
                }
            }
            else
            {
                result.success = false;
                result.msg = "Tham số truyền vào không hợp lệ";
            }
            return result;
        }
        public Msg checkDuplicateContract(int departmentId, int contractType)
        {
            var result = new Msg();
            if (contractType >= 0)
            {
                try
                {
                    var record = db.Contracts.Where(r => r.IsDelete == false && r.DepartmentId == departmentId && r.TypeContract == contractType).FirstOrDefault();
                    if (record != null)
                    {
                        result.success = true;
                        result.msg = "Hợp đồng đã có trong cơ sở dữ liệu";
                    }
                }
                catch (Exception ex)
                {
                    result.success = false;
                    result.msg = "Đã có lỗi xảy ra";
                }
            }
            else
            {
                result.success = false;
                result.msg = "Tham số truyền vào không hợp lệ";
            }
            return result;
        }
    }
}