﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;

namespace _30shine.MODEL.Bussiness
{
    public class FormMistakeModel : IFormMistakeModel
    {
        private Solution_30shineEntities db;
        /// <summary>
        /// Constructor
        /// </summary>
        public FormMistakeModel()
        {
            this.db = new Solution_30shineEntities();
        }
        public List<tbl_Form_Misstake> GetList5C()
        {
            List<tbl_Form_Misstake> ListFormMitake = new List<tbl_Form_Misstake>();
            try
            {
                ListFormMitake = db.tbl_Form_Misstake.Where(r=>r.isDelete==false && r.Slug=="luong_5c").ToList();
            }
            catch (Exception ex)
            {

            }
            return ListFormMitake;
        }
    }
}