﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;

namespace _30shine.MODEL.Bussiness
{
    public class StaffFluctuationModel : IStaffFluctuationModel
    {
        public Staff_Fluctuations AddOrUpdateRecruitment(Staff objStaff)
        {
            var objStaffFluc = new Staff_Fluctuations();
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    if (objStaff.isAccountLogin != 1)
                    {
                        objStaffFluc.CreatedDate = DateTime.Now;
                        if (objStaff.SourceId == 2)
                        {
                            objStaffFluc.StatusId = 7;
                        }
                        else
                        {
                            objStaffFluc.StatusId = 8;
                        }
                        objStaffFluc.StaffId = objStaff.Id;
                        objStaffFluc.StaffName = objStaff.Fullname;
                        objStaffFluc.StaffType = Convert.ToByte(objStaff.Type);
                        objStaffFluc.SalonCurrentId = objStaff.SalonId;
                        objStaffFluc.SalonChangeId = objStaff.SalonId;
                        objStaffFluc.Description = "";
                        objStaffFluc.IsDetele = false;
                        objStaffFluc.NumberReceive = 0;
                        objStaffFluc.NumberTransfer = 0;
                        db.Staff_Fluctuations.Add(objStaffFluc);
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            { }
            return objStaffFluc;
        }

        public Staff_Fluctuations UpdateStaff(Staff objStaff)
        {
            var objStaffFluc = new Staff_Fluctuations();
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var queryStaff = db.Staffs.FirstOrDefault(w => w.Id == objStaff.Id);
                    if (queryStaff != null)
                    {
                        //Check Salon
                        var objSalon = db.Tbl_Salon.FirstOrDefault(f => f.Id == objStaff.SalonId);
                        var staffSalonHQ = db.Tbl_Salon.FirstOrDefault(f => f.Id == queryStaff.SalonId);
                        //I.salon không thay đổi nhưng Active thay đổi hoặc salon thay đổi nhưng Active cũng thay đổi
                        if (queryStaff.SalonId != objStaff.SalonId && queryStaff.Active == objStaff.Active)
                        {
                            //Nếu không phải hội quán
                            if (objSalon.IsSalonHoiQuan == false)
                            {
                                objStaffFluc.StatusId = 16;
                                objStaffFluc.NumberTransfer = -1;
                                objStaffFluc.NumberReceive = 1;
                                if (staffSalonHQ.IsSalonHoiQuan == true)
                                {
                                    objStaffFluc.StatusId = 7;
                                    objStaffFluc.NumberTransfer = 0;
                                }
                            }
                            //Nguồn S4M chuyển qua hội quán
                            else if (queryStaff.S4MClassId != 0 || queryStaff.S4MClassId != null || queryStaff.SourceId == 2 && objSalon.IsSalonHoiQuan == true)
                            {
                                objStaffFluc.StatusId = 17;
                                objStaffFluc.NumberTransfer = -1;
                                objStaffFluc.NumberReceive = 0;
                            }
                            //Nguồn ngoài chuyển qua hội quán
                            else if (queryStaff.S4MClassId == 0 || queryStaff.S4MClassId == null && queryStaff.SourceId != 2 && objSalon.IsSalonHoiQuan == true)
                            {
                                objStaffFluc.StatusId = 18;
                                objStaffFluc.NumberTransfer = -1;
                                objStaffFluc.NumberReceive = 0;
                            }
                            //Hội quán chuyển về Salon
                            else if (staffSalonHQ.IsSalonHoiQuan == true && objSalon.IsSalonHoiQuan == false)
                            {
                                objStaffFluc.StatusId = 7;
                                objStaffFluc.NumberReceive = 1;
                                objStaffFluc.NumberTransfer = 0;
                            }
                            objStaffFluc.SalonCurrentId = queryStaff.SalonId;
                            objStaffFluc.SalonChangeId = objStaff.SalonId;
                            objStaffFluc.CreatedDate = DateTime.Now;
                            objStaffFluc.StaffType = Convert.ToByte(objStaff.Type);
                            objStaffFluc.StaffId = objStaff.Id;
                            objStaffFluc.StaffName = objStaff.Fullname;
                            objStaffFluc.Description = "";
                            objStaffFluc.IsDetele = false;
                        }
                        //II.Salon thay đổi nhưng Active không thay đổi
                        else if (queryStaff.SalonId == objStaff.SalonId && queryStaff.Active != objStaff.Active || queryStaff.SalonId != objStaff.SalonId && queryStaff.Active == objStaff.Active || queryStaff.SalonId != objStaff.SalonId && queryStaff.Active != objStaff.Active)
                        {

                            //TH1: chuyển từ trạng thái đã nghỉ sang hoạt động
                            if (queryStaff.Active == 0 && objStaff.Active == 1)
                            {
                                objStaffFluc.StatusId = 14;
                            }
                            //TH2: chuyển từ trạng thái nghỉ tạm thời sang hoạt động
                            else if (queryStaff.Active == 2 && objStaff.Active == 1)
                            {
                                objStaffFluc.StatusId = 13;
                            }
                            //TH3: chuyển từ trạng thái đang hoạt động sang đã nghỉ hoặc nghỉ tạm thời sang đã nghỉ
                            else if (queryStaff.Active == 1 && objStaff.Active == 0 || queryStaff.Active == 2 && objStaff.Active == 0)
                            {
                                objStaffFluc.StatusId = 12;
                            }
                            //TH4: chuyển từ trạng thái đang hoạt động sang nghỉ tạm thời hoặc đã nghỉ sang nghỉ tạm thời
                            else if (queryStaff.Active == 1 && objStaff.Active == 2 || queryStaff.Active == 0 && objStaff.Active == 2)
                            {
                                objStaffFluc.StatusId = 11;
                            }
                            else
                            {
                                objStaffFluc.StatusId = 7;
                            }
                            //Salon cũ giữ nguyên, salon chuyển có thể thay đổi hoặc không thay đổi
                            objStaffFluc.SalonCurrentId = queryStaff.SalonId;
                            if (queryStaff.SalonId == objStaff.SalonId)
                            {
                                objStaffFluc.SalonChangeId = 0;
                            }
                            else
                            {
                                //Điều chuyện -1 và nhận lại +1;
                                objStaffFluc.SalonChangeId = objStaff.SalonId;
                                objStaffFluc.NumberReceive = 1;
                                objStaffFluc.NumberTransfer = -1;
                            }

                            objStaffFluc.CreatedDate = DateTime.Now;
                            objStaffFluc.StaffType = Convert.ToByte(objStaff.Type);
                            objStaffFluc.StaffId = objStaff.Id;
                            objStaffFluc.StaffName = objStaff.Fullname;
                            objStaffFluc.Description = "";
                            objStaffFluc.IsDetele = false;
                        }
                        if (objStaffFluc.StaffId != null && objStaffFluc.StatusId != null)
                        {
                            db.Staff_Fluctuations.Add(objStaffFluc);
                            db.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            { }
            return objStaffFluc;
        }
    }
}