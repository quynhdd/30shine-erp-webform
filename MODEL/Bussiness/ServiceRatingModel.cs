﻿using _30shine.MODEL.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.CustomClass;
using System.Linq.Expressions;
using System.Data.Objects.SqlClient;
using System.Threading.Tasks;
using System.Data.Entity.Migrations;

namespace _30shine.MODEL.BO
{
    public class ServiceRatingModel
    {
        private static ServiceRatingModel _Instance;
        private Solution_30shineEntities db;
        public delegate bool GetRatingConditionCallback(ServiceRatingView J);

        public ServiceRatingModel()
        {
            this.db = new Solution_30shineEntities();
        }

        public void Add(Service_Rating bill)
        {

        }

        public bool Delete(int ID)
        {
            throw new NotImplementedException();
        }

        public void AddRelationship(BillService billService)
        {
            try
            {
                if (billService != null)
                {
                    var configRating = db.Service_Rating.Where(r => r.Slug == "khong-danh-gia").FirstOrDefault();
                    // Nếu không tìm được slug thì mặc định id = 0
                    if (configRating == null)
                    {
                        configRating = new Service_Rating();
                        configRating.Id = 0;
                    }
                    // Lưu bill khách không đánh giá
                    var recordRelate = db.Service_Rating_Relationship.Where(r => r.BillServiceId == billService.Id).FirstOrDefault();
                    // chưa có thì thêm mới
                    if (recordRelate == null)
                    {
                        var record = new Service_Rating_Relationship();
                        record.BillServiceId = billService.Id;
                        record.SalonId = billService.SalonId;
                        record.ServiceRatingId = configRating.Id;
                        db.Service_Rating_Relationship.Add(record);
                    }
                    var exc = db.SaveChanges();
                }
            }
            catch (Exception ex)
            {

            }
        }
        public void DeleteRelationship(int billServiceId)
        {
            try
            {
                if (billServiceId > 0)
                {
                    var listRelate = db.Service_Rating_Relationship.Where(r => r.BillServiceId == billServiceId).ToList();
                    if (listRelate.Count > 0)
                    {
                        foreach (var temp in listRelate)
                        {
                            // Cập nhật xóa các bản ghi liên quan đến bill.
                            db.Service_Rating_Relationship.Remove(temp);
                            db.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
        /**
         * Get List of Service rating
         * @return List<Service_Rating>
         **/
        public List<Service_Rating> AllRatingLabel
        {
            get
            {
                List<Service_Rating> List = db.Service_Rating.Where(w => w.IsDeleted == false || w.IsDeleted == null).ToList();
                return List;
            }
        }

        /**
         * Get ServiceRatingModel Instance
         * @return ServiceRatingModel
         **/
        public static ServiceRatingModel Instance
        {
            get
            {
                if (ServiceRatingModel._Instance == null)
                {
                    ServiceRatingModel._Instance = new ServiceRatingModel();
                }
                return ServiceRatingModel._Instance;
            }
        }

        public Service_Rating NotRatingItem
        {
            get
            {
                return db.Service_Rating.Where(w => w.Slug == "khong-danh-gia").FirstOrDefault();
            }
        }

        public List<Store_Service_Rating_Result> GetBadRatingBySalonId(int SalonId)
        {
            var List = new List<Store_Service_Rating_Result>();
            foreach (var Item in db.Store_Service_Rating(SalonId).ToList())
            {
                Item.BillServiceId = Convert.ToInt32(Item.BillServiceId);
                List.Add(new Store_Service_Rating_Result
                {
                    SalonId = Item.SalonId,
                    SalonName = Item.SalonName,
                    BillServiceId = Item.BillServiceId,
                    CustomerName = Item.CustomerName,
                    CustomerPhone = Item.CustomerPhone,
                    Staffs = Item.Staffs,
                    Rating = _GetRating(J => J.BillServiceId == Item.BillServiceId).Select(i => i.Title).Aggregate((i, j) => i + "," + j),
                    BillNote = db.BillServices.Where(w => w.Id == Item.BillServiceId).FirstOrDefault().Note
                });
            }

            return List;
        }

        public List<ServiceRatingView> GetRatingByBillId(int BillId)
        {
            List<ServiceRatingView> List;
            List = _GetRating(J => J.BillServiceId == BillId);
            return List;
        }

        public List<ServiceRatingView> _GetRating(Expression<Func<ServiceRatingView, bool>> Predicate)
        {
            List<ServiceRatingView> List = new List<ServiceRatingView>();
            List = db.Service_Rating_Relationship
                .Join(db.Service_Rating,
                    RR => RR.ServiceRatingId,
                    R => R.Id,
                    resultSelector: (RR, R) => new ServiceRatingView
                    {
                        SalonId = RR.SalonId,
                        BillServiceId = RR.BillServiceId,
                        Title = R.Title,
                        Description = R.Description,
                        Slug = R.Slug
                    }
                )
                .Where(Predicate)
                .ToList();

            return List;
        }
    }
}