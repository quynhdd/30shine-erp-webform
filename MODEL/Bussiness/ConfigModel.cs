﻿using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _30shine.MODEL.BO
{
    public class ConfigModel : IConfigModel
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public ConfigModel()
        {
            this.list = this.GetList();
        }

        private Solution_30shineEntities db = new Solution_30shineEntities();
        private List<Tbl_Config> list = new List<Tbl_Config>();

        /// <summary>
        /// Lấy danh sách cấu hình từ db
        /// </summary>
        /// <returns></returns>
        public List<Tbl_Config> GetList()
        {
            try
            {
                return this.db.Tbl_Config.Where(w=>w.IsDelete == 0).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Lấy giá trị cấu hình theo key
        /// </summary>
        /// <param name="key"></param>
        /// <returns>string|null</returns>
        public string GetValueByKey(string key)
        {
            try
            {
                var configRecord = this.list.FirstOrDefault(w=>w.Key == key);
                if (configRecord != null)
                {
                    return configRecord.Value;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}