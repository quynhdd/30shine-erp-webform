﻿using _30shine.MODEL.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;

namespace _30shine.MODEL.BO
{
    public class SalaryModel : ISalaryModel
    {
        private Solution_30shineEntities db;
        public SalaryModel()
        {
            this.db = new Solution_30shineEntities();
        }
        public FlowSalary Add(FlowSalary obj)
        {
            throw new NotImplementedException();
        }

        public bool Delete(FlowSalary obj)
        {
            throw new NotImplementedException();
        }

        public FlowSalary GetFlowSalaryById(int id)
        {
            try
            {
                return this.db.FlowSalaries.FirstOrDefault(w => w.Id == id);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public FlowSalary GetFlowSalaryByStaffDate(Staff staff, DateTime sDate)
        {
            try
            {
                if (staff != null && sDate != null)
                {
                    return this.db.FlowSalaries.FirstOrDefault(w => w.staffId == staff.Id && w.sDate == sDate);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<FlowSalary> GetFlowSalaryByStaffDate(Staff staff, DateTime timeFrom, DateTime timeTo)
        {
            throw new NotImplementedException();
        }

        public List<FlowSalary> GetListFlowSalary()
        {
            throw new NotImplementedException();
        }

        public void InitFlowSalaryByDay()
        {
            throw new NotImplementedException();
        }

        public FlowSalary Update(FlowSalary obj)
        {
            try
            {
                if (obj != null)
                {
                    var record = this.GetFlowSalaryById(obj.Id);
                    if (record != null)
                    {
                        record.staffId = obj.staffId;
                        record.SalonId = obj.SalonId;
                        record.sDate = obj.sDate;
                        record.fixSalary = obj.fixSalary;
                        record.serviceSalary = obj.serviceSalary;
                        record.productSalary = obj.productSalary;
                        record.partTimeSalary = obj.partTimeSalary;
                        record.allowanceSalary = obj.allowanceSalary;

                        record.ratingPoint = obj.ratingPoint;
                        record.bill_normal = obj.bill_normal;
                        record.bill_normal_bad = obj.bill_normal_bad;
                        record.bill_normal_good = obj.bill_normal_good;
                        record.bill_normal_great = obj.bill_normal_great;
                        record.bill_normal_norating = obj.bill_normal_norating;
                        record.bill_special = obj.bill_special;
                        record.bill_special_bad = obj.bill_special_bad;
                        record.bill_special_good = obj.bill_special_good;
                        record.bill_special_great = obj.bill_special_great;
                        record.bill_special_norating = obj.bill_special_norating;

                        record.Mistake_Point = obj.Mistake_Point;
                        record.waitTimeMistake = obj.waitTimeMistake;

                        record.ModifiedDate = DateTime.Now;

                        this.db.FlowSalaries.AddOrUpdate(record);
                        this.db.SaveChanges();

                        return record;
                    }
                    else
                    {
                        throw new Exception("Lỗi. Không tồn tại bản ghi.");
                    }
                }
                else
                {
                    throw new Exception("Lỗi. Không tồn tại bản ghi.");
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public void UpdateFlowSalaryByBill()
        {
            throw new NotImplementedException();
        }
    }
}