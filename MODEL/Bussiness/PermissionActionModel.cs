﻿using _30shine.MODEL.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.CustomClass;
using System.Linq.Expressions;
using System.Data.Entity.Migrations;

namespace _30shine.MODEL.Bussiness
{
    public class PermissionActionModel : IPermissionActionModel
    {

        /// <summary>
        /// Add Permission Action
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public PermissionAction Add(PermissionAction obj)
        {
            try
            {
                PermissionAction record = new PermissionAction();
                using (var db = new Solution_30shineEntities())
                {
                    if (obj != null)
                    {
                        record = obj;
                    }
                    record.CreatedTime = DateTime.Now;
                    record.IsActive = true;
                    record.IsDelete = false;
                    db.PermissionActions.Add(record);
                    db.SaveChanges();
                }
                return record;
            }
            catch
            {
                throw new Exception();
            }
        }

        /// <summary>
        /// Update Permission Action
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public PermissionAction Update(PermissionAction obj)
        {
            try
            {
                PermissionAction record = new PermissionAction();
                using (var db = new Solution_30shineEntities())
                {
                    record = GetById(obj.Id);
                    if (obj != null && record != null)
                    {
                        record = obj;
                    }
                    record.ModifiedTime = DateTime.Now;
                    record.IsDelete = false;
                    db.PermissionActions.AddOrUpdate(record);
                    db.SaveChanges();
                }
                return record;
            }
            catch
            {
                throw new Exception();
            }
        }

        /// <summary>
        /// Delete Permission
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public PermissionAction Delete(int id)
        {
            try
            {
                PermissionAction record = new PermissionAction();
                using (var db = new Solution_30shineEntities())
                {
                    record = GetById(id);
                    if (record != null)
                    {
                        record.ModifiedTime = DateTime.Now;
                        record.IsDelete = true;
                        db.PermissionActions.AddOrUpdate(record);
                        db.SaveChanges();
                    }
                }
                return record;
            }
            catch
            {
                throw new Exception();
            }
        }

        /// <summary>
        /// Get record by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public PermissionAction GetById(int id)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    PermissionAction record = (
                                                  from p in db.PermissionActions
                                                  where p.Id == id && p.IsDelete == false
                                                  select p
                                              )
                                              .OrderByDescending(w => w.Id)
                                              .FirstOrDefault();
                    return record;
                }
            }
            catch
            {
                throw new Exception();
            }
        }

        /// <summary>
        /// get list record by expression
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public List<PermissionAction> GetList(Expression<Func<PermissionAction, bool>> expression)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    List<PermissionAction> list = (
                                                  from p in db.PermissionActions
                                                  where p.IsDelete == false
                                                  select p
                                              ).Where(expression)
                                              .OrderByDescending(w => w.Id)
                                              .ToList();
                    return list;
                }
            }
            catch
            {
                throw new Exception();
            }
        }

        public PermissionAction Load(Expression<Func<PermissionAction, bool>> expression)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    PermissionAction record = (
                                                  from p in db.PermissionActions
                                                  where p.IsDelete == false
                                                  select p
                                              ).Where(expression)
                                              .OrderByDescending(w => w.Id)
                                              .FirstOrDefault();
                    return record;
                }
            }
            catch
            {
                throw new Exception();
            }
        }
    }
}