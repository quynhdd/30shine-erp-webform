﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using _30shine.MODEL.CustomClass;
using _30shine.MODEL;
using _30shine.MODEL.IO;
using System.Data.Entity.Migrations;

namespace _30shine.MODEL.Bussiness
{
    public class S4MClassModel : IS4MClassModel
    {
        /// <summary>
        /// Module Class s4m 
        /// Author: Như Phúc 
        /// Create: 24/10/2017
        /// </summary>
        private Solution_30shineEntities db;
        public S4MClassModel()
        {
            db = new Solution_30shineEntities();
        }
        /// <summary>
        /// Add class s4M
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public Stylist4Men_Class Add(Stylist4Men_Class obj)
        {
            try
            {
                var record = new Stylist4Men_Class();
                if (obj != null)
                {
                    record.CreatedTime = DateTime.Now;
                    record.StartTime = obj.StartTime;
                    record.Name = obj.Name;
                    record.Publish = obj.Publish;
                    record.IsDelete = obj.IsDelete;
                    record.HomeRoomTeacherId = obj.HomeRoomTeacherId;
                    record.TeacherOfTheClubId = obj.TeacherOfTheClubId;
                    record.TeacherPointCutId = obj.TeacherPointCutId;
                    this.db.Stylist4Men_Class.Add(record);
                    this.db.SaveChanges();
                    return record;
                }
                else
                {
                    throw new Exception("Có lỗi xảy ra!");
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// Get List Class
        /// </summary>
        /// <returns></returns>
        public List<store_list_Stylist4Men_Class_Result> AllList()
        {
            try
            {
                var lst = db.store_list_Stylist4Men_Class().ToList();
                return lst;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Get By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Stylist4Men_Class GetById(int? id)
        {
            try
            {
                var data = db.Stylist4Men_Class.Where(w => w.Id == id).FirstOrDefault();
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Update class s4m
        /// Author Như Phúc
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int? Update(Stylist4Men_Class obj)
        {
            try
            {
                var record = db.Stylist4Men_Class.Find(obj.Id);
                if (obj != null)
                {
                    record.StartTime = obj.StartTime;
                    record.Name = obj.Name;
                    record.HomeRoomTeacherId = obj.HomeRoomTeacherId;
                    record.TeacherOfTheClubId = obj.TeacherOfTheClubId;
                    record.TeacherPointCutId = obj.TeacherPointCutId;
                    record.ModifiedTime = DateTime.Now;
                    this.db.Stylist4Men_Class.AddOrUpdate(record);
                    return this.db.SaveChanges();

                }
                else
                {
                    throw new Exception("Có lỗi xảy ra!");
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
    }
}