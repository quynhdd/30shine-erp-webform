﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.IO;
using System.Data.Entity.Migrations;
using _30shine.MODEL.CustomClass;
using _30shine.Helpers;
using System.Net.Http;

namespace _30shine.MODEL.BO
{
    public class StaffMistakeModel : IStaffMistakeModel
    {
        private Solution_30shineEntities db;
        /// <summary>
        /// Constructor
        /// </summary>
        public StaffMistakeModel()
        {
            this.db = new Solution_30shineEntities();
        }
        /// <summary>
        /// Add 
        /// </summary>
        public Staff_Mistake Add(Staff_Mistake obj)
        {
            try
            {
                if (obj != null)
                {
                    var record = new Staff_Mistake();
                    record.StaffId = obj.StaffId;
                    record.Point = obj.Point;
                    record.Description = obj.Description;
                    record.IsDelete = obj.IsDelete;
                    record.CreateDate = obj.CreateDate;
                    record.Images = obj.Images;
                    record.FormMissTakeId = obj.FormMissTakeId;
                    record.Note = obj.Note;
                    this.db.Staff_Mistake.Add(record);
                    this.db.SaveChanges();
                    return obj;
                }
                else
                {
                    throw new Exception("Có lỗi xảy ra!");
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// Update
        /// </summary>
        public Staff_Mistake Update(Staff_Mistake obj)
        {
            try
            {
                var record = db.Staff_Mistake.Find(obj.Id);
                if (record != null)
                {
                    record.StaffId = obj.StaffId;
                    record.Point = obj.Point;
                    record.Description = obj.Description;
                    record.IsDelete = obj.IsDelete;
                    record.Images = obj.Images;
                    record.FormMissTakeId = obj.FormMissTakeId;
                    record.Note = obj.Note;
                    this.db.Staff_Mistake.AddOrUpdate(record);
                    this.db.SaveChanges();
                    return obj;
                }
                else
                {
                    throw new Exception("Có lỗi xảy ra!");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// Get List Staff_Mistake
        /// </summary>
        /// <returns></returns>
        public List<StaffMistakeClass> GetList(DateTime timeForm, DateTime timeTo, int salonId, int TypeId, int FormMissTakeId)
        {
            try
            {
                //return this.db.Store_StaffMistake_GetList(timeForm, timeTo, salonId).ToList();

                List<StaffMistakeClass> List = (from a in db.Staff_Mistake
                                                join b in db.Staffs on a.StaffId equals b.Id
                                                join c in db.tbl_Form_Misstake on a.FormMissTakeId equals c.Id into cs
                                                from c in cs.DefaultIfEmpty()

                                                where a.IsDelete == false
                                                && (b.SalonId == salonId || (salonId == 0 && b.SalonId > 0))
                                                && (a.CreateDate >= timeForm && a.CreateDate <= timeTo)
                                                && (b.Type == TypeId || (TypeId == 0 && b.Type > 0))
                                                && (a.FormMissTakeId == FormMissTakeId || (FormMissTakeId == 0 && (a.FormMissTakeId > 0 || a.FormMissTakeId == null)))
                                                select new StaffMistakeClass
                                                {
                                                    Id = a.Id,
                                                    StaffId = a.StaffId,
                                                    FormMissTakeId = a.FormMissTakeId,
                                                    CreateDate = a.CreateDate,
                                                    Description = a.Description,
                                                    Point = a.Point,
                                                    Images = a.Images,
                                                    Fullname = b.Fullname,
                                                    Note = a.Note,
                                                    Formality = c.Formality

                                                }).OrderByDescending(o => o.CreateDate).ToList();
                return List;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// Get List Staff_Mistake theo Id
        /// </summary>
        /// <returns></returns>
        public StaffMistakeClass GetListId(int id)
        {
            try
            {
                // return this.db.Store_StaffMistake_GetByID(id).FirstOrDefault();
                StaffMistakeClass data = (from a in db.Staff_Mistake
                                          join b in db.Staffs on a.StaffId equals b.Id
                                          where a.IsDelete == false && a.Id == id
                                          select new StaffMistakeClass
                                          {
                                              Id = a.Id,
                                              StaffId = a.StaffId,
                                              Point = a.Point,
                                              Description = a.Description,
                                              CreateDate = a.CreateDate,
                                              Images = a.Images,
                                              FormMissTakeId = a.FormMissTakeId,
                                              Note = a.Note,
                                              SalonId = b.SalonId,
                                          }).FirstOrDefault();
                return data;
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public List<StaffMistake5CClass> GetList5C(int _salonId, DateTime? _workDate)
        {
            List<StaffMistake5CClass> List5C = new List<StaffMistake5CClass>();
            try
            {
                List5C = (from d in db.SalaryIncomeChanges
                          join e in db.tbl_Form_Misstake on d.ChangedTypeId equals e.Id into f
                          from g in f.DefaultIfEmpty()
                          where g.isDelete == false && d.SalonId == _salonId && d.WorkDate == _workDate && d.IsDeleted == false && d.ChangedType == "luong_5c"
                          select new StaffMistake5CClass
                          {
                              InComeChangeId = d.Id,
                              ScoreFactor = d.ScoreFactor,
                              Description = d.Description,
                              WorkDate = d.WorkDate,
                              SalonId = d.SalonId,
                              StaffId = d.StaffId,
                              ChangedType = d.ChangedType,
                              ChangedTypeId = d.ChangedTypeId,
                              FormMistake = g.Formality
                          }).ToList();
            }
            catch (Exception ex)
            {
                List5C = null;
            }
            return List5C;
        }

        public Msg AddOrUpdate5C(SalaryIncomeChange _obj5C)
        {
            Msg message = new Msg();
            //Khởi tạo giá trị mặc định;
            message.data = null;
            message.msg = "";
            message.success = false;
            try
            {
                if (_obj5C != null)
                {
                    //kiem tra xem nhan vien nay da co 5C trong ngay hay chua
                    var staff5C = db.SalaryIncomeChanges.Where(r =>
                                                                r.WorkDate == _obj5C.WorkDate
                                                                && r.StaffId == _obj5C.StaffId
                                                                && r.ChangedType == "luong_5c"
                                                                //&& r.Salon_Id == _obj5C.Salon_Id
                                                                ).FirstOrDefault();
                    if (staff5C != null)
                    {
                        //Xóa point cũ
                        message = AddOrUpdateSalaryIncome(0 - staff5C.Point.Value, staff5C.StaffId.Value, staff5C.WorkDate.Value, staff5C.SalonId.Value);

                        var objstaff5C = staff5C;
                        objstaff5C.ModifiedTime = DateTime.Now;
                        objstaff5C.ScoreFactor = _obj5C.ScoreFactor;
                        objstaff5C.Description = _obj5C.Description;
                        objstaff5C.ChangedType = _obj5C.ChangedType;
                        objstaff5C.ChangedTypeId = _obj5C.ChangedTypeId;
                        objstaff5C.Point = _obj5C.Point;
                        objstaff5C.SalonId = _obj5C.SalonId;
                        db.SalaryIncomeChanges.AddOrUpdate(objstaff5C);
                        var exc = db.SaveChanges();
                        //Thêm point mới
                        message = AddOrUpdateSalaryIncome(_obj5C.Point.Value, _obj5C.StaffId.Value, _obj5C.WorkDate.Value, _obj5C.SalonId.Value);
                        // Cập nhật lên dynamoDb
                        var response = new HttpClient().PostAsJsonAsync(Libraries.AppConstants.URL_API_MANAGER_APP + "/api/salary-income-change/push-dynamodb",
                            new List<object>()
                            {
                                new {
                                objstaff5C.ChangedType,
                                objstaff5C.ChangedTypeId,
                                objstaff5C.Id,
                                objstaff5C.Point,
                                objstaff5C.SalonId,
                                objstaff5C.WorkDate,
                                objstaff5C.StaffId
                                }
                            }).Result;


                    }
                    else
                    {
                        _obj5C.CreatedTime = DateTime.Now;
                        db.SalaryIncomeChanges.Add(_obj5C);
                        var exc = db.SaveChanges();
                        if (exc >= 1)
                        {
                            message = AddOrUpdateSalaryIncome(_obj5C.Point.Value, _obj5C.StaffId.Value, _obj5C.WorkDate.Value, _obj5C.SalonId.Value);
                            // Cập nhật lên dynamoDb
                            var response = new HttpClient().PostAsJsonAsync(Libraries.AppConstants.URL_API_MANAGER_APP + "/api/salary-income-change/push-dynamodb",
                                new List<object>()
                                {
                                new {
                                _obj5C.ChangedType,
                                _obj5C.ChangedTypeId,
                                _obj5C.Id,
                                _obj5C.Point,
                                _obj5C.SalonId,
                                _obj5C.WorkDate,
                                _obj5C.StaffId,
                                }
                                }).Result;
                        }
                        else
                        {
                            message.data = 0;
                            message.msg = "Không Thêm được.";
                            message.success = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                message.msg = "Exception: " + ex.Message;
            }
            return message;
        }
        public Msg AddOrUpdateSalaryIncome(int Point, int StaffId, DateTime WorkDate, int Salon_Id)
        {
            Msg message = new Msg();
            message.success = false;
            message.msg = "";
            message.data = null;
            try
            {
                var data = (from a in db.SalaryIncomes
                            where a.StaffId == StaffId && a.WorkDate == WorkDate && a.IsDeleted == false
                            select a).FirstOrDefault();
                if (data != null)
                {
                    data.GrandTotalIncome = data.GrandTotalIncome == null ? Point : data.GrandTotalIncome + Point;
                    data.ModifiedTime = DateTime.Now;
                    db.SalaryIncomes.AddOrUpdate(data);
                    var exc = db.SaveChanges();
                    if (exc > 0)
                    {
                        message.success = true;
                        message.data = 1;
                        message.msg = "Cập nhật thành công";
                    }
                }
                else
                {
                    var dataInCome = new SalaryIncome();
                    dataInCome.Year = DateTime.Now.Year;
                    dataInCome.Month = DateTime.Now.Month;
                    dataInCome.WorkDate = WorkDate;
                    dataInCome.CreatedTime = DateTime.Now;
                    dataInCome.StaffId = StaffId;
                    dataInCome.IsDeleted = false;
                    dataInCome.SalonId = Salon_Id;
                    dataInCome.GrandTotalIncome = Point;
                    dataInCome.FixedSalary = 0;
                    dataInCome.ServiceSalary = 0;
                    dataInCome.ProductSalary = 0;
                    dataInCome.OvertimeSalary = 0;
                    dataInCome.AllowanceSalary = 0;
                    db.SalaryIncomes.Add(dataInCome);
                    var exc = db.SaveChanges();
                    if (exc > 0)
                    {
                        message.success = true;
                        message.data = 1;
                        message.msg = "Thêm thành công";
                    }
                }
            }
            catch (Exception ex) { }
            return message;
        }
    }
}