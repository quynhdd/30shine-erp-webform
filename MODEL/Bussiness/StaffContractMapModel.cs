﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using _30shine.Helpers;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
namespace _30shine.MODEL.Bussiness
{
    public class StaffContractMapModel : IStaffContractMap
    {
        private Solution_30shineEntities db;
        public StaffContractMapModel()
        {
            this.db = new Solution_30shineEntities();
        }
        public Msg InsertMapContract(DateTime? contractDate, int contractType, int staffId)
        {
            var result = new Msg();
            try
            {
                var ListContractMap = db.StaffContractMaps.Where(r => r.IsDelete == false && r.StaffId == staffId).ToList();
                var obj3thang = ListContractMap.Where(r => r.ContractId == 1).FirstOrDefault();
                var obj6thang = ListContractMap.Where(r => r.ContractId == 2).FirstOrDefault();
                var objLaodong = ListContractMap.Where(r => r.ContractId == 3).FirstOrDefault();
                var objThuviec = ListContractMap.Where(r => r.ContractId == 4).FirstOrDefault();
                var objThuviecQuanly = ListContractMap.Where(r => r.ContractId == 5).FirstOrDefault();
                var objLaodongQuanly = ListContractMap.Where(r => r.ContractId == 6).FirstOrDefault();
                //Khởi tạo data mặc định;
                var objTempContract = new StaffContractMap();
                objTempContract.StaffId = staffId;
                objTempContract.ContractId = 1;
                objTempContract.ContractDate = contractDate;
                objTempContract.IsDelete = false;
                objTempContract.Active = true;
                objTempContract.CreatedTime = DateTime.Now;
                if (contractType == 1)
                {
                    if (contractDate != null)
                    {
                        if (obj3thang != null)
                        {
                            obj3thang.ContractDate = contractDate;
                            db.StaffContractMaps.AddOrUpdate(obj3thang);
                        }
                        else
                        {
                            objTempContract.ContractId = 1;
                            db.StaffContractMaps.Add(objTempContract);
                        }
                    }
                    else
                    {
                        if (obj3thang != null)
                        {
                            db.StaffContractMaps.Remove(obj3thang);
                        }
                        if (objThuviec != null)
                        {
                            objThuviec.Active = true;
                            db.StaffContractMaps.AddOrUpdate(objThuviec);
                        }
                    }
                }
                else if (contractType == 2)
                {

                    if (contractDate != null)
                    {
                        if (obj3thang != null)
                        {
                            obj3thang.Active = false;
                            db.StaffContractMaps.AddOrUpdate(obj3thang);
                        }
                        if (obj6thang != null)
                        {
                            obj6thang.ContractDate = contractDate;
                            db.StaffContractMaps.AddOrUpdate(obj6thang);
                        }
                        else
                        {
                            objTempContract.ContractId = 2;
                            db.StaffContractMaps.Add(objTempContract);
                        }
                    }
                    else
                    {
                        if (obj3thang != null)
                        {
                            obj3thang.Active = true;
                            db.StaffContractMaps.AddOrUpdate(obj3thang);
                        }
                        if (obj6thang != null)
                        {
                            db.StaffContractMaps.Remove(obj6thang);
                        }
                    }
                }
                else if (contractType == 3)
                {
                    if (contractDate != null)
                    {
                        if (obj3thang != null)
                        {
                            obj3thang.Active = false;
                            db.StaffContractMaps.AddOrUpdate(obj3thang);
                        }
                        if (obj6thang != null)
                        {
                            obj6thang.Active = false;
                            db.StaffContractMaps.AddOrUpdate(obj6thang);
                        }
                        if (objThuviec != null)
                        {
                            objThuviec.Active = false;
                            db.StaffContractMaps.AddOrUpdate(objThuviec);
                        }
                        if (objLaodong != null)
                        {
                            objLaodong.ContractDate = contractDate;
                            db.StaffContractMaps.AddOrUpdate(objLaodong);
                        }
                        else
                        {
                            objTempContract.ContractId = 3;
                            db.StaffContractMaps.Add(objTempContract);
                        }
                    }
                    else
                    {
                        if (obj6thang != null)
                        {
                            obj6thang.Active = true;
                            db.StaffContractMaps.AddOrUpdate(obj6thang);
                        }
                        else if (obj3thang != null)
                        {
                            obj3thang.Active = true;
                            db.StaffContractMaps.AddOrUpdate(obj3thang);
                        }
                        else if (objThuviec != null)
                        {
                            objThuviec.Active = true;
                            db.StaffContractMaps.AddOrUpdate(objThuviec);
                        }
                        if (objLaodong != null)
                        {
                            db.StaffContractMaps.Remove(objLaodong);
                        }
                    }
                }
                else if (contractType == 4)
                {
                    if (contractDate != null)
                    {
                        if (objThuviec != null)
                        {
                            objThuviec.ContractDate = contractDate;
                            db.StaffContractMaps.AddOrUpdate(objThuviec);
                        }
                        else
                        {
                            objTempContract.ContractId = 4;
                            db.StaffContractMaps.Add(objTempContract);
                        }
                    }
                    else
                    {
                        if (objThuviec != null)
                        {
                            db.StaffContractMaps.Remove(objThuviec);
                        }
                    }
                }
                else if (contractType == 5)
                {
                    if (contractDate != null)
                    {
                        if (objThuviec != null)
                        {
                            objThuviec.Active = false;
                            db.StaffContractMaps.AddOrUpdate(objThuviec);
                        }
                        if (obj3thang != null)
                        {
                            obj3thang.Active = false;
                            db.StaffContractMaps.AddOrUpdate(obj3thang);
                        }
                        if (obj6thang != null)
                        {
                            obj6thang.Active = false;
                            db.StaffContractMaps.AddOrUpdate(obj6thang);
                        }
                        if (objLaodong != null)
                        {
                            objLaodong.Active = false;
                            db.StaffContractMaps.AddOrUpdate(objLaodong);
                        }
                        if (objThuviecQuanly != null)
                        {
                            objThuviecQuanly.ContractDate = contractDate;
                            db.StaffContractMaps.AddOrUpdate(objThuviecQuanly);
                        }
                        else
                        {
                            objTempContract.ContractId = 5;
                            db.StaffContractMaps.Add(objTempContract);
                        }
                    }
                    else
                    {
                        if (objLaodong != null)
                        {
                            objLaodong.Active = true;
                            db.StaffContractMaps.AddOrUpdate(objLaodong);
                        }
                        else if (obj6thang != null)
                        {
                            obj6thang.Active = true;
                            db.StaffContractMaps.AddOrUpdate(obj6thang);
                        }
                        else if (obj3thang != null)
                        {
                            obj3thang.Active = true;
                            db.StaffContractMaps.AddOrUpdate(obj3thang);
                        }
                        else if (objThuviec != null)
                        {
                            objThuviec.Active = true;
                            db.StaffContractMaps.AddOrUpdate(objThuviec);
                        }
                        if (objThuviecQuanly != null)
                        {
                            db.StaffContractMaps.Remove(objThuviecQuanly);
                        }
                    }
                }
                else if (contractType == 6)
                {
                    if (contractDate != null)
                    {
                        if (objThuviec != null)
                        {
                            objThuviec.Active = false;
                            db.StaffContractMaps.AddOrUpdate(objThuviec);
                        }
                        if (obj3thang != null)
                        {
                            obj3thang.Active = false;
                            db.StaffContractMaps.AddOrUpdate(obj3thang);
                        }
                        if (obj6thang != null)
                        {
                            obj6thang.Active = false;
                            db.StaffContractMaps.AddOrUpdate(obj6thang);
                        }
                        if (objLaodong != null)
                        {
                            objLaodong.Active = false;
                            db.StaffContractMaps.AddOrUpdate(objLaodong);
                        }
                        if (objThuviecQuanly != null)
                        {
                            objThuviecQuanly.Active = false;
                            db.StaffContractMaps.AddOrUpdate(objThuviecQuanly);
                        }
                        if (objLaodongQuanly != null)
                        {
                            objLaodongQuanly.ContractDate = contractDate;
                            db.StaffContractMaps.AddOrUpdate(objLaodongQuanly);
                        }
                        else
                        {
                            objTempContract.ContractId = 6;
                            db.StaffContractMaps.Add(objTempContract);
                        }
                    }
                    else
                    {
                        if (objThuviecQuanly != null)
                        {
                            objThuviecQuanly.Active = true;
                            db.StaffContractMaps.AddOrUpdate(objThuviecQuanly);
                        }
                        else if (objLaodong != null)
                        {
                            objLaodong.Active = true;
                            db.StaffContractMaps.AddOrUpdate(objLaodong);
                        }
                        else if (obj6thang != null)
                        {
                            obj6thang.Active = true;
                            db.StaffContractMaps.AddOrUpdate(obj6thang);
                        }
                        else if (obj3thang != null)
                        {
                            obj3thang.Active = true;
                            db.StaffContractMaps.AddOrUpdate(obj3thang);
                        }
                        else if (objThuviec != null)
                        {
                            objThuviec.Active = true;
                            db.StaffContractMaps.AddOrUpdate(objThuviec);
                        }
                        if (objLaodongQuanly != null)
                        {
                            db.StaffContractMaps.Remove(objLaodongQuanly);
                        }
                    }
                }
                var exc = db.SaveChanges();
                result.success = true;
                result.msg = "Thành công";
            }
            catch (Exception ex)
            {
                result.success = false;
                result.msg = "Đã có lỗi xảy ra!";
            }
            return result;
        }
        public Msg Insert(StaffContractMap objContract)
        {
            var result = new Msg();
            try
            {
                if (objContract != null)
                {
                    db.StaffContractMaps.Add(objContract);
                    var exc = db.SaveChanges();
                    if (exc > 0)
                    {
                        result.success = true;
                        result.msg = "Thành công!";
                    }
                    else
                    {
                        result.success = false;
                        result.msg = "Không lưu được data";
                    }
                }
                else
                {
                    result.success = false;
                    result.msg = "Data không hợp lệ";
                }
            }
            catch (Exception ex)
            {
                result.success = false;
                result.msg = "Đã xảy ra lỗi !";
            }
            return result;
        }
        public Msg Update(StaffContractMap objContract)
        {
            var result = new Msg();
            try
            {
                if (objContract != null && objContract.Id > 0)
                {
                    db.StaffContractMaps.AddOrUpdate(objContract);
                    var exc = db.SaveChanges();
                    if (exc >= 0)
                    {
                        result.success = true;
                        result.msg = "Thành công!";
                    }
                    else
                    {
                        result.success = false;
                        result.msg = "Không lưu được data";
                    }
                }
                else
                {
                    result.success = false;
                    result.msg = "Data không hợp lệ";
                }
            }
            catch (Exception ex)
            {
                result.success = false;
                result.msg = "Đã xảy ra lỗi !";
            }
            return result;
        }
        public Msg Delete(int Id)
        {
            var result = new Msg();
            try
            {
                var record = db.StaffContractMaps.Where(r => r.Id == Id && r.IsDelete == false).FirstOrDefault();
                if (record != null)
                {
                    record.IsDelete = true;
                    db.StaffContractMaps.AddOrUpdate(record);
                    var exc = db.SaveChanges();
                    if (exc > 0)
                    {
                        result.success = true;
                        result.msg = "Thành công";
                    }
                    else
                    {
                        result.success = false;
                        result.msg = "Không xóa được dữ liệu";
                    }
                }
                else
                {
                    result.success = false;
                    result.msg = "Không tìm được data";
                }
            }
            catch (Exception ex)
            {
                result.success = false;
                result.msg = "Đã có lỗi xảy ra";
            }
            return result;

        }
        public Msg GetById(int Id)
        {
            var result = new Msg();
            try
            {
                var record = db.StaffContractMaps.Where(r => r.Id == Id && r.IsDelete == false).FirstOrDefault();
                if (record != null)
                {
                    result.success = true;
                    result.msg = "Thành công";
                    result.data = record;
                }
                else
                {
                    result.success = false;
                    result.msg = "Không tìm được data";
                }
            }
            catch (Exception ex)
            {
                result.success = false;
                result.msg = "Đã có lỗi xảy ra";
            }
            return result;
        }
        public Msg GetList(Expression<Func<StaffContractMap, bool>> expression)
        {
            var result = new Msg();
            try
            {
                var list = db.StaffContractMaps.Where(expression).ToList();
                if (list.Count > 0)
                {
                    result.success = true;
                    result.msg = "Thành công";
                    result.data = list;
                }
                else
                {
                    result.success = false;
                    result.msg = "Không tìm được data";
                }
            }
            catch (Exception ex)
            {
                result.success = false;
                result.msg = "Đã có lỗi xảy ra";
            }
            return result;
        }
        public Msg Get(Expression<Func<StaffContractMap, bool>> expression)
        {
            var result = new Msg();
            try
            {
                var record = db.StaffContractMaps.Where(expression).FirstOrDefault();

                result.success = true;
                result.msg = "Thành công";
                result.data = record;
            }
            catch (Exception ex)
            {
                result.success = false;
                result.msg = "Đã có lỗi xảy ra";
            }
            return result;
        }
        public Msg UnActive(StaffContractMap obj)
        {
            var result = new Msg();
            try
            {
                obj.Active = false;
                db.StaffContractMaps.AddOrUpdate(obj);
                var exc = db.SaveChanges();
                if (exc > 0)
                {
                    result.success = true;
                    result.msg = "Thành công";
                }
                else
                {
                    result.success = false;
                    result.msg = "Không set được unActive";
                }
            }
            catch (Exception ex)
            {
                result.success = false;
                result.msg = "Đã có lỗi xảy ra";
            }
            return result;
        }
    }
}