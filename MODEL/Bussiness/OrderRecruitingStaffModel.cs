﻿using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web;

namespace _30shine.MODEL.Bussiness
{
    public class OrderRecruitingStaffModel : IOrderRecruitingStaffModel
    {
        private Solution_30shineEntities db;
        public OrderRecruitingStaffModel()
        {
            this.db = new Solution_30shineEntities();
        }
        public List<OrderRecruitingStaff> GetList(Expression<Func<OrderRecruitingStaff, bool>> expression)
        {
            return db.OrderRecruitingStaffs.Where(expression).ToList();
        }
        /// <summary>
        /// insert
        /// </summary>
        /// <param name="record"></param>
        public void Add(List<_30shine.GUI.BackEnd.TuyenDungV2.OrderRecruitmentStaffSub> list, int creatorId)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    if (list.Count > 0)
                    {
                        List<string> key = new List<string>() { "recruitment_day_hurry", "recruitment_day_offset", "recruitment_day_new", "recruitment_day_for_sm", "recruitment_day_for_dsm" };
                        var config = db.Tbl_Config.Where(f => key.Contains(f.Key) && f.IsDelete == 0 && f.Status == 1).ToList();
                        var data = list.Select(c => new OrderRecruitingStaff
                        {
                            CreatorId = creatorId,
                            RegionId = c.RegionId,
                            SalonId = c.SalonId,
                            DepartmentId = c.DepartmentId,
                            Quantity = c.Quantity,
                            Type = c.Type,
                            //Needed = Convert.ToDateTime(c.Needed, new CultureInfo("vi-VN")),
                            Completed = (
                                            c.DepartmentId == 9 ? DateTime.Now.AddDays(Convert.ToInt32(config.FirstOrDefault(f => f.Key == "recruitment_day_for_sm").Value)).Date :
                                            c.DepartmentId == 16 ? DateTime.Now.AddDays(Convert.ToInt32(config.FirstOrDefault(f => f.Key == "recruitment_day_for_dsm").Value)).Date :
                                            c.Type == 1 ? DateTime.Now.AddDays(Convert.ToInt32(config.FirstOrDefault(f => f.Key == "recruitment_day_hurry").Value)).Date :
                                            c.Type == 2 ? DateTime.Now.AddDays(Convert.ToInt32(config.FirstOrDefault(f => f.Key == "recruitment_day_offset").Value)).Date :
                                            DateTime.Now.AddDays(Convert.ToInt32(config.FirstOrDefault(f => f.Key == "recruitment_day_new").Value)).Date
                                        ),
                            Status = 1,
                            ReasonRevoke = 0,
                            HavingTry = false,
                            CreatedDate = DateTime.Now,
                            IsDelete = false
                        }).ToList();
                        foreach (var v in data)
                        {
                            var record = new OrderRecruitingStaff();
                            record = v;
                            //if (v.Needed != null)
                            //{
                            //    record.Needed = new DateTime(v.Needed.Value.Year, v.Needed.Value.Month, v.Needed.Value.Day, 23, 59, 59);
                                record.Completed = new DateTime(v.Completed.Value.Year, v.Completed.Value.Month, v.Completed.Value.Day, 23, 59, 59);
                            //}
                            db.OrderRecruitingStaffs.Add(record);
                        }
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}