﻿using _30shine.MODEL.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using _30shine.MODEL.CustomClass;
using _30shine.MODEL.ENTITY.EDMX;
using static _30shine.MODEL.CustomClass.AccountModelClass;
using System.Web.Script.Serialization;

namespace _30shine.MODEL.Bussiness
{
    public class AccountModel : IAccountModel
    {
        public AccountSession AccountInfor { get; set; }

        private Solution_30shineEntities db;

        public AccountModel()
        {
            this.db = new Solution_30shineEntities();
            this.AccountInfor = this.GetAccountInfor();
        }

        public AccountSession GetAccountInfor()
        {
            try
            {
                AccountSession account = new AccountSession();
                if (HttpContext.Current.Session != null)
                {
                    
                    var info = HttpContext.Current.Session["AccountInfo"];
                    if (info != null)
                    {
                        account = (AccountSession)info;
                    }
                }
                return account;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SetAccountInfor(AccountSession obj)
        {
            try
            {
                if (obj != null)
                {
                    var accountInfo = new AccountSession();
                    //accountInfo.UserId = obj.UserId;
                    //accountInfo.UserCode = obj.UserCode;
                    //accountInfo.UserName = obj.UserName;
                    //accountInfo.UserPhone = obj.UserPhone;
                    //accountInfo.UserEmail = obj.UserEmail;
                    //accountInfo.UserBirthDay = obj.UserBirthDay;
                    //accountInfo.UserSalonId = obj.UserSalonId;
                    //accountInfo.UserSalonName = obj.UserSalonName;
                    //accountInfo.UserDepartmentId = obj.UserDepartmentId;
                    //accountInfo.UserDepartmentName = obj.UserDepartmentName;
                    //accountInfo.UserSkillLevelId = obj.UserSkillLevelId;
                    //accountInfo.UserSkillLevelName = obj.UserSkillLevelName;
                    //accountInfo.UserPermission = obj.UserPermission;
                    //accountInfo.UserPermissionId = obj.UserPermissionId;
                    //accountInfo.UserMenu = obj.UserMenu;
                    HttpContext.Current.Session.Add("AccountInfo", obj);
                    this.AccountInfor = this.GetAccountInfor();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool IsAccountLogin()
        {
            throw new NotImplementedException();
        }

        public void Login(string account, string password)
        {
            throw new NotImplementedException();
        }

        public void Logout()
        {
            throw new NotImplementedException();
        }

        public static AccountModel Instance
        {
            get
            {
                return new AccountModel();
            }
        }

        public bool IsCheckout()
        {
            return AccountInfor.UserDepartmentId == 6;
        }

        public bool IsCheckin()
        {
            return AccountInfor.UserDepartmentId == 5;
        }
        public bool IsReception()
        {
            return AccountInfor.UserDepartmentId == 3;
        }
        public string ToJson()
        {
            return Library.Format.ObjectToJson(AccountInfor);
        }
    }
}