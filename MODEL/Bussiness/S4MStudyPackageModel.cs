﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using _30shine.MODEL.Interface;
using _30shine.MODEL.ENTITY.EDMX;
namespace _30shine.MODEL.Bussiness
{
    public class S4MStudyPackageModel : IS4MStudyPackageModel
    {
        private Solution_30shineEntities db;
        public S4MStudyPackageModel()
        {
            db = new Solution_30shineEntities();
        }

        /// <summary>
        /// Get gói học
        /// </summary>
        /// <returns></returns>
        public List<Stylist4Men_StudyPackage> GetListAll()
        {
            try
            {
                var list = db.Stylist4Men_StudyPackage.Where(w => w.IsDelete == false && w.Publish == true).ToList();
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}