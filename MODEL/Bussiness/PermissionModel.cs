﻿using _30shine.MODEL.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.CustomClass;
using _30shine.Helpers.Http;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace _30shine.MODEL.Bussiness
{
    public class PermissionModel : IPermissionModel
    {
        private Solution_30shineEntities db;
        public PermissionModel()
        {
            this.db = new Solution_30shineEntities();
        }
        public List<Tbl_Permission> GetListPermission()
        {
            try
            {
                return this.db.Tbl_Permission.ToList();
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// //Function cắt chuỗi rewite sang pageId
        /// </summary>
        /// <returns></returns>
        public Regex GetRegexPageId()
        {
            //Regex regex = new Regex(@"/([\d]*)\.html");
            Regex regex = new Regex(@"/([\d]*)\.html\?([^\?]*)|/([\d]*)\.html");
            return regex;
        }

        /// <summary>
        /// //Function cắt chuỗi rewite sang pageId special
        /// </summary>
        /// <returns></returns>
        public Regex GetRegexPageIdSpecial()
        {
            Regex regex = new Regex(@"\.html\?([^\?]*)");
            return regex;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="actionName"></param>
        /// <param name="pageID"></param>
        /// <param name="permission"></param>
        /// <returns></returns>
        public bool GetActionByActionNameAndPageId(string actionName, string pageID, string permission)
        {
            try
            {
                if (permission == "root")
                {
                    return true;
                }

                var aID = db.Tbl_Permission_Action.FirstOrDefault(a => a.aName == actionName);
                if (aID == null)
                {
                    return false;
                }
                var _aID = aID.aID.ToString();
                var result = db.Store_Permission_GetAction(pageID, permission, _aID).FirstOrDefault();
                if (result != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Kiểm tra quyền khi mở trang.
        /// </summary>
        /// <param name="actionName"></param>
        /// <param name="strPageId"></param>
        /// <param name="staffId"></param>
        /// <returns></returns>
        public bool CheckPermisionByAction(string actionName, string strPageId, int staffId)
        {
            bool check = false;
            try
            {
                if (staffId == 0)
                {
                    return true;
                }
                var checkObj = (from map in db.PermissionMenuActions
                                join action in db.PermissionActions on map.ActionId equals action.Id
                                join menu in db.PermissionMenus on map.PageId equals menu.Id
                                join staffPermision in db.PermissionStaffs on map.PermissionId equals staffPermision.PermissionId
                                where map.IsActive == true &&
                                      map.IsDelete == false &&
                                      action.IsDelete == false &&
                                      action.IsActive == true &&
                                      menu.IsDelete == false &&
                                      menu.IsActive == true &&
                                      staffPermision.IsDelete == false &&
                                      staffPermision.IsActive == true &&
                                      staffPermision.StaffId == staffId &&
                                      action.Name == actionName &&
                                      menu.PageId == strPageId
                                select new { check = true }
                                ).FirstOrDefault();
                if (checkObj != null)
                {
                    check = true;
                }
            }
            catch (Exception ex)
            {
                //Push thông báo cho mọi người (by slack) khi có lỗi xảy ra
                new Helpers.PushNotiErrorToSlack().Push("GA30CQZPF",
                                                        ex.Message + ", " + ex.InnerException,
                                                        this.ToString() + ".CheckPermisionByAction",
                                                        "phucdn",
                                                        "ERP");
            }
            return check;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<Tbl_Permission_Action> ListAction()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Tbl_Permission GetPermissionById(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pId"></param>
        /// <returns></returns>
        public string GetMenu(int pId)
        {
            using (var db = new Solution_30shineEntities())
            {
                var menu = "";
                //var listPr = new List<Tbl_Permission_Menu>();
                if (pId == 0)
                {
                    var listPr = (from m in db.Tbl_Permission_Menu
                                  where m.mParentID == 0
                                  select m).ToList();
                    foreach (var item in listPr)
                    {
                        menu += "<li>";
                        menu += "<a href='" + item.Url_Rewrite + "'";
                        if (item.Target != null)
                        {
                            menu += "target = '" + item.Target + "'";
                        }
                        menu += ">" + item.mName + "</a>";
                        var listChild = (from m in db.Tbl_Permission_Menu
                                         where m.mParentID == item.mID
                                         select m).ToList();
                        if (listChild != null)
                        {
                            menu += "<ul class='ul-sub-menu'>";
                            foreach (var itemChild in listChild)
                            {
                                menu += "<li>";
                                menu += "<a href='" + itemChild.Url_Rewrite + "'";
                                if (itemChild.Target != null)
                                {
                                    menu += "target = '" + itemChild.Target + "'";
                                }
                                menu += ">" + itemChild.mName + "</a>";
                                var listChild1 = (from m in db.Tbl_Permission_Menu
                                                  where m.mParentID == itemChild.mID
                                                  select m).ToList();
                                if (listChild1 != null)
                                {
                                    menu += "<ul class='ul-sub2-menu'>";
                                    foreach (var itemChild1 in listChild1)
                                    {
                                        menu += "<li>";
                                        menu += "<a href='" + itemChild1.Url_Rewrite + "'";
                                        if (itemChild1.Target != null)
                                        {
                                            menu += "target = '" + itemChild1.Target + "'";
                                        }
                                        menu += ">" + itemChild1.mName + "</a>";
                                        var listChild2 = (from m in db.Tbl_Permission_Menu
                                                          where m.mParentID == itemChild1.mID
                                                          select m).ToList();
                                        if (listChild2 != null)
                                        {
                                            menu += "<ul class='ul-sub2-menu'>";
                                            foreach (var itemChild2 in listChild2)
                                            {
                                                menu += "<li>";
                                                menu += "<a href='" + itemChild2.Url_Rewrite + "'";
                                                if (itemChild2.Target != null)
                                                {
                                                    menu += "target = '" + itemChild2.Target + "'";
                                                }
                                                menu += ">" + itemChild2.mName + "</a>";
                                                menu += "</li>";
                                            }
                                            menu += "</ul>";
                                        }
                                        menu += "</li>";
                                    }
                                    menu += "</ul>";
                                }
                                menu += "</li>";
                            }
                            menu += "</ul>";
                        }
                        menu += "</li>";
                    }
                }
                else
                {
                    var listPr = (from m in db.Tbl_Permission_Menu
                                  join map in db.Tbl_Permission_Map on m.mID equals map.mID
                                  where m.mParentID == 0 && map.pID == pId
                                  select m).ToList();
                    foreach (var item in listPr)
                    {
                        menu += "<li>";
                        menu += "<a href='" + item.Url_Rewrite + "'";
                        if (item.Target != null)
                        {
                            menu += "target = '" + item.Target + "'";
                        }
                        menu += ">" + item.mName + "</a>";
                        var listChild = (from m in db.Tbl_Permission_Menu
                                         join map in db.Tbl_Permission_Map on m.mID equals map.mID
                                         where m.mParentID == item.mID && map.pID == pId
                                         select m).ToList();
                        if (listChild != null)
                        {
                            menu += "<ul class='ul-sub-menu'>";
                            foreach (var itemChild in listChild)
                            {
                                menu += "<li>";
                                menu += "<a href='" + itemChild.Url_Rewrite + "'";
                                if (itemChild.Target != null)
                                {
                                    menu += "target = '" + itemChild.Target + "'";
                                }
                                menu += ">" + itemChild.mName + "</a>";
                                var listChild1 = (from m in db.Tbl_Permission_Menu
                                                  join map in db.Tbl_Permission_Map on m.mID equals map.mID
                                                  where m.mParentID == itemChild.mID && map.pID == pId
                                                  select m).ToList();
                                if (listChild1 != null)
                                {
                                    menu += "<ul class='ul-sub2-menu'>";
                                    foreach (var itemChild1 in listChild1)
                                    {
                                        menu += "<li>";
                                        menu += "<a href='" + itemChild1.Url_Rewrite + "'";
                                        if (itemChild1.Target != null)
                                        {
                                            menu += "target = '" + itemChild1.Target + "'";
                                        }
                                        menu += ">" + itemChild1.mName + "</a>";
                                        var listChild2 = (from m in db.Tbl_Permission_Menu
                                                          join map in db.Tbl_Permission_Map on m.mID equals map.mID
                                                          where m.mParentID == itemChild1.mID && map.pID == pId
                                                          select m).ToList();
                                        if (listChild2 != null)
                                        {
                                            menu += "<ul class='ul-sub2-menu'>";
                                            foreach (var itemChild2 in listChild2)
                                            {
                                                menu += "<li>";
                                                menu += "<a href='" + itemChild2.Url_Rewrite + "'";
                                                if (itemChild2.Target != null)
                                                {
                                                    menu += "target = '" + itemChild2.Target + "'";
                                                }
                                                menu += ">" + itemChild2.mName + "</a>";
                                                menu += "</li>";
                                            }
                                            menu += "</ul>";
                                        }
                                        menu += "</li>";
                                    }
                                    menu += "</ul>";
                                }
                                menu += "</li>";
                            }
                            menu += "</ul>";
                        }
                        menu += "</li>";
                    }
                }

                return menu;
            }
        }

        /// <summary>
        /// build Menu
        /// </summary>
        /// <param name="pId"></param>
        /// <returns></returns>
        public string GetMenu_V2(int pId)
        {
            using (var db = new Solution_30shineEntities())
            {
                var menu = "";
                //var listPr = new List<Tbl_Permission_Menu>();
                if (pId == 0)
                {
                    var listPr = (from m in db.Tbl_Permission_Menu
                                  where m.mParentID == 0 && m.MenuShow == true
                                  select m).ToList();
                    foreach (var item in listPr)
                    {
                        menu += "<li><a href='" + item.Url_Rewrite + "'>" + item.mName + "</a>";
                        var listChild = (from m in db.Tbl_Permission_Menu
                                         where m.mParentID == item.mID && m.MenuShow == true
                                         select m).ToList();
                        if (listChild != null)
                        {
                            menu += "<ul class='ul-sub-menu'>";
                            foreach (var itemChild in listChild)
                            {
                                menu += "<li>";
                                menu += "<a href='" + itemChild.Url_Rewrite + "'>" + itemChild.mName + "</a>";
                                var listChild1 = (from m in db.Tbl_Permission_Menu
                                                  where m.mParentID == itemChild.mID && m.MenuShow == true
                                                  select m).ToList();
                                if (listChild1 != null)
                                {
                                    menu += "<ul class='ul-sub2-menu'>";
                                    foreach (var itemChild1 in listChild1)
                                    {
                                        menu += "<li>";
                                        menu += "<a href='" + itemChild1.Url_Rewrite + "'>" + itemChild1.mName + "</a>";
                                        var listChild2 = (from m in db.Tbl_Permission_Menu
                                                          where m.mParentID == itemChild1.mID && m.MenuShow == true
                                                          select m).ToList();
                                        if (listChild2 != null)
                                        {
                                            menu += "<ul class='ul-sub2-menu'>";
                                            foreach (var itemChild2 in listChild2)
                                            {
                                                menu += "<li>";
                                                menu += "<a href='" + itemChild2.Url_Rewrite + "' target = '" + itemChild2.Target ?? "" + "'>" + itemChild2.mName +
                                                        "</a>";
                                                menu += "</li>";
                                            }
                                            menu += "</ul>";
                                        }
                                        menu += "</li>";
                                    }
                                    menu += "</ul>";
                                }
                                menu += "</li>";
                            }
                            menu += "</ul>";
                        }
                        menu += "</li>";
                    }
                }
                else
                {
                    var listPr = db.Store_Permission_Get_Menu_V2(0, pId).ToList();
                    foreach (var item in listPr)
                    {
                        menu += "<li><a href='" + item.MenuLink + "'";
                        if (item.Target != null)
                        {
                            menu += "target='" + item.Target + "'";
                        }
                        else menu += "";
                        menu += ">" + item.MenuName + "</a>";
                        var listChild = db.Store_Permission_Get_Menu_V2(item.mID, pId).ToList();
                        if (listChild != null)
                        {
                            menu += "<ul class='ul-sub-menu'>";
                            foreach (var itemChild in listChild)
                            {
                                menu += "<li>";
                                menu += "<a href='" + itemChild.MenuLink + "'";
                                if (itemChild.Target != null)
                                {
                                    menu += "target='" + itemChild.Target + "'";
                                }
                                else menu += "";
                                menu += ">" + itemChild.MenuName + "</a>";
                                var listChild1 = db.Store_Permission_Get_Menu_V2(itemChild.mID, pId).ToList();
                                if (listChild1 != null)
                                {
                                    menu += "<ul class='ul-sub2-menu'>";
                                    foreach (var itemChild1 in listChild1)
                                    {
                                        menu += "<li>";
                                        menu += "<a href='" + itemChild1.MenuLink + "'";
                                        if (itemChild1.Target != null)
                                        {
                                            menu += "target='" + itemChild1.Target + "'";
                                        }
                                        else menu += "";
                                        menu += ">" + itemChild1.MenuName + "</a>";
                                        var listChild2 = db.Store_Permission_Get_Menu_V2(itemChild1.mID, pId).ToList();
                                        if (listChild2 != null)
                                        {
                                            menu += "<ul class='ul-sub2-menu'>";
                                            foreach (var itemChild2 in listChild2)
                                            {
                                                menu += "<li>";
                                                menu += "<a href='" + itemChild2.MenuLink + "'";
                                                if (itemChild2.Target != null)
                                                {
                                                    menu += "target='" + itemChild2.Target + "'";
                                                }
                                                else menu += "";
                                                menu += ">" + itemChild2.MenuName + "</a>";
                                                menu += "</li>";
                                            }
                                            menu += "</ul>";
                                        }
                                        menu += "</li>";
                                    }
                                    menu += "</ul>";
                                }
                                menu += "</li>";
                            }
                            menu += "</ul>";
                        }
                        menu += "</li>";
                    }
                }

                return menu;
            }
        }

        /// <summary>
        /// Get menu V3
        /// </summary>
        /// <param name="staffId"></param>
        /// <returns></returns>
        public string GetMenuV3(int staffId)
        {
            var menu = "";
            using (var db = new Solution_30shineEntities())
            {
                try
                {
                    if (staffId == 0)
                    {
                        List<cls_AllMenuTemp> allMenu = getAllmenuForRoot();
                        List<cls_AllMenuTemp> listPr = getchildrenMenu(0, allMenu);
                        foreach (var item in listPr)
                        {
                            menu += "<li>";
                            menu += "<a href='" + item.menuLink + "'";
                            menu += ">" + item.menuName + "</a>";
                            List<cls_AllMenuTemp> listChild = getchildrenMenu(item.menuId, allMenu);
                            if (listChild != null)
                            {
                                menu += "<ul class='ul-sub-menu'>";
                                foreach (var itemChild in listChild)
                                {
                                    menu += "<li>";
                                    menu += "<a href='" + itemChild.menuLink + "'";
                                    menu += ">" + itemChild.menuName + "</a>";
                                    List<cls_AllMenuTemp> listChild1 = getchildrenMenu(itemChild.menuId, allMenu);
                                    if (listChild1 != null)
                                    {
                                        menu += "<ul class='ul-sub2-menu'>";
                                        foreach (var itemChild1 in listChild1)
                                        {
                                            menu += "<li>";
                                            menu += "<a href='" + itemChild1.menuLink + "'";
                                            menu += ">" + itemChild1.menuName + "</a>";
                                            List<cls_AllMenuTemp> listChild2 = getchildrenMenu(itemChild1.menuId, allMenu);
                                            if (listChild2 != null)
                                            {
                                                menu += "<ul class='ul-sub2-menu'>";
                                                foreach (var itemChild2 in listChild2)
                                                {
                                                    menu += "<li>";
                                                    menu += "<a href='" + itemChild2.menuLink + "'";
                                                    menu += ">" + itemChild2.menuName + "</a>";
                                                    menu += "</li>";
                                                }
                                                menu += "</ul>";
                                            }
                                            menu += "</li>";
                                        }
                                        menu += "</ul>";
                                    }
                                    menu += "</li>";
                                }
                                menu += "</ul>";
                            }
                            menu += "</li>";
                        }
                    }
                    else
                    {
                        //Get all menu
                        List<cls_AllMenuTemp> allMenu = getAllmenu(staffId);
                        List<cls_AllMenuTemp> listPr = getchildrenMenu(0, allMenu);
                        foreach (var item in listPr)
                        {
                            menu += "<li>";
                            menu += "<a href='" + item.menuLink + "'";
                            menu += ">" + item.menuName + "</a>";
                            List<cls_AllMenuTemp> listChild = getchildrenMenu(item.menuId, allMenu);
                            if (listChild != null)
                            {
                                menu += "<ul class='ul-sub-menu'>";
                                foreach (var itemChild in listChild)
                                {
                                    menu += "<li>";
                                    menu += "<a href='" + itemChild.menuLink + "'";
                                    menu += ">" + itemChild.menuName + "</a>";
                                    List<cls_AllMenuTemp> listChild1 = getchildrenMenu(itemChild.menuId, allMenu);
                                    if (listChild1 != null)
                                    {
                                        menu += "<ul class='ul-sub2-menu'>";
                                        foreach (var itemChild1 in listChild1)
                                        {
                                            menu += "<li>";
                                            menu += "<a href='" + itemChild1.menuLink + "'";
                                            menu += ">" + itemChild1.menuName + "</a>";
                                            List<cls_AllMenuTemp> listChild2 = getchildrenMenu(itemChild1.menuId, allMenu);
                                            if (listChild2 != null)
                                            {
                                                menu += "<ul class='ul-sub2-menu'>";
                                                foreach (var itemChild2 in listChild2)
                                                {
                                                    menu += "<li>";
                                                    menu += "<a href='" + itemChild2.menuLink + "'";
                                                    menu += ">" + itemChild2.menuName + "</a>";
                                                    menu += "</li>";
                                                }
                                                menu += "</ul>";
                                            }
                                            menu += "</li>";
                                        }
                                        menu += "</ul>";
                                    }
                                    menu += "</li>";
                                }
                                menu += "</ul>";
                            }
                            menu += "</li>";
                        }
                    }
                }
                catch (Exception ex)
                {
                    //Push thông báo cho mọi người (by slack) khi có lỗi xảy ra
                    new Helpers.PushNotiErrorToSlack().Push("GA30CQZPF",
                                                             ex.Message + ", " + ex.InnerException,
                                                             this.ToString() + ".GetMenuV3",
                                                             "phucdn",
                                                             "ERP");
                }

                return menu;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="permissionName"></param>
        /// <returns></returns>
        public Tbl_Permission GetPermissionByName(string permissionName)
        {
            try
            {
                return this.db.Tbl_Permission.FirstOrDefault(w => w.pName == permissionName);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public List<Tbl_Permission_Menu> GetModule()
        {
            Solution_30shineEntities db = new Solution_30shineEntities();
            return db.Tbl_Permission_Menu.Where(m => m.mParentID == 0).ToList();
        }

        /// <summary>
        /// //Function cắt chuỗi rewite sang pageId
        /// </summary>
        /// <param name="staffId"></param>
        /// <returns></returns>
        public List<cls_AllMenuTemp> getAllmenu(int staffId)
        {
            string sql = @" DECLARE @staffId INT
                            SET @staffId = " + staffId + @"
                            SELECT menu.Id AS menuId,menu.Pid AS menuParentId,menu.Name AS menuName
	                              ,menu.Link AS menuLink, menu.Path AS menuPath
                            FROM dbo.PermissionMenu  AS menu
	                            INNER JOIN dbo.PermissionMenuAction AS perAction ON menu.Id = perAction.PageId
	                            INNER JOIN dbo.PermissionStaff AS perStaff ON perAction.PermissionId = perStaff.PermissionId
                            WHERE	menu.IsDelete = 0 AND 
		                            menu.IsActive = 1 AND 
		                            perAction.IsDelete = 0 AND 
		                            perStaff.StaffId = @staffId AND 
		                            perStaff.IsDelete = 0 AND 
		                            perStaff.IsActive = 1 AND 
		                            perAction.IsActive = 1
                            GROUP BY menu.Id,menu.Pid,menu.Name,menu.Link,menu.Path";
            var list = db.Database.SqlQuery<cls_AllMenuTemp>(sql).ToList();
            return list;
        }

        /// <summary>
        /// Get list menu For Permission Root
        /// </summary>
        /// <returns></returns>
        public List<cls_AllMenuTemp> getAllmenuForRoot()
        {
            List<cls_AllMenuTemp> allMenu = new List<cls_AllMenuTemp>();
            allMenu = (from a in db.PermissionMenus
                       where a.IsActive == true &&
                             a.IsDelete == false
                       select new cls_AllMenuTemp
                       {
                           menuId = a.Id,
                           menuParentId = a.Pid,
                           menuName = a.Name,
                           menuLink = a.Link,
                           menuPath = a.Path,
                       }).ToList();
            return allMenu;
        }
        public string GetPermisionNames(int staffId)
        {
            string resutl = "";
            var listPermision = (from a in db.PermissionErps
                                 join c in db.PermissionStaffs on a.Id equals c.PermissionId
                                 where a.IsDelete == false &&
                                       a.IsActive == true &&
                                       c.IsDelete == false &&
                                       c.IsDelete == false &&
                                       c.StaffId == staffId
                                 select new
                                 {
                                     permisionName = a.Name,
                                 }).GroupBy(r => r.permisionName).Select(r => r.FirstOrDefault().permisionName).ToList();
            var test = listPermision.Count;

            for (var i = 0; i < listPermision.Count; i++)
            {
                resutl += listPermision[i].ToString();
                if (i < listPermision.Count - 1)
                    resutl += ",";
            }
            return resutl;
        }
        /// <summary>
        ///  //Get chirlden menu
        /// </summary>
        /// <param name="parentId"></param>
        /// <param name="allMenu"></param>
        /// <returns></returns>
        public List<cls_AllMenuTemp> getchildrenMenu(int parentId, List<cls_AllMenuTemp> allMenu)
        {
            List<cls_AllMenuTemp> list = (from a in allMenu
                                          where a.menuParentId == parentId
                                          select a).ToList();
            return list;
        }

        /// <summary>
        /// Hủy connectString khi lấy xong dữ liệu
        /// </summary>
        public void DisposeConnectString()
        {
            db.Dispose();
        }



        /// <summary>
        /// //object temp allmenu for getmenu
        /// </summary>
        public class cls_AllMenuTemp
        {
            public int menuId { get; set; }
            public int? menuParentId { get; set; }
            public string menuName { get; set; }
            public string menuLink { get; set; }
            public string menuPath { get; set; }
            //public string menuPageId { get; set; }
            //public int mapId { get; set; }
            public int? mapPermisionId { get; set; }
            public int? mapPageId { get; set; }
            //public int? mapActionId { get; set; }
        }
    }
}