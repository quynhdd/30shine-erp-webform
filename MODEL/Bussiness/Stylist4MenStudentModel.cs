﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using System.Data.Entity.Migrations;

namespace _30shine.MODEL.Bussiness
{
    public class Stylist4MenStudentModel : IStylist4MenStudentModel
    {

        private Solution_30shineEntities db;
        /// <summary>
        /// Contrustor
        /// </summary>
        public Stylist4MenStudentModel()
        {
            db = new Solution_30shineEntities();
        }

        /// <summary>
        /// Add Stylist_Student
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public Stylist4Men_Student Add(Stylist4Men_Student obj)
        {
            try
            {
                if (obj != null)
                {
                    var record = new Stylist4Men_Student();
                    record.Fullname = obj.Fullname;
                    record.Phone = obj.Phone;
                    record.Email = obj.Email;
                    record.ClassId = obj.ClassId;
                    record.CityId = obj.CityId;
                    record.DistrictId = obj.DistrictId;
                    record.Address = obj.Address;
                    record.ImageCMT1 = obj.ImageCMT1;
                    record.ImageCMT2 = obj.ImageCMT2;
                    record.StudyPackageId = obj.StudyPackageId;
                    record.NumberOfCreateProfile = obj.NumberOfCreateProfile;
                    record.TotalAmountCollected = obj.TotalAmountCollected;
                    record.TotalAmountPaid = obj.TotalAmountPaid;
                    record.TotalBill = obj.TotalBill;
                    record.IsAccept = obj.IsAccept;
                    record.Note = obj.Note;
                    record.LevelOfConcern = obj.LevelOfConcern;
                    record.CreatedTime = obj.CreatedTime;
                    record.Publish = obj.Publish;
                    record.IsDelete = obj.IsDelete;
                    record.NumberCMT = obj.NumberCMT;
                    record.Password = obj.Password;
                    record.SalonId = obj.SalonId;
                    record.PointLTCat = obj.PointLTCat;
                    record.PointLTKN = obj.PointLTKN;
                    record.PointTheoryCut = obj.PointTheoryCut;
                    record.Vote = obj.Vote;
                    record.Type = obj.Type;
                    record.StatusTuition = obj.StatusTuition;
                    this.db.Stylist4Men_Student.Add(record);
                    this.db.SaveChanges();


                }
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Update Stylist_student
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public Stylist4Men_Student Update(Stylist4Men_Student obj)
        {
            try
            {
                var record = db.Stylist4Men_Student.Find(obj.Id);
                if (record != null)
                {
                    record.Fullname = obj.Fullname;
                    record.Phone = obj.Phone;
                    record.Email = obj.Email;
                    record.ClassId = obj.ClassId;
                    record.CityId = obj.CityId;
                    record.DistrictId = obj.DistrictId;
                    record.Address = obj.Address;
                    record.ImageCMT1 = obj.ImageCMT1;
                    record.ImageCMT2 = obj.ImageCMT2;
                    record.StudyPackageId = obj.StudyPackageId;
                    record.TotalAmountCollected = obj.TotalAmountCollected;
                    record.TotalAmountPaid = obj.TotalAmountPaid;
                    //record.TotalBill = obj.TotalBill;
                    record.IsAccept = obj.IsAccept;
                    record.Note = obj.Note;
                    record.LevelOfConcern = obj.LevelOfConcern;
                    record.ModifiedTime = obj.ModifiedTime;
                    record.Publish = obj.Publish;
                    record.IsDelete = obj.IsDelete;
                    record.NumberCMT = obj.NumberCMT;
                    record.SalonId = obj.SalonId;
                    record.StatusTuition = obj.StatusTuition;
                    this.db.Stylist4Men_Student.AddOrUpdate(record);
                    this.db.SaveChanges();
                }
                return obj;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Delete Stylist student
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public Stylist4Men_Student Delete(int Id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// get list student
        /// </summary>
        /// <returns></returns>
        public List<Stylist4Men_Student> GetListStudent()
        {
            try
            {
                var list = db.Stylist4Men_Student.Where(w => w.IsDelete == false && w.Publish == true).OrderByDescending(o => o.Id).ToList();
                return list;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Check trùng số điện thoại
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public Stylist4Men_Student IssetPhoneNumer(string value)
        {
            try
            {
                return this.db.Stylist4Men_Student.FirstOrDefault(w => w.Phone == value && w.IsDelete == false);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Update số lần tiếp cận
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public Stylist4Men_Student UpdateNumberOfCreateProfile(Stylist4Men_Student obj)
        {
            try
            {

                var record = db.Stylist4Men_Student.Find(obj.Id);
                if (record != null)
                {
                    record.NumberOfCreateProfile = obj.NumberOfCreateProfile;
                    this.db.Stylist4Men_Student.AddOrUpdate(record);
                    this.db.SaveChanges();
                }

                return obj;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Bind list data Student
        /// </summary>
        /// <param name="SalonId"></param>
        /// <param name="ClassId"></param>
        /// <returns></returns>
        public List<Store_Stylist4Men_HocVienWhereSalonAndClass_V1_Result> GetByAllListStudent(DateTime timeFrom, DateTime timeTo, int ClassId, int levelOfConcern, int numberOfCreateProfile)
        {
            try
            {
                var list = db.Store_Stylist4Men_HocVienWhereSalonAndClass_V1(timeFrom, timeTo, ClassId, levelOfConcern, numberOfCreateProfile).OrderBy(o => o.Id).ToList();
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// get record where Id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public Stylist4Men_Student GetById(int Id)
        {
            try
            {
                var record = db.Stylist4Men_Student.Where(w => w.Id == Id).FirstOrDefault();
                return record;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        ///  check trùng tài khoản
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool IssetEmail(string value)
        {
            try
            {
                var kt = false;
                var data = db.Stylist4Men_Student.FirstOrDefault(w => w.Email == value && w.IsDelete == false);
                if (data != null)
                {
                    kt = true;
                }
                return kt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Insert data to Học phí
        /// </summary>
        /// <returns></returns>
        public Stylist4Men_Student GetRecordNew()
        {
            try
            {
                var data = this.db.Stylist4Men_Student.OrderByDescending(o => o.Id).FirstOrDefault();
                return data;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}