﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using _30shine.Helpers;
using _30shine.MODEL.CustomClass;
namespace _30shine.MODEL.Bussiness
{
    public class QLKhoSalonModel : IQLKhoSalonModel
    {
        private Solution_30shineEntities db;
        public QLKhoSalonModel()
        {
            this.db = new Solution_30shineEntities();
        }

        /// <summary>
        /// hàm lấy thông tin kho với, tất cả salon
        /// </summary>
        /// <param name="TimeFrom"></param>
        /// <param name="TimeTo"></param>
        /// <returns></returns>
        public List<QLKhoSalonClass> GetListNoSalon(DateTime TimeFrom, DateTime TimeTo, int SalonId)
        {
            try
            {
                var data = (from a in db.QLKho_SalonOrder
                            join b in db.QLKho_SalonOrder_Status on a.StatusId equals b.Id
                            join c in db.Tbl_Salon on a.SalonId equals c.Id
                            where a.IsDelete == false && (a.OrderDate >= TimeFrom && a.OrderDate <= TimeTo)
                            && ((a.SalonId == SalonId) || (SalonId == 0))
                            && (a.CheckCosmetic == 0 || a.CheckCosmetic == null)
                            //where a.IsDelete != true
                            select new QLKhoSalonClass
                            {
                                Id = a.Id,
                                BillCode = a.BillCode,
                                CreatedTime = a.CreatedTime,
                                SalonId = a.SalonId,
                                StatusId = a.StatusId,
                                NoteSalon = a.NoteSalon,
                                NoteKho = a.NoteKho,
                                ProductIds = a.ProductIds,
                                OrderDate = a.OrderDate,
                                ExportTime = a.ExportTime,
                                ReceivedTime = a.ReceivedTime,
                                StatusTitle = b.Title,
                                SalonName = c.Name
                            }).ToList();

                return data;

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}