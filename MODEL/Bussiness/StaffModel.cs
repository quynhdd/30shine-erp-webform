﻿using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using _30shine.MODEL.IO;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using _30shine.MODEL.CustomClass;
using _30shine.Helpers;
using System.Configuration;

namespace _30shine.MODEL.BO
{
    public class StaffModel : IStaffModel
    {
        private Solution_30shineEntities db;
        private IStaffFluctuationModel iModel;
        private static StaffModel _Instance;
        public StaffModel()
        {
            this.db = new Solution_30shineEntities();
            this.iModel = new StaffFluctuationModel();
        }

        public static StaffModel Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new StaffModel();
                }
                return _Instance;
            }
        }
        public Staff GetStaffById(int id)
        {
            try
            {
                return this.db.Staffs.FirstOrDefault(w => w.Id == id);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public Msg GetSalonShortName(int salonId)
        {
            Msg result = new Msg();
            try
            {
                var record = db.Tbl_Salon.Where(r => r.IsDelete == 0 && r.Publish == true && r.Id == salonId).FirstOrDefault();
                if (record != null)
                {
                    result.success = true;
                    result.msg = "Thành công";
                    result.data = record.ShortName;
                }
            }
            catch (Exception ex)
            {
                result.success = false;
                result.msg = "Đã có lỗi xảy ra";
            }
            return result;
        }
        public Msg GetPathContractTemp(int DepartmentId, int TypeContract)
        {
            if (TypeContract == 5 || TypeContract == 6)
            {
                DepartmentId = 9;
            }
            Msg result = new Msg();
            result.success = false;
            result.msg = "Thất bại";
            try
            {
                var record = db.Contracts.Where(r => r.IsDelete == false && r.DepartmentId == DepartmentId && r.TypeContract == TypeContract).FirstOrDefault();
                if (record != null)
                {
                    result.success = true;
                    result.msg = "Thành công";
                    result.data = record.PathContract;
                }
            }
            catch (Exception ex)
            {
                result.success = false;
                result.msg = "Đã có lỗi xảy ra !";
            }
            return result;
        }
        public bool IssetBankAccount(string bankAccount, Staff staff = null)
        {
            try
            {
                var isset = false;
                var staffObj = new Staff();
                if (staff != null)
                {
                    staffObj = this.db.Staffs.FirstOrDefault(w => w.Id != staff.Id && w.NganHang_SoTK == bankAccount && w.Active == 1 && w.IsDelete == 0);
                    if (staffObj != null)
                    {
                        isset = true;
                    }
                }
                else
                {
                    staffObj = this.db.Staffs.FirstOrDefault(w => w.NganHang_SoTK == bankAccount && w.Active == 1 && w.IsDelete == 0);
                    if (staffObj != null)
                    {
                        isset = true;
                    }
                }
                return isset;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool IssetPhone(string phone, Staff staff = null)
        {
            try
            {
                var isset = false;
                var staffObj = new Staff();
                if (staff != null)
                {
                    staffObj = this.db.Staffs.FirstOrDefault(w => w.Id != staff.Id && w.Phone == phone && w.Active == 1 && w.IsDelete == 0);
                    if (staffObj != null)
                    {
                        isset = true;
                    }
                }
                else
                {
                    staffObj = this.db.Staffs.FirstOrDefault(w => w.Phone == phone && w.Active == 1 && w.IsDelete == 0);
                    if (staffObj != null)
                    {
                        isset = true;
                    }
                }
                return isset;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool IssetEmail(string email, Staff staff = null)
        {
            try
            {
                var isset = false;
                var staffObj = new Staff();
                if (staff != null)
                {
                    staffObj = this.db.Staffs.FirstOrDefault(w => w.Id != staff.Id && w.Email == email && w.Active == 1 && w.IsDelete == 0);
                    if (staffObj != null)
                    {
                        isset = true;
                    }
                }
                else
                {
                    staffObj = this.db.Staffs.FirstOrDefault(w => w.Email == email && w.Active == 1 && w.IsDelete == 0);
                    if (staffObj != null)
                    {
                        isset = true;
                    }
                }
                return isset;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool IssetTaxCode(string taxCode, Staff staff = null)
        {
            try
            {
                var isset = false;
                var staffObj = new Staff();
                if (staff != null)
                {
                    staffObj = this.db.Staffs.FirstOrDefault(w => w.Id != staff.Id && w.MST == taxCode && w.Active == 1 && w.IsDelete == 0);
                    if (staffObj != null)
                    {
                        isset = true;
                    }
                }
                else
                {
                    staffObj = this.db.Staffs.FirstOrDefault(w => w.MST == taxCode && w.Active == 1 && w.IsDelete == 0);
                    if (staffObj != null)
                    {
                        isset = true;
                    }
                }
                return isset;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool IssetInsurranceNumber(string insurNumber, Staff staff = null)
        {
            try
            {
                var isset = false;
                var staffObj = new Staff();
                if (staff != null)
                {
                    staffObj = this.db.Staffs.FirstOrDefault(w => w.Id != staff.Id && w.NumberInsurrance == insurNumber && w.Active == 1 && w.IsDelete == 0);
                    if (staffObj != null)
                    {
                        isset = true;
                    }
                }
                else
                {
                    staffObj = this.db.Staffs.FirstOrDefault(w => w.NumberInsurrance == insurNumber && w.Active == 1 && w.IsDelete == 0);
                    if (staffObj != null)
                    {
                        isset = true;
                    }
                }
                return isset;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool IssetPersonId(string personId, Staff staff = null)
        {
            try
            {
                var isset = false;
                var staffObj = new Staff();
                if (staff != null)
                {
                    staffObj = this.db.Staffs.FirstOrDefault(w => w.Id != staff.Id && w.StaffID == personId && w.Active == 1 && w.IsDelete == 0);
                    if (staffObj != null)
                    {
                        isset = true;
                    }
                }
                else
                {
                    staffObj = this.db.Staffs.FirstOrDefault(w => w.StaffID == personId && w.Active == 1 && w.IsDelete == 0);
                    if (staffObj != null)
                    {
                        isset = true;
                    }
                }
                return isset;
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Add
        /// </summary>
        public Staff Add(Staff obj)
        {
            try
            {
                if (obj != null)
                {
                    var record = new Staff();
                    record.Fullname = obj.Fullname;
                    record.StaffID = obj.StaffID;
                    record.DateJoin = obj.DateJoin;
                    record.SkillLevel = obj.SkillLevel;
                    record.Phone = obj.Phone;
                    record.Email = obj.Email;
                    record.Avatar = obj.Avatar;
                    record.SalaryByPerson = obj.SalaryByPerson;
                    record.RequireEnroll = obj.RequireEnroll;
                    record.SN_day = obj.SN_day;
                    record.SN_month = obj.SN_month;
                    record.SN_year = obj.SN_year;
                    record.CityId = obj.CityId;
                    record.Address = obj.Address;
                    record.CreatedDate = obj.CreatedDate;
                    record.DistrictId = obj.DistrictId;
                    record.Gender = obj.Gender;
                    record.Type = obj.Type;
                    record.MST = obj.MST;
                    record.NameCMT = obj.NameCMT;
                    record.NumberInsurrance = obj.NumberInsurrance;
                    record.CMTimg1 = obj.CMTimg1;
                    record.CMTimg2 = obj.CMTimg2;
                    record.NgayTinhThamNien = obj.NgayTinhThamNien;
                    record.NganHang_ChiNhanh = obj.NganHang_ChiNhanh;
                    record.NganHang_SoTK = obj.NganHang_SoTK;
                    record.NganHang_TenTK = obj.NganHang_TenTK;
                    record.SalonId = obj.SalonId;
                    record.SourceId = obj.SourceId;
                    record.IDProvidedDate = obj.IDProvidedDate;
                    record.IDProviderLocale = obj.IDProviderLocale;
                    record.Password = obj.Password;
                    record.IsDelete = obj.IsDelete;
                    record.Publish = obj.Publish;
                    record.Active = obj.Active;
                    record.S4MClassId = obj.S4MClassId;
                    record.StudentId = obj.StudentId;
                    record.UngvienID = obj.UngvienID;
                    record.Note = obj.Note;
                    record.IsHoiQuan = obj.IsHoiQuan;
                    this.db.Staffs.Add(record);
                    this.db.SaveChanges();
                    // Update OrderCode
                    //Staff orderCode = (from c in db.Staffs
                    //                   where c.Id == record.Id
                    //                   select c).FirstOrDefault();
                    //orderCode.OrderCode = obj.Id.ToString();
                    //db.SaveChanges();
                    UpdateOrderCode(record.Id);
                    this.iModel.AddOrUpdateRecruitment(record);
                    return record;
                }
                else
                {
                    throw new Exception("Có lỗi xảy ra!");
                }

            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Update OrderCode theo Staff vừa tạo thành công!
        /// </summary>
        /// <param name="id"></param>
        private void UpdateOrderCode(int staffId)
        {
            try
            {
                var data = (from c in db.Staffs
                            where c.Id == staffId
                            select c
                            ).FirstOrDefault();
                if (data != null)
                {
                    data.OrderCode = staffId.ToString();
                    db.SaveChanges();
                }
            }
            catch { }
        }

        /// <summary>
        /// Update
        /// </summary>
        public Staff Update(Staff obj)
        {
            try
            {
                var record = db.Staffs.Find(obj.Id);
                if (record != null)
                {
                    record.Fullname = obj.Fullname;
                    record.StaffID = obj.StaffID;
                    record.DateJoin = obj.DateJoin;
                    record.SkillLevel = obj.SkillLevel;
                    record.Phone = obj.Phone;
                    record.Email = obj.Email;
                    record.Avatar = obj.Avatar;
                    record.SalaryByPerson = obj.SalaryByPerson;
                    record.RequireEnroll = obj.RequireEnroll;
                    record.SN_day = obj.SN_day;
                    record.SN_month = obj.SN_month;
                    record.SN_year = obj.SN_year;
                    record.Address = obj.Address;
                    record.CreatedDate = obj.CreatedDate;
                    record.CityId = obj.CityId;
                    record.DistrictId = obj.DistrictId;
                    record.Gender = obj.Gender;
                    record.Type = obj.Type;
                    record.MST = obj.MST;
                    record.NameCMT = obj.NameCMT;
                    record.NumberInsurrance = obj.NumberInsurrance;
                    record.CMTimg1 = obj.CMTimg1;
                    record.CMTimg2 = obj.CMTimg2;
                    record.NgayTinhThamNien = obj.NgayTinhThamNien;
                    record.NganHang_ChiNhanh = obj.NganHang_ChiNhanh;
                    record.NganHang_SoTK = obj.NganHang_SoTK;
                    record.NganHang_TenTK = obj.NganHang_TenTK;
                    record.SalonId = obj.SalonId;
                    record.SourceId = obj.SourceId;
                    record.Password = obj.Password;
                    record.IDProvidedDate = obj.IDProvidedDate;
                    record.IDProviderLocale = obj.IDProviderLocale;
                    record.IsDelete = obj.IsDelete;
                    record.Active = obj.Active;
                    record.UngvienID = obj.UngvienID;
                    record.Note = obj.Note;
                    record.IsHoiQuan = obj.IsHoiQuan;
                    this.db.Staffs.AddOrUpdate(record);
                    this.db.SaveChanges();
                    this.iModel.AddOrUpdateRecruitment(record);
                    return obj;
                }
                else
                {
                    throw new Exception("Có lỗi xảy ra!");
                }
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
        }

        /// <summary>
        /// Delete
        /// </summary>
        public Staff Delete(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get List staff
        /// </summary>
        public List<OutputStaffListing> GetListStaff(DateTime timefrom, DateTime timeto, List<int> listSalonId, int staffTypeId, int skillLevelId, int active, int snDay, int snMonth, string email, int permission, int contract, int profile)
        {
            List<OutputStaffListing> listings = new List<OutputStaffListing>();
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    string sql = $@"DECLARE
                                    @listSalonId VARCHAR(500),
                                    @department int,
                                    @skillLevel int,
                                    @active INT,
                                    @day INT,
                                    @month INT,
                                    @email NVARCHAR(50),
								    @permission INT,
                                    @contract INT,
                                    @profile INT,
                                    @timefrom VARCHAR(50),
                                    @timeto VARCHAR(50)
                                    SET @listSalonId = '{String.Join(",", listSalonId)}';
                                    SET @department = {staffTypeId};
                                    SET @skillLevel = {skillLevelId };
                                    SET @active ={ active };
                                    SET @day = {snDay };
                                    SET @month ={ snMonth };
                                    SET @email = '{ email } ';
                                    SET @permission = { permission };
                                    SET @contract = { contract };
                                    SET @profile = {profile };
                                    SET @timefrom = '{string.Format("{0:yyyy/MM/dd}", timefrom)}';
                                    SET @timeto = '{string.Format("{0:yyyy/MM/dd}", timeto)}';
                                    WITH ListRecord AS
							        (
								        SELECT 
									        staff.Id AS StaffId, staff.Fullname AS FullName,
									        ISNULL(salon.ShortName,'') as SalonName, 
									        ISNULL(skillLevel.Name,'') as LevelName,
									        ISNULL(department.Name,'') as DepartmentName,
									        staff.Phone AS StaffPhone,
									        staff.Email AS StaffEmail,
											staff.NgayTinhThamNien,
											staff.Active,
									        ISNULL(config.Label,'') AS ContractsName,
									        (SELECT Id FROM dbo.StaffProfileMap WHERE StatusProfileId = @profile AND StaffId = staff.Id AND IsDelete = 0) AS StaffProfileId,
									        CASE 
									        WHEN config.Value = '1' THEN DATEADD(month, 3, mapcontracts.ContractDate) 
									        WHEN config.Value = '2' THEN DATEADD(month, 6, mapcontracts.ContractDate)
									        WHEN config.Value = '3' THEN DATEADD(month, 12, mapcontracts.ContractDate)
									        WHEN config.Value = '4' THEN DATEADD(month, 2, mapcontracts.ContractDate) 
									        WHEN config.Value = '5' THEN DATEADD(month, 2, mapcontracts.ContractDate)
									        WHEN config.Value = '6' THEN DATEADD(month, 12, mapcontracts.ContractDate)
									        ELSE NULL END  
									        AS ContractDate,
									        mapcontracts.Active AS ContractsActive
								            FROM Staff as staff
								            LEFT JOIN Staff_Type as department on department.Id = staff.[Type]
								            LEFT JOIN dbo.TinhThanh AS city ON  city.ID = staff.CityId
								            LEFT JOIN Tbl_Salon as salon on salon.Id = staff.SalonId
								            LEFT JOIN Tbl_SkillLevel as skillLevel on skillLevel.Id = staff.SkillLevel
								            LEFT JOIN dbo.TuyenDung_Nguon AS [source] ON [source].id= staff.SourceId
								            LEFT JOIN dbo.StaffProfileMap AS pro ON pro.StaffId = staff.Id
								            LEFT JOIN dbo.Tbl_Status AS st ON st.Value = pro.StatusProfileId
								            LEFT JOIN dbo.StaffContractMap AS mapcontracts ON mapcontracts.StaffId = staff.Id 
								            LEFT JOIN dbo.Tbl_Config AS config ON mapcontracts.ContractId = TRY_PARSE(config.Value AS INT) AND config.[Key] = 'hop_dong'
								            LEFT JOIN dbo.PermissionStaff AS per ON per.StaffId = staff.Id
								            WHERE 
									            staff.IsDelete = 0 AND staff.NgayTinhThamNien >= @timefrom AND staff.NgayTinhThamNien < @timeto		
									            AND EXISTS (SELECT Item FROM dbo.SplitString(@listSalonId,',') WHERE staff.SalonId = CAST(Item AS INT)) 
									            AND ((staff.[Type] = @department) or (@department = 0 and staff.[Type] > 0))
									            AND ((staff.SkillLevel= @skillLevel) or (@skillLevel = 0 and staff.SkillLevel > 0))
									            AND ((staff.SN_day = @day) or (@day = 0))
									            AND ((staff.SN_month = @month) or (@month = 0))
									            AND staff.Active = @active
									            AND ((per.PermissionId = @permission) or (@permission = 0))
									            AND ((mapcontracts.ContractId = @contract AND @contract <> -1) or (@contract = 0) OR (@contract = -1 AND mapcontracts.ContractId IS NULL))
                                            AND ((staff.Email = @email) or (@email = '' ))
								            GROUP BY staff.Id,staff.Fullname,salon.ShortName,skillLevel.Name,department.Name,
                                                     staff.Phone,staff.Email,staff.NgayTinhThamNien,staff.Active,skillLevel.[Order],mapcontracts.Active,
                                                     mapcontracts.ContractId,mapcontracts.ContractDate,config.Label,config.Value
								            )
							        SELECT a.StaffId, a.FullName,a.StaffEmail,a.SalonName,a.LevelName,a.DepartmentName,a.StaffPhone,a.ContractsName,a.ContractDate,ISNULL(CONVERT(VARCHAR(10),a.NgayTinhThamNien,105),'') AS NgayTinhThamNien,
							        CASE
							        WHEN a.ContractDate <= GETDATE() THEN '#ff6666' ELSE '' END AS ColorDate,
									ISNULL(CASE WHEN f.Id IS NOT NULL AND f.CreatedDate > a.NgayTinhThamNien AND a.Active = 0 THEN DATEDIFF(dd, a.NgayTinhThamNien, f.CreatedDate) ELSE DATEDIFF(dd, a.NgayTinhThamNien, GETDATE()) END,0) AS SeniorityDate 
							        FROM ListRecord AS a
									LEFT JOIN dbo.Staff_Fluctuations AS f ON f.StaffId = a.StaffId AND f.StatusId = 12 AND f.IsDetele = 0
							        WHERE ((a.ContractsActive = 1) OR (a.ContractsActive IS NULL)) AND a.StaffProfileId IS NULL
							        ORDER by a.StaffId DESC";
                    listings = db.Database.SqlQuery<OutputStaffListing>(sql).ToList();
                    return listings;
                }
            }
            catch (Exception ex)
            {
                listings = new List<OutputStaffListing>();
            }
            return listings;
        }


        /// <summary>
        /// count staff mistake 
        /// </summary>
        /// <param name="timeFrom"></param>
        /// <param name="timeTo"></param>
        /// <param name="salonId"></param>
        /// <param name="stylistId"></param>
        /// <returns></returns>
        public int GetStaffErrorCountId(DateTime timeFrom, DateTime timeTo, int salonId, int stylistId)
        {
            try
            {
                int iCount = 0;
                var iCountById = db.Store_StaffMistake_CountID(timeFrom, timeTo, salonId, stylistId).ToList();
                if (iCountById.Count > 0)
                {
                    foreach (var item in iCountById)
                    {
                        iCount = (int)item.CountId;
                    }
                }
                else
                {
                    iCountById = new List<Store_StaffMistake_CountID_Result>();
                    foreach (var item in iCountById)
                    {
                        item.CountId = 0;
                    }

                }
                return iCount;
            }
            catch (Exception)
            {

                throw;
            }
        }


        /// <summary>
        /// Total kcs of stylist
        /// </summary>
        /// <param name="timeFrom"></param>
        /// <param name="timeTo"></param>
        /// <param name="salonId"></param>
        /// <param name="stylistId"></param>
        /// <returns></returns>
        public float GetStaffKcsById(DateTime timeFrom, DateTime timeTo, int salonId, int stylistId)
        {
            try
            {
                float fKCS = 0;
                var KCS = db.SCSC_BindStaffWhereSalon_Store(timeFrom, timeTo, salonId, stylistId).ToList();
                if (KCS.Count > 0)
                {
                    foreach (var item in KCS)
                    {
                        fKCS = (float)item.Total_SCSC_Error;
                    }
                }
                else
                {
                    KCS = new List<SCSC_BindStaffWhereSalon_Store_Result>();
                    foreach (var item in KCS)
                    {
                        item.Total_SCSC_Error = 0;
                    }

                }
                return fKCS;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<StaffMistake5CClass> GetList5CStaffBySalon(int salonId, int _DepartmentId, DateTime WorkDate)
        {
            List<StaffMistake5CClass> data = new List<StaffMistake5CClass>();
            try
            {
                var data1 = (from a in db.FlowTimeKeepings
                             join b in db.Staffs on a.StaffId equals b.Id
                             join c in db.Staff_Type on b.Type equals c.Id
                             where a.IsDelete == 0
                             && a.WorkDate == WorkDate
                             && a.SalonId == salonId
                             && a.IsEnroll == true
                             && b.IsDelete == 0 && b.Active == 1
                             && c.IsDelete == 0
                             && (b.Type == _DepartmentId || (b.Type > 0 && _DepartmentId == 0))

                             select new StaffMistake5CClass
                             {
                                 //Rating_salary = c.Ratting_Salary,
                                 StaffId = a.StaffId,
                                 StaffName = b.Fullname,
                                 Department = c.Name,
                                 DeparmentId = b.Type.Value,
                                 SkillLevel = b.SkillLevel.Value
                             }).ToList();

                var data2 = (from a in data1
                             join b in db.SalaryConfigs on a.DeparmentId equals b.DepartmentId
                             where b.IsDeleted == false && b.LevelId == a.SkillLevel
                             select new StaffMistake5CClass
                             {
                                 RatingSalary = b.RattingSalary,
                                 StaffId = a.StaffId,
                                 StaffName = a.StaffName,
                                 Department = a.Department,
                                 DeparmentId = a.DeparmentId,
                                 SkillLevel = a.SkillLevel
                             }).ToList();
                data = data2;
            }
            catch (Exception ex)
            {

            }
            return data;
        }

        public List<Staff> GetListStaffEnrollBySalon(int salonId, DateTime workDate)
        {
            var ListOfStaffs = new List<Staff>();
            ListOfStaffs = this.db.Database.SqlQuery<Staff>($"SELECT s.* FROM Staff as s RIGHT JOIN[FlowTimeKeeping] as f ON f.StaffId = s.Id WHERE s.IsDelete = 0 AND f.IsEnroll = 1 AND f.SalonId = {salonId} AND f.WorkDate = '{String.Format("{0:yyyy-MM-dd}", workDate)}' ORDER BY s.Id DESC").ToList();
            return ListOfStaffs;
        }

        public static StaffModel getInstance()
        {
            return new StaffModel();
        }

        public Staff GetByOrderCode(string orderCode)
        {
            try
            {
                return this.db.Staffs.FirstOrDefault(w => w.OrderCode == orderCode);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<Staff> ListGiaoVienS4M(int typeGV)
        {
            try
            {
                var list = db.Staffs.Where(w => w.Type == typeGV && w.IsDelete == 0 && w.Active == 1).ToList();
                return list;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Get data ứng viên
        /// </summary>
        /// author: Dungnm
        /// <param name="Id"></param>
        /// <returns></returns>
        public TuyenDung_UngVien GetDataRecruitment(int Id)
        {
            try
            {
                var objRecruitment = new TuyenDung_UngVien();
                var objStaff = db.Staffs.FirstOrDefault(w => w.Id == Id);
                if (objStaff != null)
                {
                    objRecruitment = db.TuyenDung_UngVien.FirstOrDefault(w => w.Id == objStaff.UngvienID);
                }
                return objRecruitment;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        ///  Báo cáo điểm SCSC TB by Id stylist
        ///  Author: dungnm
        /// </summary>
        /// <param name="timeFrom"></param>
        /// <param name="timeTo"></param>
        /// <param name="salonID"></param>
        /// <param name="StylisId"></param>
        /// <returns></returns>
        public string BindHairStyle(string timeFrom, string timeTo, int StylisId)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    CultureInfo culture = new CultureInfo("vi-VN");
                    DateTime TimeFrom = Convert.ToDateTime(timeFrom, culture);
                    DateTime TimeTo = Convert.ToDateTime(timeTo, culture);
                    var sql = @"with TempTotalBill as(
                                    select b.Staff_Hairdresser_Id as StaffId, COUNT(b.Id) as TotalBill, api.Title as HairName
                                    from BillService as b
	                                inner join Staff as staff on b.Staff_Hairdresser_Id = staff.Id
                                    inner join Tbl_Salon as salon on staff.SalonId=salon.Id
                                    inner join Customer_HairMode_Bill as c on c.BillId = b.Id
	                                INNER JOIN FlowService AS flow ON b.Id=flow.BillId
	                                inner join Api_HairMode as api on api.Id = c.HairStyleId
	                                where 
                                    b.CreatedDate between '" + TimeFrom + @"' and '" + TimeTo + @"' and b.ServiceIds !='' and b.IsDelete =0 and b.Pending = 0 	
	                                AND LEN(b.ServiceIds) =101
                                    and  salon.IsDelete =0 and salon.IsSalonHoiQuan = 0
                                    and c.HairStyleId = api.Id
	                                and flow.ServiceId=53
	                                and b.Staff_Hairdresser_Id = '" + StylisId + @"' 
                                    group by b.Staff_Hairdresser_Id, api.Title
                                    )
                                    --select StaffId from TempTotalBill group by StaffId having COUNT(StaffId) > 1  end
                                    ,
                                    TempTotalPointSCSC as (
                                    select b.Staff_Hairdresser_Id as StaffId, SUM(s.PointError) as TotalPointSCSC  
                                    from BillService as b 
	                                    inner join Staff as staff on b.Staff_Hairdresser_Id = staff.Id
                                        inner join Tbl_Salon as salon on staff.SalonId=salon.Id
                                        inner join SCSC_CheckError as s on b.Id=s.BillService_ID
                                        inner join Customer_HairMode_Bill as c on c.BillId= b.Id
	                                    INNER JOIN FlowService AS flow ON b.Id=flow.BillId
		                                inner join Api_HairMode as api on api.Id = c.HairStyleId
                                    where 
                                        b.CreatedDate between '" + TimeFrom + @"' and '" + TimeTo + @"'   
		                                and b.ServiceIds !='' and b.IsDelete =0 and b.Pending = 0  AND LEN(b.ServiceIds) =101
                                        and c.HairStyleId = api.Id
		                                and b.Staff_Hairdresser_Id = '" + StylisId + @"'
                                        and  salon.IsDelete =0 and salon.IsSalonHoiQuan = 0 
                                        and s.IsDelete=0 and s.Publish=1 and s.PointError > 0
	                                    and flow.ServiceId=53
	                                Group by b.Staff_Hairdresser_Id
                                    ) 
                                    --select * from TempTotalPointSCSC where StaffId = 1044 end
                                    ,
                                TempAVGTimeCat as (
                                    select  
                                    a.Staff_Hairdresser_Id as StaffId, 
	                                SUM(DATEDIFF(MINUTE, DATEADD(MINUTE, 12, a.InProcedureTime ),case when a.UploadImageTime is null then a.CompleteBillTime ELSE a.UploadImageTime END )) as AVGTimeCat
                                    from BillService as a 
	                                inner join Staff as staff on a.Staff_Hairdresser_Id = staff.Id
                                    INNER JOIN Customer_HairMode_Bill as b on a.Id = b.BillId
                                    INNER JOIN Tbl_Salon as salon on staff.SalonId = salon.Id
                                    INNER JOIN FlowService AS flow ON a.Id=flow.BillId
	                                inner join Api_HairMode as api on api.Id = b.HairStyleId
                                    where
                                        a.CreatedDate between '" + TimeFrom + @"' and '" + TimeTo + @"' AND a.ServiceIds !='' 
                                        AND LEN(a.ServiceIds) =101 AND a.IsDelete = 0 AND a.Pending = 0
		                                and a.Staff_Hairdresser_Id = '" + StylisId + @"'
                                        AND b.HairStyleId = api.Id
                                        AND salon.IsDelete = 0  AND salon.Publish = 1  AND salon.IsSalonHoiQuan = 0
                                        AND flow.ServiceId=53
                                        GROUP BY a.Staff_Hairdresser_Id
                                    )
                                --select * from TempAVGTimeCat end
                                select a.StaffId,s.Fullname as StaffName, a.HairName,a.TotalBill,
                                    MAX(ROUND(CAST((ISNULL(e.TotalPointSCSC,0)) as float) / CAST((case when ISNULL(a.TotalBill,0) = 0 then 1 else a.TotalBill end)   as float) ,2)) as PointSCSC_TB,
                                    MAX(ROUND(CAST((ISNULL(t.AVGTimeCat,0)) as float) / CAST((case when ISNULL(a.TotalBill,0) = 0 then 1 else a.TotalBill end)  as float) ,2)) as TimeCatTB
                                    from TempTotalBill as a
                                    left join TempTotalPointSCSC as e on a.StaffId = e.StaffId
                                    left join TempAVGTimeCat as t on t.StaffId=a.StaffId
                                    left join Staff as s on a.StaffId = s.Id
                                    where a.StaffId = '" + StylisId + @"'
                                    GROUP BY a.StaffId, s.Fullname, a.HairName,a.TotalBill";
                    var list = new List<cls_SCSC_BindHairStyle_Vesion>();
                    list = db.Database.SqlQuery<cls_SCSC_BindHairStyle_Vesion>(sql).OrderByDescending(w => w.PointSCSC_TB).ToList();
                    return new JavaScriptSerializer().Serialize(list);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Staff_Type> ListDepartment()
        {
            var list = new List<Staff_Type>();
            try
            {
                list = db.Staff_Type.Where(r => r.IsDelete == 0 && r.Publish == true).ToList();
            }
            catch (Exception ex) { }
            return list;
        }

        public object GetDataFiles(DateTime endDate, List<int> SalonIds)
        {
            try
            {
                //using db bi
                db.Database.Connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLConnectionStringBI"].ConnectionString;
                int index = 2;
                var mindate = DateTime.MinValue;

                var listSalon = (
                                    from a in db.Tbl_Salon
                                    join b in db.TinhThanhs on a.CityId equals b.ID
                                    where SalonIds.Contains(a.Id) && a.IsDelete == 0 &&
                                          a.Publish == true && a.IsSalonHoiQuan == false && a.Id != 24
                                    select new
                                    {
                                        SalonId = a.Id,
                                        SalonName = a.ShortName,
                                        CityId = b.ID,
                                        CityName = b.TenTinhThanh
                                    }
                                ).ToList();

                var listStaff = db.Staffs.Where(w => w.IsDelete == 0 && w.Active == 1 && SalonIds.Contains(w.SalonId.Value) && w.NgayTinhThamNien <= endDate && (w.NgayTinhThamNien != null && w.NgayTinhThamNien != mindate) && w.isAccountLogin == 0
                                               ).ToList().Select(s => new
                                               {
                                                   StaffId = s.Id,
                                                   SalonId = s.SalonId,
                                                   TimeWorking = (s.NgayTinhThamNien != null && s.NgayTinhThamNien != mindate) ? DateTime.Now.Subtract(s.NgayTinhThamNien.Value).Days : 0
                                               });

                var listProfile = (
                                    from a in db.Staffs
                                    join b in db.StaffProfileMaps on a.Id equals b.StaffId
                                    where SalonIds.Contains(a.SalonId.Value) &&
                                          a.IsDelete == 0 && a.Active == 1 && b.IsDelete == false && a.NgayTinhThamNien <= endDate && a.isAccountLogin == 0 &&
                                          (a.NgayTinhThamNien != null && a.NgayTinhThamNien != mindate) &&
                                          (b.StatusProfileId == 1 || b.StatusProfileId == 2 || b.StatusProfileId == 3)
                                    select new
                                    {
                                        StaffId = a.Id,
                                        SalonId = a.SalonId,
                                        ProfileId = b.StatusProfileId
                                    }
                                  ).ToList().GroupBy(
                                        a => a.StaffId, a => a, (key, value) => new { StaffId = key, ProfileQuantity = value.Count() }
                                  ).ToList();

                var listContract = (
                                    from a in db.Staffs
                                    join b in db.StaffContractMaps on a.Id equals b.StaffId
                                    where SalonIds.Contains(a.SalonId.Value) && a.NgayTinhThamNien <= endDate && a.isAccountLogin == 0 &&
                                          (a.NgayTinhThamNien != null && a.NgayTinhThamNien != mindate) &&
                                          a.IsDelete == 0 && a.Active == 1 && b.IsDelete == false && b.Active == true
                                    select new
                                    {
                                        StaffId = a.Id,
                                        SalonId = a.SalonId,
                                        ContractId = b.ContractId
                                    }
                                  ).ToList().GroupBy(
                                        a => a.StaffId, a => a, (key, value) => new { StaffId = key, ContractQuantity = value.Count() }
                                  ).ToList();

                var listStaffContract = (
                                           from a in listContract
                                           join b in listStaff on a.StaffId equals b.StaffId
                                           select new
                                           {
                                               SalonId = b.SalonId ?? 0,
                                               StaffId = a.StaffId,
                                               ContractQuantity = a.ContractQuantity
                                           }
                                        ).ToList();

                var listStaffProfile = (
                                           from a in listProfile
                                           join b in listStaff on a.StaffId equals b.StaffId
                                           select new
                                           {
                                               SalonId = b.SalonId ?? 0,
                                               StaffId = a.StaffId,
                                               ProfileQuantity = a.ProfileQuantity
                                           }
                                        ).ToList();

                var listStaffContractMap = db.StaffContractMaps.Where(r => r.IsDelete == false && (r.ContractId == 3 || r.ContractId == 6) && r.ContractDate != null).Select(r => new
                {
                    Id = r.Id,
                    StaffId = r.StaffId,
                    ContractId = r.ContractId,
                    ContractDate = r.ContractDate,
                    IsReceived = r.IsReceived
                }).ToList();

                // map staffcontract voi staff
                var listStaffContractMapStaff = (
                     from a in listStaff
                     join b in listStaffContractMap on a.StaffId equals b.StaffId
                     select new
                     {
                         StaffId = a.StaffId,
                         ContractId = b.ContractId,
                         ContractDate = b.ContractDate,
                         IsReceived = b.IsReceived,
                         SalonId = a.SalonId
                     }).ToList();


                var list = listSalon.OrderByDescending(o => o.CityId).Select(s =>
                {
                    var totalStaff = listStaff.Where(w => w.SalonId == s.SalonId).Count();
                    //thu hồ sơ
                    var totalStaffHaveExp22 = listStaff.Where(w => w.SalonId == s.SalonId && w.TimeWorking >= 22).Count();
                    var totalStaffEnoughProfile = listStaffProfile.Where(w => w.SalonId == s.SalonId && w.ProfileQuantity >= 3).Count();
                    var TotalPercentFiles = totalStaffHaveExp22 > 0 ? (double)totalStaffEnoughProfile / (double)totalStaffHaveExp22 * 100 : 0;
                    //hợp đồng học nghề
                    var TotalStaffHaveExp20 = listStaff.Where(w => w.SalonId == s.SalonId && w.TimeWorking >= 20).Count();
                    var TotalStaffContract = listStaffContract.Where(w => w.SalonId == s.SalonId && w.ContractQuantity >= 1).Count();
                    var TotalPercentContractStudy = TotalStaffHaveExp20 > 0 ? (double)TotalStaffContract / (double)TotalStaffHaveExp20 * 100 : 0;
                    //hợp đồng lao động
                    var totalHDLDGoOut = listStaffContractMapStaff.Where(w => w.SalonId == s.SalonId).Count();
                    var totalHDLDReceived = listStaffContractMapStaff.Where(w => w.SalonId == s.SalonId && w.IsReceived == true).Count();
                    var totalPercentHDLDGoOutRecieved = totalHDLDGoOut > 0 ? ((double)totalHDLDReceived / (double)totalHDLDGoOut * 100) : 0;
                    return new
                    {
                        STT = index++,
                        CityId = s.CityId,
                        CityName = s.CityName,
                        SalonId = s.SalonId,
                        SalonName = s.SalonName,
                        TotalStaff = totalStaff,
                        //thu hồ sơ
                        TotalStaffHaveExp22 = totalStaffHaveExp22,
                        TotalStaffEnoughProfile = totalStaffEnoughProfile,
                        TotalPercentFiles = TotalPercentFiles > 100 ? 100 : TotalPercentFiles,
                        //hợp đồng học nghề
                        TotalStaffHaveExp20 = TotalStaffHaveExp20,
                        TotalStaffContract = TotalStaffContract,
                        TotalPercentContractStudy = TotalPercentContractStudy > 100 ? 100 : TotalPercentContractStudy,
                        //hợp đồng lao động
                        TotalHDLDGoOut = totalHDLDGoOut,
                        TotalHDLDReceived = totalHDLDReceived,
                        TotalPercentHDLDGoOutRecieved = totalPercentHDLDGoOutRecieved > 100 ? 100 : totalPercentHDLDGoOutRecieved
                    };
                }).ToList();

                var listTotal = list.Select(s =>
                {
                    var TotalPercentFiles = Convert.ToDouble(Convert.ToDouble(list.Sum(su => su.TotalPercentFiles)) / SalonIds.Count) > 100 ? 100 : Convert.ToDouble(Convert.ToDouble(list.Sum(su => su.TotalPercentFiles)) / SalonIds.Count);
                    var TotalPercentContractStudy = Convert.ToDouble(Convert.ToDouble(list.Sum(su => su.TotalPercentContractStudy)) / SalonIds.Count) > 100 ? 100 : Convert.ToDouble(Convert.ToDouble(list.Sum(su => su.TotalPercentContractStudy)) / SalonIds.Count);
                    var TotalPercentHDLDGoOutRecieved = Convert.ToDouble(Convert.ToDouble(list.Sum(su => su.TotalPercentHDLDGoOutRecieved)) / SalonIds.Count) > 100 ? 100 : Convert.ToDouble(Convert.ToDouble(list.Sum(su => su.TotalPercentHDLDGoOutRecieved)) / SalonIds.Count);
                    var salonName = Math.Round(((TotalPercentFiles + TotalPercentContractStudy + TotalPercentHDLDGoOutRecieved) / 3), 2) + "%";

                    return new
                    {
                        STT = 1,
                        CityId = 0,
                        CityName = "30Shine",
                        SalonId = 0,
                        SalonName = salonName,
                        //thu hồ sơ
                        TotalStaff = list.Sum(su => su.TotalStaff),
                        TotalStaffHaveExp22 = list.Sum(su => su.TotalStaffHaveExp22),
                        TotalStaffEnoughProfile = list.Sum(su => su.TotalStaffEnoughProfile),
                        TotalPercentFiles = TotalPercentFiles,
                        //thu hợp đồng học nghề
                        TotalStaffHaveExp20 = list.Sum(su => su.TotalStaffHaveExp20),
                        TotalStaffContract = list.Sum(su => su.TotalStaffContract),
                        TotalPercentContractStudy = TotalPercentContractStudy,
                        TotalHDLDGoOut = listStaffContractMapStaff.Count(),
                        TotalHDLDReceived = listStaffContractMapStaff.Where(w => w.IsReceived == true).Count(),
                        TotalPercentHDLDGoOutRecieved = TotalPercentHDLDGoOutRecieved,
                    };
                }).FirstOrDefault();

                list.Insert(0, listTotal);
                return list;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Class output staff listing
        /// </summary>
        public class OutputStaffListing
        {
            public int StaffId { get; set; }
            public int? StaffProfileId { get; set; }
            public string FullName { get; set; }
            public string SalonName { get; set; }
            public string LevelName { get; set; }
            public string DepartmentName { get; set; }
            public string StaffCode { get; set; }
            public string StaffPhone { get; set; }
            public string StaffEmail { get; set; }
            //Nguồn tuyển dụng
            public string RecruitmentSource { get; set; }
            //Ngày tính thâm niên
            public int SeniorityDate { get; set; }
            //Tên hợp đồng
            public string ContractsName { get; set; }
            //Color cảnh báo
            public string ColorDate { get; set; }

            public string NgayTinhThamNien { get; set; }

        }
    }
    public partial class cls_SCSC_BindHairStyle_Vesion
    {
        public int StaffId { get; set; }
        public string StaffName { get; set; }
        public string hairName { get; set; }
        public Nullable<double> PointSCSC_TB { get; set; }
        public Nullable<double> TimeCatTB { get; set; }
    }
    public class cls_pointSkinner
    {
        public int StaffId { get; set; }
        public int Warning { get; set; }
        public int totalPoint { get; set; }
    }


}