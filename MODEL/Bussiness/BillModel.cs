﻿using _30shine.MODEL.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using _30shine.MODEL.ENTITY.EDMX;

namespace _30shine.MODEL.BO
{
    public class BillModel : IBillModel
    {
        Solution_30shineEntities db;
        public BillModel()
        {
            this.db = new Solution_30shineEntities();
        }

        public BillService Add(BillService bill)
        {
            try
            {
                var obj = new BillService();
                obj.CustomerId = bill.Id;
                obj.CustomerCode = bill.CustomerCode;
                obj.SalonId = bill.SalonId;
                obj.ServiceIds = bill.ServiceIds;
                obj.ProductIds = bill.ProductIds;
                obj.ReceptionId = bill.ReceptionId;
                obj.HCItem = bill.HCItem.TrimEnd(',');
                obj.Staff_Hairdresser_Id = bill.Staff_Hairdresser_Id;
                try
                {
                    obj.BookingId = bill.BookingId;
                }
                catch { }
                obj.BillCode = bill.BillCode;
                obj.PDFBillCode = bill.PDFBillCode;
                obj.CreatedDate = DateTime.Now;
                obj.IsDelete = 0;
                obj.Pending = 1;

                this.db.BillServices.Add(obj);
                this.db.SaveChanges();

                return obj;

                //// III. Insert bản ghi chi tiết
                //insertBillFlow(db, bill.salonID, billTemp.Id, billService.Id, bill.services);

                //// IV. Print bill
                //TimelinePrintBillPDF_Beta.GenPDF(false, billTemp, bookingTemp);

                //// V. Map ID BillService vào BillTemp
                //billTemp.BillServiceID = billService.Id;
                //db.SM_BillTemp.AddOrUpdate(billTemp);
                //db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool CheckHourPartTime(int BookingId, Staff staff, DateTime WorkDate)
        {
            bool check = false;
            //TimeSpan WorkHour = new TimeSpan(IntHour, IntMinute, 0);
            int StaffId = staff.Id;
            try
            {

                var WorkTime = db.FlowTimeKeepings.Where(r => r.IsDelete == 0 && r.IsEnroll == true && r.WorkDate == WorkDate.Date && r.StaffId == StaffId).FirstOrDefault();

                if (WorkTime != null)
                {
                    //Lay khung gio lam viec
                    int WorkTimeId = WorkTime.WorkTimeId.Value;
                    //Lay so gio tang ca
                    int WorkHour = WorkTime.WorkHour.Value;
                    //Lay chuoi gio tang ca
                    string[] strHourIds = WorkTime.HourIds.Split(',');
                    //var test = strHourIds.Length;
                    //int bookhourId = Convert.ToInt32(strHourIds[strHourIds.Length-1]);
                    int[] intHourIds = new int[strHourIds.Length];
                    for (int i = 0; i < strHourIds.Length; i++)
                    {
                        intHourIds[i] = strHourIds[i] != "" ? Convert.ToInt32(strHourIds[i]) : 0;
                    }
                    // kiem tra chuoi id gio, co gio tang ca hay khong
                    //TimeSpan BookHour = db.BookHours.Where(r => r.Id == bookhourId && r.IsDelete == 0).Select(r=>r.HourFrame).FirstOrDefault().Value;
                    //if (BookHour != null)
                    //{
                    var BookHourFrame = (
                                               from b in db.Bookings
                                               join a in db.BookHours on b.HourId equals a.Id
                                               where a.IsDelete == 0 && a.Publish == true && b.IsDelete == 0 && b.Id == BookingId
                                               select new
                                               {
                                                   HourFrame = a.HourFrame,
                                                   HourId = a.Id
                                               }).FirstOrDefault();


                    if (BookHourFrame != null)
                    {
                        var WorkTimeHour = db.WorkTimes.Where(r => r.Id == WorkTimeId).FirstOrDefault();
                        if (WorkTimeHour != null)
                        {
                            if (BookHourFrame.HourFrame < WorkTimeHour.StrartTime || BookHourFrame.HourFrame > WorkTimeHour.EnadTime)
                            {
                                check = Array.IndexOf(intHourIds, BookHourFrame.HourId) != -1 ? true : false;
                            }
                        }
                    }
                    //}

                }

            }
            catch (Exception ex)
            {
                check = false;
            }
            return check;
        }

        public bool Delete(int ID)
        {
            throw new NotImplementedException();
        }

        public List<BillService> GetListBySalon(DateTime timeFrom, DateTime timeTo, int salonID)
        {
            throw new NotImplementedException();
        }

        public List<BillService> GetListByStylist(DateTime timeFrom, DateTime timeTo, int staffID)
        {
            throw new NotImplementedException();
        }

        public void Update(BillService bill)
        {
            throw new NotImplementedException();
        }

        bool IBillModel.Delete(int ID)
        {
            throw new NotImplementedException();
        }

        BillService IBillModel.Update(BillService bill)
        {
            throw new NotImplementedException();
        }
    }
}