﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using _30shine.MODEL.Interface;
using _30shine.Helpers;
using _30shine.MODEL.ENTITY.EDMX;

namespace _30shine.MODEL.Bussiness
{
    public class PermisionGroupModel : IPermisionGroup
    {
        public Msg ListGroupPermision()
        {
            Msg result = new Msg();
            List<MODEL.ENTITY.EDMX.PermissionErp> list = new List<PermissionErp>();
            try
            {
                using (Solution_30shineEntities db = new Solution_30shineEntities())
                {
                    list = db.PermissionErps.Where(r => r.IsDelete == false).ToList();
                    result.success = true;
                    result.msg = "success!";
                    result.data = list;
                }
            }
            catch (Exception ex)
            {
                result.success = false;
                result.msg = "Đã có lỗi xảy ra";
            }
            return result;
        }
        /// <summary>
        /// Thêm mới nhóm quyền.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public Msg AddGroupPermision(ENTITY.EDMX.PermissionErp obj)
        {
            Msg result = new Msg();
            if (obj == null)
            {
                result.success = false;
                result.msg = "Data không hợp lệ!";
            }
            else
            {
                try
                {
                    using (Solution_30shineEntities db = new Solution_30shineEntities())
                    {
                        db.PermissionErps.Add(obj);
                        var exc = db.SaveChanges();
                        if (exc > 0)
                        {
                            result.success = true;
                            result.msg = "Thêm thành công ";
                        }
                        else
                        {
                            result.success = false;
                            result.msg = "Đã có lỗi xảy ra";
                        }
                    }
                }
                catch (Exception ex)
                {
                    result.success = false;
                    result.msg = "Đã có lỗi xảy ra";
                }
            }
            return result;
        }
        /// <summary>
        /// Kiểm tra nhóm quyền đã có trong db hay chưa
        /// </summary>
        /// <param name="namePermision"></param>
        /// <returns></returns>
        public Msg IsDuplicateGroupPermision(string namePermision)
        {
            Msg resutl = new Msg();
            if (namePermision == "")
            {
                resutl.success = false;
                resutl.msg = "Bạn chưa nhập tên nhóm quyền";
            }
            else
            {
                try
                {
                    using (Solution_30shineEntities db = new Solution_30shineEntities())
                    {
                        var record = db.PermissionErps.Where(r => r.Name == namePermision).FirstOrDefault();
                        if (record != null)
                        {
                            resutl.success = false;
                            resutl.msg = "Nhóm quyền đã tồn tại";
                        }
                        else
                        {
                            resutl.success = true;
                            resutl.msg = "success!";
                        }
                    }
                }
                catch (Exception ex)
                {
                    resutl.success = false;
                    resutl.msg = "Đã có lỗi xảy ra !.";
                }
            }
            return resutl;
        }
    }
}