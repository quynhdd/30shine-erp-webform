﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using _30shine.Helpers;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;

namespace _30shine.MODEL.Bussiness
{
    public class SalonServiceConfig : ISalonServiceConfig
    {
        /// <summary>
        /// get list by serviceid and departmentid
        /// </summary>
        /// <param name="ServiceId"></param>
        /// <param name="DepartmentId"></param>
        /// <returns></returns>
        public List<CustomClass.SalonServiceConfig> GetListByService(int ServiceId, int DepartmentId)
        {
            using (var db = new Solution_30shineEntities())
            {
                try
                {
                    string sql = $@"SELECT
                                     s.Id AS SalonId,
									 sv.Id AS ServiceId,
                                     s.ShortName AS SalonName,
                                     sv.Name AS ServiceName,
                                     ISNULL(c.Id,0) AS ConfigId,
                                     ISNULL(c.ServiceCoefficient,0) AS ServiceCoefficient, 
                                     ISNULL(c.ServiceBonus,0) AS ServiceBonus, 
                                     ISNULL(c.CoefficientOvertimeHour,0) AS CoefficientOvertimeHour, 
                                     ISNULL(c.CoefficientOvertimeDay,0) AS CoefficientOvertimeDay, 
                                     CASE 
                                     WHEN c.IsPublish IS NULL OR c.IsPublish = 0 THEN 0 ELSE 1 
                                     END IsPublish
                                  FROM dbo.Tbl_Salon AS s
                                  LEFT JOIN ServiceSalonConfig AS c ON c.SalonId = s.Id AND c.ServiceId = {ServiceId} AND c.IsDelete = 0 AND c.DepartmentId = {DepartmentId}
                                  LEFT JOIN [Service] AS sv ON sv.Id = {ServiceId} 
                                  WHERE s.IsDelete = 0 AND s.Publish = 1";
                    List<CustomClass.SalonServiceConfig> LstServiceConfig = db.Database.SqlQuery<_30shine.MODEL.CustomClass.SalonServiceConfig>(sql).ToList();
                    return LstServiceConfig;
                }
                catch
                {
                    throw new Exception();
                }
            }
        }

        /// <summary>
        /// get list by salonid and departmentid
        /// </summary>
        /// <param name="ServiceId"></param>
        /// <param name="DepartmentId"></param>
        /// <returns></returns>
        public List<CustomClass.SalonServiceConfig> GetListBySalon(int SalonId, int DepartmentId)
        {
            using (var db = new Solution_30shineEntities())
            {
                try
                {
                    string sql = $@"SELECT
                                        s.Id AS SalonId,
									    sv.Id AS ServiceId,
                                        s.ShortName AS SalonName,
                                        sv.Name AS ServiceName,
                                        ISNULL(c.Id,0) AS ConfigId,
                                        ISNULL(c.ServiceCoefficient,0) AS ServiceCoefficient, 
                                        ISNULL(c.ServiceBonus,0) AS ServiceBonus, 
                                        ISNULL(c.CoefficientOvertimeHour,0) AS CoefficientOvertimeHour, 
                                        ISNULL(c.CoefficientOvertimeDay,0) AS CoefficientOvertimeDay, 
                                        CASE 
                                        WHEN c.IsPublish IS NULL OR c.IsPublish = 0 THEN 0 ELSE 1 
                                        END IsPublish
                                    FROM dbo.[Service] AS sv
                                    LEFT JOIN ServiceSalonConfig AS c ON c.ServiceId = sv.Id AND c.IsDelete = 0 AND c.DepartmentId = {DepartmentId} AND c.SalonId = {SalonId}
                                    LEFT JOIN dbo.Tbl_Salon AS s ON s.Id = {SalonId} AND s.IsDelete = 0 AND s.Publish = 1
                                    WHERE 
								    sv.IsDelete = 0 AND sv.Publish = 1";
                    List<CustomClass.SalonServiceConfig> LstServiceConfig = db.Database.SqlQuery<_30shine.MODEL.CustomClass.SalonServiceConfig>(sql).ToList();
                    return LstServiceConfig;
                }
                catch
                {
                    throw new Exception();
                }
            }
        }

        public int Add(ServiceSalonConfig record)
        {
            try
            {
                var exc = 0;
                var obj = new ServiceSalonConfig();
                using (var db = new Solution_30shineEntities())
                {
                    if (record != null)
                    {
                        obj = record;
                    }
                    obj.CreatedTime = DateTime.Now;
                    obj.IsDelete = false;
                    db.ServiceSalonConfigs.Add(obj);
                    exc = db.SaveChanges();
                }
                return exc;
            }
            catch
            {
                throw new Exception();
            }
        }

        public int Update(ServiceSalonConfig record)
        {
            try
            {
                var exc = 0;
                var obj = new ServiceSalonConfig();
                using (var db = new Solution_30shineEntities())
                {
                    if (record != null)
                    {
                        obj = record;
                    }
                    obj.ModifiedTime = DateTime.Now;
                    obj.IsDelete = false;
                    db.ServiceSalonConfigs.AddOrUpdate(obj);
                    exc = db.SaveChanges();
                }
                return exc;
            }
            catch
            {
                throw new Exception();
            }
        }

        public int UpdatePublish(int Id, bool IsPublish)
        {
            try
            {
                var exc = 0;
                using (var db = new Solution_30shineEntities())
                {
                    ServiceSalonConfig record = db.ServiceSalonConfigs.FirstOrDefault(w => w.Id == Id);
                    record.IsPublish = IsPublish;
                    db.ServiceSalonConfigs.AddOrUpdate(record);
                    exc = db.SaveChanges();
                }
                return exc;
            }
            catch
            {
                throw new Exception();
            }
        }
    }
}