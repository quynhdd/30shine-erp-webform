﻿using _30shine.MODEL.CustomClass;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _30shine.MODEL.Bussiness
{
    public class Report_Sales_V2Model : IReport_Sale_V2
    {
        private Solution_30shineEntities db = new Solution_30shineEntities();

        public List<Report_Sales_V2.BCVH> BCVH(DateTime timeFrom, DateTime timeTo, int salonId)
        {
            var sql = @"DECLARE 
                        @SalonId int,
                        @FromDate date,
                        @ToDate date,
                        @time int
                        set @SalonId = " + salonId + @"
                        set @FromDate = '" + timeFrom + @"'
                        set @ToDate = '" + timeTo + @"'
                        set @time = DATEDIFF(DAY,@FromDate,@ToDate)
                       SELECT 
	                    CONVERT(float,p.SalonId) as SalonId,
                        MAX(e.ShortName) AS ShortName,
	                    SUM(ISNULL(e.TotalSales,0)) AS TongDoanhThu,
	                    SUM(ISNULL(e.TotalServiceProfit,0)) AS DoanhThuDV,
	                    SUM(ISNULL(e.TotalCosmeticProfit,0)) AS DoanhThuMP,
	                     ROUND((SUM(ISNULL(o.Stylist,0))/@time),2) AS Stylist,
	                     ROUND((SUM(ISNULL(o.Skinner,0))/@time),2) AS Skinner,
	                     ROUND((SUM(ISNULL(o.PerfectBill,0))/@time),2) AS PerfectBill,
	                     ROUND((SUM(ISNULL(o.GoodBill,0))/@time),2) AS GoodBill,
	                     ROUND((SUM(ISNULL(o.BadBill,0))/@time),2) AS BadBill,
	                     ROUND((SUM(ISNULL(o.OldCustomer,0))/@time),2) AS OldCustomer,
	                     ROUND((SUM(ISNULL(o.BookingOnline,0))/@time),2) AS BookOnline,
	                     ROUND((SUM(ISNULL(o.BillOver15m,0))/@time),2) AS BillTren15p,
	                     ROUND((SUM(ISNULL(o.BillUnder15m,0))/@time),2) AS BillDuoi15p,
	                     ROUND((SUM(ISNULL(o.BillTimeNull,0))/@time),2) AS BillTimeNull,
	                     ROUND((SUM(ISNULL(o.BillHasImage,0))/@time),2) AS BillHasImage,
	                     ROUND((SUM(ISNULL(o.ProfitByHour,0))/@time),2) AS Doanhthutheogio,
	                     ROUND((SUM(ISNULL(o.ProfitByDay,0))/@time),2) AS Doanhthutheongay,
	                     ROUND((SUM(ISNULL(o.ServiceByDay,0))/@time),2) AS Doanhsotheongay,
	                     ROUND((SUM(ISNULL(o.ServiceByHour,0))/@time),2) AS DoanhsotheoGio,
	                    CONVERT(INT,SUM(ISNULL(p.TongHoaDon,0))) AS TongHoaDon,
	                    CONVERT(INT,SUM(ISNULL(p.ShinCombo,0))) AS ShinCombo,
	                    CONVERT(INT,SUM(ISNULL(p.KidCombo,0))) AS KidCombo,
	                    CONVERT(INT,SUM(ISNULL(p.Protein,0))) AS Protein,
	                    CONVERT(INT,SUM(ISNULL(p.Duoi,0))) + CONVERT(INT,SUM(ISNULL(p.Uon,0))) AS Uon,
	                    CONVERT(INT,SUM(ISNULL(p.Uon,0))) AS Duoi,
	                    CONVERT(INT,SUM(ISNULL(p.Nhuom,0))) AS Nhuom,
	                    CONVERT(INT,SUM(ISNULL(p.Tay,0))) AS TayToc,
	                    CONVERT(INT,SUM(ISNULL(p.TayX2,0))) AS TayTocX2
	                    --e.DateCreated
                    FROM dbo.StaticExpense e 
	                    JOIN dbo.StaticOperate o ON e.SalonId = o.SalonId  AND o.WorkDate = e.WorkDate
	                    JOIN dbo.StaticServices_Profit p ON e.SalonId = p.SalonId  AND p.WorkDate = e.WorkDate
	                    WHERE 
	                    p.WorkDate >= @FromDate
	                    AND 
	                    p.WorkDate < @ToDate
	                    AND (p.SalonId = @SalonId OR @SalonId = 0) AND p.TongHoaDon > 0
	                    GROUP BY p.SalonId

                    UNION ALL

                        SELECT 
                            CONVERT(float,-1) as SalonId,
	                        'Toàn HT' AS ShortName,
	                        SUM(ISNULL(e.TotalSales,0)) AS TongDoanhThu,
	                        SUM(ISNULL(e.TotalServiceProfit,0)) AS DoanhThuDV,
	                        SUM(ISNULL(e.TotalCosmeticProfit,0)) AS DoanhThuMP,
	                        ROUND(SUM(ISNULL(o.Stylist,0))/CONVERT(FLOAT,COUNT(e.SalonId)),2) AS Stylist,
	                        ROUND(SUM(ISNULL(o.Skinner,0))/CONVERT(FLOAT,COUNT(e.SalonId)),2) AS Skinner,
	                        ROUND(SUM(ISNULL(o.PerfectBill,0))/CONVERT(FLOAT,COUNT(e.SalonId)),2) AS PerfectBill,
	                        ROUND(SUM(ISNULL(o.GoodBill,0))/CONVERT(FLOAT,COUNT(e.SalonId)),2) AS GoodBill,
	                        ROUND(SUM(ISNULL(o.BadBill,0))/CONVERT(FLOAT,COUNT(e.SalonId)),2) AS BadBill,
	                        ROUND(SUM(ISNULL(o.OldCustomer,0))/CONVERT(FLOAT,COUNT(e.SalonId)),2) AS OldCustomer,
	                        ROUND(SUM(ISNULL(o.BookingOnline,0))/CONVERT(FLOAT,COUNT(e.SalonId)),2) AS BookOnline,
	                        ROUND(SUM(ISNULL(o.BillOver15m,0))/CONVERT(FLOAT,COUNT(e.SalonId)),2) AS BillTren15m,
	                        ROUND(SUM(ISNULL(o.BillUnder15m,0))/CONVERT(FLOAT,COUNT(e.SalonId)),2) AS BillDuoi15p,
	                        ROUND(SUM(ISNULL(o.BillTimeNull,0))/CONVERT(FLOAT,COUNT(e.SalonId)),2) AS BillTimeNull,
	                        ROUND(SUM(ISNULL(o.BillHasImage,0))/CONVERT(FLOAT,COUNT(e.SalonId)),2) AS BillHasImage,
	                        ROUND(SUM(ISNULL(o.ProfitByHour,0))/CONVERT(FLOAT,COUNT(e.SalonId)),2) AS Doanhthutheogio,
	                        ROUND(SUM(ISNULL(o.ProfitByDay,0))/CONVERT(FLOAT,COUNT(e.SalonId)),2) AS Doanhthutheongay,
	                        ROUND(SUM(ISNULL(o.ServiceByDay,0))/CONVERT(FLOAT,COUNT(e.SalonId)),2) AS Dichvutheongay,
	                        ROUND(SUM(ISNULL(o.ServiceByHour,0))/CONVERT(FLOAT,COUNT(e.SalonId)),2) AS DichvutrenGio,
	                        CONVERT(INT,SUM(ISNULL(p.TongHoaDon,0))) AS TongHoaDon,
	                        CONVERT(INT,SUM(ISNULL(p.ShinCombo,0))) AS ShinCombo,
	                        CONVERT(INT,SUM(ISNULL(p.KidCombo,0))) AS KidCombo,
	                        CONVERT(INT,SUM(ISNULL(p.Protein,0))) AS Protein,
	                        CONVERT(INT,SUM(ISNULL(p.Duoi,0))) + CONVERT(INT,SUM(ISNULL(p.Uon,0))) AS Uon,
	                        CONVERT(INT,SUM(ISNULL(p.Uon,0))) AS Duoi,
	                        CONVERT(INT,SUM(ISNULL(p.Nhuom,0))) AS Nhuom,
	                        CONVERT(INT,SUM(ISNULL(p.Tay,0))) AS TayToc,
	                        CONVERT(INT,SUM(ISNULL(p.TayX2,0))) AS TayTocX2
	                        --e.DateCreated
                        FROM dbo.StaticExpense e 
	                        JOIN dbo.StaticOperate o ON e.SalonId = o.SalonId AND o.WorkDate = e.WorkDate
	                        JOIN dbo.StaticServices_Profit p ON e.SalonId = p.SalonId AND p.WorkDate = e.WorkDate
	                        WHERE 
	                        p.WorkDate >= @FromDate AND p.WorkDate < @ToDate AND (p.SalonId = @SalonId OR @SalonId = 0) AND p.TongHoaDon > 0
                        ";
            return db.Database.SqlQuery<CustomClass.Report_Sales_V2.BCVH>(sql).ToList();
        }

        public List<Report_Sales_V2.BC_KQKD> BC_KQKD(DateTime timeFrom, DateTime timeTo)
        {
            List<Report_Sales_V2.BC_KQKD> bckqkd = new List<Report_Sales_V2.BC_KQKD>();
            var sql = @"DECLARE 
                        @FromDate date,
                        @ToDate date
                        SET  @FromDate = '" + timeFrom + @"'
                        SET  @ToDate = '" + timeTo + @"'
                ;WITH temp AS(
                      SELECT 
	                    CONVERT(FLOAT,p.SalonId) AS SalonId,
	                    MAX(e.ShortName) AS ShortName,
	                    SUM(ISNULL(e.TotalSales,0)) AS TongDoanhThu,
	                    SUM(ISNULL(e.TotalServiceProfit,0)) AS DoanhThuDV,
	                    SUM(ISNULL(e.TotalCosmeticProfit,0)) AS DoanhThuMP,
	                    SUM(ISNULL(e.ProductSalary,0)) AS LuongMP,
	                    SUM(ISNULL(e.TotalProductPrice,0)) AS GiavonMP,
	                    SUM(ISNULL(e.TotalStaffSalary,0)) AS LuongNV,
	                    SUM(ISNULL(e.TotalProductUsed,0)) AS TongVT,
	                    SUM(ISNULL(e.OtherExpense,0)) AS ChiPhikhac,
	                    SUM(ISNULL(e.MKTExpense,0)) AS ChiPhiMKT,
	                    SUM(ISNULL(e.ITExpense,0)) AS ChiPhiIT,
	                    SUM(ISNULL(e.DayExpense,0)) AS ChiPhiTrongNgay,
	                    SUM(ISNULL(e.LNGAll30shine,0)) AS DTToanhethong,
	                    CONVERT(INT,SUM(ISNULL(p.TongHoaDon,0))) AS TongHoaDon,
	                    CONVERT(INT,SUM(ISNULL(p.ShinCombo,0))) AS ShinCombo,
	                    CONVERT(INT,SUM(ISNULL(p.KidCombo,0))) AS KidCombo,
	                    CONVERT(INT,SUM(ISNULL(p.Protein,0))) AS Protein,
	                    CONVERT(INT,SUM(ISNULL(p.Duoi,0))) AS Duoi,
	                    CONVERT(INT,SUM(ISNULL(p.Uon,0))) AS Uon,
	                    CONVERT(INT,SUM(ISNULL(p.Nhuom,0))) AS Nhuom,
	                    CONVERT(INT,SUM(ISNULL(p.Tay,0))) AS TayToc,
	                    CONVERT(INT,SUM(ISNULL(p.TayX2,0))) AS TayTocX2
                    FROM dbo.StaticExpense e 
	                    JOIN dbo.StaticServices_Profit p ON e.SalonId = p.SalonId AND p.WorkDate = e.WorkDate
	                    WHERE 
	                    p.WorkDate > @FromDate
	                    AND 
	                    p.WorkDate < @ToDate
	                    GROUP BY p.SalonId
	                 ),
					  TempGV_TienLuongNhanVien as 
                        (
						--select 8 as SalonS4M , ROUND(Cast(Coalesce(SUM(Coalesce(FlowSalary.fixSalary + FlowSalary.allowanceSalary + FlowSalary.partTimeSalary +									FlowSalary.serviceSalary + FlowSalary.productSalary, 0)),0) as float),0) as GV_TienLuongNhanVien
						--from FlowSalary
						--inner join Staff on FlowSalary.staffId = Staff.Id
						--where FlowSalary.IsDelete != 1 and FlowSalary.sDate >= @FromDate and FlowSalary.sDate < @ToDate
						--and Staff.[Type] = 11 
						SELECT 8 AS SalonS4M, ROUND(CAST(COALESCE(SUM(COALESCE(SalaryIncome.FixedSalary + SalaryIncome.AllowanceSalary + SalaryIncome.OvertimeSalary +						           SalaryIncome.ServiceSalary + SalaryIncome.ProductSalary, 0)), 0 )AS FLOAT), 0) AS GV_TienLuongNhanVien 
						FROM dbo.SalaryIncome 
						INNER JOIN dbo.Staff ON SalaryIncome.StaffId = Staff.Id
						WHERE SalaryIncome.IsDeleted=0 AND SalaryIncome.WorkDate>=@FromDate AND SalaryIncome.WorkDate<= @ToDate
						AND Staff.[Type] = 11
                        ),
                         --select * from TempGV_TienLuongNhanVien  end
                         TongDoanhThuMyPham as                          
                         (
						   select ksalon.Id as SalonS4M ,
							Cast(Coalesce(SUM(k.Value),0) as float) as TongDoanhTthuMyPham
							from KetQuaKinhDoanh_Salon as ksalon
							inner join KetQuaKinhDoanh_FlowImport as k on k.KQKDSalonId = ksalon.Id
							where  k.ImportDate > @FromDate and k.ImportDate < @ToDate  
							and KQKDItemId = 6 and ksalon.Id = 8 group by ksalon.Id
                         ) ,
                         --select * from TongDoanhThuMyPham
                         --- BH + QL + khác phân bổ
	                     TempCP_BH_QL_Khac as
	                     (
							select ksalon.Id as SalonS4M ,
							Cast(Coalesce(SUM(k.Value),0) as float) as CP_BH_QL_Khac
							from KetQuaKinhDoanh_Salon as ksalon
							inner join KetQuaKinhDoanh_FlowImport as k on k.KQKDSalonId = ksalon.Id
							where  k.ImportDate > @FromDate and k.ImportDate < @ToDate  
							and KQKDItemId = 2 and ksalon.Id = 8 group by ksalon.Id
	                     ),
	                     --- Marketing - FB hàng ngày (Số liệu tạm tính)
	                      TempCP_Marketing as
	                     (
							select ksalon.Id as SalonS4M ,
							Cast(Coalesce(SUM(k.Value),0) as float) as CP_Marketing
							from KetQuaKinhDoanh_Salon as ksalon
							inner join KetQuaKinhDoanh_FlowImport as k on k.KQKDSalonId = ksalon.Id
							where  k.ImportDate > @FromDate and k.ImportDate < @ToDate  
							and KQKDItemId = 3 and ksalon.Id = 8 group by ksalon.Id
	                     ),
	                     ---IT hàng ngày
	                      TempCP_IT as
	                     (
							select ksalon.Id as SalonS4M ,
							Cast(Coalesce(SUM(k.Value),0) as float) as CP_IT
							from KetQuaKinhDoanh_Salon as ksalon
							inner join KetQuaKinhDoanh_FlowImport as k on k.KQKDSalonId = ksalon.Id
							where  k.ImportDate > @FromDate and k.ImportDate < @ToDate  
							and KQKDItemId = 9 and ksalon.Id = 8 group by ksalon.Id
	                     ),
	                     -- Chi phí phát sinh trong ngày (theo chi tiết *)
	                     TempCP_PhatSinh as
	                     (
							select ksalon.Id as SalonS4M ,
							Cast(Coalesce(SUM(k.Value),0) as float) as CP_PhatSinh
							from KetQuaKinhDoanh_Salon as ksalon
							inner join KetQuaKinhDoanh_FlowImport as k on k.KQKDSalonId = ksalon.Id
							where  k.ImportDate > @FromDate and k.ImportDate < @ToDate  
							and KQKDItemId = 4 and ksalon.Id = 8 group by ksalon.Id
	                     ),
	                     BCKDS4M as 
	                     (
	                     select salon.Id , salon.SalonName,
	                     Coalesce(c1.GV_TienLuongNhanVien,0) as GV_TienLuongNhanVien ,
	                     Coalesce(c2.TongDoanhTthuMyPham,0) as TongDoanhTthuMyPham ,
	                     Coalesce(c3.CP_BH_QL_Khac,0) as CP_BH_QL_Khac ,
	                     Coalesce(c4.CP_Marketing,0)  as CP_Marketing,
	                     Coalesce(c5.CP_IT,0) as CP_IT,
	                     Coalesce(c6.CP_PhatSinh,0) as CP_PhatSinh
	                     from KetQuaKinhDoanh_Salon as salon
	                     left join TempGV_TienLuongNhanVien as c1 on salon.Id = c1.SalonS4M
	                     left join TongDoanhThuMyPham as c2 on salon.Id = c2.SalonS4M
	                     left join TempCP_BH_QL_Khac as c3 on salon.Id = c3.SalonS4M
	                     left join TempCP_Marketing as c4 on salon.Id = c4.SalonS4M
	                     left join TempCP_IT as c5 on salon.Id = c5.SalonS4M
	                     left join TempCP_PhatSinh as c6 on salon.Id = c6.SalonS4M
	                     where Id = 8
	                     ) 

						SELECT * from temp UNION all
	                     select 999,a.SalonName,0,0,a.TongDoanhTthuMyPham,0,0,a.GV_TienLuongNhanVien,0,a.CP_BH_QL_Khac,a.CP_Marketing,a.CP_IT,a.CP_PhatSinh,(a.TongDoanhTthuMyPham - a.CP_BH_QL_Khac - a.CP_IT - a.CP_Marketing - a.CP_PhatSinh - a.GV_TienLuongNhanVien - a.TongDoanhTthuMyPham) as LNG,0,0,0,0,0,0,0,0,0 from BCKDS4M as a
                          ";

           
            var list =  db.Database.SqlQuery<CustomClass.Report_Sales_V2.BC_KQKD>(sql).ToList();
            var sumData = (from l in list
                           group l by 1 into g
                           select new Report_Sales_V2.BC_KQKD
                           {
                               SalonId = -1,
                               ShortName = "Toàn HT",
                               TongHoaDon = (int)g.Sum(s => s.TongHoaDon),
                               ShinCombo = (int)g.Sum(s => s.ShinCombo),
                               KidCombo = (int)g.Sum(s => s.KidCombo),
                               Protein = (int)g.Sum(s => s.Protein),
                               Nhuom = (int)g.Sum(s => s.Nhuom),
                               Duoi = (int)g.Sum(s => s.Duoi),
                               Uon = (int)g.Sum(s => s.Uon),
                               TayToc = (int)g.Sum(s => s.TayToc),
                               TayTocX2 = (int)g.Sum(s => s.TayTocX2),
                               TongDoanhThu = (double)g.Sum(s => s.TongDoanhThu),
                               DoanhThuDV = (double)g.Sum(s => s.DoanhThuDV),
                               DoanhThuMP = (double)g.Sum(s => s.DoanhThuMP),
                               LuongMP = (double)g.Sum(s => s.LuongMP),
                               GiavonMP = (double)g.Sum(s => s.GiavonMP),
                               LuongNV = (double)g.Sum(s => s.LuongNV),
                               TongVT = (double)g.Sum(s => s.TongVT),
                               ChiPhiMKT = (double)g.Sum(s => s.ChiPhiMKT),
                               ChiPhiIT = (double)g.Sum(s => s.ChiPhiIT),
                               ChiPhikhac = (double)g.Sum(s => s.ChiPhikhac),
                               ChiPhiTrongNgay = (double)g.Sum(s => s.ChiPhiTrongNgay),
                               DTToanhethong = (double)g.Sum(s => s.DTToanhethong),
                           }).ToList();

            return list.Union(sumData).ToList();
        }

        public List<Report_Sales_V2.BC_KQKD> GetDataToday_BCKQKD(DateTime timeFrom, DateTime timeTo)
        {
            List<Report_Sales_V2.BC_KQKD> bckd = new List<Report_Sales_V2.BC_KQKD>();
            List<Report_Sales_V2.BC_KQKD> _bckd = new List<Report_Sales_V2.BC_KQKD>();
            var list1 = db.Store_BaoCaoDoanhSo_Version2(timeFrom, timeTo).ToList();
            var list2 = db.Store_BaoCaoThuChi_Version2(timeFrom, timeTo).ToList();
            var list3 = db.BaoCaoKinhDoanhS4M(timeFrom, timeTo).ToList();
            foreach (var item in list3)
            {
                Report_Sales_V2.BC_KQKD kQKD = new Report_Sales_V2.BC_KQKD();
                kQKD.SalonId = 999;
                kQKD.ShortName = item.SalonName;
                kQKD.TongHoaDon = 0;
                kQKD.ShinCombo = 0;
                kQKD.KidCombo = 0;
                kQKD.Protein = 0;
                kQKD.Duoi = 0;
                kQKD.Uon = 0;
                kQKD.Nhuom = 0;
                kQKD.TayToc = 0;
                kQKD.TayTocX2 = 0;
                kQKD.TongDoanhThu = 0;
                kQKD.DoanhThuDV = 0;
                kQKD.DoanhThuMP = item.TongDoanhTthuMyPham;
                kQKD.LuongMP = 0;
                kQKD.GiavonMP = 0;
                kQKD.LuongNV = item.GV_TienLuongNhanVien;
                kQKD.TongVT = 0;
                kQKD.ChiPhiMKT = item.CP_Marketing;
                kQKD.ChiPhikhac = item.CP_BH_QL_Khac;
                kQKD.ChiPhiIT = item.CP_IT;
                kQKD.ChiPhiTrongNgay = item.CP_PhatSinh;
                kQKD.DTToanhethong = item.LNG;
                bckd.Add(kQKD);
            }
            var listData = (from l1 in list1 join l2 in list2 on l1.SalonId equals l2.IdSalon1 select new { l1, l2 }).ToList();
            var sumData = (from l in listData
                           group l by 1 into g
                           select new Report_Sales_V2.BC_KQKD
                           {
                               SalonId = -1,
                               ShortName = "Toàn HT",
                               TongHoaDon = (int)g.Sum(s => s.l1.TongHoaDon),
                               ShinCombo = (int)g.Sum(s => s.l1.DsShineCombo),
                               KidCombo = (int)g.Sum(s => s.l1.DsKidCombo),
                               Protein = (int)g.Sum(s => s.l1.DsDuongProtein),
                               Nhuom = (int)g.Sum(s => s.l1.DsNhuom),
                               Duoi = (int)g.Sum(s => s.l1.DsDuoi),
                               Uon = (int)g.Sum(s => s.l1.DsUon),
                               TayToc = (int)g.Sum(s => s.l1.DsTay),
                               TayTocX2 = (int)g.Sum(s => s.l1.DsTay2Lan),
                               TongDoanhThu = (double)g.Sum(s => s.l2.TongDoanhThu),
                               DoanhThuDV = (double)g.Sum(s => s.l2.TongDoanhTthuDichVu),
                               DoanhThuMP = (double)g.Sum(s => s.l2.TongDoanhTthuMyPham),
                               LuongMP = (double)g.Sum(s => s.l2.GV_NhanVien_HoaHong),
                               GiavonMP = (double)g.Sum(s => s.l2.GV_MyPhamXuatBan),
                               LuongNV = (double)g.Sum(s => s.l2.GV_TienLuongNhanVien),
                               TongVT = (double)g.Sum(s => s.l2.GV_MyPhamVatTu),
                               ChiPhiMKT = (double)g.Sum(s => s.l2.CP_Marketing),
                               ChiPhiIT = (double)g.Sum(s => s.l2.CP_IT),
                               ChiPhikhac = (double)g.Sum(s => s.l2.CP_BH_QL_Khac),
                               ChiPhiTrongNgay = (double)g.Sum(s => s.l2.CP_PhtSinh),
                               DTToanhethong = (double)g.Sum(s => s.l2.LNG),
                           }).ToList();

            foreach (var item in listData)
            {
                Report_Sales_V2.BC_KQKD _bcvh = new Report_Sales_V2.BC_KQKD();
                _bcvh.SalonId = item.l1.SalonId;
                _bcvh.ShortName = item.l1.ShortName;
                _bcvh.TongHoaDon = item.l1.TongHoaDon != null ? item.l1.TongHoaDon.Value : 0;
                _bcvh.ShinCombo = item.l1.DsShineCombo != null ? item.l1.DsShineCombo.Value : 0;
                _bcvh.KidCombo = item.l1.DsKidCombo != null ? item.l1.DsKidCombo.Value : 0;
                _bcvh.Protein = item.l1.DsDuongProtein != null ? item.l1.DsDuongProtein.Value : 0;
                _bcvh.Nhuom = item.l1.DsNhuom != null ? item.l1.DsNhuom.Value : 0;
                _bcvh.Uon = item.l1.DsUon != null ? item.l1.DsUon.Value : 0;
                _bcvh.Duoi = item.l1.DsUon != null ? item.l1.DsDuoi.Value : 0;
                _bcvh.TayToc = item.l1.DsTay != null ? item.l1.DsTay.Value : 0;
                _bcvh.TayTocX2 = item.l1.DsTay2Lan != null ? item.l1.DsTay2Lan.Value : 0;
                _bcvh.TongDoanhThu = item.l2.TongDoanhThu != null ? item.l2.TongDoanhThu.Value : 0;
                _bcvh.DoanhThuDV = item.l2.TongDoanhTthuDichVu != null ? item.l2.TongDoanhTthuDichVu.Value : 0;
                _bcvh.DoanhThuMP = item.l2.TongDoanhTthuMyPham != null ? item.l2.TongDoanhTthuMyPham.Value : 0;
                _bcvh.LuongMP = item.l2.GV_NhanVien_HoaHong != null ? item.l2.GV_NhanVien_HoaHong.Value : 0;
                _bcvh.GiavonMP = item.l2.GV_MyPhamXuatBan != null ? item.l2.GV_MyPhamXuatBan.Value : 0;
                _bcvh.LuongNV = item.l2.GV_TienLuongNhanVien != null ? item.l2.GV_TienLuongNhanVien.Value : 0;
                _bcvh.TongVT = item.l2.GV_MyPhamVatTu != null ? item.l2.GV_MyPhamVatTu.Value : 0;
                _bcvh.ChiPhiMKT = item.l2.CP_Marketing != null ? item.l2.CP_Marketing.Value : 0;
                _bcvh.ChiPhiIT = item.l2.CP_IT != null ? item.l2.CP_IT.Value : 0;
                _bcvh.ChiPhikhac = item.l2.CP_BH_QL_Khac != null ? item.l2.CP_BH_QL_Khac.Value : 0;
                _bcvh.ChiPhiTrongNgay = item.l2.CP_PhtSinh != null ? item.l2.CP_PhtSinh.Value : 0;
                _bcvh.DTToanhethong = item.l2.LNG != null ? item.l2.LNG.Value : 0;
                _bckd.Add(_bcvh);
            }
            var listAll = _bckd.Union(bckd).ToList();
            return listAll.Union(sumData).ToList();
        }

        public List<Report_Sales_V2.BCVH> GetDataToday_BCVH(int SalonId, DateTime timeFrom, DateTime timeTo)
        {
            List<Report_Sales_V2.BCVH> bcvh = new List<Report_Sales_V2.BCVH>();
            var listData = db.Store_Report_Sale_V3(SalonId, timeFrom, timeTo).ToList();
            var numberSalon = listData.Count();
            var sumData = (from l in listData
                           group l by 1 into g
                           select new Report_Sales_V2.BCVH
                           {
                               SalonId = -1,
                               ShortName = "Toàn HT",
                               TongHoaDon = (int)g.Sum(s => s.TongHoaDon),
                               ShinCombo = (int)g.Sum(s => s.DsShineCombo),
                               KidCombo = (int)g.Sum(s => s.DsKidCombo),
                               Protein = (int)g.Sum(s => s.DsDuongProtein),
                               Nhuom = (int)g.Sum(s => s.DsNhuom),
                               Uon = (int)g.Sum(s => s.DsUon),
                               TayToc = (int)g.Sum(s => s.DsTay),
                               TayTocX2 = (int)g.Sum(s => s.DsTay2Lan),
                               TongDoanhThu = (double)g.Sum(s => s.TongDoanhThu),
                               DoanhThuDV = (double)g.Sum(s => s.TongDoanhTthuDichVu),
                               DoanhThuMP = (double)g.Sum(s => s.TongDoanhTthuMyPham),
                               Stylist = (double)Math.Round((double)(g.Sum(s => s.StylistDiLam)/numberSalon),2),
                               Skinner = (double)Math.Round((double)(g.Sum(s => s.SkinnerDiLam) / numberSalon), 2),
                               OldCustomer = (double)Math.Round((double)(g.Sum(s => s.PercentCusBack) / numberSalon), 2),
                               BillDuoi15p = (double)Math.Round((double)(g.Sum(s => s.PercentBill_ChoDuoi15) / numberSalon), 2),
                               BillTren15p = (double)Math.Round((double)(g.Sum(s => s.PercentBill_ChoTren15) / numberSalon), 2),
                               BillTimeNull = (double)Math.Round((double)(g.Sum(s => s.PercentBill_ChoNULL) / numberSalon), 2),
                               BookOnline = (double)Math.Round((double)(g.Sum(s => s.PercentCusBook) / numberSalon), 2),
                               PerfectBill = (double)Math.Round((double)(g.Sum(s => s.PercentBill_RatHaiLong) / numberSalon), 2),
                               GoodBill = (double)Math.Round((double)(g.Sum(s => s.PercentBill_HaiLong) / numberSalon), 2),
                               BadBill = (double)Math.Round((double)(g.Sum(s => s.PercentBill_ChuaHaiLong) / numberSalon), 2),
                               DoanhsotheoGio = (double)Math.Round((double)(g.Sum(s => s.PercentDoanhSOnWorkHour) / numberSalon), 2),
                               Doanhthutheogio = (double)Math.Round((double)(g.Sum(s => s.DoanhThuDVTrongKy) / numberSalon), 2),
                               Doanhthutheongay = (double)Math.Round((double)(g.Sum(s => s.DoanhThuDVTrongKy) / numberSalon), 2),
                               Doanhsotheongay = (double)Math.Round((double)(g.Sum(s => s.DoanhSoDVTrongKy) / numberSalon), 2),
                           }).ToList();
            foreach (var item in listData)
            {
                Report_Sales_V2.BCVH _bcvh = new Report_Sales_V2.BCVH();
                _bcvh.SalonId = item.SalonId;
                _bcvh.ShortName = item.ShortName;
                _bcvh.TongHoaDon = item.TongHoaDon;
                _bcvh.ShinCombo = item.DsShineCombo;
                _bcvh.KidCombo = item.DsKidCombo;
                _bcvh.Protein = item.DsDuongProtein;
                _bcvh.Nhuom = item.DsNhuom;
                _bcvh.Uon = item.DsUon;
                _bcvh.TayToc = item.DsTay;
                _bcvh.TayTocX2 = item.DsTay2Lan;
                _bcvh.TongDoanhThu = item.TongDoanhThu;
                _bcvh.DoanhThuDV = item.TongDoanhTthuDichVu;
                _bcvh.DoanhThuMP = item.TongDoanhTthuMyPham;
                _bcvh.Stylist = item.StylistDiLam;
                _bcvh.Skinner = item.SkinnerDiLam;
                _bcvh.OldCustomer = item.PercentCusBack;
                _bcvh.BillDuoi15p = item.PercentBill_ChoDuoi15;
                _bcvh.BillTren15p = item.PercentBill_ChoTren15;
                _bcvh.BillTimeNull = item.PercentBill_ChoNULL;
                _bcvh.BookOnline = item.PercentCusBook;
                _bcvh.PerfectBill = item.PercentBill_RatHaiLong;
                _bcvh.GoodBill = item.PercentBill_HaiLong;
                _bcvh.BadBill = item.PercentBill_ChuaHaiLong;
                _bcvh.DoanhsotheoGio = item.PercentDoanhSOnWorkHour;
                _bcvh.Doanhthutheogio = item.PercentDoanhThuOnWorkHour;
                _bcvh.Doanhthutheongay = item.DoanhThuDVTrongKy;
                _bcvh.Doanhsotheongay = item.DoanhSoDVTrongKy;
                bcvh.Add(_bcvh);
            }
            return bcvh.Union(sumData).ToList();
        }
       

        public List<Tbl_Salon> ListSalon()
        {
            return db.Tbl_Salon.Where(s => s.IsDelete == 0 && s.IsSalonHoiQuan == false).ToList();
        }

        public List<Report_Sales_V2.BCVH> GetDataToday_BCVH_v2(int SalonId, DateTime timeFrom, DateTime timeTo)
        {
            List<Report_Sales_V2.BCVH> bcvh = new List<Report_Sales_V2.BCVH>();
            var listData = db.Store_Report_Sale_V3(SalonId, timeFrom, timeTo).ToList();
            var numberSalon = listData.Count();
            var sumData = (from l in listData
                           group l by 1 into g
                           select new Report_Sales_V2.BCVH
                           {
                               SalonId = -1,
                               ShortName = "Toàn HT",
                               TongHoaDon = (int)g.Sum(s => s.TongHoaDon),
                               ShinCombo = (int)g.Sum(s => s.DsShineCombo),
                               KidCombo = (int)g.Sum(s => s.DsKidCombo),
                               Protein = (int)g.Sum(s => s.DsDuongProtein),
                               Nhuom = (int)g.Sum(s => s.DsNhuom),
                               Uon = (int)g.Sum(s => s.DsUon),
                               TayToc = (int)g.Sum(s => s.DsTay),
                               TayTocX2 = (int)g.Sum(s => s.DsTay2Lan),
                               TongDoanhThu = (double)g.Sum(s => s.TongDoanhThu),
                               DoanhThuDV = (double)g.Sum(s => s.TongDoanhTthuDichVu),
                               DoanhThuMP = (double)g.Sum(s => s.TongDoanhTthuMyPham),
                               Stylist = (double)Math.Round((double)(g.Sum(s => s.StylistDiLam) / numberSalon), 2),
                               Skinner = (double)Math.Round((double)(g.Sum(s => s.SkinnerDiLam) / numberSalon), 2),
                               OldCustomer = (double)Math.Round((double)(g.Sum(s => s.PercentCusBack) / numberSalon), 2),
                               BillDuoi15p = (double)Math.Round((double)(g.Sum(s => s.PercentBill_ChoDuoi15) / numberSalon), 2),
                               BillTren15p = (double)Math.Round((double)(g.Sum(s => s.PercentBill_ChoTren15) / numberSalon), 2),
                               BillTimeNull = (double)Math.Round((double)(g.Sum(s => s.PercentBill_ChoNULL) / numberSalon), 2),
                               BookOnline = (double)Math.Round((double)(g.Sum(s => s.PercentCusBook) / numberSalon), 2),
                               PerfectBill = (double)Math.Round((double)(g.Sum(s => s.PercentBill_RatHaiLong) / numberSalon), 2),
                               GoodBill = (double)Math.Round((double)(g.Sum(s => s.PercentBill_HaiLong) / numberSalon), 2),
                               BadBill = (double)Math.Round((double)(g.Sum(s => s.PercentBill_ChuaHaiLong) / numberSalon), 2),
                               BillHasImage = (double)Math.Round((double)(g.Sum(s => s.Percent_UpImage) / numberSalon), 2),
                               DoanhsotheoGio = (double)Math.Round((double)(g.Sum(s => s.PercentDoanhSOnWorkHour) / numberSalon), 2),
                               Doanhthutheogio = (double)Math.Round((double)(g.Sum(s => s.DoanhThuDVTrongKy) / numberSalon), 2),
                               Doanhthutheongay = (double)Math.Round((double)(g.Sum(s => s.DoanhThuDVTrongKy) / numberSalon), 2),
                               Doanhsotheongay = (double)Math.Round((double)(g.Sum(s => s.DoanhSoDVTrongKy) / numberSalon), 2),
                           }).ToList();
            //bcvh =  listData.Union(sumData);

            foreach (var item in listData)
            {
                Report_Sales_V2.BCVH _bcvh = new Report_Sales_V2.BCVH();
                _bcvh.SalonId = item.SalonId;
                _bcvh.ShortName = item.ShortName;
                _bcvh.TongHoaDon = item.TongHoaDon != null ? item.TongHoaDon.Value : 0;
                _bcvh.ShinCombo = item.DsShineCombo != null ? item.DsShineCombo.Value : 0;
                _bcvh.KidCombo = item.DsKidCombo != null ? item.DsKidCombo.Value : 0;
                _bcvh.Protein = item.DsDuongProtein != null ? item.DsDuongProtein.Value : 0;
                _bcvh.Nhuom = item.DsNhuom != null ? item.DsNhuom.Value : 0;
                _bcvh.Uon = item.DsUon != null ? item.DsUon.Value : 0;
                _bcvh.TayToc = item.DsTay != null ? item.DsTay.Value : 0;
                _bcvh.TayTocX2 = item.DsTay2Lan != null ? item.DsTay2Lan.Value : 0;
                _bcvh.TongDoanhThu = item.TongDoanhThu != null ? item.TongDoanhThu.Value : 0;
                _bcvh.DoanhThuDV = item.TongDoanhTthuDichVu != null ? item.TongDoanhTthuDichVu.Value : 0;
                _bcvh.DoanhThuMP = item.TongDoanhTthuMyPham != null ? item.TongDoanhTthuMyPham.Value : 0;
                _bcvh.Stylist = item.StylistDiLam != null ? item.StylistDiLam.Value : 0;
                _bcvh.Skinner = item.SkinnerDiLam != null ? item.SkinnerDiLam.Value : 0;
                _bcvh.OldCustomer = item.PercentCusBack != null ? item.PercentCusBack.Value : 0;
                _bcvh.BillDuoi15p = item.PercentBill_ChoDuoi15 != null ? item.PercentBill_ChoDuoi15.Value : 0;
                _bcvh.BillTren15p = item.PercentBill_ChoTren15 != null ? item.PercentBill_ChoTren15.Value : 0;
                _bcvh.BillTimeNull = item.PercentBill_ChoNULL != null ? item.PercentBill_ChoNULL.Value : 0;
                _bcvh.BookOnline = item.PercentCusBook != null ? item.PercentCusBook.Value : 0;
                _bcvh.PerfectBill = item.PercentBill_RatHaiLong != null ? item.PercentBill_RatHaiLong.Value : 0;
                _bcvh.GoodBill = item.PercentBill_HaiLong != null ? item.PercentBill_HaiLong.Value : 0;
                _bcvh.BadBill = item.PercentBill_ChuaHaiLong != null ? item.PercentBill_ChuaHaiLong.Value : 0;
                _bcvh.BillHasImage = item.Percent_UpImage != null ? item.Percent_UpImage.Value : 0;
                _bcvh.DoanhsotheoGio = item.PercentDoanhSOnWorkHour != null ? item.PercentDoanhSOnWorkHour.Value : 0;
                _bcvh.Doanhthutheogio = item.PercentDoanhThuOnWorkHour != null ? item.PercentDoanhThuOnWorkHour.Value : 0;
                _bcvh.Doanhthutheongay = item.DoanhThuDVTrongKy != null ? item.DoanhThuDVTrongKy.Value : 0;
                _bcvh.Doanhsotheongay = item.DoanhSoDVTrongKy != null ? item.DoanhSoDVTrongKy.Value : 0;
                bcvh.Add(_bcvh);

            }
            return bcvh.Union(sumData).ToList();
        }
    }
}