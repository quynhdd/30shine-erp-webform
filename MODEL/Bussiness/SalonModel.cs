﻿using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _30shine.MODEL.BO
{
    public class SalonModel : ISalonModel
    {
        private static SalonModel _Instance;
        /// <summary>
        /// Constructor
        /// </summary>
        public SalonModel()
        {

        }

        /**
         * Get Instance object
         * @return SalonModel
         **/
        public static SalonModel Instance
        {
            get
            {
                if( _Instance == null)
                {
                    _Instance = new SalonModel();
                }
                return _Instance;
            }
        }

        /// <summary>
        /// Lấy danh sách salon bao gồm cả hệ thống salon chính và salon hội quán
        /// </summary>
        /// <returns>List<Tbl_Salon></returns>
        public List<Tbl_Salon> GetList()
        {
            try
            {
                return this.db.Tbl_Salon.Where(w => w.IsDelete == 0 && w.Publish == true).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private Solution_30shineEntities db = new Solution_30shineEntities();

        /// <summary>
        /// Lấy danh sách salon chính (không bao gồm các salon hội quán)
        /// </summary>
        /// <returns>List<Tbl_Salon></returns>
        public List<Tbl_Salon> GetListCapital()
        {
            try
            {
                return this.db.Tbl_Salon.Where(w => w.IsDelete == 0 && w.Publish == true && w.IsSalonHoiQuan == false).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Lấy danh sách salon hội quán
        /// </summary>
        /// <returns>List<Tbl_Salon></returns>
        public List<Tbl_Salon> GetListHQ()
        {
            try
            {
                return this.db.Tbl_Salon.Where(w => w.IsDelete == 0 && w.Publish == true && w.IsSalonHoiQuan == true).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Lấy salon theo ID
        /// </summary>
        /// <param name="Id"></param>
        /// <returns>Tbl_Salon|null</returns>
        public Tbl_Salon GetItemByID(int id)
        {
            try
            {
                return this.GetList().FirstOrDefault(w=>w.Id == id);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Tbl_Salon GetByShortName(string shortName)
        {
            return db.Tbl_Salon.FirstOrDefault(w => w.ShortName == shortName);
        }
    }
}