﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
namespace _30shine.MODEL.Bussiness
{
    public class TinhThanhModel : ITinhThanhModel
    {
        private Solution_30shineEntities db;
        public TinhThanhModel()
        {
            db = new Solution_30shineEntities();
        }

        public List<QuanHuyen> GetListQuanHuyen(int IdTinhThanh)
        {
            try
            {
                var data = db.QuanHuyens.Where(p => p.TinhThanhID == IdTinhThanh).OrderBy(p => p.ThuTu).ToList();
                return data;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<TinhThanh> GetListTinhThanh()
        {
            try
            {
                return this.db.TinhThanhs.OrderBy(p => p.ThuTu).ToList();
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
    }
}