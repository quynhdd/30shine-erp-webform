﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Net.Mail;
using System.Net;
using _30shine.MODEL.Interface;
using System.Data.Entity.Migrations;

namespace _30shine.MODEL.Bussiness
{
    public class PermisionMenuModel : IPermisionMenu
    {
        public bool CheckDuplicatePageId(string pageId)
        {
            if (pageId != "")
            {
                try
                {
                    using (Solution_30shineEntities db = new Solution_30shineEntities())
                    {
                        var record = db.PermissionMenus.Where(r => r.PageId == pageId && r.IsDelete == false).FirstOrDefault();
                        if (record != null)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
                catch (Exception ex) { return false; }
            }
            else
            {
                return false;
            }
        }
        public Msg Update(ENTITY.EDMX.PermissionMenu obj)
        {
            Msg msg = new Msg();
            try
            {
                if (obj != null)
                {
                    using (Solution_30shineEntities db = new Solution_30shineEntities())
                    {
                        var record = db.PermissionMenus.Where(r => r.Id == obj.Id).FirstOrDefault();
                        if (record == null)
                        {
                            msg.success = false;
                            msg.msg = "Không tìm thấy bản ghi";
                        }
                        else
                        {
                            record.Link = obj.Link;
                            record.PageId = obj.PageId;
                            record.Path = obj.Path;
                            record.ModifiedTime = DateTime.Now;
                            record.Pid = obj.Pid;
                            record.Name = obj.Name;
                            record.IsActive = obj.IsActive;
                            db.PermissionMenus.AddOrUpdate(record);
                            var exc = db.SaveChanges();
                            if (exc > 0)
                            {
                                msg.success = true;
                                msg.msg = "thêm mới thành công";
                            }
                            else
                            {
                                msg.success = false;
                                msg.msg = "Thêm mới thất bại";
                            }
                        }
                    }
                }
                else
                {
                    msg.success = false;
                    msg.msg = "Data không hợp lệ";
                }
            }
            catch (Exception ex)
            {
                Send("nhuphuc1007@gmail.com", "ERROR MAIN: " + this.ToString() + ".Update", ex.Message);
                msg.success = false;
                msg.msg = ex.Message;
                return msg;
            }
            return msg;
        }
        public Msg Add(ENTITY.EDMX.PermissionMenu obj)
        {
            Msg msg = new Msg();
            try
            {
                if (obj != null)
                {
                    using (Solution_30shineEntities db = new Solution_30shineEntities())
                    {
                        db.PermissionMenus.Add(obj);
                        var exc = db.SaveChanges();
                        if (exc > 0)
                        {
                            msg.success = true;
                            msg.msg = "thêm mới thành công";
                        }
                        else
                        {
                            msg.success = false;
                            msg.msg = "Thêm mới thất bại";
                        }

                    }
                }
                else
                {
                    msg.success = false;
                    msg.msg = "Data không hợp lệ";
                }
            }
            catch (Exception ex)
            {
                Send("nhuphuc1007@gmail.com", "ERROR MAIN: " + this.ToString() + ".Update", ex.Message);
                msg.success = false;
                msg.msg = ex.Message;
                return msg;
            }
            return msg;
        }
        public Msg Delete(int id)
        {
            Msg msg = new Msg();
            try
            {
                using (Solution_30shineEntities db = new Solution_30shineEntities())
                {
                    var record = db.PermissionMenus.Where(r => r.Id == id).FirstOrDefault();
                    if (record != null)
                    {
                        record.IsDelete = true;
                        db.PermissionMenus.AddOrUpdate(record);
                        int exc = db.SaveChanges();
                        if (exc > 0)
                        {
                            msg.success = true;
                            msg.msg = "Xóa thành công";
                        }
                        else
                        {
                            msg.success = false;
                            msg.msg = "Đã có lỗi xảy ra";
                        }
                    }
                    else
                    {
                        msg.success = false;
                        msg.msg = "Đã có lỗi xảy ra";
                    }
                }
            }
            catch (Exception ex)
            {
                Send("nhuphuc1007@gmail.com", "ERROR MAIN: " + this.ToString() + ".Update", ex.Message);
                msg.success = false;
                msg.msg = ex.Message;
                return msg;
            }
            return msg;
        }
        public Msg GetById(int id)
        {
            Msg msg = new Msg();
            var record = new ENTITY.EDMX.PermissionMenu();
            try
            {
                using (Solution_30shineEntities db = new Solution_30shineEntities())
                {
                    record = db.PermissionMenus.Where(r => r.IsDelete == false && r.Id == id).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                Send("nhuphuc1007@gmail.com", "ERROR MAIN: " + this.ToString() + ".Update", ex.Message);
                msg.success = false;
                msg.msg = ex.Message;
                return msg;
            }
            msg.data = record;
            msg.success = true;
            msg.msg = "success!";
            return msg;

        }
        public Msg GetList(int MenuId)
        {
            Msg msg = new Msg();
            var list = new List<ENTITY.EDMX.PermissionMenu>();
            try
            {
                using (Solution_30shineEntities db = new Solution_30shineEntities())
                {

                    var recordMenu = db.PermissionMenus.Where(r => r.Id == MenuId && r.IsDelete == false).FirstOrDefault();
                    if (recordMenu != null)
                    {
                        if (recordMenu.Pid == 0)
                        {
                            list.Add(recordMenu);
                        }
                        else
                        {
                            list = db.PermissionMenus.Where(r => r.Pid == recordMenu.Pid && r.IsDelete == false).OrderByDescending(r => r.Id).ToList();
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Send("nhuphuc1007@gmail.com", "ERROR MAIN: " + this.ToString() + ".Update", ex.Message);
                msg.success = false;
                msg.msg = ex.Message;
                return msg;
            }
            msg.data = list;
            msg.success = true;
            msg.msg = "success!";
            return msg;
        }
        public Msg GetList()
        {
            Msg msg = new Msg();
            var list = new List<ENTITY.EDMX.PermissionMenu>();
            try
            {
                using (Solution_30shineEntities db = new Solution_30shineEntities())
                {
                    list = db.PermissionMenus.Where(r => r.IsDelete == false).OrderByDescending(r => r.Id).ToList();
                }
            }
            catch (Exception ex)
            {
                Send("nhuphuc1007@gmail.com", "ERROR MAIN: " + this.ToString() + ".Update", ex.Message);
                msg.success = false;
                msg.msg = ex.Message;
                return msg;
            }
            msg.data = list;
            msg.success = true;
            msg.msg = "success!";
            return msg;
        }
        public void Send(string ReceiveEmail, string subject, string body)
        {
            string SENDER_EMAIL = "phuc.2110@gmail.com";
            string SENDER_PASSWORD = "tflmxmdmgnyuqzhh";
            //Reading sender Email credential from web.config file
            string HostAdd = "smtp.gmail.com";
            string ToEmail = ReceiveEmail;

            //creating the object of MailMessage
            MailMessage mailMessage = new MailMessage();
            mailMessage.From = new MailAddress(SENDER_EMAIL); //From Email Id
            mailMessage.Subject = subject; //Subject of Email
            mailMessage.Body = body; //body or message of Email
            mailMessage.IsBodyHtml = true;
            //Adding Multiple recipient email id logic
            string[] Multi = ToEmail.Split(','); //spiliting input Email id string with comma(,)
            foreach (string Multiemailid in Multi)
            {
                mailMessage.To.Add(new MailAddress(Multiemailid)); //adding multi reciver's Email Id
            }
            SmtpClient smtp = new SmtpClient(); // creating object of smptpclient
            smtp.Host = HostAdd; //host of emailaddress for example smtp.gmail.com etc

            //network and security related credentials
            smtp.EnableSsl = true;
            NetworkCredential NetworkCred = new NetworkCredential();
            NetworkCred.UserName = mailMessage.From.Address;
            NetworkCred.Password = SENDER_PASSWORD;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = NetworkCred;
            smtp.Port = 587;
            smtp.Send(mailMessage); //sending Email
        }
    }
}