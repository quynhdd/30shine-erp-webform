﻿using _30shine.MODEL.ENTITY.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _30shine.MODEL.BO
{
    public class PayonConfigModel
    {
        Solution_30shineEntities db;
        private List<Tbl_Payon> list;
        public PayonConfigModel()
        {
            this.db = new Solution_30shineEntities();
            this.list = this.GetList();            
        }

        /// <summary>
        /// Lấy danh sách cấu hình lương cho bộ phận/nhân viên
        /// </summary>
        /// <returns></returns>
        public List<Tbl_Payon> GetList()
        {
            try
            {
                return this.db.Tbl_Payon.Where(w=>w.IsDelete == 0).ToList();
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Lấy giá trị cấu hình lương
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public int GetValueByKey(Staff staff, string key)
        {
            try
            {
                var item = this.list.FirstOrDefault(w => w.TypeStaffId == staff.Type && w.ForeignId == staff.SkillLevel && w.Key == key);
                if (item != null && item.Value != null)
                {
                    return item.Value.Value;
                }
                else
                {
                    return 0;
                }
            }
            catch
            {
                return 0;
            }
        }
    }
}