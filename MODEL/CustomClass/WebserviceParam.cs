﻿using _30shine.MODEL.ENTITY.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static Library.Class;

namespace _30shine.MODEL.CustomClass
{
    public class WebserviceParam
    {
    }

    public class Data_ThongkeVattu
    {
        public int? BillId { get; set; }
        public DateTime? CompletedTime { get; set; }
        public int? StylistId { get; set; }
        public int? SkinnerId { get; set; }
        public int? StylistOldId { get; set; }
        public int? SkinnerOldId { get; set; }
    }

    public class Data_StatisticSalary
    {
        public BillService Bill { get; set; }
        public bool IsVoucher { get; set; }
    }

    public class DataCustomer
    {
        public int Id { get; set; }
    }

    public class Input_Checkout
    {
        public BillService BillOld { get; set; }
        public int? StylistOld { get; set; }
        public int? SkinnerOld { get; set; }
        public int? ReceptionOld { get; set; }
        public int? SellerOld { get; set; }
        public BillService BillNew { get; set; }
        public int? StylistNew { get; set; }
        public int? SkinnerNew { get; set; }
        public int? ReceptionNew { get; set; }
        public int? SellerNew { get; set; }
        public bool? isStylistOvertimeOld { get; set; }
        public bool? isSkinnerOvertimeOld { get; set; }
        public bool? isStylistOvertimeNew { get; set; }
        public bool? isSkinnerOvertimeNew { get; set; }
        public bool? isVoucherLongTime { get; set; }
        public bool? isDelete { get; set; }
    }
}