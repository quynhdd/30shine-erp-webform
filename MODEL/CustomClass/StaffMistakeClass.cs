﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _30shine.MODEL.CustomClass
{
    public class StaffMistakeClass
    {
        public int Id { get; set; }
        public int? StaffId { get; set; }
        public int? FormMissTakeId { get; set; }
        public DateTime? CreateDate { get; set; }
        public string Description { get; set; }
        public int? Point { get; set; }
        public string Images { get; set; }
        public string Fullname { get; set; }
        public string Note { get; set; }
        public string Formality { get; set; }
        public int? SalonId { get; set; }
    }
}