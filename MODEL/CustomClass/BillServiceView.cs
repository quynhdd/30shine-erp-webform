﻿
namespace _30shine.MODEL.CustomClass
{
    public class BillServiceView
    {
        public int Id { get; set; }
        public string BillDate { get; set; }
        public string SalonName { get; set; }
        public string CustomerPhone { get; set; }
        public string CustomerName { get; set; }
        public string Img1 { get; set; }
        public string Img2 { get; set; }
        public string Img3 { get; set; }
        public string Img4 { get; set; }
        public string ImageStatusId { get; set; }
        public string ErrorNote { get; set; }
        public string NoteByStylist { get; set; }
        public string Stylist { get; set; }
        public int? CustomerId { get; set; }
        public string ServiceIds { get; set; }
        public int? ID_SCSC { get; set; }
        public int? ImageError { get; set; }
        public string NoteError { get; set; }
        public int? Shape_ID { get; set; }
        public int? SharpNess_ID { get; set; }
        public int? ComPlatetion_ID { get; set; }
        public int? ConnectTive_ID { get; set; }
        public int? HairTip_ID { get; set; }
        public int? HairRoot_ID { get; set; }
        public int? HairWaves_ID { get; set; }
        public int? TotalPointSCSC { get; set; }
        public int? ImageErrorCurling { get; set; }
        public int? TotalPointSCSCCurling { get; set; }
        public int? PointSCSC { get; set; }
        public int ImgId { get; set; }
        public string ImageCurlingBefore { get; set; }
        public string ImageCurlingAfter { get; set; }
    }
}