﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _30shine.MODEL.CustomClass
{
    public class AccountModelClass
    {
        public class AccountSession
        {
            public int UserId { get; set; }
            public string UserAccount { get; set; }
            public string UserCode { get; set; }
            public string UserName { get; set; }
            public string UserPhone { get; set; }
            public string UserEmail { get; set; }
            public string UserBirthDay { get; set; }
            public string UserPermission { get; set; }
            public string UserPermissionId { get; set; }
            public string UserMenu { get; set; }
            public int UserSalonId { get; set; }
            public string UserSalonName { get; set; }
            public int UserDepartmentId { get; set; }
            public string UserDepartmentName { get; set; }
            public int UserSkillLevelId { get; set; }
            public string UserSkillLevelName { get; set; }
        }
    }
}