﻿using System.Collections.Generic;

namespace _30shine.MODEL
{
    public class CheckListCSVCView
    {
        public int salonId { get; set; }
        public int creatorId { get; set; }
        public int category { get; set; }
        public string description { get; set; }
        public List<CheckItemCSVCView> list { get; set; }
    }
}