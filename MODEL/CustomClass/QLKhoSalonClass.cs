﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using _30shine.MODEL.ENTITY.EDMX;
namespace _30shine.MODEL.CustomClass
{
    public class QLKhoSalonClass:QLKho_SalonOrder
    {
        public string StatusTitle { get; set; }
        public string SalonName { get; set; }
    }
}