﻿using System.Collections.Generic;

namespace _30shine.MODEL
{
    public class CheckList3SView
    {
        public int? salonId { get; set; }
        public int? creatorId { get; set; }
        public int? category { get; set; }
        public string description { get; set; }
        public List<CheckItem3SView> list { get; set; }
    }
}