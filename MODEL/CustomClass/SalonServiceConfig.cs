﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _30shine.MODEL.CustomClass
{
    public class SalonServiceConfig
    {
        public string SalonName { get; set; }
        public string ServiceName { get; set; }
        public int SalonId { get; set; }
        public int ServiceId { get; set; }
        public int ConfigId { get; set; }
        public int DepartmentId { get; set; }
        public double ServiceCoefficient { get; set; }
        public double ServiceBonus { get; set; }
        public double CoefficientOvertimeHour { get; set; }
        public double CoefficientOvertimeDay { get; set; }
        public double CoefficientGolden { get; set; }
        public int IsPublish { get; set; }
    }
}