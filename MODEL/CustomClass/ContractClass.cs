﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using _30shine.MODEL.ENTITY.EDMX;
namespace _30shine.MODEL.CustomClass
{
    public class ContractClass : Contract
    {
        public string DepartmentName { get; set; }
        public string ContractNameType { get; set; }
    }
}