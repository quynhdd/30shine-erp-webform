﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _30shine.MODEL.CustomClass
{
    public class PermissionClass
    {
        public bool PermAccess { get; set; }
        public bool PermEdit { get; set; }
        public bool PermDelete { get; set; }
    }
}