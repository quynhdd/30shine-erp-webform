﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using _30shine.MODEL.ENTITY.EDMX;
namespace _30shine.MODEL.CustomClass
{
    public class StaffMistake5CClass:SalaryIncomeChange
    {
        public int InComeChangeId { get; set; }
        public string StaffName { get; set; }
        public string Department { get; set; }
        public string FormMistake { get; set; }
        public double? RatingSalary { get; set; }
        public int DeparmentId { get; set; }
        public int SkillLevel { get; set; }
    }
}