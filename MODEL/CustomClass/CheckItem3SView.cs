﻿
namespace _30shine.MODEL
{
    public class CheckItem3SView
    {
        public int itemId { get; set; }
        public bool isOk { get; set; }
        public string comment { get; set; }
        public string image { get; set; }
        public int staffId { get; set; }
    }
}