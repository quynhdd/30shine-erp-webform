﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _30shine.MODEL.CustomClass
{
    public class Report_Sales_V2
    {
        public class BCVH
        {
            //Doanh so
            public Nullable<double> SalonId { get; set; }
            public string ShortName { get; set; }
            public Nullable<int> TongHoaDon { get; set; }
            public Nullable<int> ShinCombo { get; set; }
            public Nullable<int> KidCombo { get; set; }
            public Nullable<int> Protein { get; set; }
            public Nullable<int> Uon { get; set; }
            public Nullable<int> DUoi { get; set; }
            public Nullable<int> Nhuom { get; set; }
            public Nullable<int> TayToc { get; set; }
            public Nullable<int> TayTocX2 { get; set; }
            //Doanh thu
            public Nullable<double> TongDoanhThu { get; set; }
            public Nullable<double> DoanhThuDV { get; set; }
            public Nullable<double> DoanhThuMP { get; set; }
            //Vận hành
            public Nullable<double> Stylist { get; set; }
            public Nullable<double> Skinner { get; set; }
            public Nullable<double> OldCustomer { get; set; }
            public Nullable<double> PerfectBill { get; set; }
            public Nullable<double> GoodBill { get; set; }
            public Nullable<double> BadBill { get; set; }
            public Nullable<double> BillHasImage { get; set; }
            public Nullable<double> BookOnline { get; set; }
            public Nullable<double> BillTren15p { get; set; }
            public Nullable<double> BillDuoi15p { get; set; }
            public Nullable<double> BillTimeNull { get; set; }
            public Nullable<double> Doanhthutheogio { get; set; }
            public Nullable<double> Doanhthutheongay { get; set; }
            public Nullable<double> Doanhsotheongay { get; set; }
            public Nullable<double> DoanhsotheoGio { get; set; }

        }

        public class BC_KQKD
        {
            //Doanh so
            public Nullable<double> SalonId { get; set; }
            public string ShortName { get; set; }
            public Nullable<int> TongHoaDon { get; set; }
            public Nullable<int> ShinCombo { get; set; }
            public Nullable<int> KidCombo { get; set; }
            public Nullable<int> Protein { get; set; }
            public Nullable<int> Duoi {get; set; }
            public Nullable<int> Uon { get; set; }
            public Nullable<int> Nhuom { get; set; }
            public Nullable<int> TayToc { get; set; }
            public Nullable<int> TayTocX2 { get; set; }
            //Doanh thu
            public Nullable<double> TongDoanhThu { get; set; }
            public Nullable<double> DoanhThuDV { get; set; }
            public Nullable<double> DoanhThuMP { get; set; }
            public Nullable<double> LuongMP { get; set; }
            public Nullable<double> GiavonMP { get; set; }
            public Nullable<double> LuongNV { get; set; }
            public Nullable<double> TongVT { get; set; }
            public Nullable<double> ChiPhikhac { get; set; }
            public Nullable<double> ChiPhiMKT { get; set; }
            public Nullable<double> ChiPhiIT { get; set; }
            public Nullable<double> ChiPhiTrongNgay { get; set; }
            public Nullable<double> DTToanhethong { get; set; }

        }
        public class DoanhSoTong
        {
            public int SalonId { get; set; }
            public int TotalRow { get; set; }
            public int BillRatHL { get; set; }
            public int BillHL { get; set; }
            public int BillKhongHL { get; set; }
            public int BillCoAnh { get; set; }
            public int BookOnline { get; set; }
        }
        public class Doanhso
        {
            public int SalonId { get; set; }
            public string SalonName { get; set; }
            //public int TotalCustomer { get; set; }
            public int ShineCombo { get; set; }
            public int KidCombo { get; set; }
            public int DuongProtein { get; set; }
            public int Uon { get; set; }
            public int Nhuom { get; set; }
            public int TayToc { get; set; }
            public int TayTocX2 { get; set; }
        }
        public class DoanhThu
        {
            public int SalonId { get; set; }
            public double Total { get; set; }
            public double TotalService { get; set; }
            public double TotalCosmetic { get; set; }
        }

        public class StaffKeeping
        {
            public int SalonId { get; set; }
            public double Stylist { get; set; }
            public double Skinner { get; set; }
        }
        public class Customer
        {
            public int SalonId { get; set; }
            public int OldCustomer { get; set; }
            //public int NewCustomer { get; set; }
        }
        public class TimeWaitting
        {
            public int SalonId { get; set; }
            public int Tren15p { get; set; }
            public int Duoi15p { get; set; }
            public int TimeNull { get; set; }
        }
        public class clsTotalTime
        {
            public int SalonId { get; set; }
            public int TotalTime { get; set; }
        }
        public class clsAvg
        {
            public int SalonId { get; set; }
            public double DoanhThuTheoGio { get; set; }
            public double DoanhSoTheoGio { get; set; }
            public double DoanhThuTheoNgay { get; set; }
            public double DoanhSoTheoNgay { get; set; }
        }


    }
}