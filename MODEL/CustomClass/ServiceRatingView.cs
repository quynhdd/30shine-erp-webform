﻿
namespace _30shine.MODEL.CustomClass
{
    public class ServiceRatingView
    {
        public int Id { get; set; }
        public int? BillServiceId{ get; set; }
        public int? SalonId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Slug { get; set; }
        public dynamic Meta { get; set; }
    }
}